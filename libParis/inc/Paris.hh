/****************************************************************************
 *    Copyright (C) 2016-2017 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _PARIS_CLASS
#define _PARIS_CLASS
/**
 * @file   Paris.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   March, 2017
 * @brief  Paris Class
 *
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#include "ParisCluster.hh"

class Paris : public BaseDetector
{
public:
    //! Constructor instantiation by type, number of channels, and calibration files
    Paris(const Char_t * Name,
          UShort_t NClusters,
          Bool_t RawData,
          Bool_t CalData,
          Bool_t DefaultCalibration,
          const Char_t * NameExt);
    ~Paris(void);


#ifdef WITH_ROOT
    //! Set Input/OutPut Option
    void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
    //! Set Parameter Patterns associated with Detector
    void SetParameters(Parameters* Par,Map* Map);
    //! Treat the data
    Bool_t Treat(void);

    UShort_t GetClusterNr(UShort_t BoardId,UShort_t ChannelId)
    {
        START;
        if(BoardId < MaxBoardId && ChannelId <MaxChannelId)
            return ClustersMap[BoardId][ChannelId];
        else
        {
            Char_t Message[1000];
            sprintf(Message,"In <%s><%s> Wrong Call to GetClusterNr(BoardId=%d,ChannelId=%d) : MaxBoardId  <  %d and Max Channel Id < 9", DetectorName, DetectorNameExt, BoardId, ChannelId, MaxBoardId);
            MErr * Er= new MErr(WhoamI,0,0, Message);
            throw Er;
        }
        END;
    }
    UShort_t GetDetectorNr(UShort_t BoardId,UShort_t ChannelId)
    {
        START;
        if(BoardId < MaxBoardId && ChannelId <MaxChannelId)
            return DetectorsMap[BoardId][ChannelId];
        else
        {
            Char_t Message[1000];
            sprintf(Message,"In <%s><%s> Wrong Call to GetDetectorNr(BoardId=%d,ChannelId=%d) : MaxBoardId  <  %d and Max Channel Id < 9", DetectorName, DetectorNameExt, BoardId, ChannelId, MaxBoardId);
            MErr * Er= new MErr(WhoamI,0,0, Message);
            throw Er;
        }
        END;
    };

    void SetTSRef(UShort_t Id, ULong64_t TS=0, Float_t CFD=0)
    {
        RefTS[Id] = TS;
        RefCFD[Id] = CFD;
        // Propagate to PARIS Clusters
        for(UShort_t i=0;i<NumberOfSubDetectors;i++)
            ((ParisCluster*) DetList->at(i))->SetTSRef(Id,TS,CFD);

    };


    void Clear(){
        BaseDetector::Clear();
        for(UShort_t i=0;i<4;i++)
            SetTSRef(i,0,0);
    }
protected:
    UShort_t MaxBoardId;
    UShort_t MaxChannelId;
    UShort_t **ClustersMap;
    UShort_t **DetectorsMap;
    Float_t *TOffsets;

    //! Allocate subcomponents
    void AllocateComponents(void);
    void GetTopology(void);
    void GetTimeOffsets(void);

    ULong64_t RefTS[4];
    Float_t RefCFD[4];

    BaseDetector *ParisQS;
    BaseDetector *ParisQL;
    BaseDetector *ParisCFD;
    BaseDetector *ParisT1;
    BaseDetector *ParisT2;
    BaseDetector *ParisT3;
    BaseDetector *ParisTHF;

};
#endif

