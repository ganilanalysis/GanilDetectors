/****************************************************************************
 *    Copyright (C) 2016-2017 by Antoine Lemasson
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _PARISCLUSTER_CLASS
#define _PARISCLUSTER_CLASS
/**
 * @file   ParisCluster.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   March, 2017
 * @brief  ParisCluster Class
 * 
 */

#include "Defines.hh"
#include "BaseDetector.hh"


class ParisCluster : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  ParisCluster(const Char_t * Name,
         UShort_t NDectectors,
         Bool_t RawData,
         Bool_t CalData,
         Bool_t DefaultCalibration,
         const Char_t * NameExt); 
  ~ParisCluster(void);
  
  
#ifdef WITH_ROOT
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);

  //! Create 2D Histograms for Calculated Parameters
  void CreateHistogramsCal2D(TDirectory *Dir);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);

  inline void SetTSRef(UShort_t Id, ULong64_t TS=0, Float_t CFD=0){RefTS[Id] = TS; RefCFD[Id]=CFD;};

  void Clear(){
      BaseDetector::Clear();
      for(UShort_t i=0;i<4;i++)
          SetTSRef(i,0,0);
  }


protected:

  //! Allocate subcomponents
  void AllocateComponents(void);
  ULong64_t RefTS[4];
  Float_t RefCFD[4];

};
#endif

