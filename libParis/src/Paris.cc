/****************************************************************************
 *    Copyright (C) 2016-2022 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "Paris.hh"

Paris::Paris(const Char_t *Name,
             UShort_t NClusters,
             Bool_t RawData,
             Bool_t CalData,
             Bool_t DefaultCalibration,
             const Char_t *NameExt
             )
    : BaseDetector(Name, 1, false, CalData, DefaultCalibration,false,NameExt)
{
    START;

    NumberOfSubDetectors = NClusters;
    fRawDataSubDetectors  = RawData;
    DetManager *DM = DetManager::getInstance();

    AllocateComponents();
    GetTopology();
    GetTimeOffsets();

    if(CalData)
    {
        sprintf(CalNameI[0],"ParisM");
    }

    END;
}
Paris::~Paris(void)
{
    START;
    ClustersMap = FreeDynamicArray<UShort_t>(ClustersMap ,MaxBoardId );
    DetectorsMap = FreeDynamicArray<UShort_t>(ClustersMap ,MaxBoardId );
    TOffsets = FreeDynamicArray<Float_t>(TOffsets);
     END;

}


void Paris::AllocateComponents(void)
{
    START;

    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Bool_t PosInfo = false;
    Char_t DName[100];
    UShort_t MaxMult = 1;
    MaxBoardId = 8;
    MaxChannelId = 16;

    ClustersMap = AllocateDynamicArray<UShort_t>(MaxBoardId ,MaxChannelId);
    DetectorsMap = AllocateDynamicArray<UShort_t>(MaxBoardId ,MaxChannelId);

    TOffsets = AllocateDynamicArray<Float_t>(NumberOfSubDetectors*9);
    for(UShort_t i=0;i<NumberOfSubDetectors;i++)
        for(UShort_t j=0;j<9;j++)
            TOffsets[i*9+j] = 0;


    for(UShort_t i=0;i<NumberOfSubDetectors;i++)
    {
        sprintf(DName,"Paris_C%02d",i);

        BaseDetector *Det = new ParisCluster(DName,9,RawData,CalData,DefCal,"");

        AddComponent(Det);
    }

    // Structure to store data in single branch
    ParisQS = NULL;
    ParisQL = NULL;
    ParisT1 = NULL;
    ParisT2 = NULL;
    ParisT3 = NULL;
    ParisTHF = NULL;

    CalData = true;
    RawData = false;
    sprintf(DName,"ParisQS");
    ParisQS = new BaseDetector (DName,NumberOfSubDetectors*9,RawData,CalData,DefCal,false,"",false,true,MaxMult);
    ParisQS->SetNoOutput();
    AddComponent(ParisQS);

    CalData = true;
    RawData = false;
    sprintf(DName,"ParisQL");
    ParisQL = new BaseDetector (DName,NumberOfSubDetectors*9,RawData,CalData,DefCal,false,"",false,true,MaxMult);
    ParisQL->SetNoOutput();
    AddComponent(ParisQL);

    CalData = true;
    RawData = false;
    sprintf(DName,"ParisCFD");
    ParisCFD = new BaseDetector (DName,NumberOfSubDetectors*9,RawData,CalData,DefCal,false,"",false,true,MaxMult);
    ParisCFD->SetNoOutput();
    AddComponent(ParisCFD);

    CalData = true;
    RawData = false;
    sprintf(DName,"ParisT1");
    ParisT1 = new BaseDetector (DName,NumberOfSubDetectors*9,RawData,CalData,DefCal,false,"");
    ParisT1->SetNoOutput();
    AddComponent(ParisT1);

    CalData = true;
    RawData = false;
    sprintf(DName,"ParisT2");
    ParisT2 = new BaseDetector (DName,NumberOfSubDetectors*9,RawData,CalData,DefCal,false,"");
    ParisT2->SetNoOutput();
    AddComponent(ParisT2);


    CalData = true;
    RawData = false;
    sprintf(DName,"ParisT3");
    ParisT3 = new BaseDetector (DName,NumberOfSubDetectors*9,RawData,CalData,DefCal,false,"");
    ParisT3->SetNoOutput();
    AddComponent(ParisT3);

    CalData = true;
    RawData = false;
    sprintf(DName,"ParisTHF");
    ParisTHF = new BaseDetector (DName,NumberOfSubDetectors*9,RawData,CalData,DefCal,false,"");
    ParisTHF->SetNoOutput();
    AddComponent(ParisTHF);

    END;
}

void Paris::SetParameters(Parameters* Par,Map* Map)
{ 
    START;
    for(UShort_t i=0; i< DetList->size(); i++)
        DetList->at(i)->SetParameters(Par,Map);
    END;
}

Bool_t Paris::Treat(void)
{ 
    START;
    Ctr->at(0)++;
    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->Treat();
        }
    }
    Float_t CFD =0;
    Float_t T =0;
    Long64_t TSDiff1 =0;
    Long64_t TSDiff2 =0;
    Long64_t TSDiff3 =0;
    Long64_t TSDiffHF =0;
    ULong64_t TS =0;
    for(UShort_t i=0;i<NumberOfSubDetectors;i++)
    {

        for(UShort_t k=0;k<GetSubDet(i)->GetSubDet(0)->GetCalM();k++)
        {
            ParisQS->SetCalData(GetSubDet(i)->GetSubDet(0)->GetNrAt(k)+i*9,GetSubDet(i)->GetSubDet(0)->GetCalAt(k),GetSubDet(i)->GetSubDet(0)->GetCalTSAt(k));
        }
        for(UShort_t k=0;k<GetSubDet(i)->GetSubDet(1)->GetCalM();k++)
        {
            ParisQL->SetCalData(GetSubDet(i)->GetSubDet(1)->GetNrAt(k)+i*9,GetSubDet(i)->GetSubDet(1)->GetCalAt(k),GetSubDet(i)->GetSubDet(1)->GetCalTSAt(k));

        }
        for(UShort_t k=0;k<GetSubDet(i)->GetSubDet(2)->GetCalM();k++)
        {
            CFD = GetSubDet(i)->GetSubDet(2)->GetCalAt(k);
            ParisCFD->SetCalData(GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9,GetSubDet(i)->GetSubDet(2)->GetCalAt(k),GetSubDet(i)->GetSubDet(2)->GetCalTSAt(k));

            TS = GetSubDet(i)->GetSubDet(2)->GetCalTSAt(k);
            TSDiff1 = (TS>RefTS[0] ? TS - RefTS[0] : - (RefTS[0]-TS)) ;
            TSDiff2 = (TS>RefTS[1] ? TS - RefTS[1] : - (RefTS[1]-TS)) ;
            TSDiff3 = (TS>RefTS[2] ? TS - RefTS[2] : - (RefTS[2]-TS)) ;
            TSDiffHF = (TS>RefTS[3] ? TS - RefTS[3] : - (RefTS[3]-TS)) ;
            //            cout    << "TOff set " << i << " " << GetSubDet(i)->GetSubDet(2)->GetNrAt(k) << " " << GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9
            //                    <<  " " <<  TOffsets[GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9] << endl;
            if(RefTS[0]>0)
                ParisT1->SetCalData(GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9,TSDiff1*10. + CFD-RefCFD[0] + TOffsets[GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9]);
            if(RefTS[1]>0)
                ParisT2->SetCalData(GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9,TSDiff2*10. + CFD-RefCFD[1] + TOffsets[GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9]);
            if(RefTS[2]>0)
                ParisT3->SetCalData(GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9,TSDiff3*10. + CFD-RefCFD[2] + TOffsets[GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9]);
            if(RefTS[3]>0)
                ParisTHF->SetCalData(GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9,TSDiffHF*10. + CFD-RefCFD[3] + TOffsets[GetSubDet(i)->GetSubDet(2)->GetNrAt(k)+i*9]);

        }

    }
    return(isPresent);
    END;
}

void Paris::GetTimeOffsets(void)
{
    START;
    MIFile *IF;
    char Line[255];
    int Len=255;
    Char_t *Ptr=NULL;
    UShort_t CoeffsRead = 0;

    stringstream *InOut;

    InOut = new stringstream();
    *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/ParisTOffsets.dat";
    *InOut>>Line;
    InOut = CleanDelete(InOut);


    UShort_t ClNr;
    UShort_t DetNr;
    Float_t T;
    if((IF = CheckCalibration(Line)))
    {
        UShort_t fLine;
        do
        {
            if((fLine =IF->GetLine(Line,Len)) >= V_INFO)
            {
                if((Ptr = CheckComment(Line))) //Comment line
                {
                    L->File << Ptr ;
                    L->File << endl;
                }
                else // Can read
                {
                    L->File << Line << endl;

                    if((sscanf(Line,"%hd %hd %f",&ClNr,&DetNr,&T) == 3))
                    {
                            if(ClNr<NumberOfSubDetectors && DetNr < 9)
                            {
                                TOffsets[ClNr*9+DetNr] = T;
                            }
//                            cout << " <" << ClNr << "><"<< DetNr << "> Toff : " << TOffsets[ClNr*9+DetNr]  << endl;
                            CoeffsRead++;
                    }
                    else
                    {
                        Char_t Message[1000];
                        sprintf(Message,"In <%s><%s> the Time Offsets file %s seems corrupted : Coeffs %d (out of %d)  has wrong format ! Expecting 8 values ", DetectorName, DetectorNameExt, IF->GetFileName(), CoeffsRead,  NumberOfSubDetectors*9);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                }
            }
        } while( (CoeffsRead < NumberOfSubDetectors*9) && (fLine>3));

        if(CoeffsRead<NumberOfSubDetectors*9)
        {
            Char_t Message[1000];
            sprintf(Message,"In <%s><%s> the Time Offsets file %s seems corrupted : Coeffs read : %d (out of %d)  has wrong format ! Expecting %d lines ", DetectorName, DetectorNameExt, IF->GetFileName(), CoeffsRead,  NumberOfSubDetectors*9,  NumberOfSubDetectors*9);
            MErr * Er= new MErr(WhoamI,0,0, Message);
            throw Er;
        }
        IF = CleanDelete(IF);
    }
    else
    {
        Char_t Message[600];
        sprintf(Message,"<%s><%s> Could not find the Paris Time Offsets file %s", DetectorName, DetectorNameExt, Line);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}

void Paris::GetTopology(void)
{
    START;
    MIFile *IF;
    char Line[255];
    int Len=255;
    Char_t *Ptr=NULL;
    UShort_t CoeffsRead = 0;

    stringstream *InOut;

    InOut = new stringstream();
    *InOut << getenv((EM->getPathVar()).c_str()) << "/Confs/ParisTopology.dat";
    *InOut>>Line;
    InOut = CleanDelete(InOut);


    UShort_t BoardId;
    UShort_t ChannelId;
    UShort_t ClNr;
    UShort_t DetNr;
    UShort_t type;
    Float_t X,Y,Z;
    if((IF = CheckCalibration(Line)))
    {
        UShort_t fLine;
        do
        {
            if((fLine =IF->GetLine(Line,Len)) >= V_INFO)
            {
                if((Ptr = CheckComment(Line))) //Comment line
                {
                    L->File << Ptr ;
                    L->File << endl;
                }
                else // Can read
                {
                    L->File << Line << endl;

                    if((sscanf(Line,"%hd %hd %hd %hd %hd %f %f %f",&ClNr,&DetNr,&BoardId,&ChannelId,&type,&X,&Y,&Z) == 8))
                    {
                        if(BoardId < MaxBoardId && ChannelId < MaxChannelId)
                        {
                            ClustersMap[BoardId][ChannelId] = ClNr;
                            DetectorsMap[BoardId][ChannelId] = DetNr;
                            if(ClNr<NumberOfSubDetectors)
                            {
                                DetList->at(ClNr)->SetRefPos(X,Y,Z);
                            }
 //                           cout << "Board Id " << BoardId << " Channel Id " << ChannelId << "-> <"<< ClNr << "><"<< DetNr << ">" << endl;
                            CoeffsRead++;
                        }
                        else
                        {
                            Char_t Message[1000];
                            sprintf(Message,"In <%s><%s> the calibration file %s seems corrupted : at line %d (out of %d)  has wrong format !  BoardId should be < %d and Channel Id < %d", DetectorName, DetectorNameExt, IF->GetFileName(), CoeffsRead,NumberOfSubDetectors*9,MaxBoardId,MaxChannelId);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                    }
                    else
                    {
                        Char_t Message[1000];
                        sprintf(Message,"In <%s><%s> the calibration file %s seems corrupted : Coeffs %d (out of %d)  has wrong format ! Expecting 8 values ", DetectorName, DetectorNameExt, IF->GetFileName(), CoeffsRead,  NumberOfSubDetectors*9);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                }
            }
        } while( (CoeffsRead < NumberOfSubDetectors*9) && (fLine>3));

        if(CoeffsRead<NumberOfSubDetectors*9)
        {
            Char_t Message[1000];
            sprintf(Message,"In <%s><%s> the calibration file %s seems corrupted : Coeffs read : %d (out of %d)  has wrong format ! Expecting %d lines ", DetectorName, DetectorNameExt, IF->GetFileName(), CoeffsRead,  NumberOfSubDetectors*9,  NumberOfSubDetectors*9);
            MErr * Er= new MErr(WhoamI,0,0, Message);
            throw Er;
        }
        IF = CleanDelete(IF);
    }
    else
    {
        Char_t Message[600];
        sprintf(Message,"<%s><%s> Could not find the Paris Topology file %s", DetectorName, DetectorNameExt, Line);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}


#ifdef WITH_ROOT
void Paris::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;

    // Set Paris Cluster Outputs
    for(UShort_t i = 0;i<NumberOfSubDetectors;i++)
    {

        DetList->at(i)->SetOpt(OutTTree,InTTree);
    }

    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
        }
        for(UShort_t i = 0;i<NumberOfSubDetectors;i++)
        {
            if(DetList->at(i)->HasRawData())
                DetList->at(i)->SetHistogramsRawSummary(true);
            if(DetList->at(i)->HasCalData())
                DetList->at(i)->SetHistogramsCalSummary(true);
        }

        // Add Specific
        for(UShort_t i = NumberOfSubDetectors;i<DetList->size();i++)
        {
            DetList->at(i)->SetHistogramsCal2DFolder("");
            DetList->at(i)->SetHistogramsCal1DFolder("");
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCalSummary(true);
                DetList->at(i)->SetHistogramsCal1D(false);
            }
        }
    }
    else if(fMode == MODE_D2R)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {

            if(i >= NumberOfSubDetectors)
              {
                if( DetList->at(i)->HasRawData())
                    if(i == NumberOfSubDetectors || i == (NumberOfSubDetectors+1)) // Attach TS VALUES
                        DetList->at(i)->SetOutAttachTS(true);

                DetList->at(i)->SetOutAttachRawV(true);
            }

            DetList->at(i)->OutAttach(OutTTree);

        }
        // Attach Ref TS for This event
        OutTTree->Branch(Form("%s_RefTS",DetectorName), RefTS, Form("%s_RefTS[4]/l",DetectorName));
        OutTTree->Branch(Form("%s_RefCFD",DetectorName), RefCFD, Form("%s_RefCFD[4]/F",DetectorName));
        OutAttach(OutTTree);

    }
    else if(fMode == MODE_D2A)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {

            if(i >= NumberOfSubDetectors)
              {
                if( DetList->at(i)->HasCalData())
                    if(i == NumberOfSubDetectors || i == (NumberOfSubDetectors+1)) // Attach TS VALUES
                        DetList->at(i)->SetOutAttachCalTS(true);

                DetList->at(i)->SetOutAttachCalV(true);
            }

            DetList->at(i)->OutAttach(OutTTree);

        }

        OutTTree->Branch(Form("%s_RefTS",DetectorName), RefTS, Form("%s_RefTS[3]/l",DetectorName));
        OutTTree->Branch(Form("%s_RefCFD",DetectorName), RefCFD, Form("%s_RefCFD[3]/F",DetectorName));

        OutAttach(OutTTree);
    }
    else if(fMode == MODE_R2A)
    {

        SetOutAttachCalV(true);

        OutAttach(OutTTree);

    }
    else if (fMode == MODE_RECAL)
    {
        SetInAttachCalI(true);
        InAttachCal(InTTree);

        SetOutAttachCalI(true);

        OutAttach(OutTTree);
    }
    else if (fMode == MODE_CALC)
    {
        SetNoOutput();
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}


#endif
