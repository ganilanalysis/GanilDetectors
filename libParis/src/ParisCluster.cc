/****************************************************************************
 *    Copyright (C) 2016-2017 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "ParisCluster.hh"

ParisCluster::  ParisCluster(const Char_t *Name,
                           UShort_t NDetectors,
                           Bool_t RawData,
                           Bool_t CalData,
                           Bool_t DefaultCalibration,
                           const Char_t *NameExt
                           )
    : BaseDetector(Name, 5*NDetectors, false, CalData, DefaultCalibration,false,NameExt)
{
    START;

    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;
    DetManager *DM = DetManager::getInstance();

    AllocateComponents();


    if(CalData)
    {
        // All Data to Correlate Time/Fast/Slow
        for(UShort_t i=0;i<NumberOfSubDetectors;i++)
        {
            sprintf(CalNameI[i*5],"%s_%02d_QS",Name,i);
            sprintf(CalNameI[i*5+1],"%s_%02d_QL",Name,i);
            sprintf(CalNameI[i*5+2],"%s_%02d_T",Name,i);
            sprintf(CalNameI[i*5+3],"%s_%02d_E1",Name,i);
            sprintf(CalNameI[i*5+4],"%s_%02d_E2",Name,i);
        }
    }

    END;
}
ParisCluster::~ParisCluster(void)
{
    START;
    END;

}


void ParisCluster::AllocateComponents(void)
{
    START;
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Bool_t PosInfo = false;
    Bool_t TSInfo = true;
    UShort_t MaxMult = 1;
    Char_t DName[500];
    Char_t PName[500];
    BaseDetector *Det = NULL;


        sprintf(DName,"%s%s_QS",DetectorName,DetectorNameExt);
        Det = new BaseDetector(DName,NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"",false,TSInfo,MaxMult);
        if(RawData)
        {
            for(UShort_t i=0;i<NumberOfSubDetectors;i++)
            {
                sprintf(PName,"%s%s_%02d_QS",DetectorName,DetectorNameExt,i);
                Det->SetParameterName(PName,i);
                if(CalData)
                {
                    sprintf(PName,"%s%s_%02d_QS_C",DetectorName,DetectorNameExt,i);
                    Det->SetCalName(PName,i);
                }
            }
        }
        AddComponent(Det);

        sprintf(DName,"%s%s_QL",DetectorName,DetectorNameExt);
        Det = new BaseDetector(DName,NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"",false,TSInfo,MaxMult);
        if(RawData)
        {
            for(UShort_t i=0;i<NumberOfSubDetectors;i++)
            {
                sprintf(PName,"%s%s_%02d_QL",DetectorName,DetectorNameExt,i);
                Det->SetParameterName(PName,i);
                if(CalData)
                {
                    sprintf(PName,"%s%s_%02d_QL_C",DetectorName,DetectorNameExt,i);
                    Det->SetCalName(PName,i);
                }
            }
        }
        AddComponent(Det);

        sprintf(DName,"%s%s_CFD",DetectorName,DetectorNameExt);
        Det = new BaseDetector(DName,NumberOfSubDetectors,false,true,DefCal,PosInfo,"",false,TSInfo,MaxMult);
        for(UShort_t i=0;i<NumberOfSubDetectors;i++)
        {
            if(CalData)
            {
                sprintf(PName,"%s%s_%02d_T_C",DetectorName,DetectorNameExt,i);
                Det->SetCalName(PName,i);
                Det->SetCalHistogramsParams(500,0,20,"ns");

            }
        }
        AddComponent(Det);

        sprintf(DName,"%s%s_Q1",DetectorName,DetectorNameExt);
        Det = new BaseDetector(DName,NumberOfSubDetectors,false,true,DefCal,PosInfo,"",false,TSInfo,MaxMult);
        for(UShort_t i=0;i<NumberOfSubDetectors;i++)
        {
            if(CalData)
            {
                sprintf(PName,"%s%s_%d_Q1_C",DetectorName,DetectorNameExt,i);
                Det->SetCalName(PName,0);
            }
        }
        AddComponent(Det);
        sprintf(DName,"%s%s_Q2",DetectorName,DetectorNameExt);
        Det = new BaseDetector(DName,NumberOfSubDetectors,false,true,DefCal,PosInfo,"",false,TSInfo,MaxMult);
        for(UShort_t i=0;i<NumberOfSubDetectors;i++)
        {
            if(CalData)
            {
                sprintf(PName,"%s%s_%d_Q2_C",DetectorName,DetectorNameExt,i);
                Det->SetCalName(PName,0);
            }
        }
        AddComponent(Det);
    END;
}

void ParisCluster::SetParameters(Parameters* Par,Map* Map)
{ 
    START;
//    for(UShort_t i=0; i< DetList->size(); i++)
//        DetList->at(i)->SetParameters(Par,Map);

    END;
}

Bool_t ParisCluster::Treat(void)
{ 
    START;
    Ctr->at(0)++;
    Float_t QS=0;
    Float_t QL=0;
    Float_t Q1=0;
    Float_t Q2=0;
#ifdef WITH_ROOT
    Float_t thetax = 45.*TMath::DegToRad();
    Float_t thetay = 24.*TMath::DegToRad();
#else
    Float_t thetax = 45.*3.14159265/180.;
    Float_t thetay = 24.*3.14159265/180.;
#endif
    Float_t k=1/cos(thetax+thetay);

    if(isComposite)
    {
        for(UShort_t i=0; i< 3; i++)
        {
            DetList->at(i)->Treat();

        }

        if(fCalData)
        {
            for(UShort_t j=0; j< DetList->at(0)->GetNumberOfDetectors(); j++)
            {
                QS = DetList->at(0)->GetCal(j);
                QL = DetList->at(1)->GetCal(j);

                SetCalData(j*5,QS);
                SetCalData(j*5+1,QL);
                SetCalData(j*5+2,DetList->at(2)->GetCal(j));

                if(QS>0 && QL>0)
                {
                    Q1 = k * (QS * cos(thetax) - QL * sin(thetax));
                    Q2 = k * (-QS * sin(thetay) - QL * cos(thetay));
                    DetList->at(3)->SetCalData(j,Q1);
                    DetList->at(4)->SetCalData(j,Q2);

                    SetCalData(j*5+3,Q1);
                    SetCalData(j*5+4,Q2);
                }
            }
        }
    }

    return(isPresent);
    END;
}



#ifdef WITH_ROOT
void ParisCluster::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;

   // Set histogram Hierarchy
   // SetMainHistogramFolder("");
    for(UShort_t i = 0;i<DetList->size();i++)
    {
 //       DetList->at(i)->SetMainHistogramFolder("");
        DetList->at(i)->SetHistogramsRaw1DFolder("Raw1D");
        DetList->at(i)->SetHistogramsRaw2DFolder("Raw2D");      
        DetList->at(i)->SetHistogramsCal1DFolder("Cal1D");
        DetList->at(i)->SetHistogramsCal2DFolder("Cal2D");
        SetHistogramsCal1DFolder("Cal1D");
        SetHistogramsCal2DFolder("Cal2D");
    }

    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
        }
        if(fCalData)
        {
            SetHistogramsCal1D(false);
            SetHistogramsCal2D(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(true);
                DetList->at(i)->SetHistogramsRawSummary(false);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
            }
        }
    }
    else if(fMode == MODE_D2R)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
                DetList->at(i)->SetOutAttachRawV(true);
                DetList->at(i)->SetOutAttachRawF(false);
                DetList->at(i)->SetOutAttachRawI(false);
                DetList->at(i)->SetOutAttachTS(true);

            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if(fMode == MODE_D2A)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
            SetOutAttachRawI(false);
            SetOutAttachRawV(true);
        }
        if(fCalData)
        {
            // Do not store intermediate Data used for Plotting
            SetHistogramsCal1D(false);
            SetHistogramsCal2D(true);
            SetOutAttachCalI(false);
            SetOutAttachCalV(false);
        }

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetHistogramsRawSummary(true);

                DetList->at(i)->SetOutAttachRawV(false);
                DetList->at(i)->SetOutAttachRawF(false);
                DetList->at(i)->SetOutAttachRawI(false);
                DetList->at(i)->SetOutAttachTS(false);

            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(true);

                DetList->at(i)->SetOutAttachCalF(false);
                DetList->at(i)->SetOutAttachCalI(false);
                DetList->at(i)->SetOutAttachCalV(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }

        OutAttach(OutTTree);
    }
    else if(fMode == MODE_R2A)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(i == 0 || i == 1 )
                DetList->at(i)->SetInAttachRawV(true);
            else
                DetList->at(i)->SetInAttachRawI(true);
            DetList->at(i)->InAttach(InTTree);
        }

        SetOutAttachCalI(true);

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                if(i == 0 || i == 1)
                {
                    DetList->at(i)->SetHistogramsCalSummary(true);
                    DetList->at(i)->SetOutAttachCalI(false);
                    DetList->at(i)->SetOutAttachCalV(true);
                }
                else
                {
                    DetList->at(i)->SetHistogramsCalSummary(false);
                    DetList->at(i)->SetOutAttachCalI(true);
                    // DetList->at(i)->SetOutAttachRawI(true);
                }
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if (fMode == MODE_RECAL)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(i == 0 || i == 1)
                DetList->at(i)->SetInAttachCalV(true);
            else
                DetList->at(i)->SetInAttachCalI(true);

            DetList->at(i)->InAttachCal(InTTree);
        }
        SetInAttachCalI(true);
        InAttachCal(InTTree);

        SetOutAttachCalI(true);

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                if(i == 0 || i == 1)
                {
                    DetList->at(i)->SetHistogramsCalSummary(true);
                    DetList->at(i)->SetOutAttachCalI(false);
                    DetList->at(i)->SetOutAttachCalV(true);
                }
                else
                {
                    DetList->at(i)->SetHistogramsCalSummary(false);
                    DetList->at(i)->SetOutAttachCalI(true);
                    // DetList->at(i)->SetOutAttachRawI(true);
                }
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);
    }
    else if (fMode == MODE_CALC)
    {
        SetNoOutput();
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}


void ParisCluster::CreateHistogramsCal2D(TDirectory *Dir)
{
    START;
    string Name;

    BaseDetector::CreateHistogramsCal2D(Dir);
    
    Dir->cd("");

    if(SubFolderHistCal2D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal2D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }


    Char_t HTitle[500];
    Char_t HName[500];

    for(UShort_t i=0;i<NumberOfSubDetectors;i++)
    {

        sprintf(HName,"%s_%02d_SF",DetectorName,i);
        sprintf(HTitle,"%s_%02d Slow Fast",DetectorName,i);
        AddHistoCal(CalNameI[i*5+1],CalNameI[i*5],HName,HTitle,500,0,4096,500,0,4096);

        sprintf(HName,"%s_%02d_ST",DetectorName,i);
        sprintf(HTitle,"%s_%02d Slow Time",DetectorName,i);
        AddHistoCal(CalNameI[i*5],CalNameI[i*5+2],HName,HTitle,500,0,4096,500,0,10);
    }


    END;
}



#endif
