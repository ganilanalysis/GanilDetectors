set (libNAME "PARIS")
MESSAGE("--------------------------")
MESSAGE("Building ${libNAME}")
MESSAGE("--------------------------")

include(GNUInstallDirs)

set(LIB_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
set(LIB_INC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/inc)

FILE(GLOB headers "${LIB_INC_DIR}/*.h" "${LIB_INC_DIR}/*.hh" )
FILE(GLOB src "${LIB_SRC_DIR}/*.cpp" "${LIB_SRC_DIR}/*.cc" )

get_property(BASEDETECTOR_INCLUDE_DIR GLOBAL PROPERTY BaseDetector_INCLUDE_DIR)

MESSAGE (STATUS "BaseDetector include directory : ${INC_DIR}")

include_directories(${LIB_INC_DIR} ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR} )
include_directories(${BASEDETECTOR_INCLUDE_DIR})
add_library(${libNAME} SHARED ${src})
if(WITH_ROOT)
    target_link_libraries(${libNAME} ${ROOT_LIBRARIES})
endif(WITH_ROOT)
set_target_properties(${libNAME} PROPERTIES COMPILE_DEFINITIONS "${COMPDEF}")

install(TARGETS ${libNAME} EXPORT ${CMAKE_PROJECT_NAME}Exports
   LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
   RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
   )

INSTALL(FILES ${headers} DESTINATION include)
INSTALL(FILES ${src} DESTINATION include)


set_property(GLOBAL APPEND PROPERTY  ${libNAME}_INCLUDE_DIR "${LIB_INC_DIR}")
set_property(GLOBAL APPEND PROPERTY  GanilDetectors_LIBS "${libNAME}")
