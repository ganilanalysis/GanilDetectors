/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
/**
 * @file   OptionsDefs.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   April, 2013
 * @brief  Definitions of active methods
 * Activates analysis methods used by defining preprocessor variables.
 * Detectors can be chosen among :
 * Methods can be chosen among :
 * - SWAP_DC   : bit swap for DC/CRAMS (shoudl be deprecated for data taken after 2011)
 * - SECHIP    : Secante Hyperbolique method for DC 
 * - MULTIPEAK : Allow Multiple Peak in DC
 * - WEIGHTEDAVERAGE : Enable WEIGHTEDAVERAGE for DC

 * Presorting can be chosen among :
 * - PRESORT_DC Presort Data to keep only 3 larger pads in Drift Chambers
 */


#define DEGRADED_MODE 1


// Presort Data to keep only 3 pads in Drift Chambers
//#define PRESORT_DC

// Methods for DriftChambers 
// Deprecated -- Swap of Bit in Drift Chambers 
//#define SWAP_DC
// Deprecated -- Old Crams Configurations (e585) (2011)
//#define OLDCONF_CRAMS 
// New Chaing of DCs (after 2013)
#define DC_CHAINING
// New TMW Setup of CRAMS (Both TMW1 and 2 are 96 chan readout) before 2016
//#define OLDCONF_TMW

// Secante Hyperbolique
#define SECHIP_DC

// Fit Secante Hyperbolique MW
//#define FITSECHIP_MW

// Multiple Peak
#define MULTIPEAK_DC
// Weighted Average
//#define WEIGHTEDAVERAGE_DC

// Methods for EXOGAM Gamma
//#define ESS
#define GOCCE
//#define ECC_20MEV
//#define EXOGAM_PARTNER

//Methods for FALSTAFF postion
#define WEIGHTEDAVERAGE_FST


#define FPMW_PATTERN

// Methods for TPPAC
// #define PPAC_SWAP
// Methods for TPPAC
// #define TRACE_PPAC
// Methods for TPPAC
//#define GENSECHIP
// Methods for TPPAC
// #define PPAC_RATIO
