/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _NUMEXOPARAMETERS_CLASS
#define _NUMEXOPARAMETERS_CLASS
/**
 * @file   NUMEXOParameters.hh
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   December, 2012
 * @version 0.1
 * @todo 
 * @brief  Class of GaniExpt NUMEXOParameters
 * 
 * 
 */

#include "Defines.hh"
#include "LogManager.hh"

class NUMEXOParameters
{
public:
  NUMEXOParameters(Int_t MaxPar //!< Max Number of NUMEXOParameters
						);
  ~NUMEXOParameters(void)  noexcept(false);

  //! 
  static NUMEXOParameters* getInstance(void);


  //! Add a parameter to the parameter list
  void Add(UShort_t Board,UShort_t Channel, Int_t Sample =-1,string Pname="");
  //! Autmatic Add Parameter by board
  void AddBoard (Char_t* Name, UShort_t BoardId, UShort_t NChannels, Int_t NSamples=-1);
  //! Remove a parameter from the parameter list
  void Remove(UShort_t Pid);
  //! Allocate Parameter Arrays
  void AllocateArrays(void);
  //! Set Maximum number of parameters
  void SetMaxParameters(Int_t n){MaxNUMEXOParameters = n;};
  //! Set Number of parameters
  void SetNParameters(Int_t n){NParameters = n;};
   //! Set pointers to NULL
  void SetPointers(void);
  //! Empty the parameter List
  void Clear(void);
  //! Print the parameter List
  void Print(void);
  //! Get Parameters from exptname
  void GetNUMEXOParametersFromConfigFile(const Char_t* filename);
  //! Return Parameter Name from Parameter Id
  UInt_t GetPar(string pname);
  //! Return Parameter Name from Parameter Id
  UInt_t GetParId(const UShort_t& Board, const  UShort_t& Channel, const  Int_t& Sample);
  //! Return Parameter Name from Parameter Id
  inline string GetParName(UShort_t id){return PName[id];};
  //! Return Parameter Size from Parameter Id
  inline Int_t GetMaxParameters(void){return MaxNUMEXOParameters;};
  //! Return Parameter Size from Parameter Id
  inline Int_t GetNParameters(void){return NParameters;};


protected:
  MOFile *L; /// LogFile
  //! MLaximum number of Parameters
  Int_t MaxNUMEXOParameters;
  //! Number of Parameters
  Int_t NParameters;
  //! Parameters Name Table indexed by ParId
  string *PName;
  //! Board id for Params
  Int_t *PBoard;
  //! Channel Id for Params
  Int_t *PChannel;
  //! Sample Id for Params
  Int_t *PSample;
  //! Parameters Map Table indexed by Board, Chan, Samples
  Int_t ***PMap;
  
  //! Max Number of Numexo Boards 
  UShort_t MaxBoards;
  //! Max Number of Numexo Boards 
  UShort_t MaxSamples;
  //! Max Number of Numexo Boards 
  UShort_t MaxChannels;
private:
   // The static instance of the Parameters class:
   static NUMEXOParameters* instance;

};
#endif
