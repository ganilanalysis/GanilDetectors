/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _MIFILE_CLASS
#define _MIFILE_CLASS

/**
 * @file   MIFile.hh
 * @author Maurycy Rejmund (mrejmund@ganil.fr)
 * @date   
 * @version
 * @brief  Input File Class
 * 
 * 
 */
class MIFile
{
private :
  char MIFileName[200];
  UShort_t VerboseLevel;

public:
  ifstream File;
  
  MIFile(const char * FileName, UShort_t VLevel = 0){
    START;
	 VerboseLevel = VLevel;
    strncpy(MIFileName,FileName,200);
    Open();
    END;
  };

  ~MIFile()noexcept(false){
    START;
    Close();
    END;
  };

  char* GetFileName(void)
  {
  	 START;
  	 return MIFileName;
  	 END;
  }
  
  void Open(){
    START;
    File.exceptions (ifstream::failbit | ifstream::failbit | ifstream::badbit);
    try{
      File.open(MIFileName);
    }
    catch(exception & e)
      {
          char line[500];
		  sprintf(line,"Error opening input file: %s", MIFileName);
		  MErr * Er = NULL;
		  Er = new MErr(WhoamI,100,1,line);
		  throw Er;
      }

	 if(VerboseLevel > 7) 
		cout << "Input file " << MIFileName << " opened." << endl;   
    END;
  }; 
  
  UShort_t GetLine(char * Line, streamsize n){
    UShort_t CharsRead=0;
    UShort_t Count=0;
    START;
    try{
      File.getline(Line, n);
      CharsRead = File.gcount();
      for(UShort_t i=0;i<CharsRead;i++)
        if(strncmp(Line+i," ",1) == 0) 
          Count++;
      if(Count==CharsRead)
        {
#ifdef DEBUG
          cout << "Input line filled with spaces ignored" << endl;
#endif
          return(0);
        }
      else 
		  return(CharsRead);
    }
    catch(exception & e)
      {
		  if(EoF() || Fail())
			 {
				return(0);
			 }
		  else{
             char line[500];
			 sprintf(line,"I don't know what to return in File  %s", MIFileName);
			 MErr * Er = NULL;
			 Er = new MErr(WhoamI,100,1,line);
			 throw Er;
		  }
      }
    END;
  };
  bool EoF(){
    START;
    if(File.rdstate( ) & ios::eofbit)
      return(true);
	 else 
		return(false);
    END;
  };
  bool Fail(){
    START;
    if(File.rdstate( ) & ios::failbit)
		return(true);
	 else 
		return(false);
    END;
  };
  void Close(){
    START;
    if(File) 
      {
		  File.close();
		  if(VerboseLevel > 7) 
			 cout << "Input file " << MIFileName << " closed." << endl;
      }
    END;
  };
};
#endif
