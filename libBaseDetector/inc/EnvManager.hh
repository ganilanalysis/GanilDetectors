/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _ENVMANAGER_CLASS
#define _ENVMANAGER_CLASS
/**
 * @file   EnvManager.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   June, 2012-2015
 * @todo   EnvManager
 * @brief  Singleton to Handle Environment Variable
 * .
 */
#include "Defines.hh"
class EnvManager
{
private:
  // Constructor/destructor
  EnvManager (){
    _isSetup = false;
    _pathvar = "";

    _isWatcherSetup = false;
   _watcherservervar = "";
  }
  ~EnvManager () { }

public:
  // Public Interface 
  void setPathVar (string pathvar) { 
    _pathvar = pathvar; 
    _isSetup = true; 
    // std::cout << "Setting EnvManager Path to "<< _pathvar << std::endl;    
  }
  string getPathVar (void) 
  { 
    START;
    if (isSetup()) return _pathvar; 
    else
      {
        Char_t Message[300];
        sprintf(Message,"Environment Manager Path variable name not set ! \n  Use EM->setPathVar(\"VARNAME\") to provide Environment Variable Name;");
        MErr * Er = new MErr(WhoamI,0,1000, Message);
        throw Er; 
        return _pathvar; 
      }
    END;

  }
  // Public Interface 
  void setWatcherServerVar (string var) { 
    _watcherservervar = var; 
    _isWatcherSetup = true; 
    // std::cout << "Setting EnvManager Watcher Server  to "<< _watcherservervar << std::endl;    
  }
  string getWatcherServerVar (void) 
  { 
    START;
    if (isWatcherSetup()) return _watcherservervar; 
    else
      {
        Char_t Message[300];
        sprintf(Message,"Environment Manager Watcher variable name not set ! \n  Use EM->setWatcherServerVar(\"VARNAME\") to provide Environment Variable Name;");
        MErr * Er = new MErr(WhoamI,0,1000, Message);
        throw Er; 
        return _watcherservervar; 
      }
    END;

  }

  inline bool isSetup () { return _isSetup; }

  inline bool isWatcherSetup () { return _isWatcherSetup; }


  void print()
  {
    cout << "ANALYSIS Path Var : " <<  _pathvar << endl;
    cout << "Watcher Server Var : " << _watcherservervar << endl;
  }
 

  // Creation and destruction of singleton
  static EnvManager *getInstance ()
  {
    START;    
    if (NULL == _singleton)
      {        
        std::cout << "Creating EnvManager Instance " << std::endl;
        _singleton =  new EnvManager;
      }
    return _singleton;
    END;
  }

  static void kill ()
  {
    START;
    if (NULL != _singleton)
      {
        delete _singleton;
        _singleton = NULL;
      }
    END;
  }

private:
  // Setup Flag, set to true is Path provided
  bool _isSetup;
  // Setup Flag, set to true is Path provided
  bool _isWatcherSetup;
  // Name of Analysis PATH Environment Variable
  string  _pathvar;
  // Name of Watcher server Environment Variable
  string  _watcherservervar;
  static EnvManager *_singleton;
};

#endif
