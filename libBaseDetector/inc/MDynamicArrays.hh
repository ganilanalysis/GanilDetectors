/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _MDYNAMICARRAYS_TEMPLATE
#define _MDYNAMICARRAYS_TEMPLATE

/**
 * @file   MDynamicArrays.hh
 * @author Maurycy Rejmund (mrejmund@ganil.fr)
 * @date   
 * @version
 * @brief  MDynamicArrays Template
 *
 *     Use:
 *     UShort_t **my2dArr = AllocateDynamicArray<UShort_t>(4,4);
 *      my2dArr[0][0]=5;
 *      my2dArr[2][2]=8;
 *      cout << my2dArr[0][0] << my2dArr[0][1] << endl;
 *      cout << my2dArr[1][1] <<  my2dArr[2][2]<< endl;
 * 
 *      my2dArr = FreeDynamicArray<UShort_t>(my2dArr,4);
 */

template <typename T> 
T *AllocateDynamicArray( UShort_t D1)
{
  START;
  T *dynamicArray = NULL;
  try
    {
		dynamicArray = new T [D1];
    }
  catch(exception & e)
    {
      Char_t Line[100];
      sprintf(Line,"%s", e.what());
		MErr * Er = NULL;
      Er = new MErr(WhoamI,0,0, Line);
      throw Er;      
    }
  return dynamicArray;
  
  END;
}

template <typename T>
T* FreeDynamicArray(T* dArray)
{
  START;
  if(dArray)
    delete[] dArray;
  return NULL;
  END;
}

template <class T> 
T *AllocateDynamicClassArray( UShort_t D1)
{
  START;
  T *dynamicArray = NULL;
  try
    {
      dynamicArray = new T [D1];
    }
  catch(exception & e)
    {
      Char_t Line[100];
      sprintf(Line,"%s", e.what());
      MErr * Er = NULL;
      Er = new MErr(WhoamI,0,0, Line);
      throw Er;      
    }
  return dynamicArray;

  END;
}

template <class T>
T* FreeDynamicClassArray(T* dArray)
{
  START;
  if(dArray)
    delete[] dArray;
  return NULL;
  END;
}

template <typename T> 
T **AllocateDynamicArray( UShort_t D1, UShort_t D2)
{
  START;
  T **dynamicArray;
  try
    {
      dynamicArray = new T* [D1];
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  {
			 dynamicArray[i] = new T [D2];
		 }
    }
  catch(exception & e)
    {
      Char_t Line[100];
      sprintf(Line,"%s", e.what());
      MErr * Er = NULL;
      Er = new MErr(WhoamI,0,0, Line);
      throw Er;      
    }
  return dynamicArray;

  END;
}


template <typename T>
T** FreeDynamicArray(T** dArray, UShort_t D1)
{
  START;
  if(dArray)
    {
		for(UShort_t i = 0 ; i < D1 ; i++ )
		  {
			 delete[] dArray[i];
		  }
		delete[] dArray;
    }
  return NULL;
  END;
}

template <typename T> 
T ***AllocateDynamicArray( UShort_t D1, UShort_t D2, UShort_t D3)
{
  START;
  T ***dynamicArray = NULL;
  try
    {
		dynamicArray = new T** [D1];
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  {
			 dynamicArray[i] = new T* [D2];
			 
		  }
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  for( UShort_t j = 0 ; j < D2 ; j++ )
			 {
				dynamicArray[i][j] = new T [D3];
			 }
    }
  catch(exception & e)
    {
      Char_t Line[100];
      sprintf(Line,"%s", e.what());
      MErr * Er = NULL;
      Er = new MErr(WhoamI,0,0, Line);
      throw Er;      
    }
  return dynamicArray;
  END;
}

template <typename T>
T*** FreeDynamicArray(T*** dArray, UShort_t D1, UShort_t D2)
{
  START;
  if(dArray)
    {
		for( UShort_t i = 0 ; i < D1 ; i++ )
		  {
			 for( UShort_t j = 0 ; j < D2 ; j++ )
				{
				  delete[] dArray[i][j];
				}
			 delete[] dArray[i];
		  }
		delete[] dArray;
    }
  return NULL;
  END;
}

template <typename T> 
T ****AllocateDynamicArray( UShort_t D1, UShort_t D2, UShort_t D3, UShort_t D4)
{
  START;
  T ****dynamicArray = NULL;
  try
    {
      dynamicArray = NULL;
		dynamicArray = new T*** [D1];
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  {
			 dynamicArray[i] = NULL;
			 dynamicArray[i] = new T** [D2];
		  }
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  for( UShort_t j = 0 ; j < D2 ; j++ )
			 {
				dynamicArray[i][j] = NULL;
				dynamicArray[i][j] = new T* [D3];
			 }
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  for( UShort_t j = 0 ; j < D2 ; j++ )
			 for( UShort_t k = 0 ; k < D3 ; k++ )
				{
				  dynamicArray[i][j][k] = NULL;
				  dynamicArray[i][j][k] = new T [D4];
				}
    }
  catch(exception & e)
    {
      Char_t Line[100];
      sprintf(Line,"%s", e.what());
      MErr * Er = NULL;
      Er = new MErr(WhoamI,0,0, Line);
      throw Er;      
    }
  return dynamicArray;

  END;
}

template <typename T>
T**** FreeDynamicArray(T**** dArray, UShort_t D1, UShort_t D2, UShort_t D3)
{
  START;
  if(dArray)
    {
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  {
			 for( UShort_t j = 0 ; j < D2 ; j++ )
				{
				for( UShort_t k = 0 ; k < D3 ; k++ )
				  {
					 delete[] dArray[i][j][k];
				  }
				delete[] dArray[i][j];
				}
			 delete[]  dArray[i];
		  }
		delete[] dArray;
    }
  return NULL;
  END;
}

template <typename T> 
T *****AllocateDynamicArray( UShort_t D1, UShort_t D2, UShort_t D3, UShort_t D4,UShort_t D5)
{
  START;
  T *****dynamicArray = NULL;
  try
    {
      dynamicArray = NULL;
		dynamicArray = new T**** [D1];
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  {
			 dynamicArray[i] = NULL;
			 dynamicArray[i] = new T*** [D2];
		  }
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  for( UShort_t j = 0 ; j < D2 ; j++ )
			 {
				dynamicArray[i][j] = NULL;
				dynamicArray[i][j] = new T** [D3];
			 }
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  for( UShort_t j = 0 ; j < D2 ; j++ )
			 for( UShort_t k = 0 ; k < D3 ; k++ )
				{
				  dynamicArray[i][j][k] = NULL;
				  dynamicArray[i][j][k] = new T* [D4];
				}
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  for( UShort_t j = 0 ; j < D2 ; j++ )
			 for( UShort_t k = 0 ; k < D3 ; k++ )
			   for( UShort_t p = 0 ; p < D4 ; p++ )
				{
				  dynamicArray[i][j][k][p] = NULL;
				  dynamicArray[i][j][k][p] = new T [D5];
				}
    }
  catch(exception & e)
    {
      Char_t Line[100];
      sprintf(Line,"%s", e.what());
      MErr * Er = NULL;
      Er = new MErr(WhoamI,0,0, Line);
      throw Er;      
    }
  return dynamicArray;

  END;
}

template <typename T>
T***** FreeDynamicArray(T***** dArray, UShort_t D1, UShort_t D2, UShort_t D3, UShort_t D4)
{
  START;
  if(dArray)
    {
      for( UShort_t i = 0 ; i < D1 ; i++ )
		  {
			 for( UShort_t j = 0 ; j < D2 ; j++ )
				{
				for( UShort_t k = 0 ; k < D3 ; k++ )
				  {
				    for( UShort_t p = 0 ; p < D4 ; p++ ) 
				      {
					 delete[] dArray[i][j][k][p]; 
				      }
				    delete[] dArray[i][j][k];
				  }
				delete[] dArray[i][j];
				}
			 delete[]  dArray[i];
		  }
		delete[] dArray;
    }
  return NULL;
  END;
}




#endif
