/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef TIMESTAMP_CLASS
#define TIMESTAMP_CLASS

/**
 * @file   TimeStamp.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   Nov, 2012-2015
 * @brief  TimeStamp Class
 * 
 * TimeStamp class : Generic Detector to handle Timestamps 
 */

#include "Defines.hh"
#include "BaseDetector.hh"


class TimeStamp : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  TimeStamp(const Char_t * Name,
				  UShort_t NDetectors,
				  Bool_t RawData,
				  Bool_t CalData,
				  Bool_t DefaultCalibration,
				  const Char_t * NameExt); 
  ~TimeStamp(void);
 
 
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);
  //! Clear the data
  void Clear(void);
   //! Return Validated TS
  inline ULong64_t GetTS(void) {return TS;};

  void SetLowWord(const Char_t *PName); //! Set ParameterName for Low Word out of 48 Bit 
  void SetMidWord(const Char_t *PName); //! Set ParameterName for Middle Word out of 48 Bit 
  void SetHighWord(const Char_t *PName); //! Set ParameterName for High Word out of 48 Bit 

#ifdef WITH_ROOT
  void OutAttach(TTree *OutTree);
  void InAttachCal(TTree *InTTree);
#endif
  ULong64_t TS;
  ULong64_t TSRef;
  ULong64_t FirstTS;
  ULong64_t LastTS;

protected:

  //! Allocate subcomponents
  void AllocateComponents(void);

};

#endif
