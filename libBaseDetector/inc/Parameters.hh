/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _PARAMETERS_CLASS
#define _PARAMETERS_CLASS
/**
 * @file   Parameters.hh
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   December, 2012
 * @version 0.1
 * @todo 
 * @brief  Class of GaniExpt Parameters
 * 
 * 
 */

#include "Defines.hh"
#include "LogManager.hh"

class Parameters
{
public:
  Parameters(UShort_t MaxPar //!< Max Number of Parameters
						);
  ~Parameters(void)  noexcept(false);

  //! 
  static Parameters* getInstance(void);


  //! Add a parameter to the parameter list
  void Add(UShort_t Pid, string Pname, UShort_t PSize);
  //! Remove a parameter from the parameter list
  void Remove(UShort_t Pid);
  //! Allocate Parameter Arrays
  void AllocateArrays(void);
  //! Set Maximum number of parameters
  void SetMaxParameters(UShort_t n){MaxParameters = n;};
  //! Set Number of parameters
  void SetNParameters(UShort_t n){NParameters = n;};
   //! Set pointers to NULL
  void SetPointers(void);
  //! Empty the parameter List
  void Clear(void);
  //! Print the parameter List
  void Print(void);
  //! Get Parameters from exptname
  void GetParameters(const Char_t* expname);
  //! Get Parameters from exptname
  void GetParametersAGATA(const Char_t* expname, Int_t RunNr=-1);
  //! Get Parameters from Action File
  void GetParametersFromActionFile(Char_t* ActionFileName);


  //! Return Parameter Name from Parameter Id
  UShort_t GetPar(string pname);
   //! Return Parameter Name from Parameter Id
  inline string GetParName(UShort_t id){return PName[id];};
  //! Return Parameter Size from Parameter Id
  inline UShort_t GetParSize(UShort_t id){return PSize[id];};
  //! Return Parameter Size from Parameter Id
  inline UShort_t GetMaxParameters(void){return MaxParameters;};
  //! Return Parameter Size from Parameter Id
  inline UShort_t GetNParameters(void){return NParameters;};


protected:
  MOFile *L; /// LogFile
  //! MLaximum number of Parameters
  UShort_t MaxParameters;
  //! Number of Parameters
  UShort_t NParameters;
  //! Parameters Name Table indexed by ParId
  string *PName;
  //! Parameters Size Table indexed by ParId
  UShort_t *PSize;

private:
   // The static instance of the Parameters class:
   static Parameters* instance;

};
#endif
