/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _MYERR_CLASS
#define _MYERR_CLASS

/**
 * @file   MErr.hh
 * @author Maurycy Rejmund (mrejmund@ganil.fr)
 * @date   
 * @version
 * @brief  MErr Class for exceptions handling
 * 
 * 
 */

class MErr
{
public:
  static const int MaxDepth=30;
  static const int WhatLength=500;
  static const int WhoLength=500;
  int Depth;
  char Who[MaxDepth][WhoLength];
  char What[WhatLength];
  int FunctionCode;
  int ErrorCode;
  
  MErr()
  {
	 START;
	 Depth=0;
	 END;
  }
  MErr(const char * fWho, int fFunctionCode, int fErrorCode, const char * fWhat)
  {
	 START;
	 Depth=0;
         Set(fWho, fFunctionCode, fErrorCode, fWhat);
	 END;
  }
  ~MErr()noexcept(false)
  {
	 START;
	 END;
  }
  void Set(const char * fWho, int fFunctionCode, int fErrorCode, const char * fWhat) 
  {  
	 START;
	 if(Depth==0)
		{
		  strncpy(Who[Depth], fWho, WhoLength);
		  FunctionCode = fFunctionCode;
		  ErrorCode = fErrorCode;
		  strncpy(What, fWhat,WhatLength);
		  Depth++;
		}
	 else
		{
		  cout << "Warning:\n From : " << WhoamI << "\n" << 
			 "You should  use this function where the first error occurs !\n";
		  Set(fWho);
		}
#ifdef DEBUG
	 Print();
#endif      
	 END;
  } 
  void Set(const char * fWho) 
  {  
	 START;
	 if(Depth>0)
		{
		  strncpy(Who[Depth], fWho, WhoLength);
		  Depth++;
		}
	 else
		{
		  cout << "Error:\n From : " << WhoamI << "\n" << 
			 "Wrong use of this function at the first level !\n";
		}
#ifdef DEBUG
	 Print();
#endif      
	 END;
  } 
  void Print() 
  {  
	 START;
	 int i;
	 if(Depth>0)
		{
		  for(i = Depth-1; i>1;i--)
			 {
				cerr << "From Function : ";
				cerr << Who[i] << endl;
			 } 
		  cerr << "From Function : ";
		  cerr << Who[i] << endl;;
#ifdef DEBUG
		  cerr << " FunctionCode: " << FunctionCode<< " ErrorCode: " << ErrorCode << endl;
#endif      
		  cerr << What << endl;
		}
	 END;
  } 



};

#endif

