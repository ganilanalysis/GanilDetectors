/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _DETMANAGER_CLASS
#define _DETMANAGER_CLASS
/**
 * @file   DetManager.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   Nov, 2012-2015
 * @todo   DetManager
 * @brief  Singleton to Handle Detectors
 * .
 */
#include "Defines.hh"
#include "BaseDetector.hh"
class BaseDetector;

class DetManager
{
private:
  // Constructor/destructor
  DetManager (){
    _isSetup = false;
	 DetList = NULL;
  }
  ~DetManager () {}

public:
  // Public Interface 
  void RegisterDetector (BaseDetector *Det,Char_t *Name) { 
	 if(!_isSetup){
		DetList = new vector<BaseDetector*>();
		DetName = new vector<string>();
		_isSetup = true;
	 }
	 DetList->push_back(Det);	 
	 DetName->push_back(string(Name));	 


#ifdef DEBUG
	 std::cout << "Adding " << Name  << " to DetManager" << Det << std::endl;    
#endif
  }
  
  BaseDetector* GetDetector(const Char_t *Name)
  {
	 BaseDetector* Ptr = NULL;
	 if(_isSetup){
		
		for(UShort_t i=0; i<DetList->size();i++)
		  {
			 if( DetName->at(i) == Name)
				{
				  Ptr = DetList->at(i);
				  		  std::cout << "Found " << Name  << " in DetManager - " << Ptr << std::endl;    
				  break;
				}
		  }
	 }
	 
	 if(!Ptr)
		{
		  Char_t Message[100];
		  sprintf(Message,"<DetManager> Could not find the detector  (%s)",Name);
		  MErr * Er = new MErr(WhoamI,0,0, Message);
		  throw Er;
		}

	 return Ptr;
  }

  void UnRegisterDetector (BaseDetector *Det) { 
	 Int_t Pos = -1;
	
	 if(_isSetup){

		for(UShort_t i=0; i<DetList->size();i++)
		  {
			 if( DetList->at(i) == Det)
				{
				  Pos = i;
				  break;
				}
		  }
		
		if(Pos>-1)
		  {
#ifdef DEBUG
			 std::cout << "Removing " << DetName->at(Pos) << " to DetManager" << std::endl;
#endif
			 DetList->erase(DetList->begin()+Pos);
			 DetName->erase(DetName->begin()+Pos);
		  }
		else
		  {
			 Char_t Message[100];
			 sprintf(Message,"<DetManager> Trying to remove a detector not in the Detector List (%p)",Det);
			 MErr * Er = new MErr(WhoamI,0,0, Message);
			 throw Er;
		  }
	 }
  }

  inline bool isSetup () { return _isSetup; }

  // Creation and destruction of singleton
  static DetManager *getInstance ()
  {
    START;
    
    if (NULL == _singleton)
      {        
        std::cout << "Creating DetManager Instance " << std::endl;
        _singleton =  new DetManager;
      }
    return _singleton;
    END;
  }

  void Print()
  {
	 START;
	 if(_isSetup)
		{
		  for(UShort_t i=0; i<DetName->size();i++)
			 cout << i << " " << DetName->at(i) <<  " (" << DetList->at(i) << ") "<< endl;
	 
		}
	 else
		{
		  Char_t Message[100];
        sprintf(Message,"<DetManager> Trying to set Print Detector List while no detector is declared in DetManager");
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
		}

	 END;
  }

  static void kill ()
  {
    START;
    if (NULL != _singleton)
      {
        delete _singleton;
        _singleton = NULL;
      }
    END;
  }

private:
  // Setup Flag, set to true if at least one detector is declared
  bool _isSetup;
  std::vector<BaseDetector*> *DetList;
  std::vector<std::string> *DetName;
  static DetManager *_singleton;
};

#endif
