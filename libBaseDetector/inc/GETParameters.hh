/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    Diego Ramos, diego.ramos@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _GETPARAMETERS_CLASS
#define _GETPARAMETERS_CLASS
/**
 * @file   GETParameters.hh
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   December, 2021
 * @version 0.1
 * @todo 
 * @brief  Class of GaniExpt GETParameters
 * 
 * 
 */

#include "Defines.hh"
#include "LogManager.hh"

class GETParameters
{
public:
  GETParameters(Int_t MaxPar //!< Max Number of GETParameters
						);
  ~GETParameters(void);

  //! 
  static GETParameters* getInstance(void);


  //! Add a parameter to the parameter list
  void Add(Int_t Cobo, Int_t Asad, Int_t Aget, Int_t Channel, Int_t Sample,string Pname="");
  //! Autmatic Add Parameter by board
  //void AddBoard (Char_t* Name, UShort_t BoardId, UShort_t NChannels, Int_t NSamples=-1);
  //! Remove a parameter from the parameter list
  void Remove(UShort_t Pid);
  //! Allocate Parameter Arrays
  void AllocateArrays(void);
  //! Set Maximum number of parameters
  void SetMaxParameters(Int_t n){MaxGETParameters = n;};
  //! Set Number of parameters
  void SetNParameters(Int_t n){NParameters = n;};
   //! Set pointers to NULL
  void SetPointers(void);
  //! Empty the parameter List
  void Clear(void);
  //! Print the parameter List
  void Print(void);
  //! Get Parameters from exptname
  void GetGETParametersFromConfigFile(const Char_t* filename);
  //! Return Parameter Name from Parameter Id
  UInt_t GetPar(string pname);
  //! Return Parameter Name from Parameter Id
  Int_t GetParId(const Int_t& Cobo, const Int_t& Asad, const Int_t& Aget, const Int_t& Channel, const Int_t& Sample=-1);
   //! Return Parameter Name from Parameter Id
  inline string GetParName(UShort_t id){return PName[id];};
  //! Return Parameter Size from Parameter Id
  inline Int_t GetMaxParameters(void){return MaxGETParameters;};
  //! Return Parameter Size from Parameter Id
  inline Int_t GetNParameters(void){return NParameters;};


protected:
  MOFile *L; /// LogFile
  //! MLaximum number of Parameters
  Int_t MaxGETParameters;
  //! Number of Parameters
  Int_t NParameters;
  //! Parameters Name Table indexed by ParId
  string *PName;
  //! Cobo id for Params
  Int_t *PCobo;
   //! Asad id for Params
  Int_t *PAsad;
   //! Aget id for Params
  Int_t *PAget;
  //! Channel Id for Params
  Int_t *PChannel;
  //! Sample Id for Params
  Int_t *PSample;
  //! Parameters Map Table indexed by Cobo, Asadn, Aget, Chan, Samples
  Int_t *****PMap;
  
  //! Max Number of GET Cobos 
  UShort_t MaxCobos;
  //! Max Number of GET Asads 
  UShort_t MaxAsads;
  //! Max Number of GET Agets 
  UShort_t MaxAgets;
  //! Max Number of GET Samples 
  UShort_t MaxSamples;
  //! Max Number of GET Channels 
  UShort_t MaxChannels;
private:
   // The static instance of the Parameters class:
   static GETParameters* instance;

};
#endif
