/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _LOGMANAGER_CLASS
#define _LOGMANAGER_CLASS
/**
 * @file   LogManager.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   June, 2012-2015
 * @todo   LogManager
 * @brief  Singleton to Handle Log File
 * .
 */
#include "Defines.hh"

class LogManager
{
private:
  // Constructor/destructor
  LogManager (){
	 START;
    _isSetup = false;
    _Log = NULL;
	 _LogFileName = "";
	 END;
  }
  ~LogManager () noexcept(false) {
	 START;
	 if(_isSetup)
		_Log = CleanDelete(_Log);
	 END;
  }

public:
  // Public Interface 
  
  void SetLogFile (string LogFileName) { 
  	 START;
	 if(_isSetup)
		{
		  Char_t Message[100];
        sprintf(Message,"Trying to open LogFile while the LogFile %s is already openned! \n",_LogFileName.c_str());
        MErr * Er = new MErr(WhoamI,0,1000, Message);
        throw Er; 
		}
	 else
		{
		  OpenFile(LogFileName);
		}
	 END;
  }

  void CloseFile()
  {
	 START;
	 _LogFileName = "";
	 _Log = CleanDelete(_Log);
	 _isSetup = false;
	 END;
  }

  bool OpenFile (string LogFileName) 
  { 
    START;
	 _LogFileName = LogFileName;
	 _Log = new MOFile(_LogFileName.c_str());
	 if(_Log)
		{
		  std::cout << "Setting LogFile in LogManager to "<< _LogFileName << std::endl;
		  _isSetup = true;
		  return 1;
		}
	 else
		{

		  _isSetup = false;
		  Char_t Message[100];
        sprintf(Message,"Could not open LogFile %s! \n",_LogFileName.c_str());
        MErr * Er = new MErr(WhoamI,0,1000, Message);
        throw Er; 
		  return 0;
		}
    END;

  }
  //! Change File
  bool ChangeFile(string LogFileName)
  {
     START;
     if(_isSetup)
       CloseFile();
     SetLogFile(LogFileName);
     END;
  } 
  MOFile* GetFilePtr (void) 
  { 
    START;
    if (isSetup()) return _Log; 
    else
      {
        Char_t Message[100];
        sprintf(Message,"LogFile is not openned  ! \n  Use LM->SetLogFile(\"LogFileName.log\") Open Log File;");
        MErr * Er = new MErr(WhoamI,0,1000, Message);
        throw Er; 
        return _Log; 
      }
    END;

  }
  inline bool isSetup () { return _isSetup; }

  // Creation and destruction of singleton
  static LogManager *getInstance ()
  {
    START;
    
    if (NULL == _singleton)
      {        
        std::cout << "Creating LogManager Instance " << std::endl;
        _singleton =  new LogManager;
      }
    return _singleton;
    END;
  }

  static void kill ()
  {
    START;
    if (NULL != _singleton)
      {
        delete _singleton;
        _singleton = NULL;
      }
    END;
  }

private:
  // Setup Flag, set to true is Path provided
  bool _isSetup;
  // Pointer to log File Name
  MOFile  *_Log;
  //! LOgfile name
  string _LogFileName;
  static LogManager *_singleton;
};

#endif
