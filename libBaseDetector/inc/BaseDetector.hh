/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _GANILDETECTOR_CLASS
#define _GANILDETECTOR_CLASS
/**
 * @file   BaseDetector.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   December, 2012
 * @todo   CreateCalHostogram
 * @brief  Abstract Base Class for Ganil Detectors
 * 
 * Abstract Base Class for Ganil Detectors.
 */

#include "Defines.hh"
#include "Parameters.hh"
#include "NUMEXOParameters.hh"
#include "GETParameters.hh"
#include "ParManager.hh"
#include "MesytecParameters.hh"

#include "Map.hh"
#include "MRandom.hh"

#include "DetManager.hh"
#include "EnvManager.hh"
#include "LogManager.hh"

class DetManager;

class BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  BaseDetector(const Char_t *Name,   //!< Detector Name
               UShort_t NDetectors,  //!< Number of detectors
               Bool_t RawData = true, //!< Has RawData
               Bool_t CalData = true, //!< Has CalData
               Bool_t DefaultCalibration = true, //!< Use Default Calibration
               Bool_t PositionInfo = true,      //!< Track Information
               const Char_t *NameExt="",        //!< Additional Name
               Bool_t Scale=false,        //!< OverAll scaling Factor in Calibration
               Bool_t HasTimeStamp=false,        //!< Has TS information
               UShort_t MaxMult=1,        //!< Max multiplicity : 1 standard Mode , Other for NUMEXO
               Bool_t HasTime=false        //!< Has Time from Numexo
                );
  
  //! Generic Destructor
  virtual ~BaseDetector(void) noexcept(false);

  //! Add sub-component
  virtual void AddComponent(BaseDetector* Cpnt);
  //! Add Counter to the counter list
  virtual void AddCounter(string);
  //! Increment Counter at Pos
  virtual void IncrementCounter(UShort_t Pos);
  //! Add Parameter to the Map
  virtual void AddParam(Parameters* Par, //!< Pointer to Parameter Table
								Map* Map, //!<  Pointer to Parameter Map
								string Pname,  //!< Parameter Name
								UShort_t Uid   //!< Internal Id
								); 
  //! Add Parameter range to the Map
  virtual void AddParRange(Parameters* Par, //!< Pointer to Parameter Table
									Map* Map,        //!< Ponter to Parameter Map
									string Pat,           //!< Pattern 
									UShort_t i_from,      //!< Beginning of range  
									UShort_t i_to,        //!< End of range
									const char *format="%s%02d" //!< Format
									);
  //! Add Data 
//  virtual void AddRawData(UShort_t uid, UShort_t val,ULong64_t TS=0, Int_t DataValid=-1, Int_t PileUp=-1, Int_t fTime = -1);
//  virtual void AddRawData(const UShort_t& uid,const UShort_t& val,const ULong64_t& TS=0,const Int_t& DataValid=-1,const Int_t& PileUp=-1,const Int_t& fTime = -1);
  //! Add Data with Float
  //  virtual void AddCalData(UShort_t uid, Float_t val){};
  //! Allocate subcomponents
  virtual void AllocateComponents(void);
  //! Allocate  and Initialize Arrays according to detector size
  virtual void AllocateArrays(void);
#ifdef WITH_ROOT
  //! Create and Add 1D histogram to Cal Histogram List
  virtual void AddHistoCal(const Char_t *ParX, //!<  Parameter X Name
									const Char_t *Name, //!<  Histogram Name
									const Char_t *Title,//!<  Histogram Title
									UShort_t NBinsX = 0,//!<  Number of bins in X 
									Float_t LLX = 9999,    //!<  Lower boundary in X 
									Float_t ULX = 9999,     //!<  Upper boundary in X 
									TDirectory *Dir = NULL //!< Directory where the histogram will be stored
									);

  //! Create and Add 2D histogram to Cal Histogram List
  virtual void AddHistoCal(const Char_t *ParX, //!<  Parameter X Name
									const Char_t *ParY, //!<  Parameter Y Name
									const Char_t *Name, //!<  Histogram Name
									const Char_t *Title,//!<  Histogram Title 
									UShort_t NBinsX = 0,//!<  Number of bins in X 
									Float_t LLX = 9999,    //!<  Lower boundary in X 
									Float_t ULX = 9999,    //!<  Upper boundary in X 
									UShort_t NBinsY = 0,//!<  Number of bins in Y 
									Float_t LLY = 9999,    //!<  Lower boundary in Y
									Float_t ULY = 9999,     //!<  Upper boundary in Y
									TDirectory *Dir = NULL //!< Directory where the histogram will be stored 
									);
  //! Create and Add 1D histogram to Raw Histogram List
  void AddHistoRaw(const Char_t *ParX, //!<  Parameter X Name
									const Char_t *Name, //!<  Histogram Name
									const Char_t *Title,//!<  Histogram Title
									UShort_t NBinsX = 0,//!<  Number of bins in X 
									Float_t LLX = 9999,    //!<  Lower boundary in X 
									Float_t ULX = 9999,     //!<  Upper boundary in X 
									TDirectory *Dir = NULL //!< Directory where the histogram will be stored 
									);

  //! Create and Add 2D histogram to Raw Histogram List
  void AddHistoRaw(const Char_t *ParX, //!<  Parameter X Name
									const Char_t *ParY, //!<  Parameter Y Name
									const Char_t *Name, //!<  Histogram Name
									const Char_t *Title,//!<  Histogram Title 
									UShort_t NBinsX = 0,//!<  Number of bins in X 
									Float_t LLX = 9999,    //!<  Lower boundary in X 
									Float_t ULX = 9999,    //!<  Upper boundary in X 
									UShort_t NBinsY = 0,//!<  Number of bins in Y 
									Float_t LLY = 9999,    //!<  Lower boundary in Y
									Float_t ULY = 9999,     //!<  Upper boundary in Y
									TDirectory *Dir = NULL //!< Directory where the histogram will be stored
									);

  //! Create Histograms
  virtual void CreateHistograms(TDirectory *Dir);
  //! Create  Histograms for Calculated Parameters
  virtual void CreateHistogramsCal(TDirectory *Dir);
  //! Create 1D Histograms for Calculated Parameters
  virtual void CreateHistogramsCal1D(TDirectory *Dir);
  //! Create 2D Histograms for Calculated Parameters
  virtual void CreateHistogramsCal2D(TDirectory *Dir);
  //! Create Histograms for Raw Parameters
  virtual void CreateHistogramsRaw(TDirectory *Dir);
  //! Create 1D Histograms for Raw Parameters
  virtual void CreateHistogramsRaw1D(TDirectory *Dir);
  //! Create 2D Histograms for Raw Parameters
  virtual void CreateHistogramsRaw2D(TDirectory *Dir);
#endif
  //! Set Internal Counters
  virtual void AllocateCounters(void);
  //! Calibrate
  virtual void Calibrate(void);
  //! Calibrate Charges (Supression of pedestals)
  virtual void CalibrateCharges(void);
  //! Return true if the track is in active area of detector DetNr
  virtual Bool_t CheckTrack(Float_t Xf, //<! X Focal Plane
                            Float_t Tf, //<! Theta Focal Plane
                            Float_t Yf, //<! Y Focal Plane
                            Float_t Pf, //<! Phi Focal Plane
                            Float_t zFP,//<! Focal Plane Z position
                            UShort_t DetNr//<! Detector Number
                            );
  //! Clean on destruction 
  virtual void Clean(void);
  //! Clear the class at the end of each event
  virtual void Clear(void);
  //! Clear raw data from the class
  virtual void ClearRaw(void);
  //! Clear calibrated data from the class
  virtual void ClearCal(void);
  //! Check Calibration File
  virtual MIFile* CheckCalibration(Char_t *fName);
  //! Return true if line is comment line (starting with //)
  Char_t* CheckComment(Char_t *Line);
  //! DeAllocate Arrays according to detector number of channels
  virtual void DeAllocateArrays(void);
#ifdef WITH_ROOT
  void DeleteHistograms(void);   //!< DeAllocate Histograms
  void DeleteRawHistograms(void);   //!< DeAllocate Raw Histograms
  void DeleteCalHistograms(void);   //!< DeAllocate Histograms
#endif
  //! DeAllocate Internal Counters
  virtual void DeAllocateCounters(void);
#ifdef WITH_ROOT
  //! Empty Histograms
  void EmptyHistograms(void);
  //! Fill Histograms
  void FillHistograms(void);
  //! Fill Histograms Raw
  virtual void FillHistogramsRaw(void);
  //! Fill Histograms Cal
  virtual void FillHistogramsCal(void);
#endif
  //! Generate Default Calibration File
  virtual void GenerateDefaultCalibration(Char_t *fName);
  //! Generate Default Gates
  virtual void GenerateDefaultGates(Char_t *fName);
  // Return position of comment sign (//) in the line
  Char_t* GetCommentPtr(Char_t *Line);

  // Return Get Coefficients
  template<typename T> Bool_t GetCoeffs(Char_t *Line, //!< Line to be parsed
													 T *Coeffs,    //!< Array of where coefficients are strored
													 UShort_t NC   //!< Coefficients Order
													 );  
  //! Return Internal Calibrated Parameter number from its Name
  inline Char_t* GetCalName(UShort_t Pos){return CalNameI[Pos];};
  //! Return Internal Calibrated Parameter number from its Name
  UShort_t GetCalPar(const Char_t *ParName);
  //! Return Pointer to Subdetector at Pos
  BaseDetector* GetSubDet(UShort_t Pos);
  //! Return Pointer of Detector by Name
  BaseDetector* GetDetPtr(const Char_t *Name);
  //! Return Internal Raw Parameter number from its Name
  UShort_t GetRawPar(const Char_t *ParName);
  //! Returns the two dimensional Array of calibration coefficients.x
  Float_t** GetCalCoeffs(void){return Coeffs;};
  //! Get Raw Data Multiplicity
  inline Int_t GetRawM(void){return RawM;};
  //! Get TS Data Multiplicity
  inline Int_t GetRawTSM(void){return RawM;};
  //! Get Calibrated Data Multiplicity
  inline ULong64_t GetNumberRawDuplicatedLabels(void){return RawDuplicateLabel;};
  //! Returns the Raw  Value at the position Pos.
  UShort_t GetRawAt(UShort_t Pos);
  //! Returns the Raw TS Value at the position Pos.
  ULong64_t GetRawTSAt(UShort_t Pos);
  //! Returns the Raw Time Value at the position Pos.
  UShort_t GetRawTimeAt(UShort_t Pos);
  //! Returns the Calibrated Value at the position Pos.
  Float_t GetCalAt(UShort_t Pos);
  //! Return the Calibrated Value at of the subdet p.
  inline Float_t GetCal(UShort_t Pos){return Cal[Pos];};
  //! Get Cal Data Multiplicity
  inline Int_t GetCalM(void){return CalM;};
  //! Returns the Raw TS Value at the position Pos.
  ULong64_t GetCalTSAt(UShort_t Pos);
  //! Return the Cal TS Value of the subdet p.
  inline ULong64_t GetCalTS(UShort_t Pos){return TimeStampsCal[Pos];};
  //! Return the Raw Value of the subdet p.
  inline UShort_t GetRaw(UShort_t Pos){return Raw[Pos];};
  //! Return the Raw TS Value of the subdet p.
  inline ULong64_t GetRawTS(UShort_t Pos){return TimeStamps[Pos];};
  //! Return the Raw Time Value of the subdet p.
  inline UShort_t GetRawTime(UShort_t Pos){return Time[Pos];};
  //! Return the RawName  of the subdet p.
  inline Char_t* GetRawName(UShort_t Pos){return RawNameI[Pos];};


  //  Specific Methods for MultiHit
  //! Return the Raw MultiHit Multiplicty for det p
  inline UShort_t GetMHRawM(UShort_t Pos){return RawNM[Pos];};
  //! Return the Raw Value of the subdet p with MultiHit Handling.
  inline UShort_t GetMHRaw(UShort_t Pos,UShort_t Mult){return RawN[Mult][Pos];};
  //! Return the Raw Value of the subdet p with MultiHit Handling.
  inline UShort_t GetMHRawTS(UShort_t Pos,UShort_t Mult){return TimeStampsN[Mult][Pos];};


#ifdef WITH_ROOT
  string GetCurrentHistogramFolder(void);
  //! Get Main Histogram Folder Name
  inline Char_t* GetMainHistogramFolderName(void){return MainHistogramFolder;};
#endif
  //! Get Detector Name
  inline Char_t* GetName(void){return DetectorName;};
  //! Get Detector Name Extension
  inline Char_t* GetNameExt(void){return DetectorNameExt;};
  //! Get the number of channels in the detector
  inline UShort_t GetNumberOfDetectors(void){return NumberOfDetectors;};
  //! Get the number of channels in the detector
  inline UShort_t GetNumberOfSubDetectors(void){return NumberOfSubDetectors;};
  //! Returns the Raw Value at the position Pos. 
  UShort_t GetRawNrAt(UShort_t Pos);
  //! Returns the Calibrated Value at the position Pos. 
  UShort_t GetNrAt(UShort_t Pos);
  //! Get Calibrated Data Multiplicity
  inline Int_t GetM(void){return CalM;};
  //! Get X Reference Position
  inline Double_t GetActiveArea(Int_t DetNr, Int_t Val){return ActiveArea[DetNr][Val];};
  //!Set Reference positiosn
  void SetRefPos(Float_t X, Float_t Y, Float_t Z)
  {
      RefPos[0] =X;
      RefPos[1] =Y;
      RefPos[2] =Z;
  }
  //! Get X Reference Position
  inline Float_t GetRefX(void){return RefPos[0];};
  //! Get X Reference Position
  inline Float_t GetRefY(void){return RefPos[1];};
  //! Get Z Reference Position
  inline Float_t GetRefZ(void){return RefPos[2];};
  //! Get X Reference Position
  inline Verbose_t GetVerbose(void){return VerboseLevel;};

  //! Return if the Detector has Calibrated Data 
  inline Bool_t HasCalData(void){return fCalData;}; 
  //! Return if the Detector has Raw Data 
  inline Bool_t HasRawData(void){return fRawData;}; 
  //! Return if the Detector has TS
  inline Bool_t HasTS(void){return fHasTimeStamp;};
  //! Return if the Detector has Time
  inline Bool_t HasTime(void){return fHasTime;};
  //! Generate Default Gates
  virtual void InitializeCalibration(void);
  //! Initialize Gates
  // virtual void InitializeGates(void);
  //! Check that the value is with gate (inclusive bounds)
//  Bool_t IsInGate(UShort_t Value, UShort_t Nr);
  Bool_t IsInGate(const UShort_t& Value, const UShort_t& Nr);
  //! Check that the value is with gate (inclusive bounds)
  Bool_t IsInGate(Float_t	Value, UShort_t Nr);
  //! Get Presence Status of the  detector
  inline Bool_t IsPresent(void){return isPresent;};
  //! Return validaton of the Detector 
  inline Bool_t IsValidTrack(void){return hasValidTrack;}; 
  //! Print Internal Counters
  virtual void PrintCounters(void);
  virtual void PrintCal(void);   //!< Print Calibrated Data
  virtual void PrintRaw(void);   //!< Print Raw Data

  template<typename T> void PrintDefaultCoeffs(ostream *O,       //!< Stream where to print
															  T **Coeffs,       //!< Array of Coefficients to be printed
															  Char_t **Comment, //!< Array of Comment written at the end of the line
															  UShort_t NC     //!< Coefficients Order
															  );
  void PrintOptions(ostream  &O); //!< Print Input/OutPut Options to stream O

  //! Read Calibration For Reinplementation in Daughter Class
  virtual void ReadCalibration(void){};
  //! Read Calibration 
  virtual void ReadCalibration(MIFile *IF);
  //! Read Calibrations Block from file, set the coeffients up to order NC to Array 
  template<typename T> void ReadCalibrationBlock(MIFile *IF, //!< Pointer to imput file
																 T **Array,  //!< Array of where coefficients are strored
																 UShort_t NC //!< Coefficients Order
																 );
  //! Read Default Calibration File
  virtual void ReadDefaultCalibration(void);
  //! Read Gates For Reinplementation in Daughter Class
  virtual void ReadGates(void){};
  //! Read Raw Gates
  virtual void ReadGates(MIFile *IF);
  //! Read Default Cal Gates
  virtual void ReadCalGates(void);
  //! Read Cal Gates
  virtual void ReadCalGates(MIFile *IF);
  //! Read Geometry Default Position Information of the detector : so far active area for all channels
  virtual void ReadDefaultActiveArea(void);
  //! Read  For Reinplementation in Daughter Class
  virtual void ReadPosition(void){};
 //! Read  For Reinplementation in Daughter Class
  virtual void ReadDefaultPosition(MIFile *IF);
  //! Read Geometry information
  virtual void ReadActiveArea(MIFile *IF);
  //! Register Parameters to ParManager
  virtual void RegisterParameters(ParManager *ParMan);
#ifdef WITH_ROOT
  //! Save Histograms
  void SaveHistograms(void);
#endif
  //! Set the Calibrated parameter name
  void SetCalName(const Char_t *Name, UShort_t Pos);
  //! Set the Calibrated parameter name using name and pattern
  void SetCalName(const Char_t *Format);
  //! Set Calibrated data  with Short Data
  void SetCalData(UShort_t Nr, Float_t Caldata, ULong64_t TS = 0);
  //! Set Calibrated gates value for all parameters
  void SetGateCal(Float_t ll, Float_t ul);
  //! Set Calibrated gates value for a single parameter
  void SetGateCal(Float_t ll, Float_t ul, UShort_t Nr);
  //! UnSet Cal gates
  inline void UnSetGateCal(void){fCalGate = false;} ;
  //! Set Raw gate value for all parameters
  void SetGateRaw(UShort_t ll, UShort_t ul=65535);
  //! Set Raw gate value for single parameter
  void SetGateRaw(UShort_t ll, UShort_t ul, UShort_t Nr);
  //! UnSet Raw gates
  inline void UnSetGateRaw(void){fRawGate = false;} ;
  //! Set the name of the detector
  inline void SetName(const Char_t* name){sprintf(DetectorName,"%s",name);};
  //! Set the number of detector
  inline void SetNChans(UShort_t N){NumberOfDetectors = N;}; 
  //! Set CalCharges Flag
  inline void HasCalCharges(Bool_t v){fHasCalCharges = v;}; 
  //! Unset All Outputs
  void SetNoOutput(void); 
  //! Set pointers to NULL at startSup
  virtual void SetPointers(void);
  //! Set the parameter patterns associated with detector
  virtual void SetParameters(Parameters* Par,Map* Map);
  //! Set the parameter patterns associated with detector
  virtual void SetParameters(NUMEXOParameters* Par,Map* Map);
  //! Set the parameter patterns associated with detector
  virtual void SetParameters(GETParameters* Par,Map* Map);
  //! Set the parameter patterns associated with detector
  virtual void SetParameters(MesytecParameters* Par,Map* Map);
  //! Set the parameter patterns associated with detector
  virtual void SetParametersNUMEXO(NUMEXOParameters* Par,Map* Map);
  //! Set the parameter patterns associated with detector
  virtual void SetParametersGET(GETParameters* Par,Map* Map);
  //! Set the parameter patterns associated with detector
  virtual void SetParametersMesytec(MesytecParameters* Par,Map* Map);
   //! Set the raw parameter name
  void SetParameterName(const Char_t *Name, UShort_t Pos);
  //! Set the raw parameter name using name and pattern
  void SetParameterName(const Char_t *Format);
   //! Set the raw parameter name for Raw Tree
  void SetParameterNameInRawTree(const Char_t *Name, UShort_t Pos);
  //! Set Active area of a detector
  void SetActiveArea(Float_t xmin, //!< Horizontal XMin
                             Float_t ymin, //!< Horizontal XMax 
                             Float_t xmax, //!< Vertical YMin
                             Float_t ymax, //!< Vertical YMax
                             Float_t z,    //!< Distance to the target
                             UShort_t Nr   //!< Detetector Number
                             );
  //! Set Raw data  with Short Data
//  virtual void SetRawData(UShort_t Nr, UShort_t Rawdata, ULong64_t TS=0, Int_t DataValid=-1, Int_t PileUp=-1,Int_t Time=0);
  void AddRawData(const UShort_t& Nr, const UShort_t& Rawdata, const ULong64_t& TS=0, const Int_t& DataValid=-1, const Int_t& PileUp=-1, const UShort_t &Time=0);
  //! Set isPresent Flag
  inline void SetPresent(Bool_t v){isPresent=v;};
  //! Set Verbose Level
  void SetVerbose(Verbose_t Level=V_OFF){VerboseLevel = Level;};
  //! Reset The data, counters, histos members of the Class
  virtual void Reset(void);
  //! Print Internal Counters
  virtual void ResetCounters(void);
  //! Treat the data
  virtual Bool_t Treat(void);
  //! Update Internal Counter
  virtual void UpdateCounters(void);
  // //! Update the Calibrated Value at the position Pos. 
  // virtual void UpdateCalDataAt(UShort_t Pos, Float_t Caldata);
  
 
  //! Set Raw Histogram parameters (Number of bins, lower and upper bounds)  
  void SetRawHistogramsParams(UShort_t NBins, Float_t LL, Float_t UL, string Folder="Raw");
  //! Set Calibrated Histogram parameters (Number of bins, lower and upper bounds)  
  void SetCalHistogramsParams(UShort_t NBins, Float_t LL, Float_t UL, string Unit="", string Folder="Cal");

 //! Set Flags for input Branch attachment
  void SetInAttach(void);
  //! Set Variable array Raw Data input branch from ROOT File
  void SetInAttachRawV(Bool_t v = false);
  //! Set Individual Parameter input branch from ROOT File
  void SetInAttachRawI(Bool_t v = false);
  //! Set Fixed array Raw Data input branch from ROOT File
  void SetInAttachCalF(Bool_t v = false);
  //! Set Variable array Raw Data input branch from ROOT File
  void SetInAttachCalV(Bool_t v = false);
  //! Set Individual Parameter input branch from ROOT File
  void SetInAttachCalI(Bool_t v = false);
  //! Set Flags for Branch attachment
  void SetOutAttach(void);
  //! Set Fixed array Calibrated Data attachement to ROOT File
  void SetOutAttachCalF(Bool_t v = false);
  //! Set Variable array Calibrated Data attachement to ROOT File
  void SetOutAttachCalV(Bool_t v = false);
  //! Set Individual Calibrated Data attachement to ROOT File
  void SetOutAttachCalI(Bool_t v = false);
  //! Set Individual Raw  Data attachement to ROOT File
  void SetOutAttachRawI(Bool_t v = false);
  //! Set Fixed Size Raw  Data attachement to ROOT File
  void SetOutAttachRawF(Bool_t v = false);
  //! Set Variable array of Raw Data attachement to ROOT File
  void SetOutAttachRawV(Bool_t v = false);
  //! Set Flags for Branch attachment
  void SetOutAttachTS(Bool_t v = false);
  //! Set Flags for Branch attachment
  void SetOutAttachCalTS(Bool_t v = false);
  //! Set Flags for Branch attachment
  void SetOutAttachTime(Bool_t v = false);
  //! Set Flags for Branch attachment Of Multplicityies
  void SetOutAttachRawMult(Bool_t v = false);

  //! Set Histogram 
  void SetHistograms(void);
  //! Set Histogram Cal
  void SetHistogramsCal(Bool_t v = false);
  //! Set Histogram Cal 1D
  void SetHistogramsCal1D(Bool_t v = false);
  //! Set Histogram Cal 2D
  void SetHistogramsCal2D(Bool_t v = false);
  //! Set Histogram Cal Summary
  void SetHistogramsCalSummary(Bool_t v = false);
  //! Set Histogram Raw
  void SetHistogramsRaw(Bool_t v = false);
  //! Set Histogram Raw 1D
  void SetHistogramsRaw1D(Bool_t v = false);
  //! Set Histogram Raw 2D
  void SetHistogramsRaw2D(Bool_t v = false);
  //! Set Histogram Raw Summary
  void SetHistogramsRawSummary(Bool_t v = false);
  //! Set The Analysis Mode 
  void SetAnalysisMode(Short_t Mode);
  //! Set Main Histogram Folder
  void SetMainHistogramFolder(const Char_t *name);
  //! Set HistogramFolder for Raw 1d
  void SetHistogramsRaw1DFolder(const Char_t *name);
  //! Set HistogramFolder for Raw 2d
  void SetHistogramsRaw2DFolder(const Char_t *name);
  //! Set HistogramFolder for Cal 1d
  void SetHistogramsCal1DFolder(const Char_t *name);
  //! Set HistogramFolder for Cal 2d
  void SetHistogramsCal2DFolder(const Char_t *name);

#ifdef WITH_ROOT
  //! Attach Branches from Root TTree
  virtual void InAttach(TTree *inTree);
  //! Attach Calibrated Branches from Cal Root TTree
  virtual void InAttachCal(TTree *inTree);
  //! Attach Branches to Root TTree
  virtual void OutAttach(TTree *OutTree);
  //! Attach Branches to root TTree recursively (Used for GW)
  virtual void AddBranches(TTree *OutTree);
  // Set HistoManager to subdetector 
  void SetHistoManager(HistoManager *HM);
  // Set Option for Input/Output for Main detector 
  virtual void SetIOOptions(TFile *OutTFile, const Char_t * FName, HistoManager *HM, TTree *OutTTree, TTree *InTTree);
  // Set default options  Input/Output according to fMode 
  virtual void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif

protected:
  
  //! Log Manager
  LogManager *LM;
  //! Environment Manager
  EnvManager *EM;
  //! Detector Name
  Char_t DetectorName[100];
  //! Name Extension of the detector 
  Char_t DetectorNameExt[100];
  //! Number of channels
  UShort_t NumberOfDetectors;
  //! Number of Subdetector 
  UShort_t NumberOfSubDetectors;

  //! Analysis Mode 
  Short_t fMode;
  //! Special Calibration Mode 
  Short_t CalibrationMode;

  Double_t RefPos[3]; //<! Reference position wrt Target
  Double_t **ActiveArea; //<! Array of Active Area Boundaries of Detector
  Bool_t fActiveArea;  //!< True if the active area of detector have been provided

  //! Pointer to log file
  MOFile *L;
  //! Pointer to Random
  MRandom *Rnd;
  //! true if this detector has overall scaling calibration coeff
  Bool_t fHasScalingCalib;
  //! true if this detector has specific charge calibration coeff (pedestal substration)
  Bool_t fHasCalCharges;

  //! Boolean Flag for Detector presence  
  Bool_t isPresent;
   //! Boolean Flag for Detector validation 
  Bool_t isValid;
  //! Counters vector
  vector<ULong_t> *Ctr;
  //! Counters labels
  vector<string> *CtrLbl;
  
  //! true if Raw parameter names are defined
  Bool_t fRawName;

  //! true the current track is consistent with active area of the detector
  Bool_t hasValidTrack;

  //! Fixed Size Raw Data Array
  UShort_t *Raw;
  //! Raw Data Multiplicity
  Int_t RawM;
  //! Variable Size Raw Data Array
  UShort_t *RawV;
  //! Variable Size Array of Raw Detector Numbers (hit Pattern)
  UShort_t *RawNr;
  //! Fixed size array of raw parameters name
  Char_t **RawNameI;
  //! Fixed size array of raw parameters name
  Char_t **RawTreeNameI;
  //! Variable Size Raw Gates Array
  UShort_t **RawGates;
  //!Duplicate Raw Labels
  ULong64_t RawDuplicateLabel;

  //! Fixed Size Raw Data Array
  UShort_t **RawN;
  //! Fixed Size Raw Pileup  Array
  UShort_t **PileUpN;
  //! Raw Data Multiplicity
  Int_t *RawNM;
  //! Variable Size Raw Data Array
  UShort_t **RawNV;
  //! Variable Size Array of Raw Detector Numbers (hit Pattern)
  UShort_t **RawNNr;

  //! Calibrated Data Multiplicity
  Int_t CalM;
  //! Fixed Size Calibrated Data Array
  Float_t *Cal;
  //! Variable Size Calibrated Data Array
  Float_t *CalV;
  //! Variable Size Array of Calibrated Detector Numbers (hit Pattern)
  UShort_t *CalNr;
  //! Fixed Size Caldata TimeStamps Array using index of Caldata.
  ULong64_t *TimeStampsCal;
  //! Variable Size Caldata  Timestamps Data Array
  ULong64_t *TimeStampsCalV;
  //! Variable Size Calibrated data Gates Array
  Float_t **CalGates;
  //! Names of calibrated individual elements
  Char_t **CalNameI;

  //! Fixed Size Cal Data Array
  Float_t **CalN;
  //! Cal Data Multiplicity
  Int_t *CalNM;
  //! Variable Size Cal Data Array
  Float_t **CalNV;
  //! Variable Size Array of Cal Detector Numbers (hit Pattern)
  UShort_t **CalNNr;


  //! Calibration Coefficient Array
  Float_t **Coeffs;

  //! Fixed Size TimeStamps Array
  ULong64_t *TimeStamps;
  //! Timestamps Multiplicity
  Int_t TimeStampsM;
  //! Variable Size Timestamps Data Array
  ULong64_t *TimeStampsV;
  //! Variable Size Array of TimeStamps Detector Numbers (hit Pattern)
  UShort_t *TimeStampsNr;




  //! Fixed Size Mult Data Array
  ULong64_t **TimeStampsN;
  //! Cal Data Multiplicity
  Int_t *TimeStampsNM;
  //! Variable Size Cal Data Array
  ULong64_t **TimeStampsNV;
  //! Variable Size Array of Cal Detector Numbers (hit Pattern)
  UShort_t **TimeStampsNNr;



  //! Fixed Size Time Array
  UShort_t *Time;
  //! Time Multiplicity
  Int_t TimeM;
  //! Variable Size Time Data Array
  UShort_t *TimeV;
  //! Variable Size Array of Time Detector Numbers (hit Pattern)
  UShort_t *TimeNr;

  //! Fixed Size Cal Data Array
  UShort_t **TimeN;
  //! Cal Data Multiplicity
  Int_t *TimeNM;
  //! Variable Size Cal Data Array
  UShort_t **TimeNV;
  //! Variable Size Array of Cal Detector Numbers (hit Pattern)
  UShort_t **TimeNNr;

  Bool_t fCalData;   //!< true if Cal data is required
  Bool_t fCalGate;   //!< true if Rawgates are defined
  Bool_t fPosition;  //!< True if the position of detector has been provided
  Bool_t fPositionSubDetectors;  //!< true if  the sub-detector require a position information
  Bool_t fPosInfo;//!< True if the detector require a position information
  Bool_t fRawData;   //!< true if Raw data is required
  Bool_t fRawDataSubDetectors;  //!< true if Raw data is required for subdetectors
  Bool_t fRawGate;   //!< true if Rawgates are defined
  Bool_t fHasTimeStamp;   //!< true if timestamps are used
  Bool_t fHasTime;   //!< true if timestamps are used
  UShort_t fMaxMult;   //!< MaxMultiplicty to be handled
  UShort_t fMaxMultSubDetectors;  //!< MaxMultiplicty to be handledfor subdetectors


  // Histograms Handling
  string   UnitCal1D; //!< Unit to be displayed
  string   SubFolderHistCal1D; //!< SubFolder for Calibrated 1D Spectra
  string   SubFolderHistCal2D; //!< SubFolder for Calibrated 2D Spectra
  UShort_t NBinsCal1D; //!< Numbers of bins of 1d Calibrated histograms
  Float_t LLCal1D;    //!< Lower bound of 1d Calibrated histograms
  Float_t ULCal1D;    //!< Upper bound of 1d Calibrated histograms
  string   SubFolderHistRaw1D; //!< SubFolder Name for Raw 1D Spectra
  string   SubFolderHistRaw2D; //!< SubFolder Name for Raw 2D Spectra
  UShort_t NBinsRaw1D; //!< Numbers of bins of 1d Raw histograms
  UShort_t LLRaw1D;    //!< Lower bound of 1d Raw histograms
  UShort_t ULRaw1D;    //!< Upper bound of 1d Raw histograms

  Char_t MainHistogramFolder[500]; //! Histogram Folder Name
#ifdef WITH_ROOT
  HistoManager *H; //!Pointer to Hitograms storage manager;
// Raw Histograms
  Bool_t fHistogramsRaw1D; //! true if required
  Bool_t fHistogramsRaw2D; //! true if required
  Bool_t fHistogramsRawS; //!  true if creating Summary histogram of Raw Data
  Bool_t fHistogramsRawHP; //!  true if creating HitPattern histogram of Raw Data
#endif
  Bool_t fHistogramsRaw1DCreated; //! true if Raw 1D Histograms are created
  Bool_t fHistogramsRaw2DCreated; //! true if Raw 2D Histograms are created
  Bool_t fHistogramsRawSCreated; //! true if Raw Summary 2D Histograms are created
  Bool_t fHistogramsRawHPCreated; //!  true if creating HitPattern histogram of Raw Data
  Bool_t fHistogramsRawHMultreated; //!  true if creating Multiplicity histogram of Raw Data


  UShort_t HistOffsetRaw1D;  //! Histogram global offset of Raw 1D of Histo Manager
  UShort_t HistOffsetRaw2D;  //! Histogram global offset of Raw 2D of Histo Manager

  Short_t HistRawSId;       //! Histogram offset of Raw 1D Summary Histogram
  Short_t HistRawPCId;       //! Histogram offset of Raw 1D Summary Histogram
  Short_t HistRaw1DGMultId;       //! Histogram offset of Raw 1D Global Multiplicty Histogram
  Short_t HistRaw2DGMultVsNrId;       //! Histogram offset of Raw 1D Multiplicty vs Chan Histogram

  UShort_t NHistograms1DRaw; //! Number of 1d Histogram of raw data
  UShort_t NHistograms2DRaw; //! Number of 2d Histogram of raw data 

  // Calibrated Histograms
#ifdef WITH_ROOT
  Bool_t fHistogramsCal1D; //! true if required
  Bool_t fHistogramsCal2D; //! true if required
  Bool_t fHistogramsCalS; //! true if creating Summary histogram of Calibrated Data
#endif
  Bool_t fHistogramsCal1DCreated; //! true if Calibrated 1D Histograms are created
  Bool_t fHistogramsCal2DCreated; //! true if Calibrated 2D Histograms are created
  Bool_t fHistogramsCalSCreated; //! true if Calibrated Summary Histograms are created
  UShort_t HistOffsetCal1D;  //! Histogram global offset of Calibrated 1D of Histo Manager 
  UShort_t HistOffsetCal2D;  //! Histogram global offset of Calibrated 2D of Histo Manager 
  UShort_t NHistograms1DCal; //! Number of 1d Histogram of calibrated data 
  UShort_t NHistograms2DCal; //! Number of 2d Histogram of calibrated data 

  //! VerboseLevel : 0: Print_CRITICAL, 2: Print_WARNING, 4:  Print_INFO, 8 : ? 16  LOG_DEBUG
  Verbose_t VerboseLevel;

  Char_t OutRawFName[200]; //! Out Raw Parameter Name of Fixed size array
  Char_t OutCalFName[200]; //! Out Calibrated Parameter Name of Fixed size array
  Char_t OutCalVName[200]; //! Out Calibrated Parameter Name of Variable size array
  
  //! Boolean Flag if Detector as sub-components
  Bool_t isComposite;
 
  //! Component List of GanilDector
  vector<BaseDetector*> *DetList;
  
  Float_t *XMin; //!< Horizontal XMin Extension with respect to central trajectory
  Float_t *XMax; //!< Horizontal XMax Extension with respect to central trajectory
  Float_t *YMin; //!< Vertical YMin Extension with respect to central trajectory
  Float_t *YMax; //!< Vertical YMax Extension with respect to central trajectory
  Float_t *Z; //!< Z position with respect to target


#ifdef WITH_ROOT
  //! Boolean Flag true if attaching input branch from Variable Raw Data array
  Bool_t fInAttachRawV;
  //! Boolean Flag true if attaching input branch for Individual Parameters 
  Bool_t fInAttachRawI;
  //! Boolean Flag true if attaching input branch from Variable Cal Data array
  Bool_t fInAttachCalV;
  //! Boolean Flag true if attaching input branch for Fixed Size Array Cal Data Array 
  Bool_t fInAttachCalF;
  //! Boolean Flag true if attaching input branch for Individual Parameters 
  Bool_t fInAttachCalI;
  //! Boolean Flag true if attaching Fixed array to Tree
  Bool_t fOutAttachCalF;
  //! Boolean Flag true if attaching Variable Calibrated Data array to Tree
  Bool_t fOutAttachCalV;
  //! Boolean Flag true if attaching Individual Calibrated Data array to Tree 
  Bool_t fOutAttachCalI;
  //! Boolean Flag true if attaching Individual Calibrated Data array to Tree
  Bool_t fOutAttachRawI;
  //! Boolean Flag true if attaching Fixed array Raw Data array to Tree
  Bool_t fOutAttachRawF;
  //! Boolean Flag true if attaching Individual Calibrated Data array to Tree 
  Bool_t fOutAttachRawV;
  //! Boolean Flag true if attaching Individual Calibrated Data array to Tree
  Bool_t fOutAttachTS;
  //! Boolean Flag true if attaching TS to Calibrated Data array to Tree
  Bool_t fOutAttachCalTS;
  //! Boolean Flag true if attaching Individual Calibrated Data array to Tree
  Bool_t fOutAttachTime;
  //! Boolean Flag true if attaching Individual Calibrated Data array to Tree
  Bool_t fOutAttachRawMult;

  //**************************************************************************//
  // 1D Histograms 
  //**************************************************************************//
  vector<TH1*> *HCal1D; //! List of Cal 1D Histograms
  vector<vector <TH1*> > *HCalMap1D;   //! 1D Cal Histograms Parameter Map  
  vector<TH1*> *HRaw1D; //! List of Raw 1D Histograms
  vector<vector <TH1*> > *HRawMap1D;   //! 1D Raw Histograms Parameter Map  
  

  //**************************************************************************//
  // 2D Histograms 
  //**************************************************************************//
  vector<TH1*> *HCal2D; //! List of Cal 2D Histograms
  vector< vector <std::pair<UShort_t,TH1*> > > *HCalMap2D; //! 2D Histograms Parameters Map
  vector<TH1*> *HRaw2D; //! List of Raw 2D Histograms
  vector< vector <std::pair<UShort_t,TH1*> > > *HRawMap2D; //! 2D Histograms Parameters Map

#endif
};
#endif
