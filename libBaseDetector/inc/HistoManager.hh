/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifdef WITH_ROOT
#ifndef _HISTOMANAGER_CLASS
#define _HISTOMANAGER_CLASS
#include "Defines.hh"
#include "LogManager.hh"
class HistoManager
{

private:
  //! Vector of Histporams pointers
  vector <TH1 *> Histograms;
  //! Pointer to Vector of Histograms  pointers
  vector <TH1 *> * H;
  //! Vector of Folder Names
  vector <string> Folders;
  //! Pointer to Vector of Folder names 
  vector <string> * F;
  // Pointer to Log File
  MOFile *L;

  // virtual void Fill(UShort_t V, UShort_t Pos);
  // virtual void Fill(Float_t V, UShort_t Pos);
  // virtual void Fill(UShort_t V1, UShort_t V2, UShort_t Pos);
  // virtual void Fill(Float_t V1, Float_t V2, UShort_t Pos);
 
public:
  //! Constructor
  HistoManager(void);
  //! Destructor
  virtual ~HistoManager(void)  noexcept(false);
  //! Add Histogram to the Histogram Manager List
  virtual void Add(TH1* V,             //!< Histogram
						 string FolderName  //!< Folder Name
						 );
  //! Clear Histogram in the  Histogram Manager List
  virtual void Clear(UShort_t Pos);
   //! Get Histogram by name
  virtual TH1 * FindHistogram(Char_t *Name, Bool_t Print=false);
   //! Get Histogram at Position Pos
  virtual TH1 * GetHistogram(UShort_t Pos);
  //! Get Folder name et Position Pos
  virtual string GetFolder(UShort_t Pos);
  //! Get Histogram Manager List Size
  virtual Int_t GetSize(){return H->size();};
  //! Print All Histograms
  virtual void PrintAll();
};
#endif
#endif
