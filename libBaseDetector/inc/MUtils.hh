/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef UTILS_FUNC
#define UTILS_FUNC

#define PRINT_TO_STREAM(stream,variable) (stream) << setw(20)  <<#variable" : "<< setw(5) <<(variable) 

inline std::string myreplace(std::string &s,
                      std::string toReplace,
                      std::string replaceWith)
{
  return(s.replace(s.find(toReplace), toReplace.length(), replaceWith));
}

inline Bool_t IsSpace(Char_t *c)
{
  if(((Int_t)c[0]) == 32) 
	 {
		return true;
	 }
  else
	 {
		return false;
	 } 
}

inline Bool_t IsTab(Char_t *c)
{
  if(((Int_t)c[0]) == 9) 
	 {
		return true;
	 }
  else
	 {
		return false;
	 } 
}

inline Bool_t IsWhiteSpace(Char_t *c) // Space or Tab
{
  if( IsSpace(c) || IsTab(c) )
	 return true;
  else
	 return false;
}

//! Conversion of any Number to float with formating option
template <typename T>
inline string to_string(T Number, Int_t LZero = 0, Int_t FPrecision = 5){
  // C++11 will have to_string() function
  
  stringstream convert;
  convert << fixed ;
  if(LZero > 0) // Fill With zero 
	 convert << setw(LZero) << setfill('0') ;
 
  convert << setprecision(FPrecision) ;
  convert << Number;      // insert the textual representation of 'Number' in the characters in the stream
  return convert.str();
}

// Check Space in char chain
inline bool HasSpace(Char_t *str)
{  
  char c;
  int i=0;
  while (str[i])
    {
      c=str[i];
      if (isspace(c)) 
        return true;
      i++;
    }
  return false;
}	

// ! Verbosity Enum type 
// Off:	Output no tracing and debugging messages.
//	Error	Output error-handling messages.
// Warning	Output warnings and error-handling messages.
//	Info	Output informational messages, warnings, and error-handling messages.
//	Debug	Output all debugging and tracing messages.
enum Verbose_t {V_OFF, V_ERROR, V_WARNING, V_INFO, V_DEBUG};


#endif
