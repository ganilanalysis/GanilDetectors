/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _GANILMAP_CLASS
#define _GANILMAP_CLASS
/**
 * @file   Map.hh
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   January, 2013
 * @version 0.1
 * @todo 
 * @brief  Map class 
 * 
 * Ganil Map using Fixed Size Array of pointers
 * BaseDetector objects (pointer) in the code
 */

#include "Defines.hh"
#ifdef SWAP_DC
#include "MDataOp.hh"
#endif

class BaseDetector;
class Parameters;

class Map
{
public:
  Map(UShort_t MaxParameters,Int_t MaxNUMEXOParameters=-1, Int_t MaxGETParameters=-1,Int_t MaxMesytecParameters=-1);
  ~Map(void) noexcept(false);

  //! Clean before destruction
  void Clean(void);
  //! Clearing the Map
  void Clear(void);
  //! Insert an object in the Map
  void Add(UShort_t key, //!< Parameter id
			  BaseDetector* ptr, //!< POinter to Ganil Detector
			  UShort_t uid //!< Internal detector number
			  );
  //! Insert an object in the NUMEXO Map
  void AddNUMEXO(Int_t key, //!< Parameter id
		 BaseDetector* ptr, //!< POinter to Ganil Detector
		 UShort_t uid //!< Internal detector number
		 );
  //! Insert an object in the GET Map
  void AddGET(Int_t key, //!< Parameter id
	      BaseDetector* ptr, //!< POinter to Ganil Detector
	      UShort_t uid //!< Internal detector number
	      );
  //! Insert an object in the NUMEXO Map
  void AddMesytec(Int_t key, //!< Parameter id
         BaseDetector* ptr, //!< POinter to Ganil Detector
         UShort_t uid //!< Internal detector number
         );
    
  //! AddRaw Data to Detector
  void AddData(
                                        UShort_t *Data, //!< Pointer to Label/Data
                                        ULong64_t TS=0);
  //! AddRaw Data to Detector
  void AddNUMEXOData(const UInt_t& key,const UShort_t& Data,const ULong64_t& TS,const Int_t& DataValid = -1,
         const Int_t& PileUp = -1,const Int_t& Time=-1);
  //! AddRaw Data to GETDetector
  void AddGETData(UInt_t key, UShort_t Data, ULong64_t TS, Int_t Time=-1);
  //! AddRaw Data to Detector
  void AddMesytecData(const UInt_t& key, const UShort_t& Data, const ULong64_t& TS, const UShort_t &Time=-1);
#ifdef SWAP_DC
  void AddDataOp(UShort_t key, //!< Parameter id
					  MDataOp* ptr //!< Pointer to Data Op
					  );
#endif
  //! Remove Object By Key
  void Remove(UShort_t key);
 //! Remove Object By Key
  void RemoveNUMEXO(UShort_t key);
 //! Remove Object By Key
  void RemoveGET(UShort_t key);
  //! Remove Object By Key
  void RemoveMesytec(UShort_t key);
  //! Return a BaseDetector pointer Label/Data. All Make Data modification (Bit Swaps) if needed
  BaseDetector* GetAddress(UShort_t *Data);
  //! Return a BaseDetector pointer from key
  BaseDetector* GetAddress(UShort_t key);
  //! Return a BaseDetector pointer from key
  UShort_t GetNumber(UShort_t key);
  //! Return a BaseDetector pointer from NUMEXO key
  inline BaseDetector* GetNUMEXOAddress(const UShort_t& key);
  //! Return a BaseDetector pointer from NUMEXO key
  inline UShort_t GetNUMEXONumber(const UShort_t& key);
  //! Return a BaseDetector pointer from GET key
  inline BaseDetector* GetGETAddress(const UShort_t& key);
  //! Return a BaseDetector pointer from GET key
  inline UShort_t GetGETNumber(const UShort_t& key);
  //! Return a BaseDetector pointer from Mesytec key
  inline BaseDetector* GetMesytecAddress(const UShort_t& key);
  //! Return a BaseDetector pointer from Mesytec key
  inline UShort_t GetMesytecNumber(const UShort_t& key);

  //! Print The Map 
  void Print(void);
  void DataOp(Parameters *Par); //! Bidouillages ;... 

protected:
  UInt_t MaxNumberOfParameters;  // Max Number of Parameters
  BaseDetector* *Address;   //! The pointer  Map;
  UShort_t *DetNr; //! Internal Detector Number
  
  UShort_t MaxNUMEXONumberOfParameters;  // Max Number of Parameters for NUMEXO
  BaseDetector* *NUMEXOAddress;   //! The pointer  Map;
  UShort_t *NUMEXODetNr; //! Internal Detector Number
 
  UShort_t MaxGETNumberOfParameters;  // Max Number of Parameters for GET
  BaseDetector* *GETAddress;   //! The pointer  Map;
  UShort_t *GETDetNr; //! Internal Detector Number

  UShort_t MaxMesytecNumberOfParameters;  // Max Number of Parameters for Mesytec
  BaseDetector* *MesytecAddress;   //! The pointer  Map;
  UShort_t *MesytecDetNr; //! Internal Detector Number


#ifdef SWAP_DC
  MDataOp* *MapOp;  //! Map  of Operation
  Swap *MySwap; //! Swap Operation 
#endif
};
#endif
