/****************************************************************************
 *    Copyright (C) 2012-2019 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef GETTOPOLOGY_H
#define GETTOPOLOGY_H
/**
 * @file   GETTopology.cc
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   December 2021
 * @version 0.1
 *
 * @brief  Class of To Handle GET Topology Description File
 *
 *
 */

#include "Defines.hh"
#include "GETParameters.hh"

class GETTopology
{
   public:
   GETTopology(void);
   ~GETTopology(void);

   //!
   static GETTopology* getInstance(void);

  void Init(const Char_t * confile,const Char_t * conpath);

   void GetSlot(const Char_t *ParName) {};

   private:
   GETParameters *NP;

   private:
      // The static instance of the Parameters class:
      static GETTopology* instance;

};

#endif // GETTOPOLOGY_H
