/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef MEMMANAGER_HH
#define MEMMANAGER_HH
/**
 * @file   MemManager.hh
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   January, 2013
 * @version 0.1
 * @todo 
 * @brief  MemManager class 
 * 
 * Memory Manager Class
 */
//#include "Defines.hh"


class MemManager
{
public:
  MemManager(void){};
  ~MemManager(void){};

  // Overload of new operators
  
  template <typename T> 
  void    *operator new(size_t sz,T*& vPtr)// throw(std::bad_alloc)
  { 
	 START;
  	 if(vPtr){
  		char ErrMess[256]; sprintf(ErrMess,"Trying to initialize on a non-NULL pointer for object of type : %s", typeid(*vPtr).name());
  		cout << ErrMess << endl;
		MErr *Er = NULL;
  		Er = new MErr(WhoamI,0,0, ErrMess);
  		throw Er; 
  	 }
	 void *p = NULL;
	 p =  malloc(sz);
	 if(!p) 
		{
		  char ErrMess[256]; sprintf(ErrMess,"Could not allocate memory for object of type : %s", typeid(*vPtr).name());
		  MErr *Er = NULL;
		  Er = new MErr(WhoamI,0,0, ErrMess);
		  throw Er; 
		}
	return p; 
	
	END;
  }
  template <typename T> 
  void    *operator new[](size_t sz,T*& vPtr)  //throw(std::bad_alloc)
  { 
  	 START;
  	 if(vPtr){
  		char ErrMess[256]; sprintf(ErrMess,"Trying to initialize on a non-NULL pointer for object of type : %s", typeid(*vPtr).name());
		MErr *Er = NULL;
  		Er = new MErr(WhoamI,0,0, ErrMess);
  		throw Er; 
  	 }
	 void *p = NULL;
	 p =  malloc(sz);
	 if(!p) 
		{
		  char ErrMess[256]; sprintf(ErrMess,"Could not allocate memory for object of type : %s", typeid(*vPtr).name());
		  MErr *Er = NULL;
		  Er = new MErr(WhoamI,0,0, ErrMess);
		  throw Er; 
		}
	 return p;
  	 END;
  }
  template <typename T>
  void operator delete(void*, T*& Ptr)
  {
	 START;
	 if(Ptr)
		{
		  Ptr->~T();
		  free (Ptr);
		  Ptr = NULL;
		}
	 else
		{
		  char ErrMess[256]; sprintf(ErrMess,"Unexpected Exception : trying to delete NULL pointer");
		  MErr *Er = NULL;
		  Er = new MErr(WhoamI,0,0, ErrMess);
		  throw Er; 
		}
	 END;
  }

  template <typename T>
  void operator delete[](void*, T*& Ptr)
  {
	 START;
	 if(Ptr)
		{ 
		  Ptr->~T();
		  free (Ptr);
		  Ptr = NULL;
		}
	 else
		{
		  char ErrMess[256]; sprintf(ErrMess,"Unexpected Exception : trying to delete NULL pointer");
		  MErr *Er = NULL;
		  Er = new MErr(WhoamI,0,0, ErrMess);
		  throw Er; 
		}
	 END;
  }
 
  
};
#endif
