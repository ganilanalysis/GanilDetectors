/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _DEFINES_HH
#define _DEFINES_HH
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <exception>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cstring>
#include <string>
#include <ctime>

#include <fcntl.h>
#include <unistd.h> 

#include <sys/stat.h>
#include <time.h>
#include <typeinfo>

#ifndef WITH_ROOT
#include "MTypes.hh"
#else
#include <TROOT.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TObject.h>
#include <TTree.h>
#include <TCutG.h>
#include <TH1.h>
#include <TH1F.h>
#include <TH2.h>
#include <TH2F.h>
#include <TVector3.h>
#include <TMath.h>
#include <TKey.h>
#endif

using namespace std;

//#define DEBUG
// Maximum number of Parameters (used to define the Parameter Map array) 
#define MAX_PAR 65355


#define MODE_WATCHER 0
#define MODE_D2R 1
#define MODE_D2A 2
#define MODE_R2A 3
#define MODE_CALC 4
#define MODE_RECAL 5

// // External Variables to be defined in SetupAnalysis.cc 

#define WhoamI __PRETTY_FUNCTION__

#ifdef DEBUG
#define START cout << "--> Entering " << WhoamI << endl;
#define END cout << " <-- Exiting " << WhoamI << endl
#else
#define START try{ //
#define END }catch(MErr * Er) { Er->Set(WhoamI); throw Er;  } catch(const std::exception &e){ char ErrMess[500]; sprintf(ErrMess," Unexpected Exception : %s",e.what()); MErr *Er = new MErr(WhoamI,0,0, ErrMess); throw Er; }catch(...){ char ErrMess[500]; sprintf(ErrMess," Unexpected Exception"); MErr *Er = new MErr(WhoamI,0,0, ErrMess); throw Er;} //
#endif

// Includes
#include "OptionsDefs.hh"
#include "MUtils.hh"
#include "MErr.hh"
#include "MemManager.hh"
#include "MOperators.hh"
#include "MOFile.hh"
#include "MIFile.hh"
#include "MDynamicArrays.hh"
#ifdef WITH_ROOT
#include "HistoManager.hh"
#endif
#endif
