/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "TimeStamp.hh"
TimeStamp::TimeStamp(const Char_t *Name,
                         UShort_t NDetectors, 
                         Bool_t RawData, 
                         Bool_t CalData, 
                         Bool_t DefaultCalibration, 
                         const Char_t *NameExt
                         )
: BaseDetector(Name, 2, false, true, DefaultCalibration,false,NameExt)
{
  START;  
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = true; // force raw data
  AllocateComponents(); 
  
  if(fCalData)
    {
      SetCalName(GetName(),0);    
      sprintf(CalNameI[1],"%s_diff",GetName());
    }
  FirstTS = 0;  
  LastTS = 0;
  TS = 0;
  TSRef = 0;

  // SetCalHistogramsParams(10000,0,20000); // Set Histograms for ~ 2 days of history
  SetCalHistogramsParams(10000,0,1000000); // Set Histograms for ~ 2 days of history
  UnSetGateCal();
  END;
}

TimeStamp::~TimeStamp(void)
{
  START;
  
  END;
}

void TimeStamp::Clear()
{
    START;
    BaseDetector::Clear();
    TS=0;
    END;
}

void TimeStamp::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = false;
  Bool_t DefCal = false;
  Bool_t PosInfo = false;
  
  // Silicon Energies
  Char_t Name[200];
  sprintf(Name,"%s_WORDS",GetName());
  BaseDetector *TSWORDS = new BaseDetector(Name,NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,""); 
  TSWORDS->UnSetGateRaw();
  AddComponent(TSWORDS);  

  END;
}

void TimeStamp::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  for(UShort_t i=0; i< DetList->size(); i++)
    DetList->at(i)->SetParameters(Par,Map);
  END;
}

void TimeStamp::SetLowWord(const Char_t *PName)
{
  START;
  GetSubDet(0)->SetParameterName(PName,0);
  END;
}

void TimeStamp::SetMidWord(const Char_t *PName)
{
  START;
  GetSubDet(0)->SetParameterName(PName,1);
  END;
}

void TimeStamp::SetHighWord(const Char_t *PName)
{
  START;
  GetSubDet(0)->SetParameterName(PName,2);
  END;
}

Bool_t TimeStamp::Treat(void)
{ 
  START;
  if(fMode != MODE_RECAL)
    TS = 0;
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        {
          DetList->at(i)->Treat();
        }
    }

  
  //  if(fCalData && fRawData)
    {
      ULong64_t TUp = DetList->at(0)->GetRaw(2);
      ULong64_t TMid = DetList->at(0)->GetRaw(1);
      ULong64_t TLow = DetList->at(0)->GetRaw(0);	
      TUp = TUp & 0xFFFF;
      TMid = TMid & 0xFFFF;
      TLow = TLow & 0xFFFF;
      TS = (TUp << 32) + (TMid<<16) + (TLow);
      
      if(FirstTS == 0)
	{
	  FirstTS = TS;
	  TSRef = FirstTS;
	}
      
      
      if((TS-TSRef)*1.e-8 > 20000) // reset every 20000 sec
	TSRef = TS;
      // Should also reset Histogram

      SetCalData(0,(Float_t)((TS-TSRef)*1.e-8)); // Store delta TS in Seconds
      SetCalData(1,(Float_t)((TS-LastTS)*10./1000.)); // Store delta TS in mSeconds
      
      #ifdef DEBUG
      cout << " <" << GetName() << "> "    
           << TUp << " " 
           << TMid << " " 
           << TLow << "\n" 
           << TS << " ns - " 
           << TS*1.e-8 << "s  " 
	   << FirstTS  << " " 
	   << Cal[0] << "sec \n LastTS "
	   << Cal[1]
	
	   << endl;
      #endif      
      
      LastTS = TS;
    }


	return(isPresent); 
  END;
}

#ifdef WITH_ROOT
void TimeStamp::OutAttach(TTree *OutTree)
{

  // Show Initial Vals
  DetList->at(0)->SetOutAttachRawI(false);
  DetList->at(0)->OutAttach(OutTree);

  // SetMainHistogramFolder("");
  SetHistogramsCal1DFolder("");
  SetHistogramsCal2DFolder("");
  for(UShort_t i = 0;i<DetList->size();i++)
    {
      DetList->at(i)->SetMainHistogramFolder("");
    }

  if(fMode == MODE_WATCHER)
    {
      if(fRawData)
        {
          SetHistogramsRaw(true);
        }
      if(fCalData)
        {
          SetHistogramsCal(true);      
        }
  
    }
  else if(fMode == MODE_D2A || fMode == MODE_D2R || fMode == MODE_RECAL )
    {

      if(fRawData)
        {
          SetHistogramsRaw(true);
        }

      if(fCalData)
        {
          SetHistogramsCal(true);      
        }
      Char_t BranchName[200];
      Char_t BranchType[200];
      //      if(fCalData)
        {
          sprintf(BranchName,"%s",GetName());
          sprintf(BranchType,"%s/l",GetName());
          OutTree->Branch(BranchName,&TS,BranchType);
        }
    }     

}

void TimeStamp::InAttachCal(TTree *InTree)
{
  if(fCalData)
    {
      Int_t Res = 0;
      Res = InTree->SetBranchAddress(Form("%s",GetName()),&TS);
      //      if(VerboseLevel >= V_INFO)
	cout << "Reading from Branch " << Form("%s",GetName()) << endl;
      if(Res != 0)
	{
      Char_t Message[500];
	  sprintf(Message,"In <%s><%s> Trying to attach branch \"%s\" that does not exist or match requested type!", GetName(), GetName(),Form("%s",GetName()));
	  MErr * Er= new MErr(WhoamI,0,0, Message);
	  throw Er;
  	}
    }
}
#endif
