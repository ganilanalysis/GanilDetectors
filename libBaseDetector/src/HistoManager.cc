/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifdef WITH_ROOT
#include "HistoManager.hh"
HistoManager::HistoManager(void)
{
  START;
  LogManager *LM = LogManager::getInstance();
  
  L = NULL;
  L = LM->GetFilePtr();

  // L = Log;
  H =NULL;
  H = &Histograms;
  F =NULL;
  F = &Folders;
  END;
}
HistoManager::~HistoManager() noexcept(false)
{
  START;
  for(UShort_t i=0;i<GetSize();i++)
	 {
		TH1 * Hst = GetHistogram(i);
		if(Hst)
		  {
		    //			 delete Hst;
		  }
		//		Hst = NULL;
		GetFolder(i).clear();
	 }
  // H->clear();
  // H =NULL;
  F->clear();
  F=NULL;
  END;
}
 
void HistoManager::Add(TH1* V, string FolderName)
{
  START;
  H->push_back(V);
  F->push_back(FolderName);
  END;
}

TH1 * HistoManager::FindHistogram(Char_t *Name, Bool_t Show)
{
  START;

  TH1 *Hst = NULL;
  for(UShort_t i=0;i<GetSize();i++)
      {
	  if(strcmp(H->at(i)->GetName(),Name)==0)
	      {
		  // Found The Object
		  Hst = H->at(i);
		  // if(Show)
		  //   Hst->Print();
		  return Hst;
		  break;
	      }
      }
  if(!Hst)
      {
	  Char_t Message[100];
	  sprintf(Message,"<HistoManager> Could not find histogram named %s ",Name);
	  MErr * Er = new MErr(WhoamI,0,0, Message);
	  throw Er;
      }
  return Hst;
  END;
}

 
TH1 * HistoManager::GetHistogram(UShort_t Pos)
{
  START;
  if(Pos < GetSize())
	 return H->at(Pos);
  else
	 {
		Char_t Message[100];
		sprintf(Message,"Call Out of bounds");
		MErr * Er = new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}
string HistoManager::GetFolder(UShort_t Pos)
{
  START;
  if(Pos < GetSize())
	 return F->at(Pos);
  else
	 {
		Char_t Message[100];
		sprintf(Message,"Call Out of bounds");
		MErr * Er = new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}
 
// void HistoManager::Fill(UShort_t V, UShort_t Pos)
// {
//   START;
//   if(Pos < GetSize())
// 	 GetHistogram(Pos)->Fill(V);
//   else
// 	 {
// 		Char_t Message[100];
// 		sprintf(Message,"Call Out of bounds");
// 		MErr * Er = new MErr(WhoamI,0,0, Message);
// 		throw Er;
// 	 }
//   END;
// }
// void HistoManager::Fill(Float_t V, UShort_t Pos)
// {
//   START;
//   if(Pos < GetSize())
// 	 GetHistogram(Pos)->Fill(V);
//   else
// 	 {
// 		Char_t Message[100];
// 		sprintf(Message,"Call Out of bounds");
// 		MErr * Er = new MErr(WhoamI,0,0, Message);
// 		throw Er;
// 	 }
//   END;
// }
// void HistoManager::Fill(UShort_t V1, UShort_t V2, UShort_t Pos)
// {
//   START;
//   if(Pos < GetSize())
// 	 GetHistogram(Pos)->Fill(V1,V2);
//   else
// 	 {
// 		Char_t Message[100];
// 		sprintf(Message,"Call Out of bounds");
// 		MErr * Er = new MErr(WhoamI,0,0, Message);
// 		throw Er;
// 	 }
//   END;
// }
// void HistoManager::Fill(Float_t V1, Float_t V2, UShort_t Pos)
// {
//   START;
//   if(Pos < GetSize())
// 	 GetHistogram(Pos)->Fill(V1,V2);
//   else
// 	 {
// 		Char_t Message[100];
// 		sprintf(Message,"Call Out of bounds");
// 		MErr * Er = new MErr(WhoamI,0,0, Message);
// 		throw Er;
// 	 }
//   END;
// }

void HistoManager::Clear(UShort_t Pos)
{
  START;
  
  H->at(Pos) = NULL;
  F->at(Pos) = "";

  
  END;
}
 
void HistoManager::PrintAll()
{
  START;
  TH1 *Hst = NULL;
  if(L) L->File << "Histograms:" << endl;
  cout << "Histograms:" << endl;
  for(UShort_t i=0;i<GetSize();i++)
	 {
		if((Hst = GetHistogram(i))){
		  // if(L) L->File << GetHistogram(i)->Print();
		  // if(L) L->File << "In Folder : " << GetFolder(i) << endl;;
		  Hst->Print();
		  cout << "In Folder : " << GetFolder(i) << endl;
		}
		else{
		  cout << " Histogram is already erased. Detector Object must have been deleted alread ! " << endl;
		}
	 }
  END;
}
#endif
