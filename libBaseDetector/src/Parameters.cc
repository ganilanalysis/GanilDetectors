/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "Parameters.hh"
/**
 * @file   Parameters.cc
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   December, 2012
 * @version 0.1
 * @todo  Not yet checking for duplicates at insertion ...
 * @todo  Adding a check of Parameters we want to use ... or this is to be done in BaseDetector //...
 * @brief  Class of GaniExpt Parameters
 * 
 * 
 */

/** 
 * Constructor
 */
Parameters *Parameters::instance = NULL;

Parameters* Parameters::getInstance()
{
   // A new instance of RootInput is created if it does not exist:
   if (instance == NULL) {
     cout << "Paramters === Warning !!! Trying to get instance non initialized Parameters Class " << endl;
     // instance = new Parameters(16384,Log);
   }

  // The instance of RootInput is returned:
   return instance;
}


// void Parameters::Destroy()
// {
//    if (instance != NULL) {
//       delete instance;
//       instance = 0;
//    }
// }

Parameters::Parameters(UShort_t MaxPar)
{
  START;
  instance = this;
  MaxParameters = MaxPar;
  SetPointers();
  LogManager *LM = LogManager::getInstance();
  
  L = LM->GetFilePtr();
  AllocateArrays();
  Clear();
  END;
}

/** 
 * Destructor
 */
Parameters::~Parameters(void)  noexcept(false)
{
  START;
  PName = FreeDynamicArray<string>(PName);
  PSize = FreeDynamicArray<UShort_t>(PSize);
  END;
}

/** 
 * Force pointers to NULL when constructing the class
 */
void Parameters::SetPointers(void)
{ 
  START;
  // Log File
  L = NULL;
  // Parameters Names
  PName = NULL;
  // Parameters Size
  PSize = NULL;
  END;
}

/** 
 * Allocate Parameters Arrays
 */
void Parameters::AllocateArrays()
{ 
  START;
  // Parameter Names
  PName = AllocateDynamicArray<string>(MaxParameters);
  // Parameter Size
  PSize = AllocateDynamicArray<UShort_t>(MaxParameters);
  END;
}

/** 
 * Clear the parameter arrays
 */
void Parameters::Clear(void)
{
  START;
  for (UShort_t i=0; i<MaxParameters; i++)
	 {
		PName[i] = "";
		PSize[i] = 0;
	 }
  END;
}

/** 
 * Insert a parameter in the list
 */
void Parameters::Add(UShort_t id, string name, UShort_t size)
{
  START;
  
  if(id < MaxParameters) 
	 {
		PName[id] = name;
		PSize[id] = size;
	 }
  else
	 {
		char Error[256];
		sprintf(Error,"Trying to add a parameter with ID (%d) is larger than MAX_PAR (%d) !",id,MaxParameters);
		MErr * Er = NULL;
		Er = new MErr(WhoamI,0,0, Error);
		throw Er;
	 }
  END;
}

/** 
 * Remove a parameter from the list using provided parameter id
 */
void Parameters::Remove(UShort_t Pid)
{
  START;
  PName[Pid] = "";
  PSize[Pid] = 0;
  END;
}

/** 
 * Get Parameter Id from Name
 * Return -1 if not found
 */
UShort_t Parameters::GetPar(string name)
{
  START;
  for (UShort_t i=0; i<MaxParameters; i++)
	 {
		if(PSize[i]>0)
		  if(strcmp(PName[i].c_str(),name.c_str()) == 0)
			 return i;
	 }
  //Could not find parameter
    char Error[256];
    sprintf(Error,"Could not find  parameter (%s) in parameter list !",name.c_str());
    MErr * Er = NULL;
    Er = new MErr(WhoamI,0,0, Error);
    throw Er;
    Er = CleanDelete(Er);
  return 0;
  END;
}


/** 
 * Print a dump of the parameters Map
 */
void Parameters::Print(void)
{
  START;
  cout << "==========================================" << endl;
  cout << " Dump of the Parameters List" << endl;
  cout << "==========================================" << endl;
  for (UShort_t i=0; i<MaxParameters; i++)
	 {
        if(PSize[i] > 0)
		  cout << "ParamId : " << i << " - Name : " << PName[i] << " \t (" << PSize[i] << " bytes)" << endl;
	 }
  cout << " Number of Parameters : " << GetNParameters() << endl; 
 
  END;
}

//! Specific GetParameter Method for AGATA
/* Checks Localy, then to specific directory

 */
void Parameters::GetParametersAGATA(const Char_t* expname, Int_t RunNr)
{

 START;

  struct stat FileStat;
  char* HomeDir = getenv("HOME");
  cout << "Home " << HomeDir << endl;
  char actionFilePAR[1000];
  char actionFilePAR2[100];
  char BasePath[500];
  
  

  if(RunNr == -1)
    sprintf(BasePath, "/zCurrentDirectory/Conf/");
  else
    sprintf(BasePath, "/data/Run%d/Conf/",RunNr);

  sprintf(actionFilePAR, "ACTIONS_%s.CHC_PAR", expname);
  strcpy(actionFilePAR2, actionFilePAR);

  cout << actionFilePAR << endl;

  if (stat(actionFilePAR, &FileStat) < 0) {

    strcpy(actionFilePAR, "");
    sprintf(actionFilePAR, "%s/%s",BasePath,actionFilePAR2);
    cout << actionFilePAR << endl;
    
    if (stat(actionFilePAR, &FileStat) < 0) 
      {
          
        Char_t Message[100];
        sprintf(Message,"Could not Find ACTION File : ");
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
      }
  }
  
  cout << " Found Parameter file : " << actionFilePAR <<endl;
  
  GetParametersFromActionFile(actionFilePAR);

  END;
}

/** 
 * Get Parameter from File
 */
void Parameters::GetParameters(const Char_t* expname)
{
  START;

  struct stat FileStat;
  char* HomeDir = getenv("HOME");
  cout << "Home " << HomeDir << endl;
  char actionFilePAR[500];
  char actionFilePAR2[100];
  strcpy(actionFilePAR, "");
  sprintf(actionFilePAR, "ACTIONS_%s.CHC_PAR", expname);
  strcpy(actionFilePAR2, actionFilePAR);

  // Check Local the existence of Action Files in the following order
  // 1) in the current directory
  // 2) in the GECO directory
  // 3) in Das directory

  cout << actionFilePAR << endl;

  if (stat(actionFilePAR, &FileStat) < 0) {

    strcpy(actionFilePAR, "");
    sprintf(actionFilePAR, "%s/ganacq_manip/%s/GECO/%s/ACQ/%s",
    			HomeDir, expname, expname,  actionFilePAR2);
    cout << actionFilePAR << endl;
    
    if (stat(actionFilePAR, &FileStat) < 0) {
      
      strcpy(actionFilePAR, "");
      sprintf(actionFilePAR, "%s/ganacq_manip/%s/das-save/%s",
              HomeDir, expname,  actionFilePAR2);

      cout << actionFilePAR << endl;
      if (stat(actionFilePAR, &FileStat) < 0) 
        {
          
          Char_t Message[100];
          sprintf(Message,"Could not Find ACTION File : ");
          MErr * Er = new MErr(WhoamI,0,0, Message);
          throw Er;
        }
    }
  }
  
  cout << " Found Parameter file : " << actionFilePAR <<endl;
  
  GetParametersFromActionFile(actionFilePAR);

  END;
}

void Parameters::GetParametersFromActionFile(Char_t* ActionFileName)
{
  START;

  vector<string> *ParName = new vector<string>();
  vector<UShort_t> *ParId = new vector<UShort_t>();
  vector<UShort_t> *ParSize = new vector<UShort_t>();
	
  Int_t NPars = 0;
  Int_t LineCtr = 0;
  Char_t Name[40];
  Int_t Label = 0;
  Int_t Bits = 0;
  Int_t Dummy = 0;

  MIFile *IF = NULL;
  IF = new MIFile(ActionFileName);

  cout << "<DataFile>  Reading Parameters file " << IF->GetFileName() << endl;
  char Line[255];
  UShort_t Len=255;
  
	while (IF->GetLine(Line,Len)) {
     LineCtr++;
//     if(sscanf(Line, "%s\t%d\t%u\t%d",Name,&Label, &Bits, &Dummy) == 4) // Modifs 2018 ?
       if(sscanf(Line, "%s\t%d\t%u",Name,&Label, &Bits) == 3)
       {
         ParName->push_back(Name);
         ParId->push_back(Label);
         ParSize->push_back(Bits);
         NPars++;   
       }
     else
       {

         IF = CleanDelete(IF);
         Char_t Message[500];
         sprintf(Message,"Bad Format for action file %s line %d -- ", IF->GetFileName(),LineCtr);

         MErr * Er = new MErr(WhoamI,0,0, Message);
         throw Er;
       }
   }
   IF = CleanDelete(IF);

   SetNParameters(NPars);
   for(UShort_t i=0; i< ParName->size(); i++)
     {
       Add(ParId->at(i),ParName->at(i),ParSize->at(i));
     }
  delete	 ParName;
  delete	 ParId;
  delete	 ParSize;
  
  END;
}
