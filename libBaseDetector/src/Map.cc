
/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
/**
 * @file   Map.cc
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   December, 2012
 * @version 0.1
 * @todo How to delete objects (Reconfiguration ....)
 * @todo Test of performances Map, List, ... , depend on size of Elements
 * @brief  Map class 
 * 
 * Ganil Map using Fixed Size Array of pointers
 * BaseDetector objects (pointer) in the code
 */

#include "Map.hh"
#include "BaseDetector.hh"
#include "Parameters.hh"
#include "NUMEXOParameters.hh"
#include "MesytecParameters.hh"
#include "GETParameters.hh"

Map::Map(UShort_t MaxParameters, Int_t MaxNUMEXOParameters, Int_t MaxGETParameters, Int_t MaxMesytecParameters) {
    START;

    MaxNumberOfParameters = MaxParameters;
    MaxNUMEXONumberOfParameters = MaxNUMEXOParameters;
    MaxGETNumberOfParameters = MaxGETParameters;
    MaxMesytecNumberOfParameters = MaxMesytecParameters;

    // Pointer Map
    Address = NULL;
    Address = AllocateDynamicArray<BaseDetector*>(MaxNumberOfParameters);
    // Internal detector Number Map
    DetNr = NULL;
    DetNr = AllocateDynamicArray<UShort_t>(MaxNumberOfParameters);
   // Pointer Map
    NUMEXOAddress = NULL;
    NUMEXOAddress = AllocateDynamicArray<BaseDetector*>(MaxNUMEXONumberOfParameters);
    // Internal detector Number Map
    NUMEXODetNr = NULL;
    NUMEXODetNr = AllocateDynamicArray<UShort_t>(MaxNUMEXONumberOfParameters);
   // Pointer Map
    GETAddress = NULL;
    GETAddress = AllocateDynamicArray<BaseDetector*>(MaxGETNumberOfParameters);
    // Internal detector Number Map
    GETDetNr = NULL;
    GETDetNr = AllocateDynamicArray<UShort_t>(MaxGETNumberOfParameters);
    // Pointer Map
     MesytecAddress = NULL;
     MesytecAddress = AllocateDynamicArray<BaseDetector*>(MaxMesytecNumberOfParameters);
     // Internal detector Number Map
     MesytecDetNr = NULL;
     MesytecDetNr = AllocateDynamicArray<UShort_t>(MaxMesytecNumberOfParameters);
    for (UShort_t i = 0; i < MaxNumberOfParameters; i++) {
        Address[i] = NULL;
    }
    for (UShort_t i = 0; i < MaxNUMEXONumberOfParameters; i++) {
        NUMEXOAddress[i] = NULL;
    }
    for (UShort_t i = 0; i < MaxGETNumberOfParameters; i++) {
        GETAddress[i] = NULL;
    }
    for (UShort_t i = 0; i < MaxMesytecNumberOfParameters; i++) {
        MesytecAddress[i] = NULL;
    }
#ifdef SWAP_DC
    MapOp = NULL;
    MapOp = AllocateDynamicArray<MDataOp*>(MaxNumberOfParameters);
    for (UShort_t i = 0; i < MaxNumberOfParameters; i++) {
        MapOp[i] = NULL;
    }
#endif
    END;
}

Map::~Map(void) noexcept(false)
{
  START;
  Address = FreeDynamicArray<BaseDetector*>(Address);
  DetNr = FreeDynamicArray<UShort_t>(DetNr);
  NUMEXOAddress = FreeDynamicArray<BaseDetector*>(NUMEXOAddress);
  NUMEXODetNr = FreeDynamicArray<UShort_t>(NUMEXODetNr);
  GETAddress = FreeDynamicArray<BaseDetector*>(GETAddress);
  GETDetNr = FreeDynamicArray<UShort_t>(GETDetNr);
  MesytecAddress = FreeDynamicArray<BaseDetector*>(MesytecAddress);
  MesytecDetNr = FreeDynamicArray<UShort_t>(MesytecDetNr);

#ifdef SWAP_DC
  MapOp = FreeDynamicArray<MDataOp*>(MapOp);
  MySwap = CleanDelete(MySwap);
#endif
  END;
}


void Map::Clear(void)
{
 START;

 END;
}

void Map::AddNUMEXOData(const UInt_t& key, const UShort_t& Data,const ULong64_t& TS,const Int_t& DataValid,const Int_t& PileUp,const Int_t& Time)
{
  START;
  BaseDetector * Lbl = NULL;
  try{
    if((Lbl= GetNUMEXOAddress(key)))
    {
      //      cout << "Set Data to Numexo " << key << " Data " << Data << "Pile Up" << PileUp << endl;
      Lbl->AddRawData(GetNUMEXONumber(key),Data,TS,DataValid,PileUp,Time);
    }
  }
  catch (MErr * Er)
    {
      throw Er;
    }
  END;
}

void Map::AddGETData(UInt_t key, UShort_t Data, ULong64_t TS,Int_t Time)
{
  START;
  BaseDetector * Lbl = NULL;
  try{
    if((Lbl= GetGETAddress(key)))
    {
      //cout<<Lbl->GetName()<<" "<<Lbl->GetNumberOfDetectors()<<" *** "<<key<<" "<<GetGETNumber(key)<<endl;
      //      cout << "Set Data to Numexo " << key << " Data " << Data << "Pile Up" << PileUp << endl;
 
      Lbl->AddRawData(GetGETNumber(key),Data,TS,-1,-1,Time);
      
    }
  }
  catch (MErr * Er)
    {
      throw Er;
    }
  END;
}

void Map::AddMesytecData(const UInt_t& key,const UShort_t& Data,const ULong64_t& TS, const UShort_t& Time)
{
  START;
  BaseDetector * Lbl = NULL;
  try{
    if((Lbl= GetMesytecAddress(key)))
    {
#ifdef DEBUG
        cout << "Set Data to Mesytec " << key << " ("<<GetMesytecNumber(key)<<") - Lbl " << Lbl  << "-->  Data : " << Data << " TS : " << TS  << endl;
#endif
        Lbl->AddRawData(GetMesytecNumber(key),Data,TS,-1,-1,Time);
    }
  }
  catch (MErr * Er)
    {
      throw Er;
    }
  END;
}


void Map::AddData(UShort_t *Data, ULong64_t TS)
{
  START;
  BaseDetector * Lbl = NULL;
  try{
    if((Lbl= GetAddress(Data)))
      {
        Lbl->AddRawData(GetNumber(*(Data+0)),*(Data+1),TS);
    }
  }
  catch (MErr * Er)
    {
      cerr << Lbl->GetName() << endl;
      throw Er;

    }
  END;
}

void Map::Add(UShort_t key, BaseDetector* ptr, UShort_t uid)
{
  START;
  if(key > MaxNumberOfParameters){
	 char Error[256];
	 sprintf(Error,"The key \"%d\" is larger than MAX_PAR value (%d)!",key,MaxNumberOfParameters);
	 MErr * Er = NULL;
	 Er = new(Er) MErr(WhoamI,0,0, Error);
	 throw Er;
  }

  if(NULL != Address[key]){
	 char Error[256];
	  	 sprintf(Error,"The key \"%d\" already exists. This is a duplicate !",key);
	  	 MErr * Er = NULL;
	  	 Er = new(Er) MErr(WhoamI,0,0, Error);
	  	 throw Er;
  }
  Address[key] = ptr;
  DetNr[key] = uid;
  
  END;
}

void Map::AddNUMEXO(Int_t key, BaseDetector* ptr, UShort_t uid)
{
  START;
  //cout << "Adding" << key << " " << ptr << " "<< uid << endl;
  if(key > MaxNUMEXONumberOfParameters){
	 char Error[256];
	 sprintf(Error,"The key \"%d\" is larger than MAX_PAR value (%d)!",key,MaxNUMEXONumberOfParameters);
	 MErr * Er = NULL;
	 Er = new(Er) MErr(WhoamI,0,0, Error);
	 throw Er;
  }

  if(NULL != NUMEXOAddress[key]){
	 char Error[256];
                 sprintf(Error,"The key \"%d\" already exists in NUMEXO Map. This is a duplicate !",key);
	  	 MErr * Er = NULL;
	  	 Er = new(Er) MErr(WhoamI,0,0, Error);
	  	 throw Er;
  }

  NUMEXOAddress[key] = ptr;
  NUMEXODetNr[key] = uid;
  
  END;
}

void Map::AddMesytec(Int_t key, BaseDetector* ptr, UShort_t uid)
{
  START;
  //cout << "Adding" << key << " " << ptr->GetName() << " "<< uid << endl;
  if(key > MaxMesytecNumberOfParameters){
     char Error[256];
     sprintf(Error,"The key \"%d\" is larger than MAX_PAR value (%d)!",key,MaxMesytecNumberOfParameters);
     MErr * Er = NULL;
     Er = new(Er) MErr(WhoamI,0,0, Error);
     throw Er;
  }

  if(NULL != MesytecAddress[key]){
     char Error[256];
                 sprintf(Error,"The key \"%d\" already exists in Mesytec Map. This is a duplicate !",key);
         MErr * Er = NULL;
         Er = new(Er) MErr(WhoamI,0,0, Error);
         throw Er;
  }

  MesytecAddress[key] = ptr;
  MesytecDetNr[key] = uid;

  END;
}


void Map::AddGET(Int_t key, BaseDetector* ptr, UShort_t uid)
{
  START;
  //cout << "Adding" << key << " " << ptr << " "<< uid << endl;
  if(key > MaxGETNumberOfParameters){
	 char Error[256];
	 sprintf(Error,"The key \"%d\" is larger than MAX_PAR value (%d)!",key,MaxGETNumberOfParameters);
	 MErr * Er = NULL;
	 Er = new(Er) MErr(WhoamI,0,0, Error);
	 throw Er;
  }

  if(NULL != GETAddress[key]){
	 char Error[256];
                 sprintf(Error,"The key \"%d\" already exists in GET Map. This is a duplicate !",key);
	  	 MErr * Er = NULL;
	  	 Er = new(Er) MErr(WhoamI,0,0, Error);
	  	 throw Er;
  }

  GETAddress[key] = ptr;
  GETDetNr[key] = uid;
  
  END;
}

void Map::Remove(UShort_t key)
{
  START;
  
  if(NULL == Address[key]){
	 char Error[256];
	 sprintf(Error,"The key \"%d\" was not assigned in the map while trying to delete it !",key);
	 MErr * Er = NULL;
	 Er = new(Er) MErr(WhoamI,0,0, Error);
	 throw Er;
  }
  END;
}

BaseDetector* Map::GetMesytecAddress(const UShort_t& Id)
{
  START;
  return MesytecAddress[Id];
  END;
}

UShort_t Map::GetMesytecNumber(const UShort_t& key)
{
  START;
  return MesytecDetNr[key];
  END;
}
void Map::RemoveMesytec(UShort_t key)
{
  START;

  if(NULL == MesytecAddress[key]){
     char Error[256];
     sprintf(Error,"The key \"%d\" was not assigned in the map while trying to delete it !",key);
     MErr * Er = NULL;
     Er = new(Er) MErr(WhoamI,0,0, Error);
     throw Er;
  }
  END;
}

BaseDetector* Map::GetNUMEXOAddress(const UShort_t& Id)
{
  START;
  return NUMEXOAddress[Id];
  END;
}

UShort_t Map::GetNUMEXONumber(const UShort_t& key)
{
  START;
  return NUMEXODetNr[key];
  END;
}
void Map::RemoveNUMEXO(UShort_t key)
{
  START;
  
  if(NULL == NUMEXOAddress[key]){
	 char Error[256];
	 sprintf(Error,"The key \"%d\" was not assigned in the map while trying to delete it !",key);
	 MErr * Er = NULL;
	 Er = new(Er) MErr(WhoamI,0,0, Error);
	 throw Er;
  }
  END;
}

BaseDetector* Map::GetGETAddress(const UShort_t& Id)
{
  START;
  return GETAddress[Id];
  END;
}

UShort_t Map::GetGETNumber(const UShort_t& key)
{
  START;
  return GETDetNr[key];
  END;
}
void Map::RemoveGET(UShort_t key)
{
  START;
  
  if(NULL == GETAddress[key]){
	 char Error[256];
	 sprintf(Error,"The key \"%d\" was not assigned in the map while trying to delete it !",key);
	 MErr * Er = NULL;
	 Er = new(Er) MErr(WhoamI,0,0, Error);
	 throw Er;
  }
  END;
}

BaseDetector* Map::GetAddress(UShort_t Id)
{
  START;
  return Address[Id];
  END;
}
BaseDetector* Map::GetAddress(UShort_t *Data)
{
  START;
  if(*(Data+0) <  MaxNumberOfParameters)                                       
    {
#ifdef SWAP_DC
  // Swap Operation
  if(Address[*(Data+0)])
	 {
		if(MapOp[*(Data+0)])
		  {
			 MapOp[*(Data+0)]->Do(Data);
		  }
	 }
#endif
  return Address[*(Data+0)];
    } 
  else                                                                                                                        
      {
	cout << " ====================================== \n Trying to retrieve Address with Label " << *(Data+0) << " Error ! \n =========================================== \n" << endl;          return 0;                                                                                                           
      } 
  END;
}

UShort_t Map::GetNumber(UShort_t key)
{
  START;
  return DetNr[key];
  END;
}

void Map::Print()
{
  START;
  cout << "==========================================" << endl;
  cout << " Dump of the Parameters Map" << endl;
  cout << "==========================================" << endl;
  for(UShort_t i=0; i< MaxNumberOfParameters; i++){
	 if(Address[i] != NULL)
		cout << i <<  " (" << Address[i]->GetName()<< ") at address: " << Address[i] << " with internal ID : " << DetNr[i]  <<  endl;
  }
  if(MaxNUMEXONumberOfParameters)
  cout << "============= NUMEXO =====================" << endl;
 
    for(UShort_t i=0; i< MaxNUMEXONumberOfParameters; i++){
	 if(NUMEXOAddress[i] != NULL)
		cout << i <<  " (" << NUMEXOAddress[i]->GetName()<< ") at address: " << NUMEXOAddress[i] << " with internal ID : " << NUMEXODetNr[i]  <<  endl;
  }
  if(MaxGETNumberOfParameters)
  cout << "============= GET =====================" << endl;
 
    for(UShort_t i=0; i< MaxGETNumberOfParameters; i++){
	 if(GETAddress[i] != NULL)
	   cout << i <<  " (" << GETAddress[i]->GetName()<< ") at address: " << GETAddress[i] << " with internal ID : " << GETDetNr[i]  <<  endl;
  }
    cout << "============= MESYTEC =====================" << endl;

      for(UShort_t i=0; i< MaxMesytecNumberOfParameters; i++){
       if(MesytecAddress[i] != NULL)
         cout << i <<  " (" << MesytecAddress[i]->GetName()<< ") at address: " << MesytecAddress[i] << " with internal ID : " << MesytecDetNr[i]  <<  endl;
    }
  END;
}


void Map::DataOp(Parameters *Par)
{
 START;
#ifdef SWAP_DC
 
 MySwap = NULL;
 MySwap = new Swap();

 
 Char_t PName[20]; 
 
 
 // CD1 4-5 ch 32-64
 for(UShort_t i=4; i<6;++i)
	{
	  for(UShort_t j=32; j<64; j++)
		 {
			sprintf(PName,"CD1_%d_VOIE_%d",i,j);
			Short_t PId = -1;
			PId = Par->GetPar(PName);
			AddDataOp(PId,MySwap);
		 }	  
	}
 
 // CD2 1,4-5 32-64
 for(UShort_t i=1; i<6;++i)
	for(UShort_t j=32; j<64; j++)
	  {
		 if(i == 1 || i > 3)
		 sprintf(PName,"CD2_%d_VOIE_%d",i,j);
		 Short_t PId = -1;
		 PId = Par->GetPar(PName);
		 AddDataOp(PId,MySwap);
	  }
 	

#endif
 END;
}

#ifdef SWAP_DC
void Map::AddDataOp(UShort_t key,MDataOp* Op)
{
  START;
  MapOp[key] = Op;
  END;
}
#endif
