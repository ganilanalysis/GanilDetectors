/****************************************************************************
 *    Copyright (C) 2023 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "MesytecTopology.hh"

/**
 * @file   MesytecTopology.cc
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   Jan 2023
 * @version 0.1
 *
 * @brief  Class of To Handle Mesytec Topology Description File
 *
 *
 */

/**
 * Constructor
 */
MesytecTopology *MesytecTopology::instance = NULL;

MesytecTopology::MesytecTopology()
{
    START;
    MP = nullptr;
    MP = MesytecParameters::getInstance();

    END;
}


MesytecTopology::~MesytecTopology()  noexcept(false)
{
    START;
    MP = CleanDelete(MP);
    instance = nullptr;
    END;
}

MesytecTopology* MesytecTopology::getInstance()
{
    START;
    // A new instance of RootIMPut is created if it does not exist:
    if (instance == NULL) {
        instance = new MesytecTopology();
        cout << "New instance of Mesytec topology " << instance << endl;

    }
    else
    {
        cout << "Foud existing instance of Mesytec topology " << instance << endl;


    }

    // The instance of RootIMPut is returned:
    return instance;
    END;
}

void MesytecTopology::Init(const Char_t * ConfFileName)
{
    START;
    cout << "Reading Mesytec Topology from " << ConfFileName << endl;

    Char_t Line[255];
    Char_t PName[300];
    UShort_t Len = 255;
    UShort_t fLine = 0;
    Char_t * Ptr = NULL;
    MIFile IF(ConfFileName);
    Char_t val[255];
    Int_t Ch;

    Char_t bname[255];
    Char_t btype[255];
    Int_t  bAddr;
    Int_t  bus;
    Int_t  NCh;
    Int_t  NActiveCh;
    Int_t  NSamples;
    Int_t  StartSampleNr;
    Int_t  EndSampleNr;




    while(IF.GetLine(Line,Len)){
        if( (Ptr = strstr(Line,"//")))
        {
            cout << Line << endl;
        }
        else
        {
            if(strcmp(Line, "****MesytecBoard****") == 0)
            {

                NActiveCh = -1;
                NCh = -1;
                NSamples = -1;
                bAddr= -1;
                StartSampleNr = -1;
                EndSampleNr = -1;

                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"Name : %s",bname) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s is corrupted expecting Name : ",ConfFileName);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"Type : %s",btype) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s is corrupted (%s):  expecting Type : ",ConfFileName,bname);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"Address : %x",&bAddr) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s is corrupted (%s): expecting Address :",ConfFileName,bname);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }

#ifdef DEBUG
                                cout << std::dec << "--------------------------------"<<endl;
                                cout << "Mesytec Board Config" << endl;
                                cout << "--------------------------------"<<endl;
                                cout << " Found Board : " << bname << endl;
                                cout << "    type : " << btype << endl;
                                cout << "    Address : " << bAddr << endl;
#endif







                if(strcmp(btype,"MDPP") == 0 || strcmp(btype,"MTDC") == 0)
                {
                    cout << "Track " << btype << " type " << endl;
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s): expecting Channels :",ConfFileName,bname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    IF.GetLine(Line,Len);
                    if( (! (sscanf(Line,"Active Channels  : %d",&NActiveCh) == 1)))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s): expecting Active Channels :",ConfFileName,bname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
#ifdef DEBUG
                    cout << "    Channels : " << NCh << endl;
                    cout << "    Act. Chan. : " << NActiveCh << endl;
                    cout << "    Parameter Mapping : " << endl;
#endif
                    for(UShort_t i=0; i < NActiveCh; i++)
                    {
                        IF.GetLine(Line,Len);
                        if( (! (sscanf(Line,"Ch %d : %s",&Ch,PName) == 2)))
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting Ch00 : ParName",ConfFileName,bname);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        else{
#ifdef DEBUG
                            cout << "Chan " << i << "->" << Ch << " " <<  PName  << endl;
#endif
                            MP->Add(bAddr,Ch,-1,PName);
                        }

                    }

                }
                else if(strcmp(btype,"MMR") == 0)
                {
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Bus : %d",&bus) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s): expecting Bus :",ConfFileName,bname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s): expecting Channels :",ConfFileName,bname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                    IF.GetLine(Line,Len);
                    if( (! (sscanf(Line,"Active Channels  : %d",&NActiveCh) == 1)))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s): expecting Active Channels :",ConfFileName,bname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
#ifdef DEBUG
                    cout << "    Channels : " << NCh << endl;
                    cout << "    Bus : " << bus << endl;
                    cout << "    Act. Chan. : " << NActiveCh << endl;
                    cout << "    Parameter Mapping : " << endl;
#endif
                    for(UShort_t i=0; i < NActiveCh; i++)
                    {
                        IF.GetLine(Line,Len);
                        if( (! (sscanf(Line,"Ch %03d : %s",&Ch,PName) == 2)))
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting Ch %03d : ParName",ConfFileName,bname,i);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        else{
#ifdef DEBUG
                            cout << "Bus " << bus << " Chan " << i << "->" << Ch << " " <<  PName  << endl;
#endif
                            MP->Add(bAddr,bus,Ch,PName);
                        }

                    }
                }
                else if(strcmp(btype,"VMMR") == 0)
                {
                    //    cout << "VMMR type" << endl;


//                    IF.GetLine(Line,Len);
//                    if( ! (sscanf(Line,"Active Channels  : %d",&NActiveCh) == 1))
//                    {
//                        Char_t Message[500];
//                        sprintf(Message,"Config File %s is corrupted (%s): expecting Active Channels :",ConfFileName,bname);
//                        MErr * Er= new MErr(WhoamI,0,0, Message);
//                        throw Er;
//                    }
//                    cout << "    Act. Chan. : " << NActiveCh << endl;
                    cout << "    Parameter Mapping : " << endl;

                    for(UShort_t i=0; i < NCh; i++)
                    {
                        for(Int_t k=0; k<NSamples; k++)
                        {
                            sprintf(PName,"%s_bus%02d_ch%03d",bname,i,k);
                            cout << "Bus " << i << " / Chan " <<k << " -> " << PName << endl;

                            MP->Add(bAddr,i,k,PName);

                        }

                    }


                }


            }

        }
    }

#ifdef DEBUG
    MP->Print();
#endif

    END;
}

