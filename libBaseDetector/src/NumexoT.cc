/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *   
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "NumexoT.hh"
NumexoT::NumexoT(const Char_t *Name,
        UShort_t  NumberOfDetectors,
        Bool_t RawData, 
         Bool_t CalData, 
         Bool_t DefaultCalibration, 
         const Char_t *NameExt,
         Bool_t HasTimeStamp)
  : BaseDetector(Name, 1 , false, CalData, DefaultCalibration,false, NameExt, HasTimeStamp)
{
  START;
  NumberOfSubDetectors = NumberOfDetectors;
  fRawDataSubDetectors  = RawData;
  AllocateComponents();

  if(fCalData)
    {
         sprintf(CalNameI[0],"%s_TCal", DetectorName);
        SetGateCal(-2000,2000,0);
    
   
    }
//  ReadCalibration();

#ifdef WITH_ROOT
  // Setup Histogram Hierarchy
  SetHistogramsCal1DFolder("NumexoT1D");
  SetHistogramsCal2DFolder("NumexoT2D");
#endif

  END;
}
NumexoT::~NumexoT(void)
{
  START;
  

  END;
}



void NumexoT::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = false;
  Bool_t PosInfo = false;
 
  string BaseName;
  string Name;
  
  // NumexoT1X 
  BaseName = DetectorName ;
  Name = BaseName + "";
  BaseDetector *NumexoTX = new BaseDetector(Name.c_str(),NumberOfDetectors,RawData,CalData,DefCal,PosInfo,"",true,true);
  if(RawData)
    {
      NumexoTX->SetRawHistogramsParams(500,0,32000,"");
    }
  if(CalData)
    {
      NumexoTX->SetGateCal(0,32768);
        NumexoTX->SetCalHistogramsParams(500,0,32000,"");
    }
  AddComponent(NumexoTX);
  END;
}

void NumexoT::SetParameters(NUMEXOParameters * PL_NUMEX,Map* Map)
{ 
  START;
  Char_t Name[100];

  if(isComposite)
    {
      Char_t PName[20];
      
      for(UShort_t i=0; i < 1; i++)
        for(UShort_t j=0; j < DetList->at(i)->GetNumberOfDetectors(); j++)
          {
                Int_t Board=0;
                Int_t Channel=0;
                Int_t Sample = 0; 


                // Specific for e667 Cbled 
                if(i == 0)
                  {
                    Channel = 11 ;  // b_160_c11_s0-95
                    Sample = j;
                    Board = 161; // e667
                    sprintf(Name, "DC_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
                    DetList->at(i)->SetParameterName(Name, j);
                  }
                DetList->at(i)->SetParameterName(Name, j);
                
          }
      
      for(UShort_t i=0; i< DetList->size(); i++)
        if(DetList->at(i)->HasRawData())
          {       
            NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
            DetList->at(i)->SetParametersNUMEXO( PL_NUMEX , Map);
            
          }
    }
  
  END;
}

Bool_t NumexoT::Treat(void)
{
  START;
  Ctr->at(0)++;
  if(isComposite)
    {
      if(fCalData)
        {
         isPresent = true;
        }
      
    }
  return isPresent;
  END;
}


#ifdef WITH_ROOT

void NumexoT::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
  // Set histogram Hierarchy
    SetMainHistogramFolder("");

  SetHistogramsCal1DFolder("NumexoT1D");
  SetHistogramsCal2DFolder("NumexoT2D");
 
  for(UShort_t i = 0;i<DetList->size();i++)
	 {
		DetList->at(i)->SetMainHistogramFolder("");
		DetList->at(i)->SetHistogramsRaw1DFolder("NumexoT1D");
		DetList->at(i)->SetHistogramsRaw2DFolder("NumexoT2D");
	 	DetList->at(i)->SetHistogramsCal1DFolder("NumexoT1D");
		DetList->at(i)->SetHistogramsCal2DFolder("NumexoT2D");
	 }

  if(fMode == 0)
    {
      if(fRawData)
        {
          SetHistogramsRaw(true);
          // SetOutAttachRawV(true);
        }
      if(fCalData)
        {
          SetHistogramsCal(true);
          // SetOutAttachCalI(true);
        }
      for(UShort_t i = 0;i<DetList->size();i++)
        {
          if(fRawDataSubDetectors)
            {
              DetList->at(i)->SetHistogramsRaw2D(true);
              DetList->at(i)->SetHistogramsRawSummary(true);
            }
          DetList->at(i)->SetHistogramsCal1D(false);
          DetList->at(i)->SetHistogramsCal2D(true);
          DetList->at(i)->SetHistogramsCalSummary(true);
        }
      if(fRawDataSubDetectors)
        {
          // DetList->at(2)->SetHistogramsRaw(true);
          // DetList->at(2)->SetOutAttachRawI(true);
        }
      // DetList->at(2)->SetHistogramsCal(true);
      // DetList->at(2)->SetOutAttachCalI(true);
	
      // OutAttach(OutTTree);
      // 	for(UShort_t i = 0;i<DetList->size();i++)
      // 	  DetList->at(i)->OutAttach(OutTTree);

    }
  else if(fMode == MODE_D2R)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
        {
          DetList->at(i)->SetHistogramsRaw2D(true);
          DetList->at(i)->SetHistogramsRawSummary(true);
          DetList->at(i)->SetOutAttachRawV(true);
          if(DetList->at(i)->HasTS())
              DetList->at(i)->SetOutAttachTS(true);
        }
      
      for(UShort_t i = 0;i<DetList->size();i++)
      {
          DetList->at(i)->OutAttach(OutTTree);
        DetList->at(i)->PrintOptions(std::cout);
    
      }
      OutAttach(OutTTree);
	
    }
  else if(fMode == MODE_D2A)
	 {
	   if(fRawData)
	     {
	       SetHistogramsRaw(true);
	       SetOutAttachRawV(true);
	     }
	   if(fCalData)
	     {
	       SetHistogramsCal(true);
	       SetOutAttachCalI(true);
	     }
		
	   for(UShort_t i = 0;i<DetList->size();i++)
	     {
	       if(fRawDataSubDetectors)
            {
              DetList->at(i)->SetHistogramsRaw1D(false);
              DetList->at(i)->SetHistogramsRaw2D(true);
              DetList->at(i)->SetHistogramsRawSummary(true);
              // No Charges
              DetList->at(i)->SetOutAttachRawV(true);
                if(DetList->at(i)->HasTS())
                  DetList->at(i)->SetOutAttachTS(true);

            }
	       DetList->at(i)->SetHistogramsCal1D(false);
	       DetList->at(i)->SetHistogramsCal2D(true);
	       DetList->at(i)->SetHistogramsCalSummary(true);
	       DetList->at(i)->SetOutAttachCalI(false);
          // No Charges
	       DetList->at(i)->SetOutAttachCalV(true);			 
	     }
	   if(fRawDataSubDetectors)
	     {
	       // DetList->at(2)->SetHistogramsRaw(false);
	       // DetList->at(2)->SetOutAttachRawI(false);
	     }
		OutAttach(OutTTree);
		for(UShort_t i = 0;i<DetList->size();i++)
		  DetList->at(i)->OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
      SetInAttachRawV(true);
      InAttach(InTTree);
		
      SetOutAttachCalI(true);
      OutAttach(OutTTree);
		
    }
  else if(fMode == MODE_RECAL)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(fRawDataSubDetectors)
            {
              // No Charges
              DetList->at(i)->SetInAttachRawV(false);
            }
	  // No Charges
	  if(DetList->at(i)->HasCalData())
	    DetList->at(i)->SetInAttachCalV(true);			 
	  DetList->at(i)->InAttachCal(InTTree);
	}
      SetInAttachCalI(true);
      InAttachCal(InTTree);


      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	}

      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(fRawDataSubDetectors)
            {
              DetList->at(i)->SetHistogramsRaw1D(false);
              DetList->at(i)->SetHistogramsRaw2D(true);
              DetList->at(i)->SetHistogramsRawSummary(true);
              // No Charges
              DetList->at(i)->SetOutAttachRawV(false);
            }
	  DetList->at(i)->SetHistogramsCal1D(false);
	  DetList->at(i)->SetHistogramsCal2D(true);
	  DetList->at(i)->SetHistogramsCalSummary(true);
	  DetList->at(i)->SetOutAttachCalI(false);
          // No Charges
	  DetList->at(i)->SetOutAttachCalV(true);			 
	}
      if(fRawDataSubDetectors)
	{
	  // DetList->at(2)->SetHistogramsRaw(false);
	  // DetList->at(2)->SetOutAttachRawI(false);
	}
      OutAttach(OutTTree);
      for(UShort_t i = 0;i<DetList->size();i++)
      {
          DetList->at(i)->OutAttach(OutTTree);
      }
      
    }
  else 
    {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
    }



  END;
}



#endif
