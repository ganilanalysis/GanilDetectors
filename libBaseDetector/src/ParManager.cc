/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "ParManager.hh"
/**
 * @file   ParManager.cc
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   July 2013
 * 
 */

/** 
 * Constructor
 */
ParManager::ParManager(UShort_t MaxPar, MOFile *Log)
{
  START;
  MaxParameters = MaxPar;
  SetPointers();
  L = Log;
  AllocateArrays();
  Clear();
  END;
}

/** 
 * Destructor
 */
ParManager::~ParManager(void) noexcept(false)
{
  START;
  PName = FreeDynamicArray<string>(PName);
  PType = FreeDynamicArray<UShort_t>(PType);
  PDataPtr = FreeDynamicArray<void*>(PDataPtr);
 END;
}

/** 
 * Force pointers to NULL when constructing the class
 */
void ParManager::SetPointers(void)
{ 
  START;
  // Log File
  L = NULL;
  // Parameters Names
  PName = NULL;
  // Parameters Size
  PType = NULL;
  // Data Ptr
  PDataPtr = NULL;
  END;
}

/** 
 * Allocate Parameters Arrays
 */
void ParManager::AllocateArrays()
{ 
  START;
  // Parameter Names
  PName = AllocateDynamicArray<string>(MaxParameters);
  // Parameter Size
  PType = AllocateDynamicArray<UShort_t>(MaxParameters);
  // DataPtr Size
  PDataPtr = AllocateDynamicArray<void*>(MaxParameters);
  
  END;
}

/** 
 * Clear the parameter arrays
 */
void ParManager::Clear(void)
{
  START;
  NParameters = 0;
  for (UShort_t i=0; i<MAX_PAR; i++)
	 {
		PName[i] = "";
		PType[i] = 0;
      PDataPtr[i] = NULL;
	 }
  END;
}

/** 
 * Insert a parameter in the list
 */
void ParManager::Add(string name, UShort_t id, UShort_t *ptr,  UShort_t type)
{
  START;
  
  if(NParameters < MAX_PAR) 
	 {
		PName[NParameters] = name;
		PType[NParameters] = type;
		PDataPtr[NParameters] = ptr;
      // cout << "Adding Short ParamId : " << NParameters << " - Name : " << PName[NParameters] << " \t (" << 	PType[NParameters] << " ) Adress " << PDataPtr[NParameters]<< endl;
      NParameters++;
	 }
  else
	 {
		char Error[256];
		sprintf(Error,"Trying to add a parameter to Parameter Manager with ID (%d) is larger than MAX_PAR (%d) !",id,MAX_PAR);
		MErr * Er = NULL;
		Er = new(Er) MErr(WhoamI,0,0, Error);
		throw Er;
	 }
  END;
}

/** 
 * Insert a parameter in the list
 */
void ParManager::Add(string name, UShort_t id, Float_t *ptr,  UShort_t type)
{
  START;
  
  if(NParameters < MAX_PAR) 
	 {
		PName[NParameters] = name;
		PType[NParameters] = type;
		PDataPtr[NParameters] = ptr;
      // cout << "Adding Float ParamId : " << NParameters << " - Name : " << PName[NParameters] << " \t (" << 	PType[NParameters] << " ) Adress " << PDataPtr[NParameters]<< endl;
      NParameters++;
	 }
  else
	 {
		char Error[256];
		sprintf(Error,"Trying to add a parameter to Parameter Manager with ID (%d) is larger than MAX_PAR (%d) !",id,MAX_PAR);
		MErr * Er = NULL;
		Er = new(Er) MErr(WhoamI,0,0, Error);
		throw Er;
	 }
  END;
}
/** 
 * Insert a parameter in the list
 */
void ParManager::Add(string name, UShort_t id, Double_t *ptr,  UShort_t type)
{
  START;
  
  if(NParameters < MAX_PAR) 
	 {
		PName[NParameters] = name;
		PType[NParameters] = type;
		PDataPtr[NParameters] = ptr;
      // cout << "Adding Double ParamId : " << NParameters << " - Name : " << PName[NParameters] << " \t (" << 	PType[NParameters] << " ) Adress " << PDataPtr[NParameters]<< endl;
      NParameters++;
	 }
  else
	 {
		char Error[256];
		sprintf(Error,"Trying to add a parameter to Parameter Manager with ID (%d) is larger than MAX_PAR (%d) !",id,MAX_PAR);
		MErr * Er = NULL;
		Er = new(Er) MErr(WhoamI,0,0, Error);
		throw Er;
	 }
  END;
}

/** 
 * Remove a parameter from the list using provided parameter id
 */
void ParManager::Remove(UShort_t Pid)
{
  START;
  PName[Pid] = "";
  PType[Pid] = 0;
  PDataPtr[Pid] = NULL;
  END;
}

// /** 
//  * Get Parameter Id from Name
//  * Return 0 if not found
//  */
// UShort_t ParManager::GetPar(string name)
// {
//   START;
//   for (UShort_t i=0; i<MAX_PAR; i++)
// 	 {
// 		if(PType[i]>0)
// 		  if(strcmp(PName[i].c_str(),name.c_str()) == 0)
// 			 return i;
// 	 }
//   //  Could not find parameter
//   char Error[256];
//   sprintf(Error,"Could not find  parameter (%s) in parameter list !",name.c_str());
//   MErr * Er = NULL;
//   Er = new(Er) MErr(WhoamI,0,0, Error);
//   throw Er;
//   return 0;
//   END;
// }


/** 
 * Print a dump of the parameters Map
 */
void ParManager::Print(void)
{
  START;
  cout << "==========================================" << endl;
  cout << " Dump of the Parameters List" << endl;
  cout << "==========================================" << endl;
  for (UShort_t i=0; i<MAX_PAR; i++)
	 {
		if(PType[i] > 0)
		  cout << "ParamId : " << i << " - Name : " << PName[i] << " \t Type : " << PType[i] << " \t Address " << PDataPtr[i]<< endl;
	 }
  cout << " Number of Parameters : " << GetNParameters() << endl; 
 
  END;
}



