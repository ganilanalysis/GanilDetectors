/****************************************************************************
 *    Copyright (C) 2012-2019 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Diego Ramos, diego.ramos@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "GETTopology.hh"
/**
 * @file   GETTopologu.cc
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   December 2021
 * @version 0.1
 *
 * @brief  Class of To Handle GET Topology Description File
 *
 *
 */

/**
 * Constructor
 */
GETTopology *GETTopology::instance = NULL;

GETTopology::GETTopology()
{
    START;
    NP = nullptr;
    NP = GETParameters::getInstance();

    END;
}


GETTopology::~GETTopology()
{
    START;
    NP = CleanDelete(NP);
    instance = nullptr;
    END;
}

GETTopology* GETTopology::getInstance()
{
    START;
    // A new instance of RootInput is created if it does not exist:
    if (instance == NULL) {
        instance = new GETTopology();
        cout << "New instance of GET topology " << instance << endl;

    }
    else
    {
        cout << "Foud existing instance of GET topology " << instance << endl;


    }

    // The instance of RootInput is returned:
    return instance;
    END;
}

void GETTopology::Init(const Char_t * ConfFileName, const Char_t * ConfFilePath)
{
    START;
    cout << "Reading GET Topology from " << ConfFileName << endl;

    Char_t Line[255];
    Char_t PName[255];
    UShort_t Len = 255;
    UShort_t fLine = 0;
    Char_t * Ptr = NULL;
    MIFile IF(ConfFileName);
    Char_t val[255];
    Int_t Ch;

    Char_t bname[255];
    Int_t  NCh;
    Int_t  NSamples;
    Char_t tfname[255];
    Char_t tfpath[255];
    Int_t DetCh;
    Int_t CoboID;
    Int_t AsadID;
    Int_t AgetID;
    Int_t GetCh;
    Int_t ChCounter;

    while(IF.GetLine(Line,Len)){
        if( Ptr = strstr(Line,"//"))
        {
            cout << Line << endl;
        }
        else
        {
            if(strcmp(Line, "****GETDetector****") == 0)
            {
                NCh = -1;
                NSamples = -1;  

                IF.GetLine(Line,Len);
                if( ! sscanf(Line,"Name : %s",&bname) == 1)
                {
                    Char_t Message[200];
                    sprintf(Message,"Config File %s is corrupted expecting Name : ",ConfFileName);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                IF.GetLine(Line,Len);
                if( ! sscanf(Line,"Channels : %d",&NCh) == 1)
                {
                    Char_t Message[200];
                    sprintf(Message,"Config File %s is corrupted (%s): expecting Channels :",ConfFileName,bname);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                IF.GetLine(Line,Len);
                if( ! sscanf(Line,"Samples : %d",&NSamples) == 1)
                {
                    Char_t Message[200];
                    sprintf(Message,"Config File %s is corrupted (%s):  expecting Samples :",ConfFileName,bname);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }  
		IF.GetLine(Line,Len);
                if( ! sscanf(Line,"ChaTopoFile : %s",&tfname) == 1)
                {
                    Char_t Message[200];
                    sprintf(Message,"Config File %s is corrupted expecting ChaTopoFile : ",ConfFileName);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
 
#ifdef DEBUG
                cout << "--------------------------------"<<endl;
                cout << "GET Topology Config" << endl;
                cout << "--------------------------------"<<endl;
                cout << " Found Detector : " << bname << endl;
                cout << "    NChannels : " << NCh << endl;
                cout << "    NSamples  : " << NSamples << endl;
		cout << "ChannelsTopolgyFile : " << tfname << endl;
#endif
		sprintf(tfpath,"%s/%s",ConfFilePath,tfname);
                if(NSamples == -1  )
		  {
		    ChCounter=0;
		    MIFile IF2(tfpath);
		    while(IF2.GetLine(Line,Len))
		      {
			
			DetCh = -1;
			CoboID = -1;
			AsadID = -1;
			AgetID =-1 ;
			GetCh = -1;

			if( Ptr = strstr(Line,"//"))
			  {
			    cout << Line << endl;
			  }
			else
			  { 
			    ChCounter++;
			    if( (! (sscanf(Line,"%d %d %d %d %d",&DetCh,&CoboID,&AsadID,&AgetID,&GetCh) == 5))) 
			      {
				Char_t Message[200];
				sprintf(Message,"Config File %s is corrupted : expecting 5 columns:[DetChannel,CoboID,AsadID,AgetID,GetCh] found: %d ",tfname,sscanf(Line,"%d %d %d %d %d",&DetCh,&CoboID,&AsadID,&AgetID,&GetCh));
				MErr * Er= new MErr(WhoamI,0,0, Message);
				throw Er;
			      }
			    sprintf(PName,"%s_%02d",bname,DetCh);
			    NP->Add(CoboID, AsadID, AgetID, GetCh,NSamples,PName);
			    //#ifdef DEBUG			    
			    cout<<"Cobo: "<<CoboID<<" Asad: "<<AsadID<<" Aget: "<<AgetID<<" AgetChannel: "<<GetCh<<" PName: "<<PName<<endl;
			    //#endif
			  }
		      }
		    if(ChCounter!=NCh)
		      {
			Char_t Message[200];
			sprintf(Message,"Config File %s is corrupted : expecting %d columns : found: %d ",tfname,NCh,ChCounter);
			MErr * Er= new MErr(WhoamI,0,0, Message);
			throw Er;
		      }
		  }
	    }
	    
        }
    }
    
#ifdef DEBUG
    NP->Print();
#endif
    
    END;
}

