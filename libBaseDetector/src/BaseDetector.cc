/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "BaseDetector.hh"

BaseDetector::BaseDetector(const Char_t *Name, UShort_t NDetectors, Bool_t RawData, Bool_t CalData, Bool_t DefaultCalibration, Bool_t PositionInfo, const Char_t *NameExt,Bool_t ScalingFact,Bool_t HasTimeStamped, UShort_t MaxMult, Bool_t HasTime)
{
    START;
    VerboseLevel=V_ERROR;

    fPosInfo = PositionInfo;
    strncpy(DetectorName,Name,100);
    strncpy(DetectorNameExt,NameExt,100);

    if(HasSpace(DetectorName) || HasSpace(DetectorNameExt))
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Detector Name must not contain space", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    NumberOfDetectors = NDetectors;
    fRawData = RawData;
    fHasScalingCalib = ScalingFact;
    fCalData = CalData;
    HasCalCharges(false);
    fActiveArea = false;

    fHasTimeStamp = HasTimeStamped;
    fHasTime = HasTime;
    fMaxMult = MaxMult;
    fMaxMultSubDetectors = MaxMult;


    if(fMaxMult < 1)
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Detector Name Error on Max multiplicity provided %d", DetectorName, DetectorNameExt, fMaxMult);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    if(fMaxMult>1)
        cout << " <"<< DetectorName <<"><"<<DetectorNameExt << "> Detector With Multiplicity Handling with MaxMult =" << fMaxMult  << endl;


    fRawName = false;
    fRawGate = false;
    fCalGate = false;

    if(RawData)
    {
        sprintf(OutRawFName,"%s%sRaw",DetectorName,DetectorNameExt);
    }


    if(fCalData)
    {
        sprintf(OutCalFName,"%s%s",DetectorName,DetectorNameExt);
        sprintf(OutCalVName,"%s%sV",DetectorName,DetectorNameExt);
    }

    LogManager *LM = LogManager::getInstance();

    L = NULL;
    L = LM->GetFilePtr();

    // Get Environment Instance
    EM = EnvManager::getInstance();
    if(!EM->isSetup())
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Environment Manager Path is not setup ", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;

    }

    // Random Generator
    Rnd = NULL;
    Rnd = new MRandom();

    SetPointers();

    // Allocate SubComponent
    AllocateComponents();
    AllocateCounters();
    AddCounter("Called ");
    AddCounter("Present");
    AllocateArrays();

    if(fRawData && fCalData)
        InitializeCalibration();

    if(fRawData)
        SetParameterName("%s_%02d");

    if(fRawData)
        SetGateRaw(0);


    sprintf(MainHistogramFolder,"%s%s",DetectorName,DetectorNameExt);
#ifdef WITH_ROOT
    H=NULL;
    SetInAttach();
    SetOutAttach();
    SetHistograms();
#endif

    Reset();

    if(DefaultCalibration)
        ReadDefaultCalibration();
    else
        ReadCalibration();

    if(VerboseLevel >= V_INFO)
        cout << "<" << DetectorName << "><" << DetectorNameExt << ">initialized" << endl;
    L->File << "<" << DetectorName << "><" << DetectorNameExt << ">initialized" << endl;

    if(fPosInfo)
        ReadDefaultActiveArea();
    else
        ReadPosition();

    DetManager *DM = DetManager::getInstance();
    DM->RegisterDetector((BaseDetector*)this,GetName());

    END;
}



BaseDetector::~BaseDetector()noexcept(false)
{
    START;
    PrintCounters();
    Clean();
    DetManager *DM = DetManager::getInstance();
    DM->UnRegisterDetector(this);
    END;
}



void BaseDetector::Clean(void)
{
    START;
    // Clear Sub-component first
    if(isComposite){
        for(UShort_t i=0; i< DetList->size(); i++){
            DetList->at(i) = CleanDelete(DetList->at(i));
        }
        DetList = CleanDelete(DetList);
        isComposite = false;
    }

    Rnd = CleanDelete(Rnd);
#ifdef WITH_ROOT
    DeleteHistograms();
#endif
    DeAllocateArrays();
    DeAllocateCounters();
    END;
}

void BaseDetector::Reset(void)
{
    START;
    if(isComposite){
        for(UShort_t i=0; i< DetList->size(); i++)
            DetList->at(i)->Reset();
    }

    Clear();
#ifdef WITH_ROOT
    EmptyHistograms();
#endif
    RawDuplicateLabel = 0;
    ResetCounters();
    END;
}

void BaseDetector::SetPointers(void)
{
    START;
    // SubComponents List
    isComposite = false;
    DetList = NULL;
    NumberOfSubDetectors = 0;
    fRawDataSubDetectors = false;
    // Setup Mode of the detector
    fMode = -1;


    // Calibration Mode
    CalibrationMode = false;
    char *Cal_EnvVar;
    Char_t Message[1000];
    Cal_EnvVar = getenv("VAMOS_CALMODE");
    if(Cal_EnvVar != NULL && (strcmp(Cal_EnvVar,"1") == 0))
    {
        CalibrationMode = true;
        sprintf(Message,"In <%s><%s> the calibration mode activated", DetectorName, DetectorNameExt);
        cout << Message << endl;
        // MErr * Er= new MErr(WhoamI,0,0, Message);
        // throw Er;
    }

    // Counters
    Ctr = NULL;
    CtrLbl = NULL;

    // Arrays
    RawM = 0;
    RawNameI = NULL;
    RawTreeNameI = NULL;
    Raw = NULL;
    RawV = NULL;
    RawNr = NULL;
    RawGates = NULL;

    CalM = 0;
    CalNameI = NULL;
    Cal = NULL;
    CalV = NULL;
    CalNr = NULL;
    CalGates = NULL;

    // Focal Plane Position
    hasValidTrack = false;

#ifdef WITH_ROOT
    fOutAttachCalF = false;
    fOutAttachCalV = false;
    fOutAttachCalI = false;
    fOutAttachRawF = false;
    fOutAttachRawV = false;
    fOutAttachRawI = false;
    fOutAttachTS = false;
    fOutAttachCalTS = false;
    HCal1D = NULL;
    HCalMap1D = NULL;
    HRaw1D = NULL;
    HRawMap1D = NULL;
    HCal2D = NULL;
    HCalMap2D = NULL;
    HRawMap2D = NULL;
    HRaw2D = NULL;
#endif

    END;
}
void BaseDetector::SetAnalysisMode(Short_t Mode)
{
    START;
    if(isComposite){
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->SetAnalysisMode(Mode);
        }
    }
    fMode = Mode;
    END;
}


void BaseDetector::SetNoOutput()
{
    START;
    if(fRawData)
    {
        SetHistogramsRaw(false);
        SetOutAttachRawV(false);
        SetOutAttachRawI(false);
        SetOutAttachRawF(false);

    }

    if(fHasTimeStamp)
    {
        SetOutAttachTS(false);
        SetOutAttachCalTS(false);
    }
    if(fHasTime)
    {
        SetOutAttachTime(false);
    }
    if(fCalData)
    {
        SetHistogramsCal(false);
        SetOutAttachCalF(false);
        SetOutAttachCalV(false);
        SetOutAttachCalI(false);
    }
    END;
}


void BaseDetector::SetParameters(Parameters* PL, Map* Map)
{
    START;
    UShort_t Label = 0;
    if(fMode != MODE_R2A && fMode != MODE_RECAL && fMode > -1)
    {
        if(fRawName && fRawData)
            for(UShort_t i=0;i<NumberOfDetectors;i++)
            {
                //cout << "Asking for " << RawNameI[i] << endl;
                if((Label=PL->GetPar(RawNameI[i])))
                {
                    //  cout << "Receiving " << Label << endl;
                    Map->Add(Label, this,i);
                }
                else
                {
                    cout << "Parameter " << RawNameI[i] << " not found" << endl;
                    L->File << "Parameter " << RawNameI[i] << " not found" << endl;
                }
            }
    }
    END;
}

void BaseDetector::SetParameters(NUMEXOParameters* PL, Map* Map)
{
    START;
    SetParametersNUMEXO(PL,Map);
    END;
}

void BaseDetector::SetParameters(GETParameters* PL, Map* Map)
{
    START;
    SetParametersGET(PL,Map);
    END;
}

void BaseDetector::SetParameters(MesytecParameters* PL, Map* Map)
{
    START;
    SetParametersMesytec(PL,Map);
    END;
}

void BaseDetector::SetParametersNUMEXO(NUMEXOParameters* PL, Map* Map)
{
    START;
    Int_t Label = 0;
    if(fMode != MODE_R2A && fMode != MODE_RECAL && fMode > -1)
    {
        if(fRawName && fRawData)
            for(UShort_t i=0;i<NumberOfDetectors;i++)
            {
                // cout << "Asking for " << RawNameI[i] << endl;
                if((Label=PL->GetPar(RawNameI[i]))>-1)
                {

                    Map->AddNUMEXO(Label, this,i);
                }
                else
                {
                    cout << "NUMEXO Parameter " << RawNameI[i] << " not found" << endl;
                    L->File << "NUMEXO Parameter " << RawNameI[i] << " not found" << endl;
                }
            }
    }
    END;
}

void BaseDetector::SetParametersGET(GETParameters* PL, Map* Map)
{
    START;
    UShort_t Label = 0;
    if(fMode != MODE_R2A && fMode != MODE_RECAL && fMode > -1)
    {
        if(fRawName && fRawData)
            for(UShort_t i=0;i<NumberOfDetectors;i++)
            {
                //cout << "Asking for " << RawNameI[i] << endl;

                if((Label=PL->GetPar(RawNameI[i]))>-1)
                {
                    Map->AddGET(Label, this,i);
                }
                else
                {
                    cout << "GET Parameter " << RawNameI[i] << " not found" << endl;
                    L->File << "GET Parameter " << RawNameI[i] << " not found" << endl;
                }
            }
    }
    END;
}

void BaseDetector::SetParametersMesytec(MesytecParameters* PL, Map* Map)
{
    START;
    Int_t Label = 0;
    if(fMode != MODE_R2A && fMode != MODE_RECAL && fMode > -1)
    {
        if(fRawName && fRawData)
            for(UShort_t i=0;i<NumberOfDetectors;i++)
            {
                if((Label=PL->GetPar(RawNameI[i]))>-1)
                {
                    Map->AddMesytec(Label, this,i);
                }
                else
                {
                    cout << "Mesytec Parameter " << RawNameI[i] << " not found" << endl;
                    L->File << "Mesytec Parameter " << RawNameI[i] << " not found" << endl;
                }
            }
    }
    END;
}

void BaseDetector::AddParam(Parameters* Par,Map* Map, string Pname, UShort_t Uid)
{
    START;
    UShort_t PId = 0;
    PId = Par->GetPar(Pname);
    if(PId > 0){
        // Set the Map
        Map->Add(PId,this,Uid);
    }
    else {
        char Error[256];
        sprintf(Error,"   /!\\ Warning (in detector \"%s\") : could not find  parameter (%s) in parameter list!",DetectorName,Pname.c_str());
        cout << Error << endl;
    }
    END;
}

void BaseDetector::AddParRange(Parameters* Par,Map* Map, string Pat, UShort_t i_from, UShort_t i_to,const char *format)
{
    START;
    char name[100];
    for(UShort_t i=i_from; i< i_to; ++i){
        sprintf(name, format, Pat.c_str(),i);
        AddParam(Par,Map,name,i);
    }
    END;
}

void BaseDetector::AllocateComponents(void)
{
    START;

    END;
}

void BaseDetector::AddComponent(BaseDetector* Det)
{
    START;
    if(!isComposite){
        DetList = new vector<BaseDetector*>();
        isComposite = true;
    }
    DetList->push_back(Det);
    END;
}

void BaseDetector::InitializeCalibration(void)
{
    START;
    if(fRawData)
        if(fCalData)
        {
            for(UShort_t i =0; i<NumberOfDetectors;i++)
            {
                Coeffs[i][0] = Coeffs[i][2] = 0.0;
                Coeffs[i][1] = 1.0;
                if(fHasScalingCalib)
                    Coeffs[i][3] = 1.0;

            }
        }
        else
        {
            Char_t Message[500];
            sprintf(Message,"<%s><%s> Trying to set Calibration where there is no Cal Data", DetectorName, DetectorNameExt);
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s>Trying to set Calibration where there is no Raw Data", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

// void BaseDetector::SetPosition(Float_t xmin, Float_t ymin, Float_t xmax, Float_t ymax)
// {
//   START;
//   if(!XMin) XMin = AllocateDynamicArray<Float_t>(NumberOfDetectors);
//   if(!XMax) XMax = AllocateDynamicArray<Float_t>(NumberOfDetectors);
//   if(!YMin) YMin = AllocateDynamicArray<Float_t>(NumberOfDetectors);
//   if(!YMax) YMax = AllocateDynamicArray<Float_t>(NumberOfDetectors);
//   for(UShort_t i=0;i<NumberOfDetectors;i++)
//     {
//       XMin[i] = xmin;
//       XMax[i] = xmax;
//       YMax[i] = ymin;
//       YMax[i] = ymax;
//     }
//   fPosition = true;
//   END;
// }


// void BaseDetector::SetFocalPosDetector(FocalPosition* FocalPos)
// {
//   START;
//   if(fPosInfo)
//     {
//       FP = FocalPos;
//     }
//   else
//     {
//       Char_t Message[500];
//       sprintf(Message,"Trying to set position Focal Plane psoition detector while no position
// is required in <%s><%s>", DetectorName,DetectorNameExt);
//       MErr * Er = new MErr(WhoamI,0,0, Message);
//       throw Er;
//     }
//   END;
// }

void BaseDetector::SetActiveArea(Float_t xmin, Float_t ymin, Float_t xmax, Float_t ymax, Float_t z, UShort_t Nr)
{
    START;
    if(fPosInfo)
    {
        if(Nr > NumberOfDetectors)
        {
            if(!fActiveArea)
            {
                ActiveArea = AllocateDynamicArray<Double_t>(NumberOfDetectors,5);
                for(UShort_t i=0;i<NumberOfDetectors;i++)
                    for(UShort_t j=0;j<5;j++)
                        ActiveArea[i][j] = 0.;
            }
            ActiveArea[Nr][0] = xmin;
            ActiveArea[Nr][1] = xmax;
            ActiveArea[Nr][2] = ymin;
            ActiveArea[Nr][3] = ymax;
            ActiveArea[Nr][4] = z;
            fActiveArea = true;
        }
        else
        {
            Char_t Message[500];
            sprintf(Message,"Trying to set position beyond Max NumberOfDetectors of <%s><%s> (%d)", DetectorName,DetectorNameExt, NumberOfDetectors);
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Trying to set position while no position is required in", DetectorName,DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

void BaseDetector::SetGateCal(Float_t ll, Float_t ul)
{
    START;
    if(fCalData)
    {
        if(!fCalGate) CalGates = AllocateDynamicArray<Float_t>(NumberOfDetectors,2);
        for(UShort_t i=0;i<NumberOfDetectors;i++)
        {
            CalGates[i][0] = ll;
            CalGates[i][1] = ul;
        }
        fCalGate=true;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set the Gate on Cal Data where there is no Cal Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}



void BaseDetector::SetGateCal(Float_t ll, Float_t ul, UShort_t Nr)
{
    START;
    if(fCalData)
    {
        if(Nr < NumberOfDetectors)
        {
            if(!fCalGate)
            {
                CalGates = AllocateDynamicArray<Float_t>(NumberOfDetectors,2);
                for(UShort_t i=0;i<NumberOfDetectors;i++)
                    CalGates[i][0] = CalGates[i][1] = 0;
            }
            CalGates[Nr][0] = ll;
            CalGates[Nr][1] = ul;
            fCalGate=true;
        }
        else
        {
            Char_t Message[500];
            sprintf(Message,"Max NumberOfDetectors of <%s><%s> is %d", DetectorName,DetectorNameExt, NumberOfDetectors);
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set the Gate on Cal Data where there is no Cal Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}

void BaseDetector::SetGateRaw(UShort_t ll, UShort_t ul)
{
    START;
    if(fRawData)
    {
        if(!fRawGate) RawGates = AllocateDynamicArray<UShort_t>(NumberOfDetectors,2);
        for(UShort_t i=0;i<NumberOfDetectors;i++)
        {
            RawGates[i][0] = ll;
            RawGates[i][1] = ul;
        }
        fRawGate=true;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set the Gate on Raw Data where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

void BaseDetector::SetGateRaw(UShort_t ll, UShort_t ul, UShort_t Nr)
{
    START;
    if(fRawData)
    {
        if(Nr < NumberOfDetectors)
        {
            if(!fRawGate)
            {
                RawGates = AllocateDynamicArray<UShort_t>(NumberOfDetectors,2);
                for(UShort_t i=0;i<NumberOfDetectors;i++)
                    RawGates[i][0] = RawGates[i][1] = 0;
            }
            RawGates[Nr][0] = ll;
            RawGates[Nr][1] = ul;
            fRawGate=true;
        }
        else
        {
            Char_t Message[500];
            sprintf(Message,"Max NumberOfDetectors of <%s><%s> is %d", DetectorName,DetectorNameExt, NumberOfDetectors);
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set the Gate on Raw Data where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

//Bool_t BaseDetector::IsInGate(UShort_t Value, UShort_t Nr)
Bool_t BaseDetector::IsInGate(const UShort_t& Value, const UShort_t& Nr)
{
    START;
    // Inclusive Gates
    if(fRawData)
        if(fRawGate)
            if(
                    (RawGates[Nr][0] <= Value)
                    &&
                    (Value <= RawGates[Nr][1])
                    )
                return true;
            else
                return false;
        else
            return true;
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to Check the Gate on Raw Data where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}

Bool_t BaseDetector::IsInGate(Float_t Value, UShort_t Nr)
{
    START;

    if(fCalData)
        if(fCalGate)
            if(
                    (CalGates[Nr][0] <= Value)
                    &&
                    (Value <= CalGates[Nr][1])
                    )
                return true;
            else
                return false;
        else
            return true;
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to Check the Gate on Calibrated Data where there is no Calibrated Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}




void BaseDetector::AllocateArrays(void)
{
    START;

    // Setup Standard Histograms Boundaries
    SetRawHistogramsParams(16384,0,16383);
    SetCalHistogramsParams(16384,0,16383,"","Cal");

    for(UShort_t i=0; i<3; i++)
        RefPos[i] = 0.;

    if(fRawData)
    {
        RawNameI = AllocateDynamicArray<Char_t>(NumberOfDetectors,30);
        RawTreeNameI = AllocateDynamicArray<Char_t>(NumberOfDetectors,30);
        Raw = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        RawV = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        RawNr = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        RawM = 0;
        for(UShort_t i=0; i< NumberOfDetectors; i++)
        {
            sprintf(RawNameI[i]," ");
            sprintf(RawTreeNameI[i]," ");
            Raw[i] = 0;
            RawV[i] = 0;
            RawNr[i] = 0;
        }

        if(fMaxMult>1)
        {
            // Allocation of arrays for handling multiplicity > 1
            RawN = AllocateDynamicArray<UShort_t*>(fMaxMult);
            PileUpN = AllocateDynamicArray<UShort_t*>(fMaxMult);
            RawNV = AllocateDynamicArray<UShort_t*>(fMaxMult);
            RawNNr = AllocateDynamicArray<UShort_t*>(fMaxMult);

            RawNM = AllocateDynamicArray<Int_t>(NumberOfDetectors);
            for(UShort_t i=0; i< NumberOfDetectors; i++)
                RawNM[i] = 0;

            for(UShort_t k=0; k< fMaxMult; k++)
            {
                RawN[k] = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
                PileUpN[k] = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
                RawNV[k] = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
                RawNNr[k] = AllocateDynamicArray<UShort_t>(NumberOfDetectors);

                for(UShort_t i=0; i< NumberOfDetectors; i++)
                {
                    RawN[k][i] = 0;
                    PileUpN[k][i] = 0;
                    RawNV[k][i] = 0;
                    RawNNr[k][i] = 0;
                }

            }
        }

    }
    if(fCalData)
    {
        CalNameI = AllocateDynamicArray<Char_t>(NumberOfDetectors,20);

        Cal = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        CalV = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        CalNr = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        // Set all to zero
        CalM = 0;
        SetCalName("%s_%d");
        for(UShort_t i=0; i< NumberOfDetectors; i++)
        {
            Cal[i] = 0;
            CalV[i] = 0;
            CalNr[i] = 0;
        }
        if(fRawData)
        {
            if(fHasScalingCalib)
                Coeffs = AllocateDynamicArray<Float_t>(NumberOfDetectors,4);
            else
                Coeffs = AllocateDynamicArray<Float_t>(NumberOfDetectors,3);
        }

        if(fMaxMult>1)
        {
            // Allocation of arrays for handling multiplicity > 1
            CalN = AllocateDynamicArray<Float_t*>(fMaxMult);
            CalNV = AllocateDynamicArray<Float_t*>(fMaxMult);
            CalNNr = AllocateDynamicArray<UShort_t*>(fMaxMult);

            CalNM = AllocateDynamicArray<Int_t>(NumberOfDetectors);
            for(UShort_t i=0; i< NumberOfDetectors; i++)
                CalNM[i] = 0;

            for(UShort_t k=0; k< fMaxMult; k++)
            {
                CalN[k] = AllocateDynamicArray<Float_t>(NumberOfDetectors);
                CalNV[k] = AllocateDynamicArray<Float_t>(NumberOfDetectors);
                CalNNr[k] = AllocateDynamicArray<UShort_t>(NumberOfDetectors);

                for(UShort_t i=0; i< NumberOfDetectors; i++)
                {
                    CalN[k][i] = 0;
                    CalNV[k][i] = 0;
                    CalNNr[k][i] = 0;
                }

            }

        }

    }
    if(fPosInfo)
    {
        ActiveArea  = AllocateDynamicArray<Double_t>(NumberOfDetectors,5);
        for(UShort_t i=0;i<NumberOfDetectors;i++)
            for(UShort_t j=0;j<5;j++)
                ActiveArea[i][j] = 0.;
        fActiveArea = true;
    }


    if(fHasTimeStamp)
    {
        TimeStamps = AllocateDynamicArray<ULong64_t>(NumberOfDetectors);
        TimeStampsV = AllocateDynamicArray<ULong64_t>(NumberOfDetectors);
        TimeStampsNr = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        TimeStampsM = 0;
        for(UShort_t i=0; i< NumberOfDetectors; i++)
        {
            TimeStamps[i] = 0;
            TimeStampsV[i] = 0;
            TimeStampsNr[i] = 0;
        }

        if(fMaxMult>1)
        {
            // Allocation of arrays for handling multiplicity > 1
            TimeStampsN = AllocateDynamicArray<ULong64_t*>(fMaxMult);
            TimeStampsNV = AllocateDynamicArray<ULong64_t*>(fMaxMult);
            TimeStampsNNr = AllocateDynamicArray<UShort_t*>(fMaxMult);

            TimeStampsNM = AllocateDynamicArray<Int_t>(NumberOfDetectors);
            for(UShort_t i=0; i< NumberOfDetectors; i++)
                TimeStampsNM[i] = 0;

            for(UShort_t k=0; k< fMaxMult; k++)
            {
                TimeStampsN[k] = AllocateDynamicArray<ULong64_t>(NumberOfDetectors);
                TimeStampsNV[k] = AllocateDynamicArray<ULong64_t>(NumberOfDetectors);
                TimeStampsNNr[k] = AllocateDynamicArray<UShort_t>(NumberOfDetectors);

                for(UShort_t i=0; i< NumberOfDetectors; i++)
                {
                    TimeStampsN[k][i] = 0;
                    TimeStampsNV[k][i] = 0;
                    TimeStampsNNr[k][i] = 0;
                }

            }
        }

        if(fCalData)
        {

            TimeStampsCal= AllocateDynamicArray<ULong64_t>(NumberOfDetectors);
            TimeStampsCalV = AllocateDynamicArray<ULong64_t>(NumberOfDetectors);
            for(UShort_t i=0; i< NumberOfDetectors; i++)
            {
                TimeStampsCal[i] = 0;
                TimeStampsCalV[i] = 0;
            }

        }

    }

    if(fHasTime)
    {
        Time = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        TimeV = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        TimeNr = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        TimeM = 0;
        for(UShort_t i=0; i< NumberOfDetectors; i++)
        {
            Time[i] = 0;
            TimeV[i] = 0;
            TimeNr[i] = 0;
        }

        if(fMaxMult>1)
        {
            // Allocation of arrays for handling multiplicity > 1
            TimeN = AllocateDynamicArray<UShort_t*>(fMaxMult);
            TimeNV = AllocateDynamicArray<UShort_t*>(fMaxMult);
            TimeNNr = AllocateDynamicArray<UShort_t*>(fMaxMult);

            TimeNM = AllocateDynamicArray<Int_t>(NumberOfDetectors);
            for(UShort_t i=0; i< NumberOfDetectors; i++)
                TimeNM[i] = 0;

            for(UShort_t k=0; k< fMaxMult; k++)
            {
                TimeN[k] = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
                TimeNV[k] = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
                TimeNNr[k] = AllocateDynamicArray<UShort_t>(NumberOfDetectors);

                for(UShort_t i=0; i< NumberOfDetectors; i++)
                {
                    TimeN[k][i] = 0;
                    TimeNV[k][i] = 0;
                    TimeNNr[k][i] = 0;
                }

            }
        }

    }


    END;
}

void BaseDetector::DeAllocateArrays(void)
{
    START;
    if(fRawData)
    {
        RawNameI = FreeDynamicArray<Char_t>(RawNameI,NumberOfDetectors);
        RawTreeNameI = FreeDynamicArray<Char_t>(RawTreeNameI,NumberOfDetectors);
        Raw = FreeDynamicArray<UShort_t>(Raw);
        RawV = FreeDynamicArray<UShort_t>(RawV);
        RawNr = FreeDynamicArray<UShort_t>(RawNr);
        if(fRawGate)
            RawGates = FreeDynamicArray<UShort_t>(RawGates,NumberOfDetectors);

        // DeAllocation of arrays for handling multiplicity > 1
        if(fMaxMult>1)
        {
            for(UShort_t k=0; k< fMaxMult; k++)
            {
                RawN[k] = FreeDynamicArray<UShort_t>(RawN[k]);
                PileUpN[k] = FreeDynamicArray<UShort_t>(PileUpN[k]);
                RawNV[k] = FreeDynamicArray<UShort_t>(RawNV[k]);
                RawNNr[k] = FreeDynamicArray<UShort_t>(RawNNr[k]);
            }
            RawN = FreeDynamicArray<UShort_t*>(RawN);
            RawNV = FreeDynamicArray<UShort_t*>(RawNV);
            RawNNr = FreeDynamicArray<UShort_t*>(RawNNr);

            RawNM = FreeDynamicArray<Int_t>(RawNM);

        }

    }
    if(fCalData)
    {
        Cal = FreeDynamicArray<Float_t>(Cal);
        CalNr = FreeDynamicArray<UShort_t>(CalNr);
        CalV = FreeDynamicArray<Float_t>(CalV);
        if(fCalGate)
            CalGates = FreeDynamicArray<Float_t>(CalGates,NumberOfDetectors);
        if(fRawData)
            Coeffs = FreeDynamicArray<Float_t>(Coeffs,NumberOfDetectors);
        CalNameI = FreeDynamicArray<Char_t>(CalNameI,NumberOfDetectors);

        // DeAllocation of arrays for handling multiplicity > 1
        if(fMaxMult>1)
        {
            for(UShort_t k=0; k< fMaxMult; k++)
            {
                CalN[k] = FreeDynamicArray<Float_t>(CalN[k]);
                CalNV[k] = FreeDynamicArray<Float_t>(CalNV[k]);
                CalNNr[k] = FreeDynamicArray<UShort_t>(CalNNr[k]);
            }
            CalN = FreeDynamicArray<Float_t*>(CalN);
            CalNV = FreeDynamicArray<Float_t*>(CalNV);
            CalNNr = FreeDynamicArray<UShort_t*>(CalNNr);

            CalNM = FreeDynamicArray<Int_t>(CalNM);

        }



    }

    if(fActiveArea)
    {
        ActiveArea = FreeDynamicArray<Double_t>(ActiveArea,NumberOfDetectors);
    }
    if(fHasTimeStamp)
    {
        TimeStamps = FreeDynamicArray<ULong64_t>(TimeStamps);
        TimeStampsV = FreeDynamicArray<ULong64_t>(TimeStampsV);
        TimeStampsNr = FreeDynamicArray<UShort_t>(TimeStampsNr);



        // DeAllocation of arrays for handling multiplicity > 1
        if(fMaxMult>1)
        {
            for(UShort_t k=0; k< fMaxMult; k++)
            {
                TimeStampsN[k] = FreeDynamicArray<ULong64_t>(TimeStampsN[k]);
                TimeStampsNV[k] = FreeDynamicArray<ULong64_t>(TimeStampsNV[k]);
                TimeStampsNNr[k] = FreeDynamicArray<UShort_t>(TimeStampsNNr[k]);
            }
            TimeStampsN = FreeDynamicArray<ULong64_t*>(TimeStampsN);
            TimeStampsNV = FreeDynamicArray<ULong64_t*>(TimeStampsNV);
            TimeStampsNNr = FreeDynamicArray<UShort_t*>(TimeStampsNNr);

            TimeStampsNM = FreeDynamicArray<Int_t>(TimeStampsNM);

        }

        if(fCalData)
        {
            TimeStampsCal = FreeDynamicArray<ULong64_t>(TimeStampsCal);
            TimeStampsCalV = FreeDynamicArray<ULong64_t>(TimeStampsCalV);
        }
    }
    if(fHasTime)
    {
        Time = FreeDynamicArray<UShort_t>(Time);
        TimeV = FreeDynamicArray<UShort_t>(TimeV);
        TimeNr = FreeDynamicArray<UShort_t>(TimeNr);



        // DeAllocation of arrays for handling multiplicity > 1
        if(fMaxMult>1)
        {
            for(UShort_t k=0; k< fMaxMult; k++)
            {
                TimeN[k] = FreeDynamicArray<UShort_t>(TimeN[k]);
                TimeNV[k] = FreeDynamicArray<UShort_t>(TimeNV[k]);
                TimeNNr[k] = FreeDynamicArray<UShort_t>(TimeNNr[k]);
            }
            TimeN = FreeDynamicArray<UShort_t*>(TimeN);
            TimeNV = FreeDynamicArray<UShort_t*>(TimeNV);
            TimeNNr = FreeDynamicArray<UShort_t*>(TimeNNr);

            TimeNM = FreeDynamicArray<Int_t>(TimeNM);

        }
    }
    END;
}

Bool_t BaseDetector::Treat(void)
{
    START;
    Ctr->at(0)++;
    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->Treat();
        }
    }

#ifdef WITH_ROOT
    // Special treatment for Mode3  finattachRawI, as RawM is not stored in tree
    if(fMode == MODE_R2A && fInAttachRawI) //
        for(UShort_t i=0;i<NumberOfDetectors;i++)
            AddRawData(i,Raw[i]);
#endif
    if(fHasCalCharges)
        CalibrateCharges();
    else
        Calibrate();

    if(CalM)
    {
        isPresent = true;
        Ctr->at(1)++;
    }
    return (isPresent);
    END;
}

void BaseDetector::Clear(void)
{
    START;
    if(isComposite){
        for(UShort_t i=0; i< DetList->size(); i++)
            DetList->at(i)->Clear();
    }

    isPresent = false;
    hasValidTrack = false;

    ClearRaw();
    ClearCal();
    END;
}

void BaseDetector::AllocateCounters(void)
{
    START;
    Ctr = new vector<ULong_t>();
    CtrLbl = new vector<string>();

    // Example of minimal two Counters array
    END;
}
void BaseDetector::AddCounter(string Label)
{
    START;
    Ctr->push_back(0);
    CtrLbl->push_back(Label);
    END;
}

void BaseDetector::IncrementCounter(UShort_t Pos)
{
    START;
    Ctr->at(Pos)++;
    END;
}

void BaseDetector::UpdateCounters(void)
{
    START;
    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->UpdateCounters();
        }
    }
    if(isPresent)
        Ctr->at(1)++;

    END;
}

void BaseDetector::ResetCounters(void)
{
    START;
    for(UShort_t i=0; i<Ctr->size(); i++)
        Ctr->at(i) = 0;
    END;
}

void BaseDetector::DeAllocateCounters(void)
{
    START;
    Ctr->clear();
    Ctr = CleanDelete(Ctr);

    CtrLbl->clear();
    CtrLbl = CleanDelete(CtrLbl);
    END;
}

void BaseDetector::PrintCounters(void)
{
    START;
    cout << "=========================================================" << endl;
    cout << "   <" << DetectorName << "><" << DetectorNameExt << "> - Counters" << endl;
    cout << "=========================================================" << endl;
    L->File  << "=========================================================" << endl;
    L->File  << "   <" << DetectorName << "><" << DetectorNameExt << "> - Counters" << endl;
    L->File  << "=========================================================" << endl;
    for(UShort_t i=0; i<CtrLbl->size(); i++)
    {
        cout << setw(35) << setfill(' ')<< CtrLbl->at(i) << ":\t" << Ctr->at(i) << " (" << ((Float_t) Ctr->at(i) / Ctr->at(0)*100.) << "%)" <<  endl;
        L->File  << setw(35) << setfill(' ') <<  CtrLbl->at(i) << ":\t" << Ctr->at(i) << " (" << ((Float_t) Ctr->at(i) / Ctr->at(0)*100.)  << "%)" << endl;
    }

    cout << "-----------------------------------------------------------" << endl;
    cout << "   Duplicated Labels <" << GetNumberRawDuplicatedLabels()<< endl;
    L->File  << "-----------------------------------------------------------" << endl;
    L->File  << "   Duplicated Labels <" << GetNumberRawDuplicatedLabels()<< endl;



    END;
}

void BaseDetector::PrintOptions(ostream & O)
{
    START;
    O << setfill(' ') ;

    O << "=========================================================" << endl;
    O << "   <" << DetectorName << "><" << DetectorNameExt << "> - Options" << endl;
    O << "=========================================================" << endl;

    PRINT_TO_STREAM(O,fMode) << endl;
    PRINT_TO_STREAM(O,isComposite) << endl;
    PRINT_TO_STREAM(O,fRawData) << endl;
    PRINT_TO_STREAM(O,fCalData) << endl;
    PRINT_TO_STREAM(O,fHasTimeStamp) << endl;
    PRINT_TO_STREAM(O,fHasTime) << endl;
    PRINT_TO_STREAM(O,fPosInfo) << endl;
    PRINT_TO_STREAM(O,VerboseLevel) << endl;

#ifdef WITH_ROOT
    O << " ======= Raw Histograms ======="<< endl;
    PRINT_TO_STREAM(O,fHistogramsRaw1D) << endl;
    PRINT_TO_STREAM(O,fHistogramsRaw2D) << endl;
    PRINT_TO_STREAM(O,fHistogramsRawS) << endl;
    O << " ======= Cal Histograms ======="<< endl;
    PRINT_TO_STREAM(O,fHistogramsCal1D) << endl;
    PRINT_TO_STREAM(O,fHistogramsCal2D) << endl;
    PRINT_TO_STREAM(O,fHistogramsCalS) << endl;
    O << " ======= ROOT IO Trees ======="<< endl;
    PRINT_TO_STREAM(O,fInAttachRawV) << endl;
    PRINT_TO_STREAM(O,fInAttachRawI) << endl;
    PRINT_TO_STREAM(O,fInAttachCalV) << endl;
    PRINT_TO_STREAM(O,fInAttachCalF) << endl;
    PRINT_TO_STREAM(O,fInAttachCalI) << endl;
    PRINT_TO_STREAM(O,fOutAttachCalF) << endl;
    PRINT_TO_STREAM(O,fOutAttachCalV) << endl;
    PRINT_TO_STREAM(O,fOutAttachCalI) << endl;
    PRINT_TO_STREAM(O,fOutAttachRawF) << endl;
    PRINT_TO_STREAM(O,fOutAttachRawI) << endl;
    PRINT_TO_STREAM(O,fOutAttachRawV) << endl;
    PRINT_TO_STREAM(O,fOutAttachTS) << endl;
    PRINT_TO_STREAM(O,fOutAttachCalTS) << endl;
    PRINT_TO_STREAM(O,fOutAttachTime) << endl;
#endif
    PRINT_TO_STREAM(O,fMaxMult) << endl;
    PRINT_TO_STREAM(O,fMaxMultSubDetectors) << endl;

    O << "=========================================================" << endl;
    if(isComposite)
        for(UShort_t i=0; i< DetList->size(); i++)
            DetList->at(i)->PrintOptions(O);

    END;
}


Char_t* BaseDetector::GetCommentPtr(Char_t *Line)
{
    START;
    Char_t *Ptr = NULL;
    return  Ptr = strstr(Line,"//");
    END;
}

// template<typename T>
// void BaseDetector::PrintMe(ostream *O, T  Char_t **Message, UShort_t Verbose)
// {
//   START;
//   *O << " Coefficents <" << DetectorName << "><" << DetectorNameExt << "> "  << endl;
//   for(UShort_t i=0;i<NumberOfDetectors;i++)
//     {
//       *O << setw(2) << setfill('0');
//       *O << i << " " ;
//       for(UShort_t j=0;j<N;j++)
// 	{
//           *O << showpos;
//           *O << scientific ;
//           *O << setprecision(5);
// 	  *O << Coeffs[i][j] << " " ;
// 	}
//       O->unsetf(ios::floatfield);
//       *O << Comment[i] << endl;
// 		 *O << noshowpos;
//     }
//   END;
// }


template<typename T>
void BaseDetector::PrintDefaultCoeffs(ostream *O, T  **Coeffs, Char_t **Comment, UShort_t N)
{
    START;
    *O << " Coefficents <" << DetectorName << "><" << DetectorNameExt << "> "  << endl;
    for(UShort_t i=0;i<NumberOfDetectors;i++)
    {
        *O << setw(2) << setfill('0');
        *O << i << " " ;
        for(UShort_t j=0;j<N;j++)
        {
            *O << showpos;
            *O << scientific ;
            *O << setprecision(5);
            *O << Coeffs[i][j] << " " ;
        }
        O->unsetf(ios::floatfield);
        *O << Comment[i] << endl;
        *O << noshowpos;
    }
    END;
}


template<typename T>
Bool_t BaseDetector::GetCoeffs(Char_t *Line, T *Coeffs, UShort_t N)
{
    START;
    stringstream *InOut = new stringstream();
    *InOut << Line;
    for(UShort_t j=0;j<N;j++){
        *InOut >> Coeffs[j];
        if(InOut->fail())
            return false;
    }

    // if(fHasScalingCalib && !fHasCalCharges)
    //   {
    //     for(UShort_t j=0;j<N-1;j++)
    //       Coeffs[j] *= Coeffs[N-1];
    //   }

    InOut = CleanDelete(InOut);
    return true;
    END;
}

Char_t* BaseDetector::CheckComment(Char_t *Line)
{
    START;
    Char_t *Ptr = NULL;
    if((Ptr = strstr(Line,"//"))){
        for(UShort_t i=0;i<(Ptr-Line) ;i++)
        {
            if(!IsWhiteSpace(Line+i))
            {
                Ptr = NULL;
                break;
            }
        }
    }
    return Ptr;

    END;
}


void BaseDetector::ReadDefaultPosition(MIFile *IF)
{
    if(fCalData)
    {
        char Line[255];
        UShort_t Len=255;
        Char_t *Ptr=NULL;

        IF->GetLine(Line,Len);
        while((Ptr = CheckComment(Line)))
        {
            IF->GetLine(Line,Len);
            L->File << Line << endl;
        }
        // Reference position X,Y,Z
        if(!GetCoeffs(Line,RefPos,3))
        {
            Char_t Message[500];
            sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  has wrong format !", DetectorName, DetectorNameExt, IF->GetFileName());
            MErr * Er= new MErr(WhoamI,0,0, Message);
            throw Er;
        }
    }
}


Bool_t BaseDetector::CheckTrack(Float_t Xf, Float_t Tf, Float_t Yf, Float_t Pf, Float_t zFP, UShort_t DetNr )
{
    START;
#ifdef DEBUG
    cout << endl << DetectorName << " "  << DetectorNameExt << endl;
    // For the moment ActiveArea[DetNr][4] is used for Z position, instead of GetRefZ();
    // May should overload GetRefZ(DetNr)
    cout << " Z : " << ActiveArea[DetNr][4] << " Fp " << zFP << endl;
    cout << " X : " << Xf+(ActiveArea[DetNr][4]-zFP)*tan(Tf/1000.)  << endl;
    cout << " X : " << Xf << endl;
    cout << " Tf : " << Tf << endl;
    cout << " X B Low: " << ActiveArea[DetNr][0] << endl;
    cout << " X B Up: " << ActiveArea[DetNr][2] << endl;
    cout << " Y B Low: " << ActiveArea[DetNr][1] << endl;
    cout << " Y B Up: " << ActiveArea[DetNr][3] << endl;
    cout << " fPosInfo: " << fPosInfo << endl;
    cout << " DetNr " << DetNr << endl;
#endif
    if(fPosInfo){
        if(
                (
                    ((Xf+(ActiveArea[DetNr][4]-zFP)*tan(Tf/1000.)) >= ActiveArea[DetNr][0])
                    &&
                    ((Xf+(ActiveArea[DetNr][4]-zFP)*tan(Tf/1000.)) <= ActiveArea[DetNr][2])
                    )
                // Similar for Yf Pf
                // &&
                // (
                //  ((Yf+(ActiveArea[DetNr][4]-zFP)*tan(Pf/1000.)) >= ActiveArea[DetNr][1])
                //  &&
                //  ((Yf+(ActiveArea[DetNr][4]-zFP)*tan(Pf/1000.)) <= ActiveArea[DetNr][3])
                //  )
                )
        {
            return true;
        }
        else
            return false;

    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Trying to check track while not position information defined !", DetectorName, DetectorNameExt);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}

void BaseDetector::ReadDefaultActiveArea()
{
    START;
    if(fCalData)
    {

        MIFile *IF = NULL;
        char Line[255];
        stringstream *InOut = NULL;

        if(fPosInfo){
            InOut = new stringstream();
            (*InOut) << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << DetectorName << "_Ref.cal";
            (*InOut) >>Line;
            InOut = CleanDelete(InOut);
            if((IF = CheckCalibration(Line)))
            {
                ReadActiveArea(IF);
                IF = CleanDelete(IF);
            }
            else
            {
                Char_t Message[1000];
                sprintf(Message,"In <%s><%s> the expected active area information file %s could not be found !", DetectorName, DetectorNameExt, Line);
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
        }
    }


    END;
}

void BaseDetector::ReadActiveArea(MIFile *IF)
{
    START;
    ReadCalibrationBlock(IF,ActiveArea,5);
    END;
}


void BaseDetector::ReadCalibration(MIFile *IF)
{
    START;
    if(fHasScalingCalib)
    {
        ReadCalibrationBlock(IF,Coeffs,4);
        // If Energy scaling Calibration, multiply coeffs
        if(!fHasCalCharges)
        {
            for(UShort_t i=0; i<NumberOfDetectors; i++)
                for(UShort_t j=0;j<3;j++)
                    Coeffs[i][j] *= Coeffs[i][3];
        }
    }
    else
        ReadCalibrationBlock(IF,Coeffs,3);
    END;
}

template<typename T>
void BaseDetector::ReadCalibrationBlock(MIFile *IF, T **Array, UShort_t NC)
{
    START;
    char Line[255];
    UShort_t Len=255;
    UShort_t CoeffsRead = 0;
    UShort_t CommentsRead = 0;
    Char_t *Ptr=NULL;

    Char_t **Comment = NULL;
    Comment = AllocateDynamicArray<Char_t>(NumberOfDetectors,Len);

    for(UShort_t i=0; i<NumberOfDetectors; i++)
        sprintf(Comment[i]," ");

    if(fRawData && fCalData)
    {
#ifdef DEBUG
        cout << "<" << DetectorName << "><" << DetectorNameExt <<"> Reading calibration file " << IF->GetFileName() << endl;
#endif
        L->File << "<" << DetectorName << "><" << DetectorNameExt <<"> Reading calibration file " << IF->GetFileName() << endl;
        UShort_t fLine;
        do
        {
            if((fLine =IF->GetLine(Line,Len)) >= V_INFO)
            {
                if((Ptr = CheckComment(Line))) //Comment line
                {
                    L->File << Ptr ;
                    if(CommentsRead >= V_INFO)  // 3 Line Header
                        L->File << " (at Coefficient of detector " << CoeffsRead << ")" ;
                    L->File << endl;
                    CommentsRead++;
                }
                else // Can read
                {
                    if(GetCoeffs(Line,Array[CoeffsRead],NC))
                    {
                        if((Ptr = GetCommentPtr(Line)))
                            strcpy(Comment[CoeffsRead],Ptr);
                        CoeffsRead++;
                    }
                    else
                    {
                        Comment = FreeDynamicArray<Char_t>(Comment,NumberOfDetectors);
                        Char_t Message[1000];
                        sprintf(Message,"In <%s><%s> the calibration file %s seems corrupted : Coeffs %d (out of %d)  has wrong format ! Expecting %d coefficients ", DetectorName, DetectorNameExt, IF->GetFileName(), CoeffsRead,  NumberOfDetectors-1, NC);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
                }
            }
        } while( (CoeffsRead < NumberOfDetectors) && (fLine>3));

        PrintDefaultCoeffs(&(L->File),Array,Comment,NC);

        if(CoeffsRead < NumberOfDetectors)
        {
            Comment = FreeDynamicArray<Char_t>(Comment,NumberOfDetectors);
            Char_t Message[500];
            sprintf(Message,"In <%s><%s> the calibration file %s seems corrupted %d Coeffs found while expecting %d!", DetectorName, DetectorNameExt, IF->GetFileName(), CoeffsRead,  NumberOfDetectors);
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }
    }
    Comment = FreeDynamicArray<Char_t>(Comment,NumberOfDetectors);
    END;
}

void BaseDetector::ReadDefaultCalibration()
{
    START;
    MIFile *IF = NULL;
    char Line[300];
    stringstream *InOut = NULL;

    if(fRawData && fCalData)
    {
        InOut = new stringstream();
        (*InOut) << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << DetectorName << ".cal";
        (*InOut) >>Line;
        InOut = CleanDelete(InOut);
        cout << Line << endl;
        if((IF = CheckCalibration(Line)))
        {
            ReadCalibration(IF);
            IF = CleanDelete(IF);
        }
        else
        {
            // Did not find the calibration file, so generate default one
            GenerateDefaultCalibration(Line);
        }
    }

    if(fRawGate)
    {
        // Eventually Read Gates Here
        InOut = new stringstream();
        (*InOut) << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << DetectorName << "_Th.cal";
        (*InOut) >>Line;
        InOut = CleanDelete(InOut);
        if((IF = CheckCalibration(Line)))
        {
            ReadGates(IF);
            IF = CleanDelete(IF);
        }
        else
        {
            cout << "==============================================================" << endl;
            cout << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
                    "File " << Line << " not found" << endl <<
                    "Using default calibration instead!" << endl;
            cout << "==============================================================" << endl;
            L->File << "=============================================================="<< endl;
            L->File << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl
                    << "File " << Line << " not found" << endl
                    << "Using default calibration instead!" << endl;
            L->File << "=============================================================="<< endl; // Did not find the gate file, so generate default one
            GenerateDefaultGates(Line);
        }
    }

    END;
}

MIFile * BaseDetector::CheckCalibration(Char_t *fName)
{
    START;
    MIFile *IF = NULL;
    try
    {
        IF = new MIFile(fName,VerboseLevel);
        return(IF);
    }
    catch(MErr * Er)
    {
        Er->Print();
        Er = CleanDelete(Er);
        return(NULL);
    }
    END;
}


void BaseDetector::GenerateDefaultCalibration(Char_t *fName)
{
    START;

    cout << "==============================================================" << endl;
    cout << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
            "File " << fName << " not found" << endl <<
            "Generate a new file with Default Calibrations!" << endl;
    cout << "==============================================================" << endl;
    L->File << "==============================================================" << endl;
    L->File  << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
                "File " << fName << " not found" << endl <<
                "Generate a new file with Default Calibrations!" << endl;
    L->File  << "==============================================================" << endl;

    MOFile *OF = NULL;
    try
    {
        OF = new MOFile(fName);
    }
    catch(MErr *Er)
    {
        cout << "==============================================================" << endl;
        cout << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
                "File " << fName << " Could not be created" << endl
             << "Using default calibration instead!" << endl;;
        cout << "==============================================================" << endl;
        L->File  << "==============================================================" << endl;
        L->File  << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
                    "File " << fName << " Could not be created" << endl
                 << "Using default calibration instead!" << endl;;
        L->File  << "==============================================================" << endl;
        OF = CleanDelete(OF);

    }
    const time_t now = time(0);
    char* dt = ctime(&now);

    OF->File << "// Title   : Default calibration file for <" << DetectorName << "><" << DetectorNameExt << "> :" << endl;
    OF->File << "// Date    : " << dt ;
    OF->File << "// Comment : " << endl;

    if(fHasScalingCalib)
        OF->File << "// Format  : a0 \t a1 \t a2 \t Scale \t // ParameterName" << endl;
    else
        OF->File << "// Format  : a0 \t a1 \t a2 \t // ParameterName" << endl;


    for(UShort_t i =0; i<NumberOfDetectors;i++)
    {
        for(UShort_t j=0;j<3;j++)
        {
            cout << Coeffs[i][j] << " " ;
            if(OF) OF->File << Coeffs[i][j] << " ";
        }
        if(fHasScalingCalib)
        {
            cout << Coeffs[i][3] << " " ;
            if(OF) OF->File << Coeffs[i][3] << " ";
        }

        cout << "// " << RawNameI[i]<< endl;
        if(OF) OF->File  << "// " << RawNameI[i]<< endl;
    }
    OF = CleanDelete(OF);

    END;
}

void BaseDetector::GenerateDefaultGates(Char_t *fName)
{
    START;

    cout << "==============================================================" << endl;
    cout << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
            "File " << fName << " not found" << endl <<
            "Generate a new file with Default Gates!" << endl;
    cout << "==============================================================" << endl;
    L->File << "==============================================================" << endl;
    L->File  << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
                "File " << fName << " not found" << endl <<
                "Generate a new file with Default Gates!" << endl;
    L->File  << "==============================================================" << endl;

    MOFile *OF = NULL;
    try
    {
        OF = new MOFile(fName);
    }
    catch(MErr *Er)
    {
        cout << "==============================================================" << endl;
        cout << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
                "File " << fName << " Could not be created" << endl
             << " Using dafault Gates instead!" << endl;;
        cout << "==============================================================" << endl;
        L->File  << "==============================================================" << endl;
        L->File  << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
                    "File " << fName << " Could not be created" << endl
                 << "Using dafault Gates instead!" << endl;;
        L->File  << "==============================================================" << endl;
        OF = CleanDelete(OF);

    }
    const time_t now = time(0);
    char* dt = ctime(&now);

    OF->File << "// Title   : Default Gates file for <" << DetectorName << "><" << DetectorNameExt << "> :" << endl;
    OF->File << "// Date    : " << dt ;
    OF->File << "// Comment : " << endl;
    OF->File << "// Format  : ll \t ul \t // ParameterName" << endl;

    for(UShort_t i =0; i<NumberOfDetectors;i++)
    {
        for(UShort_t j=0;j<2;j++)
        {
            cout << RawGates[i][j] << " " ;
            if(OF) OF->File  << RawGates[i][j] << " " ;
        }
        cout << "// " << RawNameI[i]<< endl;
        if(OF) OF->File << "// " << RawNameI[i]<< endl;
    }
    OF = CleanDelete(OF);

    END;
}

void BaseDetector::ReadGates(MIFile *IF)
{
    START;
    SetGateRaw(0);
    ReadCalibrationBlock(IF,RawGates,2);
    END;
}


void BaseDetector::ReadCalGates(void)
{
    START;
    MIFile *IF = NULL;
    char Line[300];
    stringstream *InOut = NULL;

    // Allocate and set Calibration Gates to default
    SetGateCal(0,0);

    if(fCalGate)
    {
        InOut = new stringstream();
        (*InOut) << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << DetectorName << "_Gates.cal";
        (*InOut) >>Line;
        InOut = CleanDelete(InOut);
        if((IF = CheckCalibration(Line)))
        {
            ReadCalGates(IF);
            IF = CleanDelete(IF);
        }
        else
        {
            cout << "==============================================================" << endl;
            cout << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
                    "File " << Line << " not found" << endl <<
                    "Using default calibration instead!" << endl;
            cout << "==============================================================" << endl;
            L->File << "=============================================================="<< endl;
            L->File << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl
                    << "File " << Line << " not found" << endl
                    << "Using default calibration instead!" << endl;
            L->File << "=============================================================="<< endl; // Did not find the gate file, so generate default one
            GenerateDefaultGates(Line);
        }
    }


    END;
}
void BaseDetector::ReadCalGates(MIFile *IF)
{
    START;
    ReadCalibrationBlock(IF,CalGates,2);

    END;
}

void BaseDetector::PrintRaw(void)
{
    START;
    if(fRawData)
    {
        printf(" =================================================================\n");
        cout << "  Print Raw data of  " << GetName() << " Event      " << endl;
        cout << "  Multiplicty :  " << RawM  <<  endl;
        printf(" =================================================================\n");
        if(RawM>0)
        {
            if(!fHasTimeStamp)
            {
                printf(" | ChNr |  Raw | ParName\n");
                for (UShort_t i=0 ; i<RawM; i++ )
                    printf(" | %4d | %06u  | %s \n",RawNr[i],RawV[i], RawNameI[RawNr[i]]);
                printf(" =================================================================\n");
            }
            else
            {
                if(!fHasTime)
                {
                    printf(" | ChNr |  Raw | \t TS \t | ParName | GatesRaw \n");
                    for (UShort_t i=0 ; i<RawM; i++ )
                        printf(" | %4d | %06u | %llu | %s | <%d - %d > \n",RawNr[i],RawV[i],TimeStamps[TimeStampsNr[i]],RawNameI[RawNr[i]], RawGates[RawNr[i]][0], RawGates[RawNr[i]][1]);
                    printf(" =================================================================\n");
                }
                else
                {
                    printf(" | ChNr |  Raw | \t Time \t | \t TS \t | ParName | GatesRaw \n");
                    for (UShort_t i=0 ; i<RawM; i++ )
                        printf(" | %4d | %06u | &06u | %06u | %llu | %s | <%d - %d> \n",
                               RawNr[i],RawV[i],Time[TimeNr[i]],TimeStamps[TimeStampsNr[i]],RawNameI[RawNr[i]], RawGates[RawNr[i]][0], RawGates[RawNr[i]][1]);
                    printf(" =================================================================\n");
                }

            }
        }
        if(fMaxMult>1)
        {
            printf(" -----------------------------------------------------------------\n");
            cout << "  Multiplicity Data >= 1  "  <<  endl;

            for(UShort_t i=0; i< NumberOfDetectors;i++)
            {
                if(RawNM[i] >0)
                {
                    cout << "  Detector #   " << i <<  " -- Par name  : " << RawNameI[i] << endl;

                    if(!fHasTimeStamp)
                    {
                        printf(" | Mult |  Raw \n");
                        for(UShort_t k=0; k< RawNM[i];k++)
                            printf(" | %4d | %06u \n",k+1,RawNV[k][i]);
                        printf(" -----------------------------------------------------------------\n");
                    }
                    else
                    {
                        if(!fHasTime)
                        {
                            printf(" | Mult | Raw | PU | \t TS \n");
                            for(UShort_t k=0; k< RawNM[i];k++)
                                printf(" | %4d | %06u | %d | %llu \n",k+1,RawNV[k][i],PileUpN[k][RawNNr[k][i]],TimeStampsN[k][i]);
                            printf(" -----------------------------------------------------------------\n");
                        }
                        else
                        {
                            printf(" | Mult | Raw | PU | \t Time | \t TS \n");
                            for(UShort_t k=0; k< RawNM[i];k++)
                                printf(" | %4d | %06u | %06u | %d | %llu \n",k+1,RawNV[k][i],PileUpN[k][RawNNr[k][i]],TimeN[k][i],TimeStampsN[k][i]);
                            printf(" -----------------------------------------------------------------\n");

                        }
                    }
                }
            }
            printf(" -----------------------------------------------------------------\n");

        }
    }

    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
            DetList->at(i)->PrintRaw();
    }


    END;
}

void BaseDetector::PrintCal(void)
{
    START;
    if(fCalData)
    {
        printf(" =================================================================\n");
        cout << "  Print Calibrated data of  " << GetName() << " Event      " << endl;
        cout << "  Multiplicty :  " << CalM  <<  endl;
        printf(" =================================================================\n");
        if(CalM>0)
        {
            if(!fHasTimeStamp)
            {

                if(CalNameI)
                {
                    for (UShort_t i=0 ; i<NumberOfDetectors; i++ )
                        cout << " | " << setw(10) << CalNameI[i] << " | " << setw(10) << Cal[i] << " |" << endl;
                }
                else
                {
                    printf(" | ChNr |   Cal   | ParName \n");
                    for (UShort_t i=0 ; i<CalM; i++ )
                        printf(" | %4d | %9.2f | %s \n",CalNr[i],CalV[i], CalNameI[CalNr[i]]);
                }

            }
            else
            {
                printf(" | ChNr |  Cal | \t TS \t | ParName | GatesCal \n");
                for (UShort_t i=0 ; i<CalM; i++ )
                {
                    printf(" | %4d | %9.2f | %llu | %s > \n",CalNr[i],CalV[i],TimeStampsCalV[i],CalNameI[CalNr[i]]);
                }
                //              for (UShort_t i=0 ; i<CalM; i++ )
                printf(" =================================================================\n");
            }

        }
        printf(" =================================================================\n");
    }
    END;
}

void BaseDetector::ClearRaw(void)
{
    START;
    if(fRawData)
    {
        // Clear Raw Parameters
        for(UShort_t i=0; i < RawM; i++){
            Raw[RawNr[i]] = 0;
            RawV[i] = 0;
            RawNr[i] = 0;
        }
        RawM = 0;
    }

    if(fHasTimeStamp)
    {
        for(UShort_t i=0; i < TimeStampsM; i++){
            TimeStamps[TimeStampsNr[i]] = 0;
            TimeStampsV[i] = 0;
            TimeStampsNr[i] = 0;
        }
        TimeStampsM = 0;
    }

    if(fHasTime)
    {
        for(UShort_t i=0; i < TimeM; i++){
            Time[TimeNr[i]] = 0;
            TimeV[i] = 0;
            TimeNr[i] = 0;
        }
        TimeM = 0;
    }


    if(fRawData)
    {
        if(fMaxMult>1)
        {
            for(UShort_t k =0; k< NumberOfDetectors;k++)
            {

                for(UShort_t i=0; i < RawNM[k]; i++){
                    RawN[i][RawNNr[i][k]] = 0;
                    PileUpN[i][RawNNr[i][k]] = 0;
                    RawNV[i][k] = 0;
                    RawNNr[i][k] = 0;
                }

                RawNM[k] = 0;
                if(fHasTimeStamp)
                {
                    for(UShort_t i=0; i < TimeStampsNM[k]; i++){
                        TimeStampsN[i][TimeStampsNNr[i][k]] = 0;
                        TimeStampsNV[i][k] = 0;
                        TimeStampsNNr[i][k] = 0;
                    }
                    TimeStampsNM[k] = 0;
                }
                if(fHasTime)
                {
                    for(UShort_t i=0; i < TimeNM[k]; i++){
                        TimeN[i][TimeNNr[i][k]] = 0;
                        TimeNV[i][k] = 0;
                        TimeNNr[i][k] = 0;
                    }
                    TimeNM[k] = 0;
                }



            }
        }
    }
    END;
}

void BaseDetector::ClearCal(void)
{
    START;
    if(fCalData)
    {
        // Clear Cal Parameters
        for(UShort_t i=0; i < CalM; i++){
            Cal[CalNr[i]] = 0;
            CalV[i] = 0;
            if(fHasTimeStamp)
            {
                TimeStampsCal[CalNr[i]] = 0;
                TimeStampsCalV[i] = 0;
            }
            CalNr[i] = 0;
        }
        CalM = 0;

        if(fMaxMult>1)
        {
            for(UShort_t k =0; k< NumberOfDetectors;k++)
            {

                for(UShort_t i=0; i < CalNM[k]; i++){
                    CalN[i][CalNNr[i][k]] = 0;
                    CalNV[i][k] = 0;
                    CalNNr[i][k] = 0;
                }

                CalNM[k] = 0;

            }
        }

    }
    END;
}


//void BaseDetector::AddRawData(UShort_t uid, UShort_t val, ULong64_t TS,  Int_t DataValid, Int_t PileUp, Int_t fTime)
//{
//    START;
//    // Add The data to Raw
//    SetRawData(uid,val,TS,DataValid,PileUp,fTime);
//    END;
//}

//void BaseDetector::SetRawData(UShort_t Nr, UShort_t RawData, ULong64_t TS, Int_t DataValid, Int_t PileUp, Int_t fTime)
void BaseDetector::AddRawData(const UShort_t& Nr,const UShort_t& RawData,const ULong64_t& TS,const Int_t& DataValid,const Int_t& PileUp,const UShort_t& fTime)
{
    START;
    Bool_t MultipleHit = false;
    if(fRawData)
    {
//            cout << "Add data 1 " << std::dec << Nr << " " << RawData << " PileUp " << PileUp << endl;
//            //cout<<"Nr:  "<<Nr<<" TS: "<<TS<<endl;
//            cout<< "Time:  "  << std::dec  << fTime << " TS: "<<TS<<endl;
        if(fMaxMult == 1)
        {
            //        cout << "Add data 2 " << Nr << " " << RawData << endl;
            for(UShort_t i=0;i< RawM; i++)
            {
                if(RawNr[i] == Nr)
                {
                    RawDuplicateLabel++;
                    Char_t Message[500];
                    //  PrintRaw();
                    sprintf(Message, "<%s><%s>Trying to add the Raw Data WITH LABEL ALREADY READ LABEL : <%d><%d><%llu> RawM = <%d>", DetectorName, DetectorNameExt, Nr, RawData,TS, RawM);
                    MErr * Er = new MErr(WhoamI, 0, 0, Message);
                    throw Er;
                }
            }
        }
        if(RawM < NumberOfDetectors)
        {
            if(Nr < NumberOfDetectors)
            {
                if(IsInGate(RawData,Nr))
                {
                    //Just checking if this is the first or N-th hit
                    for(UShort_t i=0;i< RawM; i++)
                        if(RawNr[i] == Nr)
                        {
                            MultipleHit = true;
                        }

                    //Always addung the data to the Multihit
                    if(fMaxMult > 1)
                    {
    //                        if(strcmp(DetectorName,"FPMWPat") == 7)
    //                            cout<<"\n In Class Nr:  "<< DetectorName << " "<< Nr<<" TS: "<<TS<<" RawData: "<<RawData<< endl;

                        // Fill The Multiples
                        //                          cout << "Add data 3 " << Nr << " " << RawData << " " << fMaxMult<< endl;

                        if(RawNM[Nr]>=fMaxMult)
                        {
                            Char_t Message[500];
                            sprintf(Message, "<%s><%s>Trying to add the Raw Data WITH LABEL Beyond allowed multiplicity: <%d><%d><%llu> RawM = <%d> / <%d>", DetectorName, DetectorNameExt, Nr, RawData,TS, RawNM[Nr],fMaxMult);
                            MErr * Er = new MErr(WhoamI, 0, 0, Message);
                            // throw Er;
                        }
                        else
                        {
                            RawN[RawNM[Nr]][Nr] = RawData;
                            PileUpN[RawNM[Nr]][Nr] = PileUp;
                            RawNNr[RawNM[Nr]][Nr] = Nr;
                            RawNV[RawNM[Nr]][Nr] = RawData;
                            //cout << " Multiple print " << Nr << " " << RawData << " "<< RawN[RawNM[Nr]][Nr]
                            //<<" "<< RawNV[RawNM[Nr]][Nr]<< " " << RawNM[Nr] << endl;

                            RawNM[Nr]++;

                            if(fHasTimeStamp)
                            {
                                TimeStampsN[TimeStampsNM[Nr]][Nr] = TS;
                                TimeStampsNNr[TimeStampsNM[Nr]][Nr] = Nr;
                                TimeStampsNV[TimeStampsNM[Nr]][Nr] = TS;

                                //cout << "----- Multiple TS " << Nr<<" "<<TimeStampsNNr[TimeStampsNM[Nr]][Nr]<<"  " << TimeStampsN[TimeStampsNM[Nr]][Nr] << endl;

                                TimeStampsNM[Nr]++;

                            }

                            if(fHasTime)
                            {
                                TimeN[TimeNM[Nr]][Nr] = fTime;
                                TimeNNr[TimeNM[Nr]][Nr] = Nr;
                                TimeNV[TimeNM[Nr]][Nr] = fTime;

                                //cout << "----- Multiple TS " << Nr<<" "<<TimeNNr[TimeNM[Nr]][Nr]<<"  " << TimeN[TimeNM[Nr]][Nr] << endl;

                                TimeNM[Nr]++;

                            }
                        }
                    }


                    // Adding data only if first hit
                    if(!MultipleHit)
                    {
                        //                    cout << "Add data 4 " << Nr << " " << RawData << " " << fMaxMult<< endl;

                        Raw[Nr] = RawData;
                        RawNr[RawM] = Nr;
                        RawV[RawM] = Raw[Nr];
                        RawM++;
                        if(fHasTimeStamp)
                        {
                            TimeStamps[Nr] = TS;
                            TimeStampsNr[TimeStampsM] = Nr;
                            TimeStampsV[TimeStampsM] = TimeStamps[Nr];
                            TimeStampsM++;

                        }
                        if(fHasTime)
                        {
                            Time[Nr] = fTime;
                            TimeNr[TimeM] = Nr;
                            TimeV[TimeM] = Time[Nr];
                            TimeM++;

                        }
                    }
                }
            }
            else
            {
                Char_t Message[500];
                sprintf(Message,"Max NumberOfDetectors of <%s><%s> is %d - Called with %d", DetectorName,DetectorNameExt, NumberOfDetectors,Nr);
                MErr * Er = new MErr(WhoamI,0,0, Message);
                throw Er;
            }
        }
        else
        {

            //Always addung the data to the Multihit
            if(fMaxMult > 1)
            {
                if(strcmp(DetectorName,"FPMWPat") == 7)
                    cout<<"\n In Class Nr:  "<< DetectorName << " "<< Nr<<" TS: "<<TS<<" RawData: "<<RawData<< endl;

                // Fill The Multiples
                //                          cout << "Add data 3 " << Nr << " " << RawData << " " << fMaxMult<< endl;

                if(RawNM[Nr]>=fMaxMult)
                {
                    Char_t Message[500];
                    sprintf(Message, "<%s><%s>Trying to add the Raw Data WITH LABEL Beyond allowed multiplicity: <%d><%d><%llu> RawM = <%d> / <%d>", DetectorName, DetectorNameExt, Nr, RawData,TS, RawNM[Nr],fMaxMult);
                    MErr * Er = new MErr(WhoamI, 0, 0, Message);
                    throw Er;
                }
                else
                {
                    RawN[RawNM[Nr]][Nr] = RawData;
                    PileUpN[RawNM[Nr]][Nr] = PileUp;
                    RawNNr[RawNM[Nr]][Nr] = Nr;
                    RawNV[RawNM[Nr]][Nr] = RawData;
                    cout << " Multiple print " << Nr << " " << RawData << " "<< RawN[RawNM[Nr]][Nr]
                            <<" "<< RawNV[RawNM[Nr]][Nr]<< " " << RawNM[Nr] << endl;

                    RawNM[Nr]++;

                    if(fHasTimeStamp)
                    {
                        TimeStampsN[TimeStampsNM[Nr]][Nr] = TS;
                        TimeStampsNNr[TimeStampsNM[Nr]][Nr] = Nr;
                        TimeStampsNV[TimeStampsNM[Nr]][Nr] = TS;

                        //cout << "----- Multiple TS " << Nr<<" "<<TimeStampsNNr[TimeStampsNM[Nr]][Nr]<<"  " << TimeStampsN[TimeStampsNM[Nr]][Nr] << endl;

                        TimeStampsNM[Nr]++;

                    }
                }
            }
            PrintRaw();
            Char_t Message[500];
            sprintf(Message,"Max NumberOfDetectors of <%s><%s> is %d - RawM is %d", DetectorName, DetectorNameExt, NumberOfDetectors, RawM);
            cout << Message << endl;
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }

    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to add the Raw Data where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    //  PrintRaw();


    END;
}

void BaseDetector::SetCalData(UShort_t Nr,Float_t Value, ULong64_t TS)
{
    START;
    if(fCalData)
    {
        if(CalM < NumberOfDetectors)
            if(Nr < NumberOfDetectors)
            {
                if(IsInGate(Value,Nr))
                {
                    Cal[Nr] = Value;
                    CalNr[CalM] = Nr;
                    CalV[CalM] = Cal[Nr];
                    if(fHasTimeStamp)
                    {
                        TimeStampsCal[Nr] = TS;
                        TimeStampsCalV[CalM] = TS;
                    }
                    CalM++;

                }
            }
            else
            {
                Char_t Message[500];
                sprintf(Message,"Max NumberOfDetectors of <%s><%s> is %d - but called with Nr = %d", DetectorName,DetectorNameExt, NumberOfDetectors,Nr);
                MErr * Er = new MErr(WhoamI,0,0, Message);
                throw Er;
            }
        else
        {
            Char_t Message[500];
            sprintf(Message,"Max NumberOfDetectors of <%s><%s> is %d but called with CalM %d", DetectorName, DetectorNameExt, NumberOfDetectors, CalM);
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to add the Cal Data where there is no Cal Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}


void BaseDetector::CalibrateCharges(void)
{
    START;
    if(fHasCalCharges && fHasScalingCalib)
    {
        if(fRawData && fCalData)
            for(UShort_t i=0;i<RawM;i++)
            {

                UShort_t Q = 0;
                Float_t Value =0.0;
                Rnd->Next();
                Q =  RawV[i] - Coeffs[RawNr[i]][0] + Rnd->Value();

                for(UShort_t j=1;j<3;j++)
                {
                    Value += powf((Float_t) Q ,
                                  (Float_t) j)*Coeffs[RawNr[i]][j];
                }
                Value *= Coeffs[RawNr[i]][3];
                //            cout << "Ch" << RawNr[i] << " Val " << RawV[i] << endl;
                //            cout << "Ch" << RawNr[i] << " Coef 0 " << Coeffs[RawNr[i]][0] << endl;
                //            cout << "Ch" << RawNr[i] << " Coef 1 " << Coeffs[RawNr[i]][1] << endl;
                //            cout << "Ch" << RawNr[i] << " Coef 2 " << Coeffs[RawNr[i]][2] << endl;
                //            cout << "Ch" << RawNr[i] << " Coef 3 " << Coeffs[RawNr[i]][3] << endl;
                //            cout << "Ch" << RawNr[i] << " Q " << Q << endl;
                //            cout << "Ch" << RawNr[i] << " CalVal " << Value << endl;
                SetCalData(RawNr[i],Value);
            }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to use CaalibrateCharges while detector not initialized properly for calibration coeffs <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}

void BaseDetector::Calibrate(void)
{
    START;
    if(fRawData && fCalData)
        for(UShort_t i=0;i<RawM;i++)
        {
            Float_t Value =0.0;
            Rnd->Next();

            for(UShort_t j=0;j<3;j++)
            {
                Value += powf((Float_t) RawV[i] + Rnd->Value(),
                              (Float_t) j)*Coeffs[RawNr[i]][j];
            }
            if(!fHasTimeStamp)
                SetCalData(RawNr[i],Value);
            else
                SetCalData(RawNr[i],Value,TimeStampsV[i]);
        }

    END;
}

void BaseDetector::SetCalName(const Char_t *Name, UShort_t Pos)
{
    START;
    if(fCalData)
    {
        if(Pos<NumberOfDetectors)
            sprintf(CalNameI[Pos],"%s", Name);
        else
        {
            char line[500];
            sprintf(line,"Call out of bounds please correct!");
            MErr * Er = new MErr(WhoamI,0,0,line);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set the Cal Data Name where there is no Cal Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;

}

void BaseDetector::SetCalName(const Char_t * Format)
{
    START;
    if(fCalData)
    {
        if(NumberOfDetectors > 1)
            for(UShort_t i=0;i<NumberOfDetectors;i++)
            {
                sprintf(CalNameI[i],Format, DetectorName, i, DetectorNameExt);
            }
        fRawName=true;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set the Cal Data Name where there is no Cal Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}

void BaseDetector::SetParameterName(const Char_t *Name, UShort_t Pos)
{
    if(fRawData)
    {
        if(Pos<NumberOfDetectors)
        {
            sprintf(RawNameI[Pos],"%s", Name);
            sprintf(RawTreeNameI[Pos],"%s", Name);
        }
        else
        {
            char line[500];
            sprintf(line,"<%s><%s> Call out of bounds please correct! (Pos : %d - Ndet : %d)",DetectorName, DetectorNameExt,Pos,NumberOfDetectors);
            MErr * Er = new MErr(WhoamI,0,0,line);
            throw Er;
        }
        fRawName=true;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set the Raw Data Name where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
}

void BaseDetector::SetParameterName(const Char_t * Format)
{
    START;
    if(fRawData)
    {
        if(NumberOfDetectors > 1)
            for(UShort_t i=0;i<NumberOfDetectors;i++)
            {
                sprintf(RawNameI[i],Format, DetectorName, i, DetectorNameExt);
                sprintf(RawTreeNameI[i],Format, DetectorName, i, DetectorNameExt);
            }
        else
        {
            sprintf(RawNameI[0],"%s%s", DetectorName, DetectorNameExt);
            sprintf(RawTreeNameI[0],"%s%s", DetectorName, DetectorNameExt);
        }
        fRawName=true;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set the Raw Data Name where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}

void BaseDetector::SetParameterNameInRawTree(const Char_t *Name, UShort_t Pos)
{
    if(fRawData)
    {
        if(Pos<NumberOfDetectors)
            sprintf(RawTreeNameI[Pos],"%s", Name);
        else
        {
            char line[500];
            sprintf(line,"<%s><%s> Call out of bounds please correct! (Pos : %d - Ndet : %d)",DetectorName, DetectorNameExt,Pos,NumberOfDetectors);
            MErr * Er = new MErr(WhoamI,0,0,line);
            throw Er;
        }
        fRawName=true;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set the Raw Data Name where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
}

UShort_t BaseDetector::GetRawAt(UShort_t Pos)
{
    START;
    if(fRawData)
    {
        if(Pos < RawM)
            return RawV[Pos];
        else
        {
            char line[500];
            sprintf(line,"<%s><%s>You didnt check the Multiplicity correct your call! Pos: %d - Mul : %d",DetectorName, DetectorNameExt, RawM, Pos);
            MErr * Er = new MErr(WhoamI,0,0,line);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to get the Raw Timestamp Data where there is no Raw Timestamp Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

ULong64_t BaseDetector::GetCalTSAt(UShort_t Pos)
{
    START;
    if(fHasTimeStamp)
    {
        if(Pos < CalM)
            return TimeStampsCalV[Pos];
        else
        {
            char line[500];
            sprintf(line,"<%s><%s>You didnt check the Multiplicity correct your call! Pos: %d - Mul : %d",DetectorName, DetectorNameExt, RawM, Pos);
            MErr * Er = new MErr(WhoamI,0,0,line);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to get the Calibrated Timestamps Data where there is no Timestamp Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

ULong64_t BaseDetector::GetRawTSAt(UShort_t Pos)
{
    START;
    if(fHasTimeStamp)
    {
        if(Pos < RawM)
            return TimeStampsV[Pos];
        else
        {
            char line[500];
            sprintf(line,"<%s><%s>You didnt check the Multiplicity correct your call! Pos: %d - Mul : %d",DetectorName, DetectorNameExt, RawM, Pos);
            MErr * Er = new MErr(WhoamI,0,0,line);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to get the Raw Data where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

UShort_t BaseDetector::GetRawTimeAt(UShort_t Pos)
{
    START;
    if(fHasTime)
    {
        if(Pos < RawM)
            return TimeV[Pos];
        else
        {
            char line[500];
            sprintf(line,"<%s><%s>You didnt check the Multiplicity correct your call! Pos: %d - Mul : %d",DetectorName, DetectorNameExt, RawM, Pos);
            MErr * Er = new MErr(WhoamI,0,0,line);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to get the Raw Data where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

// Float_t BaseDetector::GetRaw(UShort_t Pos)
// {
//   START;
//   if(fRawData)
//     {
//       if(Pos < NumberOfDetectorss)
//         return Raw[Pos];
//       else
//         {
//           char line[500];
//           sprintf(line,"<%s><%s>You didnt check the Multiplicity correct your call! Pos: %d - Mul : %d",DetectorName, DetectorNameExt, RawM, Pos);
//           MErr * Er = new MErr(WhoamI,0,0,line);
//           throw Er;
//         }
//     }
//   else
//     {
//       Char_t Message[500];
//       sprintf(Message,"Trying to get the Raw Data where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
//       MErr * Er = new MErr(WhoamI,0,0, Message);
//       throw Er;
//     }
//   END;
// }

UShort_t BaseDetector::GetRawNrAt(UShort_t Pos)
{
    START;
    if(fRawData)
    {
        if(Pos < RawM)
            return RawNr[Pos];
        else
        {
            char line[500];
            sprintf(line,"You didnt check the Multiplicity correct your call! (M = %d)", Pos);
            MErr * Er = new MErr(WhoamI,0,0,line);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to get the Raw Nr where there is no Raw Nr <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

BaseDetector* BaseDetector::GetSubDet(UShort_t Pos)
{
    START;
    if(Pos < DetList->size())
    {
        return DetList->at(Pos);
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Call out of Bounds, Trying to get sub-detector at Pos (%d) while there are only  %d sub-detectors", DetectorName, DetectorNameExt, Pos, (int) DetList->size()+1);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

BaseDetector* BaseDetector::GetDetPtr(const Char_t *Name)
{
    START;
    BaseDetector *Ptr=NULL;
    cout << "Looking for  " << Name << " in " << GetName() << " --> " << this <<  endl;
    // if(strcmp(Name, GetName())==0)
    // 	 {
    // 		Ptr = this;
    // 		cout << "Found <"  << Name << ">  -- Ptr " << Ptr << endl;
    // 	 }
    // else
    // 	 {
    for(UShort_t i=0;i<DetList->size();i++)
    {
        //Ptr =
        DetList->at(i)->GetDetPtr(Name);
        // if(Ptr)
        // 	break;
    }
    // 	 }

    return Ptr;
    END;
}

Float_t BaseDetector::GetCalAt(UShort_t Pos)
{
    START;
    if(fCalData)
    {
        if(Pos < CalM)
            return CalV[Pos];
        else
        {
            char line[500];
            sprintf(line,"<%s><%s>You didnt check the Multiplicity correct your call! size: %d - Pos : %d",DetectorName, DetectorNameExt, CalM, Pos);
            MErr * Er = new MErr(WhoamI,0,0,line);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to get the Cal Data where there is no Cal Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

UShort_t BaseDetector::GetNrAt(UShort_t Pos)
{
    START;
    if(fCalData)
    {
        if(Pos < CalM)
            return CalNr[Pos];
        else
        {
            char line[500];
            sprintf(line,"You didnt check the Multiplicity correct your call! (M = %d)", Pos);
            MErr * Er = new MErr(WhoamI,0,0,line);
            throw Er;
        }
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to get the Cal Nr where there is no Cal Nr <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

// void BaseDetector::UpdateCalDataAt(UShort_t Pos, Float_t Value)
// {
//   START;
//   if(fCalData)
//     {
//       if(Pos < CalM)
//         {
//           if(IsInGate(Value,GetNrAt(Pos)))
//             {
//               Cal[GetNrAt(Pos)] = Value;
//               CalV[Pos] = Value;
//             }
//           else
//             {
//               // This method is not implemented
//               // Should reorder  here .... and decrease CalM ...
//               Cal[GetNrAt(Pos)] = 0.;
//               CalV[Pos] = 0.;
//               CalNr[Pos] = 0;
//             }
//         }
//       else
//         {
//           Char_t Message[500];
//           sprintf(Message,"<%s><%s> Trying to update Calibratre Value out of CalM (%d) at Pos %d", DetectorName,DetectorNameExt, CalM,Pos);
//           MErr * Er = new MErr(WhoamI,0,0, Message);
//           throw Er;
//         }
//     }
//   else
//     {
//       Char_t Message[500];
//       sprintf(Message,"Trying to add the Cal Data where there is no Cal Data <%s><%s>", DetectorName, DetectorNameExt);
//       MErr * Er = new MErr(WhoamI,0,0, Message);
//       throw Er;
//     }
//   END;
// }

void BaseDetector::SetCalHistogramsParams(UShort_t NBins, Float_t LL, Float_t UL, string Unit, string Folder)
{
    START;
    NBinsCal1D = NBins;
    LLCal1D    = LL;
    ULCal1D    = UL;
    UnitCal1D = Unit;
    SubFolderHistCal1D = Folder;
    SubFolderHistCal2D = Folder;

    END;
}

void BaseDetector::SetRawHistogramsParams(UShort_t NBins, Float_t LL, Float_t UL, string Folder)
{
    START;
    NBinsRaw1D = NBins;
    LLRaw1D    = LL;
    ULRaw1D    = UL;
    SubFolderHistRaw1D = Folder;
    SubFolderHistRaw2D = Folder;
    END;
}


void BaseDetector::SetMainHistogramFolder(const Char_t * FName)
{
    START;
    strncpy(MainHistogramFolder,FName,100);
    END;
}
void BaseDetector::SetHistogramsRaw1DFolder(const Char_t * FName)
{
    START;
    SubFolderHistRaw1D = FName;
    END;
}
void BaseDetector::SetHistogramsRaw2DFolder(const Char_t * FName)
{
    START;
    SubFolderHistRaw2D = FName;
    END;
}
void BaseDetector::SetHistogramsCal1DFolder(const Char_t * FName)
{
    START;
    SubFolderHistCal1D = FName;
    END;
}
void BaseDetector::SetHistogramsCal2DFolder(const Char_t * FName)
{
    START;
    SubFolderHistCal2D = FName;
    END;
}



void BaseDetector::SetInAttach(void)
{
    START;
#ifdef WITH_ROOT
    SetInAttachRawV(false);
    SetInAttachRawI(false);
    SetInAttachCalV(false);
    SetInAttachCalF(false);
    SetInAttachCalI(false);
#endif
    END;
}


void BaseDetector::SetInAttachRawV(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    // If only one raw parameter, do not store array ...
    if(NumberOfDetectors >1 )
        fInAttachRawV = v;
    else
    {
        fInAttachRawV = false;
        if(v)
            SetInAttachRawI(v);
    }
#endif
    END;
}
void BaseDetector::SetInAttachRawI(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    fInAttachRawI = v;
#endif
    END;
}
void BaseDetector::SetInAttachCalV(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    if(NumberOfDetectors >1 )
        fInAttachCalV = v;
    else
    {
        fInAttachCalV = false;
        if(v)
            SetInAttachCalI(v);
    }
#endif
    END;
}
void BaseDetector::SetInAttachCalF(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    if(NumberOfDetectors >1 )
        fInAttachCalF = v;
    else
    {
        fInAttachCalF = false;
        if(v)
            SetInAttachCalI(v);
    }
#endif
    END;
}
void BaseDetector::SetInAttachCalI(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    fInAttachCalI = v;
#endif
    END;
}

void BaseDetector::SetHistograms(void)
{
    START;
    // Raw Histograms
    fHistogramsRaw1DCreated=false;
    fHistogramsRaw2DCreated=false;
    fHistogramsRawSCreated=false;
    fHistogramsRawHPCreated=false;
    HistOffsetRaw1D = HistOffsetRaw2D = 0;
    NHistograms1DRaw = NHistograms2DRaw = 0;
    HistRawPCId = HistRawSId = HistRaw1DGMultId = HistRaw2DGMultVsNrId = -1;


    SetHistogramsRaw1D();
    SetHistogramsRaw2D();
    SetHistogramsRawSummary();

    // Calibrated Histograms
    fHistogramsCal1DCreated=false;
    fHistogramsCal2DCreated=false;
    fHistogramsCalSCreated=false;
    HistOffsetCal1D = HistOffsetCal2D = 0;
    NHistograms1DCal = NHistograms2DCal = 0;

    SetHistogramsCal1D();
    SetHistogramsCal2D();
    SetHistogramsCalSummary();
    END;
}
void BaseDetector::SetHistogramsRaw(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    SetHistogramsRaw1D(v);
    SetHistogramsRaw2D(v);
#endif
    END;
}
void BaseDetector::SetHistogramsRaw1D(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    fHistogramsRaw1D =v ;
#endif
    END;
}
void BaseDetector::SetHistogramsRaw2D(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    fHistogramsRaw2D =v ;
#endif
    END;
}
void BaseDetector::SetHistogramsRawSummary(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    fHistogramsRawS =v ;
    fHistogramsRawHP = v;
    if(v)
        fHistogramsRaw2D = v;
#endif
    END;
}

void BaseDetector::SetHistogramsCal(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    SetHistogramsCal1D(v);
    SetHistogramsCal2D(v);
#endif
    END;
}
void BaseDetector::SetHistogramsCal1D(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    fHistogramsCal1D = v ;
#endif
    END;
}
void BaseDetector::SetHistogramsCal2D(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    fHistogramsCal2D = v ;
#endif
    END;
}
void BaseDetector::SetHistogramsCalSummary(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    fHistogramsCalS = v ;
    if(v)
    {
        fHistogramsCal2D = v;
        fHistogramsRawHP = v;
    }
#endif
    END;
}


void BaseDetector::SetOutAttach(void)
{
    START;
#ifdef WITH_ROOT
    if(fCalData)
    {
        SetOutAttachCalF();
        SetOutAttachCalV();
        SetOutAttachCalI();
    }
    if(fRawData)
    {
        SetOutAttachRawI();
        SetOutAttachRawF();
        SetOutAttachRawV();
        if(fHasTimeStamp)
            SetOutAttachTS();
        if(fHasTime)
            SetOutAttachTime();

        SetOutAttachRawMult();
    }
#endif
    END;
}
void BaseDetector::SetOutAttachCalF(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    if(fCalData)
    {
        fOutAttachCalF = v;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set Output Branches for Cal Data (CalF) where there is no Cal Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
#endif
    END;
}
void BaseDetector::SetOutAttachTS(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    if(fHasTimeStamp)
    {
        fOutAttachTS = v;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set Output Branches for TimeStamps where there is no TimeStamps <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
#endif
    END;
}
void BaseDetector::SetOutAttachCalTS(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    if(fHasTimeStamp)
    {
        fOutAttachCalTS = v;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set Output Branches for Cal TimeStamps where there is no TimeStamps <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
#endif
    END;
}
void BaseDetector::SetOutAttachTime(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    if(fHasTime)
    {
        fOutAttachTime = v;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set Output Branches for Time where there is no Time <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
#endif
    END;
}
void BaseDetector::SetOutAttachCalV(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    if(fCalData)
    {
        fOutAttachCalV = v;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set Output Branches for Cal Data (CalV) where there is no Cal Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
#endif
    END;
}


void BaseDetector::SetOutAttachCalI(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    if(fCalData)
    {
        fOutAttachCalI = v;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to set Output Branches for Cal Data (CalI) where there is no Cal Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
#endif
    END;
}

void BaseDetector::SetOutAttachRawF(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    if(fRawData)
    {
        fOutAttachRawF = v;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to get the Raw Data where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
#endif
    END;
}

void BaseDetector::SetOutAttachRawI(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    if(fRawData)
    {
        fOutAttachRawI = v;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"Trying to get the Raw Data where there is no Raw Data <%s><%s>", DetectorName, DetectorNameExt);
        MErr * Er = new MErr(WhoamI,0,0, Message);
        throw Er;
    }
#endif
    END;
}

void BaseDetector::SetOutAttachRawV(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    // If only one raw parameter, do not store array ...
    if(NumberOfDetectors >1)
        fOutAttachRawV = v;
    else
    {
        if(v)
            fOutAttachRawI = v;
    }
#endif
    END;
}


void BaseDetector::SetOutAttachRawMult(Bool_t v)
{
    START;
#ifdef WITH_ROOT
    fOutAttachRawMult = v;
#endif
    END;
}

UShort_t BaseDetector::GetRawPar(const Char_t *ParName)
{
    START;
    if(fRawData)
    {
        for(UShort_t i=0; i<GetNumberOfDetectors(); i++)
        {
            if(strcmp(RawNameI[i], ParName) == 0)
            {
                return i;
            }
        }
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Could Not find Raw Parmeter \"%s\" while trying to add an Histogram of Raw Data to the detector !", GetName(), GetNameExt(), ParName);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Trying to add an Histogram of Calibrated data, while there is no Calibrated data in the detector !", GetName(), GetName());
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

UShort_t BaseDetector::GetCalPar(const Char_t *ParName)
{
    START;
    if(fCalData)
    {
        for(UShort_t i=0; i<GetNumberOfDetectors(); i++)
        {
            if(strcmp(CalNameI[i], ParName) == 0)
            {
                return i;
            }
        }
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Could Not find Calibrated Parmeter \"%s\" while trying to add an Histogram to the detector !", GetName(), GetNameExt(), ParName);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Trying to add an Histogram of Calibrated data, while there is no Calibrated data in the detector !", GetName(), GetName());
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

#ifdef WITH_ROOT
void BaseDetector::InAttachCal(TTree *InTree)
{
    START;
    if(fCalData)
    {
        Int_t Res = 0;
        if(fInAttachCalV)
        {
            Res = InTree->SetBranchAddress(Form("%sM",OutCalVName),&CalM);
            if(VerboseLevel >= V_INFO)
                cout << "Reading from Branch " << Form("%sM",OutCalVName) << endl;
            if(Res != 0)
            {
                Char_t Message[500];
                sprintf(Message,"In <%s><%s> Trying to attach branch \"%s\" that does not exist or match requested type!", GetName(), GetName(),Form("%sM",OutCalVName));
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
            Res = InTree->SetBranchAddress(Form("%sN",OutCalVName),CalNr);
            if(VerboseLevel >= V_INFO)
                cout << "Reading from Branch " << Form("%sN",OutCalVName) <<  endl;
            if(Res != 0)
            {
                Char_t Message[500];
                sprintf(Message,"In <%s><%s> Trying to attach branch \"%s\" that does not exist or match requested type!", GetName(), GetName(),Form("%sN",OutCalVName));
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
            Res = InTree->SetBranchAddress(Form("%s",OutCalVName),CalV);
            if(VerboseLevel >= V_INFO)
                cout << "Reading from Branch " << Form("%s",OutCalVName) <<  endl;
            if(Res != 0)
            {
                Char_t Message[500];
                sprintf(Message,"In <%s><%s> Trying to attach branch \"%s\" that does not exist or match requested type!", GetName(), GetName(),Form("%s",OutCalVName));
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
        }
        if(fInAttachCalI)
        {
            for(UShort_t i=0;i<NumberOfDetectors;i++)
            {
                Res = InTree->SetBranchAddress(Form("%s", CalNameI[i]),Cal+i);
                if(VerboseLevel >= V_INFO)
                    cout << "Reading from Branch " << Form("%s", CalNameI[i]) <<  endl;
                if(Res)
                {
                    Char_t Message[500];
                    sprintf(Message,"In <%s><%s> Trying to attach branch \"%s\" that does not exist or match type!", GetName(), GetName(),CalNameI[i]);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
            }
        }
        if(fInAttachCalF)
        {
            Res = InTree->SetBranchAddress(Form("%s", OutCalFName),Cal);
            if(VerboseLevel >= V_INFO)
                cout << "Reading from Branch " << Form("%s",OutCalFName) <<  endl;
            if(Res)
            {
                Char_t Message[500];
                sprintf(Message,"In <%s><%s> Trying to attach branch \"%s\" that does not exist or match type!", GetName(), GetName(),OutCalFName);
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
        }


    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Trying to Attach Calibrated Branch, while there is no Calibrated data in the detector !", GetName(), GetName());
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}
void BaseDetector::InAttach(TTree *InTree)
{
    START;

    if(fRawData)
    {
        Int_t Res = 0;
        if(fInAttachRawV)
        {
            Res = InTree->SetBranchAddress(Form("%sRawM",DetectorName),&RawM);
            if(VerboseLevel >= V_INFO)
                cout << "Reading from Branch " << Form("%sRawM",DetectorName) << endl;
            if(Res != 0)
            {
                Char_t Message[500];
                sprintf(Message,"In <%s><%s> Trying to attach branch \"%s\" that does not exist or match requested type!", GetName(), GetName(),Form("%sRawM",DetectorName));
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
            Res = InTree->SetBranchAddress(Form("%sRawNr",DetectorName),RawNr);
            if(VerboseLevel >= V_INFO)
                cout << "Reading from Branch " << Form("%sRawNr",DetectorName) <<  endl;
            if(Res != 0)
            {
                Char_t Message[500];
                sprintf(Message,"In <%s><%s> Trying to attach branch \"%s\" that does not exist or match requested type!", GetName(), GetName(),Form("%sRawNr",DetectorName));
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
            Res = InTree->SetBranchAddress(Form("%sRaw",DetectorName),RawV);
            if(VerboseLevel >= V_INFO)
                cout << "Reading from Branch " << Form("%sRaw",DetectorName) <<  endl;
            if(Res != 0)
            {
                Char_t Message[500];
                sprintf(Message,"In <%s><%s> Trying to attach branch \"%s\" that does not exist or match requested type!", GetName(), GetName(),Form("%sRaw",DetectorName));
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
        }
        if(fInAttachRawI)
        {
            for(UShort_t i=0;i<NumberOfDetectors;i++)
            {
                Res = InTree->SetBranchAddress(Form("%s", RawNameI[i]),Raw+i);
                if(VerboseLevel >= V_INFO)
                    cout << "Reading from Branch " << Form("%s", RawNameI[i]) <<  endl;
                if(Res)
                {
                    Char_t Message[500];
                    sprintf(Message,"In <%s><%s> Trying to attach branch \"%s\" that does not exist or match type!", GetName(), GetName(),RawNameI[i]);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
            }
        }
    }

    END;
}

void BaseDetector::AddBranches(TTree *OutTree)
{
    START;

    OutAttach(OutTree);
    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++){
            DetList->at(i)->AddBranches(OutTree);
        }
    }


    END;
}

void BaseDetector::OutAttach(TTree *OutTree)
{
    START;
    Char_t BranchName[500];
    Char_t BranchType[500];

    if(fRawData)
    {
        // Fixed Size ordered Calibrated Data
        if(fOutAttachRawF)
        {
            sprintf(BranchName,"%s", OutRawFName);
            sprintf(BranchType,"%s[%d]/s", OutRawFName,NumberOfDetectors);
            OutTree->Branch(BranchName,Raw,BranchType);

            if(fHasTimeStamp && fOutAttachTS)
            {
                OutTree->Branch(Form("%sTS",OutRawFName),TimeStamps,Form("%sTS[%d]/l",OutRawFName,NumberOfDetectors));
            }
            if(VerboseLevel >= V_INFO)
                cout << "Adding " << BranchName << " " << BranchType << endl;
        }

        if(fOutAttachRawV)
        {
            OutTree->Branch(Form("%sRawM",DetectorName),&RawM,Form("%sRawM/I",DetectorName));
            OutTree->Branch(Form("%sRawNr",DetectorName),RawNr,Form("%sRawNr[%sRawM]/s",DetectorName,DetectorName));
            OutTree->Branch(Form("%sRaw",DetectorName),RawV,Form("%sRaw[%sRawM]/s",DetectorName,DetectorName));



            // Fixed Size Data for Raw :
            // OutTree->Branch(Form("%sRaw",DetectorName),Raw,Form("%sRaw[%d]/s",DetectorName,NumberOfDetectors));
            if(fHasTimeStamp && fOutAttachTS)
            {
                OutTree->Branch(Form("%sRawTS",DetectorName),TimeStampsV,Form("%sRawTS[%sRawM]/l",DetectorName,DetectorName));
            }
            if(fHasTime && fOutAttachTime)
            {
                OutTree->Branch(Form("%sRawT",DetectorName),TimeV,Form("%sRawT[%sRawM]/s",DetectorName,DetectorName));
            }
            if(VerboseLevel >= V_INFO)
            {
                cout << "Adding " << Form("%sRawM",DetectorName) << " " << Form("%sRawM/I",DetectorName) << endl;
                cout << "Adding " << Form("%sRawNr",DetectorName) << " " << Form("%sRawNr[%sRawM]/s",DetectorName,DetectorName) << endl;
                cout << "Adding " << Form("%sRaw",DetectorName) << " " << Form("%sRaw[%sRawM]/s",DetectorName,DetectorName) << endl;
            }


        }
        if (fOutAttachRawI) {
            for (UShort_t i = 0; i < NumberOfDetectors; i++) {
                //sprintf(BranchName, "%s", RawNameI[i]);
                //sprintf(BranchType, "%s/s", RawNameI[i]);
                sprintf(BranchName, "%s", RawTreeNameI[i]);
                sprintf(BranchType, "%s/s", RawTreeNameI[i]);
                OutTree->Branch(BranchName, Raw + i, BranchType);
                if (VerboseLevel >= V_INFO)
                    cout << "Adding " << BranchName << " " << BranchType << endl;
                if (fHasTimeStamp && fOutAttachTS) {
                    //sprintf(BranchName, "%sTS", RawNameI[i]);
                    //sprintf(BranchType, "%sTS/l", RawNameI[i]);
                    sprintf(BranchName, "%sTS", RawTreeNameI[i]);
                    sprintf(BranchType, "%sTS/l", RawTreeNameI[i]);
                    OutTree->Branch(BranchName, TimeStamps + i, BranchType);
                }
                if (fHasTime && fOutAttachTime) {
                    //sprintf(BranchName, "%sT", RawNameI[i]);
                    //sprintf(BranchType, "%sT/l", RawNameI[i]);
                    sprintf(BranchName, "%sT", RawTreeNameI[i]);
                    sprintf(BranchType, "%sT/s", RawTreeNameI[i]);
                    OutTree->Branch(BranchName, Time + i, BranchType);
                }

            }
        }
        if(fMaxMult>1 && fOutAttachRawMult)
        {

            OutTree->Branch(Form("%sRawMultM",DetectorName),RawNM,Form("%sRawMultM[%d]/I",DetectorName,NumberOfDetectors));
            if (fHasTimeStamp && fOutAttachTS)
            {
                OutTree->Branch(Form("%sTSMultM",DetectorName),TimeStampsNM,Form("%sRTSMultM[%d]/I",DetectorName,NumberOfDetectors));

                if(VerboseLevel >= V_INFO)
                {
                    cout << "Adding " << Form("%sTSMultM",DetectorName) << " " << Form("%sRTSMultM[%d]/I",DetectorName,NumberOfDetectors) << endl;

                }
            }
            if (fHasTime && fOutAttachTime)
            {
                OutTree->Branch(Form("%sTMultM",DetectorName),TimeNM,Form("%sRTMultM[%d]/I",DetectorName,NumberOfDetectors));

                if(VerboseLevel >= V_INFO)
                {
                    cout << "Adding " << Form("%sTMultM",DetectorName) << " " << Form("%sRTMultM[%d]/I",DetectorName,NumberOfDetectors) << endl;

                }
            }
            for(UShort_t i=0; i < fMaxMult; i++)
            {
                OutTree->Branch(Form("%sRawNrMult%02d",DetectorName,i+1),RawNNr[i],Form("%sRawNrMult[%d]/s",DetectorName,NumberOfDetectors));
                OutTree->Branch(Form("%sRawMult%02d",DetectorName,i+1),RawN[i],Form("%sRawMult[%d]/s",DetectorName,NumberOfDetectors));
                OutTree->Branch(Form("%sRawPUMult%02d",DetectorName,i+1),PileUpN[i],Form("%sRawPUMult[%d]/s",DetectorName,NumberOfDetectors));

                if(VerboseLevel >= V_INFO)
                {
                    cout << "Adding " << Form("%sRawNrMult%02d",DetectorName,i+1) << " " << Form("%sRawNrMult[%d]/s",DetectorName,NumberOfDetectors) << endl;
                    cout << "Adding " << Form("%sRawMult%02d",DetectorName,i+1) << " " << Form("%sRawMult[%d]/s",DetectorName,NumberOfDetectors) << endl;
                    cout << "Adding " << Form("%sRawPUMult%02d",DetectorName,i+1) << " " << Form("%sRawPUMult[%d]/s",DetectorName,NumberOfDetectors) << endl;


                }
                if (fHasTimeStamp && fOutAttachTS)
                {
                    OutTree->Branch(Form("%sTSRawMult%02d",DetectorName,i+1),TimeStampsN[i],Form("%sTSRawMult[%d]/l",DetectorName,NumberOfDetectors));
                    if(VerboseLevel >= V_INFO)
                    {
                        cout << "Adding " << Form("%sTSRawMult%02d",DetectorName,i+1) << " " << Form("%sTSRawMult[%d]/l",DetectorName,NumberOfDetectors) << endl;

                    }
                }
                if (fHasTime && fOutAttachTime)
                {
                    OutTree->Branch(Form("%sTRawMult%02d",DetectorName,i+1),TimeN[i],Form("%sTRawMult[%d]/s",DetectorName,NumberOfDetectors));
                    if(VerboseLevel >= V_INFO)
                    {
                        cout << "Adding " << Form("%sTRawMult%02d",DetectorName,i+1) << " " << Form("%sTRawMult[%d]/s",DetectorName,NumberOfDetectors) << endl;

                    }
                }

            }

        }
    }

    if(fCalData)
    {
        // Individual Detectors Calibrated data
        if(fOutAttachCalI)
        {
            for(UShort_t i=0;i<NumberOfDetectors;i++)
            {
                if (strlen(CalNameI[i]) > 0)
                {

                    sprintf(BranchName,"%s", CalNameI[i]);
                    sprintf(BranchType,"%s/F", CalNameI[i]);
                    OutTree->Branch(BranchName,Cal+i,BranchType);

                    if (fHasTimeStamp && fOutAttachCalTS) {
                        sprintf(BranchName, "%sTS",  CalNameI[i]);
                        sprintf(BranchType, "%sTS/l", CalNameI[i]);
                        OutTree->Branch(BranchName, TimeStampsCal + i, BranchType);
                    }

                    if(VerboseLevel >= V_INFO)
                        cout << "Adding " << BranchName << " " << BranchType << endl;
                }
                else
                {
                    Char_t Message[500];
                    sprintf(Message,"In <%s><%s> Trying to attach calibrated branch but the name of parameter number %d  is not set!", GetName(), GetNameExt(),i);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
            }
        }
        // Fixed Size ordered Calibrated Data
        if(fOutAttachCalF)
        {
            sprintf(BranchName,"%s", OutCalFName);
            sprintf(BranchType,"%s[%d]/F", OutCalFName,NumberOfDetectors);
            OutTree->Branch(BranchName,Cal,BranchType);
            if (fHasTimeStamp && fOutAttachCalTS) {
                sprintf(BranchName, "%sTS",  OutCalFName);
                sprintf(BranchType, "%sTS[%d]/l", OutCalFName,NumberOfDetectors);
                OutTree->Branch(BranchName, TimeStampsCal, BranchType);
            }

            if(VerboseLevel >= V_INFO)
                cout << "Adding " << BranchName << " " << BranchType << endl;
        }

        // Variable Size Calibrated Data
        if(fOutAttachCalV)
        {
            sprintf(BranchName,"%sM", OutCalVName);
            sprintf(BranchType,"%sM/I", OutCalVName);
            OutTree->Branch(BranchName,&CalM,BranchType);
            if(VerboseLevel >= V_INFO)
                cout << "Adding " << BranchName << " " << BranchType << endl;

            sprintf(BranchName,"%s", OutCalVName);
            sprintf(BranchType,"%s[%sM]/F", OutCalVName,OutCalVName);
            OutTree->Branch(BranchName,CalV,BranchType);
            if(VerboseLevel >= V_INFO)
                cout << "Adding " << BranchName << " " << BranchType << endl;

            sprintf(BranchName,"%sN", OutCalVName);
            sprintf(BranchType,"%sN[%sM]/s", OutCalVName,OutCalVName);
            OutTree->Branch(BranchName,CalNr,BranchType);

            if(fHasTimeStamp && fOutAttachCalTS)
            {
                OutTree->Branch(Form("%sTS",OutCalVName),TimeStampsCalV,Form("%sTS[%sM]/l",OutCalVName,OutCalVName));
            }

            if(VerboseLevel >= V_INFO)
                cout << "Adding " << BranchName << " " << BranchType << endl;

        }
    }
    END;
}

void BaseDetector::CreateHistograms(TDirectory *TDir)
{
    START;
    string DirName;
    TDirectory *Dir = NULL;


    if(!(Dir = TDir->GetDirectory(GetMainHistogramFolderName())))
    {
        Dir = TDir->mkdir(GetMainHistogramFolderName());
    }

    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++){
            DetList->at(i)->CreateHistograms(Dir);
        }
    }

    if(fHistogramsRaw1D || fHistogramsRaw2D || fHistogramsCal1D || fHistogramsCal2D )
    {
        CreateHistogramsRaw(Dir);
        CreateHistogramsCal(Dir);
    }

    // Reorder Alphabetically
    Dir->GetListOfKeys()->Sort();

    END;
}



void BaseDetector::CreateHistogramsCal1D(TDirectory *Dir)
{
    START;
    string Name;
    Dir->cd("");
    if(SubFolderHistCal1D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal1D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }
    if(!HCal1D)
        HCal1D = new vector<TH1*>;

    for(UShort_t i=0;i<GetNumberOfDetectors();i++)
    {
        Name.clear();
        Name = CalNameI[i];
        if(UnitCal1D.size()>0)
        {
            Name += " (";
            Name += UnitCal1D;
            Name += ")";
        }
        AddHistoCal(CalNameI[i],CalNameI[i],Name.c_str(),NBinsCal1D,LLCal1D,ULCal1D);
    }


    END;
}

void BaseDetector::CreateHistogramsCal(TDirectory *Dir)
{
    START;
    if(fHistogramsCal1D && fCalData)
    {
        if(!fHistogramsCal1DCreated)
            HistOffsetCal1D=H->GetSize();
        CreateHistogramsCal1D(Dir);
    }
    if(fHistogramsCal2D && fCalData)
    {
        if(!fHistogramsCal2DCreated)
            HistOffsetCal2D=H->GetSize();
        CreateHistogramsCal2D(Dir);
    }
    END;
}


void BaseDetector::CreateHistogramsCal2D(TDirectory *Dir)
{
    START;
    string Name;
    char HName[500];
    char HTitle[500];

    Dir->cd("");

    if(SubFolderHistCal2D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal2D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }
    if(!HCal2D)
        HCal2D = new vector<TH1*>;

    if(fHistogramsCalS)
    {
        // Summary 2D plot E vs Nr
        sprintf(HName,"%s%s_vsNr",DetectorName,DetectorNameExt);
        sprintf(HTitle,"Summary <%s><%s> (Cal)",DetectorName,DetectorNameExt);
        AddHistoCal("Nr","*",HName,HTitle,NumberOfDetectors, 0,NumberOfDetectors,NBinsCal1D, LLCal1D, ULCal1D);
    }

    //  AddHistoCal(CalNameI[0],CalNameI[1],"X_vs_Y","X_vs_Y",1000,0,100,1000,0,100);
    //  fHistogramsCal2DCreated =true;


    // if(HCalMap2D)
    // 	 for(UShort_t j=0; j<HCalMap2D->size();j++)
    // 		for(UShort_t k=0; k< HCalMap2D->at(j).size();k++)
    // 		  cout << "<" << DetectorName << "><" << DetectorNameExt
    // 				 << ">= V_INFOD Histo X: "  << j
    // 				 << " Y : " << HCalMap2D->at(j)[k].first
    // 				 << " Name: " << HCalMap2D->at(j)[k].second->GetName() << " ("<< HCalMap2D->at(j)[k].second << ")" << endl;
    END;
}

void BaseDetector::AddHistoCal(const Char_t *ParX,
                               const Char_t *ParY,
                               const Char_t *Name,
                               const Char_t *Title,
                               UShort_t NBinsX,
                               Float_t LLX,
                               Float_t ULX,
                               UShort_t NBinsY,
                               Float_t LLY,
                               Float_t ULY,
                               TDirectory *Dir)
{
    START;
    Bool_t isSum = false;
    UShort_t PXId, PYId;

    if(!fHistogramsCal2D)
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Trying to create Calibrated 2D histo but Cal2D histo are not enabled !", GetName(), GetNameExt());
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    if(Dir)
        Dir->cd("");

    // Check if its a summary plot, Sumary Plot are store in Map at Parameter = NumberOfDetectors
    if((strcmp("Nr", ParX) == 0) && (strcmp("*", ParY) == 0))
        isSum = true;

    // If Boundaries are not provided
    if(NBinsX == 0 && NBinsY == 0)
    {
        if(isSum)
        {
            NBinsX = NumberOfDetectors;
            NBinsY = NBinsCal1D;
            LLX = 0;
            LLY = LLCal1D;
            ULX = NumberOfDetectors;
            ULY = ULCal1D;
        }
        else
        {
            NBinsX = NBinsY = NBinsCal1D;
            LLX = LLY = LLCal1D;
            ULX = ULY = ULCal1D;
        }
    }

    if(!HCal2D)
        HCal2D = new vector<TH1*>;
    if(!HCalMap2D)
    {
        HCalMap2D = new vector< vector< std::pair<UShort_t,TH1*> > >;
        HCalMap2D->resize(NumberOfDetectors+1,vector<std::pair<UShort_t,TH1*> >(0));
    }
    TH1 * MHist=NULL;
    // Create the Histogram
    if((MHist = new TH2F(Name, Title, NBinsX, LLX, ULX, NBinsY, LLY, ULY)))
    {
        if(isSum)
        {
            PXId = NumberOfDetectors;
            if(HCalMap2D->at(PXId).size()>0)
            {
                Char_t Message[500];
                sprintf(Message,"<%s><%s> Trying to create again a summary plot  named \"%s\" !", GetName(), GetNameExt(),Name);
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
            PYId = 0;
            MHist->SetXTitle("Chan number");
            if(UnitCal1D.size()>0)
                MHist->SetYTitle(Form("%s%s (%s)",DetectorName,DetectorNameExt,UnitCal1D.c_str()));
            else
                MHist->SetYTitle(Form("%s%s",DetectorName,DetectorNameExt));

        }
        else
        {
            PXId = GetCalPar(ParX);
            PYId = GetCalPar(ParY);
            // Setup the internal Histogram/Parameter number Map
            if(UnitCal1D.size()>0)
            {
                MHist->SetXTitle(Form("%s (%s)",ParX,UnitCal1D.c_str()));
                MHist->SetYTitle(Form("%s (%s)",ParY,UnitCal1D.c_str()));
            }
            else
            {
                MHist->SetXTitle(ParX);
                MHist->SetYTitle(ParY);
            }
        }

        HCal2D->push_back(MHist);
        HCalMap2D->at(PXId).push_back(std::make_pair(PYId,MHist));
        H->Add(MHist, GetCurrentHistogramFolder());
        NHistograms2DCal++;
        if(isSum)
            fHistogramsCalSCreated = true;
        fHistogramsCal2DCreated = true;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Could not allocated memory to Create the calibrated histogram \"%s\"", GetName(), GetNameExt(),Name);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

void BaseDetector::AddHistoCal(const Char_t *ParX,
                               const Char_t *Name,
                               const Char_t *Title,
                               UShort_t NBinsX,
                               Float_t LLX,
                               Float_t ULX,
                               TDirectory *Dir)
{
    START;
    if(!fHistogramsCal1D)
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Trying to create Calibrated 1D histogram but Cal1D histo are not enabled !", GetName(), GetNameExt());
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    if(Dir)
        Dir->cd("");

    if(!HCal1D)
        HCal1D = new vector<TH1*>;
    if(!HCalMap1D)
    {
        HCalMap1D = new vector<vector<TH1*> >;
        HCalMap1D->resize(NumberOfDetectors,vector<TH1*>(0));
    }

    TH1 * MHist=NULL;
    // Create the Histogram
    if((MHist = new TH1F(Name, Title, NBinsX, LLX, ULX)))
    {
        // Setup the internal Histogram/Parameter number Map
        UShort_t ParId = GetCalPar(ParX);
        MHist->SetXTitle(ParX);
        HCal1D->push_back(MHist);
        HCalMap1D->at(ParId).push_back(MHist);
        H->Add(MHist, GetCurrentHistogramFolder());
        NHistograms1DCal++;
        fHistogramsCal1DCreated =true;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Could not allocated memory to Create the calibrated histogram \"%s\"", GetName(), GetNameExt(),Name);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}


void BaseDetector::CreateHistogramsRaw(TDirectory *Dir)
{
    START;

    if(fHistogramsRaw1D && fRawData)
    {
        if(!fHistogramsRaw1DCreated )
            HistOffsetRaw1D=H->GetSize();
        CreateHistogramsRaw1D(Dir);
    }
    if(fHistogramsRaw2D && fRawData)
    {
        if(!fHistogramsRaw2DCreated )
            HistOffsetRaw2D=H->GetSize();
        CreateHistogramsRaw2D(Dir);
    }
    END;
}

void BaseDetector::CreateHistogramsRaw1D(TDirectory *Dir)
{
    START;
    string Name;
    char HName[300];
    char HTitle[300];
    string HistName;
    Dir->cd("");

    Name.clear();
    HistName.clear();
    if(SubFolderHistRaw1D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistRaw1D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }
    if(!HRaw1D)
        HRaw1D = new vector<TH1*>;

    for(UShort_t i=0;i<GetNumberOfDetectors();i++)
    {
        Name.clear();
        HistName.clear();
        Name = RawTreeNameI[i];
        Name += " (ch)";
        HistName =  RawTreeNameI[i];
        HistName += "_Raw";

        AddHistoRaw(RawNameI[i],HistName.c_str(),Name.c_str(),NBinsRaw1D,LLRaw1D,ULRaw1D);
    }
    if(fHistogramsRawHP)
    {
        // Hit pattern Plot
        sprintf(HName,"%s%s_HitPat",DetectorName,DetectorNameExt);
        sprintf(HTitle,"HitPattern <%s><%s> (Raw)",DetectorName,DetectorNameExt);
        AddHistoRaw("Nr",HName,HTitle,NumberOfDetectors, 0,NumberOfDetectors);
    }

    // For Timestamps and Multiplicities
    if(fMaxMult>1)
    {
        sprintf(HName,"%s%s_GlobalMult",DetectorName,DetectorNameExt);
        sprintf(HTitle,"Global Mult.<%s><%s> (Raw)",DetectorName,DetectorNameExt);
        AddHistoRaw("GMult",HName,HTitle,fMaxMult*NumberOfDetectors, 0,fMaxMult*NumberOfDetectors);

    }

    END;
}

void BaseDetector::CreateHistogramsRaw2D(TDirectory *Dir)
{
    START;
    string Name;
    char HName[500];
    char HTitle[500];

    Dir->cd("");

    Name.clear();
    if(SubFolderHistRaw2D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistRaw2D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }
    if(!HRaw2D)
        HRaw2D = new vector<TH1*>;


    if(fHistogramsRawS)
    {
        // Summary 2D plot E vs Nr
        sprintf(HName,"%s%s_vsNr_raw",DetectorName,DetectorNameExt);
        sprintf(HTitle,"Summary <%s><%s> (ch)",DetectorName,DetectorNameExt);
        AddHistoRaw("Nr","*",HName,HTitle,NumberOfDetectors, 0,NumberOfDetectors,NBinsRaw1D, LLRaw1D, ULRaw1D);
    }

    if(fMaxMult>1)
    {
        sprintf(HName,"%s%s_MvsNr_raw",DetectorName,DetectorNameExt);
        sprintf(HTitle,"Mult <%s><%s> (ch)",DetectorName,DetectorNameExt);

        AddHistoRaw("GMult","*",Form("%s%s_Mult",DetectorName,DetectorNameExt),"Mult_vs_Nr",NumberOfDetectors,0,NumberOfDetectors,fMaxMult,0,fMaxMult);
    }
    END;
}

void BaseDetector::AddHistoRaw(const Char_t *ParX,
                               const Char_t *Name,
                               const Char_t *Title,
                               UShort_t NBinsX,
                               Float_t LLX,
                               Float_t ULX,
                               TDirectory *Dir)
{
    START;
    Bool_t isSum = false;
    Bool_t isGMult = false;
    UShort_t ParId = 0;
    if(!fHistogramsRaw1D)
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Trying to create Raw 1D histogram but Raw1D histo are not enabled !", GetName(), GetNameExt());
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    if(Dir)
        Dir->cd("");

    // Check if its a hit pattern plot, Sumary Plot are store in Map at Parameter = NumberOfDetectors


    if(!HRaw1D)
        HRaw1D = new vector<TH1*>;
    if(!HRawMap1D)
    {
        HRawMap1D = new vector<vector<TH1*> >;
        HRawMap1D->resize(NumberOfDetectors+1,vector<TH1*>(0));
    }

    if((strcmp("Nr", ParX) == 0))
        isSum = true;

    if((strcmp("GMult", ParX) == 0))
    {
        HRawMap1D->push_back(vector<TH1*>(0));
        isGMult = true;
    }


    TH1 * MHist=NULL;
    // Create the Histogram
    if((MHist = new TH1F(Name, Title, NBinsX, LLX, ULX)))
    {
        if(isSum)
        {
            if(HistRawPCId > -1)
            {
                Char_t Message[500];
                sprintf(Message,"<%s><%s> Trying to create again a Raw summary plot named \"%s\"  !", GetName(), GetNameExt(),Name);
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
            else{
                HistRawPCId = NHistograms1DRaw;
                ParId = HistRawPCId;

            }
            MHist->SetXTitle("Chan number");
            MHist->SetYTitle(Form("%s%s (Raw)",DetectorName,DetectorNameExt));
        }
        else if(isGMult)
        {
            if(HistRaw1DGMultId> -1)
            {
                Char_t Message[500];
                sprintf(Message,"<%s><%s> Trying to create again a Raw Mult plot named \"%s\"  !", GetName(), GetNameExt(),Name);
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
            else
            {
                HistRaw1DGMultId = NHistograms1DRaw;
                ParId = HistRaw1DGMultId;
            }
            MHist->SetXTitle("Chan number");
            MHist->SetYTitle(Form("%s%s (Raw)",DetectorName,DetectorNameExt));

        }
        else
        {
            MHist->SetXTitle(ParX);
            // Setup the internal Histogram/Parameter number Map
            ParId= GetRawPar(ParX);
        }
        HRaw1D->push_back(MHist);
        HRawMap1D->at(ParId).push_back(MHist);
        H->Add(MHist, GetCurrentHistogramFolder());
        NHistograms1DRaw++;

        if(isSum)
            fHistogramsRawHPCreated = true;
        //            else if(isGMult)
        //              fHistogramsRawGM1DCreated = true;
        else
            fHistogramsRaw1DCreated = true;



    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Could not allocated memory to Create the calibrated histogram \"%s\"", GetName(), GetNameExt(),Name);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    END;
}

void BaseDetector::AddHistoRaw(const Char_t *ParX,
                               const Char_t *ParY,
                               const Char_t *Name,
                               const Char_t *Title,
                               UShort_t NBinsX,
                               Float_t LLX,
                               Float_t ULX,
                               UShort_t NBinsY,
                               Float_t LLY,
                               Float_t ULY,
                               TDirectory *Dir)
{
    START;
    Bool_t isSum = false;
    Bool_t isGMult = false;
    UShort_t PXId, PYId;
    if(!fHistogramsRaw2D)
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Trying to create Raw 2D histogram but Cal2D histos are not enabled !", GetName(), GetNameExt());
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    if(Dir)
        Dir->cd("");

    // Check if its a summary plot, Sumary Plot are store in Map at Parameter = NumberOfDetectors
    if((strcmp("Nr", ParX) == 0) && (strcmp("*", ParY) == 0))
        isSum = true;

    // Check if its a summary plot, Sumary Plot are store in Map at Parameter = NumberOfDetectors
    if((strcmp("GMult", ParX) == 0) && (strcmp("*", ParY) == 0))
        isGMult= true;

    // If Boundaries are not provided
    if(NBinsX == 0 && NBinsY == 0)
    {
        if(isSum)
        {
            NBinsX = NumberOfDetectors;
            NBinsY = NBinsRaw1D;
            LLX = 0;
            LLY = LLRaw1D;
            ULX = NumberOfDetectors;
            ULY = ULRaw1D;
        }
        else
        {
            NBinsX = NBinsY = NBinsRaw1D;
            LLX = LLY = LLRaw1D;
            ULX = ULY = ULRaw1D;
        }
    }

    if(!HRaw2D)
        HRaw2D = new vector<TH1*>;
    if(!HRawMap2D)
    {
        HRawMap2D = new vector< vector< std::pair<UShort_t,TH1*> > >;
        HRawMap2D->resize(NumberOfDetectors+1,vector<std::pair<UShort_t,TH1*> >(0));
    }
    TH1 * MHist=NULL;
    // Create the Histogram
    if((MHist = new TH2F(Name, Title, NBinsX, LLX, ULX, NBinsY, LLY, ULY)))
    {
        if(isSum)
        {
            PXId = NumberOfDetectors;
            if(HRawMap2D->at(PXId).size()>0)
            {
                Char_t Message[500];
                sprintf(Message,"<%s><%s> Trying to create again a Raw summary plot named \"%s\"  !", GetName(), GetNameExt(),Name);
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
            PYId = 0;
            MHist->SetXTitle("Chan number");
            MHist->SetYTitle(Form("%s%s (Raw)",DetectorName,DetectorNameExt));
        }
        else if(isGMult)
        {
            if(HistRaw2DGMultVsNrId> -1)
            {
                Char_t Message[500];
                sprintf(Message,"<%s><%s> Trying to create again a Mult summary plot named \"%s\"  !", GetName(), GetNameExt(),Name);
                MErr * Er= new MErr(WhoamI,0,0, Message);
                throw Er;
            }
            else
            {
                HistRaw2DGMultVsNrId = NHistograms2DRaw;
                PXId = HistRaw2DGMultVsNrId;
                PYId = 0;
                MHist->SetXTitle("Chan number");
                MHist->SetYTitle(Form("%s%s (Mult)",DetectorName,DetectorNameExt));
            }
        }
        else
        {
            MHist->SetXTitle(ParX);
            MHist->SetYTitle(ParY);
            // Setup the internal Histogram/Parameter number Map
            PXId = GetRawPar(ParX);
            PYId = GetRawPar(ParY);
        }

        HRaw2D->push_back(MHist);
        HRawMap2D->at(PXId).push_back(std::make_pair(PYId,MHist));

        H->Add(MHist, GetCurrentHistogramFolder());
        NHistograms2DRaw++;
        if(isSum)
            fHistogramsRawSCreated = true;
        fHistogramsRaw2DCreated = true;
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"<%s><%s> Could not allocated memory to Create the calibrated histogram \"%s\"", GetName(), GetNameExt(),Name);
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}



string BaseDetector::GetCurrentHistogramFolder(void)
{
    START;
    string Folder = strstr(gDirectory->GetPath(),":/")+2;
    return Folder;
    END;
}

void BaseDetector::DeleteHistograms(void)
{
    START;
    DeleteRawHistograms();
    DeleteCalHistograms();

    END;
}
void BaseDetector::DeleteRawHistograms(void)
{
    START;
    if(fHistogramsRaw1DCreated)
    {
        // Clearing the entries from the Map, it will not delete the TH1
        // objects, will do below
        if(HRawMap1D)
        {
            HRawMap1D->clear();
            HRawMap1D = CleanDelete(HRawMap1D);
        }

        for(UShort_t i=0; i< HRaw1D->size(); ++i)
        {
            HRaw1D->at(i) = CleanDelete(HRaw1D->at(i));
            // Remove Entry from Histo Manager
            H->Clear(HistOffsetRaw1D+i);
        }
        NHistograms1DRaw = 0;
        HRaw1D = CleanDelete(HRaw1D);

    }

    if(fHistogramsRaw2DCreated)
    {
        if(HRawMap2D)
        {
            HRawMap2D->clear();
            HRawMap2D = CleanDelete(HRawMap2D);
        }
        for(UShort_t i=0; i< HRaw2D->size(); ++i)
        {
            HRaw2D->at(i) = CleanDelete(HRaw2D->at(i));
            // Remove from Histo Manager
            H->Clear(HistOffsetRaw2D+i);
        }
        NHistograms2DRaw = 0;
        HRaw2D = CleanDelete(HRaw2D);
    }
    END;
}
void BaseDetector::DeleteCalHistograms(void)
{
    START;
    if(fHistogramsCal1DCreated)
    {
        // Clearing the entries from the Map, it will not delete the TH1
        // objects, will do below
        HCalMap1D->clear();
        HCalMap1D = CleanDelete(HCalMap1D);
        // Will Clear properly each Histograms
        for(UShort_t i=0; i< HCal1D->size(); ++i)
        {
            HCal1D->at(i) = CleanDelete(HCal1D->at(i));
            // Remove the entry from Histo Manager
            H->Clear(HistOffsetCal1D+i);
        }
        HCal1D = CleanDelete(HCal1D);
        NHistograms1DCal = 0;
    }

    if(fHistogramsCal2DCreated)
    {

        // Clearing the entries from the Map, it will not delete the TH1
        // objects, will do below
        HCalMap2D->clear();
        HCalMap2D = CleanDelete(HCalMap2D);

        for(UShort_t i=0; i< HCal2D->size(); ++i)
        {
            HCal2D->at(i) = CleanDelete(HCal2D->at(i));
            // Remove from Histo Manager
            H->Clear(HistOffsetCal2D+i);
        }
        HCal2D = CleanDelete(HCal2D);
        NHistograms2DCal = 0;
    }
    END;
}
void BaseDetector::FillHistograms(void)
{
    START;
    if(isComposite){
        for(UShort_t i=0; i< DetList->size(); i++)
            DetList->at(i)->FillHistograms();
    }
    FillHistogramsRaw();
    FillHistogramsCal();
    END;
}

void BaseDetector::FillHistogramsRaw(void)
{
    START;
    UShort_t GMult = 0;
    if (fHistogramsRaw1DCreated && fRawData) {
        for(Int_t i=0;i<RawM;i++)
        {
            if(fHistogramsRawHPCreated)
                HRawMap1D->at(HistRawPCId)[0]->Fill(RawNr[i]);

            for(UShort_t k=0; k < HRawMap1D->at(RawNr[i]).size(); k++)
                (HRawMap1D->at(RawNr[i]))[k]->Fill(RawV[i]);
        }

        if(HistRaw1DGMultId>-1)
        {
            for(UShort_t k=0; k <NumberOfDetectors;k++)
            {
                GMult += RawNM[k];
            }

            HRawMap1D->at(HistRaw1DGMultId)[0]->Fill(GMult);
        }
    }

    if (fHistogramsRaw2DCreated && fRawData) {
        for(Int_t i=0;i<RawM;i++)
        {
            if(fHistogramsRawSCreated)
                HRawMap2D->at(NumberOfDetectors)[0].second->Fill(RawNr[i],RawV[i]);
            for(UShort_t k=0; k < HRawMap2D->at(RawNr[i]).size(); k++)
                (HRawMap2D->at(RawNr[i])[k].second)->Fill(Raw[RawNr[i]],Raw[HRawMap2D->at(RawNr[i])[k].first]);
        }

        if(HistRaw2DGMultVsNrId>-1)
        {
            for(UShort_t k=0; k <NumberOfDetectors;k++)
                if(RawNM[k]>0)
                    (HRawMap2D->at(HistRaw2DGMultVsNrId)[0].second)->Fill(k,RawNM[k]);
        }


    }
    END;
}

void BaseDetector::FillHistogramsCal(void)
{
    START;
    if (fHistogramsCal1DCreated && fCalData) {
        for(Int_t i=0;i<CalM;i++)
        {
            for(UShort_t k=0; k < HCalMap1D->at(CalNr[i]).size(); k++)
                (HCalMap1D->at(CalNr[i]))[k]->Fill(CalV[i]);
        }
    }

    if (fHistogramsCal2DCreated && fCalData) {
        for(Int_t i=0;i<CalM;i++)
        {
            if(fHistogramsCalSCreated)
                HCalMap2D->at(NumberOfDetectors)[0].second->Fill(CalNr[i],CalV[i]);

            for(UShort_t k=0; k < HCalMap2D->at(CalNr[i]).size(); k++)
                (HCalMap2D->at(CalNr[i])[k].second)->Fill(Cal[CalNr[i]],Cal[HCalMap2D->at(CalNr[i])[k].first]);
        }
    }
    END;
}
void BaseDetector::SaveHistograms(void)
{
    START;
    END;
}

void BaseDetector::EmptyHistograms(void)
{
    START;
    END;
}


#endif


#ifdef WITH_ROOT
void BaseDetector::SetIOOptions(TFile *OutTFile, const Char_t * FolderName, HistoManager *HM, TTree *OutTTree, TTree *InTTree)
{
    START;

    SetMainHistogramFolder(FolderName);
    SetHistoManager(HM);
    // Set Main Options
    SetOpt(OutTTree,InTTree);

    if(VerboseLevel >= V_INFO)
        PrintOptions(cout);
    PrintOptions(L->File);

    // Will Create Recursively the Folder Hierarchy and Histograms
    if(fMode != MODE_CALC)
    {
        if(OutTFile)
            CreateHistograms(OutTFile->GetDirectory(""));
        else
        {
            Char_t Message[500];
            sprintf(Message,"Trying to Create Histograms while TFile is not initialized properly (fMode <%d>)!",fMode );
            MErr * Er= new MErr(WhoamI,0,0, Message);
            throw Er;
        }
    }

    END;
}

void BaseDetector::SetHistoManager(HistoManager *HM)
{
    START;
    H = HM;

    if(isComposite)
        for(UShort_t i=0; i< DetList->size(); i++)
            DetList->at(i)->SetHistoManager(HM);

    END;
}

void BaseDetector::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;
    if(isComposite)
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->SetOpt(OutTTree,InTTree);
            // if(VerboseLevel >= V_INFO)
            // DetList->at(i)->PrintOptions(cout);
            ///		  DetList->at(i)->PrintOptions(L->File);
        }


    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
            SetHistogramsRaw(true);
        if(fCalData)
            SetHistogramsCal(true);
    }
    else if(fMode == MODE_D2R)
    {
        if(fRawData)
        {
            if(!fOutAttachRawF)
                SetOutAttachRawV(true);
            if(fMaxMult>1)
                SetOutAttachRawMult(fOutAttachRawMult);
        }
        OutAttach(OutTTree);
    }
    else if(fMode == MODE_D2A)
    {
        if(fRawData)
        {
            SetHistogramsRaw(false);
            SetOutAttachRawI(fOutAttachRawI);
            SetOutAttachRawV(fOutAttachRawV);
            SetOutAttachRawF(fOutAttachRawF);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(fOutAttachCalI);
            SetOutAttachCalF(fOutAttachCalF);
            SetOutAttachCalV(fOutAttachCalV);
        }
        OutAttach(OutTTree);
    }
    else if(fMode == MODE_R2A)
    {

        SetInAttachRawV(true);
        InAttach(InTTree);

        if(fRawData)
        {
            SetOutAttachRawI(fOutAttachRawI);
            SetOutAttachRawV(fOutAttachRawV);
            SetOutAttachRawF(fOutAttachRawF);
        }

        if(fCalData)
        {
            SetOutAttachCalI(true);
            SetOutAttachCalI(fOutAttachCalI);
            SetOutAttachCalF(fOutAttachCalF);
            SetOutAttachCalV(fOutAttachCalV);
        }
        OutAttach(OutTTree);

    }
    else if(fMode == MODE_RECAL) // Recalibration mode
    {
        if(fCalData)
        {
            SetInAttachCalI(true);
            InAttachCal(InTTree);
        }
        if(fCalData)
        {
            SetOutAttachCalI(true);
            SetHistogramsCal(true);
        }
        OutAttach(OutTTree);
    }
    else if(fMode == MODE_CALC) // Calculation Mode No Output
    {
        SetNoOutput();
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }



    END;
}

#endif


///

void BaseDetector::RegisterParameters(ParManager* ParMan)
{
    START;

    //  ParMan = ParManager::getInstance();
    if(isComposite)
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->RegisterParameters(ParMan);
            // if(VerboseLevel >= V_INFO)
            // DetList->at(i)->PrintOptions(cout);
            ///		  DetList->at(i)->PrintOptions(L->File);
        }

    if(fRawData)
    {
        for(UShort_t i=0; i<NumberOfDetectors ; i++)
        {
            ParMan->Add(RawNameI[i],i,&Raw[i],1);
        }
    }
    if(fCalData)
    {
        for(UShort_t i=0; i<NumberOfDetectors ; i++)
        {
            ParMan->Add(CalNameI[i],i,&Cal[i],2);
        }
    }

    END;
}
