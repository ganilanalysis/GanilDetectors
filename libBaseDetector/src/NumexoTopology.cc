/****************************************************************************
 *    Copyright (C) 2012-2019 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "NumexoTopology.hh"
/**
 * @file   NumexoTopologu.cc
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   March 2019
 * @version 0.1
 *
 * @brief  Class of To Handle Numexo Topology Description File
 *
 *
 */

/**
 * Constructor
 */
NumexoTopology *NumexoTopology::instance = NULL;

NumexoTopology::NumexoTopology()
{
    START;
    NP = nullptr;
    NP = NUMEXOParameters::getInstance();

    END;
}


NumexoTopology::~NumexoTopology()  noexcept(false)
{
    START;
    NP = CleanDelete(NP);
    instance = nullptr;
    END;
}

NumexoTopology* NumexoTopology::getInstance()
{
    START;
    // A new instance of RootInput is created if it does not exist:
    if (instance == NULL) {
        instance = new NumexoTopology();
        cout << "New instance of NUMEXO topology " << instance << endl;

    }
    else
    {
        cout << "Foud existing instance of NUMEXO topology " << instance << endl;


    }

    // The instance of RootInput is returned:
    return instance;
    END;
}

void NumexoTopology::Init(const Char_t * ConfFileName)
{
    START;
    cout << "Reading Numexo Topology from " << ConfFileName << endl;

    Char_t Line[255];
    Char_t PName[300];
    UShort_t Len = 255;
    UShort_t fLine = 0;
    Char_t * Ptr = NULL;
    MIFile IF(ConfFileName);
    Char_t val[255];
    Int_t Ch;

    Char_t bname[255];
    Char_t btype[255];
    Int_t  bNr;
    Int_t  NCh;
    Int_t  NActiveCh;
    Int_t  NSamples;
    Int_t  StartSampleNr;
    Int_t  EndSampleNr;

    vector<string> EXOGAMParameters;
    EXOGAMParameters.push_back("Inner6M");
    EXOGAMParameters.push_back("Inner20M");
    EXOGAMParameters.push_back("DeltaT");
    EXOGAMParameters.push_back("Outer1");
    EXOGAMParameters.push_back("Outer2");
    EXOGAMParameters.push_back("Outer3");
    EXOGAMParameters.push_back("Outer4");
    EXOGAMParameters.push_back("BGO");
    EXOGAMParameters.push_back("CSI");
    Char_t A[4] = {'A','B','C','D'};



    while(IF.GetLine(Line,Len)){
        if( (Ptr = strstr(Line,"//")))
        {
            cout << Line << endl;
        }
        else
        {
            if(strcmp(Line, "****NUMEXOBoard****") == 0)
            {

                NActiveCh = -1;
                NCh = -1;
                NSamples = -1;
                bNr= -1;
                StartSampleNr = -1;
                EndSampleNr = -1;

                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"Name : %s",bname) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s is corrupted expecting Name : ",ConfFileName);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"Type : %s",btype) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s is corrupted (%s):  expecting Type : ",ConfFileName,bname);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"BoardNr : %d",&bNr) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s is corrupted (%s): expecting BoardNr :",ConfFileName,bname);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"Channels : %d",&NCh) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s is corrupted (%s): expecting Channels :",ConfFileName,bname);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                IF.GetLine(Line,Len);
                if( ! (sscanf(Line,"Samples : %d",&NSamples) == 1))
                {
                    Char_t Message[500];
                    sprintf(Message,"Config File %s is corrupted (%s):  expecting Samples :",ConfFileName,bname);
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
#ifdef DEBUG
                cout << "--------------------------------"<<endl;
                cout << "Numexo Board Config" << endl;
                cout << "--------------------------------"<<endl;
                cout << " Found Board : " << bname << endl;
                cout << "    type : " << btype << endl;
                cout << "    Board Nr : " << bNr << endl;
                cout << "    NChannels : " << NCh << endl;
                cout << "    NSamples  : " << NSamples << endl;
#endif

                if(strcmp(btype,"IC") == 0 || strcmp(btype,"TAC") == 0   )
                {
                    //     cout << "Track " << btype << " type " << endl;

                    IF.GetLine(Line,Len);
                    if( (! (sscanf(Line,"Active Channels  : %d",&NActiveCh) == 1)))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s): expecting Active Channels :",ConfFileName,bname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
#ifdef DEBUG
                    cout << "    Act. Chan. : " << NActiveCh << endl;
                    cout << "    Parameter Mapping : " << endl;
#endif
                    for(UShort_t i=0; i < NActiveCh; i++)
                    {
                        IF.GetLine(Line,Len);
                        if( (! (sscanf(Line,"Ch %d : %s",&Ch,PName) == 2)))
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting Ch00 : ParName",ConfFileName,bname);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        else{
#ifdef DEBUG
                            cout << "Chan " << i << "->" << Ch << " " <<  PName  << endl;
#endif
                            NP->Add(bNr,Ch,-1,PName);
                        }

                    }

                }
                else if(strcmp(btype,"TRACK_DC") == 0)
                {
                    //    cout << "Track DC type" << endl;
                    IF.GetLine(Line,Len);
                    if( ! (sscanf(Line,"Active Channels  : %d",&NActiveCh) == 1))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s): expecting Active Channels :",ConfFileName,bname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
//                    cout << "    Act. Chan. : " << NActiveCh << endl;
//                    cout << "    Parameter Mapping : " << endl;
                    for(UShort_t i=0; i < NActiveCh; i++)
                    {
                        IF.GetLine(Line,Len);
                        if( (! (sscanf(Line,"Ch %d : %d-%d",&Ch,&StartSampleNr,&EndSampleNr) == 3)))
                        {
                            Char_t Message[500];
                            sprintf(Message,"Config File %s is corrupted (%s): expecting Ch00 : 0-64",ConfFileName,bname);
                            MErr * Er= new MErr(WhoamI,0,0, Message);
                            throw Er;
                        }
                        else{

                            if(EndSampleNr-StartSampleNr+1>NSamples)
                            {
                                Char_t Message[500];
                                sprintf(Message,"Config File %s is corrupted (%s): Number of Sample (%d) smaller than range specified (%d-%d)",ConfFileName,bname,NSamples,StartSampleNr,EndSampleNr);
                                MErr * Er= new MErr(WhoamI,0,0, Message);
                                throw Er;
                            }
                            for(Int_t k=0; k<(EndSampleNr-StartSampleNr+1); k++)
                            {
                                sprintf(PName,"%s_%d",bname,StartSampleNr+k);
                                //cout << "Chan " << i << "->" << Ch << " " << StartSampleNr << " to " << EndSampleNr<< " " << PName << endl;

                                NP->Add(bNr,Ch,k,PName);

                            }

                            //
                        }

                    }


                } else if(strcmp(btype,"EXOGAM") == 0)
                {
                    Int_t FlangeNr=0;
                    Char_t CrNr1;
                    Char_t CrNr2;
                    IF.GetLine(Line,Len);
                    if( (! (sscanf(Line,"FlangeNr : %d",&FlangeNr) == 1)))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s, board %d): expecting FlangeNr :",ConfFileName,bname,bNr);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
#ifdef DEBUG
                    cout << "EXOGAM type" << endl;
                    cout << "    Flange Nr : " << FlangeNr << endl;
#endif
                    IF.GetLine(Line,Len);
                    if( (! (sscanf(Line,"CristalNr1 : %s",&CrNr1) == 1)))
                    {
                        cout << Line << endl;
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s, board %d)): expecting CristalNr1 : <A-D>",ConfFileName,bname,bNr);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
#ifdef DEBUG
                    cout << "    Cristal Nr1 : " << CrNr1 << endl;
#endif

                    IF.GetLine(Line,Len);
                    if( (! (sscanf(Line,"CristalNr2 : %s",&CrNr2) == 1)))
                    {
                        Char_t Message[500];
                        sprintf(Message,"Config File %s is corrupted (%s): expecting CristalNr2 : <A-D>",ConfFileName,bname);
                        MErr * Er= new MErr(WhoamI,0,0, Message);
                        throw Er;
                    }
#ifdef DEBUG
                    cout << "    Cristal Nr2 : " << CrNr2 << endl;
#endif

                    //


                    for(UShort_t i=0; i < EXOGAMParameters.size(); i++)
                    {
                        sprintf(PName,"%s_Fl%02d_%c",EXOGAMParameters[i].c_str(),FlangeNr,CrNr1);
                        NP->Add(bNr,0,i,PName);

                        sprintf(PName,"%s_Fl%02d_%c",EXOGAMParameters[i].c_str(),FlangeNr,CrNr2);
                        NP->Add(bNr,1,i,PName);
                    }





                }
                else if(strcmp(btype,"TRACK_TMW") == 0)
                {
                    cout << "Track T MW type" << endl;
                }
                else if(strcmp(btype,"TRACK_FPMW") == 0)
                {
                    cout << "Track FP MW type" << endl;
                }

            }

        }
    }

#ifdef DEBUG
    NP->Print();
#endif

    END;
}

