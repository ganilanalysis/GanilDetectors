/****************************************************************************
 *    Copyright (C) 2023 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "MesytecParameters.hh"
/**
 * @file   MesytecParameters.cpp
 * @author Antoine Lemasson (lemasson@ganil.fr)
 * @date   Jan 2023
 * @brief  Class of GaniExpt MesytecParameters
 *
 *
 */

/**
 * Constructor
 */
MesytecParameters *MesytecParameters::instance = NULL;

MesytecParameters* MesytecParameters::getInstance()
{
   // A new instance of RootInput is created if it does not exist:
   if (instance == NULL) {
     cout << "Initializing new instance of MesytecParameters Class " << instance << endl;
     instance = new MesytecParameters(60000);
   }
   else
   {
//      cout << "Mesytec Paramters Found Existing Class " << instance << endl;
   }
   return instance;
}


MesytecParameters::MesytecParameters(Int_t MaxPar)
{
  START;
  instance = this;
  MaxMesytecParameters = MaxPar;
  MaxBoards = 500;
  MaxChannels = 64;
  MaxSamples = 129;

  SetPointers();
  LogManager *LM = LogManager::getInstance();

  L = LM->GetFilePtr();
  AllocateArrays();
  Clear();
  END;
}

/**
 * Destructor
 */
MesytecParameters::~MesytecParameters(void)  noexcept(false) {
    START;
    instance = nullptr;
    PName = FreeDynamicArray<string>(PName);
    PBoard = FreeDynamicArray<Int_t>(PBoard);
    PChannel = FreeDynamicArray<Int_t>(PChannel);
    PSample = FreeDynamicArray<Int_t>(PSample);
    PMap = FreeDynamicArray<Int_t>(PMap, MaxBoards, MaxChannels);
    END;
}

/**
 * Force pointers to NULL when constructing the class
 */
void MesytecParameters::SetPointers(void)
{
  START;
  // Log File
  L = NULL;
  // MesytecParameters Names
  PName = NULL;
   // MesytecParameters Names
  PMap = NULL;
  END;
}

/**
 * Allocate MesytecParameters Arrays
 */
void MesytecParameters::AllocateArrays()
{
  START;
  // Parameter Names
  PName = AllocateDynamicArray<string>(MaxMesytecParameters);
  // Parameter Names
  PBoard= AllocateDynamicArray<Int_t>(MaxMesytecParameters);
  // Parameter Names
  PChannel = AllocateDynamicArray<Int_t>(MaxMesytecParameters);
  // Parameter Names
  PSample = AllocateDynamicArray<Int_t>(MaxMesytecParameters);
  // Parameter Map between Boards, Channel, Sample and ParId
  PMap = AllocateDynamicArray<Int_t>(MaxBoards,MaxChannels,MaxSamples);


  cout << "<MesytecParameters> Mem Use" << endl;
  cout << "<MesytecParameter s> PMap " << sizeof(PMap)*MaxBoards*MaxChannels*MaxSamples/1024 << "kb" <<  endl;
  END;
}

/**
 * Clear the parameter arrays
 */
void MesytecParameters::Clear(void)
{
  START;
  NParameters = 0;
  for (UShort_t i=0; i<MaxMesytecParameters; i++)
     {
        PName[i] = "";
                PBoard[i] = -1;
                PChannel[i] = -1;
                PSample[i] = -1;
     }
   for(UShort_t i=0;i < MaxBoards; i++)
    for(UShort_t j=0;j < MaxChannels; j++)
      for(UShort_t k=0; k < MaxSamples; k++)
         PMap[i][j][k] = -1;
  END;
}

/**
 * Insert a parameter in the list
 */
void MesytecParameters::Add(UShort_t Board, UShort_t Channel, Int_t Sample, string name)
{
  START;
  Char_t ParNameTmp[100];
  Char_t ParName[200];

  if(NParameters < MaxMesytecParameters)
     {
                ++NParameters;
                if(name == "")
                {
                    sprintf(ParNameTmp,"MESYTEC_b%03d_c%02d",Board,Channel) ;
                    if(Sample>-1)
                        sprintf(ParName,"%s_s%03d",ParNameTmp,Sample);
                }
                else{
                    sprintf(ParName,"%s",name.c_str()) ;
                }

                PName[NParameters] = ParName;
                PBoard[NParameters] = Board;
                PChannel[NParameters] = Channel;
                if(Sample>-1)
                {
                    PMap[Board][Channel][Sample] = NParameters;
                    PSample[NParameters] = Sample;

                }
                else
                {
                    PMap[Board][Channel][0] = NParameters;
                    PSample[NParameters] = 0;

                }


//                 cout << NParameters <<  " --> Added Parameter "<< ParName << " =>  Board : " << Board << " - Channel : " << Channel << " Sample : " << Sample  << endl;
//                 cout << PMap[Board][Channel][Sample] <<endl;

     }
  else
     {
        char Error[256];
        sprintf(Error,"Trying to add a parameter with ID (%d) is larger than MAX_PAR (%d) !",NParameters,MaxMesytecParameters);
        MErr * Er = NULL;
        Er = new MErr(WhoamI,0,0, Error);
        throw Er;
     }

  END;
}

/**
/ * Get Parameter Id from Board Chan Sample
 * Return -1 if not found
 */
Int_t MesytecParameters::GetParId(const Int_t& Board, const Int_t& Channel, const Int_t& Sample) {
    START;

    //Int_t ParId = -1;
#ifdef DEBUG
    cout << "Trying to retrieve a parameter with ID with (Board="<<Board <<",Channel="<< Channel << ",Sample=" << Sample << ")!" << endl;
#endif
    if (
        (Board < MaxBoards)
        && (Channel < MaxChannels)
        && (Sample < MaxSamples)
        ) {
      if (Sample>-1)
           return PMap[Board][Channel][Sample];
        else
           return PMap[Board][Channel][0];
    }
    else {

        char Error[256];
        sprintf(Error, "Trying to retrieve a parameter with ID with wrong (Board=%d,Channel=%d,Sample=%d)!", Board, Channel, Sample);
        cerr << Error << endl;
        MErr * Er = NULL;
        Er = new MErr(WhoamI, 0, 0, Error);
        throw Er;
    }
    //return ParId;
    END;
}

/**
 * Get Parameter Id from Name
 * Return -1 if not found
 */
UInt_t MesytecParameters::GetPar(string name) {
    START;
//   cout << "MyGetPar " <<  name << endl;

    for (Int_t i = 1; i < NParameters+1; i++) {
      //cout << PName[i] << " " << PBoard[i] << endl;
        if (PBoard[i] > 0)
            if (strcmp(PName[i].c_str(), name.c_str()) == 0)
              {
                //        cout << "Found ! " << i << endl;
                return i;
              }
    }
    //Could not find parameter
    char Error[256];
    sprintf(Error, "Could not find  parameter (%s) in parameter list !", name.c_str());
    MErr * Er = NULL;
    Er = new MErr(WhoamI, 0, 0, Error);
    throw Er;
    Er = CleanDelete(Er);
    return 0;
    END;
}


void MesytecParameters::AddBoard (Char_t* Name, UShort_t BoardId, UShort_t NChannels, Int_t NSamples){
  START;
  Char_t ParName[100];

    for (UShort_t i = 0; i < NChannels; i++) {
        if (NSamples>-1) {
            for (UShort_t j = 0; j < NSamples; j++) {
                sprintf(ParName, "%s_b%03d_c%02d_s%03d", Name, BoardId, i, j);
                Add(BoardId, i, j, ParName);
            }
        } else {
            sprintf(ParName, "%s_b%03d_c%02d", Name, BoardId, i);
            Add(BoardId, i, -1, ParName);
        }
    }
  END;
}
/**
 * Remove a parameter from the list using provided parameter id
 */
void MesytecParameters::Remove(UShort_t Pid)
{
  START;
  PName[Pid] = "";
  END;
}



/**
 * Print a dump of the parameters Map
 */
void MesytecParameters::Print(void)
{
  START;
  cout << "==========================================" << endl;
  cout << " Dump of the MesytecParameters List" << endl;
  cout << "==========================================" << endl;
  for (Int_t i =0; i<MaxMesytecParameters; i++)
     {
            if (PBoard[i]>-1)
            cout << "ParamId : " << i << " \t- Name : " << PName[i] << " \t ( b : " << PBoard[i] << " \t c: "<< PChannel[i] << " \t s: " << PSample[i] << ")" << endl;
     }
  cout << " Number of MesytecParameters : " << GetNParameters() << endl;
  cout << " Number of MesytecParameters : " << MaxMesytecParameters << endl;

  END;
}

void MesytecParameters::GetMesytecParametersFromConfigFile(const Char_t* FileName)
{

 START;
//  char Line[255];
//  UShort_t Len=255;

//  struct stat FileStat;
//  char* HomeDir = getenv("HOME");
//  char actionFilePAR[500];
//  char actionFilePAR2[100];
//  char BasePath[500];

//  MIFile *IF;
//  char Line[255];
//  stringstream *InOut;
//  UShort_t fLine;

//  do
//    {
//      if((fLine =IF->GetLine(Line,Len)) >= V_INFO)
//        {
//         if((Ptr = CheckComment(Line))) //Comment line
//           {
//             cout << Line << endl;
//           }
//         else // Can read
//           {

//             stringstream *InOut = new stringstream();
//             *InOut << Line;
//             *InOut >> BoardName;
//             *InOut >> BoardIp;
//             *InOut >> BoardChannels;
//             *InOut >> BoardSamples;

//               if(InOut->fail())
//                 return false;
//             }


//  InOut = CleanDelete(InOut);


// 	     if(GetCoeffs(Line,Array[CoeffsRead],NC))
// 	       {
// 		 if((Ptr = GetCommentPtr(Line)))
// 		   strcpy(Comment[CoeffsRead],Ptr);
// 		 CoeffsRead++;
// 	       }


// 	     else
// 	       {
// 		 Comment = FreeDynamicArray<Char_t>(Comment,NumberOfDetectors);
// 		 Char_t Message[200];
// 		 sprintf(Message,"In <%s><%s> the calibration file %s seems corrupted : Coeffs %d (out of %d)  has wrong format ! Expecting %d coefficients ", DetectorName, DetectorNameExt, IF->GetFileName(), CoeffsRead,  NumberOfDetectors-1, NC);
// 		 MErr * Er= new MErr(WhoamI,0,0, Message);
// 		 throw Er;
// 	       }
// 	   }
//        }
//    } while( (CoeffsRead < NumberOfDetectors) && (fLine>3));
//  IF->GetLine(Line,Len);



// //  cout << actionFilePAR << endl;
// //
// //  if (stat(actionFilePAR, &FileStat) < 0) {
// //
// //    strcpy(actionFilePAR, "");
// //    sprintf(actionFilePAR, "%s/ganacq_manip/%s/GECO/%s/ACQ/%s",
// //    			HomeDir, expname, expname,  actionFilePAR2);
// //    cout << actionFilePAR << endl;
// //
// //    if (stat(actionFilePAR, &FileStat) < 0) {
// //
// //      strcpy(actionFilePAR, "");
// //      sprintf(actionFilePAR, "%s/ganacq_manip/%s/das-save/%s",
// //              HomeDir, expname,  actionFilePAR2);
// //
// //      cout << actionFilePAR << endl;
// //      if (stat(actionFilePAR, &FileStat) < 0)
// //        {
// //
// //          Char_t Message[100];
// //          sprintf(Message,"Could not Find ACTION File : ");
// //          MErr * Er = new MErr(WhoamI,0,0, Message);
// //          throw Er;
// //        }
// //    }
// //  }
// //
// //  cout << " Found Parameter file : " << actionFilePAR <<endl;
// //
// //  GetMesytecParametersFromActionFile(actionFilePAR);
 //
   END;
 }


// Char_t* MesytecParameters::CheckComment(Char_t *Line)
// {
//   START;
//   Char_t *Ptr = NULL;
//   if((Ptr = strstr(Line,"//"))){
//     for(UShort_t i=0;i<(Ptr-Line) ;i++)
//       {
//        if(!IsWhiteSpace(Line+i))
//          {
//            Ptr = NULL;
//            break;
//          }
//       }
//   }
//   return Ptr;

//   END;
// }
