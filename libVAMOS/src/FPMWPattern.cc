/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "FPMWPattern.hh"

#define DEBUG

FPMWPattern::FPMWPattern(const Char_t *Name,
        UShort_t NDetectors,
        Bool_t RawData,
        Bool_t CalData,
        Bool_t DefaultCalibration,
        const Char_t *NameExt,
        UShort_t MaxMult
        )
: BaseDetector(Name, 0 , false, CalData, DefaultCalibration, false, NameExt, true) {
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors = RawData;
    fMaxMultSubDetectors = MaxMult;

    FP = NULL;
    DetManager *DM = DetManager::getInstance();
    FP = (FocalPosition*) DM->GetDetector("FP");



    LTS = nullptr;
    // LTS = (TimeStamp*) DM->GetDetector("AGAVA_VAMOSTS");;



    // Set How many IC rows
    AllocateComponents();

    char line[100];
    AddCounter("Mult MW > 1");

    if (CalData) {
       if(strcmp(DetectorName, "FPMW_Numexo") == 0)
         {
           // Overwrite cal names
           if(fCalData)
             {
//               sprintf(CalNameI[0],"FPMW_Nr");
//               sprintf(CalNameI[1],"FPMW_MW_T");
//               sprintf(CalNameI[2],"FPMW_XMW");
//               sprintf(CalNameI[3],"FPMW_XNoMW");
             }
         }
    }

    AllocateLocalArrays();

#ifdef WITH_ROOT
    // Setup Histogram Hierarchy
    SetHistogramsCal1DFolder("FPMW_C");
    SetHistogramsCal2DFolder("FPMW_C");
    SetHistogramsRaw1DFolder("FPMW_R");
    SetHistogramsRaw2DFolder("FPMW_R");
#endif


    END;
}

FPMWPattern::~FPMWPattern(void) {
    START;
    DeAllocateLocalArrays();

    END;
}

void FPMWPattern::AllocateLocalArrays(void) {
    START;

    END;
}

void FPMWPattern::DeAllocateLocalArrays(void) {
    START;

    END;
}

void FPMWPattern::ClearCal(void) {
    START;
    BaseDetector::ClearCal();
    if (isComposite) {
        for (UShort_t i = 0; i < DetList->size(); i++)
            DetList->at(i)->ClearCal();
    }

    END;
}

void FPMWPattern::AllocateComponents(void) {
    START;
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = false;
    Bool_t DefCal = true;
    Char_t Name[100];

//    if (strcmp(DetectorName, "TFPMW_Numexo") == 0)
//        sprintf(Name, "FPMWPat");
//    else
//        sprintf(Name, "FPMW");

    if (strcmp(DetectorName, "TFPMWPat_0") == 0)
    {
        sprintf(Name, "FPMWPat_0");
        DetType = 0;
    }
    else if (strcmp(DetectorName, "TFPMWPat_1") == 0)
    {
        sprintf(Name, "FPMWPat_1");
        DetType = 0;
    }
    else if (strcmp(DetectorName, "TSAMWPat") == 0)
    {
        sprintf(Name, "SAMWPat");
        DetType = 2;
    }
    else if (strcmp(DetectorName, "TTMWPat") == 0)
    {
        sprintf(Name, "TMWPat");
        DetType = 1;
    }
    else
    {
        sprintf(Name, "FPMW");
        DetType = 0;
    }

    cout << "Type of MW Pattern : " << Name << endl;
    BaseDetector *D = new BaseDetector(Name, NumberOfSubDetectors, RawData, CalData, DefCal, true, "",true,true,fMaxMultSubDetectors);
    if (CalData) {
        D->ReadCalGates();
    }
#ifdef WITH_ROOT
    D->SetMainHistogramFolder(MainHistogramFolder);
#endif
    AddComponent(D);

//    if (strcmp(DetectorName, "TFPMW_Numexo") == 0)
//        sprintf(Name, "FPMWTime");
//    else
//        sprintf(Name, "FPMWC");

    if (strcmp(DetectorName, "TFPMWPat_0") == 0)
        sprintf(Name, "FPMWTime_0");
    else if (strcmp(DetectorName, "TFPMWPat_1") == 0)
        sprintf(Name, "FPMWTime_1");
    else if (strcmp(DetectorName, "TSAMWPat") == 0)
        sprintf(Name, "SAMWTime");
    else if (strcmp(DetectorName, "TTMWPat") == 0)
        sprintf(Name, "TMWTime");
    else
        sprintf(Name, "FPMWC");

    RawData   = false;
    CalData   = true;
//    BaseDetector *DCal = new BaseDetector(Name, NumberOfSubDetectors, RawData, CalData, DefCal, false, "",false,false);
//#ifdef WITH_ROOT
//    DCal->SetMainHistogramFolder(MainHistogramFolder);
//#endif
//    AddComponent(DCal);

    END;
}

void FPMWPattern::SetParameters(NUMEXOParameters* Par, Map* Map) {
    START;
    Char_t Name[200];

   for (UShort_t i = 0; i < NumberOfSubDetectors; i++) {

       sprintf(Name, "%s_%02d", DetList->at(0)->GetName(), i);
       DetList->at(0)->SetParameterName(Name, i);
       DetList->at(0)->SetRawHistogramsParams(10000,0,65535);
       //DetList->at(0)->SetGateRaw(0,63999);
       DetList->at(0)->SetGateRaw(0,65535);


    }
   // For Time pattern
   //DetList->at(1)->SetCalHistogramsParams(2000,-1000,1000);


    NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
    for (UShort_t i = 0; i < DetList->size(); i++)
      DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);
    END;
}

Bool_t FPMWPattern::Treat(void) {
    START;

//    PrintRaw();
//   PrintCal();

    ULong64_t RefTS = 0;
    Ctr->at(0)++;

    if (isComposite) {
      for (UShort_t i = 0; i < DetList->size(); i++) {
        DetList->at(i)->Treat();
      }
    }

    if (fCalData) {
      // Build Multiplicity
      //cout << DetList->at(0)->HasTS() << endl;
      //
      //      RefTS = LTS->GetTS();
//      // if(RefTS>0)
//       {
//          for(UShort_t i=0;i<DetList->at(0)->GetRawTSM();i++)
//          {
// //                  cout << i
// //                       << " " << DetList->at(0)->GetRawTSAt(i)
// //                       << " " << DetList->at(0)->GetRawNrAt(i)
// //                       << " " << RefTS << " "
// //                       << DetList->at(0)->GetRawTSAt(i)-RefTS << endl;

// //            DetList->at(1)->SetCalData(DetList->at(0)->GetRawNrAt(i),DetList->at(0)->GetRawTSAt(i)-RefTS );

//          }
//       }



    }


    return (isPresent);
    END;
}

#ifdef WITH_ROOT

void FPMWPattern::CreateHistogramsCal1D(TDirectory *Dir) {
    START;

    string Name;
    Dir->cd("");
    if (SubFolderHistCal1D.size() > 0) {
        Name.clear();
        Name = SubFolderHistCal1D;
        if (!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }


    END;
}

void FPMWPattern::CreateHistogramsCal2D(TDirectory *Dir) {
    START;
    string Name;

    BaseDetector::CreateHistogramsCal2D(Dir);

    Dir->cd("");

    if (SubFolderHistCal2D.size() > 0) {
        Name.clear();
        Name = SubFolderHistCal2D;
        if (!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }


    END;
}

void FPMWPattern::SetOpt(TTree *OutTTree, TTree *InTTree) {
    START;
    Char_t Name[300];
    // Set histogram Hierarchy


    for (UShort_t i = 0; i < DetList->size(); i++) {
        switch(DetType)
        {
        case 0:
            SetMainHistogramFolder("FocalPosition");
            DetList->at(i)->SetMainHistogramFolder("");
            break;
        case 1:
            SetMainHistogramFolder("TargetPos");
            DetList->at(i)->SetMainHistogramFolder("");
            break;
        case 2:
            SetMainHistogramFolder("SecondArm");
            DetList->at(i)->SetMainHistogramFolder("");
            break;
        default :
            //SetMainHistogramFolder("");
            DetList->at(i)->SetMainHistogramFolder("");
            break;
        }
        DetList->at(i)->SetHistogramsRaw1DFolder(Form("%s_R",DetList->at(i)->GetName()));
        DetList->at(i)->SetHistogramsRaw2DFolder(Form("%s_R",DetList->at(i)->GetName()));
        DetList->at(i)->SetHistogramsCal1DFolder(Form("%s_C",DetList->at(i)->GetName()));
        DetList->at(i)->SetHistogramsCal2DFolder(Form("%s_C",DetList->at(i)->GetName()));
    }

    for (UShort_t i = 0; i < NumberOfSubDetectors; i++) {
       sprintf(Name, "%s_%02d",DetList->at(0)->GetName(), i);
       DetList->at(0)->SetParameterNameInRawTree(Name, i);
    }

    if (fMode == MODE_WATCHER) {
        if (fRawData) {
            SetHistogramsRaw(true);
        }
        if (fCalData) {
            SetHistogramsCal(true);
        }
        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasRawData()) {
                DetList->at(i)->SetHistogramsRaw(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
            }
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(true);
            }
        }
    } else if (fMode == MODE_D2R) {
        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasRawData()) {
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetHistogramsRaw1D(false);
                DetList->at(i)->SetHistogramsRawSummary(true);
                DetList->at(i)->SetOutAttachRawV(true);
                DetList->at(i)->SetOutAttachRawI(false);
                DetList->at(i)->SetOutAttachTS(true);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    } else if (fMode == MODE_D2A) {
        if (fRawData) {
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
        }
        if (fCalData) {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }

        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasRawData()) {
                DetList->at(i)->SetHistogramsRaw1D(false);
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
                if(DetType == 1) // TMW
                 {

                    DetList->at(i)->SetOutAttachRawI(true);
                    DetList->at(i)->SetOutAttachRawV(false);
                }
                else
                {
                    DetList->at(i)->SetOutAttachRawV(true);
                    DetList->at(i)->SetOutAttachRawI(false);
                }
                DetList->at(i)->SetOutAttachTS(true);

            }
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                if(i == 1)
                   DetList->at(i)->SetHistogramsCalSummary(true);
                else
                   DetList->at(i)->SetHistogramsCalSummary(false);

                if(DetType == 1) // TMW
                {
                    DetList->at(i)->SetOutAttachCalI(true);
                    DetList->at(i)->SetOutAttachCalV(false);
                }else {
                        DetList->at(i)->SetOutAttachCalV(false);
                        DetList->at(i)->SetOutAttachCalF(true);
                    }
            }
            DetList->at(i)->OutAttach(OutTTree);
        }

        OutAttach(OutTTree);
    } else if (fMode == MODE_R2A) {
        for (UShort_t i = 0; i < DetList->size(); i++) {
            DetList->at(i)->SetInAttachRawV(false);
            DetList->at(i)->InAttach(InTTree);
        }

        SetOutAttachCalI(true);

        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalF(true);
                DetList->at(i)->SetOutAttachCalV(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if (fMode == MODE_RECAL) {

        for (UShort_t i = 0; i < DetList->size(); i++) {
            SetInAttachCalI(true);
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetInAttachCalF(true);
            }
            DetList->at(i)->InAttachCal(InTTree);
        }
        InAttachCal(InTTree);
        SetOutAttachCalI(true);
        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalF(true);
                DetList->at(i)->SetOutAttachCalV(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    } else if (fMode == MODE_CALC) {
        SetNoOutput();
    } else {
        Char_t Message[500];
        sprintf(Message, "In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(), fMode);
        MErr * Er = new MErr(WhoamI, 0, 0, Message);
        throw Er;
    }
    END;
}





#endif
