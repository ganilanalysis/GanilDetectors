/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *  
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "FatimaNew.hh"

FatimaNew::FatimaNew(const Char_t *Name,
                         UShort_t NDetectors, 
                         Bool_t RawData, 
                         Bool_t CalData, 
                         Bool_t DefaultCalibration, 
                         const Char_t *NameExt
                         )
: BaseDetector(Name, 2, false, CalData, DefaultCalibration,false,NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  DetManager *DM = DetManager::getInstance();

  AllocateComponents(); 

 
  if(CalData)
	 {
		sprintf(CalNameI[0],"SiETNr");
		sprintf(CalNameI[1],"SiET");
	 }

  END;
}
FatimaNew::~FatimaNew(void)
{
  START;
  END;

}


void FatimaNew::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;
  
  // Fatima E
  BaseDetector *D = new BaseDetector("FATIMA_E",NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      D->SetParameterName("%s_%02d");
      D->SetGateRaw(0,16384);
    }
  D->SetCalHistogramsParams(7000,0,7000,"keV");
  AddComponent(D); 

  // Fatima TDC
  BaseDetector *DT = new BaseDetector("FATIMA_T",NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      DT->SetParameterName("%s_%02d");
      DT->SetGateRaw(0,16384);
    }
  DT->SetCalHistogramsParams(1000,0,2000,"ns");
  AddComponent(DT);
  // TC
  BaseDetector *TAGATA = new BaseDetector("TAC_AGATA_FATIMA",1,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      TAGATA->SetParameterName("TAC_AGATA_FATIMA");
      TAGATA->SetGateRaw(0,16384);
    }
  TAGATA->SetCalHistogramsParams(1000,0,2000,"ns");
  if(CalData)
    TAGATA->SetCalName("TAC_AGATA_FATIMA_C",0);
  AddComponent(TAGATA);
 // TC
  // BaseDetector *TFATIMA = new BaseDetector("TAC_HF_FATIMA",1,RawData,CalData,DefCal,PosInfo,"");
  // if(RawData)
  //   {
  //     TFATIMA->SetParameterName("TAC_HF_FATIMA");
  //     TFATIMA->SetGateRaw(0,16384);
  //   }
  // if(CalData)
  //   TFATIMA->SetCalName("TAC_HF_FATIMA_C",0);
  // TFATIMA->SetCalHistogramsParams(1000,0,2000,"ns");
  // AddComponent(TFATIMA);
  // TC
  BaseDetector *TMW_FATIMA = new BaseDetector("TAC_TMW_FATIMA",1,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      TMW_FATIMA->SetParameterName("TAC_TMW_FATIMA");
      TMW_FATIMA->SetGateRaw(0,16384);
    }
  if(CalData)
    TMW_FATIMA->SetCalName("TAC_TMW_FATIMA_C",0);
  TMW_FATIMA->SetCalHistogramsParams(1000,0,2000,"ns");
  AddComponent(TMW_FATIMA);

  

  END;
}

void FatimaNew::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  for(UShort_t i=0; i< DetList->size(); i++)
    DetList->at(i)->SetParameters(Par,Map);
  END;
}

Bool_t FatimaNew::Treat(void)
{ 
  START;
  Ctr->at(0)++;
  
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        {
          DetList->at(i)->Treat();
        }
    }
  if(fCalData)
	  {

	  }
	return(isPresent); 
  END;
}



#ifdef WITH_ROOT
void FatimaNew::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  for(UShort_t i = 0;i<DetList->size();i++)
	 {
		DetList->at(i)->SetMainHistogramFolder("");
		DetList->at(i)->SetHistogramsRaw1DFolder("Fatima_R");
		DetList->at(i)->SetHistogramsRaw2DFolder("Fatima_R");
		DetList->at(i)->SetHistogramsCal1DFolder("Fatima_C");
		DetList->at(i)->SetHistogramsCal2DFolder("Fatima_C");
		SetHistogramsCal1DFolder("Fatima_C");
		SetHistogramsCal2DFolder("Fatima_C");
	 }
 
  if(fMode == MODE_WATCHER)
    {
		if(fRawData)
		  {
			 SetHistogramsRaw(true);
		  }
		if(fCalData)
		  {
			 SetHistogramsCal(true);
		  }
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			  if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw(true);
				  if(i < 2 ) DetList->at(i)->SetHistogramsRawSummary(true);
				}
			  if(DetList->at(i)->HasCalData())
				 {
				  DetList->at(i)->SetHistogramsCal(true);
				  if(i < 2 ) DetList->at(i)->SetHistogramsCalSummary(true);
				}
		  }
	 }
  else if(fMode == MODE_D2R)
	 {
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw(true);
				  if(i < 2 ) 
					 {
						DetList->at(i)->SetHistogramsRawSummary(true);
						DetList->at(i)->SetOutAttachRawV(true);
						DetList->at(i)->SetOutAttachRawI(false);
					 }
				  else
					 {			
						DetList->at(i)->SetOutAttachRawI(true);
					 }
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
	 }
  else if(fMode == MODE_D2A)
	 {
		if(fRawData)
		  {
			 SetHistogramsRaw(false);
			 SetOutAttachRawI(false);
			 SetOutAttachRawV(false);
		  }
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
			 SetOutAttachCalV(false);
		  }
		
		for(UShort_t i = 0;i<DetList->size();i++)
		  {			
			 if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw1D(true);
				  DetList->at(i)->SetHistogramsRaw2D(true);
				  
				   if(i < 2) 
					 {
						DetList->at(i)->SetHistogramsRawSummary(true);
						DetList->at(i)->SetOutAttachRawV(false);
						DetList->at(i)->SetOutAttachRawI(false);			 					  
					  
					 }
					else
					  {
						 DetList->at(i)->SetOutAttachRawI(true);
					  }
				}
			 if(DetList->at(i)->HasCalData())
				{
				  DetList->at(i)->SetHistogramsCal1D(true);
				  DetList->at(i)->SetHistogramsCal2D(true);
				  if(i < 2)
					 {
						DetList->at(i)->SetHistogramsCalSummary(true);
						DetList->at(i)->SetOutAttachCalI(false);
						DetList->at(i)->SetOutAttachCalV(true);			 
					 }
				  else
					 {
						DetList->at(i)->SetHistogramsCalSummary(false);
						DetList->at(i)->SetOutAttachCalI(true);
					 }
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			  if(i < 2 ) 
				 DetList->at(i)->SetInAttachRawV(true);
			  else
				 DetList->at(i)->SetInAttachRawI(true);
			 DetList->at(i)->InAttach(InTTree);
		  }
		
		SetOutAttachCalI(true);

		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			  if(DetList->at(i)->HasCalData())
				{
				  DetList->at(i)->SetHistogramsCal1D(true);
				  DetList->at(i)->SetHistogramsCal2D(true);
				  if(i < 2 ) 
					 {
						DetList->at(i)->SetHistogramsCalSummary(true);
						DetList->at(i)->SetOutAttachCalI(false);
						DetList->at(i)->SetOutAttachCalV(true);			 
					 }
				  else
					 {
						DetList->at(i)->SetHistogramsCalSummary(false);
						DetList->at(i)->SetOutAttachCalI(true);
						// DetList->at(i)->SetOutAttachRawI(true);
					 }
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
    }
  else if (fMode == MODE_RECAL)
    {
      	for(UShort_t i = 0;i<DetList->size();i++)
	  {
	    if(i < 2 ) 
	      DetList->at(i)->SetInAttachCalV(true);
	    else
	      DetList->at(i)->SetInAttachCalI(true);
	    
	    DetList->at(i)->InAttachCal(InTTree);					 
	  }
	SetInAttachCalI(true);
	InAttachCal(InTTree);

	SetOutAttachCalI(true);

	for(UShort_t i = 0;i<DetList->size();i++)
	  {
	    if(DetList->at(i)->HasCalData())
	      {
		DetList->at(i)->SetHistogramsCal1D(true);
		DetList->at(i)->SetHistogramsCal2D(true);
		if(i < 2 ) 
		  {
		    DetList->at(i)->SetHistogramsCalSummary(true);
		    DetList->at(i)->SetOutAttachCalI(false);
		    DetList->at(i)->SetOutAttachCalV(true);			 
		  }
		else
		  {
		    DetList->at(i)->SetHistogramsCalSummary(false);
		    DetList->at(i)->SetOutAttachCalI(true);
		    // DetList->at(i)->SetOutAttachRawI(true);
		  }
	      }
	    DetList->at(i)->OutAttach(OutTTree);
	  }
	OutAttach(OutTTree);
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}


#endif
