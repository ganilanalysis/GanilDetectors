/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "TPPAC.hh"
//#define TPPAC_TRACE

TPPAC::TPPAC(const Char_t *Name, 
				 UShort_t NDetectors, 
				 Bool_t RawData, 
				 Bool_t CalData, 
				 Bool_t DefaultCalibration, 
				 const Char_t *NameExt)
  : BaseDetector(Name, 22, false, CalData, DefaultCalibration, false, NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  AllocateComponents();
  AddCounter("Present X"); //2
  AddCounter("Present X-SECHS"); //3
  AddCounter("Present X-WA"); //4
  AddCounter("Saturated X"); //5
  AddCounter("Present Y"); //6
  AddCounter("Present Y-SECHS"); //7
  AddCounter("Present Y-WA"); //8
  AddCounter("Saturated Y"); //9
#ifdef GENSECHIP
  cout <<  "GENSECHIP defined" << endl;
  AddCounter("GENSECHIP X"); //10
  AddCounter("GENSECHIP Y"); //11
#endif
#ifdef GENSECHIP
  g = new TGraph(0);
#endif 

  if(fCalData)
    {
		sprintf(CalNameI[0],"%s_X", DetectorName);
		sprintf(CalNameI[1],"%s_Y", DetectorName);
		sprintf(CalNameI[2],"%s_XWA", DetectorName);
		sprintf(CalNameI[3],"%s_YWA", DetectorName);
		sprintf(CalNameI[4],"%s_QX", DetectorName);
		sprintf(CalNameI[5],"%s_QY", DetectorName);
		sprintf(CalNameI[6],"%s_MaxQX", DetectorName);
		sprintf(CalNameI[7],"%s_MaxQY", DetectorName);
		sprintf(CalNameI[8],"%s_MaxNrX", DetectorName);
		sprintf(CalNameI[9],"%s_MaxNrY", DetectorName);
		sprintf(CalNameI[10],"%s_QRX", DetectorName);
		sprintf(CalNameI[11],"%s_QRY", DetectorName);
		sprintf(CalNameI[12],"%s_MX", DetectorName);
		sprintf(CalNameI[13],"%s_MY", DetectorName);
		sprintf(CalNameI[14],"%s_MX_f", DetectorName);
		sprintf(CalNameI[15],"%s_MY_f", DetectorName);
#ifdef GENSECHIP      
		sprintf(CalNameI[16],"%s_XSG", DetectorName);
		sprintf(CalNameI[17],"%s_YSG", DetectorName);
#else
		sprintf(CalNameI[16],"%s_XRa", DetectorName);
		sprintf(CalNameI[17],"%s_YRa", DetectorName);
		sprintf(CalNameI[18],"%s_XQLM", DetectorName);
		sprintf(CalNameI[19],"%s_YQLM", DetectorName);
		sprintf(CalNameI[20],"%s_XQRM", DetectorName);
		sprintf(CalNameI[21],"%s_YQRM", DetectorName);
#endif
     // X in  [-1500, 0]
      SetGateCal(-2000,2000,0);
      // Y in [0,500]
      SetGateCal(-2000,2000,1);
      //  XWA  in  [-1500, 0]
      SetGateCal(-2000,2000,2);
      // YWA in [0,500]
      SetGateCal(-2000,2000,3);
      // QX in [0,500]
      SetGateCal(-100,10000,4);
      // QY in [0,500]
      SetGateCal(-100,10000,5);    
      // PadMaxQX in [0,500]
      SetGateCal(-100,10000,6);
		// PadMaxQY in [0,500]
      SetGateCal(-100,10000,7);
		// PadMaxQNrX 
      SetGateCal(0,64,8);
      // PadMaxQNrX 
      SetGateCal(0,64,9);
      // QMax/Q X
      SetGateCal(-2,1,10);
      // QMax/Q Y
      SetGateCal(-2,1,11);
      // MX
      SetGateCal(0,64,12);
      // MY
      SetGateCal(0,64,13);
      // MX(free)
      SetGateCal(0,64,14);
      // MY(free)
      SetGateCal(0,64,15);
#ifdef GENSECHIP      
cout <<  "GENSECHIP defined" << endl;
      // XGS
      SetGateCal(-2000,2000,16);
      // YGS
      SetGateCal(-2000,2000,17);      
#endif
#ifdef PPAC_RATIO
cout <<  "PPAC_RATIO defined" << endl;
      // XGS
      SetGateCal(-2000,2000,16);
      // YGS
      SetGateCal(-2000,2000,17);      
      // XQL/QM
      SetGateCal(0,1,18);
      // XQR/QM
      SetGateCal(0,1,19);      
      // YQL/QM
      SetGateCal(0,1,20);
      // YQR/QM
      SetGateCal(0,1,21);      
      
#endif

    }
  //  ReadCalibration();

#ifdef WITH_ROOT
  // Setup Histogram Hierarchy
  SetHistogramsCal1DFolder("Ppac1D");
  SetHistogramsCal2DFolder("Ppac2D");
#endif

  END;
}
TPPAC::~TPPAC(void)
{
  START;
#ifdef GENSECHIP
  delete g;
#endif
  END;
}
void TPPAC::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;
 
  string BaseName;
  string Name;
  BaseName = DetectorName ;
  Name = BaseName + "_0";
  BaseDetector *XStrips = new BaseDetector(Name.c_str(),128,RawData,CalData,DefCal,PosInfo,"");
  XStrips->AddCounter("Saturated");
  XStrips->AddCounter("Not-Neighbours (even Dist.)");
  XStrips->AddCounter("Strips M > 3");
  XStrips->AddCounter("Strips M > 3 (contiguous)");
  XStrips->AddCounter("Almost Neighbours ");
#ifdef PPAC_RATIO
  XStrips->AddCounter("Strips RA == 2");
#endif
   if(RawData)
	 {
		XStrips->SetRawHistogramsParams(4096,0,4096,"Raw");
	 }
    if(CalData)
    {
      XStrips->SetGateCal(50,4095);
      XStrips->SetCalHistogramsParams(4096,0,4096,"MeV","Cal");
      XStrips->SetMainHistogramFolder(Name.c_str());
    }
  AddComponent(XStrips);
  Name.clear();
  Name = BaseName + "_1";
  BaseDetector *YStrips = new BaseDetector(Name.c_str(),192,RawData,CalData,DefCal,PosInfo,"");
  YStrips->AddCounter("Saturated");
  YStrips->AddCounter("Not-Neighbours (even Dist.)");
  YStrips->AddCounter("Strips M >3");
  YStrips->AddCounter("Strips M >3 (contiguous)");
  YStrips->AddCounter("Almost Neighbours ");
#ifdef PPAC_RATIO
  YStrips->AddCounter("Strips RA == 2");
#endif
  if(RawData)
	 {
		YStrips->SetRawHistogramsParams(4096,0,4096,"Raw");
	 }
  if(CalData)
    {
		YStrips->SetGateCal(50,4095);
		YStrips->SetMainHistogramFolder(Name.c_str());
		YStrips->SetCalHistogramsParams(4096,0,4096,"MeV","Cal");
	 }
 
  AddComponent(YStrips);

  Name.clear();
  Name = BaseName + "_SI";
  BaseDetector *SI = new BaseDetector(Name.c_str(),3,RawData,fCalData,DefCal,PosInfo,"");
  SI->SetParameterName("E_SI",0);
  SI->SetParameterName("T_SI_PPAC",1);
  SI->SetParameterName("T_PPAC1_PPAC2",2);
  SI->SetRawHistogramsParams(1000,0,16383,"Raw");
  SI->SetHistogramsRaw(true);
  if(CalData)
    {
      SI->SetCalName("SIE",0);
      SI->SetCalName("SIT",1);
      SI->SetCalName("TP1_TP2",2);
      SI->SetGateCal(0,16384);
#ifdef WITH_ROOT
      SI->SetHistogramsCal1D(true);
      SI->SetHistogramsCal2D(true);
      SI->SetOutAttachCalI(true);
#endif
    }
  AddComponent(SI);


// #ifdef TPPAC_TRACE
//   cout <<  "TPPAC_TRACE defined" << endl;

//   // X Strips pattern
//   Name.clear();
//   Name = BaseName + "_0Tr";
//   BaseDetector *TrX = new BaseDetector(Name.c_str(),64,false,true,true,false,"");
//   if(CalData)
//     {
// 		TrX->SetGateCal(0,4094);
// 		TrX->SetOutAttachCalV(true);
// 		TrX->SetHistogramsCal(true);
//     }  
//   AddComponent(TrX);
//   Name.clear();
//   Name = BaseName + "_1Tr";
//   BaseDetector *TrY = new BaseDetector(Name.c_str(),64,false,true,true,false,"");
//   if(CalData)
//     {
// 		TrY->SetGateCal(0,4094);
// 		TrY->SetOutAttachCalV(true);
// 		TrY->SetHistogramsCal(true);
//     }  
//   AddComponent(TrY);

//   // X Strips pattern
//   Name.clear();
//   Name = BaseName + "_0TrM";
//   BaseDetector *TrXM = new BaseDetector(Name.c_str(),64,false,true,true,false,"");
//   if(CalData)
//     {
// 		TrXM->SetGateCal(0,4094);
// 		TrXM->SetOutAttachCalV(true);
// 		TrXM->SetHistogramsCal(true);
//     }  
//   AddComponent(TrXM);
//   Name.clear();
//   Name = BaseName + "_1TrM";
//   BaseDetector *TrYM = new BaseDetector(Name.c_str(),64,false,true,true,false,"");
//   if(CalData)
//     {
// 		TrYM->SetGateCal(0,4094);
// 		TrYM->SetOutAttachCalV(true);
// 		TrYM->SetHistogramsCal(true);
//     }  
//   AddComponent(TrYM);
// #endif

  END;
}

void TPPAC::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  if(isComposite){
    Char_t PName[20];	 
    // PPAC_0 ->X PPAC_X_VOIE_YY
    for(UShort_t i=0; i<DetList->at(0)->GetNumberOfDetectors(); i++)
      {
        sprintf(PName,"MW%d_%d",0,i);
		  DetList->at(0)->SetParameterName(PName,i);

      }
     
#ifdef PPAC_SWAP
		  cout <<  "Swap of PPAC_1 defined" << endl;
#endif
   // PPAC_2 -> Y PPAC_X_VOIE_YY
    for(UShort_t i=0; i<DetList->at(1)->GetNumberOfDetectors(); i++)
      {
        //		  sprintf(PName,"PPAC_%d_VOIE_%d",1,i);
        sprintf(PName,"MW%d_%d",1,i);
#ifdef PPAC_SWAP
		  UShort_t Chan = 0;
		  if(i%2 == 0)
			 Chan = i+1;
		  else
			 Chan = i-1;
#ifdef DEBUG_PP
		  cout << i  << " -> GPLX " << Chan << endl;
#endif
		  DetList->at(1)->SetParameterName(PName,Chan);
#else
		  DetList->at(1)->SetParameterName(PName,i);	    
#endif
      }
    
	 for(UShort_t i=0; i< DetList->size(); i++)
      DetList->at(i)->SetParameters(Par,Map);
  }
  
  END;
}

Bool_t TPPAC::Treat(void)
{
  START;
 
  if(isComposite)
    {
      // TreatWires
      if(DetList->at(0)->GetRawM())
	CalibrateWire(0);
      if(DetList->at(1)->GetRawM())
	CalibrateWire(1);
      // Treat Silicon detector
      if(DetList->at(2)->GetRawM())
	DetList->at(2)->Treat();
    }

  if(DetList->at(0)->IsPresent())
    {
      fPresentX = TreatWire(0);
    }
  else
    fPresentX = false;

  if(DetList->at(1)->IsPresent())
    {
      fPresentY = TreatWire(1);
    }
  else
    fPresentY = false;
	
  if(fPresentX && fPresentY)
    isPresent = true;

  return isPresent;
  END;
}

Bool_t TPPAC::TreatWire(UShort_t n)
{
  START;

  UShort_t NStrips = 3 ;
  Float_t QTmp[64];
  Float_t QMax;
  UShort_t NMax;
  UShort_t FStrip[64];
  Bool_t Neighbours;
  Float_t X=-1500.;
  Float_t XWA=-1500.;
  Bool_t Present = false;

  if(DetList->at(n)->GetM() >= NStrips)
	 Ctr->at(2+n*4)++;
  else
	 {
		return false;
	 }

  for(UShort_t j=0; j< DetList->at(n)->GetM();j++) //loop over strips
    {
      QTmp[j] = DetList->at(n)->GetCalAt(j);// Copy Charges
    }
  // Find GetM highest charges NStrips
  for(UShort_t k=0;k<DetList->at(n)->GetM(); k++)
    {
      QMax=0.0;
      NMax=0;
      for(UShort_t j=0;j<DetList->at(n)->GetM();j++)
		  {
			 if(QTmp[j] > QMax)
				{
				  QMax = QTmp[j];
				  NMax = j;
				}
		  }
      QTmp[NMax] = 0.0;
      FStrip[k] = NMax;
    }
  
  if(NStrips != 3) 
    {
      MErr * Er = new MErr(WhoamI,0,0, "NStrips != 3 but");
      throw Er;      
    }
  Neighbours = false;
  
  if((Neighbours = CheckNeighbours(FStrip,n)))
    {
      //      DetList->at(n)->PrintCal();
      X = SECHIP(FStrip,n);
      // cout << "Sechip " <<  X << endl;
    }
  // else
  //   {
  //  DetList->at(n)->PrintCal();
  XWA = WeightedAverage(FStrip,NStrips,n);
  if((XWA > 0 && XWA < 64) || (X > 0 && X < 64)) 
    {
      Present = true;
    }
  SetCalData(n,X);
  SetCalData(n+2,XWA);

  
  return Present;
  END;
}

void TPPAC::CalibrateWire(UShort_t n,UShort_t Saturation)
{
  START;
  // Check the there is no saturated wires 
  
  for(UShort_t i=0;i<DetList->at(n)->GetRawM();i++)
    if (DetList->at(n)->GetRawAt(i) == Saturation)
      {
		  // cout << "/!\ Saturated Wire " << DetList->at(n)->GetRawNrAt(i) << " in Plane " << n << endl;
		  Ctr->at(5+n*4)++;
		  DetList->at(n)->IncrementCounter(2);
		  DetList->at(n)->SetPresent(false);
		  // DetList->at(n)->PrintRaw(); 
		  return;
      }
  DetList->at(n)->Calibrate();
  if(DetList->at(n)->GetM())
    {
      DetList->at(n)->SetPresent(true);
      SetCalData(n+14,DetList->at(n)->GetM());
    }
  
  END;
}

Bool_t TPPAC::CheckNeighbours(UShort_t *FStrip, UShort_t n)
{
  START;
  Bool_t rval = false;
  UShort_t tmp = 0;
  if( abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[1])) == 1 
      &&
      abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[2])) == 1 )
    return true;
  else
    {
      DetList->at(n)->IncrementCounter(3);
// #ifdef TPPAC_TRACE
//       for(UShort_t k=0; k<DetList->at(n)->GetM(); k++)
// 	{
// 	  DetList->at(5+n)->SetCalData(DetList->at(n)->GetNrAt(FStrip[k]),DetList->at(n)->GetCalAt(FStrip[k]));
// 	}
// #endif
      if(DetList->at(n)->GetM()>3)
	{

	  if( abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[1])) == 1 
	      && 
	      abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[3])) == 1)
	    {
	      Float_t Diff = (DetList->at(n)->GetCalAt(FStrip[2])-DetList->at(n)->GetCalAt(FStrip[3]))/0.5/(DetList->at(n)->GetCalAt(FStrip[2])+DetList->at(n)->GetCalAt(FStrip[3]))*100 ;
	      // cout << " Were Almost neighbours" << endl;
	      // cout <<  DetList->at(n)->GetCalAt(FStrip[1]) << endl;
	      // cout <<  DetList->at(n)->GetCalAt(FStrip[2]) << endl;
	      // cout <<  DetList->at(n)->GetCalAt(FStrip[3]) << endl;
	      // cout << "\t " << Diff << "% difference"<< endl;
				  
	      if (Diff < 15) 
		{						
		  tmp = FStrip[2];
		  FStrip[2] = FStrip[3] ;
		  FStrip[3] = tmp;	
		  rval = true;
		  DetList->at(n)->IncrementCounter(6);
		}
	    }
	}

      return rval ;
    }
  END; 
}

Float_t TPPAC::WeightedAverage(UShort_t *FStrip, UShort_t NStrips, UShort_t n)
{ 
  START;
  Float_t v[2];
  Float_t QTotal=-10.;
  Float_t QRatio=-1.;
  // UShort_t StripsWA=0;
  UShort_t StripsWA=0;
  Float_t XWA=-1500.;
#ifdef GENSECHIP
cout <<  "GENSECHIP defined" << endl;
  Float_t XSG=-1500.;
#endif
  UShort_t MaxWireNr[64];
  Float_t LStripQ[64];
  Bool_t isPresent = false;
#ifdef PPAC_RATIO
  Float_t vra[6];
  Float_t QRatioL =0;
  Float_t QRatioR =0;
  UShort_t NRatioL =0;
  UShort_t NRatioR =0;
  UShort_t StripsRa =0;
  Float_t XRa=-1500.;
#endif  
  if(DetList->at(n)->GetM() > NStrips)
    {
      DetList->at(n)->IncrementCounter(4);
      //Looking for entire peak for W.A.
      //The Strips are NOT ordered  0-64 ...
      //Coul be done earlier but ....

      // Reoder the strips ...


      for(UShort_t j=0; j< DetList->at(n)->GetNumberOfDetectors();j++) 
		  {
			 LStripQ[j]=0;
			 MaxWireNr[63] = 0.;
		  }
      MaxWireNr[0] = DetList->at(n)->GetNrAt(FStrip[0]);
#ifdef  DEBUG_PP
		cout << "--> <"<<n<< "> Start ----------------------" <<endl;
#endif
	   
      for(UShort_t j=0; j< DetList->at(n)->GetM();j++) //loop over strips
		  {	       
			 LStripQ[DetList->at(n)->GetNrAt(j)] = DetList->at(n)->GetCalAt(j);// Copy Charges
#ifdef DEBUG_PP	 
			 cout << j << " " << DetList->at(n)->GetNrAt(j) << "\t Cal : " <<  DetList->at(n)->GetCalAt(j) << endl;
#endif
		  }
	   
#ifdef  DEBUG_PP
		for(UShort_t j=0; j< DetList->at(n)->GetNumberOfDetectors();j++) 
        {
			 cout << "Nr : " << j <<  " Q: "  << LStripQ[j];
			 if(j == MaxWireNr[0])  cout << "   <--- Max " ;
			 cout <<endl;
		  }

		cout << "Max Pad Nr First " << FStrip[0] << " -> " << DetList->at(n)->GetNrAt(FStrip[0]) << endl;
      
#endif
 

      StripsWA=0;

    
      for(Int_t j=MaxWireNr[0]-1;j>=0;j--)
	{
	  if((LStripQ[j] <= LStripQ[j+1]) && LStripQ[j] > 0)
	    {		   
#ifdef PPAC_RATIO
	      if(j == MaxWireNr[0]-1)
		{
		  QRatioL = LStripQ[j];
		  NRatioL = j;		  
		  StripsRa++;
		}
#endif      
	      StripsWA++;
	      MaxWireNr[StripsWA]=j;
	    }
	  else
	    break;
	}
	   
      for(Int_t j=MaxWireNr[0]+1;64;j++)
	{
	  if((LStripQ[j-1] >= LStripQ[j]) && LStripQ[j] > 0)
	    {
#ifdef PPAC_RATIO
	      if(j == MaxWireNr[0]+1)
		{
		  QRatioR = LStripQ[j];
		  NRatioR = j;		  
		  StripsRa++;
		}
#endif
	      StripsWA++;
	      MaxWireNr[StripsWA]=j;			  
	    }
	  else
	    break;
	}
	       
	       
#ifdef DEBUG_PP
      cout << "TPPAC::TreatWire("<<n<<")::Weithed Average: "  << endl;
      cout << "StripsWA: " << StripsWA << endl; 
#endif	     
      if(StripsWA >= NStrips)
	{
	  DetList->at(n)->IncrementCounter(5);
	  v[0] = v[1] = 0.0;
	  for(UShort_t k=0;k<=StripsWA;k++)
	    {		      
	      v[0] += LStripQ[MaxWireNr[k]] * ((Float_t) MaxWireNr[k]);
	      v[1] += LStripQ[MaxWireNr[k]];
#ifdef GENSECHIP
	      g->SetPoint(g->GetN(),MaxWireNr[k],LStripQ[MaxWireNr[k]]);
#endif
	    }
			 
	  XWA = v[0] / v[1];
	  QTotal = v[1];
	  QRatio = LStripQ[MaxWireNr[0]]/QTotal;
#ifdef DEBUG_PP
	  // cout << "TPPAC::TreatWire("<<n<<")::Weithed Average: "  << endl;
	  // cout << "StripsWA: " << StripsWA << endl;
	  for(UShort_t k=0;k<=StripsWA;k++)
	    cout <<  StripsWA << " WIREWA: " << MaxWireNr[k] << " Q: " << LStripQ[MaxWireNr[k]] << endl;
	  cout << "Result 2: " << XWA << "mm" << endl ;
	  cout << "Total Charge 2: " << QTotal << endl;
#endif
	 	    
	  XWA *= 1; 
	  if(XWA > 0. && XWA<64.)
	    {
	      isPresent = true;
	      // XWA -= GetRefX();
	      Ctr->at(4+n*4)++;		  
	    }
	  else 
	    {
	      XWA = -1500.;
	    }
	  
#ifdef PPAC_RATIO
	  Float_t b = 2.0;
	  if(StripsRa == 2)
	    {
	      vra[0] = QRatioR / LStripQ[MaxWireNr[0]];
	      vra[1] = QRatioL / LStripQ[MaxWireNr[0]];
	      vra[3] = (1-vra[1]) / (1-vra[0]);
	      vra[4] = atan(vra[3]);
	      vra[5] = atan(b*(vra[4]-M_PI/4.))/atan(b*M_PI/4.);
	      XRa = MaxWireNr[0] +  0.5* vra[5];
	      // cout << "\n-" << n << "-\nRatio Method X : " << XRa << " mm" << endl;
	      // cout << "WA Method X : " << XWA << " mm" << endl;
	      // cout << NRatioL <<  "\t" << LStripQ[NRatioL] << endl;
	      // cout << MaxWireNr[0] << "\t" << LStripQ[MaxWireNr[0]]  << endl;
	      // cout << NRatioR <<  "\t" << LStripQ[NRatioR] << endl;
	      DetList->at(n)->IncrementCounter(7);

#ifndef GENSECHIP
	      SetCalData(n+16,XRa); 
	      SetCalData(n+18,vra[1]); 
	      SetCalData(n+20,vra[0]); 	      
#else
	      cout << "Error, competition GENSECHIP/RATIO" << endl;
#endif
	    }
	  else
	    {
#ifdef DEBUG_PP
	      cout << "Strips RA != 2 (" << StripsRa << ")  : MaxNr " <<MaxWireNr[0]<< endl;
#endif	      
	    }
	  
#endif


#ifdef GENSECHIP
	  TF1 *fitx = new TF1("fitx","[0]/(TMath::Power(TMath::CosH(TMath::Pi()*(x-[1])/[2]),2.))",0,64);   
	  fitx->SetParameter(0,LStripQ[MaxWireNr[0]]);
	  fitx->SetParameter(1,MaxWireNr[0]);
	  fitx->SetParameter(2,3);
	  //p1x = fitx->GetParameter(1);
	  // DetList->at(n)->PrintCal();
	  // for(UShort_t k=0; k<g->GetN(); k++)
	  //   cout << k << " " << g->GetX()[k] << " " << g->GetY()[k] <<endl;
	  
	  
	  g->Fit("fitx","QN");
	  // cout << "GENSECHIP  : " << fitx->GetParameter(1) << endl;
	  // cout << "WA  : " << XWA << endl;
	  XSG = fitx->GetParameter(1);
	  if(XSG > 0. && XSG<64.)
	    {
	      // XWA -= GetRefX();
	      Ctr->at(10+n)++;		  
	    }
	  else 
	    {
	      XSG = -1500.;
	    }
	  SetCalData(n+16,XSG);
      
	  delete fitx;
	  g->Set(0);

#endif
	}
      else
	{
	  XWA = -1500.;
	  QTotal = 0;
	  QRatio = 0;
	}


      // QTotal
      SetCalData(n+4,QTotal);
      // MaxPadQ
      SetCalData(n+6,LStripQ[MaxWireNr[0]]);
      // MaxPadNrQ
      SetCalData(n+8,MaxWireNr[0]);
      // Ratio of Max Charge / QTotal
      SetCalData(n+10,QRatio);
      // Multiplicity
      SetCalData(n+12,DetList->at(n)->GetM());

    }

  if(isPresent)
    {
// #ifdef TPPAC_TRACE
//       for(UShort_t k=0; k<DetList->at(n)->GetM(); k++)
// 	{
// 	  DetList->at(3+n)->SetCalData(DetList->at(n)->GetNrAt(FStrip[k]),DetList->at(n)->GetCalAt(FStrip[k]));
// 	}
// #endif
    }
  return XWA;
  END;
}



Float_t TPPAC::SECHIP(UShort_t *FStrip,UShort_t n)
{
  START;
  Float_t v[6];
  Float_t XS=-1500;
  v[0] = sqrt(DetList->at(n)->GetCalAt(FStrip[0])/DetList->at(n)->GetCalAt(FStrip[2]));
  v[1] = sqrt(DetList->at(n)->GetCalAt(FStrip[0])/DetList->at(n)->GetCalAt(FStrip[1]));
  v[2] = 0.5*(v[0]+v[1]);
  v[3] = log(v[2]+sqrt(pow(v[2],2.f)-1.0));
  v[4] = (v[0] - v[1])/(2.0*sinh(v[3]));
  v[5] = 0.5*log((1.0+v[4])/(1.0-v[4]));
  
  XS = (Float_t) DetList->at(n)->GetNrAt(FStrip[0]) 
	 - (Float_t) (DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[1]))*v[5]/v[3];

  XS *= 1.0; //Goes 1 mm spacing
  if(XS > 0. && XS < 64)
    {
      Ctr->at(3+n*4)++;
      // XS -= GetRefX();      
      // 	 for(j=0;j<3;j++)
		// 		{
		// 		  QPk[j] = Q[FStrip[j]];
		// 		  NPk[j] = N[FStrip[j]];
		// 		}
      
    }
  else
    XS = -1500.;
  
  return XS;
  END;
}


#ifdef WITH_ROOT
void TPPAC::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
		
  // XS
  AddHistoCal(CalNameI[0],CalNameI[0],"XS (mm)",600,0,60);
  // YS
  AddHistoCal(CalNameI[1],CalNameI[1],"YS (mm)",600,0,60);
  // XWA
  AddHistoCal(CalNameI[2],CalNameI[2],"XWA (mm)",600,0,60);
  // YWA
  AddHistoCal(CalNameI[3],CalNameI[3],"YWA (mm)",600,0,60);
  // QX
  AddHistoCal(CalNameI[4],CalNameI[4],"QX",1000,0,10000);
  // QY
  AddHistoCal(CalNameI[5],CalNameI[5],"QY",1000,0,10000);
  // MaxQX
  AddHistoCal(CalNameI[6],CalNameI[6],"MaxQX",1000,0,10000);
  // MaxQY
  AddHistoCal(CalNameI[7],CalNameI[7],"MaxQY",1000,0,10000);
  // MX
  AddHistoCal(CalNameI[12],CalNameI[12],"Mult X",30,0,30);
  // MY
  AddHistoCal(CalNameI[13],CalNameI[13],"Mult Y",30,0,30);
  // MX_f
  AddHistoCal(CalNameI[14],CalNameI[14],"Mult X(free)",30,0,30);
  // MY_f
  AddHistoCal(CalNameI[15],CalNameI[15],"Mult Y(free)",30,0,30);
#ifdef GENSECHIP 
  // XSG
  AddHistoCal(CalNameI[16],CalNameI[16],"XS (SECHS Generalized) (mm)",600,0,60);
  // XSG
  AddHistoCal(CalNameI[17],CalNameI[17],"YS (SECHS Generalized) (mm)",600,0,60);
#endif

  END;
}

void TPPAC::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }
  AddHistoCal(CalNameI[1],CalNameI[0],"XY_S","X_vs_Y (SECHS)",600,0,60,600,0,60);
  AddHistoCal(CalNameI[3],CalNameI[2],"XY_WA","X_vs_Y (WeigtedAverage)",600,0,60,600,0,60);
  AddHistoCal(CalNameI[2],CalNameI[4],"XWA_vs_QX","XWA vs QX",600,0,60,1000,0,10000);
  AddHistoCal(CalNameI[3],CalNameI[5],"YWA_vs_QY","YWA vs QY",600,0,60,1000,0,10000);

  DetList->at(2)->AddHistoCal(DetList->at(2)->GetCalName(0),DetList->at(2)->GetCalName(1),"SIE_vs_SIT","SIE vs SIT",2000,0,13000,500,00,16500);
  DetList->at(2)->AddHistoCal(DetList->at(2)->GetCalName(0),DetList->at(2)->GetCalName(2),"SIE_vs_PPT","SIE vs PPACT",2000,0,13000,500,12500,13500);
  
  END;
}


void TPPAC::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
 
  if(fMode == MODE_WATCHER)
    {
	if(fRawData)
	  {
		 SetHistogramsRaw(true);
		 // SetOutAttachRawV(true);
	  }
	if(fCalData)
	  {
		 SetHistogramsCal(true);
		 // SetOutAttachCalI(true);
	  }
	for(UShort_t i = 0;i<2;i++)
	  {
		 if(fRawDataSubDetectors)
			{
			  DetList->at(i)->SetHistogramsRaw2D(true);
			  DetList->at(i)->SetHistogramsRawSummary(true);
			  // DetList->at(i)->SetOutAttachRawV(true);
			}
		 DetList->at(i)->SetHistogramsCal1D(false);
		 DetList->at(i)->SetHistogramsCal2D(true);
		 DetList->at(i)->SetHistogramsCalSummary(true);
		 // DetList->at(i)->SetOutAttachCalI(true);	
		  }
	if(fRawDataSubDetectors)
	  {
		 DetList->at(2)->SetHistogramsRaw(true);
		 // DetList->at(2)->SetOutAttachRawI(true);
	  }
	DetList->at(2)->SetHistogramsCal(true);
	// DetList->at(2)->SetOutAttachCalI(true);
	
	// OutAttach(OutTTree);
	// 	for(UShort_t i = 0;i<DetList->size();i++)
	// 	  DetList->at(i)->OutAttach(OutTTree);

	 }
  else if(fMode == MODE_D2R)
	 {
		for(UShort_t i = 0;i<2;i++)
		  {
			 DetList->at(i)->SetHistogramsRaw2D(true);
			 DetList->at(i)->SetHistogramsRawSummary(true);
			 DetList->at(i)->SetOutAttachRawV(true);
		  }
		DetList->at(2)->SetHistogramsRaw(true);
		DetList->at(2)->SetOutAttachRawI(true);
		OutAttach(OutTTree);
		for(UShort_t i = 0;i<DetList->size();i++)
		  DetList->at(i)->OutAttach(OutTTree);
	 }
  else if(fMode == MODE_D2A)
	 {
		if(fRawData)
		  {
			 SetHistogramsRaw(true);
			 SetOutAttachRawV(true);
		  }
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
		  }
		
		for(UShort_t i = 0;i<2;i++)
		  {
			 if(fRawDataSubDetectors)
				{
				  DetList->at(i)->SetHistogramsRaw1D(false);
				  DetList->at(i)->SetHistogramsRaw2D(true);
				  DetList->at(i)->SetHistogramsRawSummary(true);
				  DetList->at(i)->SetOutAttachRawV(true);
				}
			 DetList->at(i)->SetHistogramsCal1D(false);
			 DetList->at(i)->SetHistogramsCal2D(true);
			 DetList->at(i)->SetHistogramsCalSummary(true);
			 DetList->at(i)->SetOutAttachCalI(false);
			 DetList->at(i)->SetOutAttachCalV(true);			 
		  }
		if(fRawDataSubDetectors)
		  {
			 DetList->at(2)->SetHistogramsRaw(false);
			 DetList->at(2)->SetOutAttachRawI(false);
		  }
		DetList->at(2)->SetHistogramsCal(true);
		DetList->at(2)->SetOutAttachCalI(true);
#ifdef TPPAC_TRACE
		for(UShort_t i = 3;i<7;i++)
		  {
		    DetList->at(i)->SetHistogramsCal1D(false);
		    DetList->at(i)->SetHistogramsCalSummary(true);
		    DetList->at(i)->SetOutAttachCalI(false);
		    DetList->at(i)->SetOutAttachCalV(true);
		  }

#endif
		OutAttach(OutTTree);
		for(UShort_t i = 0;i<DetList->size();i++)
		  DetList->at(i)->OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
      SetInAttachRawV(true);
      InAttach(InTTree);
		
		SetOutAttachCalI(true);
      OutAttach(OutTTree);
		
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }



  END;
}

#endif
