/****************************************************************************
 *    Copyright (C) 2016-2017 by Antoine Lemasson
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "LundSi.hh"

LundSi::LundSi(const Char_t *Name,
               UShort_t NRings, 
               UShort_t NSectors, 
               Bool_t RawData, 
               Bool_t CalData, 
               Bool_t DefaultCalibration, 
               const Char_t *NameExt
               )
  : BaseDetector(Name, 1, false, CalData, DefaultCalibration,false,NameExt)
{
  START;
  
  NumberOfSectors = NSectors;
  NumberOfRings = NRings;
  // NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  DetManager *DM = DetManager::getInstance();
  
  AllocateComponents(); 

 
  if(CalData)
    {
      sprintf(CalNameI[0],"LundSiM");
    }

  END;
}
LundSi::~LundSi(void)
{
  START;
  END;

}


void LundSi::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;
  Char_t pname[100];
  // Rings Energies
  BaseDetector *REner = new BaseDetector("LundSi_E_R",NumberOfRings,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {

      
      
      REner->SetParameterName("E_Ring_%02d");
      for(UShort_t k =0; k<NumberOfRings; k++)
	{
	  sprintf(pname,"E_Ring_%02d",k);
	  REner->SetParameterName(pname,k);
	}
      //      REner->SetGateRaw(0,16384);
    }
  REner->SetCalHistogramsParams(5000,0,5000,"keV");
  AddComponent(REner);

  // Rings Times
  BaseDetector *RTimes = new BaseDetector("LundSi_T_R",NumberOfRings,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
     for(UShort_t k =0; k<NumberOfRings; k++)
	{
	  sprintf(pname,"T_Ring_%02d",k);
	  RTimes->SetParameterName(pname,k);
	}
     //      RTimes->SetParameterName("T_Ring_%02d");
    }
  RTimes->SetCalHistogramsParams(5000,0,5000,"ns");
  AddComponent(RTimes);

  // Sectors Energies
  BaseDetector *SEner = new BaseDetector("LundSi_E_S",NumberOfSectors,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
     for(UShort_t k =0; k<NumberOfSectors; k++)
	{
	  sprintf(pname,"E_Sector_%02d",k);
	  SEner->SetParameterName(pname,k);
	}
     //      SEner->SetParameterName("E_Sector_%02d");
      //      REner->SetGateRaw(0,16384);
    }
  SEner->SetCalHistogramsParams(5000,0,5000,"keV");
  AddComponent(SEner);

  // Sectors Times
  BaseDetector *STimes = new BaseDetector("LundSi_T_S",NumberOfSectors,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      for(UShort_t k =0; k<NumberOfSectors; k++)
	{
	  sprintf(pname,"T_Sector_%02d",k);
	  STimes->SetParameterName(pname,k);
	}
      // STimes->SetParameterName("T_Sector_%02d");
    }
  STimes->SetCalHistogramsParams(5000,0,5000,"ns");
  AddComponent(STimes);



  // Additional TACS TFAT_HF
  BaseDetector *TFAT_HF = new BaseDetector("TFAT_HF",1,RawData,CalData,DefCal,false,"");
  if(RawData)
    TFAT_HF->SetParameterName("TFAT_HF",0);
  if(CalData)
    TFAT_HF->SetCalName("TFAT_HF_C",0);
#ifdef WITH_ROOT
  TFAT_HF->SetRawHistogramsParams(16384,0,16383,"");
  TFAT_HF->SetCalHistogramsParams(1000,0,1000,"ns","");
#endif
  AddComponent(TFAT_HF);

  END;
}

void LundSi::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  for(UShort_t i=0; i< DetList->size(); i++)
    DetList->at(i)->SetParameters(Par,Map);
  END;
}

Bool_t LundSi::Treat(void)
{ 
  START;
  Ctr->at(0)++;
  
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        {
          DetList->at(i)->Treat();
        }
    }
  if(fCalData)
	  {

		 if(DetList->at(0)->GetM()>1 && DetList->at(1)->GetM()>1) // Both Energies and Time Present 
         {
           // Do Something 
			  // // Detector Number
			  // SetCalData(0,DetList->at(0)->GetNrAt(0));
			  // // Detector Calibrated Energy
			  // SetCalData(1,DetList->at(0)->GetCalAt(0));
			  isPresent = true;
			}
	  }
  return(isPresent); 
  END;
}



#ifdef WITH_ROOT
void LundSi::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  for(UShort_t i = 0;i<DetList->size();i++)
    {
      DetList->at(i)->SetMainHistogramFolder("");
      DetList->at(i)->SetHistogramsRaw1DFolder("LundSi_R");
      DetList->at(i)->SetHistogramsRaw2DFolder("LundSi_R");
      if(i == 1 || i == 2)
	{
	  DetList->at(i)->SetHistogramsCal1DFolder("LundSi_C_T");
	  DetList->at(i)->SetHistogramsCal2DFolder("LundSi_C_T");
	}
      else
	{
	  DetList->at(i)->SetHistogramsCal1DFolder("LundSi_C");
	  DetList->at(i)->SetHistogramsCal2DFolder("LundSi_C");
	}
      SetHistogramsCal1DFolder("LundSi_C");
      SetHistogramsCal2DFolder("LundSi_C");
    }
 
  if(fMode == MODE_WATCHER)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(true);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw(true);
	      if(i == 0 || i == 1 || i == 2 || i ==3 ) DetList->at(i)->SetHistogramsRawSummary(true);
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal(true);
	      if(i == 0 || i == 1 || i == 2 || i ==3 ) DetList->at(i)->SetHistogramsCalSummary(true);
	    }
	}
    }
  else if(fMode == MODE_D2R)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw(true);
	      if(i == 0 || i == 1 || i == 2 || i ==3) 
		{
		  DetList->at(i)->SetHistogramsRawSummary(true);
		  DetList->at(i)->SetOutAttachRawV(true);
		  DetList->at(i)->SetOutAttachRawI(false);
		}
	      else
		{			
		  DetList->at(i)->SetOutAttachRawI(true);
		}
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}
      OutAttach(OutTTree);
		
    }
  else if(fMode == MODE_D2A)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(true);
	  SetOutAttachRawI(true);
	  SetOutAttachRawV(true);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	  SetOutAttachCalV(false);
	}
		
      for(UShort_t i = 0;i<DetList->size();i++)
	{			
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw1D(true);
	      DetList->at(i)->SetHistogramsRaw2D(true);
				  
	      if(i == 0 || i == 1 || i == 2 || i ==3) 
		{
		  DetList->at(i)->SetHistogramsRawSummary(true);
		  DetList->at(i)->SetOutAttachRawV(true);
		  DetList->at(i)->SetOutAttachRawI(false);			 					  
					  
		}
	      else
		{
		  DetList->at(i)->SetOutAttachRawI(true);
		}
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal1D(true);
	      DetList->at(i)->SetHistogramsCal2D(true);
	      if(i == 0 || i == 1 || i == 2 || i ==3)
		{
		  DetList->at(i)->SetHistogramsCalSummary(true);
		  DetList->at(i)->SetOutAttachCalI(false);
		  DetList->at(i)->SetOutAttachCalV(true);			 
		}
	      else
		{
		  DetList->at(i)->SetHistogramsCalSummary(false);
		  DetList->at(i)->SetOutAttachCalI(true);
		}
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}
		
      OutAttach(OutTTree);
    }
  else if(fMode == MODE_R2A)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(i == 0 || i == 1 ) 
	    DetList->at(i)->SetInAttachRawV(true);
	  else
	    DetList->at(i)->SetInAttachRawI(true);
	  DetList->at(i)->InAttach(InTTree);
	}
		
      SetOutAttachCalI(true);

      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal1D(true);
	      DetList->at(i)->SetHistogramsCal2D(true);
	      if(i == 0 || i == 1) 
		{
		  DetList->at(i)->SetHistogramsCalSummary(true);
		  DetList->at(i)->SetOutAttachCalI(false);
		  DetList->at(i)->SetOutAttachCalV(true);			 
		}
	      else
		{
		  DetList->at(i)->SetHistogramsCalSummary(false);
		  DetList->at(i)->SetOutAttachCalI(true);
		  // DetList->at(i)->SetOutAttachRawI(true);
		}
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}
      OutAttach(OutTTree);
		
    }
  else if (fMode == MODE_RECAL)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(i == 0 || i == 1) 
	    DetList->at(i)->SetInAttachCalV(true);
	  else
	    DetList->at(i)->SetInAttachCalI(true);
	    
	  DetList->at(i)->InAttachCal(InTTree);					 
	}
      SetInAttachCalI(true);
      InAttachCal(InTTree);

      SetOutAttachCalI(true);

      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal1D(true);
	      DetList->at(i)->SetHistogramsCal2D(true);
	      if(i == 0 || i == 1) 
		{
		  DetList->at(i)->SetHistogramsCalSummary(true);
		  DetList->at(i)->SetOutAttachCalI(false);
		  DetList->at(i)->SetOutAttachCalV(true);			 
		}
	      else
		{
		  DetList->at(i)->SetHistogramsCalSummary(false);
		  DetList->at(i)->SetOutAttachCalI(true);
		  // DetList->at(i)->SetOutAttachRawI(true);
		}
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}
      OutAttach(OutTTree);
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
    {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
    }
  END;
}


#endif
