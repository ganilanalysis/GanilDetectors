/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "TACNUMEXO.hh"
#include "TACNUMEXO.hh"


TACNUMEXO::TACNUMEXO(const Char_t *Name,
        UShort_t NChan,
        Bool_t RawData,
        Bool_t CalData,
        Bool_t DefaultCalibration,
        const Char_t *NameExt,
         UShort_t MaxMult
)
: BaseDetector(Name, 0, false, CalData, DefaultCalibration, false, NameExt, true,false) {
    START;
    NumberOfSubDetectors = NChan;
    fRawDataSubDetectors = RawData;


    // Set How many IC rows
    AllocateComponents();
    char line[100];


    AllocateLocalArrays();

#ifdef WITH_ROOT
    // Setup Histogram Hierarchy
#endif


    END;
}

TACNUMEXO::~TACNUMEXO(void) {
    START;
    DeAllocateLocalArrays();

    END;
}

void TACNUMEXO::AllocateLocalArrays(void) {
    START;

    END;
}

void TACNUMEXO::DeAllocateLocalArrays(void) {
    START;


    END;
}

void TACNUMEXO::AllocateComponents(void) {
    START;
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Char_t Name[100];

    BaseDetector *D = new BaseDetector(DetectorName, NumberOfSubDetectors, RawData, CalData, DefCal, false, "",false,true);
    if(RawData)
    {
       D->SetGateRaw(0,65535);
       D->SetRawHistogramsParams(30000,0,65535);       
    }
    if (CalData) {
        D->ReadCalGates();
        D->SetCalHistogramsParams(4000,0,2000,"","Cal");

    }

#ifdef WITH_ROOT
    D->SetMainHistogramFolder(MainHistogramFolder);
#endif
    AddComponent(D);

    END;
}

void TACNUMEXO::SetParameters(NUMEXOParameters* Par, Map* Map) {
    START;
    Char_t Name[100];



    //e775
    DetList->at(0)->SetParameterName("T_AGATA_VAMOS", 0);
    DetList->at(0)->SetParameterName("T_MUGAST_FPMW", 1);
    DetList->at(0)->SetParameterName("T_FPMW_CATS2", 2);
    DetList->at(0)->SetParameterName("T_FPMW_HF", 3);
    if (fCalData) {
    DetList->at(0)->SetCalName("T_AGATA_VAMOS_C", 0); 
    DetList->at(0)->SetCalName("T_MUGAST_FPMW_C", 1);
    DetList->at(0)->SetCalName("T_FPMW_CATS2_C", 2);
    DetList->at(0)->SetCalName("T_FPMW_HF_C", 3);
    }
    NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
    for (UShort_t i = 0; i < DetList->size(); i++)
      DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);
    END;
}

Bool_t TACNUMEXO::Treat(void) {
    START;

    Ctr->at(0)++;

    if (isComposite) {
      for (UShort_t i = 0; i < DetList->size(); i++) {
        DetList->at(i)->Treat();
      }
    }

    return (isPresent);
    END;
}

#ifdef WITH_ROOT

void TACNUMEXO::CreateHistogramsCal1D(TDirectory *Dir) {
    START;

    string Name;
    Dir->cd("");
    if (SubFolderHistCal1D.size() > 0) {
        Name.clear();
        Name = SubFolderHistCal1D;
        if (!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    END;
}

void TACNUMEXO::CreateHistogramsCal2D(TDirectory *Dir) {
    START;
    string Name;

    BaseDetector::CreateHistogramsCal2D(Dir);

    Dir->cd("");

    if (SubFolderHistCal2D.size() > 0) {              //  DetList->at(i)->SetOutAttachRawI(true);

        Name.clear();
        Name = SubFolderHistCal2D;
        if (!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    END;
}

void TACNUMEXO::SetOpt(TTree *OutTTree, TTree *InTTree) {
    START;

    // Set histogram Hierarchy
    for (UShort_t i = 0; i < DetList->size(); i++) {
        DetList->at(i)->SetMainHistogramFolder("");
    }

    if (fMode == MODE_WATCHER) {
        if (fRawData) {
            SetHistogramsRaw(true);
        }
        if (fCalData) {
            SetHistogramsCal(true);
        }
        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasRawData()) {
                DetList->at(i)->SetHistogramsRaw(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
            }
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(true);
            }
        }
    } else if (fMode == MODE_D2R) {
        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasRawData()) {
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
                DetList->at(i)->SetOutAttachRawV(false);
                DetList->at(i)->SetOutAttachRawI(true);
                DetList->at(i)->SetOutAttachTS(true);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    } else if (fMode == MODE_D2A) {
        if (fRawData) {
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
        }
        if (fCalData) {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }

        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasRawData()) {
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
                DetList->at(i)->SetOutAttachRawI(true);
              //  DetList->at(i)->SetOutAttachRawV(true);
                DetList->at(i)->SetOutAttachTS(true);

            }
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalI(true);
                DetList->at(i)->SetOutAttachCalV(false);
                DetList->at(i)->SetOutAttachCalF(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }

        OutAttach(OutTTree);
    } else if (fMode == MODE_R2A) {
        for (UShort_t i = 0; i < DetList->size(); i++) {
            DetList->at(i)->SetInAttachRawV(false);
            DetList->at(i)->InAttach(InTTree);
        }

        SetOutAttachCalI(true);

        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalF(true);
                DetList->at(i)->SetOutAttachCalV(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if (fMode == MODE_RECAL) {

        for (UShort_t i = 0; i < DetList->size(); i++) {
            SetInAttachCalI(true);
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetInAttachCalF(true);
            }
            DetList->at(i)->InAttachCal(InTTree);
        }
        InAttachCal(InTTree);
        SetOutAttachCalI(true);
        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalF(true);
                DetList->at(i)->SetOutAttachCalV(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    } else if (fMode == MODE_CALC) {
        SetNoOutput();
    } else {
        Char_t Message[500];
        sprintf(Message, "In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(), fMode);
        MErr * Er = new MErr(WhoamI, 0, 0, Message);
        throw Er;
    }
    END;
}





#endif

