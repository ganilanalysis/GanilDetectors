/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "FocalPositionMW.hh"

FocalPositionMW::FocalPositionMW(const Char_t *Name,
                             UShort_t NDetectors, 
                             Bool_t RawData, 
                             Bool_t CalData, 
                             Bool_t DefaultCalibration,
                             const Char_t *NameExt
                             ) : BaseDetector(Name, 4, false, CalData, DefaultCalibration,false,NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;

  
  AllocateComponents();

  // Read Position 
  ReadPosition();

  AddCounter("All Wires Present");
  AddCounter("PresentX");
  AddCounter("PresentY");
  AddCounter("PresentX Mult 0");
  AddCounter("PresentX Mult 1");
  AddCounter("PresentX Mult 2");
  AddCounter("PresentX Mult 3");
  AddCounter("PresentX Mult 4");
  AddCounter("Xf");
  AddCounter("Tf");
  AddCounter("Yf");
  AddCounter("Pf");

  if(fCalData)
    {
      SetCalName("Xf",0);
      SetCalName("Tf",1);
      SetCalName("Yf",2);
      SetCalName("Pf",3);
    }
#ifdef WITH_ROOT
  // Setup Histogram Hierarchy
  SetHistogramsCal1DFolder("FPMW1D");
  SetHistogramsCal2DFolder("FPMW2D");
#endif

  END;
}

FocalPositionMW::~FocalPositionMW(void)
{
  START;
  END;
}

void FocalPositionMW::AllocateComponents(void)
{
  START;
  // Default Value
  VamosAngle = 0.; 
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = false;
  Bool_t PosInfo = true;


  UShort_t NWiresX[2] = {992,992};
  UShort_t NWiresY[2] = {160,160}; 


  for(UShort_t i=0; i<NumberOfSubDetectors; i++)
    {
      Char_t Name[20];
      sprintf(Name,"FPMW%d",i);
      Bool_t HasTimeStamp = true;
      BaseDetector *ADet = new  TMW(Name,NWiresX[i],NWiresY[i],RawData,CalData,DefCal,PosInfo,DetectorNameExt,HasTimeStamp);     
      AddComponent(ADet);
    }
  END;
}


void FocalPositionMW::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  // Add A Range of Parameters for sub components 
  if(isComposite){
     for(UShort_t i=0; i< DetList->size(); i++)
			{
           DetList->at(i)->SetParameters(Par,Map);
			}
  }
  END;
}

void FocalPositionMW::ReadPosition(void)
{ 
  START;
 if(fCalData)
	 {

		MIFile *IF;
		char Line[255];
		stringstream *InOut;
      UShort_t Len=255;
      Char_t *Ptr=NULL;
	    
		InOut = new stringstream();
		*InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
		*InOut>>Line;
		InOut = CleanDelete(InOut);
		
		if((IF = CheckCalibration(Line)))
		  {
			 // Position 
			 try
				{
				  ReadDefaultPosition(IF);
              // Read Vamos Angle
              IF->GetLine(Line,Len);
              while((Ptr = CheckComment(Line)))
                {
                  IF->GetLine(Line,Len);
                  L->File << Line << endl;
                }
              stringstream *InOut = new stringstream();
              *InOut << Line;
              *InOut >> VamosAngle;
              if(InOut->fail())
                {
                  InOut = CleanDelete(InOut);
                  Char_t Message[500];
                  sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read VamosAngle value !", DetectorName, DetectorNameExt, IF->GetFileName());
                  MErr * Er= new MErr(WhoamI,0,0, Message);
                  throw Er;
                }
              InOut = CleanDelete(InOut);
              cout << "    <" << DetectorName << "><" << DetectorNameExt << "> Vamos Angle set to " << VamosAngle << " deg." <<  endl;
              L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> Vamos Angle set to " << VamosAngle << " deg." <<  endl;
				}
			 catch(...)
				{
				  IF = CleanDelete(IF);
				  throw;
				}
			 IF = CleanDelete(IF);
		  }
	 }
 END;
}
Bool_t FocalPositionMW::Treat(void)
{ 
  START;
  Ctr->at(0)++;
  if(isComposite)
    {
      for(UShort_t i =0; i< DetList->size(); i++) 
        DetList->at(i)->Treat();
    }


  UShort_t XMult=0;

  if(fCalData)
    {
      for(UShort_t i=0;i<DetList->size();i++)
        {
//	  SetCalData(i+4,DetList->at(i)->GetCal(1)); // Fill Y positions
          if(DetList->at(i)->IsPresent())
            XMult++;
        }
      Ctr->at(5+XMult)++;
		
      // Require at least one (X,Y) position measurement per chamber 
//      DetList->at(1)->PrintCal();


      if((((TMW*)(DetList->at(0)))->PresentX() && ((TMW*)(DetList->at(1)))->PresentX()))
        {
          Ctr->at(3)++;
          Ctr->at(4)++;

          FocalX();

        }
        else
          {
            // Set Value to defaults
            SetCalData(0,-1500.);
            SetCalData(1,-1500.);
          }
      if((((TMW*)(DetList->at(0)))->PresentY() && ((TMW*)(DetList->at(1)))->PresentY()))
        {

          FocalY();

        }
        else
          {
            // Set Value to defaults
         SetCalData(2,-1500.);
         SetCalData(3,-1500.);
          }

#ifdef DEBUG
      if((((TMW*)(DetList->at(0)))->PresentY() && ((TMW*)(DetList->at(1)))->PresentY())
            ||
            (((TMW*)(DetList->at(0)))->PresentX() && ((TMW*)(DetList->at(1)))->PresentX()))
         PrintCal();
#endif

    }

   return(isPresent); 
   END;
 }

 Bool_t FocalPositionMW::FocalX(void)
 { 
   START;

   Float_t Tf = -1500.;
   Float_t Xf = -1500.;

//   DetList->at(0)->PrintCal();
//   DetList->at(1)->PrintCal();
//   cout << DetList->at(1)->GetCal(0) << " " << DetList->at(0)->GetCal(0) << endl;
//   cout << DetList->at(1)->GetRefZ() << " " << DetList->at(0)->GetRefZ() << endl;
//   cout << "FP Ref " << GetRefZ() << endl;


   // Tf in mrad
   Tf = (Float_t) (1000.*atan((DetList->at(1)->GetCal(0) - DetList->at(0)->GetCal(0))
                              / (DetList->at(1)->GetRefZ() - DetList->at(0)->GetRefZ())));
   // Xf in mm
   Xf = (Float_t) (DetList->at(0)->GetCal(0) + (GetRefZ()-(DetList->at(0)->GetRefZ()))*tan(Tf/1000.));

//   cout  << "Xf " << Xf << endl;
//   cout  << "Tf " << Tf << endl;

//   UShort_t i;
//   Double_t A[2];
//   Double_t B[2];
//   Float_t Tf = -1500.;
//   Float_t Xf = -1500.;

//   SetMatX();

//   for( i=0;i<2;i++)
//     A[i] = B[i] = 0.0;


//   for(UShort_t i=0;i<DetList->size();i++)
//     if(DetList->at(i)->IsPresent())
//         {
//           A[0] += ((Double_t) DetList->at(i)->GetCal(0))*DetList->at(i)->GetRefZ();
//           A[1] += (Double_t) DetList->at(i)->GetCal(0);
//         }
//     B[0] =A[0]*MatX[0][0] + A[1]*MatX[0][1];
//     B[1] =A[0]*MatX[1][0] + A[1]*MatX[1][1];

//     // Tf in mrad
//     Tf = (Float_t) (1000.*atan(B[0]));
//     Xf = (Float_t) (B[1]+B[0]*GetRefZ()); //FocalPos

     if(Tf>-1500.)
       {
         Ctr->at(11)++;
         SetCalData(1,Tf);
       }
     else
         SetCalData(1,-1500.);

     if(Xf >-1500.)
       {
         Ctr->at(10)++;
         SetCalData(0,Xf);
       }
     else
       SetCalData(0,-1500.);

     if(Xf >-1500. && Tf>-1500.)
       return true;
     else 
       return false;
   END;
 }

 Bool_t FocalPositionMW::FocalY(void)
 { 
   START;
   UInt_t i;
   Float_t Pf = -1500.;
   Float_t Yf = -1500.;


//   DetList->at(0)->PrintCal();
//   DetList->at(1)->PrintCal();
//   cout << DetList->at(1)->GetCal(1) << " " << DetList->at(0)->GetCal(1) << endl;
//   cout << DetList->at(1)->GetRefZ() << " " << DetList->at(0)->GetRefZ() << endl;
//   cout << "FP Ref " << GetRefZ() << endl;

   // pf in mrad
   Pf = (Float_t) (1000.*atan((DetList->at(1)->GetCal(1) - DetList->at(0)->GetCal(1))
                              / (DetList->at(1)->GetRefZ() - DetList->at(0)->GetRefZ())));
   // Yf in mm
   Yf = (Float_t) (DetList->at(0)->GetCal(1) + (GetRefZ()-(DetList->at(0)->GetRefZ()))*tan(Pf/1000.));

//   cout  << "Yf " << Yf << endl;
//   cout  << "Pf " << Pf << endl;



//   Float_t A[2];
//   Float_t B[2];
//   SetMatY();

//   for(i=0;i<2;i++)
//     A[i] = B[i] = 0.0;


//   for(i=0;i<DetList->size();i++)
//     if(DetList->at(i)->IsPresent())
//       {
//         A[0] += ((Double_t) DetList->at(i)->GetCal(1))*DetList->at(i)->GetRefZ();
//         A[1] += DetList->at(i)->GetCal(1);
//       }
//   B[0] =A[0]*MatY[0][0] + A[1]*MatY[0][1];
//   B[1] =A[0]*MatY[1][0] + A[1]*MatY[1][1];

//   // Pf in mrad
//   Pf = (Float_t) (1000.*atan(B[0]));
//   Yf = (Float_t) (B[1]+B[0]*GetRefZ()); //FocalPos

   if(Pf > -1500.) 
     {
         Ctr->at(13)++;
         SetCalData(3,Pf);
     }
   else 
      SetCalData(3,-1500.);

   if(Yf>-1500)
     {
       Ctr->at(12)++;
       SetCalData(2,Yf);
     }
   else 
      SetCalData(2,-1500.);

   if(Yf >-1500. && Pf>-1500.)
     return true;
   else 
     return false;
   END;
 }



 void FocalPositionMW::SetMatX(void)
 {
   START;
   UInt_t i,j;
   Double_t A[2][2];
   Double_t Det;
   Double_t Zref;

   for(i=0;i<2;i++)
     for(j=0;j<2;j++)
       A[i][j] = MatX[i][j] = 0.;

  for(i=0;i<4;i++)
    if(DetList->at(i)->IsPresent())
      {
        Zref = DetList->at(i)->GetRefZ();
        A[0][0] += pow(Zref,2.);
        A[0][1] += Zref;
        A[1][1] += 1.0;
      }
  A[1][0] = A[0][1];
  
  

  Det = A[0][0]*A[1][1] - A[0][1]*A[1][0];

  if(Det == 0.0)
    {
      MErr * Er = new MErr(WhoamI,0,0, "Det == 0 !");
      throw Er;      
    }
  else
    {
      MatX[0][0] = A[1][1]/Det;
      MatX[1][1] = A[0][0]/Det;
      MatX[1][0] = -1.0*A[0][1]/Det;
      MatX[0][1] = -1.0*A[1][0]/Det;
   }
  END;
}

void FocalPositionMW::SetMatY(void)
{
  START;
  UInt_t i,j;
  Double_t A[2][2];
  Double_t Det;
  Double_t Zref;
  
  for(i=0;i<2;i++)
    for(j=0;j<2;j++)
      A[i][j] = MatY[i][j] = 0.;
  
  for(i=0;i<4;i++)
    if(DetList->at(i)->IsPresent())
      {
        Zref = DetList->at(i)->GetRefZ();
        A[0][0] += pow(Zref,2.);
        A[0][1] += Zref;
        A[1][1] += 1.0;
      }
  A[1][0] = A[0][1];
  
  

  Det = A[0][0]*A[1][1] - A[0][1]*A[1][0];

  if(Det == 0.0)
    {
      MErr * Er = new MErr(WhoamI,0,0, "Det == 0 !");
      throw Er;      
    }
  else
    {
      MatY[0][0] = A[1][1]/Det;
      MatY[1][1] = A[0][0]/Det;
      MatY[1][0] = -1.0*A[0][1]/Det;
      MatY[0][1] = -1.0*A[1][0]/Det;
   }
  END;
}


#ifdef WITH_ROOT
void FocalPositionMW::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
  
  AddHistoCal(GetCalName(0),GetCalName(0),"Xf (mm)",1000,-500,500);
  AddHistoCal(GetCalName(2),GetCalName(2),"Yf (mm)",1000,-500,500);
  AddHistoCal(GetCalName(1),GetCalName(1),"Tf (mrad)",1000,-500,500);
  AddHistoCal(GetCalName(3),GetCalName(3),"Pf (mrad)",1000,-500,500);
 
  END;
}
void FocalPositionMW::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }

  AddHistoCal(GetCalName(0),GetCalName(2),"Xf_Yf","Xf vs Yf",1000,-500,500,1000,-500,500);
  AddHistoCal(GetCalName(0),GetCalName(1),"Xf_Tf","Xf vs Thetaf",500,-500,500,500,-500,500);
  AddHistoCal(GetCalName(3),GetCalName(1),"Pf_Tf","Phif vs Thetaf",1000,-500,500,1000,-500,500);
  
  // Ys vs Ys correlations
//  AddHistoCal(GetCalName(4),GetCalName(5),"Y0Y1","Y0 vs Y1",1000,-100,100,1000,-100,100);
//  AddHistoCal(GetCalName(4),GetCalName(6),"Y0Y2","Y0 vs Y2",1000,-100,100,1000,-100,100);
//  AddHistoCal(GetCalName(4),GetCalName(7),"Y0Y3","Y0 vs Y3",1000,-100,100,1000,-100,100);
 

  // More complicated case, cross subdectors 2D ... or even Cal/Raw Correlations
  // for(UShort_t i=0; i<NumberOfSubDetectors; i++)
  // 	 {
  // 		for(UShort_t j=1;j<NumberOfSubDetectors; j++)
  // 		  {
  // 			 sprintf(HName,"X%d_X%d",i,j);
  // 			 sprintf(HTitle,"%s X vs %s X (mm) (mm)", DetList->at(i)->GetName(), DetList->at(j)->GetName());
  // 			 DetList->at(i)->AddHistoCal(DetList->at(i)->GetCalName(0),DetList->at(j)->GetCalName(0), HName, HTitle);
  // 			 sprintf(HName,"Y%d_Y%d",i,j);
  // 			 sprintf(HTitle,"%s Y vs %s Y (mm) (mm)", DetList->at(i)->GetName(), DetList->at(j)->GetName());
  // 			 DetList->at(i)->AddHistoCal(DetList->at(i)->GetCalName(1),DetList->at(j)->GetCalName(1), HName, HTitle);
  // 		  }
  // 	 }
		
  END;
}
#endif
