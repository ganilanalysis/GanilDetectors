/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "Identification.hh"



Identification::Identification(const Char_t *Name,
                               Reconstruction *Reco,
                               FocalPosition *FocalPos, 
                               IonisationChamber *ICD,
                               SiliconWall *SID,
                               MWPPAC *MWD
                               )
: BaseDetector(Name, 18, false, true, false,false,"")
{
  START;

  
  // Reconstruction
  Rec = NULL;
  Rec = Reco;

  // Focal Position
  FP = NULL;
  FP = FocalPos;

  // Ionization Chamber
  IC= NULL;
  IC = ICD;

  // SiliconWall
  SI= NULL;
  SI = SID;

  // MWPPAC
  MW= NULL;
  MW = MWD;
 
  // Plastic
  PL= NULL;
  
  // Target PPAC
  TP= NULL;

  //Counters
  AddCounter("w. Rec Present"); //[2]
  AddCounter("w.dE (Rec) Present"); //[3]
  AddCounter("w.T (Rec) Present"); //[4]
  AddCounter("w.Si (Rec) Present"); //[6]
  AddCounter("w.Si and dE (Rec) Present"); //[7]
  AddCounter("w.T and dE (Rec) Present"); //[5]
  AddCounter("w.Si and dE and T (Rec) Present"); //[8]
  AddCounter("MW missing "); //[9]

  if(fCalData)
    {
      sprintf(CalNameI[0],"dE");
      sprintf(CalNameI[1],"E");
      sprintf(CalNameI[2],"T");
      sprintf(CalNameI[3],"V");
      sprintf(CalNameI[4],"D");
      sprintf(CalNameI[5],"Beta");
      sprintf(CalNameI[6],"Gamma");
      sprintf(CalNameI[7],"M");
      sprintf(CalNameI[8],"M_Q");
      sprintf(CalNameI[9],"Q");
      sprintf(CalNameI[10],"Qi");
      sprintf(CalNameI[11],"dQ");
      sprintf(CalNameI[12],"Mr");
      sprintf(CalNameI[13],"BetaTP");
      sprintf(CalNameI[14],"VTP");
      sprintf(CalNameI[15],"D_1");
      sprintf(CalNameI[16],"QAr");
      sprintf(CalNameI[17],"BrhoId");

    }
  END;
}

Identification::Identification(const Char_t *Name,
                               Reconstruction *Reco,
                               FocalPosition *FocalPos, 
                               IonisationChamber *ICD,
                               SiliconWall *SID,
                               MWPPAC *MWD,
                               TargetPos *TPD
                               )
: BaseDetector(Name, 18, false, true, false,false,"")
{
  START;

  DetManager *DM = DetManager::getInstance();
  TACS  = (BaseDetector*) DM->GetDetector("TACS");
  TMW1  = (TMW*) DM->GetDetector("TMW1");
  DC0  = (DriftChamber*) DM->GetDetector("DC0");
  
  // Reconstruction
  Rec = NULL;
  Rec = Reco;

  // Focal Position
  FP = NULL;
  FP = FocalPos;

  // Ionization Chamber
  IC= NULL;
  IC = ICD;

  // SiliconWall
  SI= NULL;
  SI = SID;

  // MWPPAC
  MW= NULL;
  MW = MWD;
 
  // Plastic
  PL= NULL;

  // Target PPAC
  TP= NULL;
  TP= TPD;

  //Counters
  AddCounter("w. Rec Present"); //[2]
  AddCounter("w.dE (Rec) Present"); //[3]
  AddCounter("w.T (Rec) Present"); //[4]
  AddCounter("w.Si (Rec) Present"); //[6]
  AddCounter("w.Si and dE (Rec) Present"); //[7]
  AddCounter("w.T and dE (Rec) Present"); //[5]
  AddCounter("w.Si and dE and T (Rec) Present"); //[8]
  AddCounter("MW missing "); //[9]

  if(fCalData)
    {
      sprintf(CalNameI[0],"dE");
      sprintf(CalNameI[1],"E");
      sprintf(CalNameI[2],"T");
      sprintf(CalNameI[3],"V");
      sprintf(CalNameI[4],"D");
      sprintf(CalNameI[5],"Beta");
      sprintf(CalNameI[6],"Gamma");
      sprintf(CalNameI[7],"M");
      sprintf(CalNameI[8],"M_Q");
      sprintf(CalNameI[9],"Q");
      sprintf(CalNameI[10],"Qi");
      sprintf(CalNameI[11],"dQ");
      sprintf(CalNameI[12],"Mr");
      sprintf(CalNameI[13],"BetaTP");
      sprintf(CalNameI[14],"V5HTP");
      sprintf(CalNameI[15],"D_1");
      sprintf(CalNameI[16],"QAr");
      sprintf(CalNameI[17],"BrhoId");
     }
  END;
}
// Plastic
Identification::Identification(const Char_t *Name,
                               Reconstruction *Reco,
                               FocalPosition *FocalPos, 
                               IonisationChamber *ICD,
                               Plastic *PLD
                               )
: BaseDetector(Name, 18, false, true, false,false,"")
{
  START;

  // Reconstruction
  Rec = NULL;
  Rec = Reco;

  // Focal Position
  FP = NULL;
  FP = FocalPos;

  // Ionization Chamber
  IC= NULL;
  IC = ICD;

  // SiliconWall
  SI= NULL;

  // MWPPAC
  MW= NULL;

  // Plastic
  PL = NULL;
  PL = PLD;

  //Counters
  AddCounter("w. Rec Present"); //[2]
  AddCounter("w.dE (Rec) Present"); //[3]
  AddCounter("w.T (Rec) Present"); //[4]
  AddCounter("w.PL (Rec) Present"); //[6]
  AddCounter("w.PL and dE (Rec) Present"); //[7]
  AddCounter("w.T and dE (Rec) Present"); //[5]
  AddCounter("w.PL and dE and T (Rec) Present"); //[8]
  AddCounter("MW missing "); //[9]

  if(fCalData)
    {
      sprintf(CalNameI[0],"dE");
      sprintf(CalNameI[1],"E");
      sprintf(CalNameI[2],"T");
      sprintf(CalNameI[3],"V");
      sprintf(CalNameI[4],"D");
      sprintf(CalNameI[5],"Beta");
      sprintf(CalNameI[6],"Gamma");
      sprintf(CalNameI[7],"M");
      sprintf(CalNameI[8],"M_Q");
      sprintf(CalNameI[9],"Q");
      sprintf(CalNameI[10],"Qi");
      sprintf(CalNameI[11],"dQ");
      sprintf(CalNameI[12],"Mr");
      sprintf(CalNameI[13],"BetaTP");
      sprintf(CalNameI[14],"VTP");
		sprintf(CalNameI[15],"D_1");
        sprintf(CalNameI[16],"QAr");
        sprintf(CalNameI[16],"BrhoId");

    }
  END;
}


// DetManager Handling
Identification::Identification(const Char_t *Name)
: BaseDetector(Name, 17, false, true, false,false,"")
{
  START;

  DM = DetManager::getInstance();

  // Reconstruction
  Rec = NULL;
  Rec = (Reconstruction*) DM->GetDetector("Reconstruction");

  // Focal Position
  FP = NULL;
  FP = (FocalPosition*) DM->GetDetector("FP");

  // Ionization Chamber Numexo
  ICN= NULL;
  ICN = (IonisationChamberNUMEXO*) DM->GetDetector("Ic_Numexo");

  // SiliconWall
  SI= NULL;

  // MWPPAC
    MW= NULL ;//     (MWPPAC*) DM->GetDetector("MWPPAC");

  // DC Target Position
  DCTP= NULL;
  //DCTP = (TargetDCNumexo*) DM->GetDetector("TargetDC");

  // TACS of TOF
  TACS = NULL;
  TACS  = (BaseDetector*) DM->GetDetector("TACS_N");

  DC0 = NULL;
  //DC0  = (DriftChamber*) DM->GetDetector("DC0");

  // Plastic
  PL = NULL;

  //Counters
  AddCounter("w. Rec Present"); //[2]
  AddCounter("w.dE (Rec) Present"); //[3]
  AddCounter("w.T (Rec) Present"); //[4]
  AddCounter("w.PL (Rec) Present"); //[6]
  AddCounter("w.PL and dE (Rec) Present"); //[7]
  AddCounter("w.T and dE (Rec) Present"); //[5]
  AddCounter("w.PL and dE and T (Rec) Present"); //[8]
  AddCounter("MW missing "); //[9]

  if(fCalData)
    {
      sprintf(CalNameI[0],"dE");
      sprintf(CalNameI[1],"E");
      sprintf(CalNameI[2],"T");
      sprintf(CalNameI[3],"V");
      sprintf(CalNameI[4],"D");
      sprintf(CalNameI[5],"Beta");
      sprintf(CalNameI[6],"Gamma");
      sprintf(CalNameI[7],"M");
      sprintf(CalNameI[8],"M_Q");
      sprintf(CalNameI[9],"Q");
      sprintf(CalNameI[10],"TFF");
      sprintf(CalNameI[11],"VFF");
      sprintf(CalNameI[12],"BetaFF");
      sprintf(CalNameI[13],"GammaFF");
      sprintf(CalNameI[14],"MFF");
      sprintf(CalNameI[15],"M_QFF");
      sprintf(CalNameI[16],"QFF");

    }
  END;
}

Identification::~Identification(void)
{
  START;

  END;
}

Bool_t Identification::Treat(void)
{
  START;
  Ctr->at(0)++;
  
  if((Rec && Rec->IsPresent()) || (TP &&  TP->IsPresent())) 
    {
      Ctr->at(2)++;
      Calculate();
#ifdef DEBUG
      PrintCal();
#endif
    }
  return (isPresent);
  END;
}


void Identification::Init(void)
{
  START;
  dE = E = ETot = T = V = M_Q = M = Z = Beta = Q = D = 0.0;
  D_1 = 0.0;
  Qi =0;
  dQ=0.;
  Mr=0.;
  Qi_Zr =0;
  dQ_Zr=0.;
  Mr_Zr=0.;
  Gamma = 1.0;
  VTP = 0.;
  BetaTP = 0;
  GammaTP = 1.0;
  END;
}

Bool_t Identification::Calculate(void)
{
  START;
  // Reset Local Variable
  Init();
   Float_t TOffSet[20];
   TOffSet[0] = 0;
   TOffSet[1] = 0;
   TOffSet[2] = 0;
   TOffSet[3] = 0;
   TOffSet[4] = 0;
   TOffSet[5] = 0;
   TOffSet[6] = 0;
   TOffSet[7] = 0;
   TOffSet[8] = 0;
   TOffSet[9] = 0;
   TOffSet[10] = 0;
   TOffSet[11] = 0;
   TOffSet[12] = 0;
   TOffSet[13] = 0;
   TOffSet[14] = 0;
   TOffSet[15] = 0;
   TOffSet[16] = 0;
   TOffSet[17] = 0;
   TOffSet[18] = 0;
   TOffSet[19] = 0;

  // calculate velocity

  // TOF Form
  //t1->SetAlias("mT2","T_TMW2_FPMW0_C-66.+223+52.+0.5*(FPMWPat_0RawNr==7)+0.5*(FPMWPat_0RawNr==6)+0.*(FPMWPat_0RawNr==5)-0.9*(FPMWPat_0RawNr==4)-1.9*(FPMWPat_0RawNr==3)-1.7*(FPMWPat_0RawNr==2)+1.3*(FPMWPat_0RawNr==9)+1.3*(FPMWPat_0RawNr==10)+0.5*(FPMWPat_0RawNr==11)+0.5*(FPMWPat_0RawNr==12)+0.3*(FPMWPat_0RawNr==13)-0.2*(FPMWPat_0RawNr==14)-0.5*(FPMWPat_0RawNr==15)+0.6*(FPMWPat_0RawNr==16)+0.0*(FPMWPat_0RawNr==17)-0.3*(FPMWPat_0RawNr==18)+0.*(FPMWPat_0RawNr==19)+0.*(FPMWPat_0RawNr==1)+0.*(FPMWPat_0RawNr==0)");
  Double_t T_T2_F0 = TACS->GetCal(2);
  T =   T_T2_F0 +223+52.-66.;
  //DetManager *DM = DetManager::getInstance();

  // Get MWNr
  BaseDetector *FPMWPat0 = DM->GetDetector("FPMWPat_0");
  //BaseDetector *FPMWPat1 = DM->GetDetector("FPMWPat_1");
  Int_t MW_Nr = -1;

  if(FPMWPat0->GetRawM() == 1)
      MW_Nr = FPMWPat0->GetRawNrAt(0);
  else
      MW_Nr=-1;

  if((MW_Nr>-1)&&(MW_Nr<20))
   {
     // Apply Specific Offset
     T += TOffSet[MW_Nr];
   }
  else
     T = 0;


  // Calculate D
  //   t1->SetAlias("mD2","Path-24.0/cos(TP_Theta/1000.)/cos(TP_Phi/1000.)+(768.4-760.)/cos(Tf/1000.)");

  if(FP->GetPf() > -1000)
  {
     D = Rec->GetPath()
           - (24.)/cos(TP->GetPhi()/1000.)/cos(TP->GetTheta()/1000.)
           + (768.4-760.)/cos(FP->GetPf()/1000.)/cos(FP->GetTf()/1000.) ;

     V = D/T;
     Beta =V/29.9792458;


     Gamma = 1./sqrt(1.-powf(Beta,2.f));
     M_Q = Rec->GetBrho()/3.105/Beta/Gamma;
  }


  VTP = 0;
  BetaTP = 0;
  GammaTP =  0;

  // Calculate Energy
   //  t1->SetAlias("mE2","1.*(IC[0]+IC[1])+2.*(IC[2]+IC[3]+IC[4]+0.*IC[5]+0.*IC[6])");

  Float_t  IC0 = ICN->GetCal(0);
  Float_t  IC1 = ICN->GetCal(1);
  Float_t  IC2 = ICN->GetCal(2);
  Float_t  IC3 = ICN->GetCal(3);
  Float_t  IC4 = ICN->GetCal(4);
  Float_t  IC5 = ICN->GetCal(5);
  Float_t  IC6 = ICN->GetCal(6);
  dE = IC0 + IC1;

  Float_t E1 = 1.*(IC0+IC1)+2.*(IC2+IC3+IC4+0.*IC5+0.*IC6);
  E1 *= 0.004;
  SetCalData(0,dE);
  SetCalData(1,E);
  


  if(Beta>0 && Beta <0.3 && Gamma>1.&&ETot>0.)
    { 

      M = E1/931.5016/(Gamma-1.); 
      M_Q = Rec->GetBrho()/3.105/Beta/Gamma;
      Q = M/M_Q;
      
    }
  

  SetCalData(2,T);
  SetCalData(3,V);
  SetCalData(4,D);
  SetCalData(5,Beta);
  SetCalData(6,Gamma);
  SetCalData(7,M);
  SetCalData(8,M_Q);
  SetCalData(9,Q);
  SetCalData(13,BetaTP);
  SetCalData(14,VTP);
  SetCalData(15,D_1);

  
  if(T > 0 && V > 0 && M_Q > 0 && M > 0 && Beta > 0 && Gamma > 1.0) 
    {
      isPresent=true;
    }
  
  return (isPresent);

  END;
}



#ifdef WITH_ROOT
void Identification::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
  // Set histogram Hierarchy
  SetHistogramsCal1DFolder("Id1D");
  SetHistogramsCal2DFolder("Id2D");
 
  if(fMode == MODE_WATCHER)
    {
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
    }
  else if(fMode == MODE_D2R)
	 {
      ;
	 }
  else if(fMode == MODE_D2A)
	 {
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
		  }
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
		  }      
		SetOutAttachCalI(true);
		OutAttach(OutTTree);		
    }
  else if(fMode == MODE_RECAL)
    {
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	}      
      SetOutAttachCalI(true);
      OutAttach(OutTTree);		
    }
  else if(fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}


void Identification::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
  
  AddHistoCal(GetCalName(2),"T","TOF (ns)",1000,0.,500.);
  AddHistoCal(GetCalName(3),"V","V (cm/ns)",1000,0.,5.);
  AddHistoCal(GetCalName(4),"D","D (cm)",1000,700.,900.);
  AddHistoCal(GetCalName(5),"Beta","Beta (v/c)",200,0.0,0.2);
#ifdef D_MUST2
  AddHistoCal(GetCalName(7),"M","Mass ",1000,1.,201.);
  AddHistoCal(GetCalName(8),"M_Q","M/Q",1000,0.5,5.5);
#else
  AddHistoCal(GetCalName(7),"M","Mass ",1000,1.,201.);
  AddHistoCal(GetCalName(8),"M_Q","M/Q",1000,0,15);
#endif
  END;
}

void Identification::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }
#ifdef D_MUST2
  AddHistoCal(GetCalName(1),GetCalName(0),"dE_E","dE_E",2000,0,500,1000,0,500);
  AddHistoCal(GetCalName(7),GetCalName(8),"M_M_Q","M vs M/Q",1500,0,20,3000,1.0,2.5);
  AddHistoCal(GetCalName(9),GetCalName(8),"Q_M_Q","Q vs M/Q",1500,0,20,3000,1.0,2.5);
  AddHistoCal(GetCalName(2),GetCalName(0),"dE_TOF","dE vs TOF",1000,0,300,1000,0,50);
  AddHistoCal(GetCalName(16),GetCalName(17),"BrhoQAr","QAr vs Brho",500,0,20,500,0.8,1.5);
#else
  AddHistoCal(GetCalName(1),GetCalName(0),"dE_E","dE_E",2000,50,450,2000,50,450);
  AddHistoCal(GetCalName(7),GetCalName(8),"M_M_Q","M vs M/Q",1500,50,200,3000,2.0,4.5);
  //AddHistoCal(GetCalName(16),GetCalName(17),"BrhoQAr","QAr vs Brho",300,13,20,500,0.8,1.5);
#endif
  END;
}
#endif
