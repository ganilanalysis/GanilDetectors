/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "IonisationChamberNUMEXO.hh"

#define DEBUG

IonisationChamberNUMEXO::IonisationChamberNUMEXO(const Char_t *Name,
        UShort_t NDetectors,
        UShort_t NRow,
        Bool_t RawData,
        Bool_t CalData,
        Bool_t DefaultCalibration,
        const Char_t *NameExt,
        UShort_t MaxMult
        )
: BaseDetector(Name, NRow + 4 , false, CalData, DefaultCalibration, false, NameExt, true) {
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors = RawData;
    fMaxMultSubDetectors = MaxMult;

    FP = NULL;
    DetManager *DM = DetManager::getInstance();
    // FP = (FocalPosition*) DM->GetDetector("FP");

    // Set How many IC rows
    N_Row = NRow;
    N_Col = 1;
    AllocateComponents();
    char line[100];

    for (UShort_t i = 0; i < N_Row; i++) {
        sprintf(line, "IC%d", i);
        AddCounter(line); // 2
    }
    AddCounter("Mult Pad > 1"); // N_Row+2

    if (CalData) {
        for (UShort_t i = 0; i < N_Row; i++) {
            sprintf(line, "ICdE%d", i);
            sprintf(CalNameI[i], "%s", line);
        }
        for (UShort_t i = 0; i < N_Col; i++) {
            sprintf(line, "E_%d", i);
            sprintf(CalNameI[NRow + 2 * i], "%s", line);
            sprintf(line, "dE_%d", i);
            sprintf(CalNameI[NRow + 2 * i + 1], "%s", line);
        }

        // if(NumberOfDetectors >= N_Row+2)
        if (NumberOfDetectors >= N_Row + 2 * N_Col + 2) {
            sprintf(CalNameI[N_Row + 2 * N_Col], "ICdE");
            sprintf(CalNameI[N_Row + 2 * N_Col + 1], "ICdEM1");
        } else {
            Char_t Message[500];
            sprintf(Message, "In <%s><%s> Trying to set calibrated data beyond the number of declared calibrated data (%d) !", GetName(), GetName(), NumberOfDetectors);
            MErr * Er = new MErr(WhoamI, 0, 0, Message);
            throw Er;
        }
    }

    AllocateLocalArrays();

#ifdef WITH_ROOT
    // Setup Histogram Hierarchy
    SetHistogramsCal1DFolder("IC_C");
    SetHistogramsCal2DFolder("IC_C");
    SetHistogramsRaw1DFolder("IC_R");
    SetHistogramsRaw2DFolder("IC_R");
#endif


    END;
}

IonisationChamberNUMEXO::~IonisationChamberNUMEXO(void) {
    START;
    DeAllocateLocalArrays();

    END;
}

void IonisationChamberNUMEXO::AllocateLocalArrays(void) {
    START;

    // Row Energies array
    E_Row = AllocateDynamicArray<Float_t>(N_Row);
    E_Col = AllocateDynamicArray<Float_t>(N_Col);
    dE = AllocateDynamicArray<Float_t>(N_Col);
    M_Row = AllocateDynamicArray<UShort_t>(N_Row);

    END;
}

void IonisationChamberNUMEXO::DeAllocateLocalArrays(void) {
    START;

    E_Row = FreeDynamicArray<Float_t>(E_Row);
    E_Col = FreeDynamicArray<Float_t>(E_Col);
    dE = FreeDynamicArray<Float_t>(dE);
    M_Row = FreeDynamicArray<UShort_t>(M_Row);

    END;
}

void IonisationChamberNUMEXO::ClearCal(void) {
    START;
    BaseDetector::ClearCal();
    if (isComposite) {
        for (UShort_t i = 0; i < DetList->size(); i++)
            DetList->at(i)->ClearCal();
    }

    for (UShort_t i = 0; i < N_Row; i++) {
        E_Row[i] = 0.;
        M_Row[i] = 0;
    }
    for (UShort_t i = 0; i < N_Col; i++) {
        E_Col[i] = 0.;
        dE[i] = 0;
    }
    ETotal = 0;
    ETotalM1 = 0;
    END;
}

void IonisationChamberNUMEXO::AllocateComponents(void) {
    START;
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Char_t Name[100];
    if (strcmp(DetectorName, "IcH") == 0)
        sprintf(Name, "ICH");
    else if (strcmp(DetectorName, "Ic_Numexo") == 0)
        sprintf(Name, "IC");
    else if (strcmp(DetectorName, "TFPMW_Numexo") == 0)
        sprintf(Name, "FPMWPat");
    else
        sprintf(Name, "IC");


    BaseDetector *D = new BaseDetector(Name, NumberOfSubDetectors, RawData, CalData, DefCal, true, "",true,true,fMaxMultSubDetectors);
    if (fCalData) {
        D->ReadCalGates();
    }
#ifdef WITH_ROOT
    D->SetMainHistogramFolder(MainHistogramFolder);
#endif
    AddComponent(D);

    END;
}

void IonisationChamberNUMEXO::SetParameters(NUMEXOParameters* Par, Map* Map) {
    START;
    Char_t Name[200];
    
    for (UShort_t i = 0; i < NumberOfSubDetectors; i++) {

        sprintf(Name, "%s_%02d", DetList->at(0)->GetName(), i);
        DetList->at(0)->SetParameterName(Name, i);
        DetList->at(0)->SetRawHistogramsParams(64000,0,63999);
        DetList->at(0)->SetGateRaw(0,63999);
    }

    NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
    for (UShort_t i = 0; i < DetList->size(); i++)
        DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);
    END;
}

Bool_t IonisationChamberNUMEXO::Treat(void) {
    START;
    UShort_t RowNr;
    UShort_t ColNr;

    Ctr->at(0)++;

    if (isComposite) {
      for (UShort_t i = 0; i < DetList->size(); i++) {
        DetList->at(i)->Treat();
      }
    }
    if (fCalData) {
        for (UShort_t i = 0; i < DetList->at(0)->GetM(); i++) {
            RowNr = (Int_t) (DetList->at(0)->GetNrAt(i) / 5);
            ColNr = (Int_t) (DetList->at(0)->GetNrAt(i) % 5);
            if (DetList->at(0)->GetCalAt(i) > 0.) {

                // Will do position later
                // if(FP)
                //   if (FP->IsPresent()) 
                // 	 {
                // 	   // hasValidTrack = DetList->at(0)->CheckTrack(FP->GetXf(),FP->GetTf(),FP->GetYf(),FP->GetPf(),FP->GetRefZ(),DetList->at(0)->GetNrAt(i));
                // 	 }

                E_Row[RowNr] += DetList->at(0)->GetCalAt(i);
                E_Col[ColNr] += DetList->at(0)->GetCalAt(i);

                if (RowNr == 0)
                    dE[ColNr] = DetList->at(0)->GetCalAt(i);

                M_Row[RowNr]++;
                ETotal += DetList->at(0)->GetCalAt(i);
            }

            if (M_Row[RowNr] == 1)
                ETotalM1 = ETotal;
        }

        for (UShort_t i = 0; i < N_Row; i++)
            if (M_Row[i] == 1) {
                Ctr->at(i + 2)++;
            } else {
                Ctr->at(2 + N_Row)++;
                ETotalM1 = 0.;
            }

        for (Int_t i = 0; i < N_Row; i++)
            if (E_Row[i] > 0) {
                SetCalData(i, E_Row[i]);
            } else
                ETotalM1 = 0.;

        for (Int_t i = 0; i < N_Col; i++) {
            if (E_Col[i] > 0)
                SetCalData(N_Row + 2 * i, E_Col[i]);
            if (dE[i] > 0)
                SetCalData(N_Row + 2 * i + 1, dE[i]);
        }

        if (ETotal > 0.) {
            SetCalData(N_Row + 2 * N_Col, ETotal);
            SetCalData(N_Row + 2 * N_Col + 1, ETotalM1);
            isPresent = true;
        }
    }


    return (isPresent);
    END;
}

#ifdef WITH_ROOT

void IonisationChamberNUMEXO::CreateHistogramsCal1D(TDirectory *Dir) {
    START;

    string Name;
    Dir->cd("");
    if (SubFolderHistCal1D.size() > 0) {
        Name.clear();
        Name = SubFolderHistCal1D;
        if (!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

//    // IC A
//    AddHistoCal(CalNameI[0], CalNameI[0], "ICA (MeV)", 500, 0., 1000.);
//    // IC B
//    AddHistoCal(CalNameI[1], CalNameI[1], "ICB (MeV)", 500, 0., 1000.);
//    // IC C
//    AddHistoCal(CalNameI[2], CalNameI[2], "ICB (MeV)", 500, 0., 1000.);
//    // IC Time
//    AddHistoCal(CalNameI[3], CalNameI[3], "ICT (MeV)", 500, 0., 1000.);

    END;
}

void IonisationChamberNUMEXO::CreateHistogramsCal2D(TDirectory *Dir) {
    START;
    string Name;

    BaseDetector::CreateHistogramsCal2D(Dir);

    Dir->cd("");

    if (SubFolderHistCal2D.size() > 0) {
        Name.clear();
        Name = SubFolderHistCal2D;
        if (!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    for (Int_t i = 0; i < N_Col; i++) {
        AddHistoCal(GetCalName(N_Row + 2 * i), GetCalName(N_Row + 2 * i + 1), Form("dE_E_col%d", i), Form("dE_E Col %d", i), 1000, 0, 100, 1000, 0, 100);
    }

    DetList->at(0)->AddHistoCal( DetList->at(0)->GetCalName(0), DetList->at(0)->GetCalName(1),"IC_0_1","IC_0_1",1000,0,100,1000,0,100);
    DetList->at(0)->AddHistoCal( DetList->at(0)->GetCalName(0), DetList->at(0)->GetCalName(2),"IC_0_2","IC_0_2",1000,0,100,1000,0,100);
    DetList->at(0)->AddHistoCal( DetList->at(0)->GetCalName(0), DetList->at(0)->GetCalName(3),"IC_0_3","IC_0_3",1000,0,100,1000,0,100);
    DetList->at(0)->AddHistoCal( DetList->at(0)->GetCalName(0), DetList->at(0)->GetCalName(4),"IC_0_4","IC_0_4",1000,0,100,1000,0,100);
    DetList->at(0)->AddHistoCal( DetList->at(0)->GetCalName(0), DetList->at(0)->GetCalName(5),"IC_0_5","IC_0_5",1000,0,100,1000,0,100);
      DetList->at(0)->AddHistoCal( DetList->at(0)->GetCalName(0), DetList->at(0)->GetCalName(6),"IC_0_6","IC_0_6",1000,0,100,1000,0,100);
// =======

//     AddHistoCal(GetCalName(0),GetCalName(1),"IC_0_1","IC_0_1",1000,0,100,1000,0,100);
//     AddHistoCal(GetCalName(0),GetCalName(2),"IC_0_2","IC_0_2",1000,0,100,1000,0,100);
//     AddHistoCal(GetCalName(0),GetCalName(3),"IC_0_3","IC_0_3",1000,0,100,1000,0,100);
//     AddHistoCal(GetCalName(0),GetCalName(4),"IC_0_4","IC_0_4",1000,0,100,1000,0,100);
//     AddHistoCal(GetCalName(0),GetCalName(5),"IC_0_5","IC_0_5",1000,0,100,1000,0,100);
// >>>>>>> 00e723ade40eb45efca5998ffbc695fb466fe518


    END;
}

void IonisationChamberNUMEXO::SetOpt(TTree *OutTTree, TTree *InTTree) {
    START;
    Char_t Name[200];
    // Set histogram Hierarchy
    for (UShort_t i = 0; i < DetList->size(); i++) {
        DetList->at(i)->SetMainHistogramFolder("");
        DetList->at(i)->SetHistogramsRaw1DFolder(Form("%s_R",DetList->at(0)->GetName()));
        DetList->at(i)->SetHistogramsRaw2DFolder(Form("%s_R",DetList->at(0)->GetName()));
        DetList->at(i)->SetHistogramsCal1DFolder(Form("%s_C",DetList->at(0)->GetName()));
        DetList->at(i)->SetHistogramsCal2DFolder(Form("%s_C",DetList->at(0)->GetName()));
    }

    for (UShort_t i = 0; i < NumberOfSubDetectors; i++) {
       sprintf(Name,"%s_%02d",DetList->at(0)->GetName(), i);
       DetList->at(0)->SetParameterNameInRawTree(Name, i);
    }

    if (fMode == MODE_WATCHER) {
        if (fRawData) {
            SetHistogramsRaw(true);
        }
        if (fCalData) {
            SetHistogramsCal(true);
        }
        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasRawData()) {
                DetList->at(i)->SetHistogramsRaw(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
            }
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(true);
            }
        }
    } else if (fMode == MODE_D2R) {
        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasRawData()) {
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
                DetList->at(i)->SetOutAttachRawV(true);
                DetList->at(i)->SetOutAttachRawI(false);
                DetList->at(i)->SetOutAttachTS(true);
		DetList->at(i)->SetOutAttachRawMult(true);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    } else if (fMode == MODE_D2A) {
        if (fRawData) {
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(true);
        }
        if (fCalData) {
            SetHistogramsCal(true);
            SetOutAttachCalI(false);
            SetOutAttachCalV(false);
        }

        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasRawData()) {
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
              //  DetList->at(i)->SetOutAttachRawI(true);
                DetList->at(i)->SetOutAttachRawV(true);
                DetList->at(i)->SetOutAttachTS(true);
		DetList->at(i)->SetOutAttachRawMult(true);

            }
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalI(false);
                DetList->at(i)->SetOutAttachCalV(false);
                DetList->at(i)->SetOutAttachCalF(true);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }

        OutAttach(OutTTree);
    } else if (fMode == MODE_R2A) {
        for (UShort_t i = 0; i < DetList->size(); i++) {
            DetList->at(i)->SetInAttachRawV(false);
            DetList->at(i)->InAttach(InTTree);
        }

        SetOutAttachCalI(true);

        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalF(true);
                DetList->at(i)->SetOutAttachCalV(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if (fMode == MODE_RECAL) {

        for (UShort_t i = 0; i < DetList->size(); i++) {
            SetInAttachCalI(true);
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetInAttachCalF(true);
            }
            DetList->at(i)->InAttachCal(InTTree);
        }
        InAttachCal(InTTree);
        SetOutAttachCalI(true);
        for (UShort_t i = 0; i < DetList->size(); i++) {
            if (DetList->at(i)->HasCalData()) {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalF(true);
                DetList->at(i)->SetOutAttachCalV(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    } else if (fMode == MODE_CALC) {
        SetNoOutput();
    } else {
        Char_t Message[500];
        sprintf(Message, "In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(), fMode);
        MErr * Er = new MErr(WhoamI, 0, 0, Message);
        throw Er;
    }
    END;
}





#endif
