#include "MTDC.hh"

MTDC::MTDC(const Char_t * Name,
           UShort_t NDetectors,
           Bool_t RawData,
           Bool_t CalData,
           Bool_t DefaultCalibration)
    : BaseDetector(Name,0,false,CalData,true,false,"")
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  AllocateComponents();
  TDC = nullptr;
  if(CalData)
  {
//
  }
  END;
}


MTDC::~MTDC()
{
  START;

  END;
}



void MTDC::AllocateComponents()
{
  START;
  Char_t Name[200];
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;

  // For Every telescope Allocate E and DE subdetectors
  // Delta E Strips
  sprintf(Name,"%s",DetectorName);
  TDC = new BaseDetector(Name,NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"",false,true);
  if(RawData)
  {
    for (int i=0; i < NumberOfSubDetectors; i++)
    {
      sprintf(Name,"%s_%02d",DetectorName,i);
      TDC->SetParameterName(Name, i);
    }
    TDC->SetGateRaw(0,65534);
    TDC->SetRawHistogramsParams(16384,0, 65533,"");
  }
  if(CalData)
  {
    TDC->HasCalCharges(false);
    TDC->SetCalHistogramsParams(1000,0,1000,"ns");
  }
  AddComponent(TDC);

  END;
}
void MTDC::SetParameters(Parameters* Par,Map* Map)
{
  START;
  MesytecParameters *MP = MesytecParameters::getInstance();
  if(isComposite)
  {
    for(UShort_t i=0; i< DetList->size(); i++)
    {
      DetList->at(i)->SetParametersMesytec(MP,Map);
    }

  }
  END;
}

Bool_t MTDC::Treat()
{
  START;

  Ctr->at(0)++;



  if(isComposite)
  {
    for(UShort_t i=0; i< DetList->size(); i++)
    {

        DetList->at(i)->Treat();
    }
  }

  return IsPresent();
  END;
}
#ifdef WITH_ROOT

void MTDC::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  SetMainHistogramFolder("");

  SetHistogramsCal1DFolder("");
  SetHistogramsCal2DFolder("");
  for(UShort_t i = 0;i<DetList->size();i++)
  {
    
    DetList->at(i)->SetMainHistogramFolder("");
    DetList->at(i)->SetHistogramsRaw1DFolder("MTDC_R");
    DetList->at(i)->SetHistogramsRaw2DFolder("MTDC_R");
    DetList->at(i)->SetHistogramsCal1DFolder("MTDC_C");
    DetList->at(i)->SetHistogramsCal2DFolder("MTDC_C");
  }

  if(fMode == MODE_WATCHER)
  {
    if(fRawData)
    {
      SetHistogramsRaw(true);
    }
    if(fCalData)
    {
      SetHistogramsCal(true);
    }
    for(UShort_t i = 0;i<DetList->size();i++)
    {
      if(DetList->at(i)->HasRawData())
      {

          DetList->at(i)->SetHistogramsRaw1D(false);
          DetList->at(i)->SetHistogramsRaw2D(true);
          DetList->at(i)->SetHistogramsRawSummary(true);
          DetList->at(i)->SetOutAttachRawV(false);
          DetList->at(i)->SetOutAttachRawI(false);
      }
      if(DetList->at(i)->HasCalData())
      {
        DetList->at(i)->SetHistogramsCal1D(false);
        DetList->at(i)->SetHistogramsCal2D(true);
        DetList->at(i)->SetHistogramsCalSummary(true);
      }
    }
  }
  else if(fMode == MODE_D2R)
  {
    for(UShort_t i = 0;i<DetList->size();i++)
    {
      if(DetList->at(i)->HasRawData())
      {

          DetList->at(i)->SetHistogramsRaw1D(false);
          DetList->at(i)->SetHistogramsRaw2D(true);
          DetList->at(i)->SetHistogramsRawSummary(true);
          DetList->at(i)->SetOutAttachRawV(true);
          DetList->at(i)->SetOutAttachRawF(false);
          DetList->at(i)->SetOutAttachRawI(false);
	  DetList->at(i)->SetOutAttachTS(true);
      }
      DetList->at(i)->OutAttach(OutTTree);
    }
    OutAttach(OutTTree);

  }
  else if(fMode == MODE_D2A)
  {
    if(fRawData)
    {
      SetHistogramsRaw(false);
      SetOutAttachRawI(false);
      SetOutAttachRawV(false);
    }
    if(fCalData)
    {
      SetHistogramsCal(true);
      SetOutAttachCalI(false);
      SetOutAttachCalV(false);
    }

    for(UShort_t i = 0;i<DetList->size();i++)
    {
      if(DetList->at(i)->HasRawData())
      {

          DetList->at(i)->SetHistogramsRaw1D(false);
          DetList->at(i)->SetHistogramsRaw2D(true);
          DetList->at(i)->SetHistogramsRawSummary(true);
          DetList->at(i)->SetOutAttachRawV(true);
          DetList->at(i)->SetOutAttachRawI(false);
          DetList->at(i)->SetOutAttachTS(true);

      }
      if(DetList->at(i)->HasCalData())
      {

          DetList->at(i)->SetHistogramsCal1D(false);
          DetList->at(i)->SetHistogramsCal2D(true);
          DetList->at(i)->SetHistogramsCalSummary(true);
          DetList->at(i)->SetOutAttachCalV(true);
          DetList->at(i)->SetOutAttachCalI(false);
      }
      DetList->at(i)->OutAttach(OutTTree);
    }

    OutAttach(OutTTree);
  }
  else if(fMode == MODE_R2A)
  {

    SetNoOutput();

  }
  else if(fMode == MODE_RECAL)
  {

    SetNoOutput();

  }
  else if (fMode == MODE_CALC)
  {
    SetNoOutput();
  }
  else
  {
    Char_t Message[500];
    sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
    MErr * Er= new MErr(WhoamI,0,0, Message);
    throw Er;
  }
  END;
}



#endif
