/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "Rec3D.hh"

Rec3D::Rec3D(const Char_t *Name,
                             UShort_t NDetectors, 
                             Bool_t RawData, 
                             Bool_t CalData, 
                             Bool_t DefaultCalibration,
                             const Char_t *NameExt
                             ) : BaseDetector(Name, 7, false, CalData, DefaultCalibration,false,NameExt)
{
  START;
  DetManager *DM = DetManager::getInstance();

  FP = NULL;
  FP = (FocalPosition*) DM->GetDetector("FP");

  TP = NULL;
  TP = (TargetPos*) DM->GetDetector("TP");

  
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;

  VamosAngle = FP->GetVamosAngle();

  // Allocate Coeffs Array
  RCoeffs = NULL;
  RCoeffs = AllocateDynamicArray<Double_t>(4,1050);
  for(UShort_t i=0;i < 4; i++)
    for(UShort_t j=0;j < 1050; j++)
      RCoeffs[i][j] = 0.;
    
  MRec = NULL;
  MRec = AllocateDynamicArray<Int_t>(1100,550,400,3);
  for(UShort_t i=0;i < 1100; i++)
    for(UShort_t j=0;j < 550; j++)
      for(UShort_t k=0; k < 400; k++)
      for(UShort_t l=0;l < 3; l++)
        MRec[i][j][k][l] = 0;

  // Read Rec3D Input  
  ReadCalibration();

  // Counters
  AddCounter("Called With Drift Present");

  if(fCalData)
    {
      sprintf(CalNameI[0],"Brho1");
      sprintf(CalNameI[1],"Theta1");
      sprintf(CalNameI[2],"Phi1");
      sprintf(CalNameI[3],"Path1");
      sprintf(CalNameI[4],"ThetaL1");
      sprintf(CalNameI[5],"ThetaLdeg1");
      sprintf(CalNameI[6],"PhiL1");
    }
  END;
}

Rec3D::~Rec3D(void)
{
  START;
  // Clear Coeffs Array
  RCoeffs = FreeDynamicArray<Double_t>(RCoeffs,4);
  MRec = FreeDynamicArray<Int_t>(MRec,1100,550,400);

  END;
}

void Rec3D::ReadCalibration(void)
{ 
  START;
  MIFile *IF;
  MIFile *IF1;
  int Len=255;
  char Line[255];
  char Line1[255];
  stringstream *InOut;
  
  if(fCalData)
	 {
		InOut = new stringstream();
		*InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << ".cal";
		*InOut>>Line;
		InOut = CleanDelete(InOut);
		if((IF = CheckCalibration(Line)))
		  {
          cout <<  "    <" << DetectorName << "><" << DetectorNameExt << "> Reading " << Line << endl;
          L->File <<  "    <" << DetectorName << "><" << DetectorNameExt << "> Reading " << Line << endl;
          for(UShort_t j=0; j<4 ; j++)
            {
              IF->GetLine(Line,Len);
              // cout << Line << endl;
              //  L->File << Line << endl;
              IF->GetLine(Line,Len);
              InOut = new stringstream();
              *InOut  << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << Line;
              *InOut>>Line;
              InOut = CleanDelete(InOut);          
              // Only Phi 
              
              if(j == 2)
                {
                  if((IF1 = CheckCalibration(Line)))
                    {
                      IF1->GetLine(Line,Len);
                      cout << "    <" << DetectorName << "><" << DetectorNameExt << "> " <<  Line << endl;
                      L->File  << "    <" << DetectorName << "><" << DetectorNameExt << "> " <<  Line << endl;
                      
                      for(UShort_t i=0;i<10;i++)
                        {
                          IF1->GetLine(Line,Len);
                          InOut = new stringstream();
                          *InOut << Line;
                          *InOut >> RCoeffs[j][i];
                          InOut = CleanDelete(InOut); 
                        }
                      IF1 = CleanDelete(IF1); 
                    }
                  else
                    {
                      Char_t Message[500];
                      sprintf(Message,"<%s><%s> the input file %s could not be found", DetectorName,DetectorNameExt,Line);
                      MErr * Er = new MErr(WhoamI,0,0, Message);
                      throw Er;
                    }
                }
            }
          IF = CleanDelete(IF);
        }
      else
        {
          Char_t Message[500];
          sprintf(Message,"<%s><%s> the input file %s could not be found", DetectorName,DetectorNameExt,Line);
          MErr * Er = new MErr(WhoamI,0,0, Message);
          throw Er;

        }
      // Now Read the Matrix // Check if updated
      struct stat attrib1;
      
      InOut = new stringstream();
      *InOut  << getenv((EM->getPathVar()).c_str()) << "/Calibs/MRec3D.mat";
      *InOut>>Line1;
      delete InOut;
      
      // InOut = new stringstream();
      // *InOut  << getenv((EM->getPathVar()).c_str()) << "/Calibs/MRec.dat";
      // *InOut>>Line;
      // delete InOut;
    
      if(stat(Line1, &attrib1) ==0) 
        {
            ReadMatrix(Line1);
        }
      else
        {
          cout << "File " << Line1 << " does not exist" << endl;
	
        }
      
    }
  END;
}

void Rec3D::SetBrhoRef(Float_t Brho) 
{
  START;
  cout << "    <" <<  DetectorName << "><" << DetectorNameExt << "> Reference Brho set to " << Brho << " Tm" << endl;
  L->File << "    <" <<  DetectorName << "><" << DetectorNameExt << "> Reference Brho set to " << Brho << " Tm" <<  endl;
  BrhoRef = Brho;
  END;
};
  


void Rec3D::ReadMatrix(Char_t * FName)
{
  START;

  int Len=255;
  int i,j,k;
  char Line[100];
  stringstream *InOut;
  
  try{
	MIFile *IF = new MIFile(FName);
	cout << "    <" << DetectorName << "><" << DetectorNameExt << "> Reading " << FName << endl;
	L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> Reading " << FName << endl;
	while(IF->GetLine(Line,Len))
	  {
	    InOut = new stringstream();
	    *InOut << Line;
	    *InOut >> i;
	    *InOut >> j;
	    *InOut >> k;
	    *InOut >> MRec[i][j][k][0];
	    *InOut >> MRec[i][j][k][1];
	    *InOut >> MRec[i][j][k][2];
	    delete InOut;
	  }
	delete IF;
  }
  
  catch(MErr * Er)
    {
      Er->Set(WhoamI);
      throw Er;
    }    

  END;
}


void Rec3D::Calculate(void)
{
  START;
  Double_t Brhot,Thetat,Phit,Patht;
  // Double_t Brhoty,Thetaty,Pathty;
  Double_t Vec[5],Vecp;
  Int_t VecI[3];
  Int_t i,j[10];
  
  Double_t Brho,Theta,Phi,Path;
  Double_t ThetaL,PhiL;
  
  Brho = Theta = Phi = Path = -500;
  ThetaL  = PhiL = -500.;
				
  Double_t ThetaLdeg = -500.;
  Double_t RecPhi;

  Brhot  = 0.;
  Thetat = 0.;
  Phit   = 0.;
  Patht  = 0.;
  RecPhi = 1.;


  // cout << "-----> X " << FP->GetXf() << " Th " <<   FP->GetTf() << " BrhoRef " << BrhoRef << endl;
  VecI[0] = (int) (FP->GetXf()+600.);
  VecI[1] = (int) (FP->GetTf()+200.);
  VecI[2] = (int) (TP->GetPhi()+200.);
  if(
     (VecI[0] >=0 && VecI[0] < 1100)
     &&
     (VecI[1] >=0 && VecI[1] < 550)
     )
    {
      Rnd->Next();
      Brhot = (((Double_t) MRec[VecI[0]][VecI[1]][VecI[2]][0]) + Rnd->Value())/1000.;
      Rnd->Next();
      Thetat = (((Double_t) MRec[VecI[0]][VecI[1]][VecI[2]][1]) + Rnd->Value()) - 200.;
      Thetat *=-1;
      Rnd->Next();
      Patht = (((Double_t) MRec[VecI[0]][VecI[1]][VecI[2]][2]) + Rnd->Value()) /10.;
    }

  
  Vec[0] = 1.000000;
  Vec[1] =(Double_t) (-1. * (FP->GetXf())/1000.);
  Vec[2] =(Double_t) (-1. * (FP->GetYf())/1000.);
  Vec[3] =(Double_t) (-1. * (FP->GetTf())/1000.);
  Vec[4] =(Double_t) (-1. * atan(tan((FP->GetPf())/1000.)*cos((FP->GetTf())/1000.)));
 
  i = 0;
 for(j[0]=0;j[0]<5;j[0]++)
    for(j[1]=j[0];j[1]<5;j[1]++)
      for(j[2]=j[1];j[2]<5;j[2]++)
        for(j[3]=j[2];j[3]<5;j[3]++)
          for(j[4]=j[3];j[4]<5;j[4]++)
            for(j[5]=j[4];j[5]<5;j[5]++)
              for(j[6]=j[5];j[6]<5;j[6]++)
                for(j[7]=j[6];j[7]<5;j[7]++)
                  for(j[8]=j[7];j[8]<5;j[8]++)
                    for(j[9]=j[8];j[9]<5;j[9]++)
                      {
                        Vecp = Vec[j[0]]*Vec[j[1]]*Vec[j[2]]*Vec[j[3]]*Vec[j[4]]*Vec[j[5]]*Vec[j[6]]*Vec[j[7]]*Vec[j[8]]*Vec[j[9]];
                        Phit += RCoeffs[2][i] *Vecp;
                        i++;
                      }

  if(Phit > -300. && Phit < 300.)
    RecPhi=1;
  else
    {
      Phit=0;
      RecPhi=0;
    }
  if(Brhot >0.001 && Thetat > -300. && Thetat < 300. 
                                                && Phit > -300. && Phit < 300. && Patht >0 && Patht < 2000.)
    {
      isPresent = true;
      Brho = BrhoRef*((Float_t) Brhot);
      SetCalData(0,Brho);
      Theta = (Float_t) Thetat*-1;
      SetCalData(1,Theta);
      Phi = (Float_t) Phit*-1;
      SetCalData(2,Phi);
      Path = (Float_t) Patht;
      SetCalData(3,Path);
      
#ifdef WITH_ROOT 
      // Has to be changed to be incorporated in Narwal actor 
      TVector3 *myVec;
      // TVector3 *myVecy;
      myVec = new TVector3(sin(Theta/1000.)*cos(Phi/1000.),sin(Phi/1000.),cos(Theta/1000.)*cos(Phi/1000.));



      ////
      //                          ANGLE VAMOS
      ///////


      myVec->RotateY(VamosAngle*TMath::Pi()/180.); //VAMOS angle

      ////
      //                          ANGLE VAMOS
      ///////

      ThetaL = myVec->Theta();
      PhiL = myVec->Phi();
      ThetaLdeg = ThetaL*TMath::RadToDeg();
      delete myVec;
     
#endif

      SetCalData(4,ThetaL);
      SetCalData(5,ThetaLdeg);      
      SetCalData(6,PhiL);
         //cout << " <OK> " <<  Brho << " " << Theta << " " << Phi << endl;
    } 
  else
    {
      
      //      cout << " <XX> " << Brhot << " " << Thetat << " " << Patht << " " << VecI[0] << " " << VecI[1] << " " << FP->GetXf() << " " << FP->GetTf() << endl;
    }
  END;
}


Bool_t Rec3D::Treat(void)
{ 
  START;
  Ctr->at(0)++;
  if(FP->IsPresent())
    {
      Ctr->at(2)++;
      Calculate();
#ifdef DEBUG
      PrintCal();   
#endif
    }
  
  return(isPresent); 
  END;
}


#ifdef WITH_ROOT
void Rec3D::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
  
  AddHistoCal(GetCalName(0),"Brho","Brho (Tm)",4000,0.5,2.4);
  AddHistoCal(GetCalName(1),"Theta","Theta (mrad)",1000,-200.,200.);
  AddHistoCal(GetCalName(2),"Phi","Phi (mrad)",1000,-200.,200.);
  AddHistoCal(GetCalName(3),"Path","Path (cm)",1000,700.,900.);
  AddHistoCal(GetCalName(4),"ThetaL","Theta Lab (mrad)",1000,0.,1.);
  AddHistoCal(GetCalName(5),"ThetaLdeg","Theta Lab (deg)",300,0.,30.);
  AddHistoCal(GetCalName(6),"PhiL","Phi Lab (mrad)",1000,-10.,10.);

  END;
}

void Rec3D::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }

  AddHistoCal(GetCalName(0),GetCalName(1),"Brho_Theta","Brho_Theta (Tm) (mrad)",4000,0.5,2.4,100,-200,200);
  AddHistoCal(GetCalName(7),GetCalName(1),"Xf_Theta","Xf_Theta (Tm) (mrad)",1000,-500,500,100,-200,200);
  END;
}

void Rec3D::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  SetHistogramsCal1DFolder("Rc1D");
  SetHistogramsCal2DFolder("Rc2D");
 
  if(fMode == MODE_WATCHER)
    {
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
    }
  else if(fMode == MODE_D2R)
    {
      SetNoOutput();
    }
  else if(fMode == MODE_D2A)
    {
      if(fCalData)
	{
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
		  }
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
		  }      
		SetOutAttachCalI(true);
		OutAttach(OutTTree);
		
    } 
  else if(fMode == MODE_RECAL)
    {
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	}      
      SetOutAttachCalI(true);
      OutAttach(OutTTree);
		
    }
  else if(fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}




#endif
