/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "DelhiSecondArm.hh"

DelhiSecondArm::DelhiSecondArm(const Char_t *Name, 
         UShort_t NDetectors, 
         Bool_t RawData, 
         Bool_t CalData, 
         Bool_t DefaultCalibration, 
         const Char_t *NameExt)
  : BaseDetector(Name, 6, false, CalData, DefaultCalibration, false, NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  
  DetManager *DM = DetManager::getInstance();

  FP = NULL;
  FP = (FocalPosition*) DM->GetDetector("FP");

  AllocateComponents();
  
  ReadPosition();

  AddCounter("Present TDC1 "); //2
  AddCounter("Present TDC2 "); //3

  if(fCalData)
    {
      sprintf(CalNameI[0],"%s_X", DetectorName);
      sprintf(CalNameI[1],"%s_Theta", DetectorName);
      sprintf(CalNameI[2],"%s_Y", DetectorName);
      sprintf(CalNameI[3],"%s_Phi", DetectorName);
      sprintf(CalNameI[4],"%s_ThetaL", DetectorName);
      sprintf(CalNameI[5],"%s_PhiL", DetectorName);
      // X in  [-2000,2000]
      SetGateCal(-2000,2000,0);
      // Theta  in  [-2000,2000]   
      SetGateCal(-2000,2000,1);
      // Y in [-2000,2000]
      SetGateCal(-2000,2000,2);
      // Phi in [-2000,2000]
      SetGateCal(-2000,2000,3);
      // ThetaL in [-2000,2000]
      SetGateCal(-2000,2000,4);
      // ThetaL in [-2000,2000]
      SetGateCal(-2000,2000,5);

    }
  //  ReadCalibration();


  END;
}
DelhiSecondArm::~DelhiSecondArm(void)
{
  START;
  END;
}
void DelhiSecondArm::AllocateComponents(void)
{
  START;
  
  fPresentX = false;
  fPresentY = false;
  
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;
 
  string BaseName;
  string Name;  

  BaseDetector *DCX1 = new BaseDetector("T_DSA_X1",1,RawData,CalData,DefCal,PosInfo,"");
  DCX1->SetParameterName("DSA_X1",0);  
  DCX1->SetMainHistogramFolder("");
  DCX1->SetHistogramsCal1DFolder("DSA1D");
  DCX1-> SetHistogramsCal2DFolder("DSA2D");

  if(RawData)
    {
      DCX1->SetRawHistogramsParams(4096,0,16384,"");
    }
  if(CalData)
    {
      DCX1->SetCalName("DSA_X1_C",0);
      DCX1->SetGateCal(-1000,1000);
      DCX1->SetCalHistogramsParams(1000,-1000,1000,"mm","");
    }
   AddComponent(DCX1);
  BaseDetector *DCX2 = new BaseDetector("T_DSA_X2",1,RawData,CalData,DefCal,false,"");
  DCX2->SetParameterName("DSA_X2",0);
  DCX2->SetMainHistogramFolder("");
  DCX2->SetHistogramsCal1DFolder("DSA1D");
  DCX2-> SetHistogramsCal2DFolder("DSA2D");
  if(RawData)
    {
      DCX2->SetRawHistogramsParams(4096,0,16384,"");
    }
  if(CalData)
    {
      DCX2->SetCalName("DSA_X2_C",0);
      DCX2->SetGateCal(-1000,1000);
      DCX2->SetCalHistogramsParams(1000,-1000,1000,"mm","");
    }
   AddComponent(DCX2);
  BaseDetector *DCY1 = new BaseDetector("T_DSA_Y1",1,RawData,CalData,DefCal,false,"");
  DCY1->SetParameterName("DSA_Y1",0);
  DCY1->SetMainHistogramFolder("");
  DCY1->SetHistogramsCal1DFolder("DSA1D");
  DCY1-> SetHistogramsCal2DFolder("DSA2D");
  if(RawData)
    {
      DCY1->SetRawHistogramsParams(4096,0,16384,"");
    }
  if(CalData)
    {
      DCY1->SetCalName("DSA_Y1_C",0);
      DCY1->SetGateCal(-1000,1000);
      DCY1->SetCalHistogramsParams(1000,-1000,1000,"mm","");
    }
   AddComponent(DCY1);
  BaseDetector *DCY2 = new BaseDetector("T_DSA_Y2",1,RawData,CalData,DefCal,false,"");
  DCY2->SetParameterName("DSA_Y2",0);
  DCY2->SetMainHistogramFolder("");
  DCY2->SetHistogramsCal1DFolder("DSA1D");
  DCY2-> SetHistogramsCal2DFolder("DSA2D");
if(RawData)
    {
      DCY2->SetRawHistogramsParams(4096,0,16384,"");
    }
  if(CalData)
    {
      DCY2->SetCalName("DSA_Y2_C",0);
      DCY2->SetGateCal(-1000,1000);
      DCY2->SetCalHistogramsParams(1000,-1000,1000,"mm","");
    }
   AddComponent(DCY2);
  
  

 Name.clear();
  Name ="SI_DSA";
  BaseDetector *SI_DSA = new BaseDetector(Name.c_str(),21,true,CalData,DefCal,false,"");
  
  if(RawData)
    {
      SI_DSA->SetRawHistogramsParams(4096,0,16384,"");
    }
  if(CalData)
    {
      SI_DSA->SetGateCal(0,16384);
      SI_DSA->SetCalHistogramsParams(10000,0,2000,"MeV","");
    }
 
  AddComponent(SI_DSA);

BaseDetector *TSED_DSA = new BaseDetector("T_SED_DSA",1,RawData,CalData,DefCal,false,"");
  TSED_DSA->SetParameterName("TSED_DSA",0);
  TSED_DSA->SetMainHistogramFolder("");
  TSED_DSA->SetHistogramsCal1DFolder("DSA1D");
  TSED_DSA-> SetHistogramsCal2DFolder("DSA2D");
if(RawData)
    {
      TSED_DSA->SetRawHistogramsParams(4096,0,16384,"");
    }
  if(CalData)
    {
      TSED_DSA->SetCalName("TSED_DSA_C",0);
      TSED_DSA->SetGateCal(-1000,1000);
      TSED_DSA->SetCalHistogramsParams(1000,-1000,1000,"mm","");
    }
   AddComponent(TSED_DSA);
  
  

  END;     
}

void DelhiSecondArm::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        {
          DetList->at(i)->SetParameters(Par,Map);
        }
    }  
  END;
}

Bool_t DelhiSecondArm::Treat(void)
{
  START;
  IncrementCounter(0);
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        DetList->at(i)->Treat();
    }

  if(fCalData)
    {
      for(UShort_t i = 0; i < 2 ;i++)
        if(DetList->at(i)->IsPresent())
          IncrementCounter(i);
      
      // Require at both (X,Y) position measurement per chamber 
		
      //  return 1 ;
      if(1)//DetList->at(0)->IsPresent() && DetList->at(1)->IsPresent() && DetList->at(2)->IsPresent() && DetList->at(3)->IsPresent() )
        {
          // cout << "DelhiSecondArm " << GetRefY() << " " << GetRefZ() << endl;

	  Double_t ZRefX1 = 220.; //mm
	  Double_t ZRefY1 = 240.; //mm
	  Double_t ZRefY2 = 260.; //mm
	  Double_t ZRefX2 = 280.; //mm
	  Double_t X1_Offset = 0; //mm
	  Double_t X2_Offset = 0; //mm
	  Double_t Y1_Offset = 0; //mm
	  Double_t Y2_Offset = 0; //mm

	  Double_t X1 = DetList->at(0)->GetCal(0) + X1_Offset; //mm
	  Double_t X2 = DetList->at(1)->GetCal(0) + X2_Offset; //mm
	  Double_t Y1 = DetList->at(2)->GetCal(0) + Y1_Offset; //mm
	  Double_t Y2 = DetList->at(3)->GetCal(0) + Y2_Offset; //mm

	  Double_t  Tf = atan((X1-X2)/(ZRefX1-ZRefX2))*1000.;
	  Double_t  Xf = X1+tan(Tf/1000.)*ZRefX1;

	  Double_t  Pf = atan((Y1-Y2)/(ZRefY1-ZRefY2))*1000.;
	  Double_t  Yf = Y1+tan(Pf/1000.)*ZRefY1;
	  
	  SetCalData(0,Xf);
	  SetCalData(1,Tf);
	  SetCalData(2,Yf);
	  SetCalData(3,Pf);
	  
 
          
#ifdef WITH_ROOT 
          // Has to be changed to be incorporated in Narwal actor 
          //MW target projected coordinates (Theta in 0xz and Phi in 0yz)
          Double_t Theta = GetCal(1);
          Double_t Phi = GetCal(3);
          //Zgoubi coordinates (Theta in 0xz and Phi between v particle and 0xz)
          Double_t ThetaZ = Theta;
          Double_t PhiZ = atan(tan(Phi/1000.)*cos(Theta/1000.))*1000.;
          Double_t ThetaL;
          Double_t PhiL;
          Double_t VamosAngle = FP->GetVamosAngle();
          TVector3 *myVec;
          // TVector3 *myVecy;
          myVec = new TVector3(sin(ThetaZ/1000.)*cos(PhiZ/1000.),sin(PhiZ/1000.),cos(ThetaZ/1000.)*cos(PhiZ/1000.));

          ////
          //                          ANGLE VAMOS
          ///////
          
          
          myVec->RotateY(VamosAngle*TMath::Pi()/180.); //VAMOS angle
          
          ////
          //                          ANGLE VAMOS
          ///////
          
          ThetaL = myVec->Theta();
          PhiL = myVec->Phi();

          //cout << "Theta " << Theta << " Phi " << Phi << " ThetaZ " << ThetaZ << " PhiZ " << PhiZ << " ThetaL " << ThetaL << " PhiL " << PhiL << endl;  
           
          delete myVec;
          
          SetCalData(4,ThetaL);
          SetCalData(5,PhiL);         
#endif
          



        }
      else
        {
	  // Set Value to defaults
	  for(UShort_t i=0;i<NumberOfDetectors;i++)
	    SetCalData(i,-1500.);
	
        }
    }

  
  return(isPresent); 


  END;
}



#ifdef WITH_ROOT
void DelhiSecondArm::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
		
  // XS
  AddHistoCal(CalNameI[0],CalNameI[0],"XS (mm)",1000,-50,50);
  // YS
  AddHistoCal(CalNameI[2],CalNameI[2],"YS (mm)",1000,-50,50);
  // XWA
  AddHistoCal(CalNameI[1],CalNameI[1],"Theta (mrad)",1000,-300,300);
  // YWA
  AddHistoCal(CalNameI[3],CalNameI[3],"Phi (mrad)",1000,-400,400);
 
  END;
}

void DelhiSecondArm::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }

  AddHistoCal(CalNameI[0],CalNameI[2],"XT_YT_TP","X_vs_Y (mm)",600,-20,20,600,-20,20);
  AddHistoCal(CalNameI[1],CalNameI[3],"ThT_PhT","ThT_vs_PhiT (mrad)",600,-200,200,600,-200,200);
  
  END;
}


void DelhiSecondArm::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

 
  if(isComposite)
    for(UShort_t i=0; i< 2; i++)
		{
		  DetList->at(i)->SetOpt(OutTTree,InTTree);
		  // if(VerboseLevel >= V_INFO)
		  // DetList->at(i)->PrintOptions(cout);
		  ///		  DetList->at(i)->PrintOptions(L->File);
		}

                                                          
  SetMainHistogramFolder("DelhiSecondArm");

#ifdef WITH_ROOT
  // Setup Histogram Hierarchy
  SetHistogramsCal1DFolder("DSA1D");
  SetHistogramsCal2DFolder("DSA2D");
#endif

  if(fMode == MODE_WATCHER)
    {
      if(fRawData)
	{
		    SetHistogramsRaw(true);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
      for(UShort_t i = 0;i<DetList->size();i++)
	{
          if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw1D(true);
	      DetList->at(i)->SetHistogramsRawSummary(false);
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal1D(true);
	      DetList->at(i)->SetHistogramsCalSummary(false);
	      
	    }
	}
    }
  else if(fMode == MODE_D2R)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	     	if(i==4)
			  {
			    DetList->at(i)->SetHistogramsRaw1D(false);
			    DetList->at(i)->SetHistogramsRaw2D(true);
			    DetList->at(i)->SetOutAttachRawI(false);
			    DetList->at(i)->SetOutAttachRawV(true);			 
			    DetList->at(i)->SetHistogramsRawSummary(true);
		
			  }
			else
			  {
			    DetList->at(i)->SetHistogramsRaw1D(true);
			    DetList->at(i)->SetHistogramsRaw2D(true);
			    DetList->at(i)->SetOutAttachRawI(true);
			    DetList->at(i)->SetOutAttachRawV(false);			 
			    DetList->at(i)->SetHistogramsRawSummary(false);
			    
			  }
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}
      OutAttach(OutTTree);
      
    }
  else if(fMode == MODE_D2A)
	 {
	   if(fRawData)
	     {
	       SetHistogramsRaw(false);
	       SetOutAttachRawI(false);
	       SetOutAttachRawV(false);
	     }
	   if(fCalData)
	     {
	       SetHistogramsCal(true);
	       SetOutAttachCalI(true);
	       SetOutAttachCalV(false);
	     }
		
	   for(UShort_t i = 0;i<DetList->size();i++)
		  {			
		    if(DetList->at(i)->HasRawData())
		      {
			if(i==4)
			  {
			    DetList->at(i)->SetHistogramsRaw1D(true);
			    DetList->at(i)->SetHistogramsRaw2D(true);
			    DetList->at(i)->SetOutAttachRawI(false);
			    DetList->at(i)->SetOutAttachRawV(true);			 
			    DetList->at(i)->SetHistogramsRawSummary(true);
		
			  }
			else
			  {
			    DetList->at(i)->SetHistogramsRaw1D(true);
			    DetList->at(i)->SetHistogramsRaw2D(true);
			    DetList->at(i)->SetOutAttachRawI(true);
			    DetList->at(i)->SetOutAttachRawV(false);			 
			    DetList->at(i)->SetHistogramsRawSummary(false);
			    
			  }
		      }
		    if(DetList->at(i)->HasCalData())
		      {
			if(i==4)
			  {
			    DetList->at(i)->SetHistogramsCal1D(true);
			    DetList->at(i)->SetOutAttachCalI(false);
			    DetList->at(i)->SetOutAttachCalF(true);
			    DetList->at(i)->SetHistogramsCalSummary(true);
			  }
			else
			  {
			    DetList->at(i)->SetHistogramsCal1D(true);
			    DetList->at(i)->SetOutAttachCalI(true);
			    DetList->at(i)->SetHistogramsCalSummary(false);
			  }
		      }
		    DetList->at(i)->OutAttach(OutTTree);
		  }
	   
	   OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	              DetList->at(i)->InAttach(InTTree);
        }
		
      if(fRawData)
	{
	  SetHistogramsRaw(false);
	  SetOutAttachRawI(false);
	  SetOutAttachRawV(false);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	  SetOutAttachCalV(false);
	}
      
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasCalData())
	    {
                  DetList->at(i)->SetHistogramsCal1D(false);
                  DetList->at(i)->SetHistogramsCalSummary(true);
                  DetList->at(i)->SetOutAttachCalV(true);			 
                  DetList->at(i)->OutAttach(OutTTree);
        	      }
          
		  }
		OutAttach(OutTTree);		
    }
 else if(fMode == MODE_RECAL)
   {
     for(UShort_t i = 0;i<DetList->size();i++)
       {
	 if(i == 2)
	   DetList->at(i)->SetInAttachCalI(true);
	 
	 DetList->at(i)->InAttachCal(InTTree);
	 
       }
      SetInAttachCalI(true);
      InAttachCal(InTTree);
      
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasCalData())
	    {
	      switch (i)
		{
		case 2: // Times
		  DetList->at(i)->SetHistogramsCal1D(true);
		  DetList->at(i)->SetOutAttachCalI(true);
		  break;		    
		default: 
		  break;
		}
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}

    }
  else if(fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }

  if(VerboseLevel >= V_INFO)
    PrintOptions(cout)  ;


  END;
}

#endif
