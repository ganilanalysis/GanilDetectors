#include "MTOF.hh"

MTOF::MTOF(const Char_t *Name,
    UShort_t NDetectors,
    Bool_t RawData,
    Bool_t CalData,
    Bool_t DefaultCalibration,
    const Char_t *NameExt)
  : BaseDetector(Name,0,false, CalData, DefaultCalibration,false,NameExt)

{
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  fCalData = CalData;
  DetManager *DM = DetManager::getInstance();
  AllocateComponents();

}
MTOF::~MTOF(void)
{
  START;


  END;

}

void MTOF::Clear()
{
  START;
  BaseDetector::Clear();
  
  END;
}

void MTOF::AllocateComponents(void)
{
  START;
  // Default Value
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = false;
  Bool_t PosInfo = true;
  UShort_t MaxMult = 1;
  Char_t Name[200];
  ////////////////////////////MTDCs declaration//////////////
  BaseDetector *ADet = NULL;  
  for(UShort_t i=0; i<2; i++)
    { 
      sprintf(Name,"MTDC_%01d",i);
      ADet = new  MTDC(Name,32,RawData,CalData,DefCal);
      //ADet->SetNoOutput();
      AddComponent(ADet);
    }
  ////////////////////////////MTOFs declaration/////////////
  for(UShort_t i=0; i<2; i++)
    for(int j=0;j<2;j++)
      {
	mTOF[i][j] = NULL;
	CalData = true;
	RawData = false;
	sprintf(Name,"MTOF_FP%01d_T%01d",i,j);
	mTOF[i][j] = new BaseDetector (Name,NumberOfSubDetectors,RawData,CalData,DefCal,false,"",false,true,MaxMult);
	mTOF[i][j]->SetNoOutput();
	mTOF[i][j]->SetCalHistogramsParams(2001,-1000,1000,"ns");
	AddComponent(mTOF[i][j]);
      }
  
  END;
}
void MTOF::SetParameters(Parameters* Par, Map *Map)
{
  START;
  if(isComposite)
  {
    for(UShort_t i=0; i< 2; i++)
    //for(UShort_t i=0; i< DetList->size(); i++)
      {
	DetList->at(i)->SetParameters(Par,Map);
      }
  }
  END;
}


Bool_t MTOF::Treat()
{
  START;
  Ctr->at(0)++;
  
  if(isComposite)
  {
    for(UShort_t i=0; i< 2; i++)
    {
      DetList->at(i)->Treat();
    }
  }

  for(int i=0;i<2;i++)
    {
    fRefCh[i]=false;
    MRefCh[2]=0;
    }
  for(int i=0;i<2;i++)
    {
      if(GetSubDet(RefMTDC[i])->GetSubDet(0)->GetRawM()>0)
	for(int k=0;k<GetSubDet(RefMTDC[i])->GetSubDet(0)->GetRawM();k++)
	  if(GetSubDet(RefMTDC[i])->GetSubDet(0)->GetNrAt(k)==RefCh[i])
	  {
	    fRefCh[i]=true;
	    MRefCh[i]=k;
	  }
    }
  
 if(fRefCh[0] || fRefCh[1] )
   {
     for(int k=0;k<GetSubDet(0)->GetSubDet(0)->GetRawM();k++)
       {
	 if(GetSubDet(0)->GetSubDet(0)->GetNrAt(k)<16)
	   {
	     for(int j=0;j<2;j++)
	       if(fRefCh[j])
		 //mTOF[0][j]->SetCalData(GetSubDet(0)->GetSubDet(0)->GetNrAt(k),GetSubDet(RefMTDC[j])->GetSubDet(0)->GetCalAt(MRefCh[j])-GetSubDet(0)->GetSubDet(0)->GetCalAt(k),GetSubDet(0)->GetSubDet(0)->GetRawTSAt(k));
		 mTOF[0][j]->SetCalData(GetSubDet(0)->GetSubDet(0)->GetNrAt(k),GetSubDet(0)->GetSubDet(0)->GetCalAt(k)-GetSubDet(RefMTDC[j])->GetSubDet(0)->GetCalAt(MRefCh[j]),GetSubDet(0)->GetSubDet(0)->GetRawTSAt(k));
	     
	   }
	 else if(GetSubDet(0)->GetSubDet(0)->GetNrAt(k)<32)
	   {
	     for(int j=0;j<2;j++)
	       if(fRefCh[j])
		 //mTOF[1][j]->SetCalData(GetSubDet(0)->GetSubDet(0)->GetNrAt(k)-16,GetSubDet(RefMTDC[j])->GetSubDet(0)->GetCalAt(MRefCh[j])-GetSubDet(0)->GetSubDet(0)->GetCalAt(k),GetSubDet(0)->GetSubDet(0)->GetRawTSAt(k));
		 mTOF[1][j]->SetCalData(GetSubDet(0)->GetSubDet(0)->GetNrAt(k)-16,GetSubDet(0)->GetSubDet(0)->GetCalAt(k)-GetSubDet(RefMTDC[j])->GetSubDet(0)->GetCalAt(MRefCh[j]),GetSubDet(0)->GetSubDet(0)->GetRawTSAt(k));
	   }
       }
     for(int k=0;k<GetSubDet(1)->GetSubDet(0)->GetRawM();k++)
       {     
	 if(GetSubDet(1)->GetSubDet(0)->GetNrAt(k)<4)
	   {
	     for(int j=0;j<2;j++)
	       if(fRefCh[j])
		 //mTOF[0][j]->SetCalData(GetSubDet(1)->GetSubDet(0)->GetNrAt(k)+16,GetSubDet(RefMTDC[j])->GetSubDet(0)->GetCalAt(MRefCh[j])-GetSubDet(1)->GetSubDet(0)->GetCalAt(k),GetSubDet(1)->GetSubDet(0)->GetRawTSAt(k));
		 mTOF[0][j]->SetCalData(GetSubDet(1)->GetSubDet(0)->GetNrAt(k)+16,GetSubDet(1)->GetSubDet(0)->GetCalAt(k)-GetSubDet(RefMTDC[j])->GetSubDet(0)->GetCalAt(MRefCh[j]),GetSubDet(1)->GetSubDet(0)->GetRawTSAt(k));
	   }
	 else if(GetSubDet(1)->GetSubDet(0)->GetNrAt(k)<8)
	   {
	     for(int j=0;j<2;j++)
	       if(fRefCh[j])
		 //mTOF[1][j]->SetCalData(GetSubDet(1)->GetSubDet(0)->GetNrAt(k)+16-4,GetSubDet(RefMTDC[j])->GetSubDet(0)->GetCalAt(MRefCh[j])-GetSubDet(1)->GetSubDet(0)->GetCalAt(k),GetSubDet(1)->GetSubDet(0)->GetRawTSAt(k));
		 mTOF[1][j]->SetCalData(GetSubDet(1)->GetSubDet(0)->GetNrAt(k)+16-4,GetSubDet(1)->GetSubDet(0)->GetCalAt(k)-GetSubDet(RefMTDC[j])->GetSubDet(0)->GetCalAt(MRefCh[j]),GetSubDet(1)->GetSubDet(0)->GetRawTSAt(k));
	   }
       }
   } 
 
  return IsPresent();
  END;
}



#ifdef WITH_ROOT
void MTOF::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
  // Set PISTA  Outputs
  SetMainHistogramFolder("MTOF");
  SetHistogramsRaw1DFolder("Raw");
  SetHistogramsRaw2DFolder("Cal");
  SetHistogramsCal1DFolder("Cal");
  SetHistogramsCal2DFolder("Cal");
  
  for(UShort_t i = 0;i<2;i++)
    {
      DetList->at(i)->SetOpt(OutTTree,InTTree);
      
    }
  
  for(UShort_t i = 2;i<DetList->size();i++)
    {
      DetList->at(i)->SetMainHistogramFolder("");
      DetList->at(i)->SetHistogramsRaw1DFolder("Raw");
      DetList->at(i)->SetHistogramsRaw2DFolder("Cal");
      DetList->at(i)->SetHistogramsCal1DFolder("Cal");
      DetList->at(i)->SetHistogramsCal2DFolder("Cal");
      
      if(fMode == MODE_WATCHER)
	{
	  DetList->at(i)->SetHistogramsCal2DFolder("");
	  DetList->at(i)->SetHistogramsCal1DFolder("");
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCalSummary(true);
	      DetList->at(i)->SetHistogramsCal1D(false);
	    }
	}
      else if(fMode == MODE_D2R)
	{
	  {
	    if(i >= 2)
	      {
		if( DetList->at(i)->HasRawData())
		  {
		    
		    DetList->at(i)->SetOutAttachRawV(true);
		    DetList->at(i)->OutAttach(OutTTree);
		  }
	      }
	  }
	}
      else if(fMode == MODE_D2A)
	{
	  if(i >=2) 
	  {
	    if( DetList->at(i)->HasCalData())
	      {
		DetList->at(i)->SetHistogramsCalSummary(false);
		DetList->at(i)->SetHistogramsCal2D(false);
		DetList->at(i)->SetHistogramsCal1D(false);
		
		DetList->at(i)->SetOutAttachCalTS(true);
		DetList->at(i)->SetOutAttachCalV(true);
	      }
	    DetList->at(i)->OutAttach(OutTTree);
	  }
	  
	  
	}
      else if(fMode == MODE_R2A)
	{
	  
	  SetOutAttachCalV(true);
	  
	  
	}
      else if (fMode == MODE_RECAL)
	{
	  SetInAttachCalI(true);
	  InAttachCal(InTTree);
	  
	  SetOutAttachCalI(true);
	  
	}
      else if (fMode == MODE_CALC)
	{
	  SetNoOutput();
	}
      else
	{
	  Char_t Message[500];
	  sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
	  MErr * Er= new MErr(WhoamI,0,0, Message);
	  throw Er;
	}
      
    }
  OutAttach(OutTTree);
  
  END;
}

void MTOF::CreateHistogramsCal2D(TDirectory* Dir){
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
  Dir->cd("");

  if(SubFolderHistCal2D.size()>0){
    Name.clear();
    Name = SubFolderHistCal2D;
    if(!(gDirectory->GetDirectory(Name.c_str())))
      gDirectory->mkdir(Name.c_str());
    gDirectory->cd(Name.c_str());
  }

  //AddHistoCal(GetCalName(0),GetCalName(1),"PistaXY","PistaXY (mm)",300,-150,150,300,-150,150);
  //AddHistoCal(GetCalName(3),GetCalName(4),"PistaThetaPhi","PistaThetaPhi (rad)",300,-3,3,300,-3.1415,3.1415);

  END;
}
         

#endif

