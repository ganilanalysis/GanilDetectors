/****************************************************************************
 *    Copyright (C) 2012-2016 by Antoine Lemasson
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "GFP.hh"
GFP::GFP(const Char_t * Name,
         UShort_t	NDetectors,
         Bool_t RawData,
         Bool_t CalData,
         Bool_t DefaultCalibration,
         const Char_t * NameExt)
  : BaseDetector(Name, 0, false, CalData, DefaultCalibration,false,NameExt)
{
  START;
  
  NumberOfSubDetectors = NDetectors*4;
  fRawDataSubDetectors  = RawData;

  AllocateComponents();
  
  AddCounter("GFP_E"); //2
  AddCounter("GFP_ED"); //3
  
  // if(fCalData)
  //   {
  //     sprintf(CalNameI[0],"%sAB_1", DetectorName);
  //  	sprintf(CalNameI[1],"%sAB_2", DetectorName);
  //  	sprintf(CalNameI[2],"%sAB_3", DetectorName);
  //  	sprintf(CalNameI[3],"%sAB_4", DetectorName);
  //   }
  
  END;
}


GFP::~GFP(void)
{
  START;
  END;
}


void GFP::AllocateComponents(void)
{
  START;
  Char_t Line[255];
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;


  BaseDetector * TRIG = new BaseDetector("DTRIG_GFP", 1,true,false,false,false,"");
   AddComponent(TRIG);


  sprintf(Line, "GFP");
  BaseDetector * GFP_E = new BaseDetector(Line,NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"E");
  AddComponent(GFP_E);

  sprintf(Line, "GFPD");
  BaseDetector * GFP_ED = new BaseDetector(Line,NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"E");
  AddComponent(GFP_ED);
  
  BaseDetector * T    = new BaseDetector("TAC_GFP", NumberOfSubDetectors/4,true,CalData,DefCal,false,"");
  AddComponent(T);

 
  END;
}


void GFP::SetParameters(Parameters* Par,Map* Map)
{
  START;
  Char_t PName[20];
  if(isComposite){
    if(fRawDataSubDetectors)
      {
        
        DetList->at(0)->SetParameterName("DTRIG_GFP",0);
        
        for(UShort_t i=1;i<3;i++)
          for(UShort_t j=0;j<NumberOfSubDetectors;j++)
            {
              if(j<16)
                sprintf(PName,"ADC_GFP_%d",(i-1)*16+j);
              else
                sprintf(PName,"ADC2_GFP_%d",(i-1)*16+(j-16));

              DetList->at(i)->SetParameterName(PName,j);
            }

        DetList->at(3)->SetParameterName("%s_%02d");
 

        for(UShort_t i=0;i<DetList->size();i++)
          DetList->at(i)->SetParameters(Par, Map);
      }
  }
  END;
}

Bool_t GFP::Treat(void)
{
  START;
  
   if(isComposite)
	  {
		 for(UShort_t i=0; i< DetList->size(); i++)
			{
			  DetList->at(i)->Treat();
			}
	  }

   if(fCalData)
     {

     }


  return isPresent;
  END;
}
 





#ifdef WITH_ROOT
void GFP::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  SetMainHistogramFolder("GFP");
  SetHistogramsCal1DFolder("GFP1D");
  SetHistogramsCal2DFolder("GFP2D");
  for(UShort_t i = 0;i<DetList->size();i++)
	 {
		DetList->at(i)->SetMainHistogramFolder("");
		DetList->at(i)->SetHistogramsRaw1DFolder("GFP1D");
		DetList->at(i)->SetHistogramsRaw2DFolder("GFP2D");
	 	DetList->at(i)->SetHistogramsCal1DFolder("GFP1D");
		DetList->at(i)->SetHistogramsCal2DFolder("GFP2D");
	 }
 
  if(fMode == MODE_WATCHER)
    {
      if(fRawData)
        {
		 	 SetHistogramsRaw(true);
        }
		if(fCalData)
        {
		 	 SetHistogramsCal(true);
        }
      for(UShort_t i = 0;i<DetList->size();i++)
        {
          if(DetList->at(i)->HasRawData())
            {
              if(i==1 || i == 2)
                DetList->at(i)->SetHistogramsRawSummary(true);
              else
		 			 DetList->at(i)->SetHistogramsRaw1D(true);
		 		}
          if(DetList->at(i)->HasCalData())
				{
				  if(i==1 || i == 2)
                {	 
                  DetList->at(i)->SetHistogramsCalSummary(true);
                  DetList->at(i)->SetHistogramsCal1D(true);
                }
              else
					 {
						DetList->at(i)->SetHistogramsCal1D(true);
					 }
				}
		  }
    }
  else if(fMode == MODE_D2R)
	 {
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 if(DetList->at(i)->HasRawData())
				{
				  if(i == 1 || i == 2 || i == 3) 
					 {
						DetList->at(i)->SetOutAttachRawI(true);
						DetList->at(i)->SetOutAttachRawV(false);
						DetList->at(i)->SetHistogramsRaw1D(true);
						DetList->at(i)->SetHistogramsRaw2D(true);
						DetList->at(i)->SetHistogramsRawSummary(true);                 
					 }
				  else 
					 {
						DetList->at(i)->SetHistogramsRaw1D(true);
						DetList->at(i)->SetHistogramsRawSummary(true);                 
					 }
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
	 }
  else if(fMode == MODE_D2A)
	 {
		if(fRawData)
		  {

          SetHistogramsRaw(false);
			 SetOutAttachRawI(false);
			 SetOutAttachRawV(false);
		  }
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
			 SetOutAttachCalV(false);
		  }
		
		for(UShort_t i = 0;i<DetList->size();i++)
		  {			
			 if(DetList->at(i)->HasRawData())
				{

				  if(i == 1 || i == 2 || i==3 )   
					 {
                  DetList->at(i)->SetHistogramsRawSummary(false);
                  DetList->at(i)->SetHistogramsRaw1D(false);
                  DetList->at(i)->SetHistogramsRaw2D(false);
                  DetList->at(i)->SetOutAttachRawI(false);
                  DetList->at(i)->SetOutAttachRawV(false);			 
                }
              else
                {
                  DetList->at(i)->SetOutAttachRawI(false);
                  DetList->at(i)->SetHistogramsRaw1D(false);
                }
                 
				}
			 if(DetList->at(i)->HasCalData())
				{
				  if(i ==1 || i == 2 || i == 3)
					 {
						DetList->at(i)->SetHistogramsCal1D(true);
						DetList->at(i)->SetHistogramsCalSummary(true);
						DetList->at(i)->SetOutAttachCalF(true);

					 }
				  else
					 {
						DetList->at(i)->SetHistogramsCal1D(true);
						DetList->at(i)->SetOutAttachCalI(false);
						DetList->at(i)->SetOutAttachCalF(true);
					 }
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
      SetNoOutput();
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }

  if(VerboseLevel >= V_INFO)
    PrintOptions(cout)  ;

  END;
}

#endif
