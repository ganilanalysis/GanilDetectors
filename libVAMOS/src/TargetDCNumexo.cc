/****************************************************************************
 *    Copyright (C) 2012-2019 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "TargetDCNumexo.hh"

TargetDCNumexo::TargetDCNumexo(const Char_t *Name,
                               UShort_t NDetectors,
                               Bool_t RawData,
                               Bool_t CalData,
                               Bool_t DefaultCalibration,
                               const Char_t *NameExt,
                               UShort_t MaxMult)
    : BaseDetector(Name, 10, false, CalData, DefaultCalibration, false, NameExt,false,false,MaxMult)
{
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;
    fMaxMultSubDetectors=MaxMult;
    DetManager *DM = DetManager::getInstance();

    FP = nullptr;
    FP = (FocalPosition*) DM->GetDetector("FP");

    LTS = nullptr;
    LTS = (TimeStamp*) DM->GetDetector("AGAVA_VAMOSTS");;


    if(!LTS)
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Impossible to Find Reference TS Object (LTS) !", GetName(), GetName());
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    AllocateComponents();

    ReadPosition();

    AddCounter("Present TDC1 "); //2
    AddCounter("Present TDC2 "); //3

    if(fCalData)
    {
        sprintf(CalNameI[0],"%s_X", DetectorName);
        sprintf(CalNameI[1],"%s_Theta", DetectorName);
        sprintf(CalNameI[2],"%s_Y", DetectorName);
        sprintf(CalNameI[3],"%s_Phi", DetectorName);
        sprintf(CalNameI[4],"%s_ThetaL", DetectorName);
        sprintf(CalNameI[5],"%s_PhiL", DetectorName);
        sprintf(CalNameI[6],"%s_X1", DetectorName);
        sprintf(CalNameI[7],"%s_Y1", DetectorName);
        sprintf(CalNameI[8],"%s_X2", DetectorName);
        sprintf(CalNameI[9],"%s_Y2", DetectorName);

        // X in  [-2000,2000]
        SetGateCal(-2000,2000,0);
        // Theta  in  [-2000,2000]
        SetGateCal(-2000,2000,1);
        // Y in [-2000,2000]
        SetGateCal(-2000,2000,2);
        // Phi in [-2000,2000]
        SetGateCal(-2000,2000,3);
        // ThetaL in [-2000,2000]
        SetGateCal(-2000,2000,4);
        // ThetaL in [-2000,2000]
        SetGateCal(-2000,2000,5);
        // X1 in  [-2000,2000]
        SetGateCal(-2000,2000,6);
        // Y1 in  [-2000,2000]
        SetGateCal(-2000,2000,7);
        // X2 in  [-2000,2000]
        SetGateCal(-2000,2000,8);
        // Y2 in  [-2000,2000]
        SetGateCal(-2000,2000,9);


    }

    END;
}
TargetDCNumexo::~TargetDCNumexo(void)
{
    START;
    END;
}
void TargetDCNumexo::AllocateComponents(void)
{
    START;

    fPresentX = false;
    fPresentY = false;

    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Bool_t PosInfo = true;
    Bool_t WithTS = true;

    string BaseName;
    string Name;

    BaseDetector *TargetDC_1 = new BaseDetector("TargetDC_1",4,RawData,CalData,DefCal,PosInfo,"",false,WithTS,fMaxMultSubDetectors);
    if(RawData)
    {
        TargetDC_1->SetParameterName("TDC1_X1",0);
        TargetDC_1->SetParameterName("TDC1_X2",1);
        TargetDC_1->SetParameterName("TDC1_Y1",2);
        TargetDC_1->SetParameterName("TDC1_Y2",3);


        TargetDC_1->SetRawHistogramsParams(64000,0,63999);
        TargetDC_1->SetGateRaw(0,63999);
    }
    if(CalData)
    {
        TargetDC_1->SetCalName("TargetDC1_EX1",0);
        TargetDC_1->SetCalName("TargetDC1_EX2",1);
        TargetDC_1->SetCalName("TargetDC1_EY1",2);
        TargetDC_1->SetCalName("TargetDC1_EY2",3);
        TargetDC_1->SetGateCal(-1000,1000);
        TargetDC_1->SetCalHistogramsParams(1000,-1000,1000,"mm","");
    }
    AddComponent(TargetDC_1);
    cout << "TargetDC_1 Ref Positions : " << endl;
    cout << "X1 :" <<  TargetDC_1->GetActiveArea(0,0) << " ZRef : " <<TargetDC_1->GetActiveArea(0,4) <<endl;;
    cout << "X2 :" <<  TargetDC_1->GetActiveArea(1,0) << " ZRef : " << TargetDC_1->GetActiveArea(1,4)<< endl;;
    cout << "Y1 :" <<  TargetDC_1->GetActiveArea(2,1) << " ZRef : " << TargetDC_1->GetActiveArea(2,4)<< endl;;
    cout << "Y2 :" <<  TargetDC_1->GetActiveArea(3,1) << " ZRef : " << TargetDC_1->GetActiveArea(3,4)<< endl;;

//    BaseDetector *TargetDC_2 = new BaseDetector("TargetDC_2",4,RawData,CalData,DefCal,PosInfo,"",false,WithTS,fMaxMultSubDetectors);
//    if(RawData)
//    {

//        TargetDC_2->SetParameterName("TDC2_X1",0);
//        TargetDC_2->SetParameterName("TDC2_X2",1);
//        TargetDC_2->SetParameterName("TDC2_Y1",2);
//        TargetDC_2->SetParameterName("TDC2_Y2",3);

//        TargetDC_2->SetRawHistogramsParams(64000,0,63999);
//        TargetDC_2->SetGateRaw(0,63999);
//    }
//    if(CalData)
//    {
//        TargetDC_2->SetCalName("TargetDC2_EX1",0);
//        TargetDC_2->SetCalName("TargetDC2_EX2",1);
//        TargetDC_2->SetCalName("TargetDC2_EY1",2);
//        TargetDC_2->SetCalName("TargetDC2_EY2",3);
//        TargetDC_2->SetGateCal(-1000,1000);
//        TargetDC_2->SetCalHistogramsParams(1000,-1000,1000,"mm","");
//    }
//    AddComponent(TargetDC_2);



//    cout << "TargetDC_2 Ref Positions : " << endl;
//    cout << "X1 :" <<  TargetDC_2->GetActiveArea(0,0) << " ZRef : " <<TargetDC_2->GetActiveArea(0,4) <<endl;;
//    cout << "X2 :" <<  TargetDC_2->GetActiveArea(1,0) << " ZRef : " << TargetDC_2->GetActiveArea(1,4)<< endl;;
//    cout << "Y1 :" <<  TargetDC_2->GetActiveArea(2,1) << " ZRef : " << TargetDC_2->GetActiveArea(2,4)<< endl;;
//    cout << "Y1 :" <<  TargetDC_2->GetActiveArea(3,1) << " ZRef : " << TargetDC_2->GetActiveArea(3,4)<< endl;;


    END;
}

void TargetDCNumexo::SetParameters(NUMEXOParameters* Par, Map* Map) {
    START;

    NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
    for (UShort_t i = 0; i < DetList->size(); i++)
        DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);
    END;
}

// void TargetDCNumexo::ReadPosition(void)
// {
//   START;
//   MIFile *IF;
//   char Line[255];
//   stringstream *InOut;

//   InOut = new stringstream();
//   *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
//   *InOut>>Line;
//   InOut = CleanDelete(InOut);
//   if((IF = CheckCalibration(Line)))
//     {
//       // Position
//       for(UShort_t i=0; i< DetList->size();i++)
//       {
//         try
//         {
//             DetList->at(i)->ReadDefaultPosition(IF);
//         }
//       catch(...)
//         {
//           IF = CleanDelete(IF);
//           throw;
//         }
//       }
//     }

//   //if(VerboseLevel >= V_INFO)
//     {
//       cout << "<" << DetectorName << "><" << DetectorNameExt << "> Reference positions (X,Y,Z) wrt. to target " << endl;
//       cout << "<" << DetectorName << "><" << DetectorNameExt << ">   X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ() << " mm" << endl;
//       cout << "\n" << endl;

//     }
//     L->File << "<" << DetectorName << "><" << DetectorNameExt << "> Reference positions (X,Y,Z) wrt. to target " << endl;
//     L->File << "<" << DetectorName << "><" << DetectorNameExt << ">  X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ() << " mm" << endl;

//   END;
// }

Bool_t TargetDCNumexo::Treat(void)
{
    START;

    ULong64_t RefTS = 0;
    IncrementCounter(0);
    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
            DetList->at(i)->Treat();
    }

    if(fCalData)
    {
        Double_t ZRef = 738.7; //mm
        Double_t Size = 180./2.; // mm
        Double_t ZRefY1 = DetList->at(0)->GetActiveArea(2,4)+3+6.6+16./2.; //mm
        Double_t ZRefX1 = DetList->at(0)->GetActiveArea(0,4)+3+30.8+16./2.; //mm
//        Double_t ZRefY2 = DetList->at(1)->GetActiveArea(2,4)+3.+179.6+6.6+16./2.; //mm
//        Double_t ZRefX2 = DetList->at(1)->GetActiveArea(0,4)+3+30.8+16./2.; //mm
        Double_t DriftVelocity = 5.387; //cm/us
        Double_t X_Offset[2][2];
        X_Offset[0][0] = DetList->at(0)->GetActiveArea(0,0); //mm
        X_Offset[0][1] = DetList->at(0)->GetActiveArea(1,0) ; //mm
//        X_Offset[1][0] = DetList->at(1)->GetActiveArea(0,0) ; //mm
//        X_Offset[1][1] = DetList->at(1)->GetActiveArea(1,0) ; //mm
        Double_t Y_Offset[2][2];
        Y_Offset[0][0] = DetList->at(0)->GetActiveArea(2,1) ; //mm
        Y_Offset[0][1] = DetList->at(0)->GetActiveArea(3,1) ; //mm
//        Y_Offset[1][0] = DetList->at(1)->GetActiveArea(2,1) ; //mm
//        Y_Offset[1][1] = DetList->at(1)->GetActiveArea(3,1) ; //mm
        Double_t X[2];
        Double_t Y[2];
        Double_t PresentX[2];
        Double_t PresentY[2];

        RefTS = LTS->GetTS();

//        Test Generator
//        RefTS = 10;
//        DetList->at(0)->SetRawData(0,10,((double) rand() / (RAND_MAX))*200);
//        DetList->at(0)->SetRawData(2,10,((double) rand() / (RAND_MAX))*200);
//        DetList->at(1)->SetRawData(1,10,((double) rand() / (RAND_MAX))*200);
//        DetList->at(1)->SetRawData(3,10,((double) rand() / (RAND_MAX))*200);

  //      cout  << DetList->at(0)->GetRawTS(0) << endl;

        if(RefTS >0)
        {

        for(UShort_t i=0; i< DetList->size(); i++)
        {
           X[i] = -1500.;
           Y[i] = -1500.;
            PresentX[i] = false;
            PresentY[i] = false;

            if(DetList->at(i)->GetRawTS(0)>0)
            {
                X[i] = ((((Float_t)(DetList->at(i)->GetRawTS(0)-RefTS))*10.)/1000.* DriftVelocity)*10. + X_Offset[i][0];
                PresentX[i] = true;
            }
            else if(DetList->at(i)->GetRawTS(1)>0)
            {
                X[i] = ((((Float_t)(DetList->at(i)->GetRawTS(1)-RefTS))*10.)/1000.* DriftVelocity)*10. + X_Offset[i][1];
		X[i] *=-1.;
                PresentX[i] = true;
            }
            else
            {
                X[i] = -1500.;
                PresentX[i] = false;
            }

            SetCalData(6+2*i,X[i]);

            if(DetList->at(i)->GetRawTS(2)>0)
            {
                Y[i] = ((((Float_t)(DetList->at(i)->GetRawTS(2)-RefTS))*10.)/1000.* DriftVelocity)*10. + Y_Offset[i][0];
                PresentY[i] = true;
            }
            else if(DetList->at(i)->GetRawTS(3)>0)
            {
                Y[i] = ((((Float_t)(DetList->at(i)->GetRawTS(3)-RefTS))*10.)/1000.* DriftVelocity)*10. + Y_Offset[i][1];
 		Y[i] *=-1.;
                PresentY[i] = true;
            }
            else
            {
                Y[i] = -1500.;
                PresentY[i] = false;
            }


            SetCalData(7+2*i,Y[i]);


            if(PresentX[i] > -1500 && PresentY[i] > -1500 )
                IncrementCounter(i);
        }



        if(PresentX[0] && PresentX[1])
        {
//            Double_t  Tf = atan((X[0]-X[1])/(ZRefX1-ZRefX2))*1000.;
//            Double_t  Xf = X[0]+tan(Tf/1000.)*ZRefX1;
//            SetCalData(0,Xf);
//            SetCalData(1,Tf);
        }
        else
        {
            SetCalData(0,-1500.);
            SetCalData(1,-1500.);
        }

        if(PresentY[0] && PresentY[1])
        {
//            Double_t  Pf = atan((Y[0]-Y[1])/(ZRefY1-ZRefY2))*1000.;
//            Double_t  Yf = X[0]+tan(Pf/1000.)*ZRefY1;
//            SetCalData(2,Yf);
//            SetCalData(3,Pf);
        }
        else
        {
            SetCalData(2,-1500.);
            SetCalData(3,-1500.);
        }
#ifdef WITH_ROOT
        // Has to be changed to be incorporated in Narwal actor
        //MW target projected coordinates (Theta in 0xz and Phi in 0yz)
        Double_t Theta = GetCal(1);
        Double_t Phi = GetCal(3);
        //Zgoubi coordinates (Theta in 0xz and Phi between v particle and 0xz)
        Double_t ThetaZ = Theta;
        Double_t PhiZ = atan(tan(Phi/1000.)*cos(Theta/1000.))*1000.;
        Double_t ThetaL;
        Double_t PhiL;
        Double_t VamosAngle = FP->GetVamosAngle();
        TVector3 *myVec;
        // TVector3 *myVecy;
        myVec = new TVector3(sin(ThetaZ/1000.)*cos(PhiZ/1000.),sin(PhiZ/1000.),cos(ThetaZ/1000.)*cos(PhiZ/1000.));

        ////
        //                          ANGLE VAMOS
        ///////


        myVec->RotateY(VamosAngle*TMath::Pi()/180.); //VAMOS angle

        ////
        //                          ANGLE VAMOS
        ///////

        ThetaL = myVec->Theta();
        PhiL = myVec->Phi();

        //cout << "Theta " << Theta << " Phi " << Phi << " ThetaZ " << ThetaZ << " PhiZ " << PhiZ << " ThetaL " << ThetaL << " PhiL " << PhiL << endl;

        delete myVec;

        SetCalData(4,ThetaL);
        SetCalData(5,PhiL);
#endif

        }
        else
        {
            for(UShort_t i=0;i<NumberOfDetectors;i++)
                SetCalData(i,-1500.);

        }

    }


    return(isPresent);


    END;
}



#ifdef WITH_ROOT
void TargetDCNumexo::CreateHistogramsCal1D(TDirectory *Dir)
{
    START;
    string Name;
    Dir->cd("");
    if(SubFolderHistCal1D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal1D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    // XS
    AddHistoCal(CalNameI[0],CalNameI[0],"XS (mm)",1000,-50,50);
    // YS
    AddHistoCal(CalNameI[2],CalNameI[2],"YS (mm)",1000,-50,50);
    // XWA
    AddHistoCal(CalNameI[1],CalNameI[1],"Theta (mrad)",1000,-300,300);
    // YWA
    AddHistoCal(CalNameI[3],CalNameI[3],"Phi (mrad)",1000,-400,400);
    // X1
    AddHistoCal(CalNameI[6],CalNameI[6],"X1 (mm)",1000,-1000,1000);
    // Y1
    AddHistoCal(CalNameI[7],CalNameI[7],"Y1",1000,-300,300);
    // X2
    AddHistoCal(CalNameI[8],CalNameI[8],"X2",1000,-400,400);
    // X2
    AddHistoCal(CalNameI[9],CalNameI[9],"Y2",1000,-400,400);

    END;
}

void TargetDCNumexo::CreateHistogramsCal2D(TDirectory *Dir)
{
    START;
    string Name;

    BaseDetector::CreateHistogramsCal2D(Dir);

    Dir->cd("");

    if(SubFolderHistCal2D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal2D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    AddHistoCal(CalNameI[0],CalNameI[2],"XT_YT_TP","X_vs_Y (mm)",600,-20,20,600,-20,20);
    AddHistoCal(CalNameI[1],CalNameI[3],"ThT_PhT","ThT_vs_PhiT (mrad)",600,-200,200,600,-200,200);
    AddHistoCal(CalNameI[6],CalNameI[7],"DCT1_XY","DCT1_XY (mm)",600,-1000,1000,600,-1000,1000);
    AddHistoCal(CalNameI[8],CalNameI[9],"DCT2_XY","DCT2_XY (mm)",600,-1000,1000,600,-1000,1000);

    END;
}


void TargetDCNumexo::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;

    SetMainHistogramFolder("TargetDCNumexo");

#ifdef WITH_ROOT
    // Setup Histogram Hierarchy
    SetHistogramsCal1DFolder("");
    SetHistogramsCal2DFolder("");
#endif

    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetHistogramsRawSummary(false);
		DetList->at(i)->SetOutAttachRawI(false);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCalSummary(false);

            }
        }
    }
    else if(fMode == MODE_D2R)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetOutAttachRawV(false);
                DetList->at(i)->SetOutAttachRawI(true);
                DetList->at(i)->SetOutAttachTS(true);
                DetList->at(i)->SetHistogramsRawSummary(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if(fMode == MODE_D2A)
    {
         if(fRawData)
         {
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
         }

        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetOutAttachRawI(true);
                DetList->at(i)->SetOutAttachTS(true);
                DetList->at(i)->SetOutAttachRawV(false);
                DetList->at(i)->SetHistogramsRawSummary(false);

            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetOutAttachCalI(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }

        OutAttach(OutTTree);
    }
    else if(fMode == MODE_R2A)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            DetList->at(i)->InAttach(InTTree);
        }

        if(fRawData)
        {
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(false);
                DetList->at(i)->SetHistogramsCalSummary(true);
                DetList->at(i)->SetOutAttachCalV(true);
                DetList->at(i)->OutAttach(OutTTree);
            }

        }
        OutAttach(OutTTree);
    }
    else if(fMode == MODE_RECAL)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            DetList->at(i)->InAttachCal(InTTree);

        }
        SetInAttachCalI(true);
        InAttachCal(InTTree);

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasCalData())
            {
            }
            DetList->at(i)->OutAttach(OutTTree);
        }

    }
    else if(fMode == MODE_CALC)
    {
        SetNoOutput();
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    if(VerboseLevel >= V_INFO)
        PrintOptions(cout)  ;


    END;
}

#endif
