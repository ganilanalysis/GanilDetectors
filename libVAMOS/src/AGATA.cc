/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "AGATA.hh"

AGATA::AGATA(const Char_t *Name,
                         UShort_t NDetectors, 
                         Bool_t RawData, 
                         Bool_t CalData, 
                         Bool_t DefaultCalibration, 
                         const Char_t *NameExt
                         )
: BaseDetector(Name, 0,false, CalData, DefaultCalibration,false,NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors; // Number of Cores of AGATA
  fRawDataSubDetectors  = RawData;
  AllocateComponents(); 
  if(CalData)
    {
      // sprintf(CalNameI[0],"AGAVA_STATUS");
      // sprintf(CalNameI[1],"AGAVA_EVTNUM");
      // sprintf(CalNameI[2],"AGAVA_TRIG");
      // sprintf(CalNameI[3],"AGAVA_LTRIG");
      // sprintf(CalNameI[4],"AGAVA_VAL");
      // sprintf(CalNameI[5],"AGAVA_REJ");
      // sprintf(CalNameI[6],"AGAVA_TOUT");
    }
  
  VTS = NULL;
  RTS = NULL;
  LTS = NULL;


  END;
}
AGATA::~AGATA(void)
{
  START;
  END;

}


ULong64_t AGATA::Concat2(UShort_t BitUp, UShort_t BitDwn)
{
  START;
  ULong64_t Word;

  Word = BitUp;
  Word = BitUp <<16;
  Word +=  (ULong64_t) BitDwn; 
  return Word;
  END;
}

ULong64_t AGATA::Concat3(UShort_t BitUp, UShort_t BitMid, UShort_t BitDwn)
{
  START;
  ULong64_t temp;
  ULong64_t clock;
  
  temp = (ULong64_t) BitMid;
  temp = (temp << 16);
  temp += (ULong64_t) BitDwn;
  
  clock = (ULong64_t) BitUp;
  clock = (clock << 32);
  clock += temp;

  //  clock *; // in ns
  return clock;
  END;
}


void AGATA::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = false;
  Bool_t DefCal = true;

  VTS = new TimeStamp("VTS",3,RawData,CalData,DefCal,DetectorNameExt);
  if(RawData)
    {
      VTS->SetLowWord("AGAVA_VTAGL");
      VTS->SetMidWord("AGAVA_VTAGL_UP");
      VTS->SetHighWord("AGAVA_VTAGH");
    }
  AddComponent((BaseDetector*) VTS);

  RTS = new TimeStamp("RTS",3,RawData,CalData,DefCal,DetectorNameExt);
  if(RawData)
    {
      RTS->SetLowWord("AGAVA_RTAGL");
      RTS->SetMidWord("AGAVA_RTAGL_UP");
      RTS->SetHighWord("AGAVA_RTAGH");
    } 
  AddComponent((BaseDetector*) RTS);

  LTS = new TimeStamp("LTS",3,RawData,CalData,DefCal,DetectorNameExt);
  if(RawData)
    {
      LTS->SetLowWord("AGAVA_LTAGL");
      LTS->SetMidWord("AGAVA_LTAGL_UP");
      LTS->SetHighWord("AGAVA_LTAGH");
    }
  AddComponent((BaseDetector*) LTS);
  
  END;
}

void AGATA::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  for(UShort_t i=0; i< DetList->size(); i++)
    {
      DetList->at(i)->SetParameters(Par,Map);
    }
  END;
}

Bool_t AGATA::Treat(void)
{ 
  START;
  Ctr->at(0)++;
#ifdef DEBUG 
  cout << "AGATA TREAT <<<------------------------------------>>>" << endl;
  PrintRaw();
#endif
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        DetList->at(i)->Treat();
    
      if(fCalData && fRawData)
        {
          ULong64_t TRIG;
          TRIG = Concat2(DetList->at(0)->GetRaw(4),DetList->at(0)->GetRaw(5));
          ULong64_t VAL;
          VAL = Concat2(DetList->at(0)->GetRaw(8),DetList->at(0)->GetRaw(9));
#ifdef DEBUG

          ULong64_t STAT;
          ULong64_t LTRIG;
          ULong64_t REJ;
          ULong64_t TOUT;
          ULong64_t EVT;

          STAT = Concat2(DetList->at(0)->GetRaw(0),DetList->at(0)->GetRaw(1));
          EVT = Concat2(DetList->at(0)->GetRaw(2),DetList->at(0)->GetRaw(3));
          LTRIG = Concat2(DetList->at(0)->GetRaw(6),DetList->at(0)->GetRaw(7));
          REJ = Concat2(DetList->at(0)->GetRaw(10),DetList->at(0)->GetRaw(11));
          TOUT = Concat2(DetList->at(0)->GetRaw(12),DetList->at(0)->GetRaw(13));
     
			 cout << "STAT << " << STAT<< endl;
          cout << "EVT << " << EVT<< endl;
          cout << "TRIG << " << TRIG<< endl;
          cout << "LTRIG << " << LTRIG<< endl;
          cout << "VAL << " << VAL<< endl;
          cout << "REJ << " << REJ<< endl;
          cout << "TOUT << " << TOUT << endl;
 #endif

          // SetCalData(0,(Float_t) STAT);
          // SetCalData(1,(Float_t) EVT);
          // SetCalData(2,(Float_t) TRIG);
          // SetCalData(3,(Float_t) LTRIG);
          // SetCalData(4,(Float_t) VAL);
          // SetCalData(5,(Float_t) REJ);
          // SetCalData(6,(Float_t) TOUT);

          if(DetList->at(0)->GetRaw(1)>0)
            DetList->at(0)->IncrementCounter(2);
          if(TRIG>0 && VAL>0)
            DetList->at(0)->IncrementCounter(3);
       
       
          isPresent = true;

        }
    }
  return(isPresent); 
  
  END;
}



#ifdef WITH_ROOT
  void AGATA::SetOpt(TTree *OutTTree, TTree *InTTree)
  {
    START;
    SetHistogramsCal1DFolder("");
    SetHistogramsCal2DFolder("");
    SetHistogramsRaw1DFolder("");
    SetHistogramsRaw2DFolder("");
    for(UShort_t i = 0;i<DetList->size();i++)
      {
        // if(i==0) // AGAVA blk
        //   DetList->at(i)->SetMainHistogramFolder("");
        // if(i>0) // TS
          DetList->at(i)->SetMainHistogramFolder("TS");
      }
 
    if(fMode == MODE_WATCHER)
      {
        if(fRawData)
          {
            SetHistogramsRaw(true);
          }
        if(fCalData)
          {
            SetHistogramsCal(true);
          }
        for(UShort_t i = 0;i<DetList->size();i++)
          {
            if(DetList->at(i)->HasRawData())
              {
                // if(i != 0)
                //   {
                    DetList->at(i)->SetHistogramsRaw(true);
                    DetList->at(i)->SetHistogramsRawSummary(true);
                  // }
              }
            if(DetList->at(i)->HasCalData())
              {
                // if(i == 1)
                //   {
                //     DetList->at(i)->SetHistogramsCal(true);
                //     DetList->at(i)->SetHistogramsCalSummary(true);
                //   }
                // else if(i>0)
                //   {
		DetList->at(i)->SetHistogramsCal(true);
		// }
              }
          }
      }
    else if(fMode == MODE_D2R)
      {
        for(UShort_t i = 0;i<DetList->size();i++)
          {
            if(DetList->at(i)->HasRawData())
              {
                DetList->at(i)->SetHistogramsRaw(true);
                // if(i == 0) 
                //   {
                //     DetList->at(i)->SetHistogramsRawSummary(false);
                //     DetList->at(i)->SetOutAttachRawV(false);
                //     DetList->at(i)->SetOutAttachRawI(true);
                //   }
                // else
                //   {			
		    DetList->at(i)->SetOutAttachRawV(true);
		  // }
              }
	    if(DetList->at(i)->HasCalData())
              {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
          //       if(i == 0) // Generic Block
          //         {
		    // DetList->at(i)->SetHistogramsCalSummary(false);
		    // DetList->at(i)->SetOutAttachCalV(false);			 
          //           DetList->at(i)->SetOutAttachCalI(true);
          //         }
		// else if(i == 1) // Gamma Cores
		//   {						  
                //     DetList->at(i)->SetHistogramsCalSummary(true);
                //     DetList->at(i)->SetOutAttachCalV(true);
		//   }
                // else // Individual TS
                //   {
                    DetList->at(i)->SetHistogramsCalSummary(false);
                    DetList->at(i)->SetOutAttachCalI(true);
                  // }
              }           

	    DetList->at(i)->OutAttach(OutTTree);
          }
          
        OutAttach(OutTTree);
          
      }
    else if(fMode == MODE_D2A)
      {
      
        if(fRawData)
          {
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
          }
        if(fCalData)
          {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
          }
		
        for(UShort_t i = 0;i<DetList->size();i++)
          {			
            if(DetList->at(i)->HasRawData())
	      {
		// 				  DetList->at(i)->SetHistogramsRaw1D(true);
		// 				  DetList->at(i)->SetHistogramsRaw2D(true);
		
		// if(i == 0) 
		//   {
		//     DetList->at(i)->SetHistogramsRawSummary(false);
		//     DetList->at(i)->SetOutAttachRawV(false);
		//     DetList->at(i)->SetOutAttachRawI(false);			 					  
		//   }
		// else if(i == 1) // Gamma Cores
		//   {
		//     DetList->at(i)->SetHistogramsRawSummary(false);
		//     DetList->at(i)->SetOutAttachRawV(false);
		//     DetList->at(i)->SetOutAttachRawI(false);
		// }
		// else
		//   {
		    DetList->at(i)->SetOutAttachRawI(true);
		  // }
	      }
            
	    if(DetList->at(i)->HasCalData())
              {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
          //       if(i == 0) // Generic Block
      //             {
		//     DetList->at(i)->SetHistogramsCalSummary(false);
		//     DetList->at(i)->SetOutAttachCalV(false);			 
		//     DetList->at(i)->SetOutAttachCalI(true);
      //             }
		// // else if(i == 1) // Gamma Cores
		// //   {						  
      //           //     DetList->at(i)->SetHistogramsCalSummary(true);
      //           //     DetList->at(i)->SetOutAttachCalV(true);
		// //   }
      //           else // Individual TS
      //             {
                    DetList->at(i)->SetHistogramsCalSummary(false);
                    DetList->at(i)->SetOutAttachCalI(true);
                  // }
              }
          
            DetList->at(i)->OutAttach(OutTTree);
          }
      
        OutAttach(OutTTree);
      }
    else if(fMode == MODE_R2A)
      {

        for(UShort_t i = 0;i<DetList->size();i++)
          {
            DetList->at(i)->SetInAttachRawI(true);
            DetList->at(i)->InAttach(InTTree);
          }
		
        SetOutAttachCalI(true);

        for(UShort_t i = 0;i<DetList->size();i++)
          {
            if(DetList->at(i)->HasCalData())
              {
                DetList->at(i)->SetHistogramsCal(false);
              
                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalI(true);

              }
            DetList->at(i)->OutAttach(OutTTree);
          }
        OutAttach(OutTTree);
		
      }
    else if (fMode == MODE_RECAL)
      {
	SetInAttachCalI(true);
        for(UShort_t i = 0;i<DetList->size();i++)
          {
	    if(i == 0)// Generic Block
	      {
	      }
	    else // TS
	      {
		DetList->at(i)->SetInAttachCalI(true);
	      }
	    if(DetList->at(i)->HasCalData())
	      DetList->at(i)->InAttachCal(InTTree);
	  }
	InAttachCal(InTTree);

	if(fCalData)
          {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
          }
		
        for(UShort_t i = 0;i<DetList->size();i++)
          {			
             if(DetList->at(i)->HasCalData())
              {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                if(i == 0) // Generic Block
                  {
		    // DetList->at(i)->SetHistogramsCalSummary(false);
		    // DetList->at(i)->SetOutAttachCalV(false);			 
		    // DetList->at(i)->SetOutAttachCalI(true);
                  }
		// else if(i == 1) // Gamma Cores
		//   {						  
                //     DetList->at(i)->SetHistogramsCalSummary(true);
                //     DetList->at(i)->SetOutAttachCalV(true);
		//   }
                else // Individual TS
                  {
                    DetList->at(i)->SetHistogramsCalSummary(false);
                    DetList->at(i)->SetOutAttachCalI(true);
                  }
              }
          
            DetList->at(i)->OutAttach(OutTTree);
          }
      
        OutAttach(OutTTree);
      }
     else if (fMode == MODE_CALC)
      {
        SetNoOutput();
      }
    else 
      {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
      }
    END;
  }
#endif
