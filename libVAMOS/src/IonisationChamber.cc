/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "IonisationChamber.hh"

#define DEBUG

IonisationChamber::IonisationChamber(const Char_t *Name,
                       UShort_t NDetectors,
                       UShort_t NRow,
                       Bool_t RawData,
                       Bool_t CalData,
                       Bool_t DefaultCalibration,
                       const Char_t *NameExt
                       )
  : BaseDetector(Name, NRow+10+2, false, CalData, DefaultCalibration, false, NameExt,true)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;

  
  FP = NULL;
  DetManager *DM = DetManager::getInstance();
  FP = (FocalPosition*) DM->GetDetector("FP");

  // Set How many IC rows
  N_Row = NRow;
  N_Col = 1;
  AllocateComponents();
  char line[100];
  
  for(UShort_t i=0; i<N_Row;i++)
    {
      sprintf(line,"IC%d",i);
      AddCounter(line); // 2
    }
  AddCounter("Mult Pad > 1"); // N_Row+2

  if(CalData)
    {

      sprintf(CalNameI[0],"IC_dE1");
      sprintf(CalNameI[1],"IC_dE2");
      sprintf(CalNameI[2],"IC_E");
      // for(UShort_t i=0; i<N_Row;i++)
      //   {
      //     sprintf(line,"ICdE%d",i);
      //     sprintf(CalNameI[i],"%s",line);
      //   }
      // for(UShort_t i=0; i<N_Col;i++)
      //   {
      //     sprintf(line,"E_%d",i);
      //     sprintf(CalNameI[NRow+2*i],"%s",line);
      //     sprintf(line,"dE_%d",i);
      //     sprintf(CalNameI[NRow+2*i+1],"%s",line);
      //   }
      
      // if(NumberOfDetectors >= N_Row+2)
      // if(NumberOfDetectors >= N_Row+2*N_Col+2)
      //   {
      //     sprintf(CalNameI[N_Row+2*N_Col],"ICdE");		
      //     sprintf(CalNameI[N_Row+2*N_Col+1],"ICdEM1");		
      //   }
      // else
      //   {
      //     Char_t Message[500];
      //     sprintf(Message,"In <%s><%s> Trying to set calibrated data beyond the number of declared calibrated data (%d) !", GetName(), GetName(), NumberOfDetectors );
      //     MErr * Er= new MErr(WhoamI,0,0, Message);
      //     throw Er;
      //   }
    }
  
  AllocateLocalArrays();
  
#ifdef WITH_ROOT
  // Setup Histogram Hierarchy
  SetHistogramsCal1DFolder("IC_C");
  SetHistogramsCal2DFolder("IC_C");
  SetHistogramsRaw1DFolder("IC_R");
  SetHistogramsRaw2DFolder("IC_R");
#endif


  END;
}
IonisationChamber::~IonisationChamber(void)
{
  START;
  DeAllocateLocalArrays();

  END;
}

void IonisationChamber::AllocateLocalArrays(void)
{
  START;
  
  // Row Energies array
  E_Row = AllocateDynamicArray<Float_t>(N_Row);
  E_Col = AllocateDynamicArray<Float_t>(N_Col);
  dE = AllocateDynamicArray<Float_t>(N_Col);
  M_Row = AllocateDynamicArray<UShort_t>(N_Row);

  END;
}

void IonisationChamber::DeAllocateLocalArrays(void)
{
  START;
  
  E_Row = FreeDynamicArray<Float_t>(E_Row);
  E_Col = FreeDynamicArray<Float_t>(E_Col);
  dE = FreeDynamicArray<Float_t>(dE);
  M_Row = FreeDynamicArray<UShort_t>(M_Row);

  END;
}

void IonisationChamber::ClearCal(void)
{
  START;
  BaseDetector::ClearCal();
  if(isComposite){
    for(UShort_t i=0; i< DetList->size(); i++)
      DetList->at(i)->ClearCal();
  }

  for(UShort_t i=0; i<N_Row; i++)
    {
      E_Row[i] = 0.;
      M_Row[i] = 0;
    }
  for(UShort_t i=0; i<N_Col; i++)
    {
      E_Col[i] = 0.;
      dE[i] = 0;
    }
  ETotal = 0;
  ETotalM1 = 0;
  END;
}



void IonisationChamber::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Char_t Name[100];
  if(strcmp(DetectorName, "IcH") == 0)
    sprintf(Name,"ICH");
  else
    sprintf(Name,"IC");

    
  BaseDetector *D = new BaseDetector(Name,NumberOfSubDetectors,RawData,CalData,DefCal,true,"E",true);
  if(RawData)
    D->SetParameterName("%s_%02d");
  if(fCalData)
    {
      D->ReadCalGates();
    }
#ifdef WITH_ROOT
  D->SetMainHistogramFolder(MainHistogramFolder);
#endif
  AddComponent(D);
  END;
}


void IonisationChamber::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  for(UShort_t i=0; i< DetList->size(); i++)
    DetList->at(i)->SetParameters(Par,Map);
   END;
}


Bool_t IonisationChamber::Treat(void)
{ 
  START;
  UShort_t RowNr;
  UShort_t ColNr;
  Float_t dE1=0;
  Float_t dE2=0;
  Float_t E=0;


  Ctr->at(0)++;
  
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
	{
	  DetList->at(i)->Treat();
	}
    }
  if(fCalData)
    {
      for(UShort_t i=0;i<DetList->at(0)->GetM();i++)
	{
	  RowNr = (Int_t) (DetList->at(0)->GetNrAt(i));
	  E_Row[RowNr] += DetList->at(0)->GetCalAt(i);
	  if(RowNr<2)
	    dE1 +=  E_Row[RowNr];
	  if(RowNr<3)
	    dE2 +=  E_Row[RowNr];
	       
	  E +=  E_Row[RowNr];
	}

      SetCalData(0,dE1);
      SetCalData(1,dE2);
      SetCalData(2,E);
      // for(UShort_t i=0;i<DetList->at(0)->GetM();i++)
      //   {
      // 	 RowNr = (Int_t) (DetList->at(0)->GetNrAt(i)/6);
      // 	 ColNr = (Int_t) (DetList->at(0)->GetNrAt(i) % 6);
      // 	 if(DetList->at(0)->GetCalAt(i) > 0. )
      // 	   {
			     
      // 	     // Will do position later
      // 	     // if(FP)
      // 	     //   if (FP->IsPresent()) 
      // 	     // 	 {
      // 	     // 	   // hasValidTrack = DetList->at(0)->CheckTrack(FP->GetXf(),FP->GetTf(),FP->GetYf(),FP->GetPf(),FP->GetRefZ(),DetList->at(0)->GetNrAt(i));
      // 	     // 	 }
			     
      // 	     E_Row[RowNr] += DetList->at(0)->GetCalAt(i);
      // 	     E_Col[ColNr] += DetList->at(0)->GetCalAt(i);
			     
      // 	     if(RowNr == 0)
      // 	       dE[ColNr] = DetList->at(0)->GetCalAt(i);

      // 	     M_Row[RowNr]++;
      // 	     ETotal += DetList->at(0)->GetCalAt(i);
      // 	   }
			 
      // 	 if(M_Row[RowNr] == 1)
      // 	   ETotalM1 = ETotal;      
      //   }
		
      // for(UShort_t i=0;i<N_Row;i++)
      //   if(M_Row[i] == 1)
      //     {        
      //       Ctr->at(i+2)++;
      //     }
      //   else
      //     {            
      //       Ctr->at(2+N_Row)++;
      //       ETotalM1 = 0.;
      //     }
  
      // for(Int_t i=0;i<N_Row;i++)
      //   if(E_Row[i] >0)
      //     {
      //       SetCalData(i,E_Row[i]);
      //     }
      //   else
      //     ETotalM1 = 0.;

      // for(Int_t i=0;i<N_Col;i++)
      //   {
      //     if(E_Col[i] >0)
      //       SetCalData(N_Row+2*i,E_Col[i]);
      //     if(dE[i] >0)
      //       SetCalData(N_Row+2*i+1,dE[i]);
      //   }
		
      // if(ETotal > 0.) 
      //   {
      //     SetCalData(N_Row+2*N_Col,ETotal);
      //     SetCalData(N_Row+2*N_Col+1,ETotalM1);
      //     isPresent = true;
      //   }
    }
  

  return(isPresent);
  END;
}

#ifdef WITH_ROOT
void IonisationChamber::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;

  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
	
  // IC A
  AddHistoCal(CalNameI[0],CalNameI[0],"dE1 (MeV)",500,0.,1000.);
  // IC B
  AddHistoCal(CalNameI[1],CalNameI[1],"dE2 (MeV)",500,0.,1000.);
  // IC C
  AddHistoCal(CalNameI[2],CalNameI[2],"E (MeV)",500,0.,1500.);
  
  END;
}


void IonisationChamber::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
    {
      Name.clear();
      Name = SubFolderHistCal2D ;
      if(!(gDirectory->GetDirectory(Name.c_str())))
	gDirectory->mkdir(Name.c_str());		 
      gDirectory->cd(Name.c_str());
    }


  AddHistoCal(GetCalName(2),GetCalName(0),Form("dE1_E"),Form("dE1_E"),1000,0,1500,1000,0,1000);
  AddHistoCal(GetCalName(2),GetCalName(1),Form("dE2_E"),Form("dE2_E"),1000,0,1500,1000,0,1000);

  AddHistoCal(GetCalName(2),GetCalName(0),Form("dE1_E_raz"),Form("dE1_E_raz"),1000,0,1500,1000,0,1000);
  AddHistoCal(GetCalName(2),GetCalName(1),Form("dE2_E_raz"),Form("dE2_E_raz"),1000,0,1500,1000,0,1000);


  // for(Int_t i=0;i<N_Col;i++)
  //   {
  //     AddHistoCal(GetCalName(N_Row+2*i),GetCalName(N_Row+2*i+1),Form("dE_E_col%d",i),Form("dE_E Col %d",i),1000,0,2000,1000,0,2000);
  //   }

  // for(Int_t i=0;i<N_Col;i++)
  //   {
  //     AddHistoCal(GetCalName(N_Row+2*i),GetCalName(N_Row+2*i+1),Form("dE_E_col-raz%d",i),Form("dE_E Col RAZ %d",i),1000,0,2000,1000,0,2000);
  //   }
  END;
}


void IonisationChamber::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  for(UShort_t i = 0;i<DetList->size();i++)
	 {
		DetList->at(i)->SetMainHistogramFolder("");
		DetList->at(i)->SetHistogramsRaw1DFolder("IC_R");
		DetList->at(i)->SetHistogramsRaw2DFolder("IC_R");
	 	DetList->at(i)->SetHistogramsCal1DFolder("IC_C");
		DetList->at(i)->SetHistogramsCal2DFolder("IC_C");
	 }
 
  if(fMode == MODE_WATCHER)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(true);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw(true);
	      DetList->at(i)->SetHistogramsRawSummary(true);
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal1D(true);
	      DetList->at(i)->SetHistogramsCal2D(true);
	      DetList->at(i)->SetHistogramsCalSummary(true);
	    }
		  }
	 }
  else if(fMode == MODE_D2R)
	 {
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw2D(true);
				  DetList->at(i)->SetHistogramsRawSummary(false);
				  DetList->at(i)->SetOutAttachRawV(true);
              DetList->at(i)->SetOutAttachRawI(false);
            }
				  DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
	 }
  else if(fMode == MODE_D2A)
	 {
		if(fRawData)
		  {
			 SetHistogramsRaw(false);
			 SetOutAttachRawI(false);
			 SetOutAttachRawV(false);
		  }
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
			 SetOutAttachCalV(false);
		  }
		
		for(UShort_t i = 0;i<DetList->size();i++)
		  {			
			 if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw1D(true);
				  DetList->at(i)->SetHistogramsRaw2D(true);
				  DetList->at(i)->SetHistogramsRawSummary(true);
				  DetList->at(i)->SetOutAttachRawI(false);
				  DetList->at(i)->SetOutAttachRawV(false);			 
				}
			 if(DetList->at(i)->HasCalData())
				{
				  DetList->at(i)->SetHistogramsCal1D(true);
				  DetList->at(i)->SetHistogramsCal2D(true);
				  DetList->at(i)->SetHistogramsCalSummary(false);
				  DetList->at(i)->SetOutAttachCalI(false);
				  DetList->at(i)->SetOutAttachCalV(false);			 
				  DetList->at(i)->SetOutAttachCalF(true);			 
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 DetList->at(i)->SetInAttachRawV(false);
			 DetList->at(i)->InAttach(InTTree);
		  }
		
		SetOutAttachCalI(true);

		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 if(DetList->at(i)->HasCalData())
				{
				  DetList->at(i)->SetHistogramsCal1D(true);
				  DetList->at(i)->SetHistogramsCal2D(true);
				  DetList->at(i)->SetHistogramsCalSummary(false);
				  DetList->at(i)->SetOutAttachCalF(true);
				  DetList->at(i)->SetOutAttachCalV(false);
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
    } 
  else if(fMode == MODE_RECAL)
    {
      
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  SetInAttachCalI(true);
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetInAttachCalF(true);
	    }
	  DetList->at(i)->InAttachCal(InTTree);
	}
      InAttachCal(InTTree);
      SetOutAttachCalI(true);
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal1D(true);
	      DetList->at(i)->SetHistogramsCal2D(true);
	      DetList->at(i)->SetHistogramsCalSummary(false);
	      DetList->at(i)->SetOutAttachCalF(true);
	      DetList->at(i)->SetOutAttachCalV(false);
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}
      OutAttach(OutTTree);
      
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}





#endif
