   /****************************************************************************
    *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
    *    lemasson@ganil.fr
    *
    *    Contributor(s) :
    *    Antoine Lemasson, lemasson@ganil.fr
    *    Maurycy Rejmund, rejmund@ganil.fr
    *
    *    This software is  a computer program whose purpose  is to provide data
    *    manipulation tools to Analyse experimental data collected at the VAMOS
    *    spectrometer.
    *
    *    This software is governed by the CeCILL-B license under French law and
    *    abiding by the  rules of distribution of free  software.  You can use,
    *    modify  and/ or  redistribute  the  software under  the  terms of  the
    *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    *    URL \"http://www.cecill.info\".
    *
    *    As a counterpart to the access  to the source code and rights to copy,
    *    modify  and redistribute granted  by the  license, users  are provided
    *    only with a limited warranty  and the software's author, the holder of
    *    the economic  rights, and the  successive licensors have  only limited
    *    liability.
    *
    *    In this respect, the user's attention is drawn to the risks associated
    *    with loading,  using, modifying  and/or developing or  reproducing the
    *    software by the user in light of its specific status of free software,
    *    that  may mean that  it is  complicated to  manipulate, and  that also
    *    therefore  means that it  is reserved  for developers  and experienced
    *    professionals having in-depth  computer knowledge. Users are therefore
    *    encouraged  to load  and test  the software's  suitability  as regards
    *    their  requirements  in  conditions  enabling the  security  of  their
    *    systems  and/or data to  be ensured  and, more  generally, to  use and
    *    operate it in the same conditions as regards security.
    *
    *    The fact that  you are presently reading this means  that you have had
    *    knowledge of the CeCILL-B license and that you accept its terms.
    ***************************************************************************/
   #include "DriftNUMEXO.hh"
   //#define DCE10 1
   DriftChamberNUMEXO::DriftChamberNUMEXO(const Char_t *Name,
       UShort_t NDetectors,
       Bool_t RawData,
       Bool_t CalData,
       Bool_t DefaultCalibration,
       const Char_t *NameExt)
   : BaseDetector(Name, 2, false, CalData, DefaultCalibration, false, NameExt, true, true)
   {
     START;
     NumberOfSubDetectors = NDetectors;
     fRawDataSubDetectors  = RawData;

   #ifdef PRESORT_DC
     CreatedPreSort = false;
   #endif
     // NStrips required
     NStrips = 3 ;

     SetId(Name);
     AllocateComponents();

     //Counters
     AddCounter("E"); //2
     AddCounter("T"); //3
     AddCounter("PresentWire"); //4
     AddCounter("STRIP Mult >= 3"); //5
     AddCounter("X: Sequence Hyperbolique"); //6
     AddCounter("X: Weighted Average"); //7
     AddCounter("X: MultiplePeak"); //8
     AddCounter("Y: Present"); //9
   #ifdef DCE10
     AddCounter("Ex10"); //10
   #endif

     ///////////////////////////////////counters (Diego)/////////////////////////
     for(int i=0;i<4;i++)
       DCTR[i]=0;
     /////////////////////////////////////////////////////////////


   #ifdef DEGRADED_MODE
     cout << "WARNING !! DEGRADED MODE (NO check of WIRE Energy and Time) " <<endl;
   #endif

     if(fCalData)
     {
       // X in  [-1500, 0]
       SetGateCal(-2000,2000,0);
       // Y in [0,500]
       SetGateCal(-2000,2000,1);
       sprintf(CalNameI[0],"%s_X", DetectorName);
       sprintf(CalNameI[1],"%s_Y", DetectorName);

     }
     ReadCalibration();

     END;
   }
   DriftChamberNUMEXO::~DriftChamberNUMEXO(void)
   {
     START;

     MaxStrip = FreeDynamicArray<UShort_t>(MaxStrip);

     END;
   }

   void DriftChamberNUMEXO::Clear(void)
   {
     START;
     if(DetList->at(2)->GetM())
       for(UShort_t i=0; i<3 ;i++)
         MaxStrip[i] = 0;

     BaseDetector::Clear();
     fPresentX = false;
     fPresentY = false;
     fPresentWires = false;
     END;
   }

   void DriftChamberNUMEXO::AllocateComponents(void)
   {
     START;

     MaxStrip = AllocateDynamicArray<UShort_t>(3);
     for(UShort_t i=0; i<3; i++)
       MaxStrip[i] = 0;

     Bool_t RawData = fRawDataSubDetectors;
     Bool_t CalData = fCalData;
     Bool_t DefCal = false;
     Bool_t PosInfo = false;

     string BaseName;
     string Name;
     BaseName = DetectorName ;
     Name = BaseName + "_EW";
     BaseDetector *EW = new BaseDetector(Name.c_str(),1,RawData,CalData,DefCal,PosInfo,"",false,true,3);
     if(CalData)
     {
       Name.clear();
       Name = "EWIRE_";
       Name += to_string(DCId);
       EW->SetCalName(Name.c_str(),0);
     }

     AddComponent(EW);
     Name.clear();
     Name = BaseName + "_TW";
     BaseDetector *TW = new BaseDetector(Name.c_str(),1,RawData,CalData,DefCal,PosInfo,"",false,true);
     if(CalData)
     {
       Name.clear();
       Name = "TWIRE_";
       Name += to_string(DCId);
       TW->SetCalName(Name.c_str(),0);
     }
     AddComponent(TW);

     Name.clear();
     Name = BaseName + "_Q";
     BaseDetector *Q = new BaseDetector(Name.c_str(),160,RawData,CalData,DefCal,PosInfo,"",false,true);
     Q->SetRawHistogramsParams(4096,0,4095,"");
     Q->SetCalHistogramsParams(5000,0,20000,"","");

     AddComponent(Q);




     END;
   }

   void DriftChamberNUMEXO::SetParameters(NUMEXOParameters* PL,Map* Map)
   {
     START;
     if(isComposite)
     {

       Char_t PName[20];
       Int_t Id=0;

       // 161 - Ch 0 DC0-0-64
       // 161 - Ch 1 DC0-65-96
       // 161 - Ch 2 DC0-97-160
       // 161 - Ch 3 DC1-0-64
       // 161 - Ch 4 DC1-65-96
       // 161 - Ch 5 DC1-97-160

       // 163 - Ch 0 DC0-0-64
       // 163 - Ch 1 DC0-65-96
       // 163 - Ch 2 DC0-97-160
       // 163 - Ch 3 DC1-0-64
       // 163 - Ch 4 DC1-65-96
       // 163 - Ch 5 DC1-97-160


       if(sscanf(DetectorName,"DC%1d",&Id)==1)
       {
         sprintf(PName,"EWIRE_%d_%d",Id/2+1,Id%2+1);

         DetList->at(0)->SetParameterName(PName,0);
         DetList->at(0)->SetRawHistogramsParams(64000,0,63999);
         DetList->at(0)->SetGateRaw(0,63999);

         sprintf(PName,"TWIRE_%d_%d",Id/2+1,Id%2+1);
         DetList->at(1)->SetParameterName(PName,0);
         DetList->at(1)->SetRawHistogramsParams(64000,0,63999);
         DetList->at(1)->SetGateRaw(0,63999);

         for(int i=0; i < DetList->at(2)->GetNumberOfDetectors(); i++)
         {
            sprintf(PName, "DC_%d_%d", Id, i);

           //cout << "DC" << Id << " Chan " << i << "Name : " << PName << endl;
           DetList->at(2)->SetParameterName(PName, i);
           DetList->at(2)->SetRawHistogramsParams(16384,0,16383);
           DetList->at(2)->SetGateRaw(0,16383);
         }

       }
       else
       {
         Char_t Message[500];
         sprintf(Message,"In <%s><%s> Could not retreive Drift Chamber Numbers format should be DCX where (X =0-3)!", DetList->at(2)->GetName(), DetList->at(2)->GetName());
         MErr * Er= new MErr(WhoamI,0,0, Message);
         throw Er;
       }


       NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
       DetList->at(0)->SetParametersNUMEXO(PL_NUMEX, Map);
       DetList->at(1)->SetParametersNUMEXO(PL_NUMEX, Map);
       for (UShort_t i = 2; i < DetList->size(); i++)
         DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);

     }
     END;
   }
   #ifdef PRESORT_DC
   void DriftChamberNUMEXO::SetCreatedPreSort(Bool_t v)
   {
     START;
     CreatedPreSort = v;
     END;
   }
   #endif

   Bool_t DriftChamberNUMEXO::Treat(void)
   {
     START;
     Ctr->at(0)++;

   #ifdef PRESORT_DC
     if(!CreatedPreSort)
     {
       DetList->at(2)->Treat();
       if(DetList->at(2)->GetM() > 2)
         TreatX();
       return 0;
     }
   #endif
     // Calibrate Wire Energy
     if(DetList->at(0)->Treat())
     {
       Ctr->at(2)++;
     }
   #ifdef DCE10
     if(DetList->at(3)->Treat())
     {
       Ctr->at(10)++;
     }
   #endif
     // Calibrate Wire Time
     if(DetList->at(1)->Treat())
     {
       Ctr->at(3)++;
     }

     if(fCalData)
     {
       // ForCalibs
       if(CalibrationMode)
       {
         // Only Calibrate PADs
         DetList->at(2)->Treat();
         // return 0;
       }
       else
       {
         // Treat Pads if wire is Present
   #ifdef DEGRADED_MODE
         if(1)
   #else
           if((DetList->at(0)->IsPresent() && DetList->at(1)->IsPresent()))
   #endif
           {
             Ctr->at(4)++;
             if(DetList->at(2)->Treat())
             {
               if(DetList->at(2)->GetM() > 2)
               {
                 if(!TreatX())
                   SetCalData(0,-1500.);
                 if(DetList->at(1)->IsPresent())
                 {
                   if(!TreatY())
                     SetCalData(1,-1500.);
                 }
                 else
                   SetCalData(1,-1500.);
                 isPresent = fPresentX && fPresentY;
               }
               else
               {
                 SetCalData(0,-1500.);
                 SetCalData(1,-1500.);
               }
             }
             else
             {
               SetCalData(0,-1500.);
               SetCalData(1,-1500.);
             }
           }
           else
           {
             SetCalData(0,-1500.);
             SetCalData(1,-1500.);
           }
       }
     }

     return isPresent;
     END;
   }



   void DriftChamberNUMEXO::ReadCalibration(void)
   {
     START;
     MIFile *IF;
     char Line[255];
     stringstream *InOut;

     if(fCalData)
     {
       InOut = new stringstream();
       *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << ".cal";
       *InOut>>Line;
       InOut = CleanDelete(InOut);
       if((IF = CheckCalibration(Line)))
       {
         // Position
         try
         {
           ReadReference(IF);
         }
         catch(...)
         {
           IF = CleanDelete(IF);
           throw;
         }
         // Energy
         try{
           DetList->at(0)->ReadCalibration(IF);
         }
         catch(...)
         {
           IF = CleanDelete(IF);
           throw;
         }
         // Time
         try
         {
           DetList->at(1)->ReadCalibration(IF);

         }
         catch(...)
         {
           IF = CleanDelete(IF);
           throw;

         }
         // Strips
         try
         {
           DetList->at(2)->ReadCalibration(IF);
         }
         catch(...)
         {
           IF = CleanDelete(IF);
           throw;
         }
         IF = CleanDelete(IF);
       }
       else
       {

         GenerateDefaultCalibration(Line);

       }


       // //Read Thresholds
       // if(fCalGate)
       //   {
       // 	 InOut = new stringstream();
       // 	 *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Th.cal";
       // 	 *InOut>>Line;
       // 	 InOut = CleanDelete(InOut);
       //     if(IF = CheckCalibration(Line))
       //       {
       //         // Thresholds on Strips
       //         try
       //           {
       //             DetList->at(2)->ReadCalGates(IF);
       //           }
       //         catch(...)
       //           {
       //             throw;
       //           }
       //       }

       //   }
     }
     END;
   }

   void DriftChamberNUMEXO::GenerateDefaultCalibration(Char_t *fName)
   {
     START;

     cout << "==============================================================" << endl;
     cout << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
       "File " << fName << " not found" << endl <<
       "Generate a new file with Default Calibrations!" << endl;
     cout << "==============================================================" << endl;
     L->File << "==============================================================" << endl;
     L->File  << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
       "File " << fName << " not found" << endl <<
       "Generate a new file with Default Calibrations!" << endl;
     L->File  << "==============================================================" << endl;

     MOFile *OF = NULL;
     try
     {
       OF = new MOFile(fName);
     }
     catch(MErr *Er)
     {
       cout << "==============================================================" << endl;
       cout << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
         "File " << fName << " Could not be created" << endl
         << "Using default calibration instead!" << endl;;
       cout << "==============================================================" << endl;
       L->File  << "==============================================================" << endl;
       L->File  << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
         "File " << fName << " Could not be created" << endl
         << "Using default calibration instead!" << endl;;
       L->File  << "==============================================================" << endl;
       OF = CleanDelete(OF);

     }
     const time_t now = time(0);
     char* dt = ctime(&now);

     OF->File << "// Title   : Default calibration file for <" << DetectorName << "><" << DetectorNameExt << "> :" << endl;
     OF->File << "// Date    : " << dt ;
     OF->File << "// Comment : " << endl;

     // Reference Position
     RefPos[0] = RefPos[1] = RefPos[2] = 0;
     OF->File << "// Reference Position X Y Z " << endl;
     OF->File <<  RefPos[0] << " " <<  RefPos[1] << " " <<  RefPos[2] << endl;

     //DriftVelocity cm/us   new DC 23/09/2010 MR
     DriftVelocity = 5.387;
     OF->File << "// DriftVelocity cm/us   new DC 23/09/2010 MR" << endl;
     OF->File <<   DriftVelocity << endl;

     //QThresh
     Float_t QThresh = 0.01;
     DetList->at(2)->SetGateCal(QThresh,16384.);
     OF->File << "// QThresh" << endl;
     OF->File <<  QThresh << endl;

     //Energy Wire calib
     OF->File << "//Energy Wire calib" << endl;
     OF->File << "0. 1. 0 " << endl;

     //Time Wire calib
     OF->File << "//Time Wire calib" << endl;
     OF->File << "0. 1. 0 " << endl;

     OF->File << "//Charge Strip calib " << endl;
     OF->File << "// Format  : a0 \t a1 \t a2 \t // ParameterName" << endl;

     for(UShort_t i =0; i< DetList->at(2)->GetNumberOfDetectors();i++)
     {
       for(UShort_t j=0;j<3;j++)
       {
         cout << DetList->at(2)->GetCalCoeffs()[i][j] << " " ;
         if(OF) OF->File <<  DetList->at(2)->GetCalCoeffs()[i][j] << " ";
       }
       cout << "// " <<  DetList->at(2)->GetRawName(i)<< endl;
       if(OF) OF->File  << "// " << DetList->at(2)->GetRawName(i)<< endl;
     }
     OF = CleanDelete(OF);

     END;
   }

   Bool_t DriftChamberNUMEXO::CheckMultiplePeaksD(UShort_t *FStrip)
   {
     START;

     Float_t Max2=0;
     UShort_t MMax2=0;
     Int_t ctt1=0;
     Int_t ctt2=0;

     for(UShort_t j=0;j<DetList->at(2)->GetM();j++)
     {
       if(abs(DetList->at(2)->GetNrAt(j)-DetList->at(2)->GetNrAt(FStrip[0]))>3)
       {
         if(abs(DetList->at(2)->GetNrAt(j)-DetList->at(2)->GetNrAt(FStrip[0]))>3)
           if(DetList->at(2)->GetCalAt(j)>0.5*DetList->at(2)->GetCalAt(FStrip[0]))
             {
               if(DetList->at(2)->GetCalAt(j)>Max2)
                 {
                   Max2 = DetList->at(2)->GetCalAt(j);
                   MMax2 =j;
                 }
             }
       }
     }

     for(UShort_t j=0;j<DetList->at(2)->GetM();j++)
     {
       if((abs(DetList->at(2)->GetNrAt(j)-DetList->at(2)->GetNrAt(FStrip[0]))==1)
           && (DetList->at(2)->GetCalAt(j)>0.3*DetList->at(2)->GetCalAt(FStrip[0])))
         ctt1++;
       if(Max2>0)
         if((abs(DetList->at(2)->GetNrAt(j)-DetList->at(2)->GetNrAt(MMax2))==1)
             && (DetList->at(2)->GetCalAt(j)>0.3*DetList->at(2)->GetCalAt(MMax2)))
           ctt2++;
     }

     if(ctt1!=2)
     {
   #ifdef DEBUG
       cout<<"///////////////////////////"<<endl;
       cout<<"First peak not real peak"<<endl;
       cout<<"Central Positions:  "<<DetList->at(2)->GetNrAt(FStrip[0])<<endl;
       for(UShort_t j=0;j<DetList->at(2)->GetM();j++)
         cout<< DetList->at(2)->GetNrAt(j)<<" "<<DetList->at(2)->GetCalAt(j)<<endl;
       cout<<"----------------------------"<<endl;
   #endif
     }
     if(Max2>0&&ctt2!=2)
     {
   #ifdef DEBUG
       cout<<"///////////////////////////"<<endl;
       cout<<"Second peak not real peak"<<endl;
   #endif
     }

     if(Max2>0&&ctt1==2&&ctt2==2)
     {

   #ifdef DEBUG
       cout<<"----------------------------"<<endl;
       cout<<"Central Positions:  "<<DetList->at(2)->GetNrAt(FStrip[0])<<" "<<DetList->at(2)->GetNrAt(MMax2)<<endl;
       for(UShort_t j=0;j<DetList->at(2)->GetM();j++)
         cout<< DetList->at(2)->GetNrAt(j)<<" "<<DetList->at(2)->GetCalAt(j)<<endl;
       cout<<"----------------------------"<<endl;
   #endif

       Ctr->at(8)++;
       return true;
     }

     return false;
     END;
   }


   Bool_t DriftChamberNUMEXO::CheckMultiplePeaks(UShort_t *FStrip)
   {
     START;




     for(UShort_t j=0;j<DetList->at(2)->GetNrAt(FStrip[0])-1;j++)
       if((DetList->at(2)->GetCal(j) > DetList->at(2)->GetCal(j+1)))
         if(DetList->at(2)->GetCal(j) >= 0.4*DetList->at(2)->GetCal(DetList->at(2)->GetNrAt(FStrip[0])))
         {
           Ctr->at(8)++;
//            cout<< j << " -> 11 ----------------------------"<<endl;
//            for(UShort_t j2=0;j2<DetList->at(2)->GetM();j2++)
//              cout<< DetList->at(2)->GetNrAt(j2)<<" "<<DetList->at(2)->GetCalAt(j2)<<endl;
//            cout<<"----------------------------"<<endl;
           return true;
         }

     for(UShort_t j=DetList->at(2)->GetNrAt(FStrip[0]);j<DetList->at(2)->GetNumberOfDetectors();j++)
       if(DetList->at(2)->GetCal(j) < DetList->at(2)->GetCal(j+1))
         if(DetList->at(2)->GetCal(j+1) >= 0.4*DetList->at(2)->GetCal(DetList->at(2)->GetNrAt(FStrip[0])))
         {
           Ctr->at(8)++;
//           cout<< j << " -> 22 ----------------------------"<<endl;
//           for(UShort_t j2=0;j2<DetList->at(2)->GetM();j2++)
//             cout<< DetList->at(2)->GetNrAt(j2)<<" "<<DetList->at(2)->GetCalAt(j2)<<endl;
//           cout<<"----------------------------"<<endl;
           return true;
         }
     return false;
     END;
   }

   Bool_t DriftChamberNUMEXO::CheckNeighbours(UShort_t *FStrip)
   {
     START;
     if( abs(DetList->at(2)->GetNrAt(FStrip[0])-DetList->at(2)->GetNrAt(FStrip[1])) == 1
         &&
         abs(DetList->at(2)->GetNrAt(FStrip[0])-DetList->at(2)->GetNrAt(FStrip[2])) == 1 )
       return true;
     else
     {
//       cout << "Check Neighbours " << endl;
//        cout<<DetList->at(2)->GetNrAt(FStrip[0])<<" "<<DetList->at(2)->GetNrAt(FStrip[1])<<" "<<DetList->at(2)->GetNrAt(FStrip[2])<<endl;
//         DetList->at(2)->PrintCal();
       return false;

       // if( abs(N[FStrip[0]][i]-N[FStrip[1]][i]) <=2
       // 	  &&
       // 	  abs(N[FStrip[0]][i]-N[FStrip[2]][i]) <= 2)
       // 	if(!Neighbours&&!MultiplePeak)
       // 	  {
       // 		 // if(i==2 && N[FStrip[0]][i]>=96&&N[FStrip[0]][i]<=98)
       // 		 //   {
       // 		 //   cout << i << " " << N[FStrip[0]][i] << " " << N[FStrip[1]][i] << " "<< N[FStrip[2]][i] << endl;
       // 		 //   Show_Raw();
       // 		 //   Show();
       // 		 //   }
       // 		 // H1D[12+i]->Fill( N[FStrip[0]][i]);
       // 		 // H1D[12+i]->Fill( N[FStrip[1]][i]);
       // 		 // H1D[12+i]->Fill( N[FStrip[2]][i]);
       // 	  }
     }
     END;
   }


   Float_t DriftChamberNUMEXO::SECHIP(UShort_t *FStrip)
   {
     START;
     Float_t v[6];
     Float_t XS=-1500;

     v[0] = sqrt(DetList->at(2)->GetCalAt(FStrip[0])/DetList->at(2)->GetCalAt(FStrip[2]));
     v[1] = sqrt(DetList->at(2)->GetCalAt(FStrip[0])/DetList->at(2)->GetCalAt(FStrip[1]));
     v[2] = 0.5*(v[0]+v[1]);
     v[3] = log(v[2]+sqrt(pow(v[2],2.f)-1.0));
     v[4] = (v[0] - v[1])/(2.0*sinh(v[3]));
     v[5] = 0.5*log((1.0+v[4])/(1.0-v[4]));

     XS = (Float_t) DetList->at(2)->GetNrAt(FStrip[0])
       - (Float_t) (DetList->at(2)->GetNrAt(FStrip[0])-DetList->at(2)->GetNrAt(FStrip[1]))*v[5]/v[3];

     //  printf("Pad %01d %2.3f\n",DCId ,XS);


     if(DCId%2 != 0)
       XS -= 0.5;

     XS *= -6.4; //Goes mm 6.02 pad 0.38 space

     if(XS < 0. && XS>-1500.)
     {
       Ctr->at(6)++;
       fPresentX = true;
       XS -= GetRefX();

       // 	 for(j=0;j<3;j++)
       // 		{
       // 		  QPk[j] = Q[FStrip[j]];
       // 		  NPk[j] = N[FStrip[j]];
       // 		}

     }
     else
       XS = -1500.;

     return XS;
     END;
   }

   Float_t DriftChamberNUMEXO::WeightedAverage(UShort_t *FStrip, UShort_t NStrips)
   {
     START;
     Float_t v[2];
     UShort_t StripsWA=0;
     Float_t XWA=-1500.;

     if(DetList->at(2)->GetM() >= NStrips)
            {
                   //Looking for entire peak for W.A.
                   //The Strips are ordered  0-160
                   //Could be done earlier but ....
                   StripsWA=0;
                   for(Int_t j=FStrip[0]-1;j>=0;j--)
                     {
                             if(abs(DetList->at(2)->GetNrAt(j+1)-DetList->at(2)->GetNrAt(j)) == 1)
                                   {
                                     if((DetList->at(2)->GetCalAt(j) <= DetList->at(2)->GetCalAt(j+1)))
                                            {
                                                   StripsWA++;
                                                   FStrip[StripsWA]=j;
                                            }
                                     else
                                            break;
                                   }
                            else
                                   {
                                     break;
                                   }
                     }
                   for(Int_t j=FStrip[0];j<DetList->at(2)->GetM()-1;j++)
                     if(abs(DetList->at(2)->GetNrAt(j+1)-DetList->at(2)->GetNrAt(j)) == 1)
                            {
                                   if(DetList->at(2)->GetCalAt(j) >= DetList->at(2)->GetCalAt(j+1))
                                     {
                                            StripsWA++;
                                            FStrip[StripsWA]=j+1;
                                     }
                                   else
                                     break;
                            }
                     else
                            break;


                   if(StripsWA >= NStrips)
                     {
                       v[0] = v[1] = 0.0;
                       for(UShort_t k=0;k<=StripsWA;k++)
                         {
                           v[0] += DetList->at(2)->GetCalAt(FStrip[k]) * ((Float_t) DetList->at(2)->GetNrAt(FStrip[k]));
                           v[1] += DetList->at(2)->GetCalAt(FStrip[k]);
                         }

   #ifdef DEBUG
                       cout << "DriftChamberNUMEXOv::FocalSubseqX::Weithed Average: "  << endl;
                       cout << "StripsWA: " << StripsWA << endl;
                       for(UShort_t k=0;k<=StripsWA;k++)
                         cout << "STR: " << DetList->at(2)->GetNrAt(FStrip[k]) << " Q: " << DetList->at(2)->GetCalAt(FStrip[k]) << endl;
   #endif
                       XWA = v[0] / v[1];
                       if(DCId%2 != 0)
                         XWA -= 0.5;

                       XWA *= -6.4; //Goes mm 6.02 pad 0.38 space
                       if(XWA < 0. && XWA>-1500.)
                         {
                           fPresentX = true;
                           XWA -= GetRefX();
                           Ctr->at(7)++;
                         }
                       else
                         XWA = -1500.;
                     }
            }
     return XWA;
     END;
   }


   Bool_t DriftChamberNUMEXO::TreatX(void)
   {
     START;
     // Nmmber of Required Strips
     Float_t QTmp[160];
     Float_t QMax;
     UShort_t NMax;
     UShort_t FStrip[160];
     Bool_t Neighbours;
   #ifdef MULTIPEAK_DC
     Bool_t MultiplePeak;
   #endif
   #ifdef DC_CHARGE
     Float_t QTot = 0;
   #endif

     Float_t X=-1500.;
     // Loop over strips
     if(DetList->at(2)->GetM() >= NStrips)
       Ctr->at(5)++;
     else
       return false;

     for(UShort_t j=0; j< DetList->at(2)->GetM();j++) //loop over strips
     {
       QTmp[j] = DetList->at(2)->GetCalAt(j);// Copy Charges
     }


     // Find Nstrip highest charges
     for(UShort_t k=0;k<NStrips; k++)
     {
       QMax=0.0;
       NMax=0;
       for(UShort_t j=0;j<DetList->at(2)->GetM();j++)
       {
         if(QTmp[j] > QMax)
         {
           QMax = QTmp[j];
           NMax = j;
         }
       }
       QTmp[NMax] = 0.0;
       FStrip[k] = NMax;
       MaxStrip[k] = DetList->at(2)->GetNrAt(NMax);
   #ifdef DC_CHARGE
       DetList->at(Det_Charge_Offset+1)->SetCalData(k,QMax);
       DetList->at(Det_Charge_Offset+2)->SetCalData(k,MaxStrip[k]);
       QTot += QMax;
   #endif
     }

     if(FStrip[0]>0&&FStrip[1]>0&&FStrip[2]>0)
       DCTR[0]++;

   #ifdef DC_CHARGE
     DetList->at(Det_Charge_Offset+3)->SetCalData(0,QTot);
   #endif

   #ifdef PRESORT_DC
     if(!CreatedPreSort)
       return 0;
   #endif

   #ifdef MULTIPEAK_DC
     MultiplePeak = false;
     if((MultiplePeak = CheckMultiplePeaks(FStrip)))
       //if((MultiplePeak = CheckMultiplePeaksD(FStrip)))
       {
        #ifdef DEBUG
         cout << "Multiplepeak: " << DetectorName <<  endl;
         DetList->at(2)->PrintCal();
        #endif
       }
   #endif
     if(NStrips != 3)
       {
         MErr * Er = new MErr(WhoamI,0,0, "NStrips != 3 but");
         throw Er;
       }

     Neighbours = false;
   #ifdef MULTIPEAK_DC
     if(FStrip[0]>0&&FStrip[1]>0&&FStrip[2]>0 && MultiplePeak)
       DCTR[3]++;
   #endif

   #ifdef MULTIPEAK_DC
     if(!MultiplePeak)
     {
   #endif
       if((Neighbours = CheckNeighbours(FStrip)))
       {
         X = SECHIP(FStrip);
       }
       else
       {
   #ifdef WEIGHTEDAVERAGE_DC
         X = WeightedAverage(FStrip,NStrips);
         // DetList->at(2)->PrintCal();
   #endif
           }

         if(/*FStrip[0]>0&&FStrip[1]>0&&FStrip[2]>0 &&*/ Neighbours)
           {
             DCTR[1]++;
             if(X>-1000)
               DCTR[2]++;
           }
         // else
         // 	{
         // 	  cout<<DetList->at(2)->GetName()<<" NOT NEIGHBOURS N = "<<DetList->at(2)->GetNrAt(FStrip[0])<<" "<<DetList->at(2)->GetNrAt(FStrip[1])<<" "<<DetList->at(2)->GetNrAt(FStrip[2])<<endl;
         // 	      for(UShort_t j=0;j<DetList->at(2)->GetM();j++)
         // 		cout<< DetList->at(2)->GetNrAt(j)<<" "<<DetList->at(2)->GetCalAt(j)<<endl;

         // 	}

   #ifdef MULTIPEAK_DC
     }
   #endif
     if(fPresentX)
       SetCalData(0,X);

     return fPresentX;

     END;
   }


   Bool_t DriftChamberNUMEXO::TreatY(void)
   {
     START;
     Float_t Y=-1500.;
     fPresentY = false;
     // Drift Velocity in cm/us
     Y = DetList->at(1)->GetCalAt(0);
     Y *= DriftVelocity*10./1000.; /// Eventually should convert before to prevent operation for every event...
     if(Y > 0)
     {
       fPresentY = true;
       //The wires are upside
       Y *= -1.0;
       Y -= GetRefY();
       Ctr->at(9)++;
       SetCalData(1,Y);

     }
     else
     {
       Y = -1500;
       fPresentY = false;
     }


     return fPresentY;

     END;
   }


   void DriftChamberNUMEXO::ReadReference(MIFile *IF)
   {
     START;
     char Line[255];
     UShort_t Len=255;
     Char_t *Ptr=NULL;
     Float_t QThresh=0;

     L->File << "<" << DetectorName << "><" << DetectorNameExt
       <<"> Reading Reference from file " << IF->GetFileName() << endl;
     do
     {
       IF->GetLine(Line,Len);
     }  while((Ptr = CheckComment(Line)))  ;
     // X Y Z Positions
     stringstream *InOut = NULL;
     InOut= new stringstream();
     *InOut << Line;
     *InOut >> RefPos[0];
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[500];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading YRef!",
           DetList->at(2)->GetName(),
           DetList->at(2)->GetName(),
           IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }
     *InOut >> RefPos[1];
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[500];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading YRef!",
           DetList->at(2)->GetName(),
           DetList->at(2)->GetName(),
           IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }
     *InOut >> RefPos[2];
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[500];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading ZRef!", DetList->at(2)->GetName(), DetList->at(2)->GetName(),IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }

     L->File << "<"<< DetList->at(2)->GetName() << "><"
       << DetList->at(2)->GetNameExt()
       << "> : Reference X Y Z (mm) " << endl;
     L->File <<  fixed << setprecision(3)
       << RefPos[0] << " "
       << RefPos[1] << " "
       << RefPos[2]  << endl;

     // DriftVelocity ---------------
     IF->GetLine(Line,Len);
     if(!(Ptr = CheckComment(Line)))
     {
       Char_t Message[500];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading DriftVelocity!", DetList->at(2)->GetName(), DetList->at(2)->GetName(),IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }

     L->File << "<"<< DetList->at(2)->GetName()
       << "><" <<DetList->at(2)->GetNameExt()
       << "> : " << Ptr+2 << endl;

     IF->GetLine(Line,Len);
     InOut->clear();
     *InOut << Line;
     *InOut >> DriftVelocity;
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[500];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading DriftVelocity!",
           DetList->at(2)->GetName(),
           DetList->at(2)->GetNameExt(),
           IF->GetFileName());
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }
     L->File <<  DriftVelocity  << endl;

     // Q Threholds ---------------
     IF->GetLine(Line,Len);
     if(!(Ptr = CheckComment(Line)))
     {
       Char_t Message[500];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading Q Thresholds!",
           DetList->at(2)->GetName(),
           DetList->at(2)->GetNameExt(),
           IF->GetFileName());
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }
     L->File << "<"<< DetList->at(2)->GetName()
       << "><" <<DetList->at(2)->GetNameExt()
       << "> : " << Ptr+2 << endl;

     IF->GetLine(Line,Len);
     InOut->clear();
     *InOut << Line;
     *InOut >> QThresh;
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[500];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading Q Thresholds!",
           DetList->at(2)->GetName(),
           DetList->at(2)->GetName(),
           IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }

     L->File <<  fixed << setprecision(3) <<  QThresh  << endl;
     if(QThresh >0)
     {
       if(fCalData)
         DetList->at(2)->SetGateCal(QThresh,16384.);
     }
     InOut = CleanDelete(InOut);

     END;
   }
   void DriftChamberNUMEXO::SetId(const Char_t *Name)
   {
     START;
     UShort_t Id=0;
     if(sscanf(Name,"DC%1hu",&Id)==1)
     {
       DCId = Id;
     }
     else
     {
       Char_t Message[500];
       sprintf(Message,"In <%s><%s> : Wrong Detector Name in (%s) while extracting its Id!",
           GetName(),
           GetNameExt(),
           GetName());
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     };
     END;
   }

   #ifdef WITH_ROOT
   void DriftChamberNUMEXO::CreateHistogramsCal1D(TDirectory *Dir)
   {
     START;
     string Name;
     Dir->cd("");
     if(SubFolderHistCal1D.size()>0)
     {
       Name.clear();
       Name = SubFolderHistCal1D ;
       if(!(gDirectory->GetDirectory(Name.c_str())))
         gDirectory->mkdir(Name.c_str());
       gDirectory->cd(Name.c_str());
     }

     char HName[500];
     char HTitle[500];
     sprintf(HName,"X_%s%s",GetName(),GetNameExt());
     sprintf(HTitle,"%s%s X (mm)",GetName(),GetNameExt());
     AddHistoCal(GetCalName(0),HName,HTitle,1000,-500,500);
     sprintf(HName,"Y_%s%s",GetName(),GetNameExt());
     sprintf(HTitle,"%s%s Y (mm)",GetName(),GetNameExt());
     AddHistoCal(GetCalName(1),HName,HTitle,1000,-500,500);



     END;
   }

   void DriftChamberNUMEXO::CreateHistogramsCal2D(TDirectory *Dir)
   {
     START;
     string Name;
     char HName[500];
     char HTitle[500];

     BaseDetector::CreateHistogramsCal2D(Dir);

     Dir->cd("");

     if(SubFolderHistCal2D.size()>0)
     {
       Name.clear();
       Name = SubFolderHistCal2D ;
       if(!(gDirectory->GetDirectory(Name.c_str())))
         gDirectory->mkdir(Name.c_str());
       gDirectory->cd(Name.c_str());
     }

     sprintf(HName,"X_Y_DC%d",DCId);
     sprintf(HTitle,"X vs Y %s (mm) (mm)", GetName());
     AddHistoCal(GetCalName(0),GetCalName(1),HName,HTitle,1000,-500,500,1000,-500,500);

     END;
   }

   void DriftChamberNUMEXO::SetOpt(TTree *OutTTree, TTree *InTTree)
   {
     START;
     char PName[200];

     // St histogram Hierarchy
     SetMainHistogramFolder("");
     SetHistogramsCal1DFolder("Dc1D");
     SetHistogramsCal2DFolder("Dc2D");

     for(UShort_t i = 0;i<DetList->size();i++)
     {
       DetList->at(i)->SetMainHistogramFolder("");
       DetList->at(i)->SetHistogramsRaw1DFolder("Dc1D");
       DetList->at(i)->SetHistogramsRaw2DFolder("Dc2D");
       DetList->at(i)->SetHistogramsCal1DFolder("Dc1D");
       DetList->at(i)->SetHistogramsCal2DFolder("Dc2D");
     }

     if(fMode == MODE_WATCHER)
     {
       if(fRawData)
       {
         SetHistogramsRaw(true);
       }
       if(fCalData)
       {
         SetHistogramsCal(true);
       }
       for(UShort_t i = 0;i<DetList->size();i++)
       {
         if(DetList->at(i)->HasRawData())
         {
           if(i == 2)
             DetList->at(i)->SetHistogramsRawSummary(true);
           else
             DetList->at(i)->SetHistogramsRaw1D(true);
         }
         if(DetList->at(i)->HasCalData())
         {
           switch (i)
           {
             case 2:
               DetList->at(i)->SetHistogramsCalSummary(true);
               break;
             default:
               DetList->at(i)->SetHistogramsCal1D(true);
               break;
           }
         }
       }
     }
     else if(fMode == MODE_D2R)
     {
       for(UShort_t i = 0;i<DetList->size();i++)
       {
         if(DetList->at(i)->HasRawData())
         {
           if(i == 2)
           {
             DetList->at(i)->SetOutAttachRawV(true);
             DetList->at(i)->SetHistogramsRaw2D(true);
             DetList->at(i)->SetHistogramsRawSummary(true);
           }
           else
           {
             DetList->at(i)->SetHistogramsRaw1D(true);
             DetList->at(i)->SetOutAttachRawI(true);
             DetList->at(i)->SetOutAttachTS(true);
           }
         }
         DetList->at(i)->OutAttach(OutTTree);
       }
       OutAttach(OutTTree);

     }
     else if(fMode == MODE_D2A)
     {
       if(fRawData)
       {
         SetHistogramsRaw(false);
         SetOutAttachRawI(false);
         SetOutAttachRawV(false);
       }
       if(fCalData)
       {
         SetHistogramsCal(true);
         SetOutAttachCalI(true);
         SetOutAttachCalV(false);
       }

       for(UShort_t i = 0;i<DetList->size();i++)
       {
         if(DetList->at(i)->HasRawData())
         {
           DetList->at(i)->SetHistogramsRaw1D(false);
           DetList->at(i)->SetHistogramsRaw2D(true);
           if(i == 2)
             DetList->at(i)->SetHistogramsRawSummary(true);
           DetList->at(i)->SetOutAttachRawI(false);
           DetList->at(i)->SetOutAttachRawV(false);
           if(i==0 || i == 1)
           {
             DetList->at(i)->SetOutAttachRawI(true);
             DetList->at(i)->SetOutAttachTS(true);
           }
           //                           DetList->at(i)->SetOutAttachTS(true);

         }
         if(DetList->at(i)->HasCalData())
         {
           switch (i)
           {
             case 2:
               DetList->at(i)->SetHistogramsCal1D(false);
               DetList->at(i)->SetHistogramsCalSummary(true);
               // Drift Cal Strips
               // No strips
               DetList->at(i)->SetOutAttachCalV(true);
               DetList->at(i)->SetOutAttachCalI(false);
               break;
             case 0: // EWIRE
             case 1: // TWIRE

               DetList->at(i)->SetOutAttachCalI(true);
               break;
   #ifdef DCE10
             case 3: // EWIREx10
               DetList->at(i)->SetHistogramsCal1D(false);
               DetList->at(i)->SetOutAttachCalI(false);
               break;
   #endif
   #ifdef DC_CHARGE
             case 4:
             case 5:
               DetList->at(i)->SetHistogramsCal1D(false);
               DetList->at(i)->SetOutAttachCalF(true);
               DetList->at(i)->SetOutAttachCalI(false);
               break;
             case 6:
               DetList->at(i)->SetHistogramsCal1D(true);
               DetList->at(i)->SetOutAttachCalI(true);
               break;
   #endif
             default:
               DetList->at(i)->SetHistogramsCal1D(true);
               DetList->at(i)->SetOutAttachCalV(true);
               break;
           }
         }
         DetList->at(i)->OutAttach(OutTTree);
       }

       OutAttach(OutTTree);
     }
     else if(fMode == MODE_R2A)
     {
       for(UShort_t i = 0;i<DetList->size();i++)
       {
         if(i == 2)
           DetList->at(i)->SetInAttachRawV(true);
         else
         {
           DetList->at(i)->SetInAttachRawV(false);
           DetList->at(i)->SetInAttachRawI(true);
         }
         DetList->at(i)->InAttach(InTTree);

       }

       if(fRawData)
       {
         SetHistogramsRaw(false);
         SetOutAttachRawI(false);
         SetOutAttachRawV(false);
       }
       if(fCalData)
       {
         SetHistogramsCal(true);
         SetOutAttachCalI(true);
         SetOutAttachCalV(false);
       }

       for(UShort_t i = 0;i<DetList->size();i++)
       {
         if(DetList->at(i)->HasCalData())
         {
           if(i == 2)
           {
             DetList->at(i)->SetHistogramsCal1D(false);
             DetList->at(i)->SetHistogramsCalSummary(true);
             DetList->at(i)->SetOutAttachCalV(true);
           }
           else
           {
             DetList->at(i)->SetHistogramsCal1D(true);
             DetList->at(i)->SetOutAttachCalI(true);
           }
         }
         DetList->at(i)->OutAttach(OutTTree);
       }
       OutAttach(OutTTree);
     }
     else if(fMode == MODE_RECAL)
     {
       for(UShort_t i = 0;i<DetList->size();i++)
       {
         if(DetList->at(i)->HasCalData())
         {
           switch (i)
           {
             case 2:
               // Drift Cal Strips
               DetList->at(i)->SetInAttachCalV(true);
               DetList->at(i)->SetInAttachCalI(false);
               break;
             case 0: // EWIRE
               DetList->at(i)->SetInAttachCalI(true);
               break;
             case 1: // TWIRE
   #ifdef DCE10
             case 3: // EWIREx10
               DetList->at(i)->SetInAttachCalI(false);
               break;
   #endif
   #ifdef DC_CHARGE
             case 4:
             case 5:
               DetList->at(i)->SetInAttachCalF(true);
               DetList->at(i)->SetInAttachCalI(false);
               break;
             case 6:
               DetList->at(i)->SetInAttachCalI(true);
               break;
   #endif
             default:
               DetList->at(i)->SetInAttachCalV(true);
               break;
           }
         }
         DetList->at(i)->InAttachCal(InTTree);
       }
       SetInAttachCalI(true);
       InAttachCal(InTTree);


       if(fCalData)
       {
         SetHistogramsCal(true);
         SetOutAttachCalI(true);
         SetOutAttachCalV(false);
       }

       for(UShort_t i = 0;i<DetList->size();i++)
       {

         if(DetList->at(i)->HasRawData())
         {
           DetList->at(i)->SetHistogramsRaw1D(false);
           DetList->at(i)->SetHistogramsRaw2D(true);
           if(i == 2)
             DetList->at(i)->SetHistogramsRawSummary(true);
           DetList->at(i)->SetOutAttachRawI(false);
           DetList->at(i)->SetOutAttachRawV(false);
           if(i==0)
             DetList->at(i)->SetOutAttachRawI(true);

         }
         if(DetList->at(i)->HasCalData())
         {
           switch (i)
           {
             case 2:
               DetList->at(i)->SetHistogramsCal1D(false);
               DetList->at(i)->SetHistogramsCalSummary(true);
               // Drift Cal Strips
               // No strips
               DetList->at(i)->SetOutAttachCalV(false);
               DetList->at(i)->SetOutAttachCalI(false);
               break;
             case 0: // EWIRE
               DetList->at(i)->SetOutAttachCalI(true);
               break;
             case 1: // TWIRE
   #ifdef DCE10
             case 3: // EWIREx10
               DetList->at(i)->SetHistogramsCal1D(false);
               DetList->at(i)->SetOutAttachCalI(false);
               break;
   #endif
   #ifdef DC_CHARGE
             case 4:
             case 5:
               DetList->at(i)->SetHistogramsCal1D(false);
               DetList->at(i)->SetOutAttachCalF(true);
               DetList->at(i)->SetOutAttachCalI(false);
               break;
             case 6:
               DetList->at(i)->SetHistogramsCal1D(true);
               DetList->at(i)->SetOutAttachCalI(true);
               break;
   #endif
             default:
               DetList->at(i)->SetHistogramsCal1D(true);
               DetList->at(i)->SetOutAttachCalV(true);
               break;
           }
         }
         DetList->at(i)->OutAttach(OutTTree);
       }
       OutAttach(OutTTree);

     }
     else if(fMode == MODE_CALC)
     {
       SetNoOutput();
     }
     else
     {
       Char_t Message[500];
       sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }

     if(VerboseLevel >= V_INFO)
       PrintOptions(cout)  ;

     END;
   }
   #endif
