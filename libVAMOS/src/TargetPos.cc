/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "TargetPos.hh"

TargetPos::TargetPos(const Char_t *Name, 
         UShort_t NDetectors, 
         Bool_t RawData, 
         Bool_t CalData, 
         Bool_t DefaultCalibration, 
         const Char_t *NameExt)
  : BaseDetector(Name, 6, false, CalData, DefaultCalibration, false, NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  
  DetManager *DM = DetManager::getInstance();

  FP = NULL;
  FP = (FocalPosition*) DM->GetDetector("FP");

  AllocateComponents();
  
  ReadPosition();

  AddCounter("Present TMW1 "); //2
  AddCounter("Present TMW2 "); //3

  if(fCalData)
    {
      sprintf(CalNameI[0],"%s_X", DetectorName);
      sprintf(CalNameI[1],"%s_Theta", DetectorName);
      sprintf(CalNameI[2],"%s_Y", DetectorName);
      sprintf(CalNameI[3],"%s_Phi", DetectorName);
      sprintf(CalNameI[4],"%s_ThetaL", DetectorName);
      sprintf(CalNameI[5],"%s_PhiL", DetectorName);
      // X in  [-2000,2000]
      SetGateCal(-2000,2000,0);
      // Theta  in  [-2000,2000]   
      SetGateCal(-2000,2000,1);
      // Y in [-2000,2000]
      SetGateCal(-2000,2000,2);
      // Phi in [-2000,2000]
      SetGateCal(-2000,2000,3);
      // ThetaL in [-2000,2000]
      SetGateCal(-2000,2000,4);
      // ThetaL in [-2000,2000]
      SetGateCal(-2000,2000,5);

    }
  //  ReadCalibration();


  END;
}
TargetPos::~TargetPos(void)
{
  START;
  END;
}
void TargetPos::AllocateComponents(void)
{
  START;
  
  fPresentX = false;
  fPresentY = false;
  
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = true;
 
  string BaseName;
  string Name;


  // Number of wires in X and Y direction for the two detectors
  
  UShort_t NWiresX[2] = {39,64};
  UShort_t NWiresY[2]= {60,92}; 
  // UShort_t NWiresX[2] = {96,96};
  // UShort_t NWiresY[2]= {96,96}; 
  

  for(UShort_t i=0; i<NumberOfSubDetectors; i++)
    {
      Char_t Name[20];
      sprintf(Name,"TMW%d",i+1);
      BaseDetector *ADet = new TMW(Name,NWiresX[i],NWiresY[i],RawData,CalData,DefCal,PosInfo,DetectorNameExt);
      AddComponent(ADet);
    }

//  Name.clear();
//  Name ="TACS";
//  BaseDetector *TTMW = new BaseDetector(Name.c_str(),7,true,CalData,DefCal,false,"");
//  //TTMW->SetParameterName("TMWT1_TSI",0);
//  //TTMW->SetParameterName("TMWT1_GAMMA",1);
//  //TTMW->SetParameterName("TMWT1_HF",2);
//  //TTMW->SetParameterName("TMWT1_TMWT2_D",3);
//  //TTMW->SetParameterName("TMWT1_TMWFP_D",4);
//  //TTMW->SetParameterName("TMWT2_TMWFP_D",5);
//  TTMW->SetParameterName("TMWT1_HFval",0);
//  TTMW->SetParameterName("TMWT1_TMWT2",1);
//  TTMW->SetParameterName("TDSA_TMWT1",2);
//  TTMW->SetParameterName("TMWT1_TMWFP1",3);
//  TTMW->SetParameterName("TMWT1_TMWFP2",4);
//  TTMW->SetParameterName("TDSA_TMWFP2",5);
//  TTMW->SetParameterName("TMWT2_TMWFP2",6);
//  if(RawData)
//    {
//      TTMW->SetRawHistogramsParams(4096,0,16384,"");
//    }
//  if(CalData)
//    {
//      //TTMW->SetCalName("TMWT1_TSI_C",0);
//      //TTMW->SetCalName("TMWT1_GAMMA_C",1);
//      //TTMW->SetCalName("TMWT1_HF_C",2);
//      //TTMW->SetCalName("TMWT1_TMWT2_D_C",3);
//      //TTMW->SetCalName("TMWT1_TMWFP_D_C",4);
//      //TTMW->SetCalName("TMWT2_TMWFP_D_C",5);
//      TTMW->SetCalName("TMWT1_HFval_C",0);
//      TTMW->SetCalName("TMWT1_TMWT2_C",1);
//      TTMW->SetCalName("TDSA_TMWT1_C",2);
//      TTMW->SetCalName("TMWT1_TMWFP1_C",3);
//      TTMW->SetCalName("TMWT1_TMWFP2_C",4);
//      TTMW->SetCalName("TDSA_TMWFP2_C",5);
//      TTMW->SetCalName("TMWT2_TMWFP2_C",6);
//        //TTMW->SetGateCal(0,16384);
//      TTMW->SetCalHistogramsParams(10000,0,2000,"ns","");
//    }
 
//  AddComponent(TTMW);
  
  // Setup Default reconstructed positions to be used (Hyperbolic secant)  DetList->at(i)->GetCal(VarX))
  UseWeigtedAverage(false);
  // set to true to 
  UseOneMWPlane(false);


  END;     
}

void TargetPos::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        {
          DetList->at(i)->SetParameters(Par,Map);
        }
    }  
  END;
}

void TargetPos::ReadPosition(void)
{ 
  START;
  MIFile *IF;
  char Line[255];
  stringstream *InOut;
  
  InOut = new stringstream();
  *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
  *InOut>>Line;
  InOut = CleanDelete(InOut);
  if((IF = CheckCalibration(Line)))
    {
      // Position 
      try
        {
          ReadDefaultPosition(IF);
        }
      catch(...)
        {
          IF = CleanDelete(IF);
          throw;
        }
    }

  if(VerboseLevel >= V_INFO)
    {
      cout << "<" << DetectorName << "><" << DetectorNameExt << "> Reference positions (X,Y,Z) wrt. to target " << endl;
      cout << "<" << DetectorName << "><" << DetectorNameExt << ">   X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ() << " mm" << endl;
      cout << "\n" << endl;
      
    }
    L->File << "<" << DetectorName << "><" << DetectorNameExt << "> Reference positions (X,Y,Z) wrt. to target " << endl;
    L->File << "<" << DetectorName << "><" << DetectorNameExt << ">  X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ() << " mm" << endl;

  END;
}

Bool_t TargetPos::Treat(void)
{
  START;
  IncrementCounter(0);
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        DetList->at(i)->Treat();
    }


  if(fCalData)
    {
      for(UShort_t i = 0;i < 2 ;i++)
        if(DetList->at(i)->IsPresent())
          IncrementCounter(2+i);
      
      // Require at both (X,Y) position measurement per chamber 
		
      if(DetList->at(0)->IsPresent() && DetList->at(1)->IsPresent())
        {
          // cout << "TargetPos " << GetRefY() << " " << GetRefZ() << endl;
          
          fPresentX = FocalX();
          fPresentY = FocalY();
          if(fPresentX && fPresentY) 
            isPresent = true;

          
#ifdef WITH_ROOT 
          // Has to be changed to be incorporated in Narwal actor 
          //MW target projected coordinates (Theta in 0xz and Phi in 0yz)
          Double_t Theta = GetCal(1);
          Double_t Phi = GetCal(3);
          //Zgoubi coordinates (Theta in 0xz and Phi between v particle and 0xz)
          Double_t ThetaZ = Theta;
          Double_t PhiZ = atan(tan(Phi/1000.)*cos(Theta/1000.))*1000.;
          Double_t ThetaL;
          Double_t PhiL;
          Double_t VamosAngle = FP->GetVamosAngle();
          TVector3 *myVec;
          // TVector3 *myVecy;
          myVec = new TVector3(sin(ThetaZ/1000.)*cos(PhiZ/1000.),sin(PhiZ/1000.),cos(ThetaZ/1000.)*cos(PhiZ/1000.));



          ////
          //                          ANGLE VAMOS
          ///////
          
          
          myVec->RotateY(VamosAngle*TMath::Pi()/180.); //VAMOS angle
          
          ////
          //                          ANGLE VAMOS
          ///////
          
          ThetaL = myVec->Theta();
          PhiL = myVec->Phi();

          //cout << "Theta " << Theta << " Phi " << Phi << " ThetaZ " << ThetaZ << " PhiZ " << PhiZ << " ThetaL " << ThetaL << " PhiL " << PhiL << endl;  
           
          delete myVec;
          
          SetCalData(4,ThetaL);
          SetCalData(5,PhiL);         
#endif
          



        }
      else
        {
          if(fUseOneMW)
            { 
              // If Decide to use only one out of the two !
              Float_t Theta = -1500;
              Float_t Phi = -1500;
              Float_t X = -1500;
              Float_t Y = -1500;

              for(UShort_t i=0; i<2; i++)
                if(DetList->at(i)->IsPresent())
                  {       
                    
                    Theta  = 1000.*atan(((Double_t) DetList->at(i)->GetCal(VarX)-GetRefX()) /(((TMW*) DetList->at(i))->GetRefZ_XPlane()-GetRefZ()));
                    Phi  = 1000.*atan(((Double_t) DetList->at(i)->GetCal(VarY)-GetRefY()) /(((TMW*) DetList->at(i))->GetRefZ_YPlane()-GetRefZ()));
                    X = GetRefX();
                    Y = GetRefY();
                  }
          
              if(Theta >-500. && Phi>-500.)
                {
                  SetCalData(0,X);
                  SetCalData(1,Theta);
                  SetCalData(2,Y);
                  SetCalData(3,Phi);
                  isPresent =  true;
                }
              else
                { 
                  isPresent = false;
                  SetCalData(0,-1500);
                  SetCalData(1,-1500);
                  SetCalData(2,-1500);
                  SetCalData(3,-1500);
                }
            }
          else
            {
              // Set Value to defaults
              for(UShort_t i=0;i<NumberOfDetectors;i++)
                SetCalData(i,-1500.);
            }
        }
    }
  
 
  return(isPresent); 


  END;
}

void TargetPos::UseSechsFit(Bool_t v)
{
  START;
#ifdef FITSECHIP_MW  
  if(v)
	 {
		fUseXSf = v;
		VarX = 16;
		VarY = 17;
		cout << "<" << DetectorName << "><" << DetectorNameExt << "> Using Weigted Average Pos" << endl;
		L->File << "<" << DetectorName << "><" << DetectorNameExt << "> Using Weigted Average Pos" << endl;
	 }
  else
	 {
		fUseXSf = v;
		VarX = 0;
		VarY = 1;
		cout << "<" << DetectorName << "><" << DetectorNameExt << "> Using SECHS Pos" << endl;
		L->File << "<" << DetectorName << "><" << DetectorNameExt << "> Using SECHS Pos" << endl;
	 }
  
#else
  MErr * Er = new MErr(WhoamI,0,0, "Trying to used fit sechs white #FITSECHIP_MW not defined !");
  throw Er;      
#endif

  END;
}

void TargetPos::UseOneMWPlane(Bool_t v)
{
  START;
  
  if(v)
	 {
      fUseOneMW = v;
		cout << "<" << DetectorName << "><" << DetectorNameExt << "> Using one MW Plane Opt" << endl;
		L->File << "<" << DetectorName << "><" << DetectorNameExt << "> Using one MW Plane Opt" << endl;
	 }
  else
	 {
      fUseOneMW = v;
		cout << "<" << DetectorName << "><" << DetectorNameExt << "> NOT Using one MW Plane Opt" << endl;
		L->File << "<" << DetectorName << "><" << DetectorNameExt << "> NOT Using one MW Plane Opt" << endl;
	 }
  END;
}


void TargetPos::UseWeigtedAverage(Bool_t v)
{
  START;
  
  if(v)
    {
      fUseWA = v;
      VarX = 2;
      VarY = 3;
      cout << "<" << DetectorName << "><" << DetectorNameExt << "> Using Weigted Average Pos" << endl;
      L->File << "<" << DetectorName << "><" << DetectorNameExt << "> Using Weigted Average Pos" << endl;
    }
  else
    {
      fUseWA = v;
      VarX = 0;
      VarY = 1;
      cout << "<" << DetectorName << "><" << DetectorNameExt << "> Using SECHS Pos" << endl;
      L->File << "<" << DetectorName << "><" << DetectorNameExt << "> Using SECHS Pos" << endl;
    }
  


  END;
}

Bool_t TargetPos::FocalX(void)
{ 
  START;
  fPresentX = false;
  
  UShort_t i;
  Double_t A[2];
  Double_t B[2];    
  Float_t Tf = -1500.;
  Float_t Xf = -1500.;
  
  SetMatX();
  
  for( i=0;i<2;i++)
    A[i] = B[i] = 0.0;
  
    
  for(UShort_t i=0;i<NumberOfSubDetectors; i++)
    if(DetList->at(i)->IsPresent())
      {
        A[0] += ((Double_t) DetList->at(i)->GetCal(VarX))* ((TMW*) DetList->at(i))->GetRefZ_XPlane();
        A[1] += (Double_t) DetList->at(i)->GetCal(VarX);
        // A[0] += ((Double_t) DetList->at(i)->GetCal(0))* ((TMW*) DetList->at(i))->GetRefZ_XPlane();
        // A[1] += (Double_t) DetList->at(i)->GetCal(0);
      }
  B[0] =A[0]*MatX[0][0] + A[1]*MatX[0][1]; 
  B[1] =A[0]*MatX[1][0] + A[1]*MatX[1][1];
  
  // Tf in mrad
  Tf = (Float_t) (1000.*atan(B[0]));
  Xf = (Float_t) (B[1]+B[0]*GetRefZ()); //FocalPos
  
  
  if(Xf >-500.)
    {
      SetCalData(0,Xf);
    }
  else
    SetCalData(0,-1500.);
  
  if(Tf>-500.)
    {
      SetCalData(1,Tf);
    }
  else
    SetCalData(1,-1500.);
 
  if(Xf >-500. && Tf>-500.)
    return true;
  else 
    return false;
  END;
}

Bool_t TargetPos::FocalY(void)
{ 
  START;
  UInt_t i;
  Float_t A[2];
  Float_t B[2];
  Float_t Pf = -1500.;
  Float_t Yf = -1500.;
  
  SetMatY();
  
  for(i=0;i<2;i++)
    A[i] = B[i] = 0.0;
   
  for(i=0;i<NumberOfSubDetectors;i++)
    if(DetList->at(i)->IsPresent())
      {
        A[0] += ((Double_t) DetList->at(i)->GetCal(VarY))*((TMW*) DetList->at(i))->GetRefZ_YPlane();
        A[1] += DetList->at(i)->GetCal(VarY);
        // A[0] += ((Double_t) DetList->at(i)->GetCal(1))*((TMW*) DetList->at(i))->GetRefZ_YPlane();
        // A[1] += DetList->at(i)->GetCal(1);
      }
  B[0] =A[0]*MatY[0][0] + A[1]*MatY[0][1]; 
  B[1] =A[0]*MatY[1][0] + A[1]*MatY[1][1]; 

  // Pf in mrad
  Pf = (Float_t) (1000.*atan(B[0]));
  Yf = (Float_t) (B[1]+B[0]*GetRefZ()); //FocalPos
  
  if(Yf>-500 && Yf < 500 )
    {
      SetCalData(2,Yf);
    }
  else 
    SetCalData(2,-1500.);

  if(Pf > -500. && Pf < 500) 
    {
      SetCalData(3,Pf);
    }
  else 
    SetCalData(3,-1500.);


  if(Yf >-500. && Pf>-500.)
    return true;
  else 
    return false;
  END;
}



void TargetPos::SetMatX(void)
{
  START;
  UInt_t i,j;
  Double_t A[2][2];
  Double_t Det;
  Double_t Zref;
  
  for(i=0;i<2;i++)
    for(j=0;j<2;j++)
      A[i][j] = MatX[i][j] = 0.;
  
  for(i=0;i<NumberOfSubDetectors;i++)
    if(DetList->at(i)->IsPresent())
      {
        Zref = ((TMW*) DetList->at(i))->GetRefZ_XPlane();
        A[0][0] += pow(Zref,2.);
        A[0][1] += Zref;
        A[1][1] += 1.0;
      }
  A[1][0] = A[0][1];
  
  

  Det = A[0][0]*A[1][1] - A[0][1]*A[1][0];

  if(Det == 0.0)
    {
      MErr * Er = new MErr(WhoamI,0,0, "Det == 0 !");
      throw Er;      
    }
  else
    {
      MatX[0][0] = A[1][1]/Det;
      MatX[1][1] = A[0][0]/Det;
      MatX[1][0] = -1.0*A[0][1]/Det;
      MatX[0][1] = -1.0*A[1][0]/Det;
    }
  END;
}

void TargetPos::SetMatY(void)
{
  START;
  UInt_t i,j;
  Double_t A[2][2];
  Double_t Det;
  Double_t Zref;
  
  for(i=0;i<2;i++)
    for(j=0;j<2;j++)
      A[i][j] = MatY[i][j] = 0.;
  
  for(i=0;i<NumberOfSubDetectors;i++)
    if(DetList->at(i)->IsPresent())
      {
        Zref = ((TMW*) DetList->at(i))->GetRefZ_YPlane(); 
        A[0][0] += pow(Zref,2.);
        A[0][1] += Zref;
        A[1][1] += 1.0;
      }
  A[1][0] = A[0][1];
  
  

  Det = A[0][0]*A[1][1] - A[0][1]*A[1][0];

  if(Det == 0.0)
    {
      MErr * Er = new MErr(WhoamI,0,0, "Det == 0 !");
      throw Er;      
    }
  else
    {
      MatY[0][0] = A[1][1]/Det;
      MatY[1][1] = A[0][0]/Det;
      MatY[1][0] = -1.0*A[0][1]/Det;
      MatY[0][1] = -1.0*A[1][0]/Det;
    }
  END;
}


#ifdef WITH_ROOT
void TargetPos::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
		
  // XS
  AddHistoCal(CalNameI[0],CalNameI[0],"XS (mm)",1000,-50,50);
  // YS
  AddHistoCal(CalNameI[2],CalNameI[2],"YS (mm)",1000,-50,50);
  // XWA
  AddHistoCal(CalNameI[1],CalNameI[1],"Theta (mrad)",1000,-300,300);
  // YWA
  AddHistoCal(CalNameI[3],CalNameI[3],"Phi (mrad)",1000,-400,400);
 
  END;
}

void TargetPos::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }

  AddHistoCal(CalNameI[0],CalNameI[2],"XT_YT_TP","X_vs_Y (mm)",600,-20,20,600,-20,20);
  AddHistoCal(CalNameI[1],CalNameI[3],"ThT_PhT","ThT_vs_PhiT (mrad)",600,-200,200,600,-200,200);
  
  END;
}


void TargetPos::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

 
  if(isComposite)
    for(UShort_t i=0; i< 2; i++)
      {
  	DetList->at(i)->SetOpt(OutTTree,InTTree);
     if(VerboseLevel >= V_INFO)
         DetList->at(i)->PrintOptions(cout);
  	///		  DetList->at(i)->PrintOptions(L->File);
      }
  
                                                          
  SetMainHistogramFolder("TargetPos");

#ifdef WITH_ROOT
  // Setup Histogram Hierarchy
  SetHistogramsCal1DFolder("");
  SetHistogramsCal2DFolder("");
#endif

  if(fMode == MODE_WATCHER)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(true);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
      for(UShort_t i = 0;i<DetList->size();i++)
	{
          if(DetList->at(i)->HasRawData())
	    {
	      if(i == 2) // Times
		DetList->at(i)->SetHistogramsRaw1D(true);
	    }
	  if(DetList->at(i)->HasCalData())
	    {

              switch (i)
		{
		case 2: // Times 
		  DetList->at(i)->SetHistogramsCal1D(true);
		  break;
		default: 
		  break;
		}		
	    }
	}
    }
  else if(fMode == MODE_D2R)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
              if(i == 2)  // Times
                {
                  DetList->at(i)->SetHistogramsRaw1D(true);
                  DetList->at(i)->SetOutAttachRawI(true);
          	  DetList->at(i)->OutAttach(OutTTree);
		}
	    }
	  
	}
      OutAttach(OutTTree);
		
    }
  else if(fMode == MODE_D2A)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(false);
	  SetOutAttachRawI(false);
	  SetOutAttachRawV(false);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	  SetOutAttachCalV(false);
	}
		
      for(UShort_t i = 0;i<DetList->size();i++)
	{			
	  if(DetList->at(i)->HasRawData())
	    {
	      if(i == 2)  
		{
		  DetList->at(i)->SetHistogramsRaw1D(false);
		  DetList->at(i)->SetHistogramsRaw2D(true);
		  DetList->at(i)->SetOutAttachRawI(true);
		  DetList->at(i)->SetOutAttachRawV(false);			 
		}
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      switch (i)
		{
		case 2: // Times
		  DetList->at(i)->SetHistogramsCal1D(true);
		  DetList->at(i)->SetOutAttachCalV(false);
		  DetList->at(i)->SetOutAttachCalI(true);
		  DetList->at(i)->OutAttach(OutTTree);
		  
		  break;		    
		default: 		
		  break;
		}
	    }
	}
		
      OutAttach(OutTTree);
    }
  else if(fMode == MODE_R2A)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(i == 2) 
	    {
              // DetList->at(i)->SetInAttachRawV(true);
              
              DetList->at(i)->InAttach(InTTree);
            }
	}
		
      if(fRawData)
	{
	  SetHistogramsRaw(false);
	  SetOutAttachRawI(false);
	  SetOutAttachRawV(false);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	  SetOutAttachCalV(false);
	}
      
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasCalData())
	    {
              if(i == 2)
                {
                  DetList->at(i)->SetHistogramsCal1D(false);
                  DetList->at(i)->SetHistogramsCalSummary(true);
                  DetList->at(i)->SetOutAttachCalV(true);			 
                  DetList->at(i)->OutAttach(OutTTree);
                }
	    }
          
	}
      OutAttach(OutTTree);		
    }
  else if(fMode == MODE_RECAL)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(i == 2)
	    DetList->at(i)->SetInAttachCalI(true);
	 
	  DetList->at(i)->InAttachCal(InTTree);
	 
	}
      SetInAttachCalI(true);
      InAttachCal(InTTree);
      
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasCalData())
	    {
	      switch (i)
		{
		case 2: // Times
		  DetList->at(i)->SetHistogramsCal1D(true);
		  DetList->at(i)->SetOutAttachCalI(true);
		  break;		    
		default: 
		  break;
		}
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}

    }
  else if(fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }

  if(VerboseLevel >= V_INFO)
    PrintOptions(cout)  ;


  END;
}

#endif
