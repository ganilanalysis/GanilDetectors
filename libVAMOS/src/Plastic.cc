/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "Plastic.hh"

Plastic::Plastic(const Char_t *Name,
                 UShort_t NDetectors, 
                 Bool_t RawData, 
                 Bool_t CalData, 
                 Bool_t DefaultCalibration, 
                 const Char_t *NameExt
                 )
  : BaseDetector(Name, 6,false, CalData, DefaultCalibration,false,NameExt)
{
  START;  
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;

  AllocateComponents(); 
  
  AddCounter("PLG"); // 2
  AddCounter("PLD"); // 3
  
  if(fCalData)
    {
      sprintf(CalNameI[0],"QPLG_c");
      SetGateCal(0,30000,0);
      sprintf(CalNameI[1],"QPLD_c");
      SetGateCal(0,30000,1);
      sprintf(CalNameI[2],"PL_Q");
      SetGateCal(0,30000,2);
      sprintf(CalNameI[3],"PL_X");
      SetGateCal(0,30000,3);
      sprintf(CalNameI[4],"PL_QCorr");
      SetGateCal(0,30000,4);
      sprintf(CalNameI[5],"PL_QT");
      SetGateCal(0,30000,5);
    }

#ifdef WITH_ROOT
  // Setup Histogram Hierarchy
  SetHistogramsCal1DFolder("Pl1D");
  SetHistogramsCal2DFolder("Pl2D");
#endif

  END;
}
Plastic::~Plastic(void)
{
  START;
  END;
}

Float_t Plastic::GetTime(void)
{
  START;
  return Cal[5];
  END;
}

Float_t Plastic::GetEnergy(void)
{
  START;
  return Cal[4];
  END;
}

void Plastic::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;
  
  // Plastic Charges
  BaseDetector *PLQ = new BaseDetector("PLQ",2,RawData,CalData,DefCal,PosInfo,"");
  PLQ->SetParameterName("QPL_G",0);
  PLQ->SetParameterName("QPL_D",1);
  // PLQ->SetParameterName("QPLAST_VAMOS",2);
  // PLQ->SetParameterName("QFINGER",3);
  PLQ->SetRawHistogramsParams(1000,0,16383,"");
  
  if(CalData)
	 {
		PLQ->SetCalName("QPLG_C",0);
		PLQ->SetCalName("QPLD_C",1);
	 }
  AddComponent(PLQ);
	
  //Plastic Times
  BaseDetector *PLT = new BaseDetector("PLT",4,RawData,CalData,DefCal,PosInfo,"");
  PLT->SetParameterName("TPLG_TPLD",0);
  PLT->SetParameterName("THF_PLG",1);
  PLT->SetParameterName("THFP_PARIS",2);
  PLT->SetParameterName("THFP_GAMMA",3);
  //PLT->SetParameterName("THFP_PARIS",4);
  //PLT->SetParameterName("TPLD_HF",5);
  // PLT->SetParameterName("T_ORPL_GAMMA",6);
  PLT->SetCalHistogramsParams(8000,0,2000,"","");  





  if(CalData)
    {
      PLT->SetCalName("T_PLG_PLD_C",0);
      PLT->SetCalName("T_HF_PLG_C",1);
      PLT->SetCalName("T_HFP_PARIS_C",2);
      PLT->SetCalName("T_HFP_GAMMA_C",3);
      //PLT->SetCalName("TRF_PLG_PARIS_C",4);
      // PLT->SetCalName("T_PLG_HFP_C",5);
      // PLT->SetCalName("T_ORPL_GAMMA_C",6);
    }
  PLT->SetCalHistogramsParams(1000,0,2000,"ns");
  AddComponent(PLT); 


  END;
}

void Plastic::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
	DetList->at(i)->SetParameters(Par,Map);
    }
  END;
}

Bool_t Plastic::Treat(void)
{ 
  START;
  Float_t Q,Q1,Q2,Q1R,Q2R,X,T; 
  Float_t QC=0;
  Float_t QCorr=0;
  Float_t CorF[5];
  CorF[0] = 2.18269e+06;
  CorF[1] = -1009.49;
  CorF[2] = 0.175154;
  CorF[3] = -1.35115e-05;
  CorF[4] = 3.90977e-10;

  Ctr->at(0)++;
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
	{
	  DetList->at(i)->Treat();
	}
    }
  if(fCalData)
    {
      Q = 0;
      Q1 = 0;
      Q2 = 0;
      Q1R = 0;
      Q2R = 0;
      X = 0;
      QC=0;

      // 
      if(DetList->at(0)->IsPresent())
	{
	  Ctr->at(2)++;
	  isPresent = true;
	}
      
      if(DetList->at(1)->IsPresent())
	{
	  Ctr->at(3)++;
	  isPresent = true;
	}
      
      if(DetList->at(0)->IsPresent() && DetList->at(1)->IsPresent())
	{
	  if(DetList->at(0)->GetM()>1)
	    {
	      Q1 = DetList->at(0)->GetCal(0);
	      Q2 = DetList->at(0)->GetCal(1);	  
	      Q1R = DetList->at(0)->GetRaw(0);
	      Q2R = DetList->at(0)->GetRaw(1);	  
	    }
	  else 
	    {
	      Q1 = -100;
	      Q2 = -100;
	    }

	  

	  SetCalData(0,Q1);
	  SetCalData(1,Q2);
	  // Plastic Energy 
	  Q = sqrt(Q1R*Q2R);

	  // Correction from X , Using raw TPLG_PLD
	  if( DetList->at(1)->GetRaw(0) > 0)
	    {
	      UShort_t T = DetList->at(1)->GetRaw(0);
	      
	      for(UShort_t j=0;j<5;j++)
		{
		  QC += powf((Float_t) T,
			     (Float_t) j)*CorF[j];
		}
	      // Normalization
	      QCorr = (Q-QC)*274./7390.*0.843388;
	      SetCalData(4,QCorr);
	  
	    }
	  else
	    SetCalData(4,-100);


	  X = log(Q1/Q2);
	  SetCalData(2,Q);
	  // Position
	  SetCalData(3,X);
	  // Time  BTD1
	  T = DetList->at(1)->GetCal(2);
	  // Correct for Position of Plastic
	  //	  t1->SetAlias("mT1","(1.*T_PLG_BTD1+(mTPLG-13)*0.5)*(T_PLG_BTD1>20)");
	  // t1->SetAlias("mT2","(-1.*T_PLG_BTD2+280)*(T_PLG_BTD2>20)");
  	  T += (DetList->at(1)->GetCal(0)-11.5)*0.5;
	  SetCalData(5,T);


	  isPresent = true;
	}
      
    }
  return(isPresent); 
  END;
}



#ifdef WITH_ROOT
void Plastic::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  for(UShort_t i = 0;i<DetList->size();i++)
    {
      DetList->at(i)->SetMainHistogramFolder("");
      DetList->at(i)->SetHistogramsRaw1DFolder("Pl1D");
      DetList->at(i)->SetHistogramsRaw2DFolder("Pl2D");
      DetList->at(i)->SetHistogramsCal1DFolder("Pl1D");
      DetList->at(i)->SetHistogramsCal2DFolder("Pl2D");
    }
 
  if(fMode == MODE_WATCHER)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(true);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw1D(true);
	      DetList->at(i)->SetHistogramsRaw2D(true);
	      DetList->at(i)->SetHistogramsRawSummary(false);
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal1D(true);
	      DetList->at(i)->SetHistogramsCal2D(true);
	      DetList->at(i)->SetHistogramsCalSummary(false);
	    }
	}
    }
  else if(fMode == MODE_D2R)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw2D(true);
				  DetList->at(i)->SetHistogramsRawSummary(false);
				  DetList->at(i)->SetOutAttachRawI(true);
				  DetList->at(i)->SetOutAttachRawV(false);
				}
				  DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
	 }
  else if(fMode == MODE_D2A)
	 {
		if(fRawData)
		  {
			 SetHistogramsRaw(false);
			 SetOutAttachRawI(false);
			 SetOutAttachRawV(false);
		  }
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
			 SetOutAttachCalV(false);
		  }
		
		for(UShort_t i = 0;i<DetList->size();i++)
		  {			
			 if(DetList->at(i)->HasCalData())
				{
				  DetList->at(i)->SetHistogramsCal1D(true);
				  DetList->at(i)->SetHistogramsCal2D(true);
				  DetList->at(i)->SetHistogramsCalSummary(false);
				  DetList->at(i)->SetOutAttachCalI(true);
				  DetList->at(i)->SetOutAttachCalV(false);			 
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
      SetInAttachRawI(true);
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 DetList->at(i)->SetInAttachRawI(true);
			 DetList->at(i)->InAttach(InTTree);
		  }
		InAttach(InTTree);
		
		SetOutAttachCalI(true);

		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 if(DetList->at(i)->HasCalData())
				{
				  DetList->at(i)->SetHistogramsCal1D(true);
				  DetList->at(i)->SetHistogramsCal2D(true);
				  DetList->at(i)->SetHistogramsCalSummary(false);
				  DetList->at(i)->SetOutAttachCalI(true);
				  DetList->at(i)->SetOutAttachCalV(false);
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}

void Plastic::CreateHistogramsRaw2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsRaw2D(Dir);
    
  // Dir->cd("");
  
  // if(SubFolderHistRaw2D.size()>0)
  // 	 {
  // 		Name.clear();
  // 		Name = SubFolderHistRaw2D ;
  // 		if(!(gDirectory->GetDirectory(Name.c_str())))
  // 		  gDirectory->mkdir(Name.c_str());		 
  // 		gDirectory->cd(Name.c_str());
  // 	 }
  


  END;
}
void Plastic::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }

  AddHistoCal(GetCalName(0),GetCalName(1),"QPLG_QPLD","QPLG vs QPLD",1000,0,16384,1000,0,16384);		
  END;
}

#endif
