/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "MWPPAC.hh"

MWPPAC::MWPPAC(const Char_t *Name,
               UShort_t NDetectors, 
               Bool_t RawData, 
               Bool_t CalData, 
               Bool_t DefaultCalibration, 
               const Char_t *NameExt,
               UShort_t MaxMult
               )
: BaseDetector(Name, 4, false, CalData, DefaultCalibration,false,NameExt,false,false,MaxMult)
{
  START;
  
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  fMaxMultSubDetectors = MaxMult;
  FP = NULL;
  DetManager *DM = DetManager::getInstance();
  FP = (FocalPosition*) DM->GetDetector("FP");
  isSecondArm = false;
  isFPMW = false;
  AllocateComponents(); 
  
  
  // Counter
  AddCounter("Recovered Tracks:"); //[2]
  AddCounter("InValid Tracks:"); //[3]
      
  END;
}
MWPPAC::~MWPPAC(void)
{
  START;
  END;
}

void MWPPAC::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = true;
  Char_t Name[100];
  Char_t TName[100];
  if(strcmp(DetectorName, "MWH") == 0)
    {
      sprintf(Name,"MWH");
      sprintf(TName,"TCoeffH");
      isSecondArm = true;
      PosInfo = true;
    }
  if(strcmp(DetectorName, "FPMW") == 0)
    {
      sprintf(Name,"FPMW");
      sprintf(TName,"TCoeffFPMW");
      isFPMW = true;
      PosInfo = true;
      // Overwrite cal names
      if(fCalData)
        {
          sprintf(CalNameI[0],"FPMW_Nr");
          sprintf(CalNameI[1],"FPMW_MW_T");
          sprintf(CalNameI[2],"FPMW_XMW");
          sprintf(CalNameI[3],"FPMW_XNoMW");
        }
    }
  else
    {
      sprintf(Name,"MW");
      sprintf(TName,"TCoeff");
      isSecondArm = false;
      PosInfo = true;
      if(fCalData)
	{
	  sprintf(CalNameI[0],"MW_Nr");
	  sprintf(CalNameI[1],"MW_T");
	  sprintf(CalNameI[2],"XMW");
	  sprintf(CalNameI[3],"XNoMW");
	}
    }

  BaseDetector *MWT = new BaseDetector(Name,NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"T");

  if(RawData)
    MWT->SetParameterName("%s_%02d");
#ifdef WITH_ROOT
  MWT->SetCalHistogramsParams(1000,0,1000,"ns");
  MWT->SetRawHistogramsParams(16384,0,16383,"");
#endif
  AddComponent(MWT);


  BaseDetector *TCoeff = new BaseDetector(TName,NumberOfSubDetectors,true,true,true,false,"");  
  cout << " TCoeff " << TCoeff << endl;
  AddComponent(TCoeff);


  END;
}


void MWPPAC::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  DetList->at(0)->SetParameters(Par,Map);
  END;
}


Bool_t MWPPAC::Treat(void)
{ 
  START;
  Ctr->at(0)++;
  
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        {
          DetList->at(i)->Treat();
        }
    }
		  
  if(FP)
    {
      if (FP->IsPresent()) 
        {
	  //if(!isFPMW)
	  //  {
	      for(UShort_t i=0; i< DetList->at(0)->GetM(); i++)
		{
              
	     
		  hasValidTrack = DetList->at(0)->CheckTrack(FP->GetXf(),
							     FP->GetTf(),
							     FP->GetYf(),
							     FP->GetPf(),
							     FP->GetRefZ(),
							     DetList->at(0)->GetNrAt(i));
              
              
		  if(hasValidTrack)
		    {
		      // Detector Number
		      SetCalData(0,DetList->at(0)->GetNrAt(i));
		      // Detector Calibrated Energy
		      SetCalData(1,DetList->at(0)->GetCalAt(i));
		      // Xf Pattern 
		      SetCalData(2,FP->GetXf());
		      SetCalData(3,-1500);
                  
		      isPresent = true;
		      break; 
		    }
                            
              
		}
//	    }
//	  else
//	    {
//	      if(DetList->at(0)->GetM() == 1)
//		{
//		  // Detector Number
//		  SetCalData(0,DetList->at(0)->GetNrAt(0));
//		  // Detector Calibrated Energy
//		  SetCalData(1,DetList->at(0)->GetCalAt(0));
//		  // Xf Pattern 
//		  SetCalData(2,FP->GetXf());
//		  SetCalData(3,-1500);
//                  
//		  isPresent = true;
//		  
//		}
//	      else
//		{
//
//		}
//	    }
          
        }
    }
  

   if(fCalData && !hasValidTrack)
    {
      // Second Chance, Check From spatial correlation if we match a MW_Nr 
      if(FP)
        {
          if(FP->IsPresent())
            {                                         
          
              for(UShort_t i =0; i < NumberOfSubDetectors; i++)
                {
              
                  hasValidTrack = DetList->at(0)->CheckTrack(FP->GetXf(),
                                                             FP->GetTf(),
                                                             FP->GetYf(),
                                                             FP->GetPf(),
                                                             FP->GetRefZ(),
                                                             i);
              
                  if(hasValidTrack)
                    {
                      Ctr->at(2)++;
                // Detector Number
                      SetCalData(0,i);
                      // Detector Calibrated Time, no time in this case
                      SetCalData(1,-2000);
                      // Xf Pattern 
                      SetCalData(2,FP->GetXf());
                      SetCalData(3,-1500);
                
                      isPresent = true;
                      break; 
                      // cout << "Found ! "<< i << endl;
                
                    }
            
                }
            }
        }
      if(!hasValidTrack)
        {
          Ctr->at(3)++;
          SetCalData(0,-1);
          SetCalData(1,-1500);
          SetCalData(2,-1500);
          SetCalData(3,FP->GetXf());
          // SetCalData(4,-1500);
        }
    }
      
#ifdef DEBUG
  PrintRaw();
  PrintCal();   
#endif
  return(isPresent); 
  END;
}

#ifdef WITH_ROOT
void MWPPAC::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  for(UShort_t i = 0;i<DetList->size();i++)
	 {
      DetList->at(i)->SetMainHistogramFolder("");
		DetList->at(i)->SetHistogramsRaw1DFolder("MW_R");
		DetList->at(i)->SetHistogramsRaw2DFolder("MW_R");
//		if(i == 0)
//		  {
//			 DetList->at(i)->SetHistogramsCal1DFolder("MW_C_T");
//			 DetList->at(i)->SetHistogramsCal2DFolder("MW_C_T");
//		  }
//		else
//		  {
             DetList->at(i)->SetHistogramsCal1DFolder("MW_C");
			 DetList->at(i)->SetHistogramsCal2DFolder("MW_C");
//		  }
	 	SetHistogramsCal1DFolder("MW_C");
		SetHistogramsCal2DFolder("MW_C");	 
	 }
  if(fMode == MODE_WATCHER)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(true);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw(true);
	      if(i == 0) DetList->at(i)->SetHistogramsRawSummary(true);
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal(true);
	      if(i == 0) DetList->at(i)->SetHistogramsCalSummary(true);
	    }
      if(i == 1)
	    {
	      DetList->at(i)->SetNoOutput();
	    }
	}
    }
  else if(fMode == MODE_D2R)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw(true);
                  if(i == 0 || i == 2)
					 {
						DetList->at(i)->SetHistogramsRawSummary(true);
						DetList->at(i)->SetOutAttachRawV(true);
						DetList->at(i)->SetOutAttachRawI(false);
					 }
				  else
					 {			
						DetList->at(i)->SetOutAttachRawI(true);
                
					 }
				}
         if(i == 1)
           {
             DetList->at(i)->SetNoOutput();
           }
			
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
	 }
  else if(fMode == MODE_D2A)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(false);
	  SetOutAttachRawI(false);
	  SetOutAttachRawV(false);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	  SetOutAttachCalV(false);
	}
		
      for(UShort_t i = 0;i<DetList->size();i++)
	{			
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw1D(true);
	      DetList->at(i)->SetHistogramsRaw2D(true);
				  
          if(i == 0|| i == 2)
		{
		  DetList->at(i)->SetHistogramsRawSummary(true);
		  DetList->at(i)->SetOutAttachRawV(false);
		  DetList->at(i)->SetOutAttachRawI(false);			 					  
					  
		}
	      else
		{
		  DetList->at(i)->SetOutAttachRawI(false);
                   
		}
                  
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal1D(true);
	      DetList->at(i)->SetHistogramsCal2D(true);
          if(i == 0|| i == 2)
		{
		  DetList->at(i)->SetHistogramsCalSummary(true);
		  DetList->at(i)->SetOutAttachCalI(false);
		  DetList->at(i)->SetOutAttachCalV(true);			 
		}
	      else
		{
		  DetList->at(i)->SetHistogramsCalSummary(false);
		  DetList->at(i)->SetOutAttachCalI(true);
		}
	    }
      if(i == 1)  // TCoeffs
            {
              DetList->at(i)->SetNoOutput();
            }
          
	  DetList->at(i)->OutAttach(OutTTree);
	}
		
      OutAttach(OutTTree);
    }
  else if(fMode == MODE_R2A)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
      if(i == 0|| i == 2)
	    DetList->at(i)->SetInAttachRawV(true);
	  else
	    DetList->at(i)->SetInAttachRawI(true);
	  DetList->at(i)->InAttach(InTTree);
	}
		
      SetOutAttachCalI(true);

      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw1D(true);
          if(i == 0|| i == 2)
		DetList->at(i)->SetHistogramsRawSummary(true);
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal1D(true);
	      DetList->at(i)->SetHistogramsCal2D(true);
          if(i == 0|| i == 2)
		{
		  DetList->at(i)->SetHistogramsCalSummary(true);
		  DetList->at(i)->SetOutAttachCalI(false);
		  DetList->at(i)->SetOutAttachCalV(true);			 
		}
	      else
		{
		  DetList->at(i)->SetHistogramsCalSummary(false);
		  DetList->at(i)->SetOutAttachCalI(true);
		  DetList->at(i)->SetOutAttachRawI(false);
		}
	    }
      if(i == 1)
	    {
	      DetList->at(i)->SetNoOutput();
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}
      OutAttach(OutTTree);
		
    }
  else if (fMode == MODE_RECAL)
    {
      
      SetInAttachCalI(true);
      for(UShort_t i = 0;i<DetList->size();i++)
	{
      if(i == 0|| i == 2)
	    DetList->at(i)->SetInAttachCalV(true);
	  else
	    DetList->at(i)->SetInAttachCalI(true);
	  DetList->at(i)->InAttachCal(InTTree);
	}
      InAttachCal(InTTree);
      
      SetOutAttachCalI(true);
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	
      if(i == 1)  // TCoeffs
            {
              DetList->at(i)->SetNoOutput();
            }
          
	  DetList->at(i)->OutAttach(OutTTree);
	}
      OutAttach(OutTTree);
      
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
    {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
    }
  END;

}
#endif
