/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Diego Ramos, diego.ramos@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  trueyou are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "nDetector.hh"

nDetector::nDetector(const Char_t *Name,
         UShort_t NDetectors,
         Bool_t RawData,
         Bool_t CalData,
         Bool_t DefaultCalibration,
         const Char_t *NameExt
         )
    : BaseDetector(Name, 3, false, CalData, DefaultCalibration,false,NameExt)
{
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;
 //   DetManager *DM = DetManager::getInstance();
    AllocateComponents();
    if(fCalData)
      {
	sprintf(CalNameI[0],"%s_A_Cc", DetectorName);
	sprintf(CalNameI[1],"%s_ZC_Cc", DetectorName);
	sprintf(CalNameI[2],"%s_TOF_Cc", DetectorName);
      }
	END;
}
nDetector::~nDetector(void)
{
    START;
    END;

}




void nDetector::AllocateComponents(void)
{
    START;
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Bool_t PosInfo = false;

    Char_t dname[300];
    Int_t MaxMult = 1;
    // Silicon
    sprintf(dname,"%s_A",DetectorName);
    BaseDetector *Amplitude = new BaseDetector(dname,1,RawData,CalData,DefCal,PosInfo,"",0,1,MaxMult,1);
    Amplitude->SetRawHistogramsParams(64000,0,63999);
    Amplitude->SetGateRaw(0,63999);
    if(fCalData)
      {
        sprintf(dname,"%s_A_C",DetectorName);
        Amplitude->SetCalName(dname,0);
      }
    AddComponent(Amplitude);
    // Silicon
    sprintf(dname,"%s_ZC",DetectorName);
    BaseDetector *ZCdet = new BaseDetector(dname,1,RawData,CalData,DefCal,PosInfo,"",0,1,MaxMult,1);
    ZCdet->SetRawHistogramsParams(64000,0,63999);
    ZCdet->SetGateRaw(0,63999);
    if(fCalData)
      {
	sprintf(dname,"%s_ZC_C",DetectorName);
        ZCdet->SetCalName(dname,0);
      }
    AddComponent(ZCdet);

    sprintf(dname,"%s_TOF",DetectorName);
    BaseDetector *ToF = new BaseDetector(dname,1,RawData,CalData,DefCal,PosInfo,"",0,1,MaxMult);
    ToF->SetRawHistogramsParams(64000,0,63999);
    ToF->SetGateRaw(0,63999);
    if(fCalData)
      {
        sprintf(dname,"%s_TOF_C",DetectorName);
        ToF->SetCalName(dname,0);
      }
    AddComponent(ToF);

    END;
}

void nDetector::SetParameters(NUMEXOParameters* Par, Map* Map)
{
    START;

    if(isComposite)
    {
        Char_t PName[300];
        // Silicon de1
        sprintf(PName,"%s_A",DetectorName);
        DetList->at(0)->SetParameterName(PName,0);
        DetList->at(0)->SetRawHistogramsParams(64000,0,63999);
        DetList->at(0)->SetGateRaw(0,63999);

        // Silicon de2
        sprintf(PName,"%s_ZC",DetectorName);
        DetList->at(1)->SetParameterName(PName,0);
	DetList->at(1)->SetRawHistogramsParams(64000,0,63999);
        DetList->at(1)->SetGateRaw(0,63999);
	
        // CsI
        sprintf(PName,"%s_TOF",DetectorName);
        DetList->at(2)->SetParameterName(PName,0);
	DetList->at(2)->SetRawHistogramsParams(64000,0,63999);
        DetList->at(2)->SetGateRaw(0,63999);

    }

    NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
    for (UShort_t i = 0; i < DetList->size(); i++)
        DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);

    END;
}

Bool_t nDetector::Treat(void)
{
    START;
    Ctr->at(0)++;

    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->Treat();
        }
        SetCalData(0,DetList->at(0)->GetCal(0));
        SetCalData(1,DetList->at(1)->GetCal(0));
        SetCalData(2,DetList->at(2)->GetCal(0));
    }

    return(isPresent);
    END;
}
 
#ifdef WITH_ROOT


void nDetector::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }
  Char_t PName[400];
  sprintf(PName,"%s_ZC_A",DetectorName);
  AddHistoCal(GetCalName(1),GetCalName(0),PName,PName,1000,0,60000,1000,0,60000);
  sprintf(PName,"%s_TOF_A",DetectorName);
  AddHistoCal(GetCalName(2),GetCalName(0),PName,PName,1000,0,60000,1000,0,60000);
  
  END;

}



void nDetector::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;


    // Set histogram Hierarchy
    for(UShort_t i = 0;i<DetList->size();i++)
    {
        DetList->at(i)->SetMainHistogramFolder("");
    }

    SetHistogramsCal2DFolder("nDET_2D");

    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(true);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
            }
        }
    }
    else if(fMode == MODE_D2R || fMode == MODE_D2A)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
            SetOutAttachRawI(true);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(true);
                DetList->at(i)->SetHistogramsRawSummary(false);

                DetList->at(i)->SetOutAttachRawV(false);
                //DetList->at(i)->SetOutAttachRawMult(false);
                DetList->at(i)->SetOutAttachRawI(true);
                DetList->at(i)->SetOutAttachTS(true);
                if(DetList->at(i)->HasTime())
                    DetList->at(i)->SetOutAttachTime(true);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
	        DetList->at(i)->SetOutAttachCalI(true);

            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}




#endif
