/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "TMW.hh"
TMW::TMW(const Char_t *Name, 
         UShort_t NX, 
         UShort_t NY, 
         Bool_t RawData, 
         Bool_t CalData, 
         Bool_t DefaultCalibration, 
         Bool_t PosInfo, 
         const Char_t *NameExt,
         Bool_t HasTimeStamp)
  : BaseDetector(Name, 6 , false, CalData, DefaultCalibration,PosInfo, NameExt, HasTimeStamp)
{
  START;
  NWiresX = NX;
  NWiresY = NY;
  NumberOfSubDetectors = NWiresX+NWiresY;
  fRawDataSubDetectors  = RawData;
  
  SetId(Name);
  SetGapSize(0.); // 2.4mm gap

#ifdef FITSECHIP_MW
#ifdef WITH_ROOT
  f_SECH = NULL;
  QGraph = NULL;  
  h_Q = NULL;
#endif
#endif
  AllocateComponents();

  AddCounter("Present X"); //2
  AddCounter("Present X-SECHS"); //3
  AddCounter("Present X-WA"); //4
  AddCounter("Saturated X"); //5
  AddCounter("Present Y"); //6
  AddCounter("Present Y-SECHS"); //7
  AddCounter("Present Y-WA"); //8
  AddCounter("Saturated Y"); //9
  

  // Scanf of ID
  if(fCalData)
    {
      sprintf(CalNameI[0],"%s_X", DetectorName);
      sprintf(CalNameI[1],"%s_Y", DetectorName);
      sprintf(CalNameI[2],"%s_XWA", DetectorName);
      sprintf(CalNameI[3],"%s_YWA", DetectorName);
      sprintf(CalNameI[4],"%s_MX_f", DetectorName);
      sprintf(CalNameI[5],"%s_MY_f", DetectorName);
//      sprintf(CalNameI[14],"%s_MX_f", DetectorName);
//      sprintf(CalNameI[15],"%s_MY_f", DetectorName);


      //sprintf(CalNameI[4],"%s_QX", DetectorName);
      //sprintf(CalNameI[5],"%s_QY", DetectorName);
      //sprintf(CalNameI[6],"%s_MaxQX", DetectorName);
      //sprintf(CalNameI[7],"%s_MaxQY", DetectorName);
      //sprintf(CalNameI[8],"%s_MaxNrX", DetectorName);
      //sprintf(CalNameI[9],"%s_MaxNrY", DetectorName);
      //sprintf(CalNameI[10],"%s_QRX", DetectorName);
      //sprintf(CalNameI[11],"%s_QRY", DetectorName);
      //sprintf(CalNameI[12],"%s_MX", DetectorName);
      //sprintf(CalNameI[13],"%s_MY", DetectorName);

      //sprintf(CalNameI[16],"%s_XSf", DetectorName);
      //sprintf(CalNameI[17],"%s_YSf", DetectorName);


      // sprintf(CalNameI[16],"%s_XRa", DetectorName);
      // sprintf(CalNameI[17],"%s_YRa", DetectorName);
      // sprintf(CalNameI[18],"%s_XQLM", DetectorName);
      // sprintf(CalNameI[19],"%s_YQLM", DetectorName);
      // sprintf(CalNameI[20],"%s_XQRM", DetectorName);
      // sprintf(CalNameI[21],"%s_YQRM", DetectorName);
      // X in  [-1500, 0]
      SetGateCal(-2000,2000,0);
      // Y in [0,500]
      SetGateCal(-2000,2000,1);
      //  XWA  in  [-1500, 0]
      SetGateCal(-2000,2000,2);
      // YWA in [0,500]
      SetGateCal(-2000,2000,3);

      SetGateCal(0,64,4); //13
      // MX(free)
      SetGateCal(0,64,5); //14
      // MY(free)


      // QX in [0,500]
      //SetGateCal(-100,10000,4);
      // QY in [0,500]
      //SetGateCal(-100,10000,5);    
      // PadMaxQX in [0,500]
      //SetGateCal(-100,10000,6);
      // PadMaxQY in [0,500]
      //SetGateCal(-100,10000,7);
      // PadMaxQNrX 
      //SetGateCal(0,64,8);
      // PadMaxQNrY 
      //SetGateCal(0,93,9);
      // QMax/Q X
      //SetGateCal(-2,1,10);
      // QMax/Q Y
      //SetGateCal(-2,1,11);
      // MX
      //SetGateCal(0,64,12);
      // MY

      //SetGateCal(0,64,15);
      // XSf in  [-1500, 0]
      //SetGateCal(-2000,2000,16);
      // YSf in [0,500]
      //SetGateCal(-2000,2000,17);
   
    }
  ReadCalibration();
  ReadPosition();

#ifdef WITH_ROOT
  // Setup Histogram Hierarchy
//  SetHistogramsCal1DFolder("TMW1D");
//  SetHistogramsCal2DFolder("TMW2D");
#endif

  END;
}
TMW::~TMW(void)
{
  START;
  
#ifdef FITSECHIP_MW
#ifdef WITH_ROOT
  delete f_SECH;
  f_SECH=NULL;
  delete QGraph;
  QGraph = NULL;  
  delete h_Q;
  h_Q = NULL;  
#endif
#endif

  END;
}


void TMW::ReadCalibration(void)
{
  START;
  MIFile *IF;
  char Line[255];
  stringstream *InOut;
  
  for(UShort_t n =0; n<2; n++)
    {
      InOut = new stringstream();
      *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << DetList->at(n)->GetName() << ".cal";
      *InOut>>Line;
      InOut = CleanDelete(InOut);

      if((IF = DetList->at(n)->CheckCalibration(Line)))
        {
          
         try
            {
              DetList->at(n)->ReadCalibration(IF);
            }
          catch(...)
            {
              IF = CleanDelete(IF);
              throw;
            }
          IF = CleanDelete(IF);
        }
      else
        {
          DetList->at(n)->GenerateDefaultCalibration(Line);
        }
    }

  END;
}


void TMW::ReadPosition(void)
{ 
  START;
  MIFile *IF;
  char Line[255];
  stringstream *InOut;
  ZRef[0] = ZRef[1] = 0.;
  InOut = new stringstream();
  *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
  *InOut>>Line;
  InOut = CleanDelete(InOut);
  if((IF = CheckCalibration(Line)))
    {
      // Position 
      try
        {
          ReadDefaultPosition(IF);
        }
      catch(...)
        {
          IF = CleanDelete(IF);
          throw;
        }
    }
  
  // Position of X and Y planes wrt time plane specified in Reference file (GetRefZ()) 
  if(TMWId == 0) // Y first X second
    {
      ZRef[0] = GetRefZ() + GapSize;
      ZRef[1] = GetRefZ() - GapSize;
    }
   else if(TMWId == 1) // Y first X second
    { 
      ZRef[0] = GetRefZ() + GapSize;
      ZRef[1] = GetRefZ() - GapSize;
    }
  else if (TMWId == 2) // X first Y second
    {
      ZRef[0] = GetRefZ() - GapSize;
      ZRef[1] = GetRefZ() + GapSize;
    }
  else if (TMWId == 3) // X first Y second
    {
      ZRef[0] = GetRefZ() - GapSize;
      ZRef[1] = GetRefZ() + GapSize;
    }
 
  //  if(VerboseLevel >= V_INFO)
    {
      cout << "<" << DetectorName << "><" << DetectorNameExt << "> Reference positions (X,Y,Z) " << endl;
      cout << "<" << DetectorName << "><" << DetectorNameExt << ">   Time  - X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ() << " mm" << endl;
      cout << "<" << DetectorName << "><" << DetectorNameExt << ">   XPos  - X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ_XPlane() << " mm" << endl;
      cout << "<" << DetectorName << "><" << DetectorNameExt << ">   YPos  - X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ_YPlane() << " mm" << endl;
      cout << "\n" << endl;
      
    }
    L->File << "<" << DetectorName << "><" << DetectorNameExt << "> Reference positions (X,Y,Z) " << endl;
    L->File << "<" << DetectorName << "><" << DetectorNameExt << ">   Time  - X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ() << " mm" << endl;
    L->File << "<" << DetectorName << "><" << DetectorNameExt << ">   XPos  - X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ_XPlane() << " mm" << endl;
    L->File << "<" << DetectorName << "><" << DetectorNameExt << ">   YPos  - X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ_YPlane() << " mm" << endl;

  

END;
}

void TMW::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = false;
  Bool_t PosInfo = false;
 
  string BaseName;
  string Name;
  
#ifdef FITSECHIP_MW
#ifdef WITH_ROOT
  //  TROOT::SetBatch(true);

  f_SECH = new TF1("f1_Sech","[0]/pow(TMath::CosH((x-[1]))/[2],2)",0,100);
  QGraph = new TGraph();
  Int_t MaxWires = (NWiresX<NWiresY?NWiresY:NWiresX);
  h_Q = new TH1F("h_Q","h_Q",MaxWires,1,MaxWires+1);
#endif
#endif

  // TMW1X 
  BaseName = DetectorName ;
  Name = BaseName + "_X";
  BaseDetector *TMWX = new BaseDetector(Name.c_str(),NWiresX,RawData,CalData,DefCal,PosInfo,"",true,true);
  TMWX->AddCounter("Saturated");
  TMWX->AddCounter("Not-Neighbours (even Dist.)");
  TMWX->AddCounter("Strips M > 3");
  TMWX->AddCounter("Strips M > 3 (contiguous)");
  TMWX->AddCounter("Almost Neighbours ");
  if(RawData)
    {
     if(IsFPMW)
         {
             TMWX->SetRawHistogramsParams(500,0,16000,"");
             TMWX->SetHistogramsRaw1DFolder("FPMW1D");
             TMWX->SetHistogramsRaw2DFolder("FPMW2D");
         }
     else if(IsSAMW)
         {
             TMWX->SetRawHistogramsParams(500,0,16000,"");
             TMWX->SetHistogramsRaw1DFolder("SAMW1D");
             TMWX->SetHistogramsRaw2DFolder("SAMW2D");
         }
      else
      {
          TMWX->SetRawHistogramsParams(5000,0,16000,"");
          TMWX->SetHistogramsRaw1DFolder("TMW1D");
          TMWX->SetHistogramsRaw2DFolder("TMW2D");
      }
    }
  if(CalData)
    {
      TMWX->SetGateCal(0,32768);
//      TMWX->SetMainHistogramFolder(Name.c_str());
      if(IsFPMW)
      {
          TMWX->SetCalHistogramsParams(500,0,16000,"");
          TMWX->SetHistogramsCal1DFolder("FPMW1D");
          TMWX->SetHistogramsCal2DFolder("FPMW2D");
      }
      else if(IsSAMW)
      {
          TMWX->SetCalHistogramsParams(500,0,16000,"");
          TMWX->SetHistogramsCal1DFolder("SAMW1D");
          TMWX->SetHistogramsCal2DFolder("SAMW2D");
      }
      else
      {
          TMWX->SetCalHistogramsParams(5000,0,16000,"");
          TMWX->SetHistogramsCal1DFolder("TMW1D");
          TMWX->SetHistogramsCal2DFolder("TMW2D");
      }
    }
  TMWX->HasCalCharges(true);

  AddComponent(TMWX);

  // TMWY
  Name.clear();
  Name = BaseName + "_Y";
  BaseDetector *TMWY = new BaseDetector(Name.c_str(),NWiresY,RawData,CalData,DefCal,PosInfo,"",true,true);
  TMWY->AddCounter("Saturated");
  TMWY->AddCounter("Not-Neighbours (even Dist.)");
  TMWY->AddCounter("Strips M >3");
  TMWY->AddCounter("Strips M >3 (contiguous)");
  TMWY->AddCounter("Almost Neighbours ");
  if(RawData)
    {
     if(IsFPMW)
     {
        TMWY->SetRawHistogramsParams(1000,0,16000,"");
        TMWY->SetHistogramsRaw1DFolder("FPMW1D");
        TMWY->SetHistogramsRaw2DFolder("FPMW2D");
     }
     else if(IsSAMW)
     {
        TMWY->SetRawHistogramsParams(1000,0,16000,"");
        TMWY->SetHistogramsRaw1DFolder("SAMW1D");
        TMWY->SetHistogramsRaw2DFolder("SAMW2D");
     }
     else
     {

        TMWY->SetRawHistogramsParams(5000,0,16000,"");
        TMWY->SetHistogramsRaw1DFolder("TMW1D");
        TMWY->SetHistogramsRaw2DFolder("TMW2D");
     }
  }
  if(CalData)
    {
      TMWY->SetGateCal(0,32768);
//      TMWY->SetMainHistogramFolder(Name.c_str());
      if(IsFPMW)
      {
          TMWY->SetCalHistogramsParams(1000,0,16000,"");
          TMWY->SetHistogramsCal1DFolder("FPMW1D");
          TMWY->SetHistogramsCal2DFolder("FPMW2D");

      }
      else if(IsSAMW)
      {
          TMWY->SetCalHistogramsParams(1000,0,16000,"");
          TMWY->SetHistogramsCal1DFolder("SAMW1D");
          TMWY->SetHistogramsCal2DFolder("SAMW2D");

      }else
      {
          TMWY->SetCalHistogramsParams(5000,0,16000,"");
          TMWY->SetHistogramsCal1DFolder("TMW1D");
          TMWY->SetHistogramsCal2DFolder("TMW2D");
      }
    }
  TMWY->HasCalCharges(true);

  AddComponent(TMWY);

  END;
}
void TMW::SetId(const Char_t *Name)
{
  START;
  UShort_t Id=0;

  if(sscanf(Name,"TMW%1hu",&Id)==1)
    {
      TMWId = Id;
      IsFPMW = false;
      IsSAMW = false;
    }
  else if(sscanf(Name,"FPMW%1hu",&Id)==1)
    {
      TMWId = Id;
      IsFPMW = true;
      IsSAMW = false;
    }
  else if(sscanf(Name,"SAMW%1hu",&Id)==1)
    {
      TMWId = Id;
      IsFPMW = false;
      IsSAMW = true;
    }
  else
    {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> : Wrong Detector Name in (%s) while extracting its Id!", 
              GetName(),
              GetNameExt(),
              GetName());
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
    };
  cout << "<"<< DetectorName << ">" << " Type " << (IsFPMW ? "FPMW" : "TMW") << " ID " << TMWId << endl;
  END;
}

void TMW::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  Char_t Name[100];
  
  UShort_t jtemp=0;

  if(isComposite)
    {
      Char_t PName[20];
      // i = 0 => X
      // i = 1 => Y
      for(UShort_t i=0; i < 2; i++)
        for(UShort_t j=0; j < DetList->at(i)->GetNumberOfDetectors(); j++)
          {
            if(IsFPMW)
              {

                Int_t Board=0;
                Int_t Channel=0;
                Int_t Sample = 0; 
               // readout Using NumExo !
                //  X In CRAMS => 1000 =>
//                Int_t ModuleNr = j/(192)+6*i;
//                Int_t ChanNr = j % 192;
//                sprintf(PName,"MW_%d_%d",ModuleNr,ChanNr);
//                cout << "MWFP Configuration" <<endl;
//                cout << i << " " <<j << endl;
//                cout << PName<< endl;
//                DetList->at(i)->SetParameterName(PName,j);
//             



                  //2021

                  //X possition (NUMEXO)
                  //161 channel 1 0-96
                  //161 channel 2 0-96
                  //161 channel 3 0-96
                  //161 channel 4 0-96
                  //161 channel 5 0-96
                  //161 channel 6 0-96
                  //161 channel 7 0-96
                  //161 channel 8 0-96
                  //161 channel 9 0-96
                  //161 channel 10 0-96
                  //161 channel 11 0-31
                  //Y possition (NUMEXO)
                  //161 channel 11 32-96
                  //161 channel 12 0-96
                /*
                Board=161;
                if(i == 0) //X
                {

                   Channel = j/96+1;
                   Sample = j%96;
                   sprintf(Name, "FPMW_0_%d", j);
                   //sprintf(Name, "FPMW_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
                }

                else if(i == 1)  //Y
                {
                  Channel = 10+(j+32)/96+1;
                  Sample = (j+32)%96;
                  sprintf(Name, "FPMW_0_%d", j+992);
                  //sprintf(Name, "FPMW_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
                }
                */
                if(i == 1) //Y
                {
                   Channel = j/96+1;
                   Sample = j%96;
                   sprintf(Name, "FPMW_%d_%d", TMWId, j);
                   //sprintf(Name, "FPMW_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
                }

                else if(i == 0)  //X
                {
                  Channel = 1+(j+64)/96+1;
                  Sample = (j+64)%96;
                  // if(TMWId==0)
                    sprintf(Name, "FPMW_%d_%d", TMWId, j+160);

                  //************************Modification to match with the swapped chips of FPMW1_X ********************// 07/05/2021 by Diego and Youngju
                  //else if(TMWId==1)
                    //{
                      // if(j>=64 && j<=79)
                      //sprintf(Name, "FPMW_%d_%d", TMWId, j+160+16);
                      //else if(j>=80 && j<=95)
                      //sprintf(Name, "FPMW_%d_%d", TMWId, j+160-16);
                      //else if(j>=352 && j<=367)
                      //sprintf(Name, "FPMW_%d_%d", TMWId, j+160+16);
                      //else if(j>=368 && j<=383)
                      //sprintf(Name, "FPMW_%d_%d", TMWId, j+160-16);
                      //else
                      //sprintf(Name, "FPMW_%d_%d", TMWId, j+160);
                    //}
                  //sprintf(Name, "FPMW_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
                }



                // Specific for e753 not using inners (0,1,8,9)  and and BGO/CSI (6,7,14,15)





//                if(i ==1)
//                  {
//                /// Y 160 Wire
                // 161 chan 1 0-95
                // 161 chan 2 0-63
//                Board = 161; // e667
//                    Channel = j/96+1;
//                    Sample = j%96;
//                    sprintf(Name, "FPMWPC_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
//                    DetList->at(i)->SetParameterName(Name, j);
//                  }
//                if(i == 0)
//                  {
                //                Board = 161; // e667

//                /// X 160 Wire
//                    if(j<32)
//                      {
//                        Channel = j/96+2;
//                        Sample = j+64;
//                        sprintf(Name, "FPMWPC_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
//                      }
//                    if(j>31&&j<(32+10*96))
//                      {
//                        Channel = (j-32)/96+3;
//                        Sample = (j-32)%96;
//                        //Board = 157; //e753
//                        sprintf(Name, "FPMWPC_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
//                      }

//                    // 161 chan 2 64-95 0-31
//                    // 161 chan 3 0-96
//                    // 161 chan 4 0-96 223
//                    // 161 chan 5 0-96
//                    // 161 chan 5 0-96
//                    // 161 chan 7 0-96
//                    // 161 chan 8 0-96 607
//                    // 161 chan 9 0-96
//                    // 161 chan 10 0-96
//                    // 161 chan 11 0-96
//                    // 161 chan 12 0-96 992
                  
//                  }

                  
                  // Int_t Channel = j/96;  
                  // Int_t Sample = j%96;  
                  
                  //   if(i == 0) 
                  //       sprintf(Name, "FPMWPC_0_b%03d_c%02d_s%03d", 157, Channel, Sample);
                  //   els5
                  //       sprintf(Name, "FPMWPC_0_b%03d_c%02d_s%03d", 160, Channel, Sample);
                 
                //cout << i << " " << j << " ==> Adding Parameter " << Name << endl;
                DetList->at(i)->SetParameterName(Name, j);
                
              }
            else if (IsSAMW)
            {

               if(i == 1) //Y
               {
                  sprintf(Name, "SAMW_%d_%d", TMWId, j+480);
                  //sprintf(Name, "FPMW_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
               }

               else if(i == 0)  //X
               {
                 sprintf(Name, "SAMW_%d_%d", TMWId, j);
                 //sprintf(Name, "FPMW_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
               }

               DetList->at(i)->SetParameterName(Name, j);

            }
            else
              {
                if(TMWId != 3)
                  sprintf(PName,"TMW_%d_%d",TMWId,96*i+j);
                else
                  {
		    ////////////////NOTE!!!! e849 (2024) CABLE SWAP even-odd wires ///////////////
		    if(j%2==0)
		      jtemp = j+1;
		    else
		      jtemp = j-1;
		    ////////////////////////////////////////////////////////////////////////////
                    if(i == 1) //Y
                      {
			if((j+18)<96)
			  sprintf(PName, "TMW_%d_%d", TMWId, jtemp+192+18);
			else
			  sprintf(PName, "TMW_%d_%d", TMWId, jtemp+192+18+32);
                        //sprintf(Name, "FPMW_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
                      }
                    
                    else if(i == 0)  //X
                      {
			if((j+18)<96)
			  sprintf(PName, "TMW_%d_%d", TMWId, jtemp+18);
			else
			   sprintf(PName, "TMW_%d_%d", TMWId, jtemp+18+32);
                        //sprintf(Name, "FPMW_0_b%03d_c%02d_s%03d", Board, Channel, Sample);
                      }                
                                      }
               DetList->at(i)->SetParameterName(PName,j);
              }
          }
      
      for(UShort_t i=0; i< DetList->size(); i++)
        if(DetList->at(i)->HasRawData())
          {       
                NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
                DetList->at(i)->SetParametersNUMEXO( PL_NUMEX , Map);
       }
  }
  
  END;
}

Bool_t TMW::Treat(void)
{
  START;
  Ctr->at(0)++;
  if(isComposite)
    {
      if(fCalData)
        {
          if(IsFPMW)
            {
           //   DetList->at(0)->PrintRaw();
              if(DetList->at(0)->GetRawM())
                CalibrateWire(0,40000);
               if(DetList->at(1)->GetRawM())
                 CalibrateWire(1,40000);
            }
          else
            {

              if(DetList->at(0)->GetRawM())
                CalibrateWire(0,40000);
              if(DetList->at(1)->GetRawM())
                CalibrateWire(1,40000);
              for(UShort_t i=2; i< DetList->size(); i++)
                DetList->at(i)->Treat();
            }
        }
      
    }
 
  if(fCalData)
    {
      if(DetList->at(0)->IsPresent())
        {
          fPresentX = TreatWire(0);
        }
      else
        {
          SetCalData(0,-1500);
          SetCalData(2,-1500);  
          fPresentX = false;
        }
      
      if(DetList->at(1)->IsPresent())
        {
          fPresentY = TreatWire(1);
        }
      else
        {
          SetCalData(1,-1500);
          SetCalData(3,-1500);  
          fPresentY = false;
        }
    }
  
  if(fPresentX && fPresentY)
    isPresent = true;

  return isPresent;
  END;
}

Bool_t TMW::TreatWire(UShort_t n)
{
  START;
  UShort_t NStrips = 3 ;
  Float_t QTmp[1150];
  Float_t QMax;
  UShort_t NMax;
  UShort_t FStrip[1150];
  Bool_t Neighbours;
  Float_t X=-1500.;
  Float_t XWA=-1500.;
  Bool_t Present = false;
#ifdef FITSECHIP_MW
#ifdef WITH_ROOT
  // QGraph->Set(0);
  // QGraph->Set(DetList->at(n)->GetM());
  h_Q->Reset();
#endif
#endif
  if(DetList->at(n)->GetM() >= NStrips)
    Ctr->at(2+n*4)++;
  else
    {      
      SetCalData(n,-1500.);
      SetCalData(n+2,-1500.);
      return false;
    }

  for(UShort_t j=0; j< DetList->at(n)->GetM();j++) //loop over strips
    {
      QTmp[j] = DetList->at(n)->GetCalAt(j);// Copy Charges
#ifdef FITSECHIP_MW
#ifdef WITH_ROOT
      // QGraph->SetPoint(j,DetList->at(n)->GetNrAt(j),DetList->at(n)->GetCalAt(j));
      h_Q->Fill(DetList->at(n)->GetNrAt(j),DetList->at(n)->GetCalAt(j));
#endif
#endif 
    }
  
#ifdef FITSECHIP_MW
#ifdef WITH_ROOT
  //  cout << " Det " << n << " " << DetList->at(n)->GetM()  << endl;
  //  QGraph->Print();
#endif
#endif


  // Find GetM highest charges NStrips
  for(UShort_t k=0;k<DetList->at(n)->GetM(); k++)
    {
      QMax=0.0;
      NMax=0;
      for(UShort_t j=0;j<DetList->at(n)->GetM();j++)
        {
          if(QTmp[j] > QMax)
            {
              QMax = QTmp[j];
              NMax = j;
            }
        }
      QTmp[NMax] = 0.0;
      FStrip[k] = NMax;
    }

  if(NStrips != 3) 
    {
      MErr * Er = new MErr(WhoamI,0,0, "NStrips != 3 but");
      throw Er;      
    }
  Neighbours = false;
  
#ifdef FITSECHIP_MW
  Float_t XSf = FitSECH(FStrip,n);
#endif

  if((Neighbours = CheckNeighbours(FStrip,n)))
    {
      // DetList->at(n)->PrintCal();
      X = SECHIP(FStrip,n);
    }
  else
    {
      X = -1500; 
      //    DetList->at(n)->PrintCal();
    }
  if(X > 0 && X <  DetList->at(n)->GetNumberOfDetectors())
    {      
      Present = true;
      //      if(TMWId == 3)
      //        X *= -1.; //Goes 1mm  spacing inverted to be in VAMOS REference
      //      else
      //        X *= 1.; //Goes 1mm  spacing
      
      if(n == 0)
        {
         if(IsFPMW)
         {
             X *= -1.;//Goes 1mm  spacing inverted to be in VAMOS REference
         }
         X -= GetRefX();
        }
      else if(n == 1)
        X -= GetRefY();
    }
  else
    {
      Present = false;
      X = -1500;
      
    }
  
  // Independant Calculation for Weighter average

  XWA = WeightedAverage(FStrip,NStrips,n);

  if(XWA > 0 && XWA <  DetList->at(n)->GetNumberOfDetectors())
    {

      if(n == 0)
        {
         if(IsFPMW)
         {
             XWA *= -1.;//Goes 1mm  spacing inverted to be in VAMOS REference
         }
          XWA -= GetRefX();
        }
      else if(n == 1)
        XWA -= GetRefY();
    }
  else
    {
      // Present =false;
      XWA = -1500;
    }
  
  
  SetCalData(n,X);
  SetCalData(n+2,XWA);

#ifdef FITSECHIP_MW
  if(XSf > 0 && XSf <  DetList->at(n)->GetNumberOfDetectors())
    {      
      if(TMWId == 3)
        XSf *= -1.; //Goes 1mm  spacing inverted
      else
        XSf *= 1.; //Goes 1mm  spacing
        
      // New to by pass SECHs
      Present = true;
      if(n == 0)
        XSf += GetRefX();
      else if(n == 1)
        XSf += GetRefY();
    }
  else
    {
      Present = false;
      XSf = -1500;    
    }

  //SetCalData(n+16,XSf);
#endif

  
  return Present;
  END;
}

void TMW::CalibrateWire(UShort_t n,UShort_t Saturation)
{
  START;
  //Check the there is no saturated wires 
  DetList->at(n)->IncrementCounter(0);
  for(UShort_t i=0;i<DetList->at(n)->GetRawM();i++)
    if (DetList->at(n)->GetRawAt(i) == Saturation)
      {
                cout << "/!\\ Saturated Wire " << DetList->at(n)->GetRawNrAt(i) << " in Plane " << n << endl;
  		  Ctr->at(5+n*4)++;
  		  DetList->at(n)->IncrementCounter(2);
  		//  DetList->at(n)->SetPresent(false);
  		  // DetList->at(n)->PrintRaw(); 
  		//  return;
      }

  DetList->at(n)->CalibrateCharges();
//  DetList->at(n)->PrintCal();

   if(DetList->at(n)->GetM())
     {
       DetList->at(n)->SetPresent(true);
       //SetCalData(n+14,DetList->at(n)->GetM());
       SetCalData(n+4,DetList->at(n)->GetM());
     }
  
  END;
}

Bool_t TMW::CheckNeighbours(UShort_t *FStrip, UShort_t n)
{
  START;
  Bool_t rval = false;
  UShort_t tmp = 0;
  if( abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[1])) == 1 
      &&
      abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[2])) == 1 )
    return true;
  else
    {
      DetList->at(n)->IncrementCounter(3);
      if(DetList->at(n)->GetM()>3)
        {

          if( abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[1])) == 1 
              && 
              abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[3])) == 1)
            {
              Float_t Diff = (DetList->at(n)->GetCalAt(FStrip[2])-DetList->at(n)->GetCalAt(FStrip[3]))/0.5/(DetList->at(n)->GetCalAt(FStrip[2])+DetList->at(n)->GetCalAt(FStrip[3]))*100 ;
              // cout << " Were Almost neighbours" << endl;
              // cout <<  DetList->at(n)->GetCalAt(FStrip[1]) << endl;
              // cout <<  DetList->at(n)->GetCalAt(FStrip[2]) << endl;
              // cout <<  DetList->at(n)->GetCalAt(FStrip[3]) << endl;
              // cout << "\t " << Diff << "% difference"<< endl;
				  
              if (Diff < 15) 
                {						
                  tmp = FStrip[2];
                  FStrip[2] = FStrip[3] ;
                  FStrip[3] = tmp;	
                  rval = true;
                  DetList->at(n)->IncrementCounter(6);
                }
            }
        }

      return rval ;
    }
  END; 
}

Float_t TMW::WeightedAverage(UShort_t *FStrip, UShort_t NStrips, UShort_t n)
{ 
  START;
  Float_t v[2];
  Float_t QTotal=-10.;
  Float_t QRatio=-1.;
  UShort_t StripsWA=0;
  Float_t XWA=-1500.;
  UShort_t MaxWireNr[1150];
  Float_t LStripQ[1150];
  Bool_t isPresent = false;
  if(DetList->at(n)->GetM() > NStrips)
    {
      DetList->at(n)->IncrementCounter(4);
      //Looking for entire peak for W.A.
      //The Strips are NOT ordered  0-64 ...
      //Coul be done earlier but ....

      // Reoder the strips ...


      for(UShort_t j=0; j< DetList->at(n)->GetNumberOfDetectors();j++) 
        {
          LStripQ[j]=0;
          MaxWireNr[j] = 0.;
        }
      MaxWireNr[0] = DetList->at(n)->GetNrAt(FStrip[0]);
#ifdef  DEBUG_PP
      cout << "--> <"<<n<< "> Start ----------------------" <<endl;
#endif
	   
      for(UShort_t j=0; j< DetList->at(n)->GetM();j++) //loop over strips
        {	       
          LStripQ[DetList->at(n)->GetNrAt(j)] = DetList->at(n)->GetCalAt(j);// Copy Charges
#ifdef DEBUG_PP	 
          cout << j << " " << DetList->at(n)->GetNrAt(j) << "\t Cal : " <<  DetList->at(n)->GetCalAt(j) << endl;
#endif
        }
	   
#ifdef  DEBUG_PP
      for(UShort_t j=0; j< DetList->at(n)->GetNumberOfDetectors();j++) 
        {
          cout << "Nr : " << j <<  " Q: "  << LStripQ[j];
          if(j == MaxWireNr[0])  cout << "   <--- Max " ;
          cout <<endl;
        }

		cout << "Max Pad Nr First " << FStrip[0] << " -> " << DetList->at(n)->GetNrAt(FStrip[0]) << endl;
      
#endif
 

      StripsWA=0;

    
      for(Int_t j=MaxWireNr[0]-1;j>=0;j--)
        {
          if((LStripQ[j] <= LStripQ[j+1]) && LStripQ[j] > 0)
            {		   
              StripsWA++;
              MaxWireNr[StripsWA]=j;
            }
          else
            break;
        }
	   
      for(Int_t j=MaxWireNr[0]+1; j< DetList->at(n)->GetNumberOfDetectors();j++) 
        {
          if((LStripQ[j-1] >= LStripQ[j]) && LStripQ[j] > 0)
            {
              StripsWA++;
              MaxWireNr[StripsWA]=j;			  
            }
          else
            break;
        }
	       
	       
#ifdef DEBUG_PP
      cout << "TMW::TreatWire("<<n<<")::Weithed Average: "  << endl;
      cout << "StripsWA: " << StripsWA << endl; 
#endif	     
      if(StripsWA >= NStrips)
        {
          DetList->at(n)->IncrementCounter(5);
          v[0] = v[1] = 0.0;
          for(UShort_t k=0;k<=StripsWA;k++)
            {		      
              v[0] += LStripQ[MaxWireNr[k]] * ((Float_t) MaxWireNr[k]);
              v[1] += LStripQ[MaxWireNr[k]];
            }
			 
          XWA = v[0] / v[1];
          QTotal = v[1];
          QRatio = LStripQ[MaxWireNr[0]]/QTotal;
#ifdef DEBUG_PP
          // cout << "TMW::TreatWire("<<n<<")::Weithed Average: "  << endl;
          // cout << "StripsWA: " << StripsWA << endl;
          for(UShort_t k=0;k<=StripsWA;k++)
            cout <<  StripsWA << " WIREWA: " << MaxWireNr[k] << " Q: " << LStripQ[MaxWireNr[k]] << endl;
          cout << "Result 2: " << XWA << "mm" << endl ;
          cout << "Total Charge 2: " << QTotal << endl;
#endif
	 	    
          if(XWA > 0. && XWA <DetList->at(n)->GetNumberOfDetectors())
            {
              isPresent = true;
              Ctr->at(4+n*4)++;		  
            }
          else 
            {
              XWA = -1500.;
            }
	  

        }
      else
        {
          XWA = -1500.;
          QTotal = -10;
          QRatio = -1;
        }


      // QTotal
//      SetCalData(n+4,QTotal);
//      // MaxPadQ
      //SetCalData(n+6,LStripQ[MaxWireNr[0]]);
//      // MaxPadNrQ
      //SetCalData(n+8,MaxWireNr[0]);
//      // Ratio of Max Charge / QTotal
//      SetCalData(n+10,QRatio);
//      // Multiplicity
//      SetCalData(n+12,DetList->at(n)->GetM());

    }
  else
	 {
		XWA = -1500.;
		QTotal = -10;
		QRatio = -1;
	 }


  return XWA;
  END;
}


Float_t TMW::FitSECH(UShort_t *FStrip,UShort_t n)
{
  START;
  Float_t XS=-1500;

#ifdef FITSECHIP_MW
#ifdef WITH_ROOT  
  //  cout << "FStrip" << FStrip[0] << " "<<  DetList->at(n)->GetCalAt(FStrip[0]) << " " << DetList->at(n)->GetNrAt(FStrip[0])<<   endl;
  f_SECH->SetParameter(0,DetList->at(n)->GetCalAt(FStrip[0]));
  f_SECH->SetParameter(1,DetList->at(n)->GetNrAt(FStrip[0]));
  f_SECH->SetParameter(2,1);
  // QGraph->Fit(f_SECH,"Q","");
  h_Q->Fit(f_SECH,"Q","");
  XS = f_SECH->GetParameter(1);
  //  cout << "Fit res : " << XS << " " <<  f_SECH->GetParError(1) << endl;
#endif  
#endif

  if(XS > 0. && XS < DetList->at(n)->GetNumberOfDetectors())
    {
      //Ctr->at(3+n*4)++;
    }
  else
    XS = -1500.;

  return XS;

  END;
}


Float_t TMW::SECHIP(UShort_t *FStrip,UShort_t n)
{
  START;
  Float_t v[6];
  Float_t XS=-1500;
  v[0] = sqrt(DetList->at(n)->GetCalAt(FStrip[0])/DetList->at(n)->GetCalAt(FStrip[2]));
  v[1] = sqrt(DetList->at(n)->GetCalAt(FStrip[0])/DetList->at(n)->GetCalAt(FStrip[1]));
  v[2] = 0.5*(v[0]+v[1]);
  v[3] = log(v[2]+sqrt(pow(v[2],2.f)-1.0));
  v[4] = (v[0] - v[1])/(2.0*sinh(v[3]));
  v[5] = 0.5*log((1.0+v[4])/(1.0-v[4]));
  

  XS = (Float_t) DetList->at(n)->GetNrAt(FStrip[0]) 
	 - (Float_t) (DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[1]))*v[5]/v[3];
  // cout <<  DetList->at(n)->GetNrAt(FStrip[0])  <<  " " << XS << endl; 

  if(XS > 0. && XS < DetList->at(n)->GetNumberOfDetectors())
    {
      Ctr->at(3+n*4)++;

    }
  else
    XS = -1500.;
  
  return XS;
  END;
}


#ifdef WITH_ROOT
void TMW::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
		  
  Float_t X = 0;
  Float_t Y = 0;
  Int_t M = 0;
  if(IsFPMW)
    {
      X = 1000;
      Y = 500;
      M = 30;
    }
  else if(IsSAMW)
    {
      X = 500;
      Y = 200;
      M = 30;
    }
 else
   {
     X = 50;
     Y = 50;
     M = 30;
   }

  // XS
  AddHistoCal(CalNameI[0],CalNameI[0],"XS (mm)",1000,-X,X);
  // YS
  AddHistoCal(CalNameI[1],CalNameI[1],"YS (mm)",500,-Y,Y);
  // XWA
  AddHistoCal(CalNameI[2],CalNameI[2],"XWA (mm)",1000,-X,X);
  // YWA
  AddHistoCal(CalNameI[3],CalNameI[3],"YWA (mm)",500,-Y,Y);

  //  // MX_f
    AddHistoCal(CalNameI[4],CalNameI[4],"Mult X(free)",M,0,M);
  //  // MY_f
    AddHistoCal(CalNameI[5],CalNameI[5],"Mult Y(free)",M,0,M);

//  // QX
//  AddHistoCal(CalNameI[4],CalNameI[4],"QX",1000,0,10000);
//  // QY
//  AddHistoCal(CalNameI[5],CalNameI[5],"QY",1000,0,10000);
//  // MaxQX
//  AddHistoCal(CalNameI[6],CalNameI[6],"MaxQX",1000,0,10000);
//  // MaxQY
//  AddHistoCal(CalNameI[7],CalNameI[7],"MaxQY",1000,0,10000);
//  // MX
//  AddHistoCal(CalNameI[12],CalNameI[12],"Mult X",M,0,M);
//  // MY
//  AddHistoCal(CalNameI[13],CalNameI[13],"Mult Y",M,0,M);
//  // MX_f
//  AddHistoCal(CalNameI[14],CalNameI[14],"Mult X(free)",M,0,M);
//  // MY_f
//  AddHistoCal(CalNameI[15],CalNameI[15],"Mult Y(free)",M,0,M);
//  // XS
//  AddHistoCal(CalNameI[16],CalNameI[16],"XSf (mm)",1000,-X,X);
//  // YS
//  AddHistoCal(CalNameI[17],CalNameI[17],"YSf (mm)",1000,-Y,Y);




  END;
}

void TMW::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  Float_t X = 0;
  Float_t Y = 0;
  Int_t M = 0;
if(IsFPMW)
    {
      X = 500;
      Y = 100;
      M = 30;
    }
  else if(IsSAMW)
    {
      X = 500;
      Y = 200;
      M = 30;
    }
 else
   {
     X = 50;
     Y = 50;
     M = 30;
   }


  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }
  AddHistoCal(CalNameI[0],CalNameI[1],Form("TMW%d_XY_S",TMWId),"X_vs_Y (SECHS)",600,-X,X,600,-Y,Y);
  AddHistoCal(CalNameI[2],CalNameI[3],Form("TMW%d_XY_WA",TMWId),"X_vs_Y (WeigtedAverage)",600,-X,X,600,-Y,Y);
  //  AddHistoCal(CalNameI[16],CalNameI[17],Form("TMW%d_XY_Sf",TMWId),"X_vs_Y (FitSech)",600,-X,X,600,-Y,Y);

  //AddHistoCal(CalNameI[2],CalNameI[4],Form("XWA_vs_QX_TMW%d",TMWId),Form("XWA vs QY (TMW%d)",TMWId),600,-X,X,1000,0,10000);
  //AddHistoCal(CalNameI[3],CalNameI[5],Form("YWA_vs_QY_TMW%d",TMWId),Form("YWA vs QY (TMW%d)",TMWId),600,-Y,Y,1000,0,10000);

  // DetList->at(2)->AddHistoCal(DetList->at(2)->GetCalName(0),DetList->at(2)->GetCalName(1),"SIE_vs_SIT","SIE vs SIT",2000,0,13000,500,00,16500);
  // DetList->at(2)->AddHistoCal(DetList->at(2)->GetCalName(0),DetList->at(2)->GetCalName(2),"SIE_vs_PPT","SIE vs PPACT",2000,0,13000,500,12500,13500);
  
  END;
}


void TMW::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
  // Set histogram Hierarchy
// if(TMWId == 0 || TMWId == 1 ||TMWId == 2 ||)
    SetMainHistogramFolder("");

  if(IsFPMW)
  {
      SetHistogramsCal1DFolder("FPMW1D");
      SetHistogramsCal2DFolder("FPMW2D");

  }
  else if(IsSAMW)
  {
      SetHistogramsCal1DFolder("SAMW1D");
      SetHistogramsCal2DFolder("SAMW2D");

  }
  else
  {
      SetHistogramsCal1DFolder("TMW1D");
      SetHistogramsCal2DFolder("TMW2D");
  }
  for(UShort_t i = 0;i<DetList->size();i++)
	 {
		DetList->at(i)->SetMainHistogramFolder("");
        if(TMWId == 3)
            SetMainHistogramFolder("TMW3");

        if(IsFPMW)
        {
            DetList->at(i)->SetHistogramsRaw1DFolder("FPMW1D");
            DetList->at(i)->SetHistogramsRaw2DFolder("FPMW2D");
            DetList->at(i)->SetHistogramsCal1DFolder("FPMW1D");
            DetList->at(i)->SetHistogramsCal2DFolder("FPMW2D");
        }
        else if(IsSAMW)
        {           
            DetList->at(i)->SetHistogramsRaw1DFolder("SAMW1D");
            DetList->at(i)->SetHistogramsRaw2DFolder("SAMW2D");
            DetList->at(i)->SetHistogramsCal1DFolder("SAMW1D");
            DetList->at(i)->SetHistogramsCal2DFolder("SAMW2D");
        }
        else
        {
//            if(TMWId == 3)
//                DetList->at(i)->SetMainHistogramFolder("SecondArm");
            DetList->at(i)->SetHistogramsRaw1DFolder("TMW1D");
            DetList->at(i)->SetHistogramsRaw2DFolder("TMW2D");
            DetList->at(i)->SetHistogramsCal1DFolder("TMW1D");
            DetList->at(i)->SetHistogramsCal2DFolder("TMW2D");
        }

        }

  if(fMode == 0)
    {
      if(fRawData)
        {
          SetHistogramsRaw(true);
          // SetOutAttachRawV(true);
        }
      if(fCalData)
        {
          SetHistogramsCal(true);
          // SetOutAttachCalI(true);
        }
      for(UShort_t i = 0;i<2;i++)
        {
          if(fRawDataSubDetectors)
            {
              DetList->at(i)->SetHistogramsRaw2D(true);
              DetList->at(i)->SetHistogramsRawSummary(true);
            }
          DetList->at(i)->SetHistogramsCal1D(false);
          DetList->at(i)->SetHistogramsCal2D(true);
          DetList->at(i)->SetHistogramsCalSummary(true);
        }
      if(fRawDataSubDetectors)
        {
          // DetList->at(2)->SetHistogramsRaw(true);
          // DetList->at(2)->SetOutAttachRawI(true);
        }
      // DetList->at(2)->SetHistogramsCal(true);
      // DetList->at(2)->SetOutAttachCalI(true);
	
      // OutAttach(OutTTree);
      // 	for(UShort_t i = 0;i<DetList->size();i++)
      // 	  DetList->at(i)->OutAttach(OutTTree);

    }
  else if(fMode == MODE_D2R)
    {
      for(UShort_t i = 0;i<2;i++)
        {
          DetList->at(i)->SetHistogramsRaw2D(true);
          DetList->at(i)->SetHistogramsRawSummary(true);
          DetList->at(i)->SetOutAttachRawV(true);
          if(DetList->at(i)->HasTS())
              DetList->at(i)->SetOutAttachTS(true);
        }
      for(UShort_t i = 0;i<DetList->size();i++)
      {
          DetList->at(i)->OutAttach(OutTTree);

      }
      OutAttach(OutTTree);
	
    }
  else if(fMode == MODE_D2A)
	 {
	   if(fRawData)
	     {
	       SetHistogramsRaw(true);
	       SetOutAttachRawI(fOutAttachRawI);
	       SetOutAttachRawV(fOutAttachRawV);
	     }
	   if(fCalData)
	     {
	       SetHistogramsCal(true);
	       SetOutAttachCalI(fOutAttachCalI);
	       SetOutAttachCalV(fOutAttachCalV);
	     }
		
	   for(UShort_t i = 0;i<2;i++)
	     {
	       if(fRawDataSubDetectors)
		 {
		   DetList->at(i)->SetHistogramsRaw1D(false);
		   DetList->at(i)->SetHistogramsRaw2D(true);
		   DetList->at(i)->SetHistogramsRawSummary(true);
		   // No Charges
		   // DetList->at(i)->SetOutAttachRawV(fOutAttachRawV);
		   // if(DetList->at(i)->HasTS())
		   //   DetList->at(i)->SetOutAttachTS(fOutAttachRawV);
		   
		 }
	       DetList->at(i)->SetHistogramsCal1D(false);
	       DetList->at(i)->SetHistogramsCal2D(true);
	       DetList->at(i)->SetHistogramsCalSummary(true);
	       // DetList->at(i)->SetOutAttachCalI(fOutAttachCalI);
	       // // No Charges
	       // DetList->at(i)->SetOutAttachCalV(fOutAttachCalV);
	     }
	   OutAttach(OutTTree);
	   for(UShort_t i = 0;i<DetList->size();i++)
	     DetList->at(i)->OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
      SetInAttachRawV(true);
      InAttach(InTTree);
		
      SetOutAttachCalI(true);
      OutAttach(OutTTree);
		
    }
  else if(fMode == MODE_RECAL)
    {
      for(UShort_t i = 0;i<2;i++)
	{
	  if(fRawDataSubDetectors)
            {
              // No Charges
              DetList->at(i)->SetInAttachRawV(false);
            }
	  // No Charges
	  if(DetList->at(i)->HasCalData())
	    DetList->at(i)->SetInAttachCalV(true);			 
	  DetList->at(i)->InAttachCal(InTTree);
	}
      SetInAttachCalI(true);
      InAttachCal(InTTree);


      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	}

      for(UShort_t i = 0;i<2;i++)
	{
	  if(fRawDataSubDetectors)
            {
              DetList->at(i)->SetHistogramsRaw1D(false);
              DetList->at(i)->SetHistogramsRaw2D(true);
              DetList->at(i)->SetHistogramsRawSummary(true);
              // No Charges
              DetList->at(i)->SetOutAttachRawV(false);
            }
	  DetList->at(i)->SetHistogramsCal1D(false);
	  DetList->at(i)->SetHistogramsCal2D(true);
	  DetList->at(i)->SetHistogramsCalSummary(true);
	  DetList->at(i)->SetOutAttachCalI(false);
          // No Charges
	  DetList->at(i)->SetOutAttachCalV(true);			 
	}
      if(fRawDataSubDetectors)
	{
	  // DetList->at(2)->SetHistogramsRaw(false);
	  // DetList->at(2)->SetOutAttachRawI(false);
	}


      OutAttach(OutTTree);
      for(UShort_t i = 0;i<DetList->size();i++)
      {
          DetList->at(i)->OutAttach(OutTTree);
      }
      
    }
  else 
    {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
    }




  END;
}



#endif
