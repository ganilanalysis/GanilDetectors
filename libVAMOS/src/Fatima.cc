/****************************************************************************
 *    Copyright (C) 2016-2017 by Antoine Lemasson
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "Fatima.hh"

Fatima::Fatima(const Char_t *Name,
               UShort_t NDetectors, 
               Bool_t RawData, 
               Bool_t CalData, 
               Bool_t DefaultCalibration, 
               const Char_t *NameExt
               )
  : BaseDetector(Name, 1, false, CalData, DefaultCalibration,false,NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  DetManager *DM = DetManager::getInstance();
  
  AllocateComponents(); 

 
  if(CalData)
	 {
		sprintf(CalNameI[0],"FatimaM");
    }

  END;
}
Fatima::~Fatima(void)
{
  START;
  END;

}


void Fatima::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;
 //Energies QLOng
  BaseDetector *EnerQLong = new BaseDetector("Fatima_QLong",40,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      EnerQLong->SetGateRaw(0,16384*2);
    }
  EnerQLong->SetCalHistogramsParams(5000,0,5000,"keV");
  EnerQLong->SetRawHistogramsParams(16384,0,16384*2,"chn");
  AddComponent(EnerQLong);
 
 
  // Energies QShort
  BaseDetector *EnerQShort = new BaseDetector("Fatima_QShort",40,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      EnerQShort->SetGateRaw(0,16384*2);
    }
  EnerQShort->SetCalHistogramsParams(5000,0,5000,"keV");
  EnerQShort->SetRawHistogramsParams(16384,0,16384*2,"chn");
  AddComponent(EnerQShort);

  //TsMSW
  BaseDetector *TimeStampMSW = new BaseDetector("Fatima_TsMSW",40,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      TimeStampMSW ->SetGateRaw(0,16384*2);
    }
  TimeStampMSW->SetCalHistogramsParams(16384,0,16383,"chn");
  TimeStampMSW->SetRawHistogramsParams(16384,0,16384*2,"chn");
  AddComponent(TimeStampMSW);
  
  //TsLSW
  BaseDetector *TimeStampLSW = new BaseDetector("Fatima_TsLSW",40,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      TimeStampLSW->SetGateRaw(0,16384*2);
    }
  TimeStampLSW->SetCalHistogramsParams(16384,0,16383,"chn");
  TimeStampLSW->SetRawHistogramsParams(16384,0,16384*2,"chn");
  AddComponent(TimeStampLSW);
  
  //TsEXW
  BaseDetector *TimeStampEXW = new BaseDetector("Fatima_TsEXW",40,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      TimeStampEXW->SetGateRaw(0,16384*2);
    }
  TimeStampEXW->SetCalHistogramsParams(16384,0,16383,"chn");
  TimeStampEXW->SetRawHistogramsParams(16384,0,16384*2,"chn");
  AddComponent(TimeStampEXW);
  
  //FineT
  BaseDetector *FineT = new BaseDetector("Fatima_FineT",40,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      FineT->SetGateRaw(0,16384*2);
    }
  FineT->SetCalHistogramsParams(16384,0,16383,"chn");
  FineT->SetRawHistogramsParams(16384,0,16384*2,"chn");
  AddComponent(FineT);
  
  //Res1
  BaseDetector *Res1 = new BaseDetector("Fatima_res1",40,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      Res1->SetGateRaw(0,16384*2);
    }
  Res1->SetCalHistogramsParams(16384,0,16383,"chn");
  Res1->SetRawHistogramsParams(16384,0,16384*2,"chn");
  AddComponent(Res1);
  
  //Res2
  BaseDetector *Res2 = new BaseDetector("Fatima_res2",40,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      Res2->SetGateRaw(0,16384*2);
    }
  Res2->SetCalHistogramsParams(16384,0,16383,"chn");
  Res2->SetRawHistogramsParams(16384,0,16384*2,"chn");
  AddComponent(Res2);

  //TdcH
  BaseDetector *TdcH = new BaseDetector("Fatima_TdcH",32,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      TdcH->SetGateRaw(0,16384*2);
    }
  TdcH->SetCalHistogramsParams(16384,0,16383,"chn");
  TdcH->SetRawHistogramsParams(16384,0,16384*2,"chn");
  AddComponent(TdcH);

  //TdcL
  BaseDetector *TdcL = new BaseDetector("Fatima_TdcL",32,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
    {
      TdcL->SetGateRaw(0,16384*2);
    }
  TdcL->SetCalHistogramsParams(16384,0,16383,"chn");
  TdcL->SetRawHistogramsParams(16384,0,16384*2,"chn");
  AddComponent(TdcL);
    

  // Additional TACS TFAT_HF
  BaseDetector *TFAT_HF = new BaseDetector("TFAT_HF",1,RawData,CalData,DefCal,false,"");
  if(RawData)
    TFAT_HF->SetParameterName("TFAT_HF",0);
  if(CalData)
    TFAT_HF->SetCalName("TFAT_HF_C",0);
#ifdef WITH_ROOT
  TFAT_HF->SetRawHistogramsParams(16384,0,16383,"");
  TFAT_HF->SetCalHistogramsParams(1000,0,1000,"ns","");
#endif
  AddComponent(TFAT_HF);

  END;
}

void Fatima::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
   Char_t PName[400];
  UShort_t Index = 0;
  if(isComposite)
    {
      for(UShort_t DetNr=0; DetNr<8; DetNr++) //nb of Branch
          for(UShort_t Mod=0; Mod<5; Mod++)
              for(UShort_t Chan=0; Chan<8; Chan++)
              {
                  Index = 8*Mod+Chan;
                  // QLong
                  if(DetNr==0)
                      sprintf(PName,"FATIMA_V1751_MOD%02d_CHN%02d_Qlong",Mod,Chan);
                  else if(DetNr==1)
                      sprintf(PName,"FATIMA_V1751_MOD%02d_CHN%02d_Qshort",Mod,Chan);
                  else if(DetNr==2)
                      sprintf(PName,"FATIMA_V1751_MOD%02d_CHN%02d_TsMSW",Mod,Chan);
                  else if(DetNr==3)
                      sprintf(PName,"FATIMA_V1751_MOD%02d_CHN%02d_TsLSW",Mod,Chan);
                  else if(DetNr==4)
                      sprintf(PName,"FATIMA_V1751_MOD%02d_CHN%02d_TsEXW",Mod,Chan);
                  else if(DetNr==5)
                      sprintf(PName,"FATIMA_V1751_MOD%02d_CHN%02d_FineT",Mod,Chan);
                  else if(DetNr==6)
                      sprintf(PName,"FATIMA_V1751_MOD%02d_CHN%02d_res1",Mod,Chan);
                  else if(DetNr==7)
                      sprintf(PName,"FATIMA_V1751_MOD%02d_CHN%02d_res2",Mod,Chan);

                  DetList->at(DetNr)->SetParameterName(PName,Index);
              }

      for(UShort_t DetNr=8; DetNr<10; DetNr++) //nb of Branch
          for(UShort_t Mod=0; Mod<1; Mod++)
              for(UShort_t chnr=0; chnr<32; chnr++){

                  Index = Mod*32 + chnr;
                  if(DetNr==8)
                      sprintf(PName,"FATIMA_V1290_MOD%02d_CHN%02d_TdcH",Mod,chnr);
                  else if(DetNr==9)
                      sprintf(PName,"FATIMA_V1290_MOD%02d_CHN%02d_TdcL",Mod,chnr);

                  DetList->at(DetNr)->SetParameterName(PName,Index);
              }



      for(UShort_t i=0; i< DetList->size(); i++)
          DetList->at(i)->SetParameters(Par,Map);
  }



  END;
}

Bool_t Fatima::Treat(void)
{ 
  START;
  Ctr->at(0)++;
  
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        {
          DetList->at(i)->Treat();
        }
    }
  if(fCalData)
	  {

		 if(DetList->at(0)->GetM()>1 && DetList->at(1)->GetM()>1) // Both Energies and Time Present 
         {
           // Do Something 
			  // // Detector Number
			  // SetCalData(0,DetList->at(0)->GetNrAt(0));
			  // // Detector Calibrated Energy
			  // SetCalData(1,DetList->at(0)->GetCalAt(0));
			  isPresent = true;
			}
	  }
  return(isPresent); 
  END;
}



#ifdef WITH_ROOT
void Fatima::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  for(UShort_t i = 0;i<DetList->size();i++)
    {
      DetList->at(i)->SetMainHistogramFolder("");
      DetList->at(i)->SetHistogramsRaw1DFolder("Fatima_R");
      DetList->at(i)->SetHistogramsRaw2DFolder("Fatima_R");
      if(i == 0)
	{
	  DetList->at(i)->SetHistogramsCal1DFolder("Fatima_E");
	  DetList->at(i)->SetHistogramsCal2DFolder("Fatima_E");
	}
      else
	{
	  DetList->at(i)->SetHistogramsCal1DFolder("Fatima_C");
	  DetList->at(i)->SetHistogramsCal2DFolder("Fatima_C");
	}
      SetHistogramsCal1DFolder("Fatima_C");
      SetHistogramsCal2DFolder("Fatima_C");
    }
 
  if(fMode == MODE_WATCHER)
    {
		if(fRawData)
		  {
			 SetHistogramsRaw(true);
		  }
		if(fCalData)
		  {
			 SetHistogramsCal(true);
		  }
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			  if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw(true);
				  if(i <10 ) DetList->at(i)->SetHistogramsRawSummary(true);
				}
			  if(DetList->at(i)->HasCalData())
				 {
				  DetList->at(i)->SetHistogramsCal(true);
				  if(i <10) DetList->at(i)->SetHistogramsCalSummary(true);
				}
		  }
	 }
  else if(fMode == MODE_D2R)
	 {
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw(true);
				  if(i <10) 
					 {
						DetList->at(i)->SetHistogramsRawSummary(true);
						DetList->at(i)->SetOutAttachRawV(true);
						DetList->at(i)->SetOutAttachRawI(false);
					 }
				  else
					 {			
						DetList->at(i)->SetOutAttachRawI(true);
					 }
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
	 }
  else if(fMode == MODE_D2A)
	 {
		if(fRawData)
		  {
			 SetHistogramsRaw(false);
			 SetOutAttachRawI(false);
			 SetOutAttachRawV(false);
		  }
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
			 SetOutAttachCalV(false);
		  }
		
		for(UShort_t i = 0;i<DetList->size();i++)
		  {			
			 if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw1D(true);
				  DetList->at(i)->SetHistogramsRaw2D(true);
				  
				   if(i <10) 
					 {
						DetList->at(i)->SetHistogramsRawSummary(true);
						DetList->at(i)->SetOutAttachRawV(false);
						DetList->at(i)->SetOutAttachRawI(false);			 					  
					  
					 }
					else
					  {
						 DetList->at(i)->SetOutAttachRawI(false);
					  }
				}
			 if(DetList->at(i)->HasCalData())
				{
				  DetList->at(i)->SetHistogramsCal1D(true);
				  DetList->at(i)->SetHistogramsCal2D(true);
				  if(i <10)
					 {
						DetList->at(i)->SetHistogramsCalSummary(true);
						DetList->at(i)->SetOutAttachCalI(false);
						DetList->at(i)->SetOutAttachCalV(true);			 
					 }
				  else
					 {
						DetList->at(i)->SetHistogramsCalSummary(false);
						DetList->at(i)->SetOutAttachCalI(true);
					 }
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			  if(i <10 ) 
				 DetList->at(i)->SetInAttachRawV(true);
			  else
				 DetList->at(i)->SetInAttachRawI(true);
			 DetList->at(i)->InAttach(InTTree);
		  }
		
		SetOutAttachCalI(true);

		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			  if(DetList->at(i)->HasCalData())
				{
				  DetList->at(i)->SetHistogramsCal1D(true);
				  DetList->at(i)->SetHistogramsCal2D(true);
				  if(i <10) 
					 {
						DetList->at(i)->SetHistogramsCalSummary(true);
						DetList->at(i)->SetOutAttachCalI(false);
						DetList->at(i)->SetOutAttachCalV(true);			 
					 }
				  else
					 {
						DetList->at(i)->SetHistogramsCalSummary(false);
						DetList->at(i)->SetOutAttachCalI(true);
						// DetList->at(i)->SetOutAttachRawI(true);
					 }
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
    }
  else if (fMode == MODE_RECAL)
    {
      	for(UShort_t i = 0;i<DetList->size();i++)
	  {
	    if(i <10) 
	      DetList->at(i)->SetInAttachCalV(true);
	    else
	      DetList->at(i)->SetInAttachCalI(true);
	    
	    DetList->at(i)->InAttachCal(InTTree);					 
	  }
	SetInAttachCalI(true);
	InAttachCal(InTTree);

	SetOutAttachCalI(true);

	for(UShort_t i = 0;i<DetList->size();i++)
	  {
	    if(DetList->at(i)->HasCalData())
	      {
		DetList->at(i)->SetHistogramsCal1D(true);
		DetList->at(i)->SetHistogramsCal2D(true);
		if(i <10) 
		  {
		    DetList->at(i)->SetHistogramsCalSummary(true);
		    DetList->at(i)->SetOutAttachCalI(false);
		    DetList->at(i)->SetOutAttachCalV(true);			 
		  }
		else
		  {
		    DetList->at(i)->SetHistogramsCalSummary(false);
		    DetList->at(i)->SetOutAttachCalI(true);
		    // DetList->at(i)->SetOutAttachRawI(true);
		  }
	      }
	    DetList->at(i)->OutAttach(OutTTree);
	  }
	OutAttach(OutTTree);
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}


#endif
