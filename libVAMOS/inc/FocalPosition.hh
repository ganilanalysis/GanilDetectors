/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef FOCALPOSITION_CLASS
#define FOCALPOSITION_CLASS
/**
 * @file   FocalPosition.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   January, 2013
 * @version 0.1
 * @todo 
 * @brief  FocalPosition Class
 * 
 * Generic Class for Ganil Detector that provide basic tools.
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#include "DriftChamber.hh"
#include "DriftNUMEXO.hh"
#include "NUMEXOParameters.hh"

// class BaseDetector;
//class DriftChamber;

class FocalPosition : public BaseDetector
{
public :
  //! Contructor
  FocalPosition(const Char_t * Name, //!< Detector Name
                UShort_t NDetectors, //!< Number of detectors
                Bool_t RawData = false, //!< Has RawData
                Bool_t CalData = true,  //!< Has CalData
                Bool_t DefaultCalibration = false, //!< Use Default Calibration
                const Char_t * NameExt = "",        //!< Additional Name
                Bool_t IsNumex = false);
  ~FocalPosition(void); //!< Destructor

  //! Get X focal Plane in mm
  inline Float_t GetXf(void){return Cal[0];};
  //! Get Theta focal Plane in mrad
  inline Float_t GetTf(void){return Cal[1];};
  //! Get Y focal Plane in mm
  inline Float_t GetYf(void){return Cal[2];};
  //! Get Phi focal Plane in mrad
  inline Float_t GetPf(void){return Cal[3];};
  //! Get Vamos Angle
  inline Float_t GetVamosAngle(void){return VamosAngle;};

  
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Set Parameter Patterns associated with NUMEXO Detector
  void SetParameters(NUMEXOParameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);

#ifdef WITH_ROOT
  //! Create 2D Histograms for Calculated Parameters
  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Create 1D Histograms for Calculated Parameters
  void CreateHistogramsCal1D(TDirectory *Dir);
#endif

private:

  //! Calculate Focal plane X coordinates from sub detectors
  Bool_t FocalX(void);
  //! Calculate Focal plane Y coordinates from sub detectors
  Bool_t FocalY(void);
  //! Allocate subcomponents
  void AllocateComponents(void);
  //! Read reference information from Calibration File
  void ReadPosition(void); 
  //! Least Square Matrix for X
  void SetMatX(void);
  //! Least Square Matrix for Y
  void SetMatY(void);

  ///////////////////////Diego//////////////////////////////////
  Float_t ChiSquare(Int_t nRows, Float_t* xPos, Float_t* zPos);
  /////////////////////////////////////////////////////////////

  Double_t FocalPos; //< Focal Position
  Double_t VamosAngle; //< Focal Position
  Double_t MatX[2][2]; 
  Double_t MatY[2][2];


  Bool_t IsNumexo;
};


#endif
