/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _IDENTIFICATION_CLASS
#define _IDENTIFICATION_CLASS
/**
 * @file   Identification.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   Febuary, 2013
 * @brief  Identification Class
 *
 * Identification Class
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#include "FocalPosition.hh"
#include "SiliconWall.hh"
#include "IonisationChamber.hh"
#include "IonisationChamberNUMEXO.hh"
#include "MWPPAC.hh"
#include "Plastic.hh"
#include "Reconstruction.hh"
#include "TargetPos.hh"
#include "TargetDCNumexo.hh"

class Identification : public BaseDetector
{
public :
    //! Constructor
    Identification(const Char_t * Name, //!< Detector Name
                   Reconstruction *Recon,  //!< Pointer to Reconstruction
                   FocalPosition *FocalPos,  //!< Pointer to  FocalPosition
                   IonisationChamber *IonCh,  //!< Pointer to IonisationChamber
                   SiliconWall *SIW,  //!< Pointer to SiliconWall
                   MWPPAC *MWD,  //!< Pointer to MWPPAC
                   TargetPos *TPD  //!< Pointer to TargetPos
                   );
    //! Constructor
    Identification(const Char_t * Name, //!< Detector Name
                   Reconstruction *Recon,  //!< Pointer to Reconstruction
                   FocalPosition *FocalPos,  //!< Pointer to  FocalPosition
                   IonisationChamber *IonCh,  //!< Pointer to IonisationChamber
                   SiliconWall *SIW,  //!< Pointer to SiliconWall
                   MWPPAC *MWD  //!< Pointer to MWPPAC
                   );
    //! Constructor with Plastic
    Identification(const Char_t * Name, //!< Detector Name
                   Reconstruction *Recon,  //!< Pointer to Reconstruction
                   FocalPosition *FocalPos,  //!< Pointer to  FocalPosition
                   IonisationChamber *IonCh,  //!< Pointer to IonisationChamber
                   Plastic *PLD  //!< Pointer to Plastic
                   );
    //! Constructor with DetManager
    Identification(const Char_t * Name //!< Detector Name
                   );
    ~Identification(void); //!< Destructor


#ifdef WITH_ROOT
    //! Create 1D Histograms for Calculated Parameters
    void CreateHistogramsCal1D(TDirectory *Dir);
    //! Create 2D Histograms for Calculated Parameters
    void CreateHistogramsCal2D(TDirectory *Dir);
    //! Set Input/OutPut Option
    void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
    //! Treat the data
    Bool_t Treat(void);

    //! Return Beta
    inline Float_t GetBeta(void){return Beta;};
    //! Return Beta
    inline Float_t GetBetaTP(void){return BetaTP;};
    //! Return Gamma
    inline Float_t GetGamma(void){return Gamma;};
    //! Return Time of Flight (ns)
    inline Float_t GetT(void){return T;};
    //! Return Velocity (cm/ns)
    inline Float_t GetV(void){return V;};
    //! Return Mass
    inline Float_t GetM(void){return M;};
    //! Return Mass (corrected)
    inline Float_t GetMr(void){return Mr;};
    //! Return E (Mev)
    inline Float_t GetE(void){return E;};
    //! Return Delta E (Mev)
    inline Float_t GetdE(void){return dE;};

    // Virtual Function to Get Residual energy
    virtual Float_t GetResidualEnergy() = 0 ;
    // Virtual Function to TOF
    virtual Float_t GetTimeOfFlight() = 0;

protected :
    SiliconWall *SI;     //!< Pointer to SiliconWall Objec
    MWPPAC *MW;          //!< Pointer to MWPPAC Object
    Plastic *PL;          //!< Pointer to Plastic Object


private: 
    //! Reset Local Variable
    void Init(void);
    //! Calculate the Calibrated parameters
    Bool_t Calculate(void);

protected :
    Reconstruction *Rec; //!< Pointer to Reconstruction Object
    FocalPosition *FP;   //!< Pointer to FocalPosition Object
    IonisationChamber *IC;      //!< Pointer to IonisationChamber Object
    IonisationChamberNUMEXO *ICN;
    TargetPos *TP;       //!< Pointer to TargetPos Object
    DriftChamber *DC0; // pointer to DC0
    TMW *TMW1; // pointer to TMW1
    TargetDCNumexo *DCTP;
    BaseDetector *TACS;
    DetManager *DM;
    Float_t D;
    Float_t D_1;

    Float_t dE;
    Float_t E;
    Float_t ETot;
    Float_t T;
    Float_t V;
    Float_t Beta;
    Float_t BetaTP;
    Float_t Gamma;
    Float_t GammaTP;
    Float_t VTP;
    Float_t M_Q;
    Float_t Q;
    Int_t Qi;
    Float_t dQ_Zr;
    Int_t Qi_Zr;
    Float_t dQ;
    Float_t M;
    Float_t Mr;
    Float_t Mr_Zr;
    Float_t Z;

};
#endif
