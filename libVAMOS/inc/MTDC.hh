#ifndef MTDC_H
#define MTDC_H

#include "BaseDetector.hh"
#include "MesytecParameters.hh"

class MTDC : public BaseDetector
{
public:
    //! Constructor
    MTDC(const Char_t * Name,
         UShort_t NDetectors,
                   Bool_t RawData,
                   Bool_t CalData,
                   Bool_t DefaultCalibration);
     ~MTDC(void); //!< Destructor

    void AllocateComponents(void);

    //! Treat the data
    Bool_t Treat(void);

    //! SetParameters
    void SetParameters(Parameters* Par,Map* Map);


#ifdef WITH_ROOT
    void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif

private:

    BaseDetector* TDC;
};

#endif // MTDC_H
