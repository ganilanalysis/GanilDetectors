/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _TargetDCNumexo_CLASS
#define _TargetDCNumexo_CLASS

/**
 * @file   TargetDCNumexo.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   Mar,2019
 * @brief  TargetDCNumexo Class
 *
 * Target Drift for MUGAST Campaign Class
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#include "FocalPosition.hh"
#include "TimeStamp.hh"

class TargetDCNumexo : public BaseDetector
{
public:
    TargetDCNumexo(const Char_t * Name,  //!< Detector Name
                   UShort_t NDetectors,   //!< Number of sub detectors
                   Bool_t RawData,        //!< Has Raw Data
                   Bool_t CalData,        //!< Has Calibrated Data
                   Bool_t DefaultCalibration,//!< Used Default Calibration
                   const Char_t * NameExt ="", //!< Name extension
                   UShort_t MaxMult = 1 //!< Name extension
                   );
    ~TargetDCNumexo(void);

    // //! Clear At the end of event;
    // void Clear(void);

    Bool_t Treat(void);   //!< Treat the data
    // void ReadCalibration(void);   //!< Specific reading of Calibration and thresholds


    //! Get X focal Plane in mm
    inline Float_t GetX(void){return Cal[0];};
    //! Get Theta focal Plane in mrad
    inline Float_t GetTheta(void){return Cal[1];};
    //! Get Y focal Plane in mm
    inline Float_t GetY(void){return Cal[2];};
    //! Get Phi focal Plane in mrad
    inline Float_t GetPhi(void){return Cal[3];};
    //! Get Theta focal Plane in rad
    inline Float_t GetThetaL(void){return Cal[4];};
    //! Get Phi focal Plane in rad
    inline Float_t GetPhiL(void){return Cal[5];};

#ifdef WITH_ROOT
    //! Create 2D Histograms for Calculated Parameters
    void CreateHistogramsCal2D(TDirectory *Dir);
    //! Create 1D Histograms for Calculated Parameters
    void CreateHistogramsCal1D(TDirectory *Dir);
    // Set default options  Input/Output according to fMode
    void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
    //! Set NUMEXO Parameter Patterns associated with Detector
    void SetParameters(NUMEXOParameters* Par, Map* Map);


protected:

    //! Allocate subcomponents
    void AllocateComponents(void);



    Double_t TargetPosition; //< Focal Position
    Bool_t fPresentX;
    Bool_t fPresentY;

private:
    //! FocalPosition information
    FocalPosition *FP;
    //! Reference TimeStamps
    TimeStamp *LTS;

};
#endif
