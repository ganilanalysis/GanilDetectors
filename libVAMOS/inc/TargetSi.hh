#ifndef TARGETSI_H
#define TARGETSI_H

#include "BaseDetector.hh"
#include "MesytecParameters.hh"

class TargetSi : public BaseDetector
{
public:
    //! Constructor
    TargetSi(const Char_t * Name,
         UShort_t NDetectors,
                   Bool_t RawData,
                   Bool_t CalData,
                   Bool_t DefaultCalibration);
     ~TargetSi(void); //!< Destructor

    void AllocateComponents(void);

    //! Treat the data
    Bool_t Treat(void);

    //! SetParameters
    void SetParameters(Parameters* Par,Map* Map);


#ifdef WITH_ROOT
    void SetOpt(TTree *OutTTree, TTree *InTTree);
    //! Create 2D Histograms for Calculated Parameters
    //void CreateHistogramsCal2D(TDirectory *Dir);

#endif

private:

    BaseDetector* Si_front;
    BaseDetector* Si_back;
};

#endif // TARGETSI_H
