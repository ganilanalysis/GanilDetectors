/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef RECONSTRUCTION_CLASS
#define RECONSTRUCTION_CLASS
/**
 * @file   Reconstruction.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   January, 2013
 * @version 0.1
 * @todo 
 * @brief  Reconstruction Class
 * 
 * Generic Class for Ganil Detector that provide basic tools.
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#include "FocalPosition.hh"
#include "TimeStamp.hh"

class Reconstruction : public BaseDetector
{
public :
  //! Cosntructor
  Reconstruction(const Char_t * Name,
                UShort_t NDetectors,
                Bool_t RawData = false,
                Bool_t CalData = true,
                Bool_t DefaultCalibration = false,
                const Char_t * NameExt = ""
                ); 
  ~Reconstruction(void); //!> Destructor

  
  //! Return Brho (Tm)
  inline Float_t GetBrho(void){return Cal[0];}
  //! Return Path (mm?)
  inline Float_t GetPath(void){return Cal[3];}
  //! Return Phi (rad)
  inline Float_t GetPhi(void){return Cal[2];}
  //! Return Phi Lab (mrad)
  inline Float_t GetPhiL(void){return Cal[6];}
  //! Return Theta (mrad)
  inline Float_t GetTheta(void){return Cal[1];}
  //! Return Theta Lab (mrad)
  inline Float_t GetThetaL(void){return Cal[4];}
  //! Return Theta Lab (deg)
  inline Float_t GetThetaLdeg(void){return Cal[5];}
  //! Return Theta Lab (deg)
  inline Float_t GetVamosAngle(void){return VamosAngle;}
  //! Treat the data
  Bool_t Treat(void);

  //! Set Reference Brho
  void SetBrhoRef(Float_t Brho);
  
private:
  //! FocalPosition information
  FocalPosition *FP;
  //! FocalPosition information
  TimeStamp *VAMOSTS;
  ULong64_t RefTS =0;
 #ifdef WITH_ROOT
  //! Create 1D Histograms for Calculated Parameters
  void CreateHistogramsCal1D(TDirectory *Dir);
  //! Create 2D Histograms for Calculated Parameters
  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
  //! Reconstruct parameters 
  void Calculate(void); 
  //! Read reference information from Calibration File
  void ReadCalibration(void); 
  //! Read Matrix from File
  void ReadMatrix(Char_t * FName); 
  //! Set Matrix
  void SetMatrix(Char_t * FName); 
  //! Write Matrix to File
  void WriteMatrix(Char_t * FName); 
  
  //! Refrence Brho in (Tm)
  Float_t BrhoRef; // Tm
  //! Vamos angle in degree
  Float_t VamosAngle; //deg

  Double_t **RCoeffs; //!< Coeffs D,T,P,Path tenth order reconst in x,y,t,p
 
  Int_t ***MRec; //!< Matrix [1100][550][3];
 

};


#endif
