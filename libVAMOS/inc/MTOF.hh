#ifndef MTOF_H
#define MTOF_H

#include "BaseDetector.hh"
#include "MesytecParameters.hh"
#include "MTDC.hh"

class MTOF : public BaseDetector
{
public:
    //! Constructor instantiation by type, number of channels, and calibration files
    MTOF(const Char_t * Name,
          UShort_t NDetectors,
          Bool_t RawData,
          Bool_t CalData,
          Bool_t DefaultCalibration,
          const Char_t * NameExt);

    ~MTOF(void);
    //! Allocate sub components :
    void AllocateComponents(void);
#ifdef WITH_ROOT
    //! Set Input/OutPut Option
    void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
    //! Clear Arrays
    void Clear(void);
    //!
    void SetParameters(Parameters *Par, Map *Map);
    //! Treat the data
    Bool_t Treat(void);

  void CreateHistogramsCal2D(TDirectory* Dir); 
  
  UShort_t RefMTDC[2];
  UShort_t RefCh[2];
  inline void SetRef(int TMWNr, int MTDCNr, UShort_t ChNr) { RefMTDC[TMWNr] = MTDCNr; RefCh[TMWNr] = ChNr;};
private:
  int MRefCh[2];
  Bool_t fRefCh[2];

  BaseDetector* mTOF[2][2];
 

};

#endif // MTOF_H
