/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _MWPPAC_CLASS
#define _MWPPAC_CLASS
/**
 * @file   MWPPAC.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   February, 2013
 * @todo Add/Check Tracking correlations
 * @brief  MWPPAC Class
 * 
 * MWPPAC  class  :  Calibrate  time from  Multi-wire  Parallel  Plate
 * Avalanche Chamber.
 */

#include "Defines.hh"
#include "FocalPosition.hh"
#include "BaseDetector.hh"


class MWPPAC : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  MWPPAC(const Char_t * Name,      //!< Detector Name
         UShort_t NDetectors,      //!< Number of sub detectors
         Bool_t RawData,           //!< Has Raw Data
         Bool_t CalData,           //!< Has Calibrated Data
         Bool_t DefaultCalibration,//!< Used Default Calibration
         const Char_t * NameExt,    //!< Name extension
         UShort_t MaxMult = 1 //!< MaXMultiplicity handled
         ); 
  //! Destructor
  ~MWPPAC(void);


  //! Return Time from valid PPAC section
  inline Float_t GetTime(void){
    if(isPresent) 
      return Cal[1]; 
    else
      return 0;
  }
  //! Return Nr of valid PPAC section
  inline UShort_t GetNr(void){
    if(isPresent) 
      return (UShort_t)Cal[0]; 
    else
      return 0;
  }
#ifdef WITH_ROOT
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);



protected:
  //! Allocate subcomponents
  void AllocateComponents(void);

  FocalPosition *FP;   //!< Pointer to FocalPosition information
  Bool_t isSecondArm;
  Bool_t isFPMW;
};
#endif
