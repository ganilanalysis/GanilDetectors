#ifndef nDetector_H
#define nDetector_H

#include "Defines.hh"
#include "BaseDetector.hh"

class nDetector : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  nDetector(const Char_t * Name,
                  UShort_t NDetectors,
                  Bool_t RawData,
                  Bool_t CalData,
                  Bool_t DefaultCalibration,
                  const Char_t * NameExt);
  ~nDetector(void);



#ifdef WITH_ROOT
  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Set Input/OutPut Option
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
  //! Custom 2D Histograms
//  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Custom 1D Histograms
//  void CreateHistogramsCal1D(TDirectory *Dir);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(NUMEXOParameters* Par, Map* Map);
  //! Treat the data
  Bool_t Treat(void);


protected:

//! Allocate subcomponents
  void AllocateComponents(void);


};


#endif // nDetector_H
