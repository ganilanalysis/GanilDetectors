/****************************************************************************
 *    Copyright (C) 2012-2019 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _IDMUGAST_CLASS
#define _IDMUGAST_CLASS
/**
 * @file   IdMugast.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   April, 2019
 * @brief  MUGAST Identification Class
 *
 */

#include "Defines.hh"
#include "Identification.hh"

class IdMugast : public Identification
{
   public :
   //! Constructor with Plastic
   IdMugast(const Char_t *Name)
      :Identification(Name)
   {
      START;
      cout << " Setup ID e810" << endl;
      FPMWPat0 = DM->GetDetector("FPMWPat_0");
      cout << FPMWPat0->GetNumberOfDetectors()<< endl;
      TP = (TargetPos*) DM->GetDetector("TP");
      END;
   };

   ~IdMugast(void)
   {
      START;

      END;
   }; //!< Destructor

   // Virtual Function to Get Residual energy
   Float_t GetResidualEnergy()
   {
      START;
      if(ICN)
         return ICN->GetETotal();
      else
         return 0;
      END;
   };
   // Virtual Function to Get Residual energy
   Float_t Init()
   {
      START;
      dE = E = ETot = T = V = M_Q = M = Z = Beta = Q = D = 0.0;
      D_1 = 0.0;
      Qi =0;
      dQ=0.;
      Mr=0.;
      Qi_Zr =0;
      dQ_Zr=0.;
      Mr_Zr=0.;
      Gamma = 1.0;
      VTP = 0.;
      BetaTP = 0;
      GammaTP = 1.0;
      END;
   };
   // Virtual Function to TOF
   Float_t GetTimeOfFlight()
   {
      START;
      // FPMW_CATS2
      if(TACS)
         return TACS->GetCal(2);
      else
         return 0;
      END;
   };

   // Virtual Function to TOF
   Bool_t Calculate()
   {
      START;
      Init();

      // TOF Form
      //t1->SetAlias("mT2","T_TMW2_FPMW0_C-66.+223+52.
      // +0.5*(FPMWPat_0RawNr==7)
      // +0.5*(FPMWPat_0RawNr==6)
      // +0.*(FPMWPat_0RawNr==5)
      // -0.9*(FPMWPat_0RawNr==4)
      // -1.9*(FPMWPat_0RawNr==3)
      //x -1.7*(FPMWPat_0RawNr==2)
      // +1.3*(FPMWPat_0RawNr==9)
      // +1.3*(FPMWPat_0RawNr==10)
      // +0.5*(FPMWPat_0RawNr==11)
      // +0.5*(FPMWPat_0RawNr==12)
      // +0.3*(FPMWPat_0RawNr==13)
      // -0.2*(FPMWPat_0RawNr==14)
      // -0.5*(FPMWPat_0RawNr==15)
      // +0.6*(FPMWPat_0RawNr==16)
      // +0.0*(FPMWPat_0RawNr==17)
      // -0.3*(FPMWPat_0RawNr==18)
      // +0.*(FPMWPat_0RawNr==19)
      // +0.*(FPMWPat_0RawNr==1)
      // +0.*(FPMWPat_0RawNr==0)");
      Double_t T_T2_F0 = TACS->GetCal(2);
      T =   T_T2_F0 +223+52.-66.;
      //T += 4.5; // After FPMW0-1 Position Chnage

      V = 0;
      Beta = 0;
      Gamma =  0;

      Double_t TFF=0;
      Double_t VFF=0;
      Double_t BetaFF=0;
      Double_t GammaFF=0;
      Double_t MFF=0;
      Double_t M_QFF=0;
      Double_t QFF=0;

      //DetManager *DM = DetManager::getInstance();

      // Get MWNr
      //BaseDetector *FPMWPat1 = DM->GetDetector("FPMWPat_1");

         Int_t MW_Nr = -1;

         if(FPMWPat0->GetRawM() == 1)
         {
            //           FPMWPat0->PrintRaw();
            MW_Nr = FPMWPat0->GetRawNrAt(0);
            //         cout << MW_Nr << endl;
         }
         else
            MW_Nr=-1;

         // Time Offset from 23/05/2021 22:28
         switch (MW_Nr) {
            case 0:
               T += 0;
               break;
            case 1:
               T += 0;
               break;
            case 2:
               T += -1.7;
               break;
            case 3:
               T += -1.9;
               break;
            case 4:
               T += -0.9;
               break;
            case 5:
               T += 0.;
               break;
            case 6:
               T += 0.5;
               break;
            case 7:
               T += 0.5;
               break;
            case 8:
               T += 0;
               break;
            case 9:
               T += +1.3;
               break;
            case 10:
               T += +1.3;
               break;
            case 11:
               T += +0.5;
               break;
            case 12:
               T += +0.5;
               break;
            case 13:
               T += +0.3;
               break;
            case 14:
               T += -0.2;
               break;
            case 15:
               T += -0.5;
               break;
            case 16:
               T += +0.6;
               break;
            case 17:
               T += 0.0;
               break;
            case 18:
               T += -0.3;
               break;
            case 19:
               T += 0;
               break;
            case 20:
               T += 0;
               break;
            default:
               T = -1500.;
         }


         // Time Offset from DR

         //t1->SetAlias("mT2","T+0*(FPMWPat_0RawNr==0)
//         + 0*(FPMWPat_0RawNr==1)
//               + 4.21316*(FPMWPat_0RawNr==2)
//               + 3.17432*(FPMWPat_0RawNr==3)
//               + 2.2524*(FPMWPat_0RawNr==4)
//               + 1.53673*(FPMWPat_0RawNr==5)
//               + 1.17762*(FPMWPat_0RawNr==6)
//               + 0.714603*(FPMWPat_0RawNr==7)
//               + 0.650209*(FPMWPat_0RawNr==8)
//               + 0.580169*(FPMWPat_0RawNr==9)
//               + 0.594421*(FPMWPat_0RawNr==10)
//               + 0.616872*(FPMWPat_0RawNr==11)
//               + 0.800988*(FPMWPat_0RawNr==12)
//               + 1.01011*(FPMWPat_0RawNr==13)
//               + 1.37615*(FPMWPat_0RawNr==14)
//               + 1.5123*(FPMWPat_0RawNr==15)
//               + 1.91537*(FPMWPat_0RawNr==16)
//               + 2.21096*(FPMWPat_0RawNr==17)
//               + 2.69025*(FPMWPat_0RawNr==18)
//               + 2.79234*(FPMWPat_0RawNr==19)");



         switch (MW_Nr) {
            case 0:
               T += 0;
               break;
            case 1:
               T += 0;
               break;
            case 2:
               T += 4.21316;
               break;
            case 3:
               T += 3.17432;
               break;
            case 4:
               T += 2.2524;
               break;
            case 5:
               T += 1.53673;
               break;
            case 6:
               T += 1.17762;
               break;
            case 7:
               T += 0.714603;
               break;
            case 8:
               T += 0.650209;
               break;
            case 9:
               T += 0.580169;
               break;
            case 10:
               T += 0.594421;
               break;
            case 11:
               T += 0.616872;
               break;
            case 12:
               T += 0.800988;
               break;
            case 13:
               T += 1.01011;
               break;
            case 14:
               T += 1.37615;
               break;
            case 15:
               T += 1.5123;
               break;
            case 16:
               T += 1.91537;
               break;
            case 17:
               T += 2.21096;
               break;
            case 18:
               T += 2.69025;
               break;
            case 19:
               T += 2.79234;
               break;
            case 20:
               T += 0;
               break;
            default:
               T = -1500.;
         }
         TFF = T; // - 8. ;


         // Calculate D
         //   t1->SetAlias("mD2","Path-24.0/cos(TP_Theta/1000.)/cos(TP_Phi/1000.)+(768.4-760.)/cos(Tf/1000.)");
         if(Rec->IsPresent())
         {
         if(FP->GetPf() > -1000 && FP->GetTf() > -1000 && TP->GetTheta() > -1000 && TP->GetPhi() > -1000)
         {
            D = Rec->GetPath()
                  - (24.)/cos(TP->GetPhi()/1000.)/cos(TP->GetTheta()/1000.)
                  + (768.4-760.)/cos(FP->GetTf()/1000.) ;
//cos(FP->GetPf()/1000.)
            V = D/T;
            Beta =V/29.9792458;

            Gamma = 1./sqrt(1.-powf(Beta,2.f));
            M_Q = Rec->GetBrho()/3.105/Beta/Gamma;

            VFF = D/TFF;
            BetaFF =VFF/29.9792458;

            GammaFF = 1./sqrt(1.-powf(BetaFF,2.f));
            M_QFF = Rec->GetBrho()/3.105/BetaFF/GammaFF;


#ifdef DEBUG
            if(MW_Nr>-1)
            {
               cout << "D " << D << endl;
               cout << "M_Q " << M_Q << endl;
               cout << "T " << T << " " << T_T2_F0 << " " << MW_Nr << endl;

            }
#endif
         }


         VTP = 0;
         BetaTP = 0;
         GammaTP =  0;
         E = 0;
         dE = 0;
         // Calculate Energy
         //  t1->SetAlias("mE2","1.*(IC[0]+IC[1])+2.*(IC[2]+IC[3]+IC[4]+0.*IC[5]+0.*IC[6])");
         //       t1->SetAlias("mE2","1.8*IC[0]+IC[1]+2.*(IC[2]+IC[3]+IC[4]+IC[5])");
         //       t1->SetAlias("mM2","(0.00338*mE2)/931.5016/(mGamma2-1.)");
         //       t1->SetAlias("mQ2","mM2/mM_Q2");
         Float_t  IC0 = ICN->GetSubDet(0)->GetCal(0);
         Float_t  IC1 = ICN->GetSubDet(0)->GetCal(1);
         Float_t  IC2 = ICN->GetSubDet(0)->GetCal(2);
         Float_t  IC3 = ICN->GetSubDet(0)->GetCal(3);
         Float_t  IC4 = ICN->GetSubDet(0)->GetCal(4);
         Float_t  IC5 = ICN->GetSubDet(0)->GetCal(5);
         Float_t  IC6 = ICN->GetSubDet(0)->GetCal(6);
         dE = 1.8*IC0 + IC1;
         E = dE + 2*(IC2+IC3+IC4+IC5);
         //Float_t E1 = 1.*(IC0+IC1)+2.*(IC2+IC3+IC4+0.*IC5+0.*IC6);
         //E1 *= 0.004;
         Float_t E1 = 1.8*IC0+IC1+2.*(IC2+IC3+IC4+IC5);
         E1 *= 0.00327;
#ifdef DEBUG
         cout << "IC0 " << IC0 << endl;
         cout << "IC1 " << IC1 << endl;
         cout << "IC2 " << IC2 << endl;
         cout << "IC3 " << IC3 << endl;
         cout << "IC4 " << IC4 << endl;
         cout << "IC5 " << IC5 << endl;
         cout << "IC6 " << IC6 << endl;
         cout << "E1 " << E1 << endl;
#endif

         SetCalData(0,dE);
         SetCalData(1,E);



         if(Beta>0 && Beta <0.3 && Gamma>1.&&E1>1.)
         {

            M = E1/931.5016/(Gamma-1.);
            M_Q = Rec->GetBrho()/3.105/Beta/Gamma;
            Q = M/M_Q;

            MFF = E1/931.5016/(GammaFF-1.);
            M_QFF = Rec->GetBrho()/3.105/BetaFF/GammaFF;
            QFF = MFF/M_QFF;

         }
      }
      SetCalData(2,T);
      SetCalData(3,V);
      SetCalData(4,D);
      SetCalData(5,Beta);
      SetCalData(6,Gamma);
      SetCalData(7,M);
      SetCalData(8,M_Q);
      SetCalData(9,Q);
//      SetCalData(13,BetaTP);
//      SetCalData(14,VTP);
//      SetCalData(15,D_1);
//      //SetCalData(16,QAr);

      SetCalData(10,TFF);
      SetCalData(11,VFF);
      SetCalData(12,BetaFF);
      SetCalData(13,GammaFF);
      SetCalData(14,MFF);
      SetCalData(15,M_QFF);
      SetCalData(16,QFF);
//      SetCalData(17,Rec->GetBrho());

      if(T > 0 && V > 0 && M_Q > 0 && M > 0 && Beta > 0 && Gamma > 1.0)
      {
         isPresent=true;
         //PrintCal();

      }

      return (isPresent);




      END;
   };


   Bool_t Treat(void)
   {
      START;
      Ctr->at(0)++;
      //    cout << " ID Treat" << endl;
      if((Rec && Rec->IsPresent()))
      {
         Ctr->at(2)++;
         Calculate();
#ifdef DEBUG
         PrintCal();
#endif
      }
      return (isPresent);
      END;
   }

   BaseDetector * FPMWPat0;

};

#endif
