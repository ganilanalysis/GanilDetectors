/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _DC_CLASS
#define _DC_CLASS
/**
 * @file   DriftChamber.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   December, 2012
 * @version 0.1
 * @todo Read Calibration and thresholds
 * @brief  DriftChamber Class
 * 
 * DriftChamber class
 */

#include "Defines.hh"
#include "BaseDetector.hh"

class DriftChamber : public BaseDetector
{
public:
  DriftChamber(const Char_t * Name,  //!< Detector Name
				  UShort_t NDetectors,   //!< Number of sub detectors
				  Bool_t RawData,        //!< Has Raw Data
				  Bool_t CalData,        //!< Has Calibrated Data
				  Bool_t DefaultCalibration,//!< Used Default Calibration
				  const Char_t * NameExt //!< Name extension
               );
  ~DriftChamber(void);

  //! Clear At the end of event;
  void Clear(void);
  //! True if wires are present
  Bool_t PresentWires(void) {return fPresentWires;};
  //! True X position is present
  Bool_t PresentX(void) {return fPresentX;};
  //! True Y position is present
  Bool_t PresentY(void) {return fPresentY;};

  Bool_t Treat(void);   //!< Treat the data
  void ReadCalibration(void);   //!< Specific reading of Calibration and thresholds

  //! Generate Default Calibration File
  void GenerateDefaultCalibration(Char_t *fName);


#ifdef PRESORT_DC
  //! Set Presorted Data Creation flag
  void SetCreatedPreSort(Bool_t v);
#endif


#ifdef WITH_ROOT
  //! Create 2D Histograms for Calculated Parameters
  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Create 1D Histograms for Calculated Parameters
  void CreateHistogramsCal1D(TDirectory *Dir);
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par, //!< Pointer to Parameter Names Table
							Map* Map         //!< Pointer to Detectors Map
							); 
  Bool_t TreatX(void);   //!< Treat X position and return True if X within bounds [-1500,0]
  Bool_t TreatY(void);   //!< Treat Y position and return True if Y within bounds [0, ...]
 
  UShort_t GetMaxStrip(UShort_t i){ if(i<3) return MaxStrip[i];};
  
  //! Get X focal Plane in mm
  inline Float_t GetX(void){return Cal[0];};
  //! Get Theta focal Plane in mrad
  inline Float_t GetY(void){return Cal[1];};
  

protected:
  //! Allocate subcomponents
  void AllocateComponents(void);
  //! Look for Multiple Peaks
  Bool_t CheckMultiplePeaks(UShort_t *FStrip //!< Array of Found Strips Number 
                            ); 
  //! Check that found strip are neigbours 
  Bool_t CheckNeighbours(UShort_t *FStrip //!< Array of Found Strips Number 
                         ); 
  //! Return position determined weigted average method
  Float_t WeightedAverage(UShort_t *FStrip, //!< Array of Found Strips Number 
                          UShort_t NStrips  //!< Number of Strips
                          ); 
  void ReadReference(MIFile *IF); //!< Read reference information from Calibration File
  void SetId(const Char_t *Name); //!< Set Unique Id of the Chamber (0-3)
  Float_t SECHIP(UShort_t *FStrip); //!< Return position determined by Squente Hyperbolique

  UShort_t DCId;  //!< Unique Id of the Chamber (0-3)
  Float_t DriftVelocity; //!< DriftVelocity in mm/ns
  
  Bool_t fPresentWires;
  Bool_t fPresentX;
  Bool_t fPresentY;

  // Array of max Strip Number ordered
  UShort_t *MaxStrip;
  UShort_t NStrips; //! Number of strips required
#ifdef PRESORT_DC
  Bool_t CreatedPreSort;
#endif
  
#ifdef DC_CHARGE
  UShort_t Det_Charge_Offset;
#endif


};
#endif
