/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _IONISATIONCHAMBER_CLASS
#define _IONISATIONCHAMBER_CLASS
/**
 * @file   IonisationChamber.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   January, 2013
 * @version 0.1
 * @todo Add/Check Tracking correlations
 * @brief  IonisationChamber Class
 * 
 * IonisationChamber  class :  Calibrate  each row  of the  Ionization
 * Chamber and  calculate total energy  Loss.  If row  multiplicity is
 * larger that 1, the event is rejected. The sum energy is set to 0.
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#include "FocalPosition.hh"

class IonisationChamber : public BaseDetector
{
public:
  //! Constructor 
  IonisationChamber(const Char_t * Name,       //!< Detector Name 
                    UShort_t NDetectors,       //!< Number of sub detectors
                    UShort_t NRaw,             //!< Number of Raws
                    Bool_t RawData,            //!< Has Raw Data
                    Bool_t CalData,            //!< Has Calibrated Data
                    Bool_t DefaultCalibration, //!< Used Default Calibration
                    const Char_t * NameExt     //!< Name Extension
                    ); 
  ~IonisationChamber(void);
 
  //! Return Total energy deposited in the Ionisation Chamber (Mev)
  inline Float_t GetETotal(void) {return ETotal;}
  //! Return Total energy deposited in the Ionisation Chamber (Mev)
  inline Float_t GetETotalM1(void) {return ETotalM1;}
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);
 
protected:
#ifdef WITH_ROOT
  //!< Create Histograms of Calibrated parameters
  void CreateHistogramsCal1D(TDirectory *Dir);
  //!< Create Histograms of Calibrated parameters
  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif

  void AllocateLocalArrays(void);  //!< Allocate specific arrays for IC
  void ClearCal(void);        //!< Clear Calibrated Data
  void DeAllocateLocalArrays(void);//!< DeAllocate specific arrays for IC
  //! Allocate subcomponents
  void AllocateComponents(void);


  FocalPosition *FP;//!< Pointer to FocalPosition information
  Float_t *E_Row;  //!< Table of Row Energies
  Float_t *E_Col;  //!< Table of Column Energies
  Float_t *dE;  //!< Table of dE Column Energies
  Float_t ETotal;  //!< Total Energy in IC
  Float_t ETotalM1;  //!< Total Energy in IC Multiplicity 1 per row, all raws
  UShort_t *M_Row; //!< Table of Row Multiplicity
  UShort_t N_Row; //!< Number of Ionization Chamber rows
  UShort_t N_Col; //!< Number of Ionization Chamber columns
  

};
#endif
