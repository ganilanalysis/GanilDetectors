/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _IDPL_CLASS
#define _IDPL_CLASS
/**
 * @file   IdPlastic.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   Febuary, 2013
 * @brief  Identification Class
 * 
 * Identification with Plastic Class
 */

#include "Defines.hh"
#include "Identification.hh"
class IdPlastic : public Identification
{
public :
  //! Constructor with Plastic
  IdPlastic(const Char_t * Name, //!< Detector Name
				Reconstruction *Reco,  //!< Pointer to Reconstruction
				FocalPosition *FocalPos,  //!< Pointer to  FocalPosition
				IonisationChamber *ICD,  //!< Pointer to IonisationChamber
				Plastic *PLD  //!< Pointer to Plastic
				)
	 : Identification(Name,Reco,FocalPos,ICD,PLD)
  {
	 START;
	 END;
  }; 
  ~IdPlastic(void)
  {
	 START;
	 END;
  }; //!< Destructor
  
  // Virtual Function to Get Residual energy
  Float_t GetResidualEnergy()
  {
	 START;
	 return PL->GetEnergy();
	 END;
  };
  // Virtual Function to TOF
  Float_t GetTimeOfFlight()
  {
	 START;
	 return PL->GetTime();
	 END;
  };
  

};

#endif
