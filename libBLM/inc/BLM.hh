#ifndef BLM_H
#define BLM_H

#include "Defines.hh"
#include "BaseDetector.hh"
#include "TCutG.h"

class BLM : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  BLM(const Char_t * Name,
                  UShort_t NDetectors,
                  Bool_t RawData,
                  Bool_t CalData,
                  Bool_t DefaultCalibration,
                  const Char_t * NameExt);
  ~BLM(void);



#ifdef WITH_ROOT
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
  //! Custom 2D Histograms
//  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Custom 1D Histograms
  void CreateHistogramsCal1D(TDirectory *Dir);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);


  void NewCycle(ULong64_t TS);

protected:

//! Allocate subcomponents
  void AllocateComponents(void);

  ULong64_t CycleTS;
  ULong64_t TStart;
  ULong64_t MicTSRef;
  ULong64_t MicTSLast;
  Bool_t NewMacCycle;
  // Cycle Histogram
  TH1F* MacTime1;
  TH1F* MicTime1;
  TH1F* MicTime1_10;
  TH1F* MacTime2;
  TH1F* MicTime2;
  TH1F* MicTime2_10;
  TH1F* MicDiffTime;
  TH1F* MicDiffTime2;

  TH1F* hTime;
  TH1F* QCal1;
  TH1F* QCal2;
  TH1F* QRaw1;
  TH1F* QRaw2;


  UInt_t *FreqArray;
  UInt_t *FreqArrayG;
  UShort_t FreqArraySize;
  Int_t FreqBinsW;
  Int_t Pulse;
};


#endif // BLM_H
