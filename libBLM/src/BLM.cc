/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  trueyou are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "BLM.hh"

BLM::BLM(const Char_t *Name,
         UShort_t NDetectors,
         Bool_t RawData,
         Bool_t CalData,
         Bool_t DefaultCalibration,
         const Char_t *NameExt
         )
    : BaseDetector(Name, 0, false, CalData, DefaultCalibration,false,NameExt)
{
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;
    DetManager *DM = DetManager::getInstance();
    CycleTS = 0;
    MicTSRef = 0;
    NewMacCycle = false;
    AllocateComponents();

    Pulse = 1e9; // in ns
    FreqBinsW = 100*1e3; // in ns
    FreqArraySize = Pulse/FreqBinsW;
    FreqArray = AllocateDynamicArray<UInt_t>(FreqArraySize);
    FreqArrayG = AllocateDynamicArray<UInt_t>(FreqArraySize);
    for(Int_t i=0;i < FreqArraySize; i++ )
    {
        FreqArray[i] = 0;
        FreqArrayG[i] = 0;
    }

    MicTSLast = 0;

    END;
}
BLM::~BLM(void)
{
    START;

    FreqArray = FreeDynamicArray<UInt_t>(FreqArray);
    FreqArrayG = FreeDynamicArray<UInt_t>(FreqArrayG);


    END;

}


void BLM::NewCycle(ULong64_t TS)
{
    START;
    cout << "New Cycle - Delta TS " << TS-CycleTS <<  "  Time " << TS-TStart << " " <<  (TS-TStart)*1e-8 << endl;

    NewMacCycle = true;
    CycleTS = TS;

    // Fill Cumulative Histogram  and Zero Array

//    for(Int_t i=0;i < FreqArraySize; i++ )
//    {
//        if(CycleTS >0)
//        {
//            hCycle->Fill(i*FreqBinsW/1e3,FreqArray[i]);
//            hCycleG->Fill(i*FreqBinsW/1e3,FreqArrayG[i]);

//            hTime->Fill((TS-TStart)*1.e-8,FreqArray[i]);

//        }
//        FreqArray[i] = 0;
//        FreqArrayG[i] = 0;
//    }
    END;
}


void BLM::AllocateComponents(void)
{
    START;
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Bool_t PosInfo = false;
    Char_t pname[100];
    Int_t MaxMult = 10;
    // Charges
    BaseDetector *Energies = new BaseDetector("BLMQ",2,RawData,CalData,DefCal,false,"",0,1,MaxMult);
    Energies->SetRawHistogramsParams(64000,0,63999);
    Energies->SetGateRaw(0,63999);
    if(fCalData)
    {
        Energies->SetCalName("BLMQ1_Cal",0);
        Energies->SetCalName("BLMQ2_Cal",1);

    }
    Energies->SetCalHistogramsParams(16000,0,100);
    AddComponent(Energies);

    // Gates
    BaseDetector *Gates = new BaseDetector("MicSync",1,RawData,false,DefCal,false,"",0,1,MaxMult);
    AddComponent(Gates);

    // Sync
    BaseDetector *Sync = new BaseDetector("Sync",1,RawData,false,DefCal,false,"",0,1,MaxMult);
    AddComponent(Sync);

    END;
}

void BLM::SetParameters(Parameters* Par,Map* Map)
{
    START;

    if(isComposite)
    {

        Char_t PName[20];
        Int_t Id=0;
        // Charges
        sprintf(PName,"BLM_Q1");
        DetList->at(0)->SetParameterName(PName,0);
        DetList->at(0)->SetRawHistogramsParams(64000,0,63999);
        DetList->at(0)->SetGateRaw(0,63999);

        sprintf(PName,"BLM_Q2");
        DetList->at(0)->SetParameterName(PName,1);
        DetList->at(0)->SetRawHistogramsParams(64000,0,63999);
        DetList->at(0)->SetGateRaw(0,63999);

        // Gates
        sprintf(PName,"BLM_MicSync");
        DetList->at(1)->SetParameterName(PName,0);
        DetList->at(1)->SetRawHistogramsParams(100,0,63999);
        DetList->at(1)->SetGateRaw(0,63999);

        // Synchro
        sprintf(PName,"Sync");
        DetList->at(2)->SetParameterName(PName,0);
        DetList->at(2)->SetRawHistogramsParams(100,0,63999);
        DetList->at(2)->SetGateRaw(0,63999);

    }

    NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
    for (UShort_t i = 0; i < DetList->size(); i++)
        DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);

    END;
}

Bool_t BLM::Treat(void)
{
    START;
    Ctr->at(0)++;

    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->Treat();
        }
    }

    // Macro Sync

    if(DetList->at(2)->GetRawM()>0)
    {

        if(CycleTS == 0)
        {
            TStart = DetList->at(2)->GetRawTSAt(0);
            cout << TStart << endl;
        }
        NewCycle(DetList->at(2)->GetRawTSAt(0));
    }

    // Micro Cycle
    if(DetList->at(1)->GetRawM()>0)
    {

        if(NewMacCycle == true )
        {
            MicTSRef = DetList->at(1)->GetRawTSAt(0);
            NewMacCycle = false;
        }

        // Fill Histo TS relative to MicRefTS in us
        MicDiffTime->Fill((DetList->at(1)->GetRawTSAt(0)-MicTSRef)*10/1000.);
        MicDiffTime2->Fill((DetList->at(1)->GetRawTSAt(0)-MicTSRef)*10/1000.);
      //  cout << DetList->at(1)->GetRawTSAt(0) - MicTSLast << " " << endl;

        MicTSLast = DetList->at(1)->GetRawTSAt(0);
    }


    if(DetList->at(0)->GetRawM()>0)
    {

        // Exemple of Loop over MultiHit
        if(fMaxMult>1)
        {
            for(UShort_t k=0; k<DetList->at(0)->GetMHRawM(0);k++)
            {
                QRaw1->Fill(DetList->at(0)->GetMHRaw(k,0));
            }
            for(UShort_t k=0; k<DetList->at(0)->GetMHRawM(1);k++)
            {
                QRaw2->Fill(DetList->at(0)->GetMHRaw(k,1));
            }

        }


        if(DetList->at(0)->GetCal(0)>0)
        {
            QCal1->Fill(DetList->at(0)->GetCal(0));
            if(!NewMacCycle)
            {
                MacTime1->Fill((DetList->at(0)->GetRawTS(0) - CycleTS)*10/1000.); // Macro Cycle
                MicTime1->Fill((DetList->at(0)->GetRawTS(0) - MicTSRef)*10/1000.); // Macro Cycle
                MicTime1_10->Fill((DetList->at(0)->GetRawTS(0) - MicTSRef)*10/1000.); // Macro Cycle
            }
        }
        if(DetList->at(0)->GetCal(1)>0)
        {
            QCal2->Fill(DetList->at(0)->GetCal(1));
            if(!NewMacCycle)
            {
                MacTime2->Fill((DetList->at(0)->GetRawTS(1) - CycleTS)*10./1000.); // Macro Cycle
                MicTime2->Fill((DetList->at(0)->GetRawTS(1) - MicTSRef)*10/1000.); // Macro Cycle
                MicTime2_10->Fill((DetList->at(0)->GetRawTS(1) - MicTSRef)*10/1000.); // Macro Cycle
            }
        }

    }
    return(isPresent);
    END;
}



#ifdef WITH_ROOT
void BLM::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;

    // Set histogram Hierarchy
    for(UShort_t i = 0;i<DetList->size();i++)
    {
        DetList->at(i)->SetMainHistogramFolder("");
    }


    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(true);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
            }
        }
    }
    else if(fMode == MODE_D2R || fMode == MODE_D2A)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
            SetOutAttachRawI(true);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(true);
                DetList->at(i)->SetHistogramsRawSummary(false);

                DetList->at(i)->SetOutAttachRawV(false);
                //DetList->at(i)->SetOutAttachRawMult(true);
                DetList->at(i)->SetOutAttachRawMult(false);
                DetList->at(i)->SetOutAttachRawI(true);
                DetList->at(i)->SetOutAttachTS(true);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalI(true);

            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}

void BLM::CreateHistogramsCal1D(TDirectory *Dir)
{
    START;
    string Name;

    BaseDetector::CreateHistogramsCal1D(Dir);

    Dir->cd("");

    if(SubFolderHistRaw1D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistRaw1D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    if(!HRaw1D)
        HRaw1D = new vector<TH1*>;
    if(!HRawMap1D)
    {
        HRawMap1D = new vector<vector<TH1*> >;
        HRawMap1D->resize(NumberOfDetectors+16,vector<TH1*>(0));
    }


    hTime = new TH1F("hTime","hTime",100000,0,100000);
    hTime->SetXTitle(Form("T (sec)"));
    HRaw1D->push_back(hTime);
    HRawMap1D->at(NHistograms1DRaw).push_back(hTime);
    H->Add(hTime, GetCurrentHistogramFolder());
    NHistograms1DRaw++;



    MacTime1 = new TH1F("MacTime1","MacTime1",Pulse/1000./100.,0,Pulse/1000./10);
    MacTime1->SetXTitle(Form("DeltaT (us)"));
    HRaw1D->push_back(MacTime1);
    HRawMap1D->at(NHistograms1DRaw).push_back(MacTime1);
    H->Add(MacTime1, GetCurrentHistogramFolder());
    NHistograms1DRaw++;

    MicTime1 = new TH1F("MicTime1","MicTime1",Pulse/1000./100.,0,Pulse/1000./10);
    MicTime1->SetXTitle(Form("DeltaT (us)"));
    HRaw1D->push_back(MicTime1);
    HRawMap1D->at(NHistograms1DRaw).push_back(MicTime1);
    H->Add(MicTime1, GetCurrentHistogramFolder());
    NHistograms1DRaw++;

    MicTime1_10 = new TH1F("MicTime1_10","MicTime1_10",Pulse/1000./100.,0,Pulse/1000./100);
    MicTime1_10->SetXTitle(Form("DeltaT (us)"));
    HRaw1D->push_back(MicTime1_10);
    HRawMap1D->at(NHistograms1DRaw).push_back(MicTime1_10);
    H->Add(MicTime1_10, GetCurrentHistogramFolder());
    NHistograms1DRaw++;

    MacTime2 = new TH1F("MacTime2","MacTime2",Pulse/1000./100.,0,Pulse/1000./10);
    MacTime2->SetXTitle(Form("DeltaT (us)"));
    HRaw1D->push_back(MacTime2);
    HRawMap1D->at(NHistograms1DRaw).push_back(MacTime2);
    H->Add(MacTime2, GetCurrentHistogramFolder());
    NHistograms1DRaw++;

    MicTime2 = new TH1F("MicTime2","MicTime2",Pulse/1000./100.,0,Pulse/1000./10);
    MicTime2->SetXTitle(Form("DeltaT (us)"));
    HRaw1D->push_back(MicTime2);
    HRawMap1D->at(NHistograms1DRaw).push_back(MicTime2);
    H->Add(MicTime2, GetCurrentHistogramFolder());
    NHistograms1DRaw++;

    MicTime2_10 = new TH1F("MicTime2_10","MicTime2_10",Pulse/1000./100.,0,Pulse/1000./100);
    MicTime2_10->SetXTitle(Form("DeltaT (us)"));
    HRaw1D->push_back(MicTime2_10);
    HRawMap1D->at(NHistograms1DRaw).push_back(MicTime2_10);
    H->Add(MicTime2_10, GetCurrentHistogramFolder());
    NHistograms1DRaw++;

    MicDiffTime= new TH1F("MicDiffTime","MicDiffTime",Pulse/1000./100.,0,Pulse/1000./10);
    MicDiffTime->SetXTitle(Form("DeltaT (us)"));
    HRaw1D->push_back(MicDiffTime);
    HRawMap1D->at(NHistograms1DRaw).push_back(MicDiffTime);
    H->Add(MicDiffTime, GetCurrentHistogramFolder());
    NHistograms1DRaw++;

    MicDiffTime2= new TH1F("MicDiffTime2","MicDiffTime2 10ms",Pulse/1000./100.,0,Pulse/1000./100);
    MicDiffTime2->SetXTitle(Form("DeltaT (us)"));
    HRaw1D->push_back(MicDiffTime2);
    HRawMap1D->at(NHistograms1DRaw).push_back(MicDiffTime2);
    H->Add(MicDiffTime2, GetCurrentHistogramFolder());
    NHistograms1DRaw++;

    QCal1 = new TH1F("QCal1","QCal1",16000,0,16000.);
    QCal1->SetXTitle(Form("Q (keV)"));
    HRaw1D->push_back(QCal1);
    HRawMap1D->at(NHistograms1DRaw).push_back(QCal1);
    H->Add(QCal1, GetCurrentHistogramFolder());
    NHistograms1DRaw++;


    QCal2 = new TH1F("QCal2","QCal2",16000,0,16000.);
    QCal2->SetXTitle(Form("Q (keV)"));
    HRaw1D->push_back(QCal2);
    HRawMap1D->at(NHistograms1DRaw).push_back(QCal2);
    H->Add(QCal2, GetCurrentHistogramFolder());
    NHistograms1DRaw++;

    QRaw1 = new TH1F("QRaw1","QRaw1",16000,0,16000.);
    QRaw1->SetXTitle(Form("Q (keV)"));
    HRaw1D->push_back(QRaw1);
    HRawMap1D->at(NHistograms1DRaw).push_back(QRaw1);
    H->Add(QRaw1, GetCurrentHistogramFolder());
    NHistograms1DRaw++;


    QRaw2 = new TH1F("QRaw2","QRaw2",16000,0,16000.);
    QRaw2->SetXTitle(Form("Q (keV)"));
    HRaw1D->push_back(QRaw2);
    HRawMap1D->at(NHistograms1DRaw).push_back(QRaw2);
    H->Add(QRaw2, GetCurrentHistogramFolder());
    NHistograms1DRaw++;


    END;
}




#endif
