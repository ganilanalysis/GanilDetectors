#ifndef NFSNEUTRONDET_H
#define NFSNEUTRONDET_H

#include "Defines.hh"
#include "BaseDetector.hh"
#include "TCutG.h"

class NFSNeutronDet : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  NFSNeutronDet(const Char_t * Name,
                  UShort_t NDetectors,
                  Bool_t RawData,
                  Bool_t CalData,
                  Bool_t DefaultCalibration,
                  const Char_t * NameExt);
  ~NFSNeutronDet(void);



#ifdef WITH_ROOT
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
  //! Custom 2D Histograms
  void CreateHistogramsCal2D(TDirectory *Dir);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);


  void SetNeutron2DCutsFile(char *path)
  {
    START;
    Has2DCuts = true; 
    Load2DCuts(path);
    END;
  }

  void SetNeutronTOFOI( Int_t DetNr, Float_t LL, Float_t UL)
  {
    START;
    if(DetNr < NumberOfSubDetectors)
      {
	Neutron_ROI_LL[DetNr] = LL;
	Neutron_ROI_UL[DetNr] = UL;
      }
    else
      {
	Char_t Message[200];
	sprintf(Message,"try to set gate beyond Detector Size %d/%d ",DetNr, NumberOfSubDetectors);
	//cout << Message << endl;
	MErr * Er= new MErr(WhoamI,0,0, Message);
	throw Er;
      }
    END;
  }
  
  void Load2DCuts(char * path)
  {
    START;
    TFile *CutFile = new TFile(path);
    TCutG *cut = nullptr; 
    CutFile->ls();
    Cuts2D.resize(NumberOfSubDetectors,nullptr);

    for(UShort_t i=0; i < NumberOfSubDetectors; i++)
      {
	cut = (TCutG*) CutFile->Get(Form("Cut_neutron%d",i+1));				
	//	cout << Form("Cut_neutron%d",i+1) << " " << cut << endl;
	if(cut)
	  {
	    //cut->Print();
	    Cuts2D.push_back((TCutG*)cut->Clone());
	  }
	else
	  {
	    Char_t Message[200];
	    sprintf(Message,"Could not find PH_ZC Cut_neutron%d for Neutron Detector %d in 2D Cuts File %s ",i+1, i+1, path);
	    cout << Message << endl;
	    //	    MErr * Er= new MErr(WhoamI,0,0, Message);
	    // throw Er;
	  }
      }
    CutFile->Close();
    END;
  }

  void SetNeutronThreshold(Int_t DetNr, Float_t Th)
  {
    START;
    if(DetNr < NumberOfSubDetectors)
      {
	Neutron_Th[DetNr] = Th;
      }
    else
      {
	Char_t Message[200];
	sprintf(Message,"Trying to set gate Threshold beyond  Detector Size %d/%d ",DetNr, NumberOfSubDetectors);
	//cout << Message << endl;
	MErr * Er= new MErr(WhoamI,0,0, Message);
	throw Er;
      }
    END;
  }

protected:
  
//! Allocate subcomponents
  void AllocateComponents(void);

  Float_t* Neutron_ROI_LL;
  Float_t* Neutron_ROI_UL;
  Float_t* Neutron_Th;
  Bool_t Has2DCuts;
  vector <TCutG*> Cuts2D;
};


#endif // NFSNEUTRONDET_H
