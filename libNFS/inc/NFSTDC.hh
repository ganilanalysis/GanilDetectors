#ifndef NFSTDC_H
#define NFSTDC_H

#include "Defines.hh"
#include "BaseDetector.hh"

class NFSTDC : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  NFSTDC(const Char_t * Name,
                  UShort_t NDetectors,
                  Bool_t RawData,
                  Bool_t CalData,
                  Bool_t DefaultCalibration,
                  const Char_t * NameExt);
  ~NFSTDC(void);



#ifdef WITH_ROOT
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);
  
  ULong64_t TDCTime;

protected:

//! Allocate subcomponents
  void AllocateComponents(void);


};


#endif // NFSTDC_H
