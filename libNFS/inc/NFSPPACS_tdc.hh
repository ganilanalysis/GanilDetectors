#ifndef NFSPPACSTDC_H
#define NFSPPACSTDC_H

#include "Defines.hh"
#include "BaseDetector.hh"
#include "NFSTDC.hh"

class NFSPPACS_tdc : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  NFSPPACS_tdc(const Char_t * Name,
                  UShort_t NDetectors,
                  Bool_t RawData,
                  Bool_t CalData,
                  Bool_t DefaultCalibration,
                  const Char_t * NameExt);
  ~NFSPPACS_tdc(void);



#ifdef WITH_ROOT
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);
  

protected:

//! Allocate subcomponents
  void AllocateComponents(void);


};


#endif // NFSPPACSTDC_H
