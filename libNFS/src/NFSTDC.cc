/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Diego Ramos, diego.ramos@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  trueyou are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "NFSTDC.hh"

NFSTDC::NFSTDC(const Char_t *Name,
         UShort_t NDetectors,
         Bool_t RawData,
         Bool_t CalData,
         Bool_t DefaultCalibration,
         const Char_t *NameExt
         )
  : BaseDetector(Name, 1, false, CalData, DefaultCalibration,false,NameExt,0,1,1,0)
{
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;
    AllocateComponents();
    if(fCalData)
      {
	sprintf(CalNameI[0],"%s_C", DetectorName);
      }
	END;
}
NFSTDC::~NFSTDC(void)
{
    START;
    END;

}

void NFSTDC::AllocateComponents(void)
{
    START;
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = false;
    Bool_t PosInfo = false;

    Char_t dname[300];
    Int_t MaxMult = 1;
    // LowerWord
    sprintf(dname,"%sW",DetectorName);
    BaseDetector *LowerT = new BaseDetector(dname,NumberOfSubDetectors,RawData,false,DefCal,PosInfo,"",0,1,1,0);
    LowerT->UnSetGateRaw();
    AddComponent(LowerT);
    // HigherWord
    sprintf(dname,"%sW_UP",DetectorName);
    BaseDetector *HigherT = new BaseDetector(dname,NumberOfSubDetectors,RawData,false,DefCal,PosInfo,"",0,1,1,0);
    HigherT->UnSetGateRaw();
    AddComponent(HigherT);

    END;
}

void NFSTDC::SetParameters(Parameters* Par,Map* Map)
{
  START;
  for(UShort_t i=0; i< DetList->size(); i++)
    DetList->at(i)->SetParameters(Par,Map);
  END;
}


Bool_t NFSTDC::Treat(void)
{
    START;
    Ctr->at(0)++;

    TDCTime = 0;
   
    if(isComposite)
      {
        for(UShort_t i=0; i< DetList->size(); i++)
	  {
            DetList->at(i)->Treat();
	  }
      }
    //ULong64_t TLow = DetList->at(0)->GetRaw(0);	
    //ULong64_t TUp = DetList->at(1)->GetRaw(0);
    UShort_t TLow = DetList->at(0)->GetRaw(0);	
    UShort_t TUp = DetList->at(1)->GetRaw(0);
    TUp = TUp & 0xFFFF;
    TLow = TLow & 0xFFFF;
    TDCTime = (TUp<<16) + (TLow);

    // SetRawData(0,(Float_t)(TDCTime*0.1)); // TDC resolution 100 ps
    //SetCalData(0,(Float_t)(TDCTime*0.1)); // TDC resolution 100 ps 
    if(DetList->at(0)->GetRawM()>0&&DetList->at(1)->GetRawM()>0)
      //SetCalData(0,(Float_t)(TDCTime*0.1), DetList->at(0)->GetRawTSAt(0));// TDC resolution 100 ps
      SetCalData(0,(Float_t)(TDCTime), DetList->at(0)->GetRawTSAt(0));    
      
    return(isPresent);
    END;
}


 
#ifdef WITH_ROOT
void NFSTDC::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;
    
    // Set histogram Hierarchy
    for(UShort_t i = 0;i<DetList->size();i++)
    {
        DetList->at(i)->SetMainHistogramFolder("");
    }

    SetCalHistogramsParams(1000,0,60000);

    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(true);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
            }
        }
	if(fCalData)
	  {
	    SetHistogramsCal(true); 
	  }
    }
    else if(fMode == MODE_D2R || fMode == MODE_D2A)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
            SetOutAttachRawI(true);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
	    SetOutAttachCalTS(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(false);
                DetList->at(i)->SetHistogramsRawSummary(false);

                DetList->at(i)->SetOutAttachRawV(false);
                //DetList->at(i)->SetOutAttachRawMult(false);
                DetList->at(i)->SetOutAttachRawI(false);
                DetList->at(i)->SetOutAttachTS(false);
                if(DetList->at(i)->HasTime())
                    DetList->at(i)->SetOutAttachTime(true);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
	        DetList->at(i)->SetOutAttachCalI(true);

            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}




#endif
