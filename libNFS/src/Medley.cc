/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  trueyou are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "Medley.hh"

Medley::Medley(const Char_t *Name,
         UShort_t NDetectors,
         Bool_t RawData,
         Bool_t CalData,
         Bool_t DefaultCalibration,
         const Char_t *NameExt
         )
    : BaseDetector(Name, 0, false, CalData, DefaultCalibration,false,NameExt)
{
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;
    AllocateComponents();

    END;
}
Medley::~Medley(void)
{
    START;
    END;

}




void Medley::AllocateComponents(void)
{
    START;
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    BaseDetector *ADet = NULL;
    Char_t Name[20];

    for(Int_t i=0; i<NumberOfSubDetectors; i++ )
    {
        cout << "Medley" << i << endl;
        sprintf(Name,"Medley_%1d",i+1);
        ADet = new  MedleyTelescope(Name,1,RawData,CalData,DefCal,"");
        AddComponent(ADet);
    }

    END;
}

void Medley::SetParameters(Parameters* Par,Map* Map)
{
    START;

    if(isComposite)
    {
        for (UShort_t i = 0; i < DetList->size(); i++)
            DetList->at(i)->SetParameters(Par, Map);
    }

    END;
}

Bool_t Medley::Treat(void)
{
    START;
    Ctr->at(0)++;

    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->Treat();
        }
    }

    return(isPresent);
    END;
}



// #ifdef WITH_ROOT
// void Medley::SetOpt(TTree *OutTTree, TTree *InTTree)
// {
//     START;



//     // Set histogram Hierarchy
//     for(UShort_t i = 0;i<DetList->size();i++)
//     {
//         DetList->at(i)->SetMainHistogramFolder("");
//     }


//     for(UShort_t i = 0;i<DetList->size();i++)
//             DetList->at(i)->SetOpt(OutTTree,InTTree);


//     if(fMode == MODE_WATCHER)
//     {
//         if(fRawData)
//         {
//             SetHistogramsRaw(true);
//         }
//         for(UShort_t i = 0;i<DetList->size();i++)
//         {
//             if(DetList->at(i)->HasRawData())
//             {
//                 DetList->at(i)->SetHistogramsRaw(true);
//             }
//             if(DetList->at(i)->HasCalData())
//             {
//                 DetList->at(i)->SetHistogramsCal(true);
//             }
//         }
//     }
//     else if(fMode == MODE_D2R || fMode == MODE_D2A)
//     {
//         if(fRawData)
//         {
//             SetHistogramsRaw(true);
//             SetOutAttachRawI(true);
//             SetOutAttachRawV(false);
//         }
//         if(fCalData)
//         {
//             SetHistogramsCal(true);
//             SetOutAttachCalI(true);
//             SetOutAttachCalV(false);
//         }
//         for(UShort_t i = 0;i<DetList->size();i++)
//         {
//             if(DetList->at(i)->HasRawData())
//             {
//                 DetList->at(i)->SetHistogramsRaw(true);
//                 DetList->at(i)->SetHistogramsRawSummary(false);

//                 DetList->at(i)->SetOutAttachRawV(false);
//                 //DetList->at(i)->SetOutAttachRawMult(false);
//                 DetList->at(i)->SetOutAttachRawI(true);
//                 DetList->at(i)->SetOutAttachTS(true);
//             }
//             if(DetList->at(i)->HasCalData())
//             {
//                 DetList->at(i)->SetHistogramsCal(true);
//                 DetList->at(i)->SetHistogramsCalSummary(false);
//                 DetList->at(i)->SetOutAttachCalI(true);

//             }
//             DetList->at(i)->OutAttach(OutTTree);
//         }
//         OutAttach(OutTTree);

//     }
//     else
//     {
//         Char_t Message[200];
//         sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
//         MErr * Er= new MErr(WhoamI,0,0, Message);
//         throw Er;
//     }
//     END;
// }




//#endif
