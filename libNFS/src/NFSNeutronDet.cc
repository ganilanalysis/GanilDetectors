/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "NFSNeutronDet.hh"

NFSNeutronDet::NFSNeutronDet(const Char_t *Name,
                         UShort_t NDetectors,
                         Bool_t RawData,
                         Bool_t CalData,
                         Bool_t DefaultCalibration,
                         const Char_t *NameExt
                         )
: BaseDetector(Name, 3*NDetectors+1, false, CalData, DefaultCalibration,false,NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  DetManager *DM = DetManager::getInstance();

  AllocateComponents();

  Neutron_ROI_LL = AllocateDynamicArray<Float_t>(NumberOfSubDetectors);
  Neutron_ROI_UL = AllocateDynamicArray<Float_t>(NumberOfSubDetectors);
  Neutron_Th = AllocateDynamicArray<Float_t>(NumberOfSubDetectors);

  if(CalData)
     {
      for(UShort_t k =0; k<NumberOfSubDetectors; k++)
        {
          sprintf(CalNameI[3*k],"NFS_L%01d",k+1);
          sprintf(CalNameI[3*k+1],"NFS_ZC%01d",k+1);
          sprintf(CalNameI[3*k+2],"NFS_T%01d",k+1);
	  Neutron_Th[k] = 0;
	  Neutron_ROI_LL[k] = 0;
	  Neutron_ROI_UL[k] = 4096;

        }
      sprintf(CalNameI[3*NDetectors],"TOF_Rec");


     }


//  SetCalData(0,100);
//  SetCalData(1,101);

//  cout << "--------**************---------- Pointer  \n" << Cal << " Specific +1 " << Cal+1 <<  "Value : " << *(Cal+1)
//       <<endl;
//        *(Cal+1) = 1000;
//  cout << "--------**************---------- Pointer  \n" << Cal
//       << " Specific +1 " << Cal+1 <<  "Value : " << *(Cal+1)
//       <<endl;

  END;
}
NFSNeutronDet::~NFSNeutronDet(void)
{
  START;

  Neutron_ROI_LL = FreeDynamicArray<Float_t>(Neutron_ROI_LL);
  Neutron_ROI_UL = FreeDynamicArray<Float_t>(Neutron_ROI_UL); 
  Neutron_Th = FreeDynamicArray<Float_t>(Neutron_Th); 

  END;

}


void NFSNeutronDet::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;
  Char_t pname[100];

  // Light Output ~ Energies
  BaseDetector *Energies = new BaseDetector("PH",NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
  {
      for(UShort_t k =0; k<NumberOfSubDetectors; k++)
      {
          if(k == 0) // Det 1
          {
            // Energies->SetParameterName("ADC_0",k);
            sprintf(pname,"PH_%01d",k+1);
            Energies->SetParameterNameInRawTree(pname,k);
          }
          else if(k == 1) // Det 2
          {
            // Energies->SetParameterName("ADC_2",k);
            sprintf(pname,"PH_%01d",k+1);
            Energies->SetParameterNameInRawTree(pname,k);
          }
          else if(k == 2) // Det 3
          {
            // Energies->SetParameterName("ADC_6",k);
            sprintf(pname,"PH_%01d",k+1);
            Energies->SetParameterNameInRawTree(pname,k);
          }
      }
//
//      {
//          sprintf(pname,"ADC_%01d",k);
//          Energies->SetParameterName(pname,k);
//      }
      Energies->SetGateRaw(0,4096);
  }
#ifdef WITH_ROOT
  Energies->SetRawHistogramsParams(4096,0,4096,"");
  Energies->SetCalHistogramsParams(5000,0,5000,"MeV");
#endif
  if(fCalData)
  {

      for(UShort_t k =0; k<NumberOfSubDetectors; k++)
      {
          sprintf(pname,"L_%01d",k+1);
          Energies->SetGateCal(0,5000);
          Energies->SetCalName(pname,k);
      }
  }
  AddComponent(Energies);

  CalData = false;
  // N/gamma discrimination
  BaseDetector *NGDiscri = new BaseDetector("ZC",NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"");
  if(RawData)
  {

      for(UShort_t k =0; k<NumberOfSubDetectors; k++)
      {
          if(k == 0) // Det 1
          {
            // NGDiscri->SetParameterName("ADC_1",k);
            sprintf(pname,"ZC_%01d",k+1);
            NGDiscri->SetParameterNameInRawTree(pname,k);
          }
          else if(k == 1) // Det 2
          {
            // NGDiscri->SetParameterName("ADC_3",k);
            sprintf(pname,"ZC_%01d",k+1);
            NGDiscri->SetParameterNameInRawTree(pname,k);
          }
          else if(k == 2) // Det 3
          {
            //  NGDiscri->SetParameterName("ADC_7",k);
            sprintf(pname,"ZC_%01d",k+1);
            NGDiscri->SetParameterNameInRawTree(pname,k);
          }
      }

//      for(UShort_t k =0; k<NumberOfSubDetectors; k++)
//      {
//          sprintf(pname,"ADC_%01d",k+4);
//          NGDiscri->SetParameterName(pname,k);
//      }
      NGDiscri->SetGateRaw(0,4096);
#ifdef WITH_ROOT
      NGDiscri->SetRawHistogramsParams(4096,0,4096,"");
      NGDiscri->SetRawHistogramsParams(4096,0,4096,"");

#endif
  }
  // if(fCalData)
  // {
  //   NGDiscri->SetCalHistogramsParams(4096,0,4096,"");
  //     for(UShort_t k =0; k<NumberOfSubDetectors; k++)
  //     {
  //         sprintf(pname,"ZC_%01d",k);
  //         NGDiscri->SetCalName(pname,k);
  //     }
  //     NGDiscri->SetGateCal(0,4096);

  // }
  AddComponent(NGDiscri);

  CalData = true;
  // TOF (One for the moment)
  BaseDetector *Time = new BaseDetector("NFS_NeutronT",5,RawData,CalData,DefCal,false,"");
    //      for(UShort_t k =0; k<NumberOfSubDetectors; k++)
//      for(UShort_t k =0; k<5; k++)
      {
	if(RawData){
        Time->SetParameterName("TOF",0);
        sprintf(pname,"TOF1_Raw");
        Time->SetParameterNameInRawTree(pname,0);

        Time->SetParameterName("T_BAF2",1);
        sprintf(pname,"TOFPL_Raw");
        Time->SetParameterNameInRawTree(pname,1);

        Time->SetParameterName("T_NFS_TC",2);
        sprintf(pname,"T_NFS_TC_Raw");
        Time->SetParameterNameInRawTree(pname,2);

        Time->SetParameterName("T_TC",3);
        sprintf(pname,"T_TC_Raw");
        Time->SetParameterNameInRawTree(pname,3);

        Time->SetParameterName("T_HF_TC",4);
        sprintf(pname,"T_HF_TC_Raw");
        Time->SetParameterNameInRawTree(pname,4);
    }
	if(CalData)
	  {
	    //	    sprintf(pname,"TOF%d",k+1);

        Time->SetCalName("TOF1",0);
        Time->SetCalName("TOF_PL",1);
        Time->SetCalName("T_NFS_TC",2);
        Time->SetCalName("T_TC",3);
        Time->SetCalName("T_HF_TC",4);

	  }  
      }

#ifdef WITH_ROOT
  Time->SetRawHistogramsParams(4096,0,4096,"");
  Time->SetCalHistogramsParams(5000,0,5000,"ns","");
#endif
  AddComponent(Time);

  // Conditionned Data
  BaseDetector *CondCalData = new BaseDetector("NFS_Cond",4*NumberOfSubDetectors,false,CalData,false,false,"");
  if(CalData)
    {
      for(UShort_t k =0; k<NumberOfSubDetectors; k++)
        {
          sprintf(pname,"NFS_L%01d_TCut",k+1);
          CondCalData->SetCalName(pname,4*k);
          sprintf(pname,"NFS_ZC%01d_TCut",k+1);
          CondCalData->SetCalName(pname,4*k+1);
          sprintf(pname,"NFS_T%01d_2DGateN",k+1);
          CondCalData->SetCalName(pname,4*k+2);
          sprintf(pname,"NFS_T%01d_2DGateG",k+1);
          CondCalData->SetCalName(pname,4*k+3);
        }
    }
#ifdef WITH_ROOT
  CondCalData->SetCalHistogramsParams(1000,0,5000,"","");
#endif
  AddComponent(CondCalData);


  END;
}

void NFSNeutronDet::SetParameters(Parameters* Par,Map* Map)
{
  START;
  for(UShort_t i=0; i< DetList->size(); i++)
    DetList->at(i)->SetParameters(Par,Map);
  END;
}

Bool_t NFSNeutronDet::Treat(void)
{
  START;
  Ctr->at(0)++;

  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        {
          DetList->at(i)->Treat();
        }
    }
  if(fCalData)
      {


      Float_t TRec = 0;
      Float_t TC_Freq = 160;

      if(DetList->at(2)->GetCal(3)> 100 && DetList->at(2)->GetCal(2)  > 50 &&  DetList->at(2)->GetCal(4)> 50
        )
        {
          Float_t T_TC = DetList->at(2)->GetCal(3);
          Float_t T_HF_TC = DetList->at(2)->GetCal(4);
          Float_t T_NFS_TC = DetList->at(2)->GetCal(2);

          TRec = T_NFS_TC - T_HF_TC + TC_Freq*((int)(T_TC/TC_Freq + 0.5));

        }
        SetCalData(3*NumberOfSubDetectors,TRec);

      for(UShort_t i=0; i < NumberOfSubDetectors; i++)
      {
          // Energies
	if( DetList->at(0)->GetCal(i) > 0 && DetList->at(0)->GetRaw(i) > Neutron_Th[i]  )
              SetCalData(3*i, DetList->at(0)->GetCal(i));
          // ZC
          if( DetList->at(1)->GetRaw(i) > 0  )
              SetCalData(3*i+1, DetList->at(1)->GetRaw(i));
          // TOF / detector
          if( DetList->at(0)->GetCal(i) > Neutron_Th[i]  )
              SetCalData(3*i+2, DetList->at(2)->GetCal(0));
	  else
	      SetCalData(3*i+2, 0);

          //cout << DetList->at(2)->GetCal(0) << " " << Neutron_ROI_LL << " : " << Neutron_ROI_UL << endl;
          // Create Conditionned Data ON Raw TOF
              if(DetList->at(2)->GetRaw(0) > Neutron_ROI_LL[i]
                 && DetList->at(2)->GetRaw(0) < Neutron_ROI_UL[i]
                )
              {
		// PH
                  if( DetList->at(0)->GetRaw(i) > Neutron_Th[i]  )
                      DetList->at(3)->SetCalData(4*i, DetList->at(0)->GetRaw(i));

                  // ZC
                  if( DetList->at(1)->GetRaw(i) > 0  )
                      DetList->at(3)->SetCalData(4*i+1, DetList->at(1)->GetRaw(i));

             }

              // Generated Gated TOF on 2D Cuts (PH,ZC)
              if(Cuts2D[i])
              {
                  if(Cuts2D[i]->IsInside(DetList->at(0)->GetCal(i),DetList->at(1)->GetRaw(i)))
                      DetList->at(3)->SetCalData(4*i+2, DetList->at(2)->GetCal(0));
                  else
                      DetList->at(3)->SetCalData(4*i+3, DetList->at(2)->GetCal(0));
              }
      }





      }
    return(isPresent);
  END;
}



#ifdef WITH_ROOT
void NFSNeutronDet::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  for(UShort_t i = 0;i<DetList->size();i++)
     {
        DetList->at(i)->SetMainHistogramFolder("");
        if(i == 0 )
          {
            DetList->at(i)->SetHistogramsRaw1DFolder("Raw");
            DetList->at(i)->SetHistogramsRaw2DFolder("Raw");
            DetList->at(i)->SetHistogramsCal1DFolder("Cal");
            DetList->at(i)->SetHistogramsCal2DFolder("Cal");
          }
        else if(i == 1 )
          {
            DetList->at(i)->SetHistogramsRaw1DFolder("Raw");
            DetList->at(i)->SetHistogramsRaw2DFolder("Raw");
            DetList->at(i)->SetHistogramsCal1DFolder("Cal");
            DetList->at(i)->SetHistogramsCal2DFolder("Cal");
          }
        else if(i == 2 )
          {
            DetList->at(i)->SetHistogramsRaw1DFolder("Raw");
            DetList->at(i)->SetHistogramsRaw2DFolder("Raw");
            DetList->at(i)->SetHistogramsCal1DFolder("Cal");
            DetList->at(i)->SetHistogramsCal2DFolder("Cal");
          }
        else if(i == 3 )
        {
          DetList->at(i)->SetHistogramsRaw1DFolder("Gated");
          DetList->at(i)->SetHistogramsRaw2DFolder("Gated");
          DetList->at(i)->SetHistogramsCal1DFolder("Gated");
          DetList->at(i)->SetHistogramsCal2DFolder("Gated");
        }
        SetHistogramsCal1DFolder("Cal");
        SetHistogramsCal2DFolder("Cal");
     }


  if(fMode == MODE_WATCHER)
    {
        if(fRawData)
          {
             SetHistogramsRaw(true);
          }
        if(fCalData)
          {
             SetHistogramsCal(true);
          }
        for(UShort_t i = 0;i<DetList->size();i++)
          {
              if(DetList->at(i)->HasRawData())
                {
                  DetList->at(i)->SetHistogramsRaw(true);
                  if(i == 0) DetList->at(i)->SetHistogramsRawSummary(true);
                }
              if(DetList->at(i)->HasCalData())
                 {
                  DetList->at(i)->SetHistogramsCal(true);
                  if(i == 0) DetList->at(i)->SetHistogramsCalSummary(true);
                }
          }
     }
  else if(fMode == MODE_D2R)
     {
        if(fCalData)
          {
             SetHistogramsCal(true);
             SetOutAttachCalI(true);
             SetOutAttachCalV(false);
          }
        for(UShort_t i = 0;i<DetList->size();i++)
          {
             if(DetList->at(i)->HasRawData())
                {
                  DetList->at(i)->SetHistogramsRaw(true);
                  if(i == 0 || i == 1 )
                     {
                        DetList->at(i)->SetHistogramsRawSummary(true);
                        DetList->at(i)->SetOutAttachRawV(false);
                        DetList->at(i)->SetOutAttachRawI(true);
                     }
                  else
                     {
                        DetList->at(i)->SetOutAttachRawI(true);
                     }
                }
             DetList->at(i)->OutAttach(OutTTree);
          }
        OutAttach(OutTTree);

     }
  else if(fMode == MODE_D2A)
     {
        if(fRawData)
          {
             SetHistogramsRaw(false);
             SetOutAttachRawI(false);
             SetOutAttachRawV(false);
          }
        if(fCalData)
          {
             SetHistogramsCal(true);
             SetOutAttachCalI(true);
             SetOutAttachCalV(false);
          }

        for(UShort_t i = 0;i<DetList->size();i++)
          {
             if(DetList->at(i)->HasRawData())
                {
                  DetList->at(i)->SetHistogramsRaw1D(true);
                  DetList->at(i)->SetHistogramsRaw2D(true);

                   if(i == 0 || i == 1)
                     {
                        DetList->at(i)->SetHistogramsRawSummary(true);
                        DetList->at(i)->SetOutAttachRawV(false);
                        DetList->at(i)->SetOutAttachRawI(true);

                     }
                    else
                      {
                         DetList->at(i)->SetOutAttachRawI(true);
                      }
                }
             if(DetList->at(i)->HasCalData())
                {
                  DetList->at(i)->SetHistogramsCal1D(true);
                  DetList->at(i)->SetHistogramsCal2D(true);
                  if(i == 0 )
                     {
                        DetList->at(i)->SetHistogramsCalSummary(true);
                        DetList->at(i)->SetOutAttachCalI(false);
                        DetList->at(i)->SetOutAttachCalV(false);
                     }
		  else  if(i == 3 )
                     {
                        DetList->at(i)->SetHistogramsCalSummary(false);
                        DetList->at(i)->SetOutAttachCalI(false);
                        DetList->at(i)->SetOutAttachCalV(false);
                     }

                  else
                     {
                        DetList->at(i)->SetHistogramsCalSummary(false);
                        DetList->at(i)->SetOutAttachCalI(true);
                     }
                }
             DetList->at(i)->OutAttach(OutTTree);
          }

        OutAttach(OutTTree);
     }
  else if(fMode == MODE_R2A)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
          {
              if(i == 0)
                 DetList->at(i)->SetInAttachRawV(true);
              else
                 DetList->at(i)->SetInAttachRawI(true);
             DetList->at(i)->InAttach(InTTree);
          }

        SetOutAttachCalI(true);

        for(UShort_t i = 0;i<DetList->size();i++)
          {
              if(DetList->at(i)->HasCalData())
                {
                  DetList->at(i)->SetHistogramsCal1D(true);
                  DetList->at(i)->SetHistogramsCal2D(true);
                  if(i == 0)
                     {
                        DetList->at(i)->SetHistogramsCalSummary(true);
                        DetList->at(i)->SetOutAttachCalI(false);
                        DetList->at(i)->SetOutAttachCalV(true);
                     }
                  else
                     {
                        DetList->at(i)->SetHistogramsCalSummary(false);
                        DetList->at(i)->SetOutAttachCalI(true);
                        // DetList->at(i)->SetOutAttachRawI(true);
                     }
                }
             DetList->at(i)->OutAttach(OutTTree);
          }
        OutAttach(OutTTree);

    }
  else if (fMode == MODE_RECAL)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
      {
        if(i == 0)
          DetList->at(i)->SetInAttachCalV(true);
        else
          DetList->at(i)->SetInAttachCalI(true);

        DetList->at(i)->InAttachCal(InTTree);
      }
    SetInAttachCalI(true);
    InAttachCal(InTTree);

    SetOutAttachCalI(true);

    for(UShort_t i = 0;i<DetList->size();i++)
      {
        if(DetList->at(i)->HasCalData())
          {
        DetList->at(i)->SetHistogramsCal1D(true);
        DetList->at(i)->SetHistogramsCal2D(true);
        if(i == 0)
          {
            DetList->at(i)->SetHistogramsCalSummary(true);
            DetList->at(i)->SetOutAttachCalI(false);
            DetList->at(i)->SetOutAttachCalV(true);
          }
        else
          {
            DetList->at(i)->SetHistogramsCalSummary(false);
            DetList->at(i)->SetOutAttachCalI(true);
            // DetList->at(i)->SetOutAttachRawI(true);
          }
          }
        DetList->at(i)->OutAttach(OutTTree);
      }
    OutAttach(OutTTree);
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else
     {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
     }
  END;
}


void NFSNeutronDet::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);

  Dir->cd("");

  if(SubFolderHistCal2D.size()>0)
    {
      Name.clear();
      Name = SubFolderHistCal2D ;
      if(!(gDirectory->GetDirectory(Name.c_str())))
          gDirectory->mkdir(Name.c_str());
      gDirectory->cd(Name.c_str());
    }

  for(Int_t i=0;i< NumberOfSubDetectors ;i++)
  {
    //
    AddHistoCal(GetCalName(3*i),GetCalName(3*i+1),Form("PH_ZC%d",i+1),Form("PH_ZC%d",i+1),1000,0,4096,1000,0,4096);
    AddHistoCal(GetCalName(3*i+2),GetCalName(3*i),Form("TOF_L%d",i+1),Form("TOF_L%d",i+1),1000,0,4096,1000,0,4096);
  }
  
  

  // Adding Conditionned Histograms
  Dir->cd("");
  Name.clear();
  Name = "Gated";
  if(!(gDirectory->GetDirectory(Name.c_str())))
      gDirectory->mkdir(Name.c_str());
  gDirectory->cd(Name.c_str());

 for(Int_t i=0;i< NumberOfSubDetectors;i++)
  {

    // DetList->at(3)->AddHistoCal(DetList->at(3)->GetCalName(4*i+2),Form("TOF%d_2DGated_N",i+1),Form("TOF%d Neutron (2D Gated neutron) ",i+1),1000,0,4096);
    // DetList->at(3)->AddHistoCal(DetList->at(3)->GetCalName(4*i+3),Form("TOF%d_Gated_G",i+1),Form("TOF%d Gamma  (!2D Gated neutron) ",i+1),1000,0,4096);


    DetList->at(3)->AddHistoCal(DetList->at(3)->GetCalName(4*i),DetList->at(3)->GetCalName(4*i+1),Form("PH_ZC%d_TCut",i+1),Form("PH_ZC%d (TCut Neutrons)",i+1),1000,0,4096,1000,0,4096);
    DetList->at(3)->AddHistoCal(DetList->at(3)->GetCalName(4*i),DetList->at(3)->GetCalName(4*i+2),Form("PH_TOF%d_TCut",i+1),Form("PH_TOF%d (TCut Neutrons)",i+1),1000,0,4096,1000,0,4096);
  }



  END;
}




#endif
