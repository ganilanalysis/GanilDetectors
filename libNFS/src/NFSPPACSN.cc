/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Diego Ramos, diego.ramos@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  trueyou are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "NFSPPACSN.hh"

NFSPPACSN::NFSPPACSN(const Char_t *Name,
         UShort_t NDetectors,
         Bool_t RawData,
         Bool_t CalData,
         Bool_t DefaultCalibration,
         const Char_t *NameExt
         )
  : BaseDetector(Name, 2*NDetectors, false, CalData, DefaultCalibration,false,NameExt,0,1,1,0)
{
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;
    AllocateComponents();
    if(fCalData)
      {
      for(UShort_t i=0; i<NumberOfSubDetectors; i++)
	{
	  sprintf(CalNameI[i],"%s%01d_X_cc", DetectorName,i+1);
	  sprintf(CalNameI[i+NumberOfSubDetectors],"%s%01d_Y_cc", DetectorName,i+1);
	}
      }
    END;
}
NFSPPACSN::~NFSPPACSN(void)
{
    START;
    END;

}

void NFSPPACSN::AllocateComponents(void)
{
    START;
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Bool_t PosInfo = false;

    Char_t dname[300];
    Int_t MaxMult = 1;
    
    //Energy
    BaseDetector *PPACE[NumberOfSubDetectors];
    //Position
    BaseDetector *PPACX[NumberOfSubDetectors];
    BaseDetector *PPACY[NumberOfSubDetectors];
    //TOF
    BaseDetector *PPACTOF;
    
    for(UShort_t i=0; i<NumberOfSubDetectors; i++)
      {
	sprintf(dname,"%s%01d_A",DetectorName,i+1);
	PPACE[i] =  new BaseDetector(dname,1,RawData,CalData,DefCal,PosInfo,"",0,1,1,0);
	if(fCalData)
	  {
	    sprintf(dname,"%s%01d_A_C",DetectorName,i+1);
	    PPACE[i]->SetCalName(dname,0);
	  }	
	AddComponent(PPACE[i]);
	sprintf(dname,"%s%01d_X",DetectorName,i+1);
	PPACX[i] = new BaseDetector(dname,1,RawData,CalData,DefCal,PosInfo,"",0,1,1,0);
	if(fCalData)
	  {
	    sprintf(dname,"%s%01d_X_C",DetectorName,i+1);
	    PPACX[i]->SetCalName(dname,0);
	  }	
	AddComponent(PPACX[i]);
	sprintf(dname,"%s%01d_Y",DetectorName,i+1);
	PPACY[i] = new BaseDetector(dname,1,RawData,CalData,DefCal,PosInfo,"",0,1,1,0);
	if(fCalData)
	  {
	    sprintf(dname,"%s%01d_Y_C",DetectorName,i+1);
	    PPACY[i]->SetCalName(dname,0);
	  }	
	AddComponent(PPACY[i]);
      }
    sprintf(dname,"%s_TOF",DetectorName);
    PPACTOF = new BaseDetector(dname,1,RawData,CalData,DefCal,PosInfo,"",0,1,1,0);
    if(fCalData)
      {
	sprintf(dname,"%s_TOF_C",DetectorName);
	PPACTOF->SetCalName(dname,0);
      }	
    AddComponent(PPACTOF);
    
    END;
}

void NFSPPACSN::SetParameters(Parameters* Par,Map* Map)
{
  START;
  NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
  for (UShort_t i = 0; i < DetList->size(); i++)
    DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);
  END;
}


Bool_t NFSPPACSN::Treat(void)
{
    START;
    Ctr->at(0)++;
    if(isComposite)
      {
        for(UShort_t i=0; i< DetList->size(); i++)
	  {
            DetList->at(i)->Treat();
	  }
      }
    for(UShort_t i=0; i<NumberOfSubDetectors; i++)
      {
	if(DetList->at(3*i+1)->GetCalM()>0)
	    SetCalData(i, DetList->at(3*i+1)->GetCalAt(0));
	else
	    SetCalData(i,-1000.);
	if(DetList->at(3*i+2)->GetCalM()>0)
	    SetCalData(i+NumberOfSubDetectors, DetList->at(3*i+2)->GetCalAt(0));         
	else 
	    SetCalData(i+NumberOfSubDetectors,-1000.);
	 
      }
    
    return(isPresent);
    END;
}


 
#ifdef WITH_ROOT
void NFSPPACSN::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;
    
    // Set histogram Hierarchy
    for(UShort_t i = 0;i<DetList->size();i++)
    {
        DetList->at(i)->SetMainHistogramFolder("");
        DetList->at(i)->SetRawHistogramsParams(16384,0,65535);
         //DetList->at(i)->SetCalHistogramsParams(1000,0,60000);
    }

    //SetCalHistogramsParams(1000,0,60000);

    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(true);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
            }
        }
	if(fCalData)
	  {
	    SetHistogramsCal(true); 
	    Char_t PName[100]; 
	    for(UShort_t i=0; i<NumberOfSubDetectors; i++)
	      {
		sprintf(PName,"%s%01d_XY",DetectorName,i+1);
		AddHistoCal(GetCalName(i),GetCalName(i+NumberOfSubDetectors),PName,PName,1000,-500,500,1000,-500,500);
	      }
	  }
    }
    else if(fMode == MODE_D2R || fMode == MODE_D2A)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
            SetOutAttachRawI(true);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(false);
            SetOutAttachCalI(false);
            SetOutAttachCalV(false);
	    // SetOutAttachCalTS(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(false);
                DetList->at(i)->SetHistogramsRawSummary(false);

                DetList->at(i)->SetOutAttachRawV(false);
                //DetList->at(i)->SetOutAttachRawMult(false);
                DetList->at(i)->SetOutAttachRawI(true);
                DetList->at(i)->SetOutAttachTS(true);
                if(DetList->at(i)->HasTime())
                    DetList->at(i)->SetOutAttachTime(true);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
	        DetList->at(i)->SetOutAttachCalI(true);
	        DetList->at(i)->SetOutAttachCalTS(false);

            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}




#endif
