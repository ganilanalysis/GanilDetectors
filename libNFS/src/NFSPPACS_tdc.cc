/****************************************************************************
 *    Copyright (C) 2020 by Antoine Lemasson
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Diego Ramos, diego.ramos@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  trueyou are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "NFSPPACS_tdc.hh"

NFSPPACS_tdc::NFSPPACS_tdc(const Char_t *Name,
         UShort_t NDetectors,
         Bool_t RawData,
         Bool_t CalData,
         Bool_t DefaultCalibration,
         const Char_t *NameExt
         )
  : BaseDetector(Name, 3*NDetectors, false, CalData, DefaultCalibration,false,NameExt,0,1,1,0)
{
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;
    AllocateComponents();
    if(fCalData)
      for(UShort_t i=0; i<NumberOfSubDetectors; i++)
	{
	  sprintf(CalNameI[i],"%s%01d_HF_TOF", DetectorName,i+1);
	  sprintf(CalNameI[i+NumberOfSubDetectors],"%s%01d_Y", DetectorName,i+1);
	  sprintf(CalNameI[i+2*NumberOfSubDetectors],"%s%01d_X", DetectorName,i+1);
	}
    END;
}
NFSPPACS_tdc::~NFSPPACS_tdc(void)
{
    START;
    END;

}

void NFSPPACS_tdc::AllocateComponents(void)
{
    START;
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = false;
    Bool_t PosInfo = false;

    Char_t dname[300];
    Int_t MaxMult = 1;
    
    //Energy
    BaseDetector *PPACE[NumberOfSubDetectors];
    //TDC
    BaseDetector *PPACTop[NumberOfSubDetectors];
    BaseDetector *PPACBottom[NumberOfSubDetectors];
    BaseDetector *PPACRight[NumberOfSubDetectors];
    BaseDetector *PPACLeft[NumberOfSubDetectors];
    BaseDetector *PPACTime[NumberOfSubDetectors];
    BaseDetector *HFTime;
    
    for(UShort_t i=0; i<NumberOfSubDetectors; i++)
      {
	sprintf(dname,"%s%01d_A",DetectorName,i+1);
	PPACE[i] =  new BaseDetector(dname,1,RawData,CalData,DefCal,PosInfo,"",0,1,1,0);
	if(fCalData)
	  {
	    sprintf(dname,"%s%01d_E",DetectorName,i+1);
	    PPACE[i]->SetCalName(dname,0);
	  }	
	AddComponent(PPACE[i]);
	sprintf(dname,"%s%01d_T",DetectorName,i+1);
	PPACTop[i] = new NFSTDC(dname,1,RawData,CalData,false,"");
	AddComponent(PPACTop[i]);
	sprintf(dname,"%s%01d_B",DetectorName,i+1);
	PPACBottom[i] = new NFSTDC(dname,1,RawData,CalData,false,"");
	AddComponent(PPACBottom[i]);
	sprintf(dname,"%s%01d_R",DetectorName,i+1);
	PPACRight[i] = new NFSTDC(dname,1,RawData,CalData,false,"");
	AddComponent(PPACRight[i]);
	sprintf(dname,"%s%01d_L",DetectorName,i+1);
	PPACLeft[i] = new NFSTDC(dname,1,RawData,CalData,false,"");
	AddComponent(PPACLeft[i]);
	sprintf(dname,"%s%01d_TDC",DetectorName,i+1);
	PPACTime[i] = new NFSTDC(dname,1,RawData,CalData,false,"");
	AddComponent(PPACTime[i]);
      }
    sprintf(dname,"%sHF_TDC",DetectorName);
    HFTime = new NFSTDC(dname,1,RawData,CalData,false,"");
    AddComponent(HFTime);

    END;
}

void NFSPPACS_tdc::SetParameters(Parameters* Par,Map* Map)
{
  START;
  for(UShort_t i=0; i< DetList->size(); i++)
    DetList->at(i)->SetParameters(Par,Map);
  END;
}


Bool_t NFSPPACS_tdc::Treat(void)
{
    START;
    Ctr->at(0)++;
   
    if(isComposite)
      {
        for(UShort_t i=0; i< DetList->size(); i++)
	  {
            DetList->at(i)->Treat();
	  }
      }
    for(UShort_t i=0; i<NumberOfSubDetectors; i++)
      {
	if(DetList->at(6*i+1)->GetCalM()>0  && DetList->at(6*i+2)->GetCalM()>0)
	  SetCalData(i+NumberOfSubDetectors, DetList->at(6*i+2)->GetCalAt(0)-DetList->at(6*i+1)->GetCalAt(0)  );
	if(DetList->at(6*i+3)->GetCalM()>0  && DetList->at(6*i+4)->GetCalM()>0)
	  SetCalData(i+2*NumberOfSubDetectors, DetList->at(6*i+4)->GetCalAt(0)-DetList->at(6*i+3)->GetCalAt(0)  );
	if(DetList->at(6*i+5)->GetCalM()>0  && DetList->at(6*NumberOfSubDetectors)->GetCalM()>0)
	  SetCalData(i, -1.*DetList->at(6*i+5)->GetCalAt(0)+DetList->at(6*NumberOfSubDetectors)->GetCalAt(0)  );
      }
    
    return(isPresent);
    END;
}


 
#ifdef WITH_ROOT
void NFSPPACS_tdc::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;
    
    // Set histogram Hierarchy
    for(UShort_t i = 0;i<DetList->size();i++)
    {
        DetList->at(i)->SetMainHistogramFolder("");
        DetList->at(i)->SetCalHistogramsParams(1000,0,60000);;
    }

    //SetCalHistogramsParams(1000,0,60000);

    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(true);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
            }
        }
	if(fCalData)
	  {
	    SetHistogramsCal(true); 
	  }
    }
    else if(fMode == MODE_D2R || fMode == MODE_D2A)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
            SetOutAttachRawI(true);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
	    // SetOutAttachCalTS(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(false);
                DetList->at(i)->SetHistogramsRawSummary(false);

                DetList->at(i)->SetOutAttachRawV(false);
                //DetList->at(i)->SetOutAttachRawMult(false);
                DetList->at(i)->SetOutAttachRawI(false);
                DetList->at(i)->SetOutAttachTS(false);
                if(DetList->at(i)->HasTime())
                    DetList->at(i)->SetOutAttachTime(true);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal(true);
                DetList->at(i)->SetHistogramsCalSummary(false);
	        DetList->at(i)->SetOutAttachCalI(true);
	        DetList->at(i)->SetOutAttachCalTS(true);

            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}




#endif
