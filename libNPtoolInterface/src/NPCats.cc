/****************************************************************************
 *    Copyright (C) 2012-2019 by Antoine Lemasson
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "NPCats.hh"

NPCats::NPCats(const Char_t *Name,
               UShort_t NumberOfStrips,
               Bool_t RawData,
               Bool_t CalData,
               Bool_t DefaultCalibration,
               Bool_t PosInfo,
               const Char_t *NameExt
               )
   : BaseDetector(Name, 0, false, false, DefaultCalibration ,PosInfo,NameExt)
{
   START;
   NumberOfSubDetectors = NumberOfStrips;
   fRawDataSubDetectors  = RawData;


   AllocateComponents();

   END;
}

NPCats::~NPCats(void)
{
   START;

   END;
}


void NPCats::SetOpt(TTree *OutTTree, TTree *InTTree)
{
   START;
   for(UShort_t i=0; i< DetList->size(); i++)
   {
      DetList->at(i)->SetOpt(OutTTree,InTTree);
      if(VerboseLevel >= V_INFO)
         DetList->at(i)->PrintOptions(cout);
      DetList->at(i)->PrintOptions(L->File);
   }

   if(fMode == 0)
   {
      if(fRawData)
      {
         SetHistogramsRaw(true);
      }
      for(UShort_t i = 0;i<DetList->size();i++)
      {
         if(DetList->at(i)->HasRawData())
         {
            DetList->at(i)->SetHistogramsRaw1D(false);
            DetList->at(i)->SetHistogramsRaw2D(true);
            DetList->at(i)->SetHistogramsRawSummary(true);
         }
      }


      if(fCalData)
      {
         SetHistogramsCal(true);
      }
      for(UShort_t i = 0;i<DetList->size();i++)
      {
         if(DetList->at(i)->HasCalData())
         {
            DetList->at(i)->SetHistogramsCal1D(false);
            DetList->at(i)->SetHistogramsCal2D(true);
            DetList->at(i)->SetHistogramsCalSummary(true);
         }
      }
   }

   else if(fMode == MODE_D2R || fMode == MODE_D2A )
   {
      for(UShort_t i = 0;i<DetList->size();i++)
      {
         if(DetList->at(i)->HasRawData())
         {
            DetList->at(i)->SetOutAttachRawV(false);
            DetList->at(i)->SetOutAttachRawI(false);
            DetList->at(i)->SetHistogramsRaw1D(false);
            DetList->at(i)->SetHistogramsRaw2D(true);
            DetList->at(i)->SetHistogramsRawSummary(true);
         }
         DetList->at(i)->OutAttach(OutTTree);
      }
      OutAttach(OutTTree);

   }
   else if(fMode == 3)
   {
      SetInAttachRawV(true);
      InAttach(InTTree);

      SetOutAttachCalI(true);
      OutAttach(OutTTree);
   }
   else
   {
      Char_t Message[200];
      sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
   }


   END;
}

void NPCats::AllocateComponents(void)
{
   START;
   Bool_t RawData = fRawDataSubDetectors;
   Bool_t CalData = fCalData;
   Bool_t DefCal = true;
   Bool_t PosInfo = false;
   Char_t Name[100];
   Char_t PName[100];
   BaseDetector *Det = nullptr;

   for(UShort_t i=0; i < 2; i++)
   {
      // CATS X
      sprintf(Name,"CATS%dX",i+1);
      Det = new BaseDetector(Name,NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"");
      if(RawData)
        Det->SetParameterName("%s_%d");
      AddComponent(Det);

      sprintf(Name,"CATS%dY",i+1);
      Det = new BaseDetector(Name,NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"");
      if(RawData)
        Det->SetParameterName("%s_%d");
      AddComponent(Det);
   }

   END;
}

void NPCats::SetParameters(Parameters* Par,Map* Map)
{
   START;

   for(UShort_t i=0; i< DetList->size(); i++)
      DetList->at(i)->SetParameters(Par,Map);

   END;
}

Bool_t NPCats::Treat(void)
{
   START;
   Ctr->at(0)++;
   for(UShort_t i=0; i< DetList->size(); i++)
      DetList->at(i)->Treat();

   // Now Fill The NPTool Objects

   if(isComposite)
   {
#ifdef WITH_NPTOOL
      for(UShort_t i=0; i< DetList->size(); i++)
      {
        //        cout << i << " " << (i%2)+1 << endl;

         if(i==0)
            for(UShort_t j=0; j < DetList->at(i)->GetRawM(); j++)
               fCATSData->SetStripX(1,DetList->at(i)->GetRawNrAt(j)+1,DetList->at(i)->GetRawAt(j));
         else if(i==1)
            for(UShort_t j=0; j < DetList->at(i)->GetRawM(); j++)
               fCATSData->SetStripY(1,DetList->at(i)->GetRawNrAt(j)+1,DetList->at(i)->GetRawAt(j));
         else if(i==2)
            for(UShort_t j=0; j < DetList->at(i)->GetRawM(); j++)
               fCATSData->SetStripX(2,DetList->at(i)->GetRawNrAt(j)+1,DetList->at(i)->GetRawAt(j));
         else if(i==3)
            for(UShort_t j=0; j < DetList->at(i)->GetRawM(); j++)
               fCATSData->SetStripY(2,DetList->at(i)->GetRawNrAt(j)+1,DetList->at(i)->GetRawAt(j));
      }
#endif

   }
   return(isPresent);
   END;
}
