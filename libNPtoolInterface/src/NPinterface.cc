#include "NPinterface.hh"
/**
 * @file   NPInterface.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   July, 2018
 * @brief  NPTOOL Interface Class
 *
 */

NPInterface::NPInterface(const Char_t *Name,
                         UShort_t NDetectors,
                         Bool_t RawData,
                         Bool_t CalData,
                         Bool_t DefaultCalibration,
                         const Char_t *ConfFileName)
     : BaseDetector(Name, 0,false,false,true,false,"")
{

    START;
#ifdef WITH_NPTOOL
    string detectorfileName = getenv((EM->getPathVar()).c_str());
    detectorfileName += "/Confs/";
    detectorfileName += ConfFileName;

    cout << "Reading Configuration from " <<  detectorfileName << endl;
    NPOptionManager::getInstance()->SetVerboseLevel(0);

    m_NPinputParser = new NPL::InputParser(detectorfileName);
    fMust2Data = new TMust2Data();
    fMugastData = new TMugastData();
#endif
    // Read later From File the configurations
    NMust2Telescope = 4;
    NGaspardDet = 0;
    NTraceDet = 0;

    AllocateComponents();

    END;
}


NPInterface::~NPInterface()
{
    START;
#ifdef WITH_NPTOOL
    m_NPinputParser->Dump();
    delete  m_NPinputParser;
    m_NPinputParser = nullptr;
    delete fMust2Data;
#endif
    END;
}



Bool_t NPInterface::Treat()
{
    START;
    Ctr->at(0)++;

    if(isComposite)
       for(UShort_t i=0; i< DetList->size(); i++)
          DetList->at(i)->Treat();

    END;
}

void NPInterface::Clear()
{
    START;
    BaseDetector::Clear();
#ifdef WITH_NPTOOL
    fMust2Data->Clear();
    fMugastData->Clear();
#endif
    END;
}

void NPInterface::SetParameters(Parameters* Par,Map* Map)
{
   START;
   if(isComposite)
      for(UShort_t i=0; i< DetList->size(); i++)
         DetList->at(i)->SetParameters(Par,Map);
   END;
}

void NPInterface::AllocateComponents()
{
    START;

    Bool_t RawData = true;
    Bool_t CalData = false;
    Bool_t DefCal = true;
    Bool_t PosInfo = false;
    Char_t Name[100];
    Mugast *DetMG = nullptr;
    Must2 *DetMM = nullptr;
    // NPCats *DetCATS = nullptr;
    UShort_t MGNr = 0;
    // Must2 Telescopes
#ifdef WITH_NPTOOL
    std::vector<NPL::InputBlock*> Block = m_NPinputParser->GetAllBlocksWithToken("M2Telescope");
    for(unsigned int i = 0 ; i < Block.size() ; i++){
       cout << "     -> Adding Mugast Must2 "<< i+1 << endl;
       sprintf(Name,"MM%02d",i+1);

       DetMM = new Must2(Name,i+1,RawData,CalData,DefCal,PosInfo,"");
       DetMM->SetRawDataPointer(fMust2Data);
       AddComponent((BaseDetector*) DetMM);
    }
    // Mugast
    Block.empty();
    Block = m_NPinputParser->GetAllBlocksWithToken("Mugast");
    for(unsigned int i = 0 ; i < Block.size() ; i++){
       // Get detector Number
       MGNr = Block[i]->GetInt("detectornumber");
       cout << "     -> Adding Mugast MG "<< MGNr << " of type " << Block[i]->GetMainValue() << endl;
       sprintf(Name,"MG%02d",MGNr);
       DetMG = new Mugast(Name,MGNr, Block[i]->GetMainValue(),RawData,CalData,DefCal,PosInfo,"");
       if(DetMG)
          DetMG->SetRawDataPointer(fMugastData);
       AddComponent((BaseDetector*) DetMG);

    }


#endif
    END;
}

void NPInterface::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
  if(isComposite)
     for(UShort_t i=0; i< DetList->size(); i++)
     {
        DetList->at(i)->SetOpt(OutTTree,InTTree);
     }

  if(fMode == MODE_D2R || fMode == MODE_D2A)
  {
#ifdef WITH_NPTOOL
     OutTTree->Branch("MUST2", "TMust2Data", &fMust2Data);
     OutTTree->Branch("Mugast", "TMugastData", &fMugastData);
#endif

  }

  OutAttach(OutTTree);
  END;
}

