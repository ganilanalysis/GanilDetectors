/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef MUST2_DET
#define MUST2_DET

/**
 * @file   Must2.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   July, 2018
 * @brief  Must2Telescope wrapper Class
 *
 */

#include "Defines.hh"
#include "BaseDetector.hh"

#ifdef WITH_NPTOOL
#include "TMust2Physics.h"
#include "TMust2Data.h"
#include "NPDetectorManager.h"
#include "NPOptionManager.h"
#endif

class Must2 : public BaseDetector
{
public:
    //! Constructor instantiation by type, number of channels, and calibration files
    Must2(const Char_t * Name,
          UShort_t NDetectors,
          Bool_t RawData,
          Bool_t CalData,
          Bool_t DefaultCalibration,
          Bool_t PosInfo,
          const Char_t * NameExt);
    ~Must2(void);

    //! Set Parameter Patterns associated with Detector
    void SetParameters(Parameters* Par,Map* Map);
    //! Treat the data and Fill NPTool Objects
    Bool_t Treat(void);
#ifdef WITH_ROOT
    void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif

#ifdef WITH_NPTOOL
    // Set the Raw Data Pointer
    inline void SetRawDataPointer(TMust2Data *ptr) {fMust2Data = ptr;}
#endif
    double ThetaCalculation (TVector3 A , TVector3 B);
    double PhiCalculation (TVector3 A, TVector3 B);

protected:

    //! Allocate subcomponents
    void AllocateComponents(void);
#ifdef WITH_NPTOOL
    TMust2Data *fMust2Data;
#endif
    static const int mult = 10;

    Int_t MM;
    Double_t ThetaLab[mult];
    Double_t PhiLab[mult];

};

#endif
