/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef IDLISE_HH
#define IDLISE_HH

/**
 * @file   IdLISE.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   May, 2020
 * @brief  IdLISE Class
 *
 * IdLISE class : Get momentum distribution at LISE.
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#include "Caviar.hh"


class IdLISE : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  IdLISE(const Char_t * Name,
                  UShort_t NDetectors,
                  Bool_t RawData,
                  Bool_t CalData,
                  Bool_t DefaultCalibration,
                  const Char_t * NameExt);
  ~IdLISE(void);

  std::vector<double>* vec;



  void AddCut(UShort_t CutNr, const Char_t* Name,const Char_t* CutFileName)
  {
      START;
#ifdef WITH_ROOT

      TFile *CutFile = new TFile(CutFileName);
      TCutG *cut = nullptr;


      cut = (TCutG*) CutFile->Get(Form("dE_AoQ"));
      if(!cut->InheritsFrom("TCutG"))
      {
          Cuts2D[CutNr]= NULL;

          Char_t Message[200];
          sprintf(Message,"dE_AoQ is not of TCutG type in 2D Cuts File %s ",CutFileName);
          cout << Message << endl;
          //	    MErr * Er= new MErr(WhoamI,0,0, Message);
          // throw Er;
      }
      if(cut)
      {
          cut->Print();
          cout << CutNr << endl;
          cout << Cuts2D.size() << endl;
          Cuts2D[CutNr]= (TCutG*)cut->Clone();
          Cuts2DName[CutNr] = Name;
      }
      else
      {
          Cuts2D[CutNr]= NULL;

          Char_t Message[200];
          sprintf(Message,"Could not find dE_AoQ in 2D Cuts File %s ",CutFileName);
          cout << Message << endl;
          //	    MErr * Er= new MErr(WhoamI,0,0, Message);
          // throw Er;
      }

      CutFile->Close();
#endif
      END;
  };
#ifdef WITH_ROOT
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
  void CreateHistogramsCal2D(TDirectory *Dir);
  void CreateHistogramsCal1D(TDirectory *Dir);

#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);
  //! Treat the Wires
  void ReadPosition(void);

  //! Set Reference Brho
  void SetBrhoRef(Float_t Brho);

protected:

  //! Allocate subcomponents
  void AllocateComponents(void);
  Float_t CATSD6Pos;
  Float_t CATSD4Pos;
  Float_t IdLISEPos;
  Float_t BrhoD32;
  Float_t T16_LISE;
//!to define a circle cut in DD6 XY plot [2024 05 28]
	Int_t		XcDD6;
	Int_t		YcDD6;
	Int_t		RDD6;
//! end of circle cut [2024 05 28]
  Int_t   Nparam;
  Int_t   DE_D4min1; 
  Int_t   DE_D4max1;
  Int_t   DE_D4min2;
  Int_t   DE_D4max2;
  Int_t   DE_D4min3;
  Int_t   DE_D4max3;
  Int_t NAlpha 		;
  Int_t NAlpha_X	;
  Int_t   DD6_1_Emin;
  Int_t   DD6_1_Emax;
  Int_t   DD6_2_Emin;
  Int_t   DD6_2_Emax;
	Int_t		DD6_1_F62; 
	Int_t		DD6_1_E1D6; 
	Int_t		DD6_1_DD6_2;
	Int_t		DD6_1_WIN; 
	Int_t		DD6_1_CELL; 
	Int_t		DD6_1_SID6; 
	Int_t		DD6_1_DEG; 
	Int_t		DD6_1_WHEEL; 

	Int_t 	TAC_LISE_Evt[7], D4_LISE_Evt[5], D6_LISE_Evt[8];
	ULong64_t TAC_LISE_FirstTS[7], D4_LISE_FirstTS[5], D6_LISE_FirstTS[8];
	ULong64_t FirstTS; 

  BaseDetector *DetD4;
  BaseDetector *DetD6;
  Caviar *DetCaviar;
  BaseDetector *ICD6;
  BaseDetector *TAC_LISE;

  BaseDetector *PL_ZDD;
  BaseDetector *IC_ZDD;
  BaseDetector *DC_ZDD;

  BaseDetector *CATS1TS;
  BaseDetector *CAVIARTS;

  ULong64_t RefTS;


  UShort_t NumberOfCuts;

#ifdef WITH_ROOT
  vector<TCutG*> Cuts2D;
  vector<string> Cuts2DName;
#endif

};
#endif // IdLISE_HH
