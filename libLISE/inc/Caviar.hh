/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef CAVIAR_HH
#define CAVIAR_HH

/**
 * @file   Caviar.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   May, 2020
 * @brief  Caviar Class
 *
 * Caviar class : Get momentum distribution at LISE.
 */

#include "Defines.hh"
#include "BaseDetector.hh"


class Caviar : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  Caviar(const Char_t * Name,
                  UShort_t NDetectors,
                  Bool_t RawData,
                  Bool_t CalData,
                  Bool_t DefaultCalibration,
                  const Char_t * NameExt);
  ~Caviar(void);

#ifdef WITH_ROOT
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
  void CreateHistogramsCal2D(TDirectory *Dir);
  void CreateHistogramsCal1D(TDirectory *Dir);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par,Map* Map);
  //! Treat the data
  Bool_t Treat(void);
  //! Treat the Wires
  void TreatWires(void);
  //! Treat the Wires
  void ReadPosition(void);

protected:

  //! Allocate subcomponents
  void AllocateComponents(void);
  Float_t CATSD6Pos;
  Float_t CATSD4Pos;
  Float_t CAVIARPos;
  Float_t BrhoD32;
  Float_t T16_LISE;
  Float_t CaviarRefWire;
  Int_t WireMin;
  Int_t WireMax;

  ULong64_t RefTS;
  BaseDetector *CAVIARTS;

};
#endif // CAVIAR_HH
