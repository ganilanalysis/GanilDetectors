#ifndef SIANNULAR_CLASS
#define SIANNULAR_CLASS
/**
 * @file   SiAnnular.hh
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#ifdef WITH_ROOT
#include "TRandom.h"
#endif

class SiAnnular : public BaseDetector
{
public:
  //! Constructor instantiation by type, number of channels, and calibration files
  SiAnnular(const Char_t * Name,
                  UShort_t NDetectors,
                  Bool_t RawData,
                  Bool_t CalData,
                  Bool_t DefaultCalibration,
                  const Char_t * NameExt,
                  UShort_t MaxMult);
  ~SiAnnular(void);


  //! Return Calibrated Total Energy
  inline Float_t GetX(void) {return Cal[0]; }
  inline Float_t GetY(void) {return Cal[1];}
  inline Float_t GetZ(void){return Cal[2];}
  inline Float_t GetTheta(void) {return Cal[3];}
  inline Float_t GetPhi(void) {return Cal[4];}
  inline Float_t GetErings(void) {return Cal[5];}
  inline Float_t GetEsectors(void) {return Cal[6];}


#ifdef WITH_ROOT
  //! Create 2D Histograms for Calculated Parameters
  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Create 1D Histograms for Calculated Parameters
  void CreateHistogramsCal1D(TDirectory *Dir);
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParametersNUMEXO(NUMEXOParameters* PL,Map* Map);

  void CalculatePosition(void);


  //! Treat the data
  Bool_t Treat(void);


protected:

  //! Allocate subcomponents
  void AllocateComponents(void);

  Float_t Zref;
  Float_t SpZ;
  Float_t SpX;
  Float_t SpY;
  Float_t SpTh;
  Float_t SpPh;
  Float_t Esec;
  Float_t Ering;
#ifdef WITH_ROOT
  TRandom *rn;
#endif

};
#endif

