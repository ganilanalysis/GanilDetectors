/****************************************************************************
 *    Copyright (C) 2012-2019 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "DriftChamberZDD.hh"

DriftChamberZDD::DriftChamberZDD(const Char_t *Name,
                               UShort_t NDetectors,
                               Bool_t RawData,
                               Bool_t CalData,
                               Bool_t DefaultCalibration,
                               const Char_t *NameExt,
                               UShort_t MaxMult)
    : BaseDetector(Name, 8, false, CalData, DefaultCalibration,false,NameExt,true)
{
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;
    fMaxMultSubDetectors=MaxMult;
    DetManager *DM = DetManager::getInstance();

//    FP = nullptr;
//    FP = (FocalPosition*) DM->GetDetector("FP");

//    LTS = nullptr;
//    LTS = (TimeStamp*) DM->GetDetector("AGAVA_VAMOSTS");;
//    if(!LTS)
//    {
//        Char_t Message[500];
//        sprintf(Message,"In <%s><%s> Impossible to Find Reference TS Object (LTS) !", GetName(), GetName());
//        MErr * Er= new MErr(WhoamI,0,0, Message);
//        throw Er;
//    }
    DATATRIG_CATS1 = nullptr;
    DATATRIG_CATS1 = (BaseDetector*) DM->GetDetector("DATATRIG_CATS1");

    TP = nullptr;
    TP = (CATSTracker*) DM->GetDetector("TP");

    AllocateComponents();

    ReadPosition();

    AddCounter("Present TDC1 "); //2
    AddCounter("Present TDC2 "); //3

    if(fCalData)
    {
        sprintf(CalNameI[0],"%s_X", DetectorName);
        SetCalName("Theta_AT",1);
        sprintf(CalNameI[2],"%s_Y", DetectorName);
        SetCalName("Phi_AT",3);
        sprintf(CalNameI[4],"%s_TSdX1", DetectorName);
        sprintf(CalNameI[5],"%s_TSdX2", DetectorName);
        sprintf(CalNameI[6],"%s_TSdY1", DetectorName);
        sprintf(CalNameI[7],"%s_TSdY2", DetectorName);


        // X in  [-2000,2000]
        SetGateCal(-2000,2000,0);
        // Theta  in  [-2000,2000]
        SetGateCal(-2000,2000,1);
        // Y in [-2000,2000]
        SetGateCal(-2000,2000,2);
        // Phi in [-2000,2000]
        SetGateCal(-2000,2000,3);

        // TSdX1 in [-2000,2000]
        SetGateCal(-2000,2000,4);
        // TSdX2 in [-2000,2000]
        SetGateCal(-2000,2000,5);
        // TSdY1 in [-2000,2000]
        SetGateCal(-2000,2000,6);
        // TSdY2 in [-2000,2000]
        SetGateCal(-2000,2000,7);


    }

    END;
}
DriftChamberZDD::~DriftChamberZDD(void)
{
    START;
    END;
}
void DriftChamberZDD::AllocateComponents(void)
{
    START;

    fPresentX = false;
    fPresentY = false;

    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Bool_t PosInfo = true;
    Bool_t WithTS = true;

    string BaseName;
    string Name;

    BaseDetector *DC_ZDD = new BaseDetector("DC_ZDD",4,RawData,CalData,DefCal,PosInfo,"",false,WithTS,fMaxMultSubDetectors);
    if(RawData)
    {
        DC_ZDD->SetParameterName("DC_X1",0);
        DC_ZDD->SetParameterName("DC_X2",1);
        DC_ZDD->SetParameterName("DC_Y1",2);
        DC_ZDD->SetParameterName("DC_Y2",3);


        DC_ZDD->SetRawHistogramsParams(64000,0,63999);
        DC_ZDD->SetGateRaw(0,63999);
    }
    AddComponent(DC_ZDD);
    cout << "DC_ZDD Ref Positions : " << endl;
    cout << "X1off :" <<  DC_ZDD->GetActiveArea(0,2) << " ZRef : " <<DC_ZDD->GetActiveArea(0,4) <<endl;;
    cout << "X2off :" <<  DC_ZDD->GetActiveArea(1,2) << " ZRef : " << DC_ZDD->GetActiveArea(1,4)<< endl;;
    cout << "Y1off :" <<  DC_ZDD->GetActiveArea(2,3) << " ZRef : " << DC_ZDD->GetActiveArea(2,4)<< endl;;
    cout << "Y2off :" <<  DC_ZDD->GetActiveArea(3,3) << " ZRef : " << DC_ZDD->GetActiveArea(3,4)<< endl;;

    END;
}

void DriftChamberZDD::SetParametersNUMEXO(NUMEXOParameters* Par, Map* Map) {
    START;

    NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
    for (UShort_t i = 0; i < DetList->size(); i++)
        DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);
    END;
}

// void DriftChamberZDD::ReadPosition(void)
// {
//   START;
//   MIFile *IF;
//   char Line[255];
//   stringstream *InOut;

//   InOut = new stringstream();
//   *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
//   *InOut>>Line;
//   InOut = CleanDelete(InOut);
//   if((IF = CheckCalibration(Line)))
//     {
//       // Position
//       for(UShort_t i=0; i< DetList->size();i++)
//       {
//         try
//         {
//             DetList->at(i)->ReadDefaultPosition(IF);
//         }
//       catch(...)
//         {
//           IF = CleanDelete(IF);
//           throw;
//         }
//       }
//     }

//   //if(VerboseLevel >= V_INFO)
//     {
//       cout << "<" << DetectorName << "><" << DetectorNameExt << "> Reference positions (X,Y,Z) wrt. to target " << endl;
//       cout << "<" << DetectorName << "><" << DetectorNameExt << ">   X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ() << " mm" << endl;
//       cout << "\n" << endl;

//     }
//     L->File << "<" << DetectorName << "><" << DetectorNameExt << "> Reference positions (X,Y,Z) wrt. to target " << endl;
//     L->File << "<" << DetectorName << "><" << DetectorNameExt << ">  X: " << GetRefX() << " Y: " << GetRefY() << " Z: " << GetRefZ() << " mm" << endl;

//   END;
// }

Bool_t DriftChamberZDD::Treat(void)
{
    START;

    ULong64_t RefTS = -1500;
    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
            DetList->at(i)->Treat();
    }

    if(fCalData)
    {
        Double_t ZRef = 738.7; //mm
        Double_t Size = 180./2.; // mm
        Double_t ZRefY = DetList->at(0)->GetActiveArea(2,4); //mm
        Double_t ZRefX = DetList->at(0)->GetActiveArea(0,4); //mm
//        Double_t ZRefY2 = DetList->at(1)->GetActiveArea(2,4)+3.+179.6+6.6+16./2.; //mm
//        Double_t ZRefX2 = DetList->at(1)->GetActiveArea(0,4)+3+30.8+16./2.; //mm
//        Double_t DriftVelocity = 5.387; //cm/us
        Double_t DriftVelocity[4]; //cm/us
        DriftVelocity[0] = DetList->at(0)->GetActiveArea(0,1); //cm/us
        DriftVelocity[1] = DetList->at(0)->GetActiveArea(1,1); //cm/us
        DriftVelocity[2] = DetList->at(0)->GetActiveArea(2,1); //cm/us
        DriftVelocity[3] = DetList->at(0)->GetActiveArea(3,1); //cm/us

        Double_t X_Offset[2];
        X_Offset[0] = DetList->at(0)->GetActiveArea(0,2); //mm
        X_Offset[1] = DetList->at(0)->GetActiveArea(1,2); //mm
//        X_Offset[0] = DetList->at(0)->GetActiveArea(0,0); //mm
//        X_Offset[1] = DetList->at(0)->GetActiveArea(1,0) ; //mm
        Double_t Y_Offset[2];
        Y_Offset[0] = DetList->at(0)->GetActiveArea(2,3); //mm
        Y_Offset[1] = DetList->at(0)->GetActiveArea(3,3); //mm
//        Y_Offset[0] = DetList->at(0)->GetActiveArea(2,1) ; //mm
//        Y_Offset[1] = DetList->at(0)->GetActiveArea(3,1) ; //mm


        Double_t X = -1500.;
        Double_t Y = -1500.;
        Bool_t PresentX = false;
        Bool_t PresentY = false;

        if (DATATRIG_CATS1->GetRawM() > 0){
            RefTS = DATATRIG_CATS1->GetRawTSAt(0);
        }

        if(RefTS >0)
        {
            if(DetList->at(0)->GetRawTS(0)>0)
            {
                X = ((((Float_t)(RefTS-DetList->at(0)->GetRawTS(0)))*10.)/1000.* DriftVelocity[0])*10. + X_Offset[0]+(0.4*rand() / (RAND_MAX));
                PresentX = true;
                SetCalData(4,RefTS-DetList->at(0)->GetRawTS(0));
            }
            else if(DetList->at(0)->GetRawTS(1)>0)
            {

                X = ((((Float_t)(RefTS-DetList->at(0)->GetRawTS(1)))*10.)/1000.* DriftVelocity[1])*10. + X_Offset[1]+(0.4*rand() / (RAND_MAX));
                X *=-1.;
                PresentX = true;
                SetCalData(5,RefTS-DetList->at(0)->GetRawTS(1));

            }
            else
            {
                X = -1500.;
                PresentX = false;
            }

            if(DetList->at(0)->GetRawTS(2)>0)
            {
                Y = ((((Float_t)(RefTS-DetList->at(0)->GetRawTS(2)))*10.)/1000.* DriftVelocity[2])*10. + Y_Offset[0]+(0.4*rand() / (RAND_MAX));
                Y *=-1.;
                PresentY = true;
                SetCalData(6,RefTS-DetList->at(0)->GetRawTS(2));

            }
            else if(DetList->at(0)->GetRawTS(3)>0)
            {
                Y = ((((Float_t)(RefTS-DetList->at(0)->GetRawTS(3)))*10.)/1000.* DriftVelocity[3])*10. + Y_Offset[1]+(0.4*rand() / (RAND_MAX));
                PresentY = true;
                SetCalData(7,RefTS-DetList->at(0)->GetRawTS(3));

            }
            else
            {
                Y = -1500.;
                PresentY = false;
            }

//            if(PresentX && PresentY){
//                IncrementCounter(0);
//            }



        if(PresentX)
        {
            SetCalData(0,X);
        }
        else
        {
            SetCalData(0,-1500.);
        }

        if(PresentY)
        {
            SetCalData(2,Y);
        }
        else
        {
            SetCalData(2,-1500.);
        }

        if (PresentX && PresentY){
            isPresent = true;
        }
        Float_t Xf = TP->GetCal(0);
        Float_t Yf = TP->GetCal(2);

        Float_t Theta_AT = -1500.;
        Float_t Phi_AT = -1500.;
        Bool_t PresentAngle = false;

        if (Yf >-1500. && Yf <1500. && Yf != 0. && Xf>-1500. && Xf<1500. && Xf != 0){
            if (PresentX && PresentY) {
                Theta_AT = (Float_t)(1000*atan((Xf-X)/(TP->GetRefZ()-ZRefX))); //mRad
                Phi_AT = (Float_t)(1000*atan((Yf-Y)/(TP->GetRefZ()-ZRefY))); //mRad
                PresentAngle = true;

            }
            else {
                Theta_AT = -1500;
                Phi_AT = -1500;
            }
        }
        if (PresentAngle && Theta_AT<1500. && Phi_AT<1500. ) {
            SetCalData(1,Theta_AT);
            SetCalData(3,Phi_AT);
        }
        else {
            Theta_AT = -1500;
            Phi_AT = -1500;
            SetCalData(1,Theta_AT);
            SetCalData(3,Phi_AT);
        }


#ifdef WITH_ROOT
//        // Has to be changed to be incorporated in Narwal actor
//        //MW target projected coordinates (Theta in 0xz and Phi in 0yz)
//        Double_t Theta = GetCal(1);
//        Double_t Phi = GetCal(3);
//        //Zgoubi coordinates (Theta in 0xz and Phi between v particle and 0xz)
//        Double_t ThetaZ = Theta;
//        Double_t PhiZ = atan(tan(Phi/1000.)*cos(Theta/1000.))*1000.;
//        Double_t ThetaL;
//        Double_t PhiL;
////        Double_t VamosAngle = FP->GetVamosAngle();
//        TVector3 *myVec;
//        // TVector3 *myVecy;
//        myVec = new TVector3(sin(ThetaZ/1000.)*cos(PhiZ/1000.),sin(PhiZ/1000.),cos(ThetaZ/1000.)*cos(PhiZ/1000.));

//        ////
//        //                          ANGLE VAMOS
//        ///////


////        myVec->RotateY(VamosAngle*TMth::Pi()/180.); //VAMOS angle

//        ////
//        //                          ANGLE VAMOS
//        ///////

//        ThetaL = myVec->Theta();
//        PhiL = myVec->Phi();

//        //cout << "Theta " << Theta << " Phi " << Phi << " ThetaZ " << ThetaZ << " PhiZ " << PhiZ << " ThetaL " << ThetaL << " PhiL " << PhiL << endl;

//        delete myVec;

//        SetCalData(4,ThetaL);
//        SetCalData(5,PhiL);
#endif

        }
        else
        {
            for(UShort_t i=0;i<NumberOfDetectors;i++)
                SetCalData(i,-1500.);

        }

    }


    return(isPresent);


    END;
}



#ifdef WITH_ROOT
void DriftChamberZDD::CreateHistogramsCal1D(TDirectory *Dir)
{
    START;
    string Name;
    Dir->cd("");
    if(SubFolderHistCal1D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal1D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    // X
    AddHistoCal(CalNameI[0],CalNameI[0],"X (mm)",1000,-200,200);
    // Y
    AddHistoCal(CalNameI[2],CalNameI[2],"Y (mm)",1000,-200,200);
    // Theta
    AddHistoCal(CalNameI[1],CalNameI[1],"Theta (mrad)",1000,-300,300);
    // Phi
    AddHistoCal(CalNameI[3],CalNameI[3],"Phi (mrad)",1000,-400,400);
    // TSdX1
    AddHistoCal(CalNameI[4],CalNameI[4],"TSdX1 (TS unit)",1000,-500,500);
    // TSdX2
    AddHistoCal(CalNameI[5],CalNameI[5],"TSdX2 (TS unit)",1000,-500,500);
    // TSdY1
    AddHistoCal(CalNameI[6],CalNameI[6],"TSdY1 (TS unit)",1000,-500,500);
    // TSdY2
    AddHistoCal(CalNameI[7],CalNameI[7],"TSdY2 (TS unit)",1000,-500,500);

    END;
}

void DriftChamberZDD::CreateHistogramsCal2D(TDirectory *Dir)
{
    START;
    string Name;

    BaseDetector::CreateHistogramsCal2D(Dir);

    Dir->cd("");

    if(SubFolderHistCal2D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal2D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    AddHistoCal(CalNameI[0],CalNameI[2],"X_Y","X_vs_Y (mm)",200,-100,100,200,-100,100);
    AddHistoCal(CalNameI[1],CalNameI[3],"Th_Ph","Th_vs_Phi (mrad)",600,-200,200,600,-200,200);
//    AddHistoCal(CalNameI[6],CalNameI[7],"DCT1_XY","DCT1_XY (mm)",600,-1000,1000,600,-1000,1000);
//    AddHistoCal(CalNameI[8],CalNameI[9],"DCT2_XY","DCT2_XY (mm)",600,-1000,1000,600,-1000,1000);

    END;
}


void DriftChamberZDD::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;

    SetMainHistogramFolder("DriftChamberZDD");

#ifdef WITH_ROOT
    // Setup Histogram Hierarchy
    SetHistogramsCal1DFolder("");
    SetHistogramsCal2DFolder("");
#endif

    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetHistogramsRawSummary(false);
        DetList->at(i)->SetOutAttachRawI(false);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCalSummary(false);

            }
        }
    }
    else if(fMode == MODE_D2R)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetOutAttachRawV(false);
                DetList->at(i)->SetOutAttachRawI(true);
                DetList->at(i)->SetOutAttachTS(true);
                DetList->at(i)->SetHistogramsRawSummary(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if(fMode == MODE_D2A)
    {
         if(fRawData)
         {
            SetHistogramsRaw(true);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
            SetOutAttachRawF(true);
         }

        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
            SetOutAttachCalF(false);
        }

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw1D(true);
                DetList->at(i)->SetHistogramsRaw2D(false);
                DetList->at(i)->SetOutAttachRawF(true);
                DetList->at(i)->SetOutAttachTS(true);
                DetList->at(i)->SetOutAttachRawV(false);
                DetList->at(i)->SetHistogramsRawSummary(false);

            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(false);
                DetList->at(i)->SetOutAttachCalF(false);
                DetList->at(i)->SetHistogramsCalSummary(false);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }

        OutAttach(OutTTree);
    }
    else if(fMode == MODE_R2A)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            DetList->at(i)->InAttach(InTTree);
        }

        if(fRawData)
        {
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(false);
                DetList->at(i)->SetHistogramsCalSummary(true);
                DetList->at(i)->SetOutAttachCalV(true);
                DetList->at(i)->OutAttach(OutTTree);
            }

        }
        OutAttach(OutTTree);
    }
    else if(fMode == MODE_RECAL)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            DetList->at(i)->InAttachCal(InTTree);

        }
        SetInAttachCalI(true);
        InAttachCal(InTTree);

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasCalData())
            {
            }
            DetList->at(i)->OutAttach(OutTTree);
        }

    }
    else if(fMode == MODE_CALC)
    {
        SetNoOutput();
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    if(VerboseLevel >= V_INFO)
        PrintOptions(cout)  ;


    END;
}

#endif
