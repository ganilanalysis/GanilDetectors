/****************************************************************************
 *    Copyright (C) 2022 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "IdLISE.hh"

IdLISE::IdLISE(const Char_t *Name,
               UShort_t NCuts,
               Bool_t RawData,
               Bool_t CalData,
               Bool_t DefaultCalibration,
               const Char_t *NameExt
               )
    : BaseDetector(Name, 51+NCuts, false, CalData, DefaultCalibration,false,NameExt)
{
	START;
	NumberOfSubDetectors 	= 0;
	NumberOfCuts 		= NCuts;
	Nparam 			= 51;
	DE_D4min1		= 800;//3900;//500;
	DE_D4max1		= 1800;//4300;//2000;
	DE_D4min2		= 10000;
	DE_D4max2		= 22000;
	DE_D4min3		= 22000;
	DE_D4max3		= 65635;
	NAlpha 			= 0;
	NAlpha_X 		= 0;
	DD6_1_Emin 		= 200;
	DD6_1_Emax 		= 1000;
	DD6_1_F62		= 305.; 
	DD6_1_E1D6		= 435.; 
	DD6_1_DD6_2		= 685.5;
	DD6_1_WIN		= 839.; 
	DD6_1_CELL		= 919.;  


#ifdef WITH_ROOT
    Cuts2D.resize(NCuts,nullptr);
    Cuts2DName.resize(NCuts,"");
#endif
    fRawDataSubDetectors  = RawData;
    DetManager *DM = DetManager::getInstance();
    // Default Values
    BrhoD32 = 2.4352;// Brho D32 in Tm la rigidité magnétique mesurée expérimentalement dans le second dipôle
    T16_LISE = 1.653;// cm/% le terme de dispersion de l’optique employée lors de l’expérience
    CATSD6Pos = 42.0235; // Distance in meters IdLISE - CFA
    CATSD4Pos = 19.236; // Distance in meters IdLISE - CFA

		//CAVIARTS = DM->GetDetector("DATATRIG_CAVIAR");// CAVIAR TS -- modif  2024 06 04

		FirstTS = 1e15; 
     for (Int_t i = 0; i<6; i++)
    {
        TAC_LISE_Evt[i] 		= 0;
        TAC_LISE_FirstTS[i] = 0;
    }

    for (Int_t i = 0; i<4; i++)
    	{
        D4_LISE_Evt[i] 			= 0;
        D4_LISE_FirstTS[i] 	= 0;
    	}

    for (Int_t i = 0; i<4; i++)
    	{
      	D6_LISE_Evt[i] 			= 0;
      	D6_LISE_FirstTS[i] 	= 0;
			}



    DetCaviar = NULL;
    DetD4 = NULL;
    DetD6 = NULL;
    ICD6 = NULL;
    TAC_LISE = NULL;
    PL_ZDD = NULL;
    IC_ZDD = NULL;
    DC_ZDD = NULL;

    DetCaviar = (Caviar*) DM->GetDetector("Caviar");
    DetD4 = DM->GetDetector("D4_LISE");
    DetD6 = DM->GetDetector("D6_LISE");
    ICD6 = DM->GetDetector("CHIO_LISE");
    TAC_LISE = DM->GetDetector("TAC_LISE");

    // GetRefeence TS for TS Plots
    RefTS = 0;


    try{
        char *TS = getenv("REF_LISETS");
        if (TS!=NULL)
        {
           RefTS = atoll(TS);

           cout << "<" << Name <<  "> REF TS from environment variable REF_LISETS : "<< RefTS <<endl;
        }else
        {
            cout << "/!\\ Could not Retrieve REF TS from environment variable REF_LISETS" <<endl;
        }

    }
    catch(...)
    {

    }

  // Try to Get ZDD Detector when available
    try{
        CATS1TS = DM->GetDetector("DATATRIG_CATS1");
    }
    catch(MErr *Er)
    {
        Er->Print();
        CATS1TS = NULL;
        delete Er;
    }

  // Try to Get ZDD Detector when available
    try{
        PL_ZDD = DM->GetDetector("Plastic");
    }
    catch(MErr *Er)
    {
        Er->Print();
        PL_ZDD = NULL;
        delete Er;

    }
    try{
        IC_ZDD = DM->GetDetector("IC_ZDD");
    }
    catch(MErr *Er)
    {
        Er->Print();
        IC_ZDD = NULL;
        delete Er;

    }
    try{
        DC_ZDD = DM->GetDetector("DC_ZDD");
    }
    catch(MErr *Er)
    {
        Er->Print();
        DC_ZDD = NULL;
        delete Er;

    }


    ReadPosition();

    AllocateComponents();

if(CalData)
    {
        sprintf(CalNameI[0],"A_Q_D4");
        sprintf(CalNameI[1],"Beta_D4");
        sprintf(CalNameI[2],"TOF_CATSD4");
        sprintf(CalNameI[3],"A_Q_D6");
        sprintf(CalNameI[4],"Beta_D6");
        sprintf(CalNameI[5],"TOF_CFA");
        sprintf(CalNameI[6],"dE_D6");
        sprintf(CalNameI[7],"dE_D4");
        sprintf(CalNameI[8],"dE_ZDD");
        sprintf(CalNameI[9],"E_PL");
        sprintf(CalNameI[10],"X_PL");
        sprintf(CalNameI[11],"TOF_D4_D6");
        sprintf(CalNameI[12],"TOF_CATSD6");
        sprintf(CalNameI[13],"TimeD6");
        sprintf(CalNameI[14],"TimeD4");
        sprintf(CalNameI[15],"A_Q_D4_D6");
        sprintf(CalNameI[16],"Beta_D4_D6");
        sprintf(CalNameI[17],"DD4X");
        sprintf(CalNameI[18],"DD4Y");
        sprintf(CalNameI[19],"DD4X_mm");
        sprintf(CalNameI[20],"DD4Y_mm");
        sprintf(CalNameI[21],"DD4X_Cond_DE_D4_1");
        sprintf(CalNameI[22],"DD4Y_Cond_DE_D4_1");
        sprintf(CalNameI[23],"DD4X_Cond_DE_D4_2");
        sprintf(CalNameI[24],"DD4Y_Cond_DE_D4_2");
        sprintf(CalNameI[25],"DD4X_Cond_DE_D4_3");
        sprintf(CalNameI[26],"DD4Y_Cond_DE_D4_3");
        sprintf(CalNameI[27],"TDD4_Cond1");
        sprintf(CalNameI[28],"CavPos_mm");
        sprintf(CalNameI[29],"DD6_1_X");
        sprintf(CalNameI[30],"DD6_1_Y");
        sprintf(CalNameI[31],"DD6_1_X_Cond");
        sprintf(CalNameI[32],"DD6_1_Y_Cond");
        sprintf(CalNameI[33],"DD6_1_T_Cond");
        sprintf(CalNameI[34],"E1D6_X");
        sprintf(CalNameI[35],"E1D6_Y");
        sprintf(CalNameI[36],"F62_X");
        sprintf(CalNameI[37],"F62_Y");
        sprintf(CalNameI[38],"WIN_X");
        sprintf(CalNameI[39],"WIN_Y");
        sprintf(CalNameI[40],"CELL_X");
        sprintf(CalNameI[41],"CELL_Y");
        sprintf(CalNameI[42],"E1D6");
        sprintf(CalNameI[43],"E2D6");
        sprintf(CalNameI[44],"T_DD6_1_HF");
        sprintf(CalNameI[45],"T_DD4_HF");
        sprintf(CalNameI[46],"E1D4");
        sprintf(CalNameI[47],"E2D4");
        sprintf(CalNameI[48],"DD6_1_X_C");
        sprintf(CalNameI[49],"DD6_1_Y_C");
        sprintf(CalNameI[50],"CHIO_LISE");


        for(int i = 0;i < NCuts; i++)
        {
            sprintf(CalNameI[Nparam+i],"XWire_Cut_%02d",i);
        }
    }

    END;
}
IdLISE::~IdLISE(void)
{
    START;
    END;
}

void IdLISE::SetBrhoRef(Float_t Brho)
{
  START;
  cout << "    <" <<  DetectorName << "><" << DetectorNameExt << "> Reference Brho (BrhoD32) set to " << Brho << " Tm" << endl;
  L->File << "    <" <<  DetectorName << "><" << DetectorNameExt << "> Reference Brho (BrhoD32) set to " << Brho << " Tm" <<  endl;
  BrhoD32 = Brho;
  END;
};


void IdLISE::ReadPosition(void)
{
    START;
    if(fCalData)
    {

        MIFile *IF;
        char Line[255];
        stringstream *InOut;
        UShort_t Len=255;
        Char_t *Ptr=NULL;

        InOut = new stringstream();
        *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
        *InOut>>Line;
        InOut = CleanDelete(InOut);

        if((IF = CheckCalibration(Line)))
        {
            // Position
            try
            {
                // Read Detector Positions
                IF->GetLine(Line,Len);
                while((Ptr = CheckComment(Line)))
                {
                    IF->GetLine(Line,Len);
                    L->File << Line << endl;
                }
                stringstream *InOut = new stringstream();
                *InOut << Line;
                *InOut >> CATSD4Pos;
                if(InOut->fail())
                {
                    InOut = CleanDelete(InOut);
                    Char_t Message[500];
                    sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read CATSD4 Position value !", DetectorName, DetectorNameExt, IF->GetFileName());
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                InOut = CleanDelete(InOut);
                cout << "    <" << DetectorName << "><" << DetectorNameExt << "> CATSD4Pos set to " << CATSD4Pos << " m" <<  endl;
                L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> CATSD4Pos set to " << CATSD4Pos << " m." <<  endl;
                IF->GetLine(Line,Len);
                while((Ptr = CheckComment(Line)))
                {
                    IF->GetLine(Line,Len);
                    L->File << Line << endl;
                }
                InOut = new stringstream();
                *InOut << Line;
                *InOut >> CATSD6Pos;
                if(InOut->fail())
                {
                    InOut = CleanDelete(InOut);
                    Char_t Message[500];
                    sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read CATSD6 Position value !", DetectorName, DetectorNameExt, IF->GetFileName());
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                InOut = CleanDelete(InOut);

                cout << "    <" << DetectorName << "><" << DetectorNameExt << "> CATSD6Pos set to " << CATSD6Pos << " meters" <<  endl;
                L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> CATSD6Pos set to " << CATSD6Pos << " meters" <<  endl;


                IF->GetLine(Line,Len);
                while((Ptr = CheckComment(Line)))
                {
                    IF->GetLine(Line,Len);
                    L->File << Line << endl;
                }
                InOut = new stringstream();
                *InOut << Line;
                *InOut >> IdLISEPos;
                if(InOut->fail())
                {
                    InOut = CleanDelete(InOut);
                    Char_t Message[500];
                    sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read IdLISE Position value !", DetectorName, DetectorNameExt, IF->GetFileName());
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                InOut = CleanDelete(InOut);
                cout << "    <" << DetectorName << "><" << DetectorNameExt << "> IdLISEPos set to " << IdLISEPos << " meters" <<  endl;
                L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> IdLISEPos set to " << IdLISEPos << " meters" <<  endl;
                IF->GetLine(Line,Len);
                while((Ptr = CheckComment(Line)))
                {
                    IF->GetLine(Line,Len);
                    L->File << Line << endl;
                }
                InOut = new stringstream();
                *InOut << Line;
                *InOut >> T16_LISE;
                if(InOut->fail())
                {
                    InOut = CleanDelete(InOut);
                    Char_t Message[500];
                    sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read T16_LISE value !", DetectorName, DetectorNameExt, IF->GetFileName());
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                InOut = CleanDelete(InOut);
                cout << "    <" << DetectorName << "><" << DetectorNameExt << "> T16_LISE set to " << T16_LISE << " cm/%" <<  endl;
                L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> T16_LISE set to " << T16_LISE << " cm/%" <<  endl;


                IF->GetLine(Line,Len);
                while((Ptr = CheckComment(Line)))
                {
                    IF->GetLine(Line,Len);
                    L->File << Line << endl;
                }
                InOut = new stringstream();
                *InOut << Line;
                *InOut >> BrhoD32;
                if(InOut->fail())
                {
                    InOut = CleanDelete(InOut);
                    Char_t Message[500];
                    sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read BrhoD32 value !", DetectorName, DetectorNameExt, IF->GetFileName());
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                InOut = CleanDelete(InOut);
                cout << "    <" << DetectorName << "><" << DetectorNameExt << "> BrhoD32 set to " << BrhoD32 << " Tm" <<  endl;
                L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> BrhoD32 set to " << BrhoD32 << " Tm" <<  endl;


            }
            catch(...)
            {
                IF = CleanDelete(IF);
                throw;
            }
            IF = CleanDelete(IF);
        }
    }
    END;
}

void IdLISE::AllocateComponents(void)
{
    START;

    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = false;
    Bool_t DefCal = true;
    Bool_t PosInfo = true;
    END;
}

void IdLISE::SetParameters(Parameters* Par,Map* Map)
{
    START;
    END;
}


Bool_t IdLISE::Treat(void)
{
    START;
    Ctr->at(0)++;
    Float_t PLE[2];
    PLE[0] = PLE[1] = -1500;
    Float_t E_PL = -1500;
    Float_t X_PL = -1500;
    Float_t PL_Size = 220; //mm
    Float_t ICE = -1500;
    Float_t ICTh = 1; // MeV
    ULong64_t CATSTime = 0;


    if(fCalData)
    {


        if(CATS1TS)
            CATSTime = CATS1TS->GetRawTS(0);

        if(CATSTime>0)
        {
            SetCalData(14,(CATSTime-RefTS)*1e-8/60/60);
        }
        else
            SetCalData(14,-1500);

		for (Int_t i = 0; i<6; i++)	
		{
			if ( TAC_LISE->GetRaw(i) > 0)
			{
				TAC_LISE_Evt[i]++;
				if (TAC_LISE_Evt[i] == 1)
				{
					TAC_LISE_FirstTS[i] = TAC_LISE->GetRawTS(i);
					if (TAC_LISE_FirstTS[i] < FirstTS) FirstTS = TAC_LISE_FirstTS[i];
					cout << "!!!!!!!!!!!!!!! THIS IS THE FIRST TAC_LISE[ " << i << " ] event !!!!!! -> TS = " << TAC_LISE_FirstTS[i] << endl;
					cout << "----> FIRST TS = " << FirstTS << endl;
				}
			}
		}

		for (Int_t i = 0; i<4; i++)	
		{
			if ( DetD4->GetRaw(i) > 0)
			{
				D4_LISE_Evt[i]++;
				if (D4_LISE_Evt[i] == 1)
				{
					D4_LISE_FirstTS[i] = DetD4->GetRawTS(i);
					if (D4_LISE_FirstTS[i] < FirstTS) FirstTS = D4_LISE_FirstTS[i];
					cout << "!!!!!!!!!!!!!!! THIS IS THE FIRST D4_LISE[ " << i << " ] event !!!!!! -> TS = " << D4_LISE_FirstTS[i] << endl;
					cout << "----> FIRST TS = " << FirstTS << endl;
				}
			}
		}

		for (Int_t i = 0; i<4; i++)	
		{
			if ( DetD6->GetRaw(i) > 0)
			{
				D6_LISE_Evt[i]++;
				if (D6_LISE_Evt[i] == 1)
				{
					D6_LISE_FirstTS[i] = DetD6->GetRawTS(i);
					if (D6_LISE_FirstTS[i] < FirstTS) FirstTS = D6_LISE_FirstTS[i];
					cout << "!!!!!!!!!!!!!!! THIS IS THE FIRST D6_LISE[ " << i << " ] event !!!!!! -> TS = " << D6_LISE_FirstTS[i] << endl;
					cout << "----> FIRST TS = " << FirstTS << endl;
				}
			}
		}


////////////////////////////////////////////////////////////////////////
/////////:////////// 							DELICAT D4				/////////://////////
////////////////////////////////////////////////////////////////////////
        if(DetD4->GetRaw(1)>0)	SetCalData(17,DetD4->GetRaw(1));
        else
            SetCalData(17,-1500);

        if(DetD4->GetRaw(2)>0)
            SetCalData(18,DetD4->GetRaw(2));
        else
            SetCalData(18,-1500);

         if (TAC_LISE->GetRaw(0)> 0) SetCalData(45,TAC_LISE->GetRaw(0));
        else
            SetCalData(45,-1500);




        if(DetD4->GetRaw(3)>0)
					{
            SetCalData(46,DetD4->GetCal(3));
            SetCalData(7,DetD4->GetRaw(3));

						if(DetD4->GetRaw(3) > DE_D4min1 && DetD4->GetRaw(3) < DE_D4max1)
							{
            		if (DetD4->GetRaw(1) > 0) SetCalData(21,DetD4->GetRaw(1));
								else SetCalData(21,-1500);
            		if (DetD4->GetRaw(2) > 0) SetCalData(22,DetD4->GetRaw(2));
								else SetCalData(22,-1500);
            		if (TAC_LISE->GetRaw(0)> 0) SetCalData(27,TAC_LISE->GetRaw(0));
								else SetCalData(27,-1500);
							}
						else
							{
            		SetCalData(21,-1500);
            		SetCalData(22,-1500);
            		SetCalData(27,-1500);
							}
						if(DetD4->GetRaw(3) > DE_D4min2 && DetD4->GetRaw(3) < DE_D4max2)
							{
            		if (DetD4->GetRaw(1) > 0) SetCalData(23,DetD4->GetRaw(1));
								else SetCalData(23,-1500);
            		if (DetD4->GetRaw(2) > 0) SetCalData(24,DetD4->GetRaw(2));
								else SetCalData(24,-1500);
							}
						else
							{
            		SetCalData(23,-1500);
            		SetCalData(24,-1500);
							}
						if(DetD4->GetRaw(3) > DE_D4min3 && DetD4->GetRaw(3) < DE_D4max3)
							{
            		if (DetD4->GetRaw(1) > 0) SetCalData(25,DetD4->GetRaw(1));
								else SetCalData(25,-1500);
            		if (DetD4->GetRaw(2) > 0) SetCalData(26,DetD4->GetRaw(2));
								else SetCalData(26,-1500);
							}						
						else
							{
            		SetCalData(25,-1500);
            		SetCalData(26,-1500);
							}
					}
        else
					{
            SetCalData(7,-1500);
            SetCalData(21,-1500);
            SetCalData(22,-1500);
            SetCalData(23,-1500);
            SetCalData(24,-1500);
            SetCalData(25,-1500);
            SetCalData(26,-1500);
						SetCalData(27,-1500);
						SetCalData(46,-1500);
					}

        if(DetD4->GetRaw(1)>0)
            SetCalData(19,DetD4->GetCal(1));
        else
            SetCalData(19,-1500);

        if(DetD4->GetRaw(2)>0)
            SetCalData(20,DetD4->GetCal(2));
        else
            SetCalData(20,-1500);


////////////////////////////////////////////////////////////////////////
/////////:////////// 							E2D4         			/////////://////////
////////////////////////////////////////////////////////////////////////
        if(DetD4->GetRaw(0)>0) SetCalData(47,DetD4->GetCal(0));
        else SetCalData(47,-1500);

////////////////////////////////////////////////////////////////////////
/////////:////////// 							DELICAT D6_1			/////////://////////
////////////////////////////////////////////////////////////////////////

	if (DetD6->GetRaw(2) > DD6_1_Emin && DetD6->GetRaw(2) < DD6_1_Emax)
	{
		if (TAC_LISE->GetRaw(3) > 0)	SetCalData(33,TAC_LISE->GetRaw(3));
		else	SetCalData(33,-1500);
	}
	else	SetCalData(33,-1500);


	if(DetD6->GetRaw(0)>0)
	{
		SetCalData(29,DetD6->GetRaw(0));
		if (DetD6->GetRaw(2) > DD6_1_Emin && DetD6->GetRaw(2) < DD6_1_Emax)
		{
			SetCalData(31,DetD6->GetRaw(0)); 
		}
		else
		{
			SetCalData(31,-1500);
		}
	}
	else
	{
		SetCalData(29,-1500);
		SetCalData(31,-1500);
	}

	if(DetD6->GetRaw(1)>0)
	{
		SetCalData(30,DetD6->GetRaw(1));
		if (DetD6->GetRaw(2) > DD6_1_Emin && DetD6->GetRaw(2) < DD6_1_Emax)
		{
			SetCalData(32,DetD6->GetRaw(1)); 
		}
		else
		{
			SetCalData(32,-1500); 
		}
	}
	else
	{
		SetCalData(30,-1500);
		SetCalData(32,-1500);
	}
	if (TAC_LISE->GetRaw(3) > 100)	SetCalData(44,TAC_LISE->GetCal(3));
        else
            SetCalData(44,-1500);

////////////////////////////////////////////////////////////////////////
/////////:////////// 							CHIO				/////////://////////
////////////////////////////////////////////////////////////////////////
        if(ICD6->GetCal(0)>0)
            SetCalData(50,ICD6->GetCal(0));
        else
            SetCalData(50,-1500);


////////////////////////////////////////////////////////////////////////
/////////:////////// 								E1D6						/////////://////////
////////////////////////////////////////////////////////////////////////
        if(DetD6->GetRaw(2)>0)
            SetCalData(42,DetD6->GetCal(2));
        else
            SetCalData(42,-1500);

////////////////////////////////////////////////////////////////////////
/////////:////////// 								E2D6						/////////://////////
////////////////////////////////////////////////////////////////////////
        if(DetD6->GetRaw(3)>0)
            SetCalData(43,DetD6->GetCal(3));
        else
            SetCalData(43,-1500);

////////////////////////////////////////////////////////////////////////
/////////:////////// 								ICD6						/////////://////////
////////////////////////////////////////////////////////////////////////
        if(ICD6->GetCal(0)>0)
            SetCalData(6,ICD6->GetCal(0));
        else
            SetCalData(6,-1500);

////////////////////////////////////////////////////////////////////////
/////////:////////// 							PL_ZDD						/////////://////////
////////////////////////////////////////////////////////////////////////
        // Treat PL (dirty Treat for now)
        if(PL_ZDD)
        {
            if(PL_ZDD->GetCalM() == 2)
            {
                for(UShort_t l=0;l<PL_ZDD->GetCalM(); l++)
                {
                    PLE[PL_ZDD->GetNrAt(l)/5] = PL_ZDD->GetCalAt(l);
                }
            }

            if(PLE[0] > 0 && PLE[1]>0)
            {
                E_PL = sqrt(PLE[0]*PLE[1]);
                X_PL = PL_Size * log(PLE[0]/PLE[1]);
            }
        }
        SetCalData(9,E_PL);
        SetCalData(10,X_PL);


////////////////////////////////////////////////////////////////////////
/////////:////////// 							IC_ZDD						/////////://////////
////////////////////////////////////////////////////////////////////////
        // Treat IC (dirty Treat for now)
        if(IC_ZDD)
        {
            if(IC_ZDD->GetCalM())
             {
                ICE = 0 ;
                for(UShort_t l=0;l<IC_ZDD->GetCalM(); l++)
                    ICE += IC_ZDD->GetCalAt(l) * (IC_ZDD->GetCal(l) > ICTh);
            }
            else
                ICE = -1500;
        }
        SetCalData(8,ICE);


////////////////////////////////////////////////////////////////////////
/////////:////////// 			 Brho reconstruction			/////////://////////
////////////////////////////////////////////////////////////////////////

        Float_t D,V,T,Beta,A_Q,xdisp_cm,BrhoWire;
        D = V = T = A_Q = Beta = xdisp_cm  = BrhoWire = 0.0;
        Float_t Gamma = 1.0;
  			Int_t   ECavMin	=	 500;


        // Now reconstruct Brho !
        xdisp_cm = DetCaviar->GetCal(2); // x Disp in cm
				
        if(TAC_LISE->GetRaw(1)> 0) // TOF CATS D4 HF
        {

            T = TAC_LISE->GetCal(1);
            D=CATSD4Pos*100;
            V = D/T;
            Beta =V/29.9792458;
            Gamma = 1./sqrt(1.-powf(Beta,2.f));
            SetCalData(1,Beta); // Beta
            SetCalData(2,T); // TOF

            if(xdisp_cm > -1500)
            {
                BrhoWire = BrhoD32*(1+xdisp_cm/(100*T16_LISE));
                A_Q = BrhoWire/3.105/Beta/Gamma;
                SetCalData(0,A_Q); // A/Q
            }
            else
                SetCalData(0,-1500); // A/Q

        }
        else
        {

            SetCalData(0,-1500); // A/Q
            SetCalData(1,-1500); // Beta
            SetCalData(2,-1500); // TOF

        }
        if(TAC_LISE->GetRaw(3)> 0) // TOF DD6_1 HF
        {
            T = TAC_LISE->GetCal(3);
            D=CATSD6Pos*100;
            V = D/T;
            Beta =V/29.9792458;
            Gamma = 1./sqrt(1.-powf(Beta,2.f));
            SetCalData(4,Beta); // Beta
            SetCalData(5,T); // TOF

            if(xdisp_cm > -1500)
            {
                BrhoWire = BrhoD32*(1+xdisp_cm/(100*T16_LISE));
                A_Q = BrhoWire/3.105/Beta/Gamma;
                SetCalData(3,A_Q); // A/Q
            }
            else
                SetCalData(3,-1500); // A/Q


        }
        else
        {
            SetCalData(3,-1500); // A/Q
            SetCalData(4,-1500); // Beta
            SetCalData(5,-1500); // TOF

        }
        if(TAC_LISE->GetRaw(1)> 0) // TOF DD4 DD6
        {
            T = TAC_LISE->GetCal(1);
            D=(CATSD6Pos-CATSD4Pos)*100;
            V = D/T;
            Beta =V/29.9792458;
            Gamma = 1./sqrt(1.-powf(Beta,2.f));
            SetCalData(16,Beta); // Beta
            SetCalData(11,T); // TOF

            if(xdisp_cm > -1500)
            {
                BrhoWire = BrhoD32*(1+xdisp_cm/(100*T16_LISE));
                A_Q = BrhoWire/3.105/Beta/Gamma;
                SetCalData(15,A_Q); // A/Q
            }
            else
                SetCalData(15,-1500); // A/Q


        }
        else
        {
            SetCalData(15,-1500); // A/Q
            SetCalData(16,-1500); // Beta
            SetCalData(11,-1500); // TOF

        }


////////////////////////////////////////////////////////////////////////
/////////:////////// 			 				CAVIAR						/////////://////////
////////////////////////////////////////////////////////////////////////

#ifdef WITH_ROOT
        // Caviar Gated Data
      if(xdisp_cm > -1500)
				{
        	if (DetD4->GetCal(3) > -1500) 
						{
        			SetCalData(28, DetCaviar->GetCal(0)); //WirePos
	
						}					
					else 						
						{
							SetCalData(28,-1500);					
						}					

            for(UShort_t k=0; k < Cuts2D.size();k++)
            {
                if(Cuts2D.at(k))
                {
                    if(Cuts2D.at(k)->IsInside(GetCal(0),GetCal(7)))
                    {
                        SetCalData(Nparam+k,xdisp_cm);
                    }
                    else if(Cuts2D.at(k)->IsInside(GetCal(3),GetCal(6)))
                    {
                        SetCalData(Nparam+k,xdisp_cm);
                    }
                    else
                    {
                        SetCalData(Nparam+k,-1500);
                    }
                }
                else
                {
                    SetCalData(Nparam+k,-1500);
                }
            }
				}
					else 						
						{
							SetCalData(28,-1500);					
						}					
#endif

    }
    return(isPresent);
    END;
}


#ifdef WITH_ROOT
void IdLISE::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;

    SetHistogramsCal1DFolder("Id1D");
    SetHistogramsCal2DFolder("Id2D");


    if(fMode == MODE_WATCHER)
    {

        if(fRawData)
        {

            SetHistogramsRaw(true);

        }
        if(fCalData)
        {

            SetHistogramsCal(true);

        }

    }
    else if(fMode == MODE_D2R)
    {
        if(fRawData)
        {
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }
        OutAttach(OutTTree);

    }
    else if(fMode == MODE_D2A)
    {
        if(fRawData)
        {
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }
        OutAttach(OutTTree);
    }
    else if(fMode == MODE_R2A)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(i == 0)
                DetList->at(i)->SetInAttachRawV(true);
            else
                DetList->at(i)->SetInAttachRawI(true);
            DetList->at(i)->InAttach(InTTree);
        }

        SetOutAttachCalI(true);

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                if(i == 0)
                {
                    DetList->at(i)->SetHistogramsCalSummary(true);
                    DetList->at(i)->SetOutAttachCalI(false);
                    DetList->at(i)->SetOutAttachCalV(true);
                }
                else
                {
                    DetList->at(i)->SetHistogramsCalSummary(false);
                    DetList->at(i)->SetOutAttachCalI(true);
                }
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if (fMode == MODE_RECAL)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(i == 0)
                DetList->at(i)->SetInAttachCalV(true);
            else
                DetList->at(i)->SetInAttachCalI(true);

            DetList->at(i)->InAttachCal(InTTree);
        }
        SetInAttachCalI(true);
        InAttachCal(InTTree);

        SetOutAttachCalI(true);

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);
                if(i == 0)
                {
                    DetList->at(i)->SetHistogramsCalSummary(true);
                    DetList->at(i)->SetOutAttachCalI(false);
                    DetList->at(i)->SetOutAttachCalV(true);
                }
                else
                {
                    DetList->at(i)->SetHistogramsCalSummary(false);
                    DetList->at(i)->SetOutAttachCalI(true);
                }
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);
    }
    else if (fMode == MODE_CALC)
    {
        SetNoOutput();
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}
void IdLISE::CreateHistogramsCal2D(TDirectory *Dir)
{
    START;
    string Name;

    BaseDetector::CreateHistogramsCal2D(Dir);

    Dir->cd("");

    if(SubFolderHistCal2D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal2D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }
    AddHistoCal(GetCalName(0),GetCalName(7),"ED4_A_Q_D4","dE A_Q D4",2000,2,4,2000,0,2000);
    AddHistoCal(GetCalName(2),GetCalName(7),"ED4_T4","dE TOF D4_HF",1000,100,400,2000,0,2000);
    AddHistoCal(GetCalName(3),GetCalName(6),"ICD6_A_Q_D6","dE A_Q D6",2000,2,4,2000,0,100);
    AddHistoCal(GetCalName(5),GetCalName(6),"ICD6_T6","dE TOF D6_HF",1000,300,600,2000,0,100);
    AddHistoCal(GetCalName(9),GetCalName(8),"dEE_ZDD","dE E ZDD",1000,100,3100,1000,0,10000);
    AddHistoCal(GetCalName(15),GetCalName(6),"ICD6_A_Q_D4_D6","dE A_Q D4_D6",2000,2,4,2000,0,100);
    AddHistoCal(GetCalName(13),GetCalName(2),"TCATSD4_vs_T","T_CATSD4 vs Time (h) ",10000,000,300,400,100,300);
    AddHistoCal(GetCalName(14),GetCalName(5),"TCFA_vs_T","T_CFA vs Time (h) ",10000,00,300,600,200,500);
    AddHistoCal(GetCalName(14),GetCalName(11),"TCATSD4_CFA_vs_T","T_CATSD4_CFA vs Time (h) ",10000,00,300,600,200,500);
    AddHistoCal(GetCalName(14),GetCalName(12),"TCATD6_vs_T","T_CATSD6 vs Time (h) ",10000,00,300,600,200,500);
    AddHistoCal(GetCalName(11),GetCalName(6),"TCATSD4_CFA_vs_dE_D6","T_CATSD4_CFA vs dE_D6 ",10000,00,300,2000,0,100);
    AddHistoCal(GetCalName(17),GetCalName(18),"DD4_Y_vs_X","DD4_Y_vs_X",1024,0,65355,1024,0,65355);
    AddHistoCal(GetCalName(19),GetCalName(20),"DD4_Y_vs_X_mm","DD4_Y_vs_X_mm",1200,-60.,60.,1200,-60.,60.);
    AddHistoCal(GetCalName(21),GetCalName(22),"DD4_Y_vs_X_DE_D4_Cond1","DD4_Y_vs_X_DE_D4_Cond1",1024,0,65355,1024,0,65355);
    AddHistoCal(GetCalName(23),GetCalName(24),"DD4_Y_vs_X_DE_D4_Cond2","DD4_Y_vs_X_DE_D4_Cond2",1024,0,65355,1024,0,65355);
    AddHistoCal(GetCalName(25),GetCalName(26),"DD4_Y_vs_X_DE_D4_Cond3","DD4_Y_vs_X_DE_D4_Cond3",1024,0,65355,1024,0,65355);
    AddHistoCal(GetCalName(29),GetCalName(30),"DD6_1_Y_vs_X","DD6_1_Y_vs_X",1024,0,65355,1024,0,65355);
    AddHistoCal(GetCalName(31),GetCalName(32),"DD6_1_Y_vs_X_Cond","DD6_1_Y_vs_X_Cond",1024,0,65355,1024,0,65355);
    AddHistoCal(GetCalName(47),GetCalName(48),"DDD6_1_Y_C_vs_DDD6_1_X_C","DDD6_1_Y_C_vs_DDD6_1_X_C",1024,-50,50,1024,-50,50);
    AddHistoCal(GetCalName(34),GetCalName(35),"E1D6_Y_vs_E1D6_X","E1D6_Y_vs_E1D6_X",1024,-50,50,1024,-50,50);
    AddHistoCal(GetCalName(36),GetCalName(37),"F62_Y_vs_F62_X","F62_Y_vs_F62_X",1024,-50,50,1024,-50,50);
    AddHistoCal(GetCalName(38),GetCalName(39),"WIN_Y_vs_WIN_X","WIN_Y_vs_WIN_X",1024,-50,50,1024,-50,50);
    AddHistoCal(GetCalName(40),GetCalName(41),"CELL_Y_vs_CELL_X","CELL_Y_vs_CELL_X",1024,-50,50,1024,-50,50);

//-------------------ID-----------------

    AddHistoCal(GetCalName(45),GetCalName(46),"E1D4_vs_T_DD4_HF","E1D4_vs_T_DD4_HF",1024,0,65355,1024,0,65355);
    AddHistoCal(GetCalName(45),GetCalName(47),"E2D4_vs_T_DD4_HF","E2D4_vs_T_DD4_HF",1024,0,65355,1024,0,65355);
    AddHistoCal(GetCalName(44),GetCalName(42),"E1D6_vs_T_DD6_1_HF","E1D6_vs_T_DD6_1_HF",1024,0,65355,1024,0,65355);
    AddHistoCal(GetCalName(45),GetCalName(42),"E1D6_vs_T_DD4_HF","E1D6_vs_T_DD4_HF",1024,0,65355,1024,0,65355);
    AddHistoCal(GetCalName(44),GetCalName(50),"CHIO_vs_T_DD6_1_HF","CHIO_vs_T_DD6_1_HF",1024,0,65355,1024,0,65355);

    END;
}
void IdLISE::CreateHistogramsCal1D(TDirectory *Dir)
{
    START;
    string Name;
    Dir->cd("");
    if(SubFolderHistCal1D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal1D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    AddHistoCal(GetCalName(0),"A_Q_D4","A_Q (D4) ",1000,1.,4.);
    AddHistoCal(GetCalName(1),"Beta_d4","Beta D4 (cm/ns)",1000,0.,1.);
    AddHistoCal(GetCalName(2),"TOF_d4","TOF D4 (ns)",1000,200.,400.);
    AddHistoCal(GetCalName(3),"A_Q_D6","A_Q (D4) ",1000,1.,4.);
    AddHistoCal(GetCalName(4),"Beta_D6","Beta D6 (cm/ns)",1000,0.,1.);
    AddHistoCal(GetCalName(5),"TOF_D6","TOF D6 (ns)",1000,300.,600.);
    AddHistoCal(GetCalName(14),"TimeD4","TimeD4 (ns)",1000,0.,60000.);
    AddHistoCal(GetCalName(15),"A_Q_D4_D6","A_Q (D4_D6) ",1000,1.,4.);
    AddHistoCal(GetCalName(17),"DD4X_Raw","DD4X_Raw",4096, 0., 65355.);
    AddHistoCal(GetCalName(18),"DD4Y_Raw","DD4Y_Raw",4096, 0., 65355.);
    AddHistoCal(GetCalName(19),"DD4X_mm","DD4X_mm",1200,-60,60.);
    AddHistoCal(GetCalName(20),"DD4Y_mm","DD4Y_mm",1200,-60,60.);
    AddHistoCal(GetCalName(21),"DD4X_Raw_DE_D4_Cond1","DD4X_Raw_DE_D4_Cond1",4096, 0., 65355.);
    AddHistoCal(GetCalName(23),"DD4X_Raw_DE_D4_Cond2","DD4X_Raw_DE_D4_Cond2",4096, 0., 65355.);
    AddHistoCal(GetCalName(25),"DD4X_Raw_DE_D4_Cond3","DD4X_Raw_DE_D4_Cond3",4096, 0., 65355.);
    AddHistoCal(GetCalName(22),"DD4Y_Raw_DE_D4_Cond1","DD4Y_Raw_DE_D4_Cond1",4096, 0., 65355.);
    AddHistoCal(GetCalName(24),"DD4Y_Raw_DE_D4_Cond2","DD4Y_Raw_DE_D4_Cond2",4096, 0., 65355.);
    AddHistoCal(GetCalName(26),"DD4Y_Raw_DE_D4_Cond3","DD4Y_Raw_DE_D4_Cond3",4096, 0., 65355.);
    AddHistoCal(GetCalName(27),"TDD4_Cond1","TDD4_Cond1",4096, 0., 65355.);
    AddHistoCal(GetCalName(28),"CavPos","CavPos",100,0.,100.);
    AddHistoCal(GetCalName(29),"DD6_1_X","DD6_1_X",4096, 0., 65355.);
    AddHistoCal(GetCalName(30),"DD6_1_Y","DD6_1_Y",4096, 0., 65355.);
    AddHistoCal(GetCalName(31),"DD6_1_X_Cond","DD6_1_X_Cond",4096, 0., 65355.);
    AddHistoCal(GetCalName(32),"DD6_1_Y_Cond","DD6_1_Y_Cond",4096, 0., 65355.);
    AddHistoCal(GetCalName(33),"DD6_1_T_Cond","DD6_1_T_Cond",4096, 0., 65355.);
    AddHistoCal(GetCalName(34),"E1D6_X","E1D6_X",16384, 0., 65355.);
    AddHistoCal(GetCalName(35),"E1D6_Y","E1D6_Y",16384, 0., 65355.);
    AddHistoCal(GetCalName(36),"F62_X","F62_X",16384, 0., 65355.);
    AddHistoCal(GetCalName(37),"F62_Y","F62_Y",16384, 0., 65355.);
    AddHistoCal(GetCalName(38),"WIN_X","WIN_X",16384, 0., 65355.);
    AddHistoCal(GetCalName(39),"WIN_Y","WIN_Y",16384, 0., 65355.);
    AddHistoCal(GetCalName(40),"CELL_X","CELL_X",16384, 0., 65355.);
    AddHistoCal(GetCalName(41),"CELL_Y","CELL_Y",16384, 0., 65355.);
    AddHistoCal(GetCalName(42),"E1D6","E1D6",16384, 0., 65355.);
    AddHistoCal(GetCalName(43),"E2D6","E2D6",16384, 0., 65355.);
    AddHistoCal(GetCalName(44),"T_DD6_1_HF","T_DD6_1_HF",16384, 0., 65355.);
    AddHistoCal(GetCalName(45),"T_DD4_HF","T_DD4_HF",16384, 0., 65355.);
    AddHistoCal(GetCalName(46),"E1D4","E1D4",16384, 0., 182.);
    AddHistoCal(GetCalName(47),"E2D4","E2D4",16384, 0., 65355.);
    AddHistoCal(GetCalName(48),"DD6_1_X_C","DD6_1_X_C",1024,-50,50);
    AddHistoCal(GetCalName(49),"DD6_1_Y_C","DD6_1_Y_C",1024,-50,50);
    AddHistoCal(GetCalName(50),"CHIO_LISE","CHIO_LISE",16384, 0., 65355.);

    for(UShort_t k=0;k<NumberOfCuts;k++)
    {
        AddHistoCal(GetCalName(Nparam+k),Form("xdisp_cm_cut%02d",k),Form("xdisp_cm_cut%02d",k),400,-20.,20.);
    }

    END;
}



#endif
