/****************************************************************************
 *    Copyright (C) 2022 by Antoine Lemasson
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "Caviar.hh"

Caviar::Caviar(const Char_t *Name,
               UShort_t NWires,
               Bool_t RawData,
               Bool_t CalData,
               Bool_t DefaultCalibration,
               const Char_t *NameExt
               )
    : BaseDetector(Name, 4, false, CalData, DefaultCalibration,false,NameExt)
{
    START;
    NumberOfSubDetectors = NWires;
    fRawDataSubDetectors  = RawData;
    DetManager *DM = DetManager::getInstance();
    // Default Values
    BrhoD32 = 2.4352 ;// Brho D32 in Tm la rigidité magnétique mesurée expérimentalement dans le second dipôle
    T16_LISE = 1.653 ;// cm/% le terme de dispersion de l’optique employée lors de l’expérience
    CATSD6Pos = 42.0235 ; // Distance in meters CAVIAR - CFA
    CATSD4Pos = 19.236 ; // Distance in meters CAVIAR - CFA
    CaviarRefWire = 47.4;

    ReadPosition();

    AllocateComponents();

    // GetRefeence TS for TS Plots
    RefTS = 0;

    try{
        char *TS = getenv("REF_LISETS");
        if (TS!=NULL)
        {
           RefTS = atoll(TS);

           cout << "<" << Name <<  "> REF TS from environment variable REF_LISETS : "<< RefTS <<endl;
        }else
        {
            cout << "/!\\ Could not Retrieve REF TS from environment variable REF_LISETS" <<endl;
        }

    }
    catch(...)
    {

    }

    CAVIARTS = DM->GetDetector("DATATRIG_CAVIAR");


    if(CalData)
    {
        sprintf(CalNameI[0],"WirePos");
        sprintf(CalNameI[1],"WireM");
        sprintf(CalNameI[2],"xdisp_cm");
        sprintf(CalNameI[3],"TimeCaviar");
     }


    END;
}
Caviar::~Caviar(void)
{
    START;
    END;

}

void Caviar::ReadPosition(void)
{
    START;
    if(fCalData)
    {

        MIFile *IF;
        char Line[255];
        stringstream *InOut;
        UShort_t Len=255;
        Char_t *Ptr=NULL;

        InOut = new stringstream();
        *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
        *InOut>>Line;
        InOut = CleanDelete(InOut);

        if((IF = CheckCalibration(Line)))
        {
            // Position
            try
            {
                // Read Detector Positions
                IF->GetLine(Line,Len);
                while((Ptr = CheckComment(Line)))
                {
                    IF->GetLine(Line,Len);
                    L->File << Line << endl;
                }
                InOut = new stringstream();
                *InOut << Line;
                *InOut >> CaviarRefWire;
                if(InOut->fail())
                {
                    InOut = CleanDelete(InOut);
                    Char_t Message[500];
                    sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read CaviarRefWire value !", DetectorName, DetectorNameExt, IF->GetFileName());
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                InOut = CleanDelete(InOut);
                cout << "    <" << DetectorName << "><" << DetectorNameExt << "> CaviarRefWire set to " << CaviarRefWire << "" <<  endl;
                L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> CaviarRefWire set to " << CaviarRefWire << "" <<  endl;
                IF->GetLine(Line,Len);
                while((Ptr = CheckComment(Line)))
                {
                    IF->GetLine(Line,Len);
                    L->File << Line << endl;
                }
                InOut = new stringstream();
                *InOut << Line;
                *InOut >> WireMin;
                if(InOut->fail())
                {
                    InOut = CleanDelete(InOut);
                    Char_t Message[500];
                    sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read WireMin value !", DetectorName, DetectorNameExt, IF->GetFileName());
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                InOut = CleanDelete(InOut);
                cout << "    <" << DetectorName << "><" << DetectorNameExt << "> CaviarRefWireMin set to " << WireMin << "" <<  endl;
                L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> CaviarRefWireMin set to " << WireMin << "" <<  endl;
                IF->GetLine(Line,Len);
                while((Ptr = CheckComment(Line)))
                {
                    IF->GetLine(Line,Len);
                    L->File << Line << endl;
                }
                InOut = new stringstream();
                *InOut << Line;
                *InOut >> WireMax;
                if(InOut->fail())
                {
                    InOut = CleanDelete(InOut);
                    Char_t Message[500];
                    sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read WireMax value !", DetectorName, DetectorNameExt, IF->GetFileName());
                    MErr * Er= new MErr(WhoamI,0,0, Message);
                    throw Er;
                }
                InOut = CleanDelete(InOut);
                cout << "    <" << DetectorName << "><" << DetectorNameExt << "> CaviarRefWireMax set to " << WireMax << "" <<  endl;
                L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> CaviarRefWireMax set to " << WireMax << "" <<  endl;

            }
            catch(...)
            {
                IF = CleanDelete(IF);
                throw;
            }
            IF = CleanDelete(IF);
        }
    }
 END;
}

void Caviar::AllocateComponents(void)
{
    START;



    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = false;
    Bool_t DefCal = true;
    Bool_t PosInfo = true;

    // Wire Patterns words
    BaseDetector *D = new BaseDetector("WiresWords",6,RawData,false,DefCal,PosInfo,"",true);
    if(RawData)
    {
        D->SetParameterName("CAVIAR_1_16",0);
        D->SetParameterName("CAVIAR_17_32",1);
        D->SetParameterName("CAVIAR_33_48",2);
        D->SetParameterName("CAVIAR_49_64",3);
        D->SetParameterName("CAVIAR_65_80",4);
        D->SetParameterName("CAVIAR_81_96",5);

        D->SetGateRaw(0,65355);
    }
    AddComponent(D);
    // Wires
    BaseDetector *Wires = new BaseDetector("Wire",NumberOfSubDetectors,false,true,DefCal,false,"");
    Wires->SetCalHistogramsParams(2,0,2,"","Cal");
    AddComponent(Wires);


    END;
}

void Caviar::SetParameters(Parameters* Par,Map* Map)
{
    START;
    NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();

    // VME EBYEDAT for WIRES
    DetList->at(0)->SetParameters(Par,Map);

    END;
}
void  Caviar::TreatWires(void)
{
    START;
    UShort_t  bit[6];
    UShort_t Wire;
    Wire = 0;

    //  Firt Extract the Wire Info
    if(DetList->at(0)->GetRawM()==6)
    {
        for(ULong64_t k=0; k < 6; k++ )
        {
            bit[k] = DetList->at(0)->GetRaw(k);
            //cout << k << " " << bit[k] << endl;
        }

        for(Int_t i=0;i<6;i++){
            for(Int_t j=15;j>-1;j--){
                Double_t a = bit[i]/pow(2.0,(double)(j));
                if(a>=1){
                    bit[i]=bit[i]-pow(2.0,(double)(j));
                    Wire = j+i*16+1;
/*
// Config LISE 2023 -> N'importe quoi !!! C.f. analyse CAVIAR_2024.ods
                    if (Wire == 15) Wire = 43;
                    if (Wire == 31) Wire = 44;
                    if (Wire == 24) 	Wire = 93;
                    if (Wire == 25) 	Wire = 95;
                    if (Wire == 65) 	Wire = 28;
                    //if (Wire == 65) Wire = 93; // r\E9par\E9 par Maud
                    //if(Wire != 80)
// Modif 20/03/2024
                    if (Wire == 1) Wire = 28;
                    if (Wire == 2) Wire = 32;
                    if (Wire == 3) Wire = 43;
                    if (Wire == 4) Wire = 44;
                    if (Wire == 65) Wire = 80;
                    if (Wire == 93) Wire = 77;
                    if (Wire == 94) Wire = 48;
// Modif 10/04/2024
                    if (Wire == 92) Wire = 45;
// Modif 15/04/2024
                    if (Wire == 88) Wire = 99;
                    if (Wire == 89) Wire = 44;
                    if (Wire == 90) Wire = 45;
                    if (Wire == 91) Wire = 46;
*/
// Modif 26/04/2024

// Fin modif                    

	if(Wire >= WireMin && Wire <= WireMax)
                    DetList->at(1)->SetCalData(Wire-1,1);

                }
            }
        }
    }else{
        if( DetList->at(0)->GetRawM()>0)
        {
            cout << "\n !Warning!  Caviar Mult != 6 " << DetList->at(0)->GetRawM() << endl;
            DetList->at(0)->PrintRaw();
        }
    }




END;
}


Bool_t Caviar::Treat(void)
{
    START;
    Ctr->at(0)++;

    Float_t AverageWire=-1500;


    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->Treat();
        }
    }

    if(fCalData)
    {
        ULong64_t CAVIARTime = 0;
        CAVIARTime = CAVIARTS->GetRawTS(0);
        if(CAVIARTime>0)
        {
            SetCalData(3,(CAVIARTime-RefTS)*1e-8/60/60);
//            cout << "CAVIAR : " <<CAVIARTime << " - " << RefTS << " " << (CAVIARTime-RefTS)*10. << " nsec "<<  endl;
        }
        else
            SetCalData(3,-1500);

        TreatWires();

        if( DetList->at(1)->GetCalM()==1){
            AverageWire = DetList->at(1)->GetNrAt(0);
        }
        else if(DetList->at(1)->GetCalM()>1){
            AverageWire=0;
            for(Int_t i=0;i<DetList->at(1)->GetCalM();i++){
                AverageWire += DetList->at(1)->GetNrAt(i);
            }
            AverageWire = AverageWire/DetList->at(1)->GetCalM();
        }

        SetCalData(0,AverageWire);
        SetCalData(1,DetList->at(1)->GetCalM());
        if(AverageWire>-1500)
            SetCalData(2,(AverageWire-CaviarRefWire)/10.);
        else
            SetCalData(2,-1500.);

      }
    return(isPresent);
  END;
}



#ifdef WITH_ROOT
void Caviar::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;


//  SetHistogramsCal2DFolder("Caviar1D");
//  SetHistogramsCal1DFolder("Caviar2D");
  SetHistogramsCal1DFolder("");
  SetHistogramsCal2DFolder("");

  // Set histogram Hierarchy
  for(UShort_t i = 0;i<DetList->size();i++)
     {
        DetList->at(i)->SetMainHistogramFolder("");
        DetList->at(i)->SetHistogramsRaw1DFolder("");
        DetList->at(i)->SetHistogramsRaw2DFolder("");
     }

  if(fMode == MODE_WATCHER)
    {
        if(fRawData)
          {
             SetHistogramsRaw(true);
          }
        if(fCalData)
          {
             SetHistogramsCal(true);
          }
        for(UShort_t i = 0;i<DetList->size();i++)
          {
              if(DetList->at(i)->HasRawData())
                  if(i == 0)
                  {
                      DetList->at(i)->SetHistogramsRaw(false);
                      DetList->at(i)->SetHistogramsRawSummary(true);
                  }
              if(DetList->at(i)->HasCalData())
              {
                  DetList->at(i)->SetHistogramsCal(false);
                  DetList->at(i)->SetHistogramsCalSummary(false);
                  if(i==1)
                  {
                      DetList->at(i)->SetHistogramsCal(true);
                      DetList->at(i)->SetHistogramsCalSummary(true);
                  }

              }
        }
     }
  else if(fMode == MODE_D2R)
     {
        for(UShort_t i = 0;i<DetList->size();i++)
          {
             if(DetList->at(i)->HasRawData())
                {
                  DetList->at(i)->SetHistogramsRaw(true);
                  if(i == 0)
                     {
                        DetList->at(i)->SetHistogramsRawSummary(true);
                        DetList->at(i)->SetOutAttachRawV(true);
                        DetList->at(i)->SetOutAttachRawI(false);
                     }
                  else
                     {
                        DetList->at(i)->SetOutAttachRawI(true);
                     }
                }
             DetList->at(i)->OutAttach(OutTTree);
          }
        OutAttach(OutTTree);

     }
  else if(fMode == MODE_D2A)
     {
        if(fRawData)
          {
             SetHistogramsRaw(false);
             SetOutAttachRawI(false);
             SetOutAttachRawV(false);
          }
        if(fCalData)
          {
             SetHistogramsCal(true);
             SetOutAttachCalI(true);
             SetOutAttachCalV(false);
          }

        for(UShort_t i = 0;i<DetList->size();i++)
          {
             if(DetList->at(i)->HasRawData())
                {
                  DetList->at(i)->SetHistogramsRaw1D(false);
                  DetList->at(i)->SetHistogramsRaw2D(false);

                   if(i == 0)
                     {
                        DetList->at(i)->SetHistogramsRawSummary(false);
                        DetList->at(i)->SetOutAttachRawV(false);
                        DetList->at(i)->SetOutAttachRawI(false);
                     }
                    else
                      {
                         DetList->at(i)->SetOutAttachRawV(true);
                         if(i==1)
                             DetList->at(i)->SetHistogramsRawSummary(true);
                         else
                             DetList->at(i)->SetHistogramsRawSummary(false);

                         DetList->at(i)->SetHistogramsCalSummary(false);


                      }
                }
             if(DetList->at(i)->HasCalData())
                {
                  DetList->at(i)->SetHistogramsCal1D(false);
                  DetList->at(i)->SetHistogramsCal2D(false);
                  DetList->at(i)->SetHistogramsCalSummary(false);
                  DetList->at(i)->SetOutAttachCalI(false);
                  DetList->at(i)->SetOutAttachCalV(true);
                  if(i==1)
                  {
                      DetList->at(i)->SetHistogramsCal(true);
                    DetList->at(i)->SetHistogramsCalSummary(true);
                  }
                }
             DetList->at(i)->OutAttach(OutTTree);
          }

        OutAttach(OutTTree);
     }
  else if(fMode == MODE_R2A)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
          {
              if(i == 0)
                 DetList->at(i)->SetInAttachRawV(true);
              else
                 DetList->at(i)->SetInAttachRawI(true);
             DetList->at(i)->InAttach(InTTree);
          }

        SetOutAttachCalI(true);

        for(UShort_t i = 0;i<DetList->size();i++)
          {
              if(DetList->at(i)->HasCalData())
                {
                  DetList->at(i)->SetHistogramsCal1D(true);
                  DetList->at(i)->SetHistogramsCal2D(true);
                  if(i == 0)
                     {
                        DetList->at(i)->SetHistogramsCalSummary(true);
                        DetList->at(i)->SetOutAttachCalI(false);
                        DetList->at(i)->SetOutAttachCalV(true);
                     }
                  else
                     {
                        DetList->at(i)->SetHistogramsCalSummary(false);
                        DetList->at(i)->SetOutAttachCalI(true);
                        // DetList->at(i)->SetOutAttachRawI(true);
                     }
                }
             DetList->at(i)->OutAttach(OutTTree);
          }
        OutAttach(OutTTree);

    }
  else if (fMode == MODE_RECAL)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
      {
        if(i == 0)
          DetList->at(i)->SetInAttachCalV(true);
        else
          DetList->at(i)->SetInAttachCalI(true);

        DetList->at(i)->InAttachCal(InTTree);
      }
    SetInAttachCalI(true);
    InAttachCal(InTTree);

    SetOutAttachCalI(true);

    for(UShort_t i = 0;i<DetList->size();i++)
      {
        if(DetList->at(i)->HasCalData())
          {
        DetList->at(i)->SetHistogramsCal1D(true);
        DetList->at(i)->SetHistogramsCal2D(true);
        if(i == 0)
          {
            DetList->at(i)->SetHistogramsCalSummary(true);
            DetList->at(i)->SetOutAttachCalI(false);
            DetList->at(i)->SetOutAttachCalV(true);
          }
        else
          {
            DetList->at(i)->SetHistogramsCalSummary(false);
            DetList->at(i)->SetOutAttachCalI(true);
            // DetList->at(i)->SetOutAttachRawI(true);
          }
          }
        DetList->at(i)->OutAttach(OutTTree);
      }
    OutAttach(OutTTree);
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else
     {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
     }
  END;
}


#endif


#ifdef WITH_ROOT
void Caviar::CreateHistogramsCal2D(TDirectory *Dir)
{
    START;
    string Name;

    Dir->cd("");

    if(SubFolderHistCal2D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal2D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }
    AddHistoCal(GetCalName(3),GetCalName(2),"Xdisp_Time","X Disp(cm) vs Time(h) ",10000,0,300,40,-10,10);
    AddHistoCal(GetCalName(0),GetCalName(1),"Wirepos_mult","Wire pos vs Mult ",100,0,100,96,0,96);

    END;
}
void Caviar::CreateHistogramsCal1D(TDirectory *Dir)
{
    START;
    string Name;
    Dir->cd("");
    if(SubFolderHistCal1D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal1D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    AddHistoCal(GetCalName(0),"WirePos","Wire Position(D4) ",100,0,100);
    AddHistoCal(GetCalName(1),"WireM","Wire Multiplicity",96,0,96);
    AddHistoCal(GetCalName(2),"XDisp","X Disp (cm)",400,-20.,20.);
    END;
}
#endif
