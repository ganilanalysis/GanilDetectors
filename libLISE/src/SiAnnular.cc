#include "SiAnnular.hh"

SiAnnular::SiAnnular(const Char_t *Name,
                     UShort_t NDetectors,
                     Bool_t RawData,
                     Bool_t CalData,
                     Bool_t DefaultCalibration,
                     const Char_t *NameExt,
                     UShort_t MaxMult

                     )
    : BaseDetector(Name,7, false, CalData, DefaultCalibration,false,NameExt,true)
{
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;
      fMaxMultSubDetectors = MaxMult;

    DetManager *DM = DetManager::getInstance();

    AllocateComponents();
#ifdef WITH_ROOT
    rn = new TRandom();
#endif

    if(CalData)
    {
        sprintf(CalNameI[0],"%s_X", DetectorName);
        sprintf(CalNameI[1],"%s_Y", DetectorName);
        sprintf(CalNameI[2],"%s_Z", DetectorName);
        sprintf(CalNameI[3],"%s_Theta", DetectorName);
        sprintf(CalNameI[4],"%s_Phi", DetectorName);
        sprintf(CalNameI[5],"%s_Erings", DetectorName);
        sprintf(CalNameI[6],"%s_Esectors", DetectorName);
//        SetGateCal(-50,50,0);
//        SetGateCal(-50,50,1);
//#ifdef WITH_ROOT
//        SetGateCal(0,TMath::Pi(),3);
//        SetGateCal(0,2*TMath::Pi(),4);
//#endif
//        SetGateCal(-10,16000,5);
//        SetGateCal(-10,16000,6);
    }
    //  char Line[255];
    //  stringstream *InOut;
    //  InOut = new stringstream();
    //  *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
    //  *InOut>>Line;
    //  InOut = CleanDelete(InOut);
    //  MIFile *IF = new MIFile(Line);
    //  for(int i=0;i<2;i++)
    //    IF->GetLine(Line,255);
    //  InOut = new stringstream();
    //  *InOut<<Line;
    //  *InOut>>Zref;
    //  InOut = CleanDelete(InOut);
    //  IF = CleanDelete(IF);

    END;
}
SiAnnular::~SiAnnular(void)
{
    START;

#ifdef WITH_ROOT
    delete rn;
#endif

    END;

}


void SiAnnular::CalculatePosition(void)
{
    START;
    Double_t Rand=0;
    isPresent = false;
    Zref = 384.2; //mm from target
    #ifdef WITH_ROOT
      Float_t pi = TMath::Pi();
    #else
      Float_t pi = 3.14159265358979312;
    #endif
    if(DetList->at(0)->GetCalM() == 1 && DetList->at(1)->GetCalM() == 1 ){

        Int_t Str_i = -1;
        Int_t Sec_i = -1;

        if(DetList->at(0)->GetNrAt(0)>=0 && DetList->at(0)->GetNrAt(0) <= 31){
            Str_i = DetList->at(0)->GetNrAt(0) % 16;
        }
        else if(DetList->at(0)->GetNrAt(0) >=32 && DetList->at(0)->GetNrAt(0) <= 63){
            Str_i = 15-(DetList->at(0)->GetNrAt(0) % 16);
        }
#ifdef WITH_ROOT
        Rand = rn->Uniform();
#else
        Rnd->Next();
        Rand = Rnd->Value();
#endif
        //cout << DetList->at(0)->GetNrAt(0) << " " << Str_i << "\n";
        Double_t rho = 24.0 + Str_i*2  + (Rand) * 2.0 ;
        Double_t rho0 = 24.0 + Str_i*2  + 0.5 *  2.0 ;

        Double_t theta = atan (rho/Zref)  ;
        Double_t thetadeg = theta / pi * 180.0;
        Double_t theta0 = atan (rho0/Zref)  ;
        Double_t theta0deg = theta0 / pi * 180.0;
#ifdef WITH_ROOT
        Rand = rn->Uniform();
#else
        Rnd->Next();
        Rand = Rnd->Value();
#endif
        Sec_i = DetList->at(1)->GetNrAt(0);
        Double_t phi = Sec_i * pi/8 +  (Rand) * pi/8;
        Double_t phideg = phi / pi * 180.0;
        Double_t phi0 = Sec_i * pi/8 +  0.5 * pi/8;
        Double_t phi0deg = phi0 / pi * 180.0;

        Double_t rad = Zref/cos(theta);
        Double_t rad0 = Zref/cos(theta0);
        Float_t S1_X = rad * sin(theta) * cos(phi);
        Float_t S1_Y = rad * sin(theta) * sin(phi);
        Float_t S1_X0 = rad0 * sin(theta0) * cos(phi0);
        Float_t S1_Y0 = rad0 * sin(theta0) * sin(phi0);

        if(Sec_i > -1 && Str_i > -1 )
        {
            SetCalData(0,S1_X); // in mm
            SetCalData(1,S1_Y); // in mm
            SetCalData(2,Zref); // in mm
            SetCalData(3,theta); // in rad
            SetCalData(4,phi); // in rad
            SetCalData(5,DetList->at(0)->GetCalAt(0)); // E Rings
            SetCalData(6,DetList->at(1)->GetCalAt(0) ); // E Sec
        }
        else
        {
            SetCalData(0,-1500);
            SetCalData(1,-1500);
            SetCalData(2,-1500);
            SetCalData(3,-1500);
            SetCalData(4,-1500);
            SetCalData(5,-1500);
            SetCalData(6,-1500);
        }
    }
    else {
        SetCalData(0,-1500);
        SetCalData(1,-1500);
        SetCalData(2,-1500);
        SetCalData(3,-1500);
        SetCalData(4,-1500);
        SetCalData(5,-1500);
        SetCalData(6,-1500);

    }



    END;
}


void SiAnnular::AllocateComponents(void)
{
    START;
    Char_t Name[200];
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Bool_t PosInfo = false;


    sprintf(Name,"%s_Str",DetectorName);

    BaseDetector *Str = new BaseDetector(Name,64,RawData,CalData,DefCal,PosInfo,"",true,true,fMaxMultSubDetectors,true);
    if(RawData)
    {
        for (int i=0; i < 64; i++)
        {
            if(i<32)
                sprintf(Name,"%s_U_Str_%02d",DetectorName,i); // U0-31
            else
                sprintf(Name,"%s_D_Str_%02d",DetectorName,i-32); // D0-31
            Str->SetParameterName(Name, i);
            Str->SetRawHistogramsParams(1000,0,65535);
        }
        Str->SetGateRaw(0,65000);
    }

    Str->SetCalHistogramsParams(1000,0,1000,"MeV");
    //    Str->SetCalHistogramsParams(1000,0,200,"MeV");
    if(CalData)
        //Str->SetGateCal(0,200);
        Str->SetGateCal(0,66000);

    AddComponent(Str);


    sprintf(Name,"%s_Sec",DetectorName);
    BaseDetector *Sec = new BaseDetector(Name,16,RawData,CalData,DefCal,PosInfo,"",true,true,fMaxMultSubDetectors,true);
    if(RawData)
    {
        for (int i=0; i < 16; i++)
        {
            sprintf(Name,"%s_S_%02d",DetectorName,i);
            Sec->SetParameterName(Name, i);

            Sec->SetRawHistogramsParams(1000,0,65535);
        }
        Sec->SetGateRaw(0,65000);
    }

    Sec->SetCalHistogramsParams(1000,0,2000,"MeV");
    //    Sec->SetCalHistogramsParams(1000,0,200,"MeV");
    if(CalData)
        //Sec->SetGateCal(0,200);
        Sec->SetGateCal(0,66000);

    AddComponent(Sec);


    END;
}

void SiAnnular::SetParametersNUMEXO(NUMEXOParameters* PL,Map* Map)
{
    START;

    NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();

    for (UShort_t i = 0; i < DetList->size(); i++)
        DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);

    END;
}

Bool_t SiAnnular::Treat(void)
{
    START;
    Ctr->at(0)++;

    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->Treat();
        }
    }
    if(fCalData)
        CalculatePosition();



    return(isPresent);
    END;
}

#ifdef WITH_ROOT
void SiAnnular::CreateHistogramsCal1D(TDirectory *Dir)
{
    START;
    string Name;

    Dir->cd("");

    if(SubFolderHistCal1D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal1D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }
    AddHistoCal(CalNameI[0],Form("%s_X",DetectorName),Form("%s_X (mm)",DetectorName),300,-60,60);
    AddHistoCal(CalNameI[1],Form("%s_Y",DetectorName),Form("%s_Y (mm)",DetectorName),300,-60,60);
    AddHistoCal(CalNameI[3],Form("%s_Theta",DetectorName),Form("%s_Theta (rad)",DetectorName),200,0,0.3);
    AddHistoCal(CalNameI[4],Form("%s_Phi",DetectorName),Form("%s_Phi (rad)",DetectorName),200,0,6.4);
    AddHistoCal(CalNameI[5],Form("%s_dE",DetectorName),Form("%s_dE (rings) (MeV)",DetectorName),1000,0,1000);
    AddHistoCal(CalNameI[6],Form("%s_E",DetectorName),Form("%s_E (sectors) (MeV)",DetectorName),1000,0,2000);

    END;
}
#endif

#ifdef WITH_ROOT
void SiAnnular::CreateHistogramsCal2D(TDirectory *Dir)
{
    START;
    string Name;

    BaseDetector::CreateHistogramsCal2D(Dir);

    Dir->cd("");

    if(SubFolderHistCal2D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal2D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }
    AddHistoCal(CalNameI[0],CalNameI[1],Form("%s_X_vs_Y",DetectorName),Form("%s_X_vs_Y",DetectorName),300,-60,60,300,-60,60);
    AddHistoCal(CalNameI[3],CalNameI[4],Form("%s_th_vs_phi",DetectorName),Form("%s_th_vs_phi",DetectorName),200,0,0.3,200,0,6.4);
    AddHistoCal(CalNameI[6],CalNameI[5],Form("%s_dE_E",DetectorName),Form("%s_dE_E",DetectorName),1000,0,2000,1000,0,1000);

    END;
}
#endif

#ifdef WITH_ROOT
void SiAnnular::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;
    string Name;
    //SetMainHistogramFolder("SiAnnular");
    SetHistogramsCal1DFolder("S1_1D");
    SetHistogramsCal2DFolder("S1_2D");

    // Set histogram Hierarchy
    for(UShort_t i = 0;i<DetList->size();i++)
    {
        DetList->at(i)->SetMainHistogramFolder("");
        // DetList->at(i)->SetHistogramsRaw1DFolder("Raw1D");
        DetList->at(i)->SetHistogramsRaw1DFolder("S1_1D");
        DetList->at(i)->SetHistogramsRaw2DFolder("S1_2D");
        DetList->at(i)->SetHistogramsCal1DFolder("S1_1D");
        DetList->at(i)->SetHistogramsCal2DFolder("S1_2D");
    }

    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(true);
        }
        if(fCalData)
        {
            SetHistogramsCal1D(true);
            SetHistogramsCal2D(true);
            SetHistogramsCalSummary(false);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(true);

                DetList->at(i)->SetHistogramsRawSummary(true);
            }

            if(DetList->at(i)->HasCalData())
            {
            //                DetList->at(i)->SetHistogramsCal(true);
                DetList->at(i)->SetHistogramsCalSummary(true);
            }
        }
    }
    else if(fMode == MODE_D2R)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(true);

                DetList->at(i)->SetHistogramsRawSummary(true);

                DetList->at(i)->SetOutAttachRawV(true);
                DetList->at(i)->SetOutAttachRawI(false);
                DetList->at(i)->SetOutAttachTS(true);

            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if(fMode == MODE_D2A)
    {
        if(fRawData)
        {
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(true);

            SetHistogramsCal1D(true);
            SetHistogramsCal2D(true);
            SetHistogramsCalSummary(false);
            SetOutAttachCalI(true);
            SetOutAttachCalV(false);
        }

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw1D(false);
                DetList->at(i)->SetHistogramsRaw2D(true);

                DetList->at(i)->SetHistogramsRawSummary(true);
                DetList->at(i)->SetOutAttachTS(true);

                DetList->at(i)->SetOutAttachRawV(true);
                DetList->at(i)->SetOutAttachRawI(false);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(false);
                DetList->at(i)->SetHistogramsCal2D(true);

                DetList->at(i)->SetHistogramsCalSummary(true);
//                DetList->at(i)->SetOutAttachCalI(true);
//                DetList->at(i)->SetOutAttachCalV(true);

            }
            DetList->at(i)->OutAttach(OutTTree);
        }

        OutAttach(OutTTree);
    }
    else if(fMode == MODE_R2A)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            DetList->at(i)->SetInAttachRawV(true);
            DetList->at(i)->InAttach(InTTree);
        }

        SetOutAttachCalI(true);

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);

                DetList->at(i)->SetHistogramsCalSummary(true);
                DetList->at(i)->SetOutAttachCalI(false);
                DetList->at(i)->SetOutAttachCalV(true);

            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if (fMode == MODE_RECAL)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {

            DetList->at(i)->SetInAttachCalV(true);

            DetList->at(i)->InAttachCal(InTTree);
        }
        SetInAttachCalI(true);
        InAttachCal(InTTree);

        SetOutAttachCalI(true);

        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCal1D(true);
                DetList->at(i)->SetHistogramsCal2D(true);

                DetList->at(i)->SetHistogramsCalSummary(false);
                DetList->at(i)->SetOutAttachCalI(true);
                // DetList->at(i)->SetOutAttachRawI(true);

            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);
    }
    else if (fMode == MODE_CALC)
    {
        SetNoOutput();
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }
    END;
}


#endif
