/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    Diego Ramos, diego.ramos@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _FST_ID_CLASS
#define _FST_ID_CLASS
/**
 * @file   FST_ID.hh
 * @Author Diego Ramos (diego.ramos@ganil.fr)
 * @date   January, 2022
 * @brief  Identification Class
 *
 * Identification Class
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#include "FST_Position.hh"
#include "TMW.hh"

class FST_ID : public BaseDetector
{
public :
    //! Constructor with DetManager
    FST_ID(const Char_t * Name //!< Detector Name
                   );
    ~FST_ID(void); //!< Destructor

  void ReadPosition(void);

#ifdef WITH_ROOT
    //! Create 1D Histograms for Calculated Parameters
    void CreateHistogramsCal1D(TDirectory *Dir);
    //! Create 2D Histograms for Calculated Parameters
    void CreateHistogramsCal2D(TDirectory *Dir);
    //! Set Input/OutPut Option
    void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
  //! Treat the data
  Bool_t Treat(void);
  
  //! Return Time of Flight (ns)
  inline Float_t GetT(void){return FST_TOF;};
  //! Return Velocity (cm/ns)
  inline Float_t GetV(void){return FST_V;};
  //! Return E (Mev)
  inline Float_t GetE(void){return FST_Etot;};
  
private: 
  //! Reset Local Variable
  void Init(void);
  //! Calculate the Calibrated parameters
  Bool_t Calculate(void);
  
  void Get_dEnm(Float_t FEpost, Float_t FV, Float_t FThX, Float_t FThY);
  void Get_dEnmPPAC(Float_t FEpost, Float_t FV);
  
protected :
  FST_Position *FST_Start;
  FST_Position *FST_Stop;
  BaseDetector *FST_TACS;
  BaseDetector *FST_E;
  BaseDetector *FST_TarIc;
  TMW *FST_TMW;
  DetManager *DM;

  Float_t FST_TOF;
  Float_t FST_LOF;
  Float_t FST_V;
  Float_t FST_Beta;
  Float_t FST_Gamma;
  Float_t FST_Th;
  Float_t FST_Ph;
  Float_t FST_ThL;
  Float_t FST_PhL;
  Float_t FST_Etot;
  Float_t FST_dEnm;
  Float_t FST_dEnmStart;
  Float_t FST_dEnmStop;
  Float_t FST_dEnmChio;
  Float_t FST_M;
  Float_t FST_dE;

  Float_t POS_REF[3];
  Float_t FST_ANGLE;
  Float_t FST_TOFFSET;

};
#endif
