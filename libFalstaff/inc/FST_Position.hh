/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    Diego Ramos, diego.ramos@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _FSTPOSITION_CLASS
#define _FSTPOSITION_CLASS
/**
 * @file   FST_Position.hh
 * @Author Diego Ramos (diego.ramos@ganil.fr)
 * @date   December, 2021
 * @version 0.1
 * @todo Read Calibration and thresholds
 * @brief  FST_Position Class
 *
 * FST_Position class
 */

#include "Defines.hh"
#include "BaseDetector.hh"

class FST_Position : public BaseDetector
{
public:
  FST_Position(const Char_t * Name,  //!< Detector Name
                                  UShort_t NXDetectors,   //!< Number of sub detectors X
                                  UShort_t NYDetectors,   //!< Number of sub detectors Y
                                  Bool_t RawData,        //!< Has Raw Data
                                  Bool_t CalData,        //!< Has Calibrated Data
                                  Bool_t DefaultCalibration,//!< Used Default Calibration
                                  const Char_t * NameExt //!< Name extension
               );
  ~FST_Position(void);

  //! Clear At the end of event;
  void Clear(void);
  //! True X position is present
  Bool_t PresentX(void) {return fPresentX;};
  //! True Y position is present
  Bool_t PresentY(void) {return fPresentY;};

  Bool_t Treat(void);   //!< Treat the data
  void ReadCalibration(void);   //!< Specific reading of Calibration and thresholds

  //! Generate Default Calibration File
  void GenerateDefaultCalibration(Char_t *fName);

#ifdef WITH_ROOT
  //! Create 2D Histograms for Calculated Parameters
  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Create 1D Histograms for Calculated Parameters
  void CreateHistogramsCal1D(TDirectory *Dir);
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
  void OutAttach(TTree *OutTree);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParametersGET(GETParameters* PL, //!< Pointer to Parameter Names Table
                     Map* Map         //!< Pointer to Detectors Map
                                                        );
  Bool_t TreatX(void);   //!< Treat X position and return True if X within bounds [-1500,0]
  Bool_t TreatY(void);   //!< Treat Y position and return True if Y within bounds [0, ...]
  Bool_t TreatZ(void);   //!< Treat Z position and return True if Z within bounds [0, ...]

  UShort_t GetMaxStrip(UShort_t i){ if(i<3) return MaxStrip[i];};

  //! Get X focal Plane in mm
  inline Float_t GetX(void){return Cal[0];};
  //! Get Y
  inline Float_t GetY(void){return Cal[1];};
  //! Get Y
  inline Float_t GetZ(void){return Cal[2];};


protected:
  //! Allocate subcomponents
  void AllocateComponents(void);
  //! Look for Multiple Peaks
  Bool_t CheckMultiplePeaks(UShort_t *FStrip, //!< Array of Found Strips Number
			    Int_t atNumber
                            );

  
  //! Check that found strip are neigbours
  Bool_t CheckNeighbours(UShort_t *FStrip, //!< Array of Found Strips Number
			 Int_t atNumber
                         );
  //! Return position determined weigted average method
  Float_t WeightedAverage(UShort_t *FStrip, //!< Array of Found Strips Number
                          UShort_t NStrips,  //!< Number of Strips
			  Int_t atNumber
                          );
  //! Return position determined weigted average method
  Float_t SimpleWeightedAverage(UShort_t NStrips,  //!< Number of Strips
				Int_t atNumber);

  void ReadReference(MIFile *IF); //!< Read reference information from Calibration File
  Float_t SECHIP(UShort_t *FStrip, Int_t atNumber); //!< Return position determined by Squente Hyperbolique


  Bool_t fPresentX;
  Bool_t fPresentY;

  UShort_t NumberOfSubdetectorsX;
  UShort_t NumberOfSubdetectorsY;
  // Array of max Strip Number ordered
  UShort_t *MaxStrip;
  UShort_t NStrips; //! Number of strips required

#ifdef DC_CHARGE
  UShort_t Det_Charge_Offset;
#endif

  Float_t RefPos[3];
  Int_t BucketMin;
  Int_t BucketMax;

  ULong64_t TimeStampX;
  ULong64_t TimeStampY;
  

};
#endif
