/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    Diego Ramos, diego.ramos@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "FST_ID.hh"

// DetManager Handling
FST_ID::FST_ID(const Char_t *Name)
: BaseDetector(Name, 7, false, true, false,false,"")
{
  START;

  DM = DetManager::getInstance();

  // TACS of TOF
  FST_TACS = NULL;
  FST_TACS  = (BaseDetector*) DM->GetDetector("FST_TACS");
  // CHIO Energy
  FST_E = NULL;
  FST_E = (BaseDetector*) DM->GetDetector("FST_Energy");
  // POSITIONS
  FST_Start = NULL;
  FST_Stop = NULL;
  //FST_Start = (FST_Position*)DM->GetDetector("FST_Start");
  //FST_Stop = (FST_Position*)DM->GetDetector("FST_Stop");
  //TargetCHIO
  FST_TarIc = NULL;
  FST_TarIc = (BaseDetector*)DM->GetDetector("IC_TARGET");
  FST_TMW = NULL;
  FST_TMW = (TMW*)DM->GetDetector("TMW3");
  
  if(fCalData)
    {
      //sprintf(CalNameI[0],"FST_TOF");
      //sprintf(CalNameI[1],"FST_LOF");
      sprintf(CalNameI[0],"FST_dE");
      sprintf(CalNameI[1],"FST_Etot");
      sprintf(CalNameI[2],"FST_V");
      sprintf(CalNameI[3],"FST_Beta");
      sprintf(CalNameI[4],"FST_Gamma");
      //sprintf(CalNameI[7],"FST_Th");
      //sprintf(CalNameI[8],"FST_Ph");
      //sprintf(CalNameI[9],"FST_ThL");
      //sprintf(CalNameI[10],"FST_PhL");
      sprintf(CalNameI[5],"FST_M");
      //sprintf(CalNameI[12],"FST_dEnmStart");
      //sprintf(CalNameI[13],"FST_dEnmStop");
      //sprintf(CalNameI[14],"FST_dEnmChio");
      sprintf(CalNameI[6],"FST_dEnm");

    }
  
  ReadPosition();

  END;
}

FST_ID::~FST_ID(void)
{
  START;

  END;
}

void FST_ID::ReadPosition(void)
{ 
  START;
 if(fCalData)
	 {
	   MIFile *IF;
	   char Line[255];
	   stringstream *InOut;
	   UShort_t Len=255;
	   Char_t *Ptr=NULL;
	   
	   InOut = new stringstream();
	   *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
	   *InOut>>Line;
	   InOut = CleanDelete(InOut);
		
	   if((IF = CheckCalibration(Line)))
	     {
	       try
		 {
		   //Read FALSTAFF Position with respect TARGET
IF->GetLine(Line,Len);
		   while((Ptr = CheckComment(Line)))
		     {
		       IF->GetLine(Line,Len);
		       L->File << Line << endl;
		     }
		   stringstream *InOut = new stringstream();
		   *InOut << Line;
		   *InOut >> POS_REF[0];
		   if(InOut->fail())
		     {
		       InOut = CleanDelete(InOut);
		       Char_t Message[200];
		       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading XRef!",DetectorName, DetectorNameExt, IF->GetFileName() );
		       MErr * Er= new MErr(WhoamI,0,0, Message);
		       throw Er;
		     }
		   *InOut >> POS_REF[1];
		   if(InOut->fail())
		     {
		       InOut = CleanDelete(InOut);
		       Char_t Message[200];
		       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading YRef!",DetectorName, DetectorNameExt,IF->GetFileName() );
		       MErr * Er= new MErr(WhoamI,0,0, Message);
		       throw Er;
		     }
		   *InOut >> POS_REF[2];
		   if(InOut->fail())
		     {
		       InOut = CleanDelete(InOut);
		       Char_t Message[200];
		       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading ZRef!",DetectorName, DetectorNameExt,IF->GetFileName() );
		       MErr * Er= new MErr(WhoamI,0,0, Message);
		       throw Er;
		     }
		   
		   L->File << "<"<< DetectorName << "><" << DetectorNameExt<< "> : Reference X Y Z (mm) " << endl;
		   L->File <<  fixed << setprecision(3)<< POS_REF[0] << " "<< POS_REF[1] << " "<< POS_REF[2]  << endl;
		   
		   cout<< "<"<< DetectorName << "><" << DetectorNameExt<< "> : Reference X Y Z (mm) " << "("<<  fixed << setprecision(3)<< POS_REF[0] << " "<< POS_REF[1] << " "<< POS_REF[2] <<")" << endl;
		   

		   // Read FALSTAFF Angle
		   IF->GetLine(Line,Len);
		   while((Ptr = CheckComment(Line)))
		     {
		       IF->GetLine(Line,Len);
		       L->File << Line << endl;
		     }
		   stringstream *InOut2 = new stringstream();
		   InOut2->clear();
		   *InOut2 << Line;
		   *InOut2 >> FST_ANGLE;
		   if(InOut2->fail())
		     {
		       InOut2 = CleanDelete(InOut);
		       Char_t Message[200];
		       sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read FALSTAFF Angle value !", DetectorName, DetectorNameExt, IF->GetFileName());
		       MErr * Er= new MErr(WhoamI,0,0, Message);
		       throw Er;
		     }
		   InOut2 = CleanDelete(InOut2);
		   cout << "    <" << DetectorName << "><" << DetectorNameExt << "> Falstaff Angle set to " << FST_ANGLE << " deg." <<  endl;
		   L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> Falstaff Angle set to " << FST_ANGLE << " deg." <<  endl;
		  
		   
		   // Read TIME OFFSET
		   IF->GetLine(Line,Len);
		   while((Ptr = CheckComment(Line)))
		     {
		       IF->GetLine(Line,Len);
		       L->File << Line << endl;
		     };
		   stringstream *InOut3 = new stringstream();
		   InOut3->clear();
		   *InOut3 << Line;
		   *InOut3 >> FST_TOFFSET;
		   if(InOut3->fail())
		     {
		       InOut3 = CleanDelete(InOut3);
		       Char_t Message[200];
		       sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  : Could not read FALSTAFF Time OFFSET value !", DetectorName, DetectorNameExt, IF->GetFileName());
		       MErr * Er= new MErr(WhoamI,0,0, Message);
		       throw Er;
		     }
		   InOut3 = CleanDelete(InOut3);
		   cout << "    <" << DetectorName << "><" << DetectorNameExt << "> Falstaff TimeOffset set to " << FST_TOFFSET << " ns" <<  endl;
		   L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> Falstaff TimeOffset set to " << FST_TOFFSET << " ns" <<  endl;

		 }
	       catch(...)
		 {
		   IF = CleanDelete(IF);
		   throw;
		 }
	       IF = CleanDelete(IF);
	     }
	 }
 END;
}

Bool_t FST_ID::Treat(void)
{
  START;
  
  //if(FST_Start && FST_Stop && FST_TACS && FST_E)
  
  //{
      Calculate();
      //}
  return (isPresent);
  END;
}

void FST_ID::Init(void)
{
  START;
    
  FST_TOF = FST_LOF = FST_V = FST_Beta = FST_Gamma = FST_Th = FST_Ph = FST_ThL = FST_PhL = FST_Etot = -1500;
  FST_M = 0;
  FST_dEnmStart = FST_dEnmStop = FST_dEnmChio = FST_dEnm = 0.;
  FST_dE = 0.;
  END;
}

void  FST_ID::Get_dEnm(Float_t FEpost, Float_t FV, Float_t FThX, Float_t FThY)
{
  START;
 
  FST_dEnm = 0;
  //FST_dEnmStart = 0.;
  // FST_dEnmStop = FEpost*(0.54328-0.47235*FV+0.12858*pow(FV,2))*(1-0.959658*sin(FThY)+2.00386*pow(sin(FThY),2));
  //FST_dEnmChio = FEpost*(0.1712-0.12642*FV+0.02925*pow(FV,2));
  
  //FST_dEnm =  FST_dEnmStart+FST_dEnmStop+FST_dEnmChio;
  
  //only with PPAC
  if(FV>0&&FEpost>0)
    FST_dEnm = 5.70851+0.714188*(FEpost/FV)-0.0316099*pow(FEpost/FV,2)+0.00170626*pow(FEpost/FV,3)-4.53821e-05*pow(FEpost/FV,4)+6.20337e-07*pow(FEpost/FV,5)-4.3019e-09*pow(FEpost/FV,6)+1.20514e-11*pow(FEpost/FV,7);

  END;
}
/*
Bool_t FST_ID::Calculate(void)
{
  START;
  // Reset Local Variable
  Init();
  isPresent = false;

  if(FST_Start->GetX()>-1500 && FST_Stop->GetX()>-1500 &&
     FST_Start->GetY()>-1500 && FST_Stop->GetY()>-1500 &&
     FST_Start->GetZ()>-1500 && FST_Stop->GetZ()>-1500)
    {
      //LOF
      FST_LOF = sqrt(pow(FST_Stop->GetX()-FST_Start->GetX(),2)+pow(FST_Stop->GetY()-FST_Start->GetY(),2)+pow(FST_Stop->GetZ()-FST_Start->GetZ(),2)) /10.; //cm
      
      //ANGLES
      FST_Th = atan((FST_Stop->GetX()-FST_Start->GetX())/(FST_Stop->GetZ()-FST_Start->GetZ())) * 1000.; //mrad
      FST_Ph = atan((FST_Stop->GetY()-FST_Start->GetY())/(FST_Stop->GetZ()-FST_Start->GetZ())) * 1000.; //mrad
    }

  //Rotation with respect the Beam
#ifdef WITH_ROOT 
  if(FST_Th>-1500 && FST_Ph>-1500)
    {
      TVector3 *myVec;
      myVec = new TVector3(sin(FST_Th/1000.),sin(FST_Ph/1000.),cos(FST_Th/1000.)*cos(FST_Ph/1000.));
      //                          ANGLE FALSTAFF
      ///////
      myVec->RotateY(FST_ANGLE*TMath::Pi()/180.);
      ////
      //                          ANGLE FALSTAFF
      
      FST_ThL = myVec->Theta(); //rad
      FST_PhL = myVec->Phi();  //rad
      delete myVec;
    }
#endif



  // TOF
  if(FST_Start->GetX()>-1500 && FST_Start->GetX()>-1500)
    {
      if(FST_Start->GetX()>=0 && FST_Start->GetX()>=0)
	FST_TOF = FST_TACS->GetCal(0);
      else if(FST_Start->GetX()<0 && FST_Start->GetX()<0)
	FST_TOF = FST_TACS->GetCal(1);
      else if(FST_Start->GetX()>=0 && FST_Start->GetX()<0)
	FST_TOF = FST_TACS->GetCal(2);
      else
	FST_TOF = FST_TACS->GetCal(3);
    }

  //V
  if(FST_TOF>-1500 && FST_LOF>-1500)
    {
      FST_V = FST_LOF/FST_TOF; //cm/ns
      FST_Beta = FST_V/29.9792458;
      if(FST_Beta<1)
	FST_Gamma = 1./sqrt(1.-pow(FST_Beta,2));
    }

  //dE not measured and Etot
  if(FST_E->GetCal(0)>-1500 && FST_Beta>-1500 && FST_Beta<1 && FST_Th>-1500 && FST_Ph>-1500)
    {
      Get_dEnm(FST_E->GetCal(0),FST_V, FST_Th/1000., FST_Ph/1000.);
      FST_Etot = FST_E->GetCal(0) + FST_dEnm;
    }
  
  // Mass
  if(FST_Etot>-1500 && FST_dEnm>-1500 && FST_Beta>-1500 && FST_Beta<1)
    {
      FST_M  = FST_Etot/931.5016/(FST_Gamma-1.);
    }
  
  
  SetCalData(0,FST_TOF);
  SetCalData(1,FST_LOF);  
  SetCalData(2,FST_dEnm);
  SetCalData(3,FST_Etot);
  SetCalData(4,FST_V);
  SetCalData(5,FST_Beta);
  SetCalData(6,FST_Gamma);
  SetCalData(7,FST_Th);
  SetCalData(8,FST_Ph);
  SetCalData(9,FST_ThL);
  SetCalData(10,FST_PhL);
  SetCalData(11,FST_M);
  SetCalData(12,FST_dEnmStart);
  SetCalData(13,FST_dEnmStop);
  SetCalData(14,FST_dEnmChio);

  if(FST_LOF > -1500) 
    {
      isPresent=true;
    }
  
  return (isPresent);

  END;
}
*/


Bool_t FST_ID::Calculate(void)
{
  START;
  // Reset Local Variable
  Init();
  isPresent = false;
 
  if(FST_TACS->GetCal(0)>0&&FST_TMW)
    {
      //LOF and TOF
      FST_LOF =0.1*(POS_REF[2]-FST_TMW->GetRefZ_XPlane()); //cm
      FST_TOF = FST_TOFFSET - 1*FST_TACS->GetCal(0); //ns

      //V
      FST_V = FST_LOF/FST_TOF; //cm/ns
      FST_Beta = FST_V/29.9792458;
      if(FST_Beta<1)
	FST_Gamma = 1./sqrt(1.-pow(FST_Beta,2));
    }
  
  //dE target Chio
  if( FST_TarIc->GetCal(0)>0)
    FST_dE =  FST_TarIc->GetCal(0);
    

  //dE not measured and Etot
  if(FST_E->GetCal(0)>-1500 && FST_Beta>0 && FST_Beta<1)
    {
      Get_dEnm(FST_E->GetCal(0),FST_V,0,0);
      FST_Etot = FST_E->GetCal(0) + FST_dEnm;
    }
  
  // Mass
  if(FST_Etot>-1500 && FST_dEnm>-1500 && FST_Beta>0 && FST_Beta<1)
    {
      FST_M  = FST_Etot/931.5016/(FST_Gamma-1.);
    }
  
  
  SetCalData(0,FST_dE);
  SetCalData(1,FST_Etot);
  SetCalData(2,FST_V);
  SetCalData(3,FST_Beta);
  SetCalData(4,FST_Gamma);
  SetCalData(5,FST_M);
  SetCalData(6,FST_dEnm);

  if(FST_LOF > -1500) 
    {
      isPresent=true;
    }
  
  return (isPresent);

  END;
}


#ifdef WITH_ROOT
void FST_ID::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
  // Set histogram Hierarchy
  SetHistogramsCal1DFolder("FST_Id1D");
  SetHistogramsCal2DFolder("FST_Id2D");
 
  if(fMode == MODE_WATCHER)
    {
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
    }
  else if(fMode == MODE_D2R)
	 {
      ;
	 }
  else if(fMode == MODE_D2A)
	 {
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
		  }
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
		  }      
		SetOutAttachCalI(true);
		OutAttach(OutTTree);		
    }
  else if(fMode == MODE_RECAL)
    {
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	}      
      SetOutAttachCalI(true);
      OutAttach(OutTTree);		
    }
  else if(fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
		Char_t Message[200];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}


void FST_ID::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
  
  //AddHistoCal(GetCalName(0),"FST_TOF","FST_TOF (ns)",1000,0.,500.);
  //AddHistoCal(GetCalName(1),"FST_LOF","FST_LOF (cm)",1000,0.,500.);
  AddHistoCal(GetCalName(2),"FST_V","FST_V (cm/ns)",1000,0.,5.);
  AddHistoCal(GetCalName(5),"FST_M","FST_M ",1000,0,500.);
  
  END;
}

void FST_ID::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }

  //AddHistoCal(GetCalName(1),GetCalName(0),"FST_TOF_vs_LOF","FST_TOF_vs_LOF",2000,0,500,1000,0,500);
  AddHistoCal(GetCalName(2),GetCalName(0),"FST_V_vs_dE","FST_V_vs_dE",1000,0,5,1000,0,50000);
 
  END;
}
#endif
