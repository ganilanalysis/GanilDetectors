   /****************************************************************************
    *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
    *    lemasson@ganil.fr
    *
    *    Contributor(s) :
    *    Antoine Lemasson, lemasson@ganil.fr
    *    Maurycy Rejmund, rejmund@ganil.fr
    *    Diego Ramos, diego.ramos@ganil.fr
    *
    *    This software is  a computer program whose purpose  is to provide data
    *    manipulation tools to Analyse experimental data collected at the VAMOS
    *    spectrometer.
    *
    *    This software is governed by the CeCILL-B license under French law and
    *    abiding by the  rules of distribution of free  software.  You can use,
    *    modify  and/ or  redistribute  the  software under  the  terms of  the
    *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    *    URL \"http://www.cecill.info\".
    *
    *    As a counterpart to the access  to the source code and rights to copy,
    *    modify  and redistribute granted  by the  license, users  are provided
    *    only with a limited warranty  and the software's author, the holder of
    *    the economic  rights, and the  successive licensors have  only limited
    *    liability.
    *
    *    In this respect, the user's attention is drawn to the risks associated
    *    with loading,  using, modifying  and/or developing or  reproducing the
    *    software by the user in light of its specific status of free software,
    *    that  may mean that  it is  complicated to  manipulate, and  that also
    *    therefore  means that it  is reserved  for developers  and experienced
    *    professionals having in-depth  computer knowledge. Users are therefore
    *    encouraged  to load  and test  the software's  suitability  as regards
    *    their  requirements  in  conditions  enabling the  security  of  their
    *    systems  and/or data to  be ensured  and, more  generally, to  use and
    *    operate it in the same conditions as regards security.
    *
    *    The fact that  you are presently reading this means  that you have had
    *    knowledge of the CeCILL-B license and that you accept its terms.
    ***************************************************************************/
   #include "FST_Position.hh"
   //#define DCE10 1
   FST_Position::FST_Position(const Char_t *Name,
       UShort_t NXDetectors,
       UShort_t NYDetectors,
       Bool_t RawData,
       Bool_t CalData,
       Bool_t DefaultCalibration,
       const Char_t *NameExt)
     : BaseDetector(Name,7, false, CalData, DefaultCalibration, false, NameExt, true, true)
   {
     START;

     NumberOfSubdetectorsX = NXDetectors;
     NumberOfSubdetectorsY = NYDetectors;
     fRawDataSubDetectors  = RawData;

     // NStrips required
     //NStrips = 3 ;
     NStrips = 1 ;

     AllocateComponents();

if(fCalData)
     {
       for(int i=0;i<7;i++)
	 SetGateCal(-2000,4095,i);
       sprintf(CalNameI[0],"%s_X", DetectorName);
       sprintf(CalNameI[1],"%s_Y", DetectorName);
       sprintf(CalNameI[2],"%s_Z", DetectorName);
       sprintf(CalNameI[3],"%s_QMaxX", DetectorName);
       sprintf(CalNameI[4],"%s_QMaxY", DetectorName);
       sprintf(CalNameI[5],"%s_QMaxXN", DetectorName);
       sprintf(CalNameI[6],"%s_QMaxYN", DetectorName);
       
     }
 ReadCalibration();

     END;
   }
   FST_Position::~FST_Position(void)
   {
     START;

     MaxStrip = FreeDynamicArray<UShort_t>(MaxStrip);

     END;
   }

   void FST_Position::Clear(void)
   {
     START;
     if(DetList->at(0)->GetM())
       for(UShort_t i=0; i<3 ;i++)
	 MaxStrip[i] = 0;
     
     BaseDetector::Clear();
     fPresentX = false;
     fPresentY = false; 
     
     TimeStampX = 0;
     TimeStampY = 0;
     
     END;
   }

   void FST_Position::AllocateComponents(void)
   {
     START;

     MaxStrip = AllocateDynamicArray<UShort_t>(3);
     for(UShort_t i=0; i<3; i++)
       MaxStrip[i] = 0;

     Bool_t RawData = fRawDataSubDetectors;
     Bool_t CalData = fCalData;
     Bool_t DefCal = false;
     Bool_t PosInfo = false;

     string BaseName;
     string Name;
     BaseName = DetectorName ;
     Name = BaseName + "_QX";
     //BaseDetector *QX = new BaseDetector(Name.c_str(),NumberOfSubdetectorsX,RawData,CalData,DefCal,PosInfo,"",false,true);
     BaseDetector *QX = new BaseDetector(Name.c_str(),NumberOfSubdetectorsX,RawData,CalData,DefCal,PosInfo,"",false,true,NumberOfSubdetectorsX,true);
     QX->SetRawHistogramsParams(4096,0,4095,"");
     QX->SetCalHistogramsParams(4096,0,4095,"","");

     AddComponent(QX);

     Name.clear();
     Name = BaseName + "_QY";
     //BaseDetector *QY = new BaseDetector(Name.c_str(),NumberOfSubdetectorsY,RawData,CalData,DefCal,PosInfo,"",false,true);
     BaseDetector *QY = new BaseDetector(Name.c_str(),NumberOfSubdetectorsY,RawData,CalData,DefCal,PosInfo,"",false,true,NumberOfSubdetectorsY,true);
     QY->SetRawHistogramsParams(4096,0,4095,"");
     QY->SetCalHistogramsParams(4096,0,4095,"","");

     AddComponent(QY);


     END;
   }

   void FST_Position::SetParametersGET(GETParameters* PL,Map* Map)
   {
     START;
     
     if(isComposite)
     {

       Char_t PName[200];
       for(int i=0; i < DetList->at(0)->GetNumberOfDetectors(); i++)
         {
	   sprintf(PName, "%s_X_%02d",DetectorName, i);
           DetList->at(0)->SetParameterName(PName, i);
           DetList->at(0)->SetRawHistogramsParams(4096,0,4095);
           DetList->at(0)->SetGateRaw(0,4095);
         }
       for(int i=0; i < DetList->at(1)->GetNumberOfDetectors(); i++)
         {
	   sprintf(PName, "%s_Y_%02d",DetectorName, i);
           DetList->at(1)->SetParameterName(PName, i);
           DetList->at(1)->SetRawHistogramsParams(4096,0,4095);
           DetList->at(1)->SetGateRaw(0,4095);
         }
       
       GETParameters * PL_GET = GETParameters::getInstance();
       for (UShort_t i = 0; i < DetList->size(); i++)
         DetList->at(i)->SetParametersGET(PL_GET, Map);

     }
     END;
   }

   Bool_t FST_Position::Treat(void)
   {
     START;
    
 
     if(fCalData)
     {
       // ForCalibs
       if(CalibrationMode)
       {
         // Only Calibrate PADs
         DetList->at(0)->Treat();
	 DetList->at(1)->Treat();
         // return 0;
       }
       else
	 {
	   if(DetList->at(0)->Treat() && DetList->at(1)->Treat())
	     {
	       if(DetList->at(0)->GetM() > 0)
		 {
		   TimeStampX = DetList->at(0)->GetRawTSAt(0);
		   if(!TreatX())
		     {
		       SetCalData(0,-1500.);
		     }
		 }
	       else
		 {
		   SetCalData(0,-1500.);
		   SetCalData(3,-1500.);
		   SetCalData(5,-1500.);
		   TimeStampX = 0;
		 }
	       if(DetList->at(1)->GetM() > 0)
		 {
		   TimeStampY = DetList->at(1)->GetRawTSAt(0);
		   if(!TreatY())
		     {
		       SetCalData(1,-1500.);
		     }
		 }
	       else
		 {
		   SetCalData(1,-1500.);
		   SetCalData(4,-1500.);
		   SetCalData(6,-1500.);
		   TimeStampY = 0;
		 }
	       if(fPresentY)
		 {
		   if(!TreatZ())
		     {
		       SetCalData(2, -1500.);
		     }
		 }
	       else
		 {
		   SetCalData(2,-1500.);
		 }
	       
	       isPresent = fPresentX && fPresentY;
	     }
	   else
	     {
	       SetCalData(0,-1500.);
	       SetCalData(1,-1500.);
	       SetCalData(2,-1500.);
	       SetCalData(3,-1500.);
	       SetCalData(4,-1500.);
	       SetCalData(5,-1500.);
	       SetCalData(6,-1500.);
	       TimeStampX =  TimeStampY = 0;
	     }
	 }
     }
     
     return isPresent;
     END;
   }



void FST_Position::ReadCalibration(void)
{
  START;
     
  MIFile *IF;
  char Line[255];
  stringstream *InOut;

  if(fCalData)
    {
      
      InOut = new stringstream();
      *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << ".cal";
      *InOut>>Line;
      InOut = CleanDelete(InOut);
      
      if((IF = CheckCalibration(Line)))
	{
	  // Position
	  try
	    {
	      ReadReference(IF);
	    }
	  catch(...)
	    {
	      IF = CleanDelete(IF);
	      throw;
	    }
	  
	  // StripsX
	  try
	    {
	      DetList->at(0)->ReadCalibration(IF);

	    }
	  catch(...)
	    {
	      IF = CleanDelete(IF);
	      throw;

	    }
	  // StripsY
	  try
	    {
	      DetList->at(1)->ReadCalibration(IF);
	    }
	  catch(...)
	    {
	      IF = CleanDelete(IF);
	      throw;
	    }
	  IF = CleanDelete(IF);
	}
      else
	{
	  GenerateDefaultCalibration(Line);
	}

    }
    END;
}

void FST_Position::GenerateDefaultCalibration(Char_t *fName)
{
  START;
     
  cout << "==============================================================" << endl;
  cout << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
    "File " << fName << " not found" << endl <<
    "Generate a new file with Default Calibrations!" << endl;
  cout << "==============================================================" << endl;
  L->File << "==============================================================" << endl;
  L->File  << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
    "File " << fName << " not found" << endl <<
    "Generate a new file with Default Calibrations!" << endl;
  L->File  << "==============================================================" << endl;

  MOFile *OF = NULL;
  try
    {
      OF = new MOFile(fName);
    }
  catch(MErr *Er)
    {
      cout << "==============================================================" << endl;
      cout << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
	"File " << fName << " Could not be created" << endl
	   << "Using default calibration instead!" << endl;;
      cout << "==============================================================" << endl;
      L->File  << "==============================================================" << endl;
      L->File  << "<" << DetectorName << "><" << DetectorNameExt << "> :" << endl <<
	"File " << fName << " Could not be created" << endl
	       << "Using default calibration instead!" << endl;;
      L->File  << "==============================================================" << endl;
      OF = CleanDelete(OF);

    }
  const time_t now = time(0);
  char* dt = ctime(&now);

  OF->File << "// Title   : Default calibration file for <" << DetectorName << "><" << DetectorNameExt << "> :" << endl;
  OF->File << "// Date    : " << dt ;
  OF->File << "// Comment : " << endl;

  // Reference Position
  RefPos[0] = RefPos[1] = RefPos[2] = 0;
  OF->File << "// Reference Position X Y Z " << endl;
  OF->File <<  RefPos[0] << " " <<  RefPos[1] << " " <<  RefPos[2] << endl;

 // Bucket Limits
  BucketMin = BucketMax = 0;
  OF->File << "// Bucket Range: Min Max" << endl;
  OF->File <<  BucketMin << " " <<  BucketMax << endl;

  Float_t QThresh = 0.01;
  //QXThresh
  DetList->at(0)->SetGateCal(QThresh,4095.);
  OF->File << "// QXThresh" << endl;
  OF->File <<  QThresh << endl;

  //QYThresh
  DetList->at(1)->SetGateCal(QThresh,4095.);
  OF->File << "// QYThresh" << endl;
  OF->File <<  QThresh << endl;

  OF->File << "//Charge Strip calib " << endl;
  OF->File << "// Format  : a0 \t a1 \t a2 \t // ParameterName" << endl;
     
  //X position
  for(UShort_t i =0; i< DetList->at(0)->GetNumberOfDetectors();i++)
    {
      for(UShort_t j=0;j<3;j++)
	{
	  cout << DetList->at(0)->GetCalCoeffs()[i][j] << " " ;
	  if(OF) OF->File <<  DetList->at(0)->GetCalCoeffs()[i][j] << " ";
	}
      cout << "// " <<  DetList->at(0)->GetRawName(i)<< endl;
      if(OF) OF->File  << "// " << DetList->at(0)->GetRawName(i)<< endl;
    }

  //Y position
  for(UShort_t i =0; i< DetList->at(1)->GetNumberOfDetectors();i++)
    {
      for(UShort_t j=0;j<3;j++)
	{
	  cout << DetList->at(1)->GetCalCoeffs()[i][j] << " " ;
	  if(OF) OF->File <<  DetList->at(1)->GetCalCoeffs()[i][j] << " ";
	}
      cout << "// " <<  DetList->at(1)->GetRawName(i)<< endl;
      if(OF) OF->File  << "// " << DetList->at(1)->GetRawName(i)<< endl;
    }
  OF = CleanDelete(OF);
     
  END;
}


Bool_t FST_Position::CheckMultiplePeaks(UShort_t *FStrip, Int_t atNumber)
   {
     START;




     for(UShort_t j=0;j<DetList->at(atNumber)->GetNrAt(FStrip[0])-1;j++)
       if((DetList->at(atNumber)->GetCal(j) > DetList->at(atNumber)->GetCal(j+1)))
         if(DetList->at(atNumber)->GetCal(j) >= 0.4*DetList->at(atNumber)->GetCal(DetList->at(atNumber)->GetNrAt(FStrip[0])))
         {
           return true;
         }

     for(UShort_t j=DetList->at(atNumber)->GetNrAt(FStrip[0]);j<DetList->at(atNumber)->GetNumberOfDetectors();j++)
       if(DetList->at(atNumber)->GetCal(j) < DetList->at(atNumber)->GetCal(j+1))
         if(DetList->at(atNumber)->GetCal(j+1) >= 0.4*DetList->at(atNumber)->GetCal(DetList->at(atNumber)->GetNrAt(FStrip[0])))
         {
           return true;
         }
     return false;
     END;
   }

Bool_t FST_Position::CheckNeighbours(UShort_t *FStrip, Int_t atNumber)
   {
     START;
     if( abs(DetList->at(atNumber)->GetNrAt(FStrip[0])-DetList->at(atNumber)->GetNrAt(FStrip[1])) == 1
         &&
         abs(DetList->at(atNumber)->GetNrAt(FStrip[0])-DetList->at(atNumber)->GetNrAt(FStrip[2])) == 1 )
       return true;
     else
     {
//       cout << "Check Neighbours " << endl;
//        cout<<DetList->at(atNumber)->GetNrAt(FStrip[0])<<" "<<DetList->at(atNumber)->GetNrAt(FStrip[1])<<" "<<DetList->at(atNumber)->GetNrAt(FStrip[2])<<endl;
//         DetList->at(atNumber)->PrintCal();
       return false;

       // if( abs(N[FStrip[0]][i]-N[FStrip[1]][i]) <=2
       // 	  &&
       // 	  abs(N[FStrip[0]][i]-N[FStrip[2]][i]) <= 2)
       // 	if(!Neighbours&&!MultiplePeak)
       // 	  {
       // 		 // if(i==2 && N[FStrip[0]][i]>=96&&N[FStrip[0]][i]<=98)
       // 		 //   {
       // 		 //   cout << i << " " << N[FStrip[0]][i] << " " << N[FStrip[1]][i] << " "<< N[FStrip[2]][i] << endl;
       // 		 //   Show_Raw();
       // 		 //   Show();
       // 		 //   }
       // 		 // H1D[12+i]->Fill( N[FStrip[0]][i]);
       // 		 // H1D[12+i]->Fill( N[FStrip[1]][i]);
       // 		 // H1D[12+i]->Fill( N[FStrip[2]][i]);
       // 	  }
     }
     END;
   }


Float_t FST_Position::SECHIP(UShort_t *FStrip, Int_t atNumber)
   {
     START;
     Float_t v[6];
     Float_t XS=-1500;

     v[0] = sqrt(DetList->at(atNumber)->GetCalAt(FStrip[0])/DetList->at(atNumber)->GetCalAt(FStrip[2]));
     v[1] = sqrt(DetList->at(atNumber)->GetCalAt(FStrip[0])/DetList->at(atNumber)->GetCalAt(FStrip[1]));
     v[2] = 0.5*(v[0]+v[1]);
     v[3] = log(v[2]+sqrt(pow(v[2],2.f)-1.0));
     v[4] = (v[0] - v[1])/(2.0*sinh(v[3]));
     v[5] = 0.5*log((1.0+v[4])/(1.0-v[4]));

     XS = (Float_t) DetList->at(atNumber)->GetNrAt(FStrip[0])
       - (Float_t) (DetList->at(atNumber)->GetNrAt(FStrip[0])-DetList->at(atNumber)->GetNrAt(FStrip[1]))*v[5]/v[3];


     //XS *= -6.4; //Goes mm 6.02 pad 0.38 space

     if(XS < 0. && XS>-1500.)
     {
       fPresentX = true;
       // XS -= GetRefX();

     }
     else
       XS = -1500.;

     return XS;
     END;
   }

Float_t FST_Position::WeightedAverage(UShort_t *FStrip, UShort_t NStrips, Int_t atNumber)
   {
     START;
     Float_t v[2];
     UShort_t StripsWA=0;
     Float_t XWA=-1500.;

     //cout<<"***checkSorting***"<<endl;
     //for(int i=0;i<DetList->at(atNumber)->GetM();i++)
     //cout<<" MultPos: "<<i<<" ChPos: "<<DetList->at(atNumber)->GetNrAt(i)<<" BucketPos: "<<DetList->at(atNumber)->GetRawTimeAt(i)<<endl;

     if(DetList->at(atNumber)->GetM() >= NStrips)
       {
	 //Looking for entire peak for W.A.
	 //The Strips are ordered  0-160
	 //Could be done earlier but ....
	 StripsWA=0;
	 for(Int_t j=FStrip[0]-1;j>=0;j--)
	   {
	     //if(abs(DetList->at(atNumber)->GetNrAt(j+1)-DetList->at(atNumber)->GetNrAt(j)) == 1)
	     //{
	     //if((DetList->at(atNumber)->GetCalAt(j) <= DetList->at(atNumber)->GetCalAt(j+1)))  
	     if(DetList->at(atNumber)->GetRawTimeAt(j) >= BucketMin && DetList->at(atNumber)->GetRawTimeAt(j) <= BucketMax)
	       if(DetList->at(atNumber)->GetCalAt(j)>0)
		 {
		   StripsWA++;
		   FStrip[StripsWA]=j;
		 }
	     //else
	     //break;
	     //}
	     //else
	     //{
	     //break;
	     //}
	   }
	 for(Int_t j=FStrip[0];j<DetList->at(atNumber)->GetM()-1;j++)
	   {
	     //if(abs(DetList->at(atNumber)->GetNrAt(j+1)-DetList->at(atNumber)->GetNrAt(j)) == 1)
	     //{
	     //if(DetList->at(atNumber)->GetCalAt(j) >= DetList->at(atNumber)->GetCalAt(j+1))    
	     if(DetList->at(atNumber)->GetRawTimeAt(j) >= BucketMin && DetList->at(atNumber)->GetRawTimeAt(j) <= BucketMax)
	       if(DetList->at(atNumber)->GetCalAt(j+1)>0)
		 {
		   StripsWA++;
		   FStrip[StripsWA]=j+1;
		 }
	     //else
	     //break;
	     //}
	     //else
	     //break;
	   }
       
	 if(StripsWA >= NStrips)
	   {
	     v[0] = v[1] = 0.0;
	     for(UShort_t k=0;k<=StripsWA;k++)
	       {
		 v[0] += DetList->at(atNumber)->GetCalAt(FStrip[k]) * ((Float_t) DetList->at(atNumber)->GetNrAt(FStrip[k]));
		 v[1] += DetList->at(atNumber)->GetCalAt(FStrip[k]);
	       }
	     
#ifdef DEBUG
	     cout << "FST_Positionv::FocalSubseqX::Weithed Average: "  << endl;
	     cout << "StripsWA: " << StripsWA << endl;
	     for(UShort_t k=0;k<=StripsWA;k++)
	       cout << "STR: " << DetList->at(atNumber)->GetNrAt(FStrip[k]) << " Q: " << DetList->at(atNumber)->GetCalAt(FStrip[k]) << endl;
#endif
	     XWA = v[0] / v[1];
	     
	     //XWA *= -6.4; //Goes mm 6.02 pad 0.38 space
	     XWA *= 2.76; //Goes mm 2.75 pad
	     if(XWA>-1500.)
	       {
		 XWA -= RefPos[atNumber];
	       }
	     else
	       XWA = -1500.;
	   }
       }
     return XWA;
     END;
   }

Float_t FST_Position::SimpleWeightedAverage(UShort_t NStrips,Int_t atNumber)
{
  START;
  Float_t v[2];
  Float_t XWA=-1500.;
  
  if(DetList->at(atNumber)->GetM() >= NStrips)
    {		 
      v[0] = v[1] = 0.0;
      for(UShort_t k=0;k<DetList->at(atNumber)->GetM();k++) 
	if(DetList->at(atNumber)->GetRawTimeAt(k) >= BucketMin && DetList->at(atNumber)->GetRawTimeAt(k) <= BucketMax)
	  if(DetList->at(atNumber)->GetCalAt(k)>0)
	    {
	      v[0] += DetList->at(atNumber)->GetCalAt(k) * ((Float_t) DetList->at(atNumber)->GetNrAt(k));
	      v[1] += DetList->at(atNumber)->GetCalAt(k);
	    }
      XWA = v[0] / v[1];    
      XWA *= 2.76; //Goes mm 2.75 pad
      if(XWA>-1500.)
	{
	  XWA -= RefPos[atNumber];
	}
      else
	XWA = -1500.;
    }
     
  return XWA;
  END;
}

Bool_t FST_Position::TreatX(void)
{
  START;
  
  // Nmmber of Required Strips
  Float_t QTmp[NumberOfSubdetectorsX];
  Float_t QMax;
  UShort_t NMax;
  UShort_t FStrip[NumberOfSubdetectorsX];
  for(UShort_t j=0; j<NumberOfSubdetectorsX ;j++)
    {
      QTmp[j]=0.;
      FStrip[j]=0.;
    }
  Bool_t Neighbours;
#ifdef MULTIPEAK_FST
  Bool_t MultiplePeak;
#endif
  Float_t X=-1500.;
  // Loop over strips
  if(DetList->at(0)->GetM() < NStrips)
    return false;

  for(UShort_t j=0; j< DetList->at(0)->GetM();j++) //loop over strips
    if(DetList->at(0)->GetRawTimeAt(j) >= BucketMin && DetList->at(0)->GetRawTimeAt(j) <= BucketMax)
      {
	QTmp[j] = DetList->at(0)->GetCalAt(j);// Copy Charges
      }
  
  // Find Nstrip highest charges
  for(UShort_t k=0;k<NStrips; k++)
    {
      QMax=0.0;
      NMax=0;
      for(UShort_t j=0;j<DetList->at(0)->GetM();j++)
	{
	  //cout<<DetList->at(0)->GetRawTSAt(j)<<endl;
	  //cout<<DetList->at(0)->GetRawTimeAt(j)<<endl;
	  if(QTmp[j] > QMax)
	    {
	      QMax = QTmp[j];
	      NMax = j;
	    }
	}
      QTmp[NMax] = 0.0;
      FStrip[k] = NMax;
      MaxStrip[k] = DetList->at(0)->GetNrAt(NMax);
    }
  
  SetCalData(3,DetList->at(0)->GetCalAt(FStrip[0]));
  SetCalData(5,MaxStrip[0]);
  
  /*
    

   #ifdef MULTIPEAK_FST
     MultiplePeak = false;
     if((MultiplePeak = CheckMultiplePeaks(FStrip,0)))
       {
        #ifdef DEBUG
         cout << "Multiplepeak: " << DetectorName <<  endl;
         DetList->at(0)->PrintCal();
        #endif
       }
   #endif
     if(NStrips != 3)
       {
         MErr * Er = new MErr(WhoamI,0,0, "NStrips != 3 but");
         throw Er;
       }

     Neighbours = false;
 
   #ifdef MULTIPEAK_FST
     if(!MultiplePeak)
     {
   #endif
       if((Neighbours = CheckNeighbours(FStrip,0)))
       {
         X = SECHIP(FStrip,0);
       }
       else
       {
       #ifdef WEIGHTEDAVERAGE_FST
         X = WeightedAverage(FStrip,NStrips,0);
   #endif
           }


   #ifdef MULTIPEAK_FST
     }
   #endif
     */
     
#ifdef WEIGHTEDAVERAGE_FST
  //X = WeightedAverage(FStrip,NStrips,0);
  X = -1.*SimpleWeightedAverage(NStrips,0);
#endif
  
  if(X>-1500)
    {
      fPresentX=true;
      SetCalData(0,X);
    }

  //SetCalData(0,X);
  return fPresentX;
  
  END;
}
 
 Bool_t FST_Position::TreatY(void)
 {
  START;
  // Nmmber of Required Strips
  Float_t QTmp[NumberOfSubdetectorsY];
  Float_t QMax;
  UShort_t NMax;
  UShort_t FStrip[NumberOfSubdetectorsY];
  for(UShort_t j=0; j<NumberOfSubdetectorsY ;j++)
    {
      QTmp[j]=0.;
      FStrip[j]=0.;
    }
  Bool_t Neighbours;
#ifdef MULTIPEAK_FST
  Bool_t MultiplePeak;
#endif
  
  Float_t X=-1500.;
  // Loop over strips
  if(DetList->at(1)->GetM() < NStrips)
    return false;
  
  for(UShort_t j=0; j< DetList->at(1)->GetM();j++) //loop over strips
    if(DetList->at(1)->GetRawTimeAt(j) >= BucketMin && DetList->at(1)->GetRawTimeAt(j) <= BucketMax)
      {
	QTmp[j] = DetList->at(1)->GetCalAt(j);// Copy Charges
      }
  
  // Find Nstrip highest charges
  for(UShort_t k=0;k<NStrips; k++)
    {
      QMax=0.0;
      NMax=0;
      for(UShort_t j=0;j<DetList->at(1)->GetM();j++)
	{
	  if(QTmp[j] > QMax)
	    {
	      QMax = QTmp[j];
	      NMax = j;
	    }
	}
      QTmp[NMax] = 0.0;
      FStrip[k] = NMax;
      MaxStrip[k] = DetList->at(1)->GetNrAt(NMax);
    }
  SetCalData(4,DetList->at(1)->GetCalAt(FStrip[0]));
  SetCalData(6,MaxStrip[0]);

     /*


   #ifdef MULTIPEAK_FST
     MultiplePeak = false;
     if((MultiplePeak = CheckMultiplePeaks(FStrip,0)))
       {
        #ifdef DEBUG
         cout << "Multiplepeak: " << DetectorName <<  endl;
         DetList->at(0)->PrintCal();
        #endif
       }
   #endif
     if(NStrips != 3)
       {
         MErr * Er = new MErr(WhoamI,0,0, "NStrips != 3 but");
         throw Er;
       }

     Neighbours = false;
 
   #ifdef MULTIPEAK_FST
     if(!MultiplePeak)
     {
   #endif
       if((Neighbours = CheckNeighbours(FStrip,0)))
       {
         X = SECHIP(FStrip,0);
       }
       else
       {
   #ifdef WEIGHTEDAVERAGE_FST
         X = WeightedAverage(FStrip,NStrips,0);
   #endif
           }


   #ifdef MULTIPEAK_FST
     }
   #endif
     */
#ifdef WEIGHTEDAVERAGE_FST
  //X = WeightedAverage(FStrip,NStrips,1);  
  X = SimpleWeightedAverage(NStrips,1);
#endif

  if(X>-1500)
    {
      fPresentY=true;
      // Y axis is tilted 45 deg with respect the perpendicular
      X = X*sqrt(2.)/2;
      SetCalData(1,X);
    } 
  //SetCalData(1,X);
  return fPresentY;
  
  END;
 }


 Bool_t FST_Position::TreatZ(void)
 {
  START;
  
  Float_t X=-1500.;
  Bool_t fPresentZ = false;
  
  if(GetY()>-1500)
    {
      fPresentZ=true;
      // Z postion depends on the Y position, Y axis is tilted 45 deg with respect the perpendicular
      X = RefPos[2]-GetY(); 
      SetCalData(2,X);
    }
  
  return fPresentZ;
  
  END;
 }

   void FST_Position::ReadReference(MIFile *IF)
   {
     START;
     char Line[255];
     UShort_t Len=255;
     Char_t *Ptr=NULL;
     Float_t QThresh=0;

     L->File << "<" << DetectorName << "><" << DetectorNameExt
       <<"> Reading Reference from file " << IF->GetFileName() << endl;
     do
     {
       IF->GetLine(Line,Len);
     }  while((Ptr = CheckComment(Line)))  ;
     // X Y Z Positions
     stringstream *InOut = NULL;
     InOut= new stringstream();
     *InOut << Line;
     *InOut >> RefPos[0];
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[200];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading XRef!",
           DetList->at(0)->GetName(),
           DetList->at(1)->GetName(),
           IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }
     *InOut >> RefPos[1];
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[200];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading YRef!",
           DetList->at(0)->GetName(),
           DetList->at(1)->GetName(),
           IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }
     *InOut >> RefPos[2];
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[200];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading ZRef!", 
	       DetList->at(0)->GetName(), 
	       DetList->at(1)->GetName(),
	       IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }
     
     L->File << "<"<< DetList->at(0)->GetName() << "><"
	     << DetList->at(0)->GetNameExt()
	     << "> : Reference X Y Z (mm) " << endl;
     L->File <<  fixed << setprecision(3)
	     << RefPos[0] << " "
	     << RefPos[1] << " "
	     << RefPos[2]  << endl;

     // BUCKETs LIMITS---------------
     IF->GetLine(Line,Len);
     if(!(Ptr = CheckComment(Line)))
       {
	 Char_t Message[200];
	 sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading BUCKET LIMITS",
		 DetList->at(0)->GetName(),
		 DetList->at(9)->GetNameExt(),
	       IF->GetFileName());
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }
     L->File << "<"<< DetList->at(0)->GetName()
       << "><" <<DetList->at(0)->GetNameExt()
       << endl;

     IF->GetLine(Line,Len);
     InOut->clear();
     *InOut << Line;
     *InOut >> BucketMin;
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[200];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading BucketMin!",
           DetList->at(0)->GetName(),
           DetList->at(0)->GetName(),
           IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     } 
     *InOut >> BucketMax;
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[200];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading BucketMax!", 
	       DetList->at(0)->GetName(), 
	       DetList->at(0)->GetName(),
	       IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }

     L->File <<  BucketMin  << " "
	     <<  BucketMax  << endl;

     // QX Threholds ---------------
     IF->GetLine(Line,Len);
     if(!(Ptr = CheckComment(Line)))
     {
       Char_t Message[200];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading Q Thresholds!",
           DetList->at(0)->GetName(),
           DetList->at(9)->GetNameExt(),
           IF->GetFileName());
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }
     L->File << "<"<< DetList->at(0)->GetName()
       << "><" <<DetList->at(0)->GetNameExt()
       << endl;

     IF->GetLine(Line,Len);
     InOut->clear();
     *InOut << Line;
     *InOut >> QThresh;
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[200];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading QX Thresholds!",
           DetList->at(0)->GetName(),
           DetList->at(0)->GetName(),
           IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }

     L->File <<  fixed << setprecision(3) <<  QThresh  << endl;
     if(QThresh >0)
     {
       if(fCalData)
         DetList->at(0)->SetGateCal(QThresh,4095.);
     }
      // QY Threholds ---------------
     IF->GetLine(Line,Len);
     if(!(Ptr = CheckComment(Line)))
     {
       Char_t Message[200];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading QY Thresholds!",
           DetList->at(1)->GetName(),
           DetList->at(1)->GetNameExt(),
           IF->GetFileName());
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }
     L->File << "<"<< DetList->at(1)->GetName()
       << "><" <<DetList->at(1)->GetNameExt()
       << endl;

     IF->GetLine(Line,Len);
     InOut->clear();
     *InOut << Line;
     *InOut >> QThresh;
     if(InOut->fail())
     {
       InOut = CleanDelete(InOut);
       Char_t Message[200];
       sprintf(Message,"In <%s><%s> : Wrong File Format in (%s) while reading QY Thresholds!",
           DetList->at(1)->GetName(),
           DetList->at(1)->GetName(),
           IF->GetFileName() );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }

     L->File <<  fixed << setprecision(3) <<  QThresh  << endl;
     if(QThresh >0)
     {
       if(fCalData)
         DetList->at(1)->SetGateCal(QThresh,4095.);
     }
     InOut = CleanDelete(InOut);
     
     END;
   }

   #ifdef WITH_ROOT
   void FST_Position::CreateHistogramsCal1D(TDirectory *Dir)
   {
     START;
     string Name;
     Dir->cd("");
     if(SubFolderHistCal1D.size()>0)
     {
       Name.clear();
       Name = SubFolderHistCal1D ;
       if(!(gDirectory->GetDirectory(Name.c_str())))
         gDirectory->mkdir(Name.c_str());
       gDirectory->cd(Name.c_str());
     }

     char HName[100];
     char HTitle[100];
     sprintf(HName,"X_%s%s",GetName(),GetNameExt());
     sprintf(HTitle,"%s%s X (mm)",GetName(),GetNameExt());
     AddHistoCal(GetCalName(0),HName,HTitle,1000,-500,500);
     sprintf(HName,"Y_%s%s",GetName(),GetNameExt());
     sprintf(HTitle,"%s%s Y (mm)",GetName(),GetNameExt());
     AddHistoCal(GetCalName(1),HName,HTitle,1000,-500,500);



     END;
   }

   void FST_Position::CreateHistogramsCal2D(TDirectory *Dir)
   {
     START;
     string Name;
     char HName[100];
     char HTitle[100];

     BaseDetector::CreateHistogramsCal2D(Dir);

     Dir->cd("");

     if(SubFolderHistCal2D.size()>0)
     {
       Name.clear();
       Name = SubFolderHistCal2D ;
       if(!(gDirectory->GetDirectory(Name.c_str())))
         gDirectory->mkdir(Name.c_str());
       gDirectory->cd(Name.c_str());
     }

     sprintf(HName,"X_Y_%s",GetName());
     sprintf(HTitle,"X vs Y %s (mm) (mm)", GetName());
     AddHistoCal(GetCalName(0),GetCalName(1),HName,HTitle,1000,-500,500,1000,-500,500);

     END;
   }
 
void FST_Position::OutAttach(TTree *OutTTree)
{
  START;

  Char_t BranchName[50];
  Char_t BranchType[50];
   
  sprintf(BranchName,"%s_X_TS",GetName());
  sprintf(BranchType, "%s_X_TS/l", GetName());
  OutTTree->Branch(BranchName, &TimeStampX, BranchType);

  sprintf(BranchName,"%s_Y_TS",GetName());
  sprintf(BranchType, "%s_Y_TS/l", GetName()); 
  OutTTree->Branch(BranchName, &TimeStampY, BranchType);

  BaseDetector::OutAttach(OutTTree);
  
  END;
}

  void FST_Position::SetOpt(TTree *OutTTree, TTree *InTTree)
   {
     START;
   
     
     // St histogram Hierarchy
     SetMainHistogramFolder("");
     SetHistogramsCal1DFolder("POS1D");
     SetHistogramsCal2DFolder("POS2D");

     for(UShort_t i = 0;i<DetList->size();i++)
     {
       DetList->at(i)->SetMainHistogramFolder("");
       DetList->at(i)->SetHistogramsRaw1DFolder("POS1D");
       DetList->at(i)->SetHistogramsRaw2DFolder("POS2D");
       DetList->at(i)->SetHistogramsCal1DFolder("POS1D");
       DetList->at(i)->SetHistogramsCal2DFolder("POS2D");
     }

     if(fMode == MODE_WATCHER)
     {
       if(fRawData)
       {
         SetHistogramsRaw(true);
       }
       if(fCalData)
       {
         SetHistogramsCal(true);
       }
       for(UShort_t i = 0;i<DetList->size();i++)
       {
         if(DetList->at(i)->HasRawData())
         {
             DetList->at(i)->SetHistogramsRawSummary(true);
         }
         if(DetList->at(i)->HasCalData())
	   {
	     DetList->at(i)->SetHistogramsCalSummary(true);
           
	   }
       }
     }
     else if(fMode == MODE_D2R)
     {
       for(UShort_t i = 0;i<DetList->size();i++)
       {
         if(DetList->at(i)->HasRawData())
	   {
	     DetList->at(i)->SetOutAttachRawV(true);
	     DetList->at(i)->SetOutAttachTime(true);
	     DetList->at(i)->SetHistogramsRaw2D(true);
	     DetList->at(i)->SetHistogramsRawSummary(true);
         }
         DetList->at(i)->OutAttach(OutTTree);
       }

       OutAttach(OutTTree);

     }
     else if(fMode == MODE_D2A)
     {
       if(fRawData)
       {
         SetHistogramsRaw(false);
         SetOutAttachRawI(false);
         SetOutAttachRawV(false);
       }
       if(fCalData)
       {
         SetHistogramsCal(true);
         SetOutAttachCalI(true);
         SetOutAttachCalV(false);
       }

       for(UShort_t i = 0;i<DetList->size();i++)
       {
         if(DetList->at(i)->HasRawData())
         {
           DetList->at(i)->SetHistogramsRaw1D(false);
           DetList->at(i)->SetHistogramsRaw2D(true);
	   DetList->at(i)->SetHistogramsRawSummary(true);
           DetList->at(i)->SetOutAttachRawI(false);
           DetList->at(i)->SetOutAttachRawV(false);
	   DetList->at(i)->SetOutAttachTime(true);

         }
         if(DetList->at(i)->HasCalData())
         {
	   DetList->at(i)->SetHistogramsCal1D(false);
	   DetList->at(i)->SetHistogramsCalSummary(true);
	   // Drift Cal Strips
	   // No strips
	   DetList->at(i)->SetOutAttachCalV(true);
	   DetList->at(i)->SetOutAttachCalI(false);
	      
         }
         DetList->at(i)->OutAttach(OutTTree);
       }

       OutAttach(OutTTree);
     }
     else if(fMode == MODE_R2A)
     {
       for(UShort_t i = 0;i<DetList->size();i++)
	 {
           DetList->at(i)->SetInAttachRawV(true);
	   DetList->at(i)->InAttach(InTTree);

	 }

       if(fRawData)
       {
         SetHistogramsRaw(false);
         SetOutAttachRawI(false);
         SetOutAttachRawV(false);
       }
       if(fCalData)
       {
         SetHistogramsCal(true);
         SetOutAttachCalI(true);
         SetOutAttachCalV(false);
       }

       for(UShort_t i = 0;i<DetList->size();i++)
	 {
	   if(DetList->at(i)->HasCalData())
	     {
             DetList->at(i)->SetHistogramsCal1D(false);
             DetList->at(i)->SetHistogramsCalSummary(true);
             DetList->at(i)->SetOutAttachCalV(true);
          
	     }
	   DetList->at(i)->OutAttach(OutTTree);
	 }
       OutAttach(OutTTree);
     }

     else if(fMode == MODE_CALC)
     {
       SetNoOutput();
     }
     else
     {
       Char_t Message[200];
       sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
       MErr * Er= new MErr(WhoamI,0,0, Message);
       throw Er;
     }

     if(VerboseLevel >= V_INFO)
       PrintOptions(cout)  ;

     END;
   }
   #endif
