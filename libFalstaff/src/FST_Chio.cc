   /****************************************************************************
    *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
    *    lemasson@ganil.fr
    *
    *    Contributor(s) :
    *    Antoine Lemasson, lemasson@ganil.fr
    *    Maurycy Rejmund, rejmund@ganil.fr
    *    Diego Ramos, diego.ramos@ganil.fr
    *
    *    This software is  a computer program whose purpose  is to provide data
    *    manipulation tools to Analyse experimental data collected at the VAMOS
    *    spectrometer.
    *
    *    This software is governed by the CeCILL-B license under French law and
    *    abiding by the  rules of distribution of free  software.  You can use,
    *    modify  and/ or  redistribute  the  software under  the  terms of  the
    *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
    *    URL \"http://www.cecill.info\".
    *
    *    As a counterpart to the access  to the source code and rights to copy,
    *    modify  and redistribute granted  by the  license, users  are provided
    *    only with a limited warranty  and the software's author, the holder of
    *    the economic  rights, and the  successive licensors have  only limited
    *    liability.
    *
    *    In this respect, the user's attention is drawn to the risks associated
    *    with loading,  using, modifying  and/or developing or  reproducing the
    *    software by the user in light of its specific status of free software,
    *    that  may mean that  it is  complicated to  manipulate, and  that also
    *    therefore  means that it  is reserved  for developers  and experienced
    *    professionals having in-depth  computer knowledge. Users are therefore
    *    encouraged  to load  and test  the software's  suitability  as regards
    *    their  requirements  in  conditions  enabling the  security  of  their
    *    systems  and/or data to  be ensured  and, more  generally, to  use and
    *    operate it in the same conditions as regards security.
    *
    *    The fact that  you are presently reading this means  that you have had
    *    knowledge of the CeCILL-B license and that you accept its terms.
    ***************************************************************************/
#include "FST_Chio.hh"

FST_Chio::FST_Chio(const Char_t *Name,
		   UShort_t NDetectors,
		   Bool_t RawData,
		   Bool_t CalData,
		   Bool_t DefaultCalibration,
		   const Char_t *NameExt,
		   int ParOffSet)
  : BaseDetector(Name,0, false, false, DefaultCalibration, false, NameExt, true, true)
{
  START;
  
  NumberOfSubdetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  FalstaffParOffSet = ParOffSet;
  AllocateComponents();
  
  END;
}
FST_Chio::~FST_Chio(void)
{
  START;

  END;
}

void FST_Chio::Clear(void)
{
  START;
     
  BaseDetector::Clear(); 
     
  TimeStampChio = 0;
     
  END;
}

void FST_Chio::AllocateComponents(void)
{
  START;

  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = false;
  Bool_t PosInfo = false;

  string BaseName;
  string Name;
  BaseName = DetectorName ;
  Name = BaseName + "_Trace";
  BaseDetector *Trace = new BaseDetector(Name.c_str(),NumberOfSubdetectors,RawData,CalData,DefCal,PosInfo,"",false,true);
 
  AddComponent(Trace);


  END;
}

void FST_Chio::SetParameters(Parameters* PL,Map* Map)
{
  START;
 
  Parameters * PL = Parameters::getInstance();
  
  // Add Virtual Parameters  
  Char_t ParN[500];
  for(Int_t i=0;i<DetList->at(0)->GetNumberOfDetectors();i++){
    sprintf(ParN,"%s_Trace_%04d",DetectorName,i);
     PL->Add(FalstaffParOffSet+i,ParN,14);
  }

  Char_t PName[200];
  for(int i=0; i < DetList->at(0)->GetNumberOfDetectors(); i++)
    {
      sprintf(PName, "%s_Trace_%04d",DetectorName, i);
      DetList->at(0)->SetParameterName(PName, i);
      DetList->at(0)->SetRawHistogramsParams(16384,0,16384);
      DetList->at(0)->SetGateRaw(0,16383);
    }
  
  DetList->at(0)->SetParameters(PL, Map);
  
  
  END;
}

Bool_t FST_Chio::Treat(void)
{
  START;
  TimeStampChio = 0;
  if(DetList->at(0)->GetRawM()>0)
    {
      TimeStampChio = DetList->at(0)->GetRawTSAt(0);
      //cout<<TimeStampChio<<endl;  
      isPresent = true;
    }
  return isPresent;
  END;
}
#ifdef WITH_ROOT
void FST_Chio::OutAttach(TTree *OutTTree)
{
  START;

  Char_t BranchName[150];
  Char_t BranchType[150];
   
  sprintf(BranchName,"%s_Trace_TS",GetName());
  sprintf(BranchType, "%s_Trace_TS/l", GetName());
  OutTTree->Branch(BranchName, &TimeStampChio, BranchType);

  BaseDetector::OutAttach(OutTTree);
  
  END;
}

void FST_Chio::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
   
  for(UShort_t i = 0;i<DetList->size();i++)
    { 
      // No Histograms !
      DetList->at(i)->SetHistogramsRaw(false);
      DetList->at(i)->SetHistogramsRaw1DFolder("Raw");
      DetList->at(i)->SetHistogramsRaw2DFolder("Raw");
      DetList->at(i)->SetHistogramsCal(false);
      DetList->at(i)->SetHistogramsCal1DFolder("Cal");
      DetList->at(i)->SetHistogramsCal2DFolder("Cal");
      DetList->at(i)->SetHistogramsRawSummary(true);
      
    }
  
  if(fMode == MODE_WATCHER)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(true);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRawSummary(true);
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCalSummary(true);
           
	    }
	}
    }
  else if(fMode == MODE_D2R)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetOutAttachRawV(true);
	      //DetList->at(i)->SetOutAttachTime(false);
	      //DetList->at(i)->SetHistogramsRaw2D(true);
	      //DetList->at(i)->SetHistogramsRawSummary(true);
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}

      OutAttach(OutTTree);

    }
  else if(fMode == MODE_D2A)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(false);
	  SetOutAttachRawI(false);
	  SetOutAttachRawV(false);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	  SetOutAttachCalV(false);
	}

      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetOutAttachRawV(true);
	      //DetList->at(i)->SetHistogramsRaw1D(false);
	      //DetList->at(i)->SetHistogramsRaw2D(true);
	      //DetList->at(i)->SetHistogramsRawSummary(true);
	      //DetList->at(i)->SetOutAttachRawI(false);
	      //DetList->at(i)->SetOutAttachRawV(false);
	      //DetList->at(i)->SetOutAttachTime(false);

	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      //DetList->at(i)->SetHistogramsCal1D(false);
	      //DetList->at(i)->SetHistogramsCalSummary(true);
	      // Drift Cal Strips
	      // No strips
	      //DetList->at(i)->SetOutAttachCalV(true);
	      //DetList->at(i)->SetOutAttachCalI(false);
	      
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}

      OutAttach(OutTTree);
    }
  else if(fMode == MODE_R2A)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  DetList->at(i)->SetInAttachRawV(true);
	  DetList->at(i)->InAttach(InTTree);

	}

      if(fRawData)
	{
	  //SetHistogramsRaw(false);
	  //SetOutAttachRawI(false);
	  //SetOutAttachRawV(false);
	}
      if(fCalData)
	{
	  //SetHistogramsCal(true);
	  //SetOutAttachCalI(true);
	  //SetOutAttachCalV(false);
	}

      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasCalData())
	    {
	      //DetList->at(i)->SetHistogramsCal1D(false);
	      //DetList->at(i)->SetHistogramsCalSummary(true);
	      //DetList->at(i)->SetOutAttachCalV(true);
          
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}
      OutAttach(OutTTree);
    }

  else if(fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else
    {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
    }

  if(VerboseLevel >= V_INFO)
    PrintOptions(cout)  ;

  END;
}
#endif
