# Source this script to set up the LIBVAMOS build that this script is part of.
#
# This script if for the bash like shells, see env.csh for csh like shells.
#
# Author: Antoine Lemasson 29/11/2013

export VAMOSLIB=@INSTALL_CMAKE_DIR@

if [ -z "${LD_LIBRARY_PATH}" ]; then
   LD_LIBRARY_PATH=$VAMOSLIB/lib; export LD_LIBRARY_PATH       # Linux, ELF HP-UX
else
   LD_LIBRARY_PATH=$VAMOSLIB/lib:$LD_LIBRARY_PATH; export LD_LIBRARY_PATH
fi

if [ -z "${DYLD_LIBRARY_PATH}" ]; then
   DYLD_LIBRARY_PATH=$VAMOSLIB/lib; export DYLD_LIBRARY_PATH   # Mac OS X
else
   DYLD_LIBRARY_PATH=$VAMOSLIB/lib:$DYLD_LIBRARY_PATH; export DYLD_LIBRARY_PATH
fi