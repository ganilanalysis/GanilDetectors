# Source this script to set up the LIBVAMOS build that this script is part of.
#
# This script if for the csh like shells, see env.sh for bash like shells.
#
# Author: Antoine Lemasson 29/11/2013

setenv VAMOSLIB @INSTALL_CMAKE_DIR@

if ($?LD_LIBRARY_PATH) then
   setenv LD_LIBRARY_PATH $VAMOSLIB/lib:$LD_LIBRARY_PATH      # Linux, ELF HP-UX
else
   setenv LD_LIBRARY_PATH $VAMOSLIB/lib
endif

if ($?DYLD_LIBRARY_PATH) then
   setenv DYLD_LIBRARY_PATH $VAMOSLIB/lib:$DYLD_LIBRARY_PATH  # Mac OS X
else
   setenv DYLD_LIBRARY_PATH $VAMOSLIB/lib
endif
