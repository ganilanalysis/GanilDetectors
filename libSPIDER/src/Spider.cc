#include "Spider.hh"

Spider::Spider(const Char_t *Name,
	       UShort_t NDetectors, 
	       Bool_t RawData, 
	       Bool_t CalData, 
	       Bool_t DefaultCalibration, 
	       const Char_t *NameExt
	       )
  : BaseDetector(Name,7, false, CalData, DefaultCalibration,false,NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  fCalData = CalData;
  DetManager *DM = DetManager::getInstance();
#ifdef WITH_ROOT
  rn =new TRandom();
#endif
  AllocateComponents(); 
  
  if(CalData)
    {
      sprintf(CalNameI[0],"%s_X", DetectorName);
      sprintf(CalNameI[1],"%s_Y", DetectorName);
      sprintf(CalNameI[2],"%s_Z", DetectorName);
      sprintf(CalNameI[3],"%s_Theta", DetectorName);
      sprintf(CalNameI[4],"%s_Phi", DetectorName);
      sprintf(CalNameI[5],"%s_Erings", DetectorName);
      sprintf(CalNameI[6],"%s_Esectors", DetectorName);
      SetGateCal(-50,50,0);
      SetGateCal(-50,50,1);
#ifdef WITH_ROOT    
      SetGateCal(0,TMath::Pi(),3);
      SetGateCal(0,2*TMath::Pi(),4);
#endif
      SetGateCal(-10,16000,5);
      SetGateCal(-10,16000,6);
    }
  char Line[255];
  stringstream *InOut;
  InOut = new stringstream();
  *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
  *InOut>>Line;
  InOut = CleanDelete(InOut);
  MIFile *IF = new MIFile(Line);
  for(int i=0;i<2;i++)
    IF->GetLine(Line,255);
  InOut = new stringstream();
  *InOut<<Line;
  *InOut>>Zref;
  InOut = CleanDelete(InOut);
  IF = CleanDelete(IF);
  
  END;
}
Spider::~Spider(void)
{
  START;
  END;

}


void Spider::CalculatePosition(void)
{
  START;
  //isPresent = false;
#ifdef WITH_ROOT
  Float_t pi = TMath::Pi();
#else
  Zref = 25.0; //mm from target 
  Float_t pi = 3.14159265358979312;
#endif
  SpX=SpY=SpTh=SpPh=Esec=Ering = -1500;
  SpZ = Zref;
  Int_t SecM, RingM;
  SecM = RingM = -1;

  Bool_t quadrant = false;
  if(DetList->at(0)->GetM()>0 && DetList->at(1)->GetM()>0)
    for (int i=0; i <DetList->at(1)->GetM() ; i++)
      if(DetList->at(1)->GetCalAt(i)>0)
	{
	  SpPh = pi/2.+2*pi/16.*(15.-DetList->at(1)->GetNrAt(i)+0.5);
	  if(SpPh>2*pi) SpPh -= 2*pi;
	  
	  for (int j=0; j <DetList->at(0)->GetM() ; j++)
	    if(DetList->at(0)->GetCalAt(j)>0) 
	      {
		//////////only taking coherent quadrants//////////
		quadrant=false;
		if(SpPh<pi/2) {
		  if(DetList->at(0)->GetNrAt(j)/16==1)
		    quadrant=true;
		}
		else if(SpPh<pi){
		  if(DetList->at(0)->GetNrAt(j)/16==0)
		    quadrant=true;
		}
		else if(SpPh<3./2*pi){
		  if(DetList->at(0)->GetNrAt(j)/16==2)
		    quadrant=true;
		}
		else{
		  if(DetList->at(0)->GetNrAt(j)/16==3)
		    quadrant=true;
		}	
		///////////////////////////////////////////////////
		if(quadrant)
		  {
		    if(DetList->at(1)->GetCalAt(i)>Esec)
		      { 
			Esec = DetList->at(1)->GetCalAt(i);
			SecM = i;
		      } 
		    if(DetList->at(0)->GetCalAt(j)>Ering)
		      {
			Ering = DetList->at(0)->GetCalAt(j);
			RingM = j;
		      }
		  }
	      }
	}
  if(SecM>-1 && RingM>-1)
    {
      SpPh = pi/2.+2*pi/16.*(15.-DetList->at(1)->GetNrAt(SecM)+0.5);
      if(SpPh>2*pi) SpPh -= 2*pi;

#ifdef WITH_ROOT
      SpPh += 2.*pi/16*(rn->Rndm()-0.5);

      if( (DetList->at(0)->GetNrAt(RingM)/16==0) | (DetList->at(0)->GetNrAt(RingM)/16==2) )
	SpTh = atan((24.+1.5*(15.-DetList->at(0)->GetNrAt(RingM)%16+0.5 + (rn->Rndm()-0.5) ))/Zref);
      else if( (DetList->at(0)->GetNrAt(RingM)/16==1) | (DetList->at(0)->GetNrAt(RingM)/16==3) )
	SpTh = atan((24.+1.5*(DetList->at(0)->GetNrAt(RingM)%16+0.5 + (rn->Rndm()-0.5) ))/Zref);
#else
       if( (DetList->at(0)->GetNrAt(RingM)/16==0) | (DetList->at(0)->GetNrAt(RingM)/16==2) )
	SpTh = atan((24.+1.5*(15.-DetList->at(0)->GetNrAt(RingM)%16+0.5 ))/Zref);
      else if( (DetList->at(0)->GetNrAt(RingM)/16==1) | (DetList->at(0)->GetNrAt(RingM)/16==3) )
	SpTh = atan((24.+1.5*(DetList->at(0)->GetNrAt(RingM)%16+0.5 ))/Zref);
#endif  
      SpX = Zref*tan(SpTh)*cos(SpPh);
      SpY = Zref*tan(SpTh)*sin(SpPh); 
      isPresent = true;                    
    
    } 
  
  else 
    SpPh= -1500;
  
  
  SetCalData(0,SpX);
  SetCalData(1,SpY);
  SetCalData(2,SpZ);
  SetCalData(3,SpTh);
  SetCalData(4,SpPh);
  SetCalData(5,Ering);		    
  SetCalData(6,Esec);   
  
 
  END;
}


void Spider::AllocateComponents(void)
{
   START;
   Char_t Name[200];
   Bool_t RawData = fRawDataSubDetectors;
   Bool_t CalData = fCalData;
   Bool_t DefCal = true;
   Bool_t PosInfo = false;
//  
//   for(UShort_t i=0; i<NumberOfSubDetectors; i++)
//     {
//       Char_t Name[20];
//       sprintf(Name,"DC%d",i);
//       Spider *ADet = new Spider(Name,rawData,calData,DefCal,DetectorNameExt);
//       AddComponent(ADet);
//     }
//
//   // for(UShort_t k= 0; k<4;k++)
//   //   {
//   //     SpiderQ *D = new SpiderQ("",1,RawData,CalData,DefCal,PosInfo,"",true);
//   //   }
   
   sprintf(Name,"%s_Str",DetectorName);

  BaseDetector *Str = new BaseDetector(Name,64,RawData,CalData,DefCal,PosInfo,"",true);
  if(RawData)
    {
      for (int i=0; i < 64; i++)
       	{
       	  if(i<32)
       	    sprintf(Name,"%s_U_Str_%02d",DetectorName,i);
       	  else
       	    sprintf(Name,"%s_D_Str_%02d",DetectorName,i-32);
       	  Str->SetParameterName(Name, i);
	}
      Str->SetGateRaw(0,16384);
    }

  Str->SetCalHistogramsParams(1000,0,200,"MeV");
  if(CalData)
    //Str->SetGateCal(0,200);
    Str->SetGateCal(0,20000);

  AddComponent(Str);

 
  sprintf(Name,"%s_Sec",DetectorName);
  BaseDetector *Sec = new BaseDetector(Name,16,RawData,CalData,DefCal,PosInfo,"",true);
  if(RawData)
    {
      for (int i=0; i < 16; i++)
	{
	  sprintf(Name,"%s_Sec_%02d",DetectorName,i);
	  Sec->SetParameterName(Name, i);
	}
      Sec->SetGateRaw(0,16384);
    }
  
  Sec->SetCalHistogramsParams(1000,0,200,"MeV");
  if(CalData)
    //Sec->SetGateCal(0,200);
    Sec->SetGateCal(0,20000);
   
  AddComponent(Sec);


   END;
}

void Spider::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  for(UShort_t i=0; i< DetList->size(); i++)
    DetList->at(i)->SetParameters(Par,Map);
  END;
}

Bool_t Spider::Treat(void)
{ 
  START;
  Ctr->at(0)++;

  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        {
          DetList->at(i)->Treat();
        }
    }
  if(fCalData)
    CalculatePosition();
  
  return(isPresent); 
  END;
}

#ifdef WITH_ROOT
void Spider::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }
  AddHistoCal(CalNameI[0],CalNameI[1],Form("%s_X_vs_Y",DetectorName),"X_vs_Y",100,-50,50,100,-50,50);
  
  END;
}
#endif

#ifdef WITH_ROOT
void Spider::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
  string Name;
  //SetMainHistogramFolder("Spider");
  SetHistogramsCal1DFolder("Sp1D");
  SetHistogramsCal2DFolder("Sp2D");
  

  // Name = DetectorName + "_XY";
  //  sprintf(Name,"%s_XY",DetectorName.c_str());
  //AddHistoCal(CalNameI[0],CalNameI[1],"X_Y","X_vs_Y",600,-50,50,600,-50,50);
  
  // Set histogram Hierarchy
  for(UShort_t i = 0;i<DetList->size();i++)
    {
      DetList->at(i)->SetMainHistogramFolder("");
      // DetList->at(i)->SetHistogramsRaw1DFolder("Raw1D");
      DetList->at(i)->SetHistogramsRaw1DFolder("Sp1D");
      DetList->at(i)->SetHistogramsRaw2DFolder("Sp2D");
      DetList->at(i)->SetHistogramsCal1DFolder("Sp1D");
      DetList->at(i)->SetHistogramsCal2DFolder("Sp2D");
// DetList->at(i)->SetHistogramsRaw2DFolder("Raw2D");
      // if(i == 1 || i == 2)
      // 	{
      // 	  DetList->at(i)->SetHistogramsCal1DFolder("SI_C_T");
      // 	  DetList->at(i)->SetHistogramsCal2DFolder("SI_C_T");
      // 	}
      // else
      // 	{
      // 	  DetList->at(i)->SetHistogramsCal1DFolder("SI_C");
      // 	  DetList->at(i)->SetHistogramsCal2DFolder("SI_C");
      // 	}
      SetHistogramsCal1DFolder("Cal1D");
      SetHistogramsCal2DFolder("Cal2D");
    }
 
  if(fMode == MODE_WATCHER)
    {
      if(fRawData)
	{
	  SetHistogramsRaw(true);
	}
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  }
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw(true);
	      DetList->at(i)->SetHistogramsRawSummary(true);
	    }
	  if(DetList->at(i)->HasCalData())
	    {
	      DetList->at(i)->SetHistogramsCal(true);
	      DetList->at(i)->SetHistogramsCalSummary(true);
	      }
	}
    }
  else if(fMode == MODE_D2R)
    {
      for(UShort_t i = 0;i<DetList->size();i++)
	{
	  if(DetList->at(i)->HasRawData())
	    {
	      DetList->at(i)->SetHistogramsRaw(true);

	      DetList->at(i)->SetHistogramsRawSummary(true);
	      DetList->at(i)->SetOutAttachRawV(true);
	      DetList->at(i)->SetOutAttachRawI(false);
	    }
	  DetList->at(i)->OutAttach(OutTTree);
	}
      OutAttach(OutTTree);
      
    }
  else if(fMode == MODE_D2A)
    {
		if(fRawData)
		  {
			 SetHistogramsRaw(false);
			 SetOutAttachRawI(false);
			 SetOutAttachRawV(false);
		  }
		if(fCalData)
		  {
		    SetHistogramsCal(true);
		    SetOutAttachCalI(true);
		    SetOutAttachCalV(false);
		  }
		
		for(UShort_t i = 0;i<DetList->size();i++)
		  {			
		    if(DetList->at(i)->HasRawData())
		      {
			DetList->at(i)->SetHistogramsRaw1D(true);
			DetList->at(i)->SetHistogramsRaw2D(true);
			
			DetList->at(i)->SetHistogramsRawSummary(true);
			DetList->at(i)->SetOutAttachRawV(false);
			DetList->at(i)->SetOutAttachRawI(false);			 					  
		      }
		    if(DetList->at(i)->HasCalData())
		      {
			DetList->at(i)->SetHistogramsCal1D(true);
			DetList->at(i)->SetHistogramsCal2D(true);

			DetList->at(i)->SetHistogramsCalSummary(true);
			DetList->at(i)->SetOutAttachCalI(false);
			DetList->at(i)->SetOutAttachCalV(true);			 
			
		      }
		    DetList->at(i)->OutAttach(OutTTree);
		  }
		
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
		    DetList->at(i)->SetInAttachRawV(true);
		    DetList->at(i)->InAttach(InTTree);
		  }
		
		SetOutAttachCalI(true);
		
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			  if(DetList->at(i)->HasCalData())
				{
				  DetList->at(i)->SetHistogramsCal1D(true);
				  DetList->at(i)->SetHistogramsCal2D(true);
			      
				  DetList->at(i)->SetHistogramsCalSummary(true);
				  DetList->at(i)->SetOutAttachCalI(false);
				  DetList->at(i)->SetOutAttachCalV(true);			 
				  
				}
			  DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
    }
  else if (fMode == MODE_RECAL)
    {
      	for(UShort_t i = 0;i<DetList->size();i++)
	  {

	    DetList->at(i)->SetInAttachCalV(true);
	    
	    DetList->at(i)->InAttachCal(InTTree);					 
	  }
	SetInAttachCalI(true);
	InAttachCal(InTTree);

	SetOutAttachCalI(true);

	for(UShort_t i = 0;i<DetList->size();i++)
	  {
	    if(DetList->at(i)->HasCalData())
	      {
		DetList->at(i)->SetHistogramsCal1D(true);
		DetList->at(i)->SetHistogramsCal2D(true);
		
		DetList->at(i)->SetHistogramsCalSummary(false);
		DetList->at(i)->SetOutAttachCalI(true);
		// DetList->at(i)->SetOutAttachRawI(true);
		
	      }
	    DetList->at(i)->OutAttach(OutTTree);
	  }
	OutAttach(OutTTree);
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}


#endif
