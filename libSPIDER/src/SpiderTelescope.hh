#ifndef _SPIDERTELESCOPE_CLASS
#define _SPIDERTELESCOPE_CLASS

#include "Defines.hh"
#include "BaseDetector.hh"
#include "Spider.hh"

class SpiderTelescope : public BaseDetector
{
public :
  //! Constructor
  SpiderTelescope(const Char_t * Name, //!< Detector Name
                 Spider *SPDE,  //!< Pointer to Spider DE detector
                 Spider *SPE  //!< Pointer to  Spider E detector
                 ); 
   ~SpiderTelescope(void); //!< Destructor


#ifdef WITH_ROOT
  //! Create 1D Histograms for Calculated Parameters
  void CreateHistogramsCal1D(TDirectory *Dir);
  //! Create 2D Histograms for Calculated Parameters
  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Set Input/OutPut Option
  void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
  //! Treat the data
  Bool_t Treat(void);
  
protected :
  Spider *SpDE;     //!< Pointer to Spider dE Object
  Spider *SpE;          //!< Pointer to Spider E Object
  
private: 
  //! Reset Local Variable
  void Init(void);
 //! Calculate the Calibrated parameters
  Bool_t Calculate(void);
  
  Float_t SpAngle;
  Float_t X_dE;
  Float_t Y_dE;
  Float_t Z_dE;
  Float_t Th_dE;
  Float_t Ph_dE;
  Float_t X_E;
  Float_t Y_E;
  Float_t Z_E;
  Float_t Th_E;
  Float_t Ph_E;
  Float_t dE;
  Float_t E;
  Float_t Etot;
};
#endif
