#include "SpiderTelescope.hh"


SpiderTelescope::SpiderTelescope(const Char_t *Name,
                               Spider *SPDE,
                               Spider *SPE
                               )
: BaseDetector(Name, 11, false, true, false,false,"")
{
  START;
  
  
  // Spider detector
  SpDE = NULL;
  SpE = NULL;
  SpDE = SPDE;
  SpE = SPE;

  if(fCalData)
    {
      sprintf(CalNameI[0],"%s_X_dE", DetectorName);
      sprintf(CalNameI[1],"%s_Y_dE", DetectorName);
      //sprintf(CalNameI[2],"%s_Z_dE", DetectorName);
      sprintf(CalNameI[2],"%s_Th_dE", DetectorName);
      sprintf(CalNameI[3],"%s_Ph_dE", DetectorName);
      
      sprintf(CalNameI[4],"%s_X_E", DetectorName);
      sprintf(CalNameI[5],"%s_Y_E", DetectorName);
      //sprintf(CalNameI[7],"%s_Z_E", DetectorName);
      sprintf(CalNameI[6],"%s_Th_E", DetectorName);
      sprintf(CalNameI[7],"%s_Ph_E", DetectorName);
      
      sprintf(CalNameI[8],"%s_dE", DetectorName);
      sprintf(CalNameI[9],"%s_E", DetectorName);
      sprintf(CalNameI[10],"%s_Etot", DetectorName);
    }
  
  char Line[255];
  stringstream *InOut;
  InOut = new stringstream();
  *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
  *InOut>>Line;
  InOut = CleanDelete(InOut);
  MIFile *IF = new MIFile(Line);
  for(int i=0;i<2;i++)
    IF->GetLine(Line,255);
  InOut = new stringstream();
  *InOut<<Line;
  *InOut>>SpAngle;
  InOut = CleanDelete(InOut);
  IF = CleanDelete(IF);
  
END;
}

SpiderTelescope::~SpiderTelescope(void)
{
  START;

  END;
}

Bool_t SpiderTelescope::Treat(void)
{
  START;
  Ctr->at(0)++;
  
 
      Calculate();
#ifdef DEBUG
      PrintCal();
#endif
   
  return (isPresent);
  END;
}


void SpiderTelescope::Init(void)
{
  START;
  X_dE = Y_dE = Z_dE = Th_dE = Ph_dE = X_E = Y_E = Z_E = Th_E = Ph_E = dE = E = Etot = -1500.0;
  END;
}

Bool_t SpiderTelescope::Calculate(void)
{
  START;
  // Reset Local Variable
  Init();
#ifdef WITH_ROOT
  Float_t pi = TMath::Pi();
  
  if(SpDE && SpDE->IsPresent()) 
    {
      X_dE = -1.* SpDE->GetCal(0);
      Y_dE = SpDE->GetCal(1);
      //Z_dE = SpDE->GetCal(2)-SpDE->GetCal(0)*sin(SpAngle*pi/180.);  
      Th_dE = SpDE->GetCal(3);
      Ph_dE = pi-SpDE->GetCal(4);
      dE = SpDE->GetCal(5)*cos(Th_dE);
    } 
  if(SpE &&  SpE->IsPresent())
    {
      //X_E = SpE->GetCal(0)*cos(SpAngle*pi/180.);
      //Y_E = SpE->GetCal(1);
      //Z_E = SpE->GetCal(2)-SpE->GetCal(0)*sin(SpAngle*pi/180.);
      //Th_E = atan((sqrt(X_E*X_E+Y_E*Y_E))/(Z_E));
      //Ph_E =atan(Y_E/X_E); 
      X_E = SpE->GetCal(0);
      Y_E = SpE->GetCal(1);  
      Th_E = SpE->GetCal(3);
      Ph_E = SpE->GetCal(4);
      E = SpE->GetCal(6);
    }
  if((SpDE && SpDE->IsPresent()) && (SpE &&  SpE->IsPresent())) 
    Etot = SpDE->GetCal(5) + E;
  else if((SpDE && SpDE->IsPresent())) 
    Etot = SpDE->GetCal(5); 
  
  SetCalData(0,X_dE);
  SetCalData(1,Y_dE);
  //SetCalData(2,Z_dE);
  SetCalData(2,Th_dE);
  SetCalData(3,Ph_dE);
  SetCalData(4,X_E);
  SetCalData(5,Y_E);
  //SetCalData(7,Z_E);
  SetCalData(6,Th_E);
  SetCalData(7,Ph_E);
  SetCalData(8,dE);
  SetCalData(9,E);
  SetCalData(10,Etot);
#endif   
  if(Etot>0) 
    {
      isPresent=true;
    }
  
  return (isPresent);
  
  END;
}


#ifdef WITH_ROOT
void SpiderTelescope::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
  // Set histogram Hierarchy
  SetHistogramsCal1DFolder("Sp1D");
  SetHistogramsCal2DFolder("Sp2D");
 
  if(fMode == MODE_WATCHER)
    {
      if(fCalData)
	{
	  SetHistogramsCal(true);
	}
    }
  else if(fMode == MODE_D2R)
	 {
      ;
	 }
  else if(fMode == MODE_D2A)
	 {
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
		  }
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
		  }      
		SetOutAttachCalI(true);
		OutAttach(OutTTree);		
    }
  else if(fMode == MODE_RECAL)
    {
      if(fCalData)
	{
	  SetHistogramsCal(true);
	  SetOutAttachCalI(true);
	}      
      SetOutAttachCalI(true);
      OutAttach(OutTTree);		
    }
  else if(fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }
  END;
}


void SpiderTelescope::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
  END;
}

void SpiderTelescope::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }

  AddHistoCal(GetCalName(10),GetCalName(8),"dE_E","dE_E",3000,0,300,3000,0,200);
  
  END;
}
#endif
