/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _EXOGAM_CLASS
#define _EXOGAM_CLASS
/**
 * @file   Exogam.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   Feb, 2012
 * @brief  EXOGAM Class
 *
 * Exogam class : Calibrate each  Clover detector Array.
 */
//#define ExtTSRef 1
#define ADDBACK 1
//#define ADDBACK_BGO 1
//#define ADDBACK_BGO_CSI 1
//#define EXO_VAMOS 1



#include "Defines.hh"
#ifdef EXO_VAMOS
#include "Reconstruction.hh"
#include "TargetPos.hh"
#include "Identification.hh"
#endif
#include "Clover.hh"
#include "BaseDetector.hh"


class Exogam : public BaseDetector
{
public:
    //! Constructor
    Exogam(const Char_t * Name,
           UShort_t NDetectors,
           Bool_t RawData,
           Bool_t CalData,
           Bool_t DefaultCalibration,
           const Char_t * NameExt);
    ~Exogam(void);

    //! Clear Arrays
    void Clear(void);

#ifdef WITH_ROOT  
    //! Set Input/OutPut Option
    void SetOpt(TTree *OutTTree, TTree *InTTree);
    //! Set individual calibrated  Parameters names
    void CreateHistograms(TFile *OFile);
    //! Convert from Lab to Vamos Coordinates
    void Lab2Vamos(Float_t ThetaL, Float_t PhiL, Float_t * Theta, Float_t * Phi);
#endif
    //! Set Read Specific Calibration/ANgles
    void ReadAngles(void);
    //! Set Parameter Patterns associated with Detector
    void SetParameters(Parameters* Par,Map* Map);
    //! Set Parameter Patterns associated with Detector
    void SetParametersNUMEXO(NUMEXOParameters* PL,Map* Map);
    //! Treat the data
    Bool_t Treat(void);

    //! Doppler Correction
    Float_t DopplerCorrection(Float_t Eg, Float_t Beta, Float_t CosAlpha);
    //! Vamos-Gamma Angle Calc
    Float_t CosAlpha(Float_t ThetaG, Float_t PhiG, Float_t ThetaV, Float_t PhiV);

    //! Set Internal Time Gates in 10 ns tics units
    void SetTSGates(Int_t LL, Int_t UL)
    {
        TSGate[0] = LL;
        TSGate[1] = UL;
    }
    //! Set Reference Time Gates in 10 ns tics units
    void SetRefTSGates(Int_t LL, Int_t UL)
    {
        RefTSGate[0] = LL;
        RefTSGate[1] = UL;
    }
    //! Check TS
    Bool_t CheckTSRef(ULong64_t T, ULong64_t TRef, Int_t LL, Int_t UL)
    {
        START;

        if(((Int_t) (T- TRef)) >= LL && ((Int_t) (T - TRef)) <= UL)
        {
            return true;
        }
        else
        {
            return false;
        }
        END;
    }

    //! SetTS Ref Detector
    void SetTSRefDetName(string DetName)
    {
#ifdef ExtTSRef
        TSRefDetName = DetName;
        //    DetManager *DM = DetManager::getInstance();
        //    RefDetTS = (TimeStamp*) DM->GetDetector(TSRefDetName.c_str());
#endif
    }

    void SetFlangeNr(Int_t ClNr, Int_t FlNr)
    {
        if(ClNr < NumberOfDetectors)
            FlangeMap[ClNr] = FlNr;

    }


protected:

    //! Allocate subcomponents
    void AllocateComponents(void);
#ifdef EXO_VAMOS
    //! Reconstruction information
    Reconstruction *Reco;
    //! Reconstruction information
    Identification *Id;
    //! Target Pos information
    TargetPos *TP;
#endif
    //! ANgle Outers
    //Float_t **Pos_Outers;
    Float_t Pos_Outers[16*16][3];

    //! FlangeMap
    Int_t *FlangeMap;

    //! Local Variable for
    //! Inner Multiplicity
    Int_t InM;
    //! Clover Nr
    UShort_t *InN;
    //! Inner Cal Energy
    Float_t *InV;
    //! Inner Cal Time
    Float_t *InT;
    //! Inner Timstamp
    ULong64_t *InTS;
    UShort_t *InUsed;
    UShort_t *BGOH;
    UShort_t *CSIH;

    Int_t OuM;
    UShort_t *OuN;
    Float_t *OuV;
    UShort_t *OuUsed;
    ULong64_t *OuTS;

    Int_t BGOM;
    UShort_t *BGON;
    Float_t *BGOV;
    ULong64_t *BGOTS;

    Int_t CSIM;
    UShort_t *CSIN;
    Float_t *CSIV;
    ULong64_t *CSITS;

    //! CLover Data
#ifdef ADDBACK
    Int_t ClE_ABM;
    Float_t *ClE_AB;
    Float_t *ClT_AB;
    Float_t *ClEDC_AB;
    UShort_t *ClE_ABN;
    ULong64_t *ClE_ABTS;
    Float_t *ClA_AB;
    UShort_t *ClE_ABSegMaxN;
    Float_t *ClE_ABSegMaxT;
    Float_t *ClE_ABSegMaxP;
    Float_t *ClE_ABSegMaxX;
    Float_t *ClE_ABSegMaxY;
    Float_t *ClE_ABSegMaxZ;
    Float_t *ClE_SegMaxE_AB;

#endif

#ifdef ADDBACK_BGO
    Int_t ClE_AB_BGOM;
    Float_t *ClE_AB_BGO;
    Float_t *ClEDC_AB_BGO;
    UShort_t *ClE_AB_BGON;
    ULong64_t *ClE_AB_BGOTS ;
    Float_t *ClA_AB_BGO;
    UShort_t *ClE_AB_BGOSegMaxN;

    Float_t *ClE_SegMaxE_AB_BGO;
#endif

#ifdef ADDBACK_BGO_CSI
    Int_t ClE_AB_BGO_CSIM;
    Float_t *ClE_AB_BGO_CSI;
    Float_t *ClEDC_AB_BGO_CSI;
    UShort_t *ClE_AB_BGO_CSIN;
    ULong64_t *ClE_AB_BGO_CSITS ;
    Float_t *ClA_AB_BGO_CSI;
    UShort_t *ClE_AB_BGO_CSISegMaxN;
    Float_t *ClE_AB_BGO_CSISegMaxT;
    Float_t *ClE_AB_BGO_CSISegMaxP;
    Float_t *ClE_AB_BGO_CSISegMaxX;
    Float_t *ClE_AB_BGO_CSISegMaxY;
    Float_t *ClE_AB_BGO_CSISegMaxZ;

    Float_t *ClE_SegMaxE_AB_BGO_CSI;
#endif

    Int_t TSGate[2];
    Int_t RefTSGate[2];

    Float_t InRange;
    Float_t OuRange;
    Float_t BGORange;
    Float_t CSIRange;

#ifdef ExtTSRef
    ULong64_t TSRef;
    std::string TSRefDetName;
#endif


    //! Internal detector
    Int_t Det_ECC_Offset;
    Int_t Det_ECC20_Offset;
    Int_t Det_GOCCE_Offset;
    Int_t Det_BGO_Offset;
    Int_t Det_CSI_Offset;
    Int_t Det_ESS_Offset;
    Int_t Det_Analysis_Offset;
    Int_t Det_Eg_Offset;
    Int_t Det_Ex_Offset;
    Int_t Det_ExCr_Offset;

};
#endif

