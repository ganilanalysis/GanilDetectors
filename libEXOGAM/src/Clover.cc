/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "Clover.hh"
Clover::Clover(const Char_t * Name,
               UShort_t	NDetectors,
               Bool_t RawData,
               Bool_t CalData,
               Bool_t DefaultCalibration,
               const Char_t * NameExt,
               UShort_t MaxMult)
  : BaseDetector(Name, 3, false, CalData, DefaultCalibration,false,NameExt)
{
  START;
  
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  fMaxMultSubDetectors = MaxMult;

  // Get Clover Number
  SetId(); 

  AllocateComponents();
  
  AddCounter("ECC_E"); //2
  AddCounter("ECC_T"); //3
  AddCounter("GOCCE_E"); //4
  AddCounter("GOCCE_T"); //5
  AddCounter("ESS"); //6
  AddCounter("Add Back"); //7
  AddCounter("Compton Rejection"); //8
  
  if(fCalData)
	 {
      sprintf(CalNameI[0],"%s_AB", DetectorName);
		sprintf(CalNameI[1],"%s_AB_AC", DetectorName);
        sprintf(CalNameI[2],"%s_Eg", DetectorName);
     }
  

  // ReadCalibration();
  END;
}


Clover::~Clover(void)
{
  START;
  END;
}


void Clover::SetId()
{
  START;
  UShort_t Id=0;

  if(sscanf(DetectorName,"Clover%02hu",&Id)==1)
    {
      ClId = Id;
    }
  else
    {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> : Wrong Detector Name in (%s) while extracting its Id! Format should be CloverXX", 
              GetName(),
              GetNameExt(),
              GetName());
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
    };
  END;
}


void Clover::AllocateComponents(void)
{
  START;
  Char_t Line[255];
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = false;
  Bool_t PosInfo = false;

  sprintf(Line, "Inner6M_%02d",ClId);
  BaseDetector * Inner6M = new BaseDetector(Line, 4,RawData,CalData,DefCal,PosInfo,"",false,true,fMaxMultSubDetectors,false);
  AddComponent(Inner6M);
  sprintf(Line, "Inner20M_%02d",ClId);
  BaseDetector * Inner20M = new BaseDetector(Line, 4,RawData,CalData,DefCal,PosInfo,"",false,true,fMaxMultSubDetectors,false);
  AddComponent(Inner20M);
  sprintf(Line, "DeltaT_%02d",ClId);
  BaseDetector * DeltaT = new BaseDetector(Line, 4,RawData,CalData,DefCal,PosInfo,"",false,true,fMaxMultSubDetectors,false);
  AddComponent(DeltaT);
  sprintf(Line, "Outers_%02d",ClId);
  BaseDetector * Outers = new BaseDetector(Line, 4*4,RawData,CalData,DefCal,PosInfo,"",false,true,fMaxMultSubDetectors,false);
  AddComponent(Outers);

  sprintf(Line, "BGO_%02d",ClId);
  BaseDetector * BGO = new BaseDetector(Line, 4,RawData,CalData,DefCal,PosInfo,"",false,true,fMaxMultSubDetectors,false);
  AddComponent(BGO);

  sprintf(Line, "CSI_%02d",ClId);
  BaseDetector * CSI = new BaseDetector(Line, 4,RawData,CalData,DefCal,PosInfo,"",false,true,fMaxMultSubDetectors,false);
  AddComponent(CSI);
  END;
}

void Clover::SetParametersNUMEXO(NUMEXOParameters* PL,Map* Map)
{
    SetParameters(NULL,Map);
}
void Clover::SetParameters(Parameters* Par,Map* Map)
{
  START;
  Char_t A[4] = {'A','B','C','D'};
  Char_t PName[100];
  if(isComposite){
    if(fRawDataSubDetectors)
      {
        for(UShort_t j=0;j<4;j++)
          {
            sprintf(PName,"Inner6M_Cl%02d_Cr%02d",ClId,j);
            DetList->at(0)->SetParameterName(PName, j);

            sprintf(PName,"Inner20M_Cl%02d_Cr%02d",ClId,j);
            DetList->at(1)->SetParameterName(PName, j);

            sprintf(PName,"DeltaT_Cl%02d_Cr%02d",ClId,j);
            DetList->at(2)->SetParameterName(PName, j);

            sprintf(PName,"BGO_Cl%02d_Cr%02d",ClId,j);
            DetList->at(4)->SetParameterName(PName, j);

            sprintf(PName,"CSI_Cl%02d_Cr%02d",ClId,j);
            DetList->at(5)->SetParameterName(PName, j);
          }


        for(UShort_t j=0;j<4;j++)
          for(UShort_t k=0;k<4;k++)
            {
              sprintf(PName,"Outer%d_%02d_%02d",k+1,ClId,j);
              DetList->at(3)->SetParameterName(PName, j*4+k);
            }

        NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();

        for(UShort_t i=0;i<DetList->size();i++)
            DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);
      }
  }
  END;
}

Bool_t Clover::Treat(void)
{
  START;
  
   if(isComposite)
	  {
		 for(UShort_t i=0; i< DetList->size(); i++)
			{
			  DetList->at(i)->Treat();
			}
	  }

   if(fCalData)
     {

     }


  return isPresent;
  END;
}
 


void Clover::ReadCalibration(void)
{
  START;
  MIFile *IF;
  char Line[255];
  stringstream *InOut;
  if(fCalData)
    {
      InOut = new stringstream();
      *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << ".cal";
      *InOut>>Line;
      InOut = CleanDelete(InOut);
      if((IF = CheckCalibration(Line)))
        {
          // Read ECCE calibrations
          for(UShort_t i=0;i<GetNumberOfDetectors();i++)
            DetList->at(i+0*GetNumberOfDetectors())->ReadCalibration(IF);
          // Read ECCT calibrations
          for(UShort_t i=0;i<GetNumberOfDetectors();i++)
            DetList->at(i+1*GetNumberOfDetectors())->ReadCalibration(IF);
          IF = CleanDelete(IF);
        }
      else
        {
          Char_t Message[500];
          sprintf(Message,"<%s><%s> Could not find the calibration file %s", DetectorName, DetectorNameExt, Line);
          MErr * Er = new MErr(WhoamI,0,0, Message);
          throw Er;
        }
#ifdef GOCCE
      InOut = new stringstream();
      *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << "GOCCE" << ".cal";
      *InOut>>Line;
      InOut = CleanDelete(InOut);
      if((IF = CheckCalibration(Line)))
        {
          for(UShort_t i=0;i<GetNumberOfDetectors();i++)
            DetList->at(i+2*GetNumberOfDetectors())->ReadCalibration(IF);
          for(UShort_t i=0;i<GetNumberOfDetectors();i++)
            DetList->at(i+3*GetNumberOfDetectors())->ReadCalibration(IF);
          IF = CleanDelete(IF);
        }
      else
        {
          Char_t Message[500];
          sprintf(Message,"<%s><%s> Could not find the calibration file %s", DetectorName, DetectorNameExt, Line);
          MErr * Er = new MErr(WhoamI,0,0, Message);
          throw Er;
        }

      // Angles

    // InOut = new strstream();
    // *InOut  << getenv("ANALYSIS") << "/Calibs/GOCCE1.cal";
    // *InOut>>Line;
    // IF = new MyIFile(Line);
    // delete InOut;
    // cout << "Reading " << Line << endl;
    // L->File << "Reading " << Line << endl;
    // for(i=0;i<5;i++)
    //   {
    //     IF->GetLine(Line,Len);
    //     cout << Line << endl;
    //     L->File << Line << endl;
    //   }
    // for(i=0;i<16*16;i+=4)
    //   {
    //     IF->GetLine(Line,Len);
    //     InOut = new strstream();
    //     *InOut << Line;
    //     for(j=0;j<4;j++) 
    //       *InOut >> Ang_G[i+j][0] >> Ang_G[i+j][1];
    //     for(j=0;j<4;j++) 
    //       L->File <<  Ang_G[i+j][0] << " " << Ang_G[i+j][1]<< " ";
    //     L->File << " GOCCEA" << i/16 << endl; 
    //     for(k=0;k<4;k++)
    //       {
    //         L->File << i+k << " " << Ang_G[i+k][0] << " " << Ang_G[i+k][1] << endl;
    //       }

    //     for(k=0;k<4;k++)
    //       {
    //         for(Int_t j=0;j<2;j++)
    //           Ang_G[i+k][j] *= (TMath::Pi())/180.;
    //       }
    //     delete InOut;
    //   }
    // delete IF
#endif
    }
  END;
}




#ifdef WITH_ROOT
void Clover::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  SetMainHistogramFolder("");
  SetHistogramsCal1DFolder("Exo1D");
  SetHistogramsCal2DFolder("Exo2D");
  for(UShort_t i = 0;i<DetList->size();i++)
	 {
		DetList->at(i)->SetMainHistogramFolder("");
		DetList->at(i)->SetHistogramsRaw1DFolder("Exo1D");
		DetList->at(i)->SetHistogramsRaw2DFolder("Exo2D");
	 	DetList->at(i)->SetHistogramsCal1DFolder("Exo1D");
		DetList->at(i)->SetHistogramsCal2DFolder("Exo2D");
	 }
 
  if(fMode == MODE_WATCHER)
    {
		// if(fRawData)
		//   {
		// 	 SetHistogramsRaw(true);
		//   }
		// if(fCalData)
		//   {
		// 	 SetHistogramsCal(true);
		//   }
		// for(UShort_t i = 0;i<DetList->size();i++)
		//   {
		// 	  if(DetList->at(i)->HasRawData())
		// 		{
		// 		  if(i == 2)
		// 			 DetList->at(i)->SetHistogramsRawSummary(true);
		// 		  else
		// 			 DetList->at(i)->SetHistogramsRaw1D(true);
		// 		}
		// 	  if(DetList->at(i)->HasCalData())
		// 		{
		// 		  if(i == 2)
		// 			 DetList->at(i)->SetHistogramsCalSummary(true);
		// 		  else
		// 			 {
		// 				DetList->at(i)->SetHistogramsCal1D(true);
		// 			 }
		// 		}
		//   }
	 }
  else if(fMode == MODE_D2R)
	 {
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 if(DetList->at(i)->HasRawData())
				{
						DetList->at(i)->SetHistogramsRaw1D(true);
                        DetList->at(i)->SetOutAttachRawV(true);

				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);
		
	 }
  else if(fMode == MODE_D2A)
	 {
		if(fRawData)
		  {
			 SetHistogramsRaw(false);
			 SetOutAttachRawI(false);
			 SetOutAttachRawV(false);
		  }
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
			 SetOutAttachCalV(false);
		  }
		
		for(UShort_t i = 0;i<DetList->size();i++)
		  {			
			 if(DetList->at(i)->HasRawData())
				{
				  DetList->at(i)->SetHistogramsRaw1D(false);
				  DetList->at(i)->SetHistogramsRaw2D(true);
					 DetList->at(i)->SetHistogramsRawSummary(true);
				  DetList->at(i)->SetOutAttachRawI(false);
                  DetList->at(i)->SetOutAttachRawV(true);
				}
			 if(DetList->at(i)->HasCalData())
				{
						DetList->at(i)->SetHistogramsCal1D(false);
						DetList->at(i)->SetHistogramsCalSummary(true);
						DetList->at(i)->SetOutAttachCalV(true);			 
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		
		OutAttach(OutTTree);
	 }
  else if(fMode == MODE_R2A)
    {
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 if(i == 2) 
			 	DetList->at(i)->SetInAttachRawV(true);
			 else
            {
              DetList->at(i)->SetInAttachRawV(false);
              DetList->at(i)->SetInAttachRawI(true);
            }
			 
			 DetList->at(i)->InAttach(InTTree);
		  }
		
		if(fRawData)
		  {
			 SetHistogramsRaw(false);
			 SetOutAttachRawI(false);
			 SetOutAttachRawV(false);
		  }
		if(fCalData)
		  {
			 SetHistogramsCal(true);
			 SetOutAttachCalI(true);
			 SetOutAttachCalV(false);
		  }
	
		for(UShort_t i = 0;i<DetList->size();i++)
		  {
			 if(DetList->at(i)->HasCalData())
				{
				  if(i == 2)
					 {
						DetList->at(i)->SetHistogramsCal1D(false);
						DetList->at(i)->SetHistogramsCalSummary(true);
						DetList->at(i)->SetOutAttachCalV(true);			 
					 }
				  else
					 {
						DetList->at(i)->SetHistogramsCal1D(true);
						DetList->at(i)->SetOutAttachCalI(true);
					 }
				}
			 DetList->at(i)->OutAttach(OutTTree);
		  }
		OutAttach(OutTTree);		
    }
  else if (fMode == MODE_CALC)
    {
      SetNoOutput();
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }

  if(VerboseLevel >= V_INFO)
    PrintOptions(cout)  ;

  END;
}

#endif
