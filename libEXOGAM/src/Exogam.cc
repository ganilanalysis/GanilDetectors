/****************************************************************************
 *    Copyright (C) 2012-2024 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "Exogam.hh"
#define WITH_EXOGAM_REA 1

Exogam::Exogam(const Char_t * Name,
               UShort_t	NDetectors,
               Bool_t RawData,
               Bool_t CalData,
               Bool_t DefaultCalibration,
               const Char_t * NameExt)
    : BaseDetector(Name, NDetectors, false, CalData, DefaultCalibration,false,NameExt)
{
    START;

    cout << "    <" << DetectorName << "><" << DetectorNameExt << "> Setting Up Exogam for " << NDetectors << " Clovers"  << endl;
    L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> Setting Up Exogam for " << NDetectors << " Clovers"  << endl;
    cout << "    <" << DetectorName << "><" << DetectorNameExt << "> ECC_20MEV defined "  << endl;
    L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> ECC_20MEV defined "  << endl;
#ifdef EXOGAM_PARTNER
    cout << "    <" << DetectorName << "><" << DetectorNameExt << "> EXOGAM_PARTNER defined "  << endl;
    L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> EXOGAM_PARTNER defined "  << endl;
#endif
    cout << "    <" << DetectorName << "><" << DetectorNameExt << "> GOCCE defined "  << endl;
    L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> GOCCE defined "  << endl;
    cout << "    <" << DetectorName << "><" << DetectorNameExt << "> BGO defined "  << endl;
    L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> BGO defined "  << endl;
    cout << "    <" << DetectorName << "><" << DetectorNameExt << "> CSI defined "  << endl;
    L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> CSI defined "  << endl;

#ifdef ADDBACK
    cout << "    <" << DetectorName << "><" << DetectorNameExt << "> ADDBACK defined "  << endl;
    L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> ADDBACK defined "  << endl;
#endif
#ifdef ADDBACK_BGO
    cout << "    <" << DetectorName << "><" << DetectorNameExt << "> ADDBACK_BGO defined "  << endl;
    L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> ADDBACK_BGO defined "  << endl;
#endif
#ifdef ADDBACK_BGO_CSI
    cout << "    <" << DetectorName << "><" << DetectorNameExt << "> ADDBACK_BGO_CSI defined "  << endl;
    L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> ADDBACK_BGO_CSI defined "  << endl;
#endif

    DetManager *DM = DetManager::getInstance();

#ifdef EXO_VAMOS
    Reco = NULL;
    Id = NULL;
    TP = NULL;
    Reco = (Reconstruction*) DM->GetDetector("Reconstruction");
    TP = (TargetPos*) DM->GetDetector("TP");
    // Id = (Identification*) DM->GetDetector("Identification");
#endif
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;

    AllocateComponents();
    ReadAngles();
    END;
}


Exogam::~Exogam(void)
{
    START;


    FlangeMap = FreeDynamicArray<Int_t>(FlangeMap);

    // Free  Arrays
    if(fCalData)
    {

        //Pos_Outers = FreeDynamicArray<Float_t>(Pos_Outers,NumberOfDetectors*16);

        InN = FreeDynamicArray<UShort_t>(InN);
        InV = FreeDynamicArray<Float_t>(InV);
        InTS = FreeDynamicArray<ULong64_t>(InTS);
        InT = FreeDynamicArray<Float_t>(InT);
        InUsed = FreeDynamicArray<UShort_t>(InUsed);
        BGOH = FreeDynamicArray<UShort_t>(BGOH);
        CSIH = FreeDynamicArray<UShort_t>(CSIH);

        OuN = FreeDynamicArray<UShort_t>(OuN);
        OuV = FreeDynamicArray<Float_t>(OuV);
        OuTS = FreeDynamicArray<ULong64_t>(OuTS);
        OuUsed = FreeDynamicArray<UShort_t>(OuUsed);

        BGON = FreeDynamicArray<UShort_t>(BGON);
        BGOV = FreeDynamicArray<Float_t>(BGOV);
        BGOTS = FreeDynamicArray<ULong64_t>(BGOTS);

        CSIN = FreeDynamicArray<UShort_t>(CSIN);
        CSIV = FreeDynamicArray<Float_t>(CSIV);
        CSITS = FreeDynamicArray<ULong64_t>(CSITS);
#ifdef ADDBACK

        ClE_AB = FreeDynamicArray<Float_t>(ClE_AB);
        ClT_AB = FreeDynamicArray<Float_t>(ClT_AB);
        ClEDC_AB = FreeDynamicArray<Float_t>(ClEDC_AB);
        ClE_ABN = FreeDynamicArray<UShort_t>(ClE_ABN);
        ClE_ABTS  = FreeDynamicArray<ULong64_t>(ClE_ABTS);;
        ClA_AB = FreeDynamicArray<Float_t>(ClA_AB);
        ClE_SegMaxE_AB = FreeDynamicArray<Float_t>(ClE_SegMaxE_AB);
        ClE_ABSegMaxN = FreeDynamicArray<UShort_t>(ClE_ABSegMaxN);
        ClE_ABSegMaxT  = FreeDynamicArray<Float_t>(ClE_ABSegMaxT);
        ClE_ABSegMaxP  = FreeDynamicArray<Float_t>(ClE_ABSegMaxP);
        ClE_ABSegMaxX  = FreeDynamicArray<Float_t>(ClE_ABSegMaxX);
        ClE_ABSegMaxY  = FreeDynamicArray<Float_t>(ClE_ABSegMaxY);
        ClE_ABSegMaxZ  = FreeDynamicArray<Float_t>(ClE_ABSegMaxZ);
#endif
#ifdef ADDBACK_BGO
        ClE_AB_BGO = FreeDynamicArray<Float_t>(ClE_AB_BGO);
        ClE_AB_BGON = FreeDynamicArray<UShort_t>(ClE_AB_BGON);
        ClE_AB_BGOTS  = FreeDynamicArray<ULong64_t>(ClE_AB_BGOTS);
        ClA_AB_BGO = FreeDynamicArray<Float_t>(ClA_AB_BGO);
        ClEDC_AB_BGO = FreeDynamicArray<Float_t>(ClEDC_AB_BGO);
        ClE_AB_BGOSegMaxN = FreeDynamicArray<UShort_t>(ClE_AB_BGOSegMaxN);
        ClE_SegMaxE_AB_BGO = FreeDynamicArray<Float_t>(ClE_SegMaxE_AB_BGO);
#endif
#ifdef ADDBACK_BGO_CSI
        ClE_AB_BGO_CSI = FreeDynamicArray<Float_t>(ClE_AB_BGO_CSI);
        ClE_AB_BGO_CSIN  = FreeDynamicArray<UShort_t>(ClE_AB_BGO_CSIN);
        ClE_AB_BGO_CSITS  = FreeDynamicArray<ULong64_t>(ClE_AB_BGO_CSITS);
        ClA_AB_BGO_CSI = FreeDynamicArray<Float_t>(ClA_AB_BGO_CSI);
        ClEDC_AB_BGO_CSI = FreeDynamicArray<Float_t>(ClEDC_AB_BGO_CSI);
        ClE_AB_BGO_CSISegMaxN  = FreeDynamicArray<UShort_t>(ClE_AB_BGO_CSISegMaxN);
        ClE_AB_BGO_CSISegMaxT  = FreeDynamicArray<Float_t>(ClE_AB_BGO_CSISegMaxT);
        ClE_AB_BGO_CSISegMaxP  = FreeDynamicArray<Float_t>(ClE_AB_BGO_CSISegMaxP);
        ClE_AB_BGO_CSISegMaxX  = FreeDynamicArray<Float_t>(ClE_AB_BGO_CSISegMaxX);
        ClE_AB_BGO_CSISegMaxY  = FreeDynamicArray<Float_t>(ClE_AB_BGO_CSISegMaxY);
        ClE_AB_BGO_CSISegMaxZ  = FreeDynamicArray<Float_t>(ClE_AB_BGO_CSISegMaxZ);
        ClE_SegMaxE_AB_BGO_CSI = FreeDynamicArray<Float_t>(ClE_SegMaxE_AB_BGO_CSI);
#endif

    }
    END;
}


void Exogam::Clear()
{
    START;
    BaseDetector::Clear();
#ifdef ExtTSRef
    TSRef = 0;
#endif
    if(fCalData)
    {
        //New Event
        InM = 0;
        OuM = 0;
        BGOM = 0;
        CSIM = 0;

#ifdef ADDBACK
        for(Int_t i = 0; i< ClE_ABM; i++)
        {
            ClE_AB[i] = 0.;
            ClT_AB[i] = 0.;
            ClEDC_AB[i] = 0.;
            ClE_ABN[i] = 0;
            ClE_ABTS[i] = 0.;
            ClA_AB[i] = 0.;
            ClE_SegMaxE_AB[i] = 0.;
            ClE_ABSegMaxN[i] = 0;
            ClE_ABSegMaxT[i] = 0.;
            ClE_ABSegMaxP[i] = 0.;
            ClE_ABSegMaxX[i] = 0.;
            ClE_ABSegMaxY[i] = 0.;
            ClE_ABSegMaxZ[i] = 0.;
        }
        ClE_ABM = 0;
#endif
#ifdef ADDBACK_BGO
        for(Int_t i = 0; i< ClE_AB_BGOM; i++)
        {

            ClE_AB_BGO[i] = 0.;
            ClA_AB_BGO[i] = 0.;
            ClEDC_AB_BGO[i] = 0.;
            ClE_AB_BGOTS[i] = 0.;
            ClE_AB_BGON[i] = 0;
            ClE_SegMaxE_AB_BGO[i] = 0.;
            ClE_AB_BGOSegMaxN[i] = 0;
        }
        ClE_AB_BGOM = 0;
#endif
#ifdef ADDBACK_BGO_CSI
        for(Int_t i = 0; i< ClE_AB_BGO_CSIM; i++)
        {
            ClE_AB_BGO_CSIN[i] = 0;
            ClA_AB_BGO_CSI[i] = 0.;
            ClE_AB_BGO_CSITS[i] = 0;
            ClEDC_AB_BGO_CSI[i] = 0.;
            ClE_AB_BGO_CSI[i] = 0.;
            ClE_SegMaxE_AB_BGO_CSI[i] = 0.;
            ClE_AB_BGO_CSISegMaxN[i] = 0;
            ClE_AB_BGO_CSISegMaxT[i] = 0.;
            ClE_AB_BGO_CSISegMaxP[i] = 0.;
            ClE_AB_BGO_CSISegMaxX[i] = 0.;
            ClE_AB_BGO_CSISegMaxY[i] = 0.;
            ClE_AB_BGO_CSISegMaxZ[i] = 0.;


        }
        ClE_AB_BGO_CSIM = 0;
#endif
    }


    END;

}


void Exogam::AllocateComponents(void)
{
    START;
    Char_t Line[255];
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = true;
    Bool_t HasTime = true;

#ifdef ExtTSRef
    TSRef = 0;
    TSRefDetName = "";
#endif

    InRange = 6000;
    OuRange = 20000;
    BGORange = 2000*3;
    CSIRange = 2000*2;

    // Prompt Gate
    SetTSGates(-20,20);
    SetRefTSGates(-20,20);
    // FlangeMap
    FlangeMap = AllocateDynamicArray<Int_t>(NumberOfDetectors);

    if(fCalData)
    {
        // ANgles
        //Pos_Outers = AllocateDynamicArray<Float_t>(NumberOfDetectors*16,3);

        // Local Handling of Filtereed Data Vectors
        InM = 0;
        InN = AllocateDynamicArray<UShort_t>(NumberOfDetectors*4);
        InV = AllocateDynamicArray<Float_t>(NumberOfDetectors*4);
        InT = AllocateDynamicArray<Float_t>(NumberOfDetectors*4);
        InTS = AllocateDynamicArray<ULong64_t>(NumberOfDetectors*4);
        InUsed = AllocateDynamicArray<UShort_t>(NumberOfDetectors*4);
        BGOH = AllocateDynamicArray<UShort_t>(NumberOfDetectors*4);
        CSIH = AllocateDynamicArray<UShort_t>(NumberOfDetectors*4);

        OuM = 0;
        OuN = AllocateDynamicArray<UShort_t>(NumberOfDetectors*4*4);
        OuV = AllocateDynamicArray<Float_t>(NumberOfDetectors*4*4);
        OuTS = AllocateDynamicArray<ULong64_t>(NumberOfDetectors*4*4);
        OuUsed = AllocateDynamicArray<UShort_t>(NumberOfDetectors*4*4);

        BGOM = 0;
        BGON = AllocateDynamicArray<UShort_t>(NumberOfDetectors*4);
        BGOV = AllocateDynamicArray<Float_t>(NumberOfDetectors*4);
        BGOTS = AllocateDynamicArray<ULong64_t>(NumberOfDetectors*4);

        CSIM = 0;
        CSIN = AllocateDynamicArray<UShort_t>(NumberOfDetectors*4);
        CSIV = AllocateDynamicArray<Float_t>(NumberOfDetectors*4);
        CSITS = AllocateDynamicArray<ULong64_t>(NumberOfDetectors*4);

        // CLover Data
#ifdef ADDBACK
        ClE_ABM = 0;
        ClE_AB = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClT_AB = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_ABN = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        ClE_ABTS  = AllocateDynamicArray<ULong64_t>(NumberOfDetectors);;
        ClEDC_AB = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClA_AB = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_ABSegMaxN = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        ClE_ABSegMaxT  = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_ABSegMaxP  = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_ABSegMaxX  = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_ABSegMaxY  = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_ABSegMaxZ  = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_SegMaxE_AB = AllocateDynamicArray<Float_t>(NumberOfDetectors);
#endif
#ifdef ADDBACK_BGO
        ClE_AB_BGOM = 0;
        ClE_AB_BGO = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_AB_BGON = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        ClE_AB_BGOTS  = AllocateDynamicArray<ULong64_t>(NumberOfDetectors);
        ClEDC_AB_BGO = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClA_AB_BGO = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_AB_BGOSegMaxN = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        ClE_SegMaxE_AB_BGO = AllocateDynamicArray<Float_t>(NumberOfDetectors);
#endif
#ifdef ADDBACK_BGO_CSI
        ClE_AB_BGO_CSIM = 0;
        ClE_AB_BGO_CSI = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_AB_BGO_CSIN  = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        ClE_AB_BGO_CSITS  = AllocateDynamicArray<ULong64_t>(NumberOfDetectors);
        ClEDC_AB_BGO_CSI = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClA_AB_BGO_CSI = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_AB_BGO_CSISegMaxN  = AllocateDynamicArray<UShort_t>(NumberOfDetectors);
        ClE_AB_BGO_CSISegMaxT  = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_AB_BGO_CSISegMaxP  = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_AB_BGO_CSISegMaxX  = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_AB_BGO_CSISegMaxY  = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_AB_BGO_CSISegMaxZ  = AllocateDynamicArray<Float_t>(NumberOfDetectors);
        ClE_SegMaxE_AB_BGO_CSI = AllocateDynamicArray<Float_t>(NumberOfDetectors);
#endif
        for(Int_t i = 0; i< NumberOfDetectors*4; i++)
        {
            InN[i] = 0;
            InV[i] = 0.0;
            InT[i] = 0.0;
            InTS[i] = 0;
            InUsed[i] = 0;
            BGOH[i] = 0;
            CSIH[i] = 0;
        }
        InM = 0;
        for(Int_t i = 0; i< NumberOfDetectors*4*4; i++)
        {
            OuN[i] = 0;
            OuV[i] = 0.0;
            OuTS[i] = 0;
            OuUsed[i] = 0;

        }
        OuM = 0;

        for(Int_t i = 0; i< NumberOfDetectors*4; i++)
        {
            BGON[i] = 0;
            BGOV[i] = 0.0;
            BGOTS[i] = 0;
        }
        BGOM = 0;

        for(Int_t i = 0; i< NumberOfDetectors*4; i++)
        {
            CSIN[i] = 0;
            CSIV[i] = 0.0;
            CSITS[i] = 0;
        }
        CSIM = 0;

        for(Int_t i = 0; i< NumberOfDetectors; i++)
        {
#ifdef ADDBACK
            ClE_AB[i] = 0.;
            ClT_AB[i] = 0.;
            ClEDC_AB[i] = 0.;
            ClA_AB[i] = 0.;
            ClE_ABN[i] = 0.;
            ClE_ABTS[i] = 0.;
            ClE_SegMaxE_AB[i] = 0.;
            ClE_ABSegMaxN[i] = 0.;
            ClE_ABSegMaxT[i] = 0;
            ClE_ABSegMaxP[i] = 0;
            ClE_ABSegMaxX[i] = 0;
            ClE_ABSegMaxY[i] = 0;
            ClE_ABSegMaxZ[i] = 0;
#endif
#ifdef ADDBACK_BGO
            ClE_AB_BGO[i] = 0.;
            ClEDC_AB_BGO[i] = 0.;
            ClA_AB_BGO[i] = 0.;
            ClE_AB_BGON[i] = 0.;
            ClE_AB_BGOTS[i] = 0.;
            ClE_AB_BGOSegMaxN[i] = 0.;
            ClE_SegMaxE_AB_BGO[i] = 0.;
#endif
#ifdef ADDBACK_BGO_CSI
            ClEDC_AB_BGO_CSI[i] = 0.;
            ClA_AB_BGO_CSI[i] = 0.;
            ClE_AB_BGO_CSIN[i] = 0;
            ClE_AB_BGO_CSITS[i] = 0.;
            ClE_AB_BGO_CSI[i] = 0.;
            ClE_SegMaxE_AB_BGO_CSI[i] = 0.;
            ClE_AB_BGO_CSISegMaxN[i] = 0;
            ClE_AB_BGO_CSISegMaxT[i] = 0;
            ClE_AB_BGO_CSISegMaxP[i] = 0;
            ClE_AB_BGO_CSISegMaxX[i] = 0;
            ClE_AB_BGO_CSISegMaxY[i] = 0;
            ClE_AB_BGO_CSISegMaxZ[i] = 0;
#endif
        }
#ifdef ADDBACK
        ClE_ABM = 0;
#endif
#ifdef ADDBACK_BGO
        ClE_AB_BGOM = 0;
#endif
#ifdef ADDBACK_BGO_CSI
        ClE_AB_BGO_CSIM = 0;
#endif

    }

    BaseDetector* Inner6M = new BaseDetector("Inner6M",4*NumberOfDetectors,RawData,CalData,DefCal,false,"",false,true,fMaxMultSubDetectors,HasTime);
    AddComponent(Inner6M);
    Det_ECC_Offset = DetList->size()-1;

    Det_ECC20_Offset = DetList->size();
    BaseDetector* Inner20M = new BaseDetector("Inner20M",4*NumberOfDetectors,RawData,CalData,DefCal,false,"",false,true,fMaxMultSubDetectors,HasTime);
    AddComponent(Inner20M);

#ifndef WITH_EXOGAM_REA
    // BaseDetector* DeltaT = new BaseDetector("DeltaT",4*NumberOfDetectors,RawData,CalData,DefCal,false,"",false,true,fMaxMultSubDetectors,HasTime);
    // AddComponent(DeltaT);
#endif
    Det_GOCCE_Offset = DetList->size();
    BaseDetector* Outers = new BaseDetector("Outers",16*NumberOfDetectors,RawData,CalData,DefCal,false,"",false,true,fMaxMultSubDetectors,HasTime);
    AddComponent(Outers);

    Det_BGO_Offset = DetList->size();
    BaseDetector* BGO= new BaseDetector("BGO",4*NumberOfDetectors,RawData,CalData,DefCal,false,"",false,true,fMaxMultSubDetectors,HasTime);
    AddComponent(BGO);

    Det_CSI_Offset = DetList->size();
    BaseDetector* CSI= new BaseDetector("CSI",4*NumberOfDetectors,RawData,CalData,DefCal,false,"",false,true,fMaxMultSubDetectors,HasTime);
    AddComponent(CSI);
    NumberOfSubDetectors = DetList->size();

    END;
}

void Exogam::SetParametersNUMEXO(NUMEXOParameters* PL,Map* Map)
{
    START;
    SetParameters(NULL,Map);
    END;
}

void Exogam::SetParameters(Parameters* Par,Map* Map)
{
    START;
    Char_t A[4] = {'A','B','C','D'};
    Char_t PName[20];
    if(isComposite)
    {
        if(fRawDataSubDetectors)
        {
            for(UShort_t i=0;i<NumberOfDetectors; i++)
            {
                for(UShort_t j=0;j<4;j++)
                {

                    sprintf(PName,"Inner6M_Fl%02d_%c",FlangeMap[i],A[j]);
                    DetList->at(Det_ECC_Offset)->SetParameterName(PName, 4*i+j);

                    sprintf(PName,"Inner20M_Fl%02d_%c",FlangeMap[i],A[j]);
                    DetList->at(Det_ECC20_Offset)->SetParameterName(PName, 4*i+j);

#ifndef WITH_EXOGAM_REA
                    sprintf(PName,"DeltaT_Fl%02d_%c",FlangeMap[i],A[j]);
                    DetList->at(Det_ECC20_Offset+1)->SetParameterName(PName, 4*i+j);
#endif
                    for(UShort_t k=0;k<4;k++)
                    {
                        sprintf(PName,"Outer%d_Fl%02d_%c",k+1,FlangeMap[i],A[j]);
                        DetList->at(Det_GOCCE_Offset)->SetParameterName(PName, i*16+j*4+k);
                    }

                    sprintf(PName,"BGO_Fl%02d_%c",FlangeMap[i],A[j]);
                    DetList->at(Det_BGO_Offset)->SetParameterName(PName, 4*i+j);

                    sprintf(PName,"CSI_Fl%02d_%c",FlangeMap[i],A[j]);
                    DetList->at(Det_CSI_Offset)->SetParameterName(PName, 4*i+j);

                }
            }

            NUMEXOParameters * PL_NUMEX = NUMEXOParameters::getInstance();
            for(UShort_t i=0;i<DetList->size();i++)
                {
                DetList->at(i)->SetParametersNUMEXO(PL_NUMEX, Map);
            }
        }
    }
    END;
}



Bool_t Exogam::Treat(void)
{
    START;
    Ctr->at(0)++;
#ifdef WITH_ROOT
    TVector3 Vec(1,1,1);
#endif
    Float_t Theta=0.0;
    Float_t Phi=0.0;

    Float_t ThetaV=0.0;
    Float_t PhiV=0.0;

    if(isComposite)
    {
        for(UShort_t i=0; i< DetList->size(); i++)
	  {
	    DetList->at(i)->Treat();
	  }
    }

    //return 1;

#ifdef ExtTSRef
    //TSRef = GetTS();
#endif
    if(fCalData)
    {
        UShort_t Cl1;
        UShort_t Cr1;
        UShort_t Sg1;
        UShort_t Cl2;
        UShort_t Cr2;
        UShort_t Sg2;


         for(Int_t i = 0; i< DetList->at(Det_ECC_Offset)->GetM()  && i < NumberOfDetectors*4; i++)
         {
	   //	   cout << DetList->at(Det_ECC_Offset)->GetCalAt(i) << endl;
	   if(DetList->at(Det_ECC_Offset)->GetCalAt(i) > 10 && DetList->at(Det_ECC_Offset)->GetCalAt(i) < InRange && DetList->at(Det_ECC_Offset)->GetCalTSAt(i) > 0 )
             {
#ifdef ExtTSRef
                if(CheckTSRef(DetList->at(Det_ECC_Offset)->GetCalTSAt(i),TSRef, RefTSGate[0],RefTSGate[1]))
#endif
                {
                    InV[InM] = DetList->at(Det_ECC_Offset)->GetCalAt(i);
                    InN[InM] = DetList->at(Det_ECC_Offset)->GetNrAt(i);
                    InTS[InM] = DetList->at(Det_ECC_Offset)->GetCalTSAt(i);
#ifndef WITH_EXOGAM_REA
                    InT[InM] = DetList->at(Det_ECC_Offset+2)->GetCalAt(i);
#else
//                    cout << "Get Time ?" << endl;
//                    cout << i << " " << DetList->at(Det_ECC_Offset)->GetRawTimeAt(i) << endl;;
                    InT[InM] = DetList->at(Det_ECC_Offset)->GetRawTimeAt(i);
//                    InT[InM] = DetList->at(Det_ECC_Offset+2)->GetCalAt(i);

#endif
                    InUsed[InM] = 0;
                    BGOH[InM] = 0;
                    CSIH[InM] = 0;
                    InM++;
                }
             }
         }

        for(Int_t i = 0; i< DetList->at(Det_GOCCE_Offset)->GetM() && i < NumberOfDetectors*4*4; i++)
        {
            if(DetList->at(Det_GOCCE_Offset)->GetCalAt(i)> 10 && DetList->at(Det_GOCCE_Offset)->GetCalAt(i) < OuRange && DetList->at(Det_GOCCE_Offset)->GetCalTSAt(i) > 0)
            {
#ifdef ExtTSRef
                if(CheckTSRef(DetList->at(Det_ECC_Offset)->GetCalTSAt(i),TSRef, RefTSGate[0],RefTSGate[1]))
#endif
                {
                    OuV[OuM] = DetList->at(Det_GOCCE_Offset)->GetCalAt(i);
                    OuN[OuM] = DetList->at(Det_GOCCE_Offset)->GetNrAt(i);
                    OuTS[OuM] = DetList->at(Det_GOCCE_Offset)->GetCalTSAt(i);
                    OuUsed[OuM] = 0;
                    //cout << "Out " << OuM << " " <<  OuN[OuM] << " " << DetList->at(Det_GOCCE_Offset)->GetNrAt(i) <<  endl;
                    OuM++;
                }
            }
        }

        for(Int_t i = 0; i< DetList->at(Det_BGO_Offset)->GetM() && i < NumberOfDetectors*4; i++)
        {
            if(DetList->at(Det_BGO_Offset)->GetCalAt(i) > 10 && DetList->at(Det_BGO_Offset)->GetCalAt(i)  < BGORange && DetList->at(Det_BGO_Offset)->GetCalTSAt(i) > 0)
            {
#ifdef ExtTSRef
                if(CheckTSRef(DetList->at(Det_ECC_Offset)->GetCalTSAt(i),TSRef, RefTSGate[0],RefTSGate[1]))
#endif
                {                BGOV[BGOM] = DetList->at(Det_BGO_Offset)->GetCalAt(i);
                    BGON[BGOM] = DetList->at(Det_BGO_Offset)->GetNrAt(i);
                    BGOTS[BGOM] = DetList->at(Det_BGO_Offset)->GetCalTSAt(i) ;
                    // cout << "BGO " << BGOM << " " <<  BGON[BGOM] << " " << BGORawNr[i] <<  endl;
                    //  cout << "BGO " << BGOM << " " <<  BGON[BGOM] << " " <<  Cl1 <<  endl;
                    BGOM++;
                }
            }
        }

        for(Int_t i = 0; i< DetList->at(Det_CSI_Offset)->GetM() && i < NumberOfDetectors*4; i++)
        {
            if(DetList->at(Det_CSI_Offset)->GetCalAt(i) > 10 && DetList->at(Det_CSI_Offset)->GetCalAt(i)  < CSIRange && DetList->at(Det_CSI_Offset)->GetCalTSAt(i) > 0)
            {
#ifdef ExtTSRef
                if(CheckTSRef(DetList->at(Det_ECC_Offset)->GetCalTSAt(i),TSRef, RefTSGate[0],RefTSGate[1]))
#endif
                {
                    CSIV[CSIM] = DetList->at(Det_CSI_Offset)->GetCalAt(i);
                    CSIN[CSIM] = DetList->at(Det_CSI_Offset)->GetNrAt(i) ;
                    CSITS[CSIM] = DetList->at(Det_CSI_Offset)->GetCalTSAt(i) ;
                    // cout << "CSI " << CSIM << " " <<  CSIN[CSIM] << " " << CSIRawNr[i] <<  endl;
                    CSIM++;
                }
            }
        }


        for(Int_t i = 0; i< InM; i++)
        {
            Cl1 = InN[i]/4;
            Cr1 = InN[i]%4;
            for(Int_t j = 0; j< BGOM; j++)
            {
                Cl2 = BGON[j]/4;
                Cr2 = BGON[j]%4;
                if(Cl1 == Cl2)// && Cr1 == Cr2)
                {
                    if(
                            ((Int_t) (InTS[i] - BGOTS[j])) >= TSGate[0]
                            &&
                            ((Int_t) (InTS[i] - BGOTS[j])) <= TSGate[1]
                            )
                    {
                        BGOH[i] = 1;
                    }
                }
            }
            for(Int_t j = 0; j< CSIM; j++)
            {
                Cl2 = CSIN[j]/4;
                Cr2 = CSIN[j]%4;
                if(Cl1 == Cl2)// && Cr1 == Cr2)
                {
                    if(
                            ((Int_t) (InTS[i] - CSITS[j])) >= TSGate[0]
                            &&
                            ((Int_t) (InTS[i] - CSITS[j])) <= TSGate[1]
                            )
                    {
                        CSIH[i] = 1;
                    }
                }
            }
        }




        Int_t nWhileLoops;
        Int_t CrystalsUsed;
        Int_t OutersUsed;
        //  cout << "\nEvent " << InM << " crystals" << endl;

#ifdef ADDBACK
        // AddBack
        CrystalsUsed = 0;

        for(Int_t i = 0; i< InM; i++)
        {
            InUsed[i] = 0;
        }
        // OuterCounter
        OutersUsed = 0;

        for(Int_t i = 0; i< OuM; i++)
        {
            OuUsed[i] = 0;
        }

        nWhileLoops = 0;
        if(InM>0)
            do{
            for(Int_t i = 0; i< InM; i++)
            {
                //                cout << "=========================== \nCheck AB " <<  InN[i]/4 << " " << InN[i]%4 << " " << InUsed[i] << endl;
                if(InUsed[i] == 0)
                {
                    Cl1 = InN[i]/4;
                    Cr1 = InN[i]%4;
                    if(ClE_ABTS[ClE_ABM] == 0) //First Crystal gives TS
                    {
                        ClE_ABTS[ClE_ABM] = InTS[i];
                        ClE_AB[ClE_ABM] = InV[i];
                        ClE_ABN[ClE_ABM] = FlangeMap[Cl1];
                        InUsed[i] = 1;
                        CrystalsUsed++;

                        for(Int_t j = 0; j< OuM; j++)
                        {

                            Cl2 = OuN[j]/16;
                            Cr2 = (OuN[j]-Cl2*16)/4;

                            if(OuUsed[j] == 0)
                            {

                                if((ClE_ABN[ClE_ABM] == Cl2) && (Cr1 == Cr2))
                                {
                                    if(((Int_t) (OuTS[j] - ClE_ABTS[ClE_ABM]) >= TSGate[0])
                                            &&
                                            ((Int_t) (OuTS[j] - ClE_ABTS[ClE_ABM]) <= TSGate[1]
                                             ))
                                    {

                                        OuUsed[j] = 1;
                                        OutersUsed++;
                                        if(OuV[j] >  ClE_SegMaxE_AB[ClE_ABM])
                                        {
                                            ClE_SegMaxE_AB[ClE_ABM] = OuV[j];
                                            ClE_ABSegMaxN[ClE_ABM] = OuN[j];
                                            ClT_AB[ClE_ABM] = InT[i];
                                        }

                                    }

                                }
                            }
                            //  cout << "<"<<j << "> : Cl_Outer " << Cl2 << " Cr_Outer " << Cr2 << " SegN " << OuN[j] << " -> Used : "<< OuUsed[j] <<  " Outer " << OuV[j] << "/"<< ClE_SegMaxE_AB[ClE_ABM] << "  DetalTS: " << ((Int_t) (OuTS[j]- ClE_ABTS[ClE_ABM])) <<  endl;

                        }
                    }
                    else
                    {
                        if(
                                FlangeMap[Cl1] == ClE_ABN[ClE_ABM]
                                &&
                                ((Int_t) (InTS[i] - ClE_ABTS[ClE_ABM])) >= TSGate[0]
                                &&
                                ((Int_t) (InTS[i] - ClE_ABTS[ClE_ABM])) <= TSGate[1]
                                ) // If in the same Clover and in TSGate AddBack
                        {
                            ClE_AB[ClE_ABM] += InV[i];
                            InUsed[i] = 1;
                            CrystalsUsed++;

                            for(Int_t j = 0; j< OuM; j++)
                            {

                                Cl2 = OuN[j]/16;
                                Cr2 = (OuN[j]-Cl2*16)/4;

                                if(OuUsed[j] == 0)
                                {

                                    if((ClE_ABN[ClE_ABM] == Cl2) && (Cr1 == Cr2))
                                    {
                                        if(((Int_t) (OuTS[j] - ClE_ABTS[ClE_ABM]) >= TSGate[0])
                                                &&
                                                ((Int_t) (OuTS[j] - ClE_ABTS[ClE_ABM]) <= TSGate[1]
                                                 ))
                                        {

                                            OuUsed[j] = 1;
                                            OutersUsed++;
                                            if(OuV[j] >  ClE_SegMaxE_AB[ClE_ABM])
                                            {
                                                ClE_SegMaxE_AB[ClE_ABM] = OuV[j];
                                                ClE_ABSegMaxN[ClE_ABM] = OuN[j];
                                                ClT_AB[ClE_ABM] = InT[i];
					    }

                                        }

                                    }
                                }
                                //   cout << "<"<<j << "> : Cl_Outer " << Cl2 << " Cr_Outer " << Cr2 << " SegN " << OuN[j] << " -> Used : "<< OuUsed[j] <<  " Outer " << OuV[j] << "/"<< ClE_SegMaxE_AB[ClE_ABM] << "  DetalTS: " << ((Int_t) (OuTS[j]- ClE_ABTS[ClE_ABM])) <<  endl;

                            }

                            //    cout << "Added Cl " << Cl << " Flange "  << FlangeMap[Cl1] << " Cr " << Cr1 << " TDiff " << ((Int_t) (InTS[i] - ClE_ABTS[ClE_ABM])) << endl;
                        }
                        else
                        {
                        }
                    }
                }
            }
            if(ClE_ABTS[ClE_ABM] > 0)
            {
                //    cout << "Cl " << ClE_ABN[ClE_ABM] << " ref " << ClE_ABTS[ClE_ABM] << "Max Seg N " << ClE_ABSegMaxN[ClE_ABM] << endl;
                if(ClE_ABM>15)
                {
		  cout << " Cannot Go Beyond " << ClE_ABM << " / " << NumberOfDetectors << endl;
                }
                ClE_ABM++;
                //    cout << endl;

            }
            else
            {
                cout << "ADDBACK: I didn't find a new clover !" << endl;
                cout << "ADDBACK: CrystalsUsed " << CrystalsUsed << " out of " << InM << endl;
            }

            //  if(InM>0) cout << "CrystalsUsed " << CrystalsUsed << " " << InM  << " " << ClE_ABM << endl;
            nWhileLoops++;  // One while loop is one Clover !
            if(nWhileLoops == NumberOfDetectors)
            {
                cout << "ADDBACK: Reached " <<  nWhileLoops << " for " << NumberOfDetectors << " of possible Clovers" << endl;
            }
        }
        while(CrystalsUsed < InM && ClE_ABM < NumberOfDetectors && nWhileLoops < NumberOfDetectors);

#ifdef WITH_ROOT
        for(UShort_t k=0;k <ClE_ABM; k++)
        {
//                        cout << k << "/"<< ClE_ABM << " Cl #" << ClE_ABN[k] << " E = " << ClE_AB[k] << " keV -  MaxSeg : " << ClE_ABSegMaxN[k] << endl;
//                            cout << "\t  X: " << Pos_Outers[ClE_ABSegMaxN[k]][0] << endl;
//                            cout         << " Y: " << Pos_Outers[ClE_ABSegMaxN[k]][1]<< endl;
//                            cout         << " Z: " << Pos_Outers[ClE_ABSegMaxN[k]][2] << endl;
//                            cout << " Theta : " << Vec.Theta()*TMath::RadToDeg() << " Phi: " << Vec.Phi()*TMath::RadToDeg() << endl;
            Vec.SetXYZ(Pos_Outers[ClE_ABSegMaxN[k]][0],Pos_Outers[ClE_ABSegMaxN[k]][1],Pos_Outers[ClE_ABSegMaxN[k]][2]);

            Theta = Vec.Theta();
            Phi = Vec.Phi();


            ClE_ABSegMaxT[k] = Theta;
            ClE_ABSegMaxP[k] = Phi;
            ClE_ABSegMaxX[k] = Pos_Outers[ClE_ABSegMaxN[k]][0];
            ClE_ABSegMaxY[k] = Pos_Outers[ClE_ABSegMaxN[k]][1];
            ClE_ABSegMaxZ[k] = Pos_Outers[ClE_ABSegMaxN[k]][2];
#ifdef EXO_VAMS
            if(TP->GetTheta()>-1500 && TP->GetPhi()>-1500)
            {
                ThetaV = (TP->GetTheta())/1000.;
                PhiV =(TP->GetPhi())/1000.;
            }
            else
            {
                ThetaV = 0 ;//(TP->GetTheta())/1000.;
                PhiV = 0; //(TP->GetPhi())/1000.;
            }
#else
                ThetaV = 0 ;//(TP->GetTheta())/1000.;
                PhiV = 0; //(TP->GetPhi())/1000.;
#endif

            Float_t Beta = 0.02; // Id->Beta;
            ClA_AB[k] = CosAlpha(Theta,Phi,ThetaV,PhiV);;
            //if(Beta> 0 && ThetaV >)
            ClEDC_AB[k] = DopplerCorrection(ClE_AB[k],Beta,ClA_AB[k]);

            //cout<< ClE_ABN[k] << " " << ClE_ABSegMaxN[k] << " " << ClE_ABSegMaxN[k]/16 << " " << ClT_AB[k] << endl;
            //                cout << "DC : " << ClEDC_AB[k] << " keV"<<endl;
        }
#endif
#endif

#ifdef ADDBACK_BGO

        // AddBack
        CrystalsUsed = 0;

        for(Int_t i = 0; i< InM; i++)
        {
            InUsed[i] = 0;
        }
        // OuterCounter
        OutersUsed = 0;

        for(Int_t i = 0; i< OuM; i++)
        {
            OuUsed[i] = 0;
        }
        nWhileLoops = 0;
        if(InM>0)
            do{
            for(Int_t i = 0; i< InM; i++)
            {
                //     cout << "Check AB_BGO " <<  InN[i]/4 << " " << InN[i]%4 << " " << InUsed[i] << " " << BGOH[i]  << endl;
                if(BGOH[i] == 1)
                {
                    InUsed[i] = 1;
                    CrystalsUsed++;
                }
                if(InUsed[i] == 0)
                {
                    Cl1 = InN[i]/4;
                    Cr1 = InN[i]%4;
                    if(ClE_AB_BGOTS[ClE_AB_BGOM] == 0) //First Crystal gives TS
                    {
                        ClE_AB_BGOTS[ClE_AB_BGOM] = InTS[i];
                        ClE_AB_BGO[ClE_AB_BGOM] = InV[i];
                        ClE_AB_BGON[ClE_AB_BGOM] = FlangeMap[Cl1];
                        InUsed[i] = 1;
                        CrystalsUsed++;

                        for(Int_t j = 0; j< OuM; j++)
                        {

                            Cl2 = OuN[j]/16;
                            Cr2 = (OuN[j]-Cl2*16)/4;

                            if(OuUsed[j] == 0)
                            {

                                if((ClE_AB_BGON[ClE_AB_BGOM] == FlangeMap[Cl2]) && (Cr1 == Cr2))
                                {
                                    if(((Int_t) (OuTS[j] - ClE_AB_BGOTS[ClE_AB_BGOM]) >= TSGate[0])
                                            &&
                                            ((Int_t) (OuTS[j] - ClE_AB_BGOTS[ClE_AB_BGOM]) <= TSGate[1]
                                             ))
                                    {

                                        OuUsed[j] = 1;
                                        OutersUsed++;
                                        if(OuV[j] >  ClE_SegMaxE_AB_BGO[ClE_AB_BGOM])
                                        {
                                            ClE_SegMaxE_AB_BGO[ClE_AB_BGOM] = OuV[j];
                                            ClE_AB_BGOSegMaxN[ClE_AB_BGOM] = OuN[j];
                                        }

                                    }

                                }
                            }
                            //        cout << "<"<<j << "> : Cl_Outer " << Cl2 << " Cr_Outer " << Cr2 << " SegN " << OuN[j] << " -> Used : "<< OuUsed[j] <<  " Outer " << OuV[j] << "/"<< ClE_SegMaxE_AB_BGO[ClE_AB_BGOM] << "  DetalTS: " << ((Int_t) (OuTS[j]- ClE_AB_BGOTS[ClE_AB_BGOM])) <<  endl;

                        }

                        //   cout << "Cl " << Cl1 << " Cr " << Cr1 << " ref " << ClE_AB_BGOTS[ClE_AB_BGOM] << endl;
                    }
                    else
                    {
                        if(
                                FlangeMap[Cl1] == ClE_AB_BGON[ClE_AB_BGOM]
                                &&
                                ((Int_t) (InTS[i] - ClE_AB_BGOTS[ClE_AB_BGOM])) >= TSGate[0]
                                &&
                                ((Int_t) (InTS[i] - ClE_AB_BGOTS[ClE_AB_BGOM])) <= TSGate[1]
                                ) // If in the same Clover and in TSGate AddBack
                        {
                            ClE_AB_BGO[ClE_AB_BGOM] += InV[i];
                            InUsed[i] = 1;
                            CrystalsUsed++;
                            for(Int_t j = 0; j< OuM; j++)
                            {

                                Cl2 = OuN[j]/16;
                                Cr2 = (OuN[j]-Cl2*16)/4;

                                if(OuUsed[j] == 0)
                                {

                                    if((ClE_AB_BGON[ClE_AB_BGOM] == FlangeMap[Cl2]) && (Cr1 == Cr2))
                                    {
                                        if(((Int_t) (OuTS[j] - ClE_AB_BGOTS[ClE_AB_BGOM]) >= TSGate[0])
                                                &&
                                                ((Int_t) (OuTS[j] - ClE_AB_BGOTS[ClE_AB_BGOM]) <= TSGate[1]
                                                 ))
                                        {

                                            OuUsed[j] = 1;
                                            OutersUsed++;
                                            if(OuV[j] >  ClE_SegMaxE_AB_BGO[ClE_AB_BGOM])
                                            {
                                                ClE_SegMaxE_AB_BGO[ClE_AB_BGOM] = OuV[j];
                                                ClE_AB_BGOSegMaxN[ClE_AB_BGOM] = OuN[j];
                                            }

                                        }

                                    }
                                }
                                //     cout << "<"<<j << "> : Cl_Outer " << Cl2 << " Cr_Outer " << Cr2 << " SegN " << OuN[j] << " -> Used : "<< OuUsed[j] <<  " Outer " << OuV[j] << "/"<< ClE_SegMaxE_AB_BGO[ClE_AB_BGOM] << "  DetalTS: " << ((Int_t) (OuTS[j]- ClE_AB_BGOTS[ClE_AB_BGOM])) <<  endl;

                            }
                            //		cout << "Added Cl " << Cl1 << " Cr " << Cr1 << " TDiff " << ((Int_t) (InTS[i] - ClE_AB_BGOTS[ClE_AB_BGOM])) << endl;
                        }
                        else
                        {

                        }
                    }
                }
            }
            if(ClE_AB_BGOTS[ClE_AB_BGOM] > 0)
            {
                if(ClE_AB_BGOM>15)
                {
                    cout << " Cannot Go Beyond " << ClE_AB_BGOM << " / " << NumberOfDetectors << endl;
                }
                ClE_AB_BGOM++;
                //	    cout << endl;
            }
            else
            {
                cout << "ADDBACK_BGO: I didn't find a new clover !" << endl;
                cout << "ADDBACK_BGO: CrystalsUsed " << CrystalsUsed << " out of " << InM << endl;
                if(CrystalsUsed == InM)
                    cout << "ADDBACK_BGO: Maybe all remaining crystals were suppressed" << endl;
                else if(CrystalsUsed < InM)
                {
                    cout << "ADDBACK_BGO: CrystalsUsed " << CrystalsUsed << " smaller than " << InM << endl;
                }

            }

            //	if(InM>0) cout << "CrystalsUsed " << CrystalsUsed << " " << InM  << " " << ClE_AB_BGOM << endl;
            nWhileLoops++;  // One while loop is one Clover !
            if(nWhileLoops == NumberOfDetectors)
            {
                cout << "ADDBACK_BGO: Reached " <<  nWhileLoops << " for " << NumberOfDetectors << " of possible Clovers" << endl;
            }

        }
        while(CrystalsUsed < InM && ClE_AB_BGOM < NumberOfDetectors && nWhileLoops < NumberOfDetectors);

#ifdef WITH_ROOT
        for(UShort_t k=0;k <ClE_AB_BGOM; k++)
        {
            Vec.SetXYZ(Pos_Outers[ClE_AB_BGOSegMaxN[k]][0],Pos_Outers[ClE_AB_BGOSegMaxN[k]][1],Pos_Outers[ClE_AB_BGOSegMaxN[k]][2]);
            //            cout << k << "/"<< ClE_AB_BGOM << " Cl #" << ClE_AB_BGON[k] << " E = " << ClE_AB_BGO[k] << " keV -  MaxSeg : " << ClE_AB_BGOSegMaxN[k] << endl;
            //                cout << "\t  X: " << Pos_Outers[ClE_AB_BGOSegMaxN[k]][0]
            //                        << " Y: " << Pos_Outers[ClE_AB_BGOSegMaxN[k]][1]
            //                        << " Z: " << Pos_Outers[ClE_AB_BGOSegMaxN[k]][2] << endl;
            //                cout << " Theta : " << Vec.Theta()*TMath::RadToDeg() << " Phi: " << Vec.Phi()*TMath::RadToDeg() << endl;

            Theta = Vec.Theta();
            Phi = Vec.Phi();
            ThetaV = 0 ;//(TP->GetTheta())/1000.;
            PhiV = 0; //(TP->GetPhi())/1000.;
            Float_t Beta = 0.02; // Id->Beta;
            ClA_AB_BGO[k] = CosAlpha(Theta,Phi,ThetaV,PhiV);;
            //if(Beta> 0 && ThetaV >)
            ClEDC_AB_BGO[k] = DopplerCorrection(ClE_AB_BGO[k],Beta,ClA_AB_BGO[k]);

            //                cout << "DC : " << ClEDC_AB_BGO[k] << " keV"<<endl;
        }
#endif
#endif
#ifdef ADDBACK_BGO_CSI
        // AddBack
        CrystalsUsed = 0;

        for(Int_t i = 0; i< InM; i++)
        {
            InUsed[i] = 0;
        }
        // OuterCounter
        OutersUsed = 0;

        for(Int_t i = 0; i< OuM; i++)
        {
            OuUsed[i] = 0;
        }
        nWhileLoops = 0;
        if(InM>0)
            do{
            for(Int_t i = 0; i< InM; i++)
            {
                //    cout << "Check AB_BGO_CSI " <<  InN[i]/4 << " " << InN[i]%4 << " " << InUsed[i] << " " << BGOH[i] << " " << CSIH[i]<< endl;
                if(CSIH[i] == 1 || BGOH[i] == 1)
                {
                    InUsed[i] = 1;
                    CrystalsUsed++;
                }
                if(InUsed[i] == 0)
                {
                    Cl1 = InN[i]/4;
                    Cr1 = InN[i]%4;
                    if(ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM] == 0) //First Crystal gives TS
                    {
                        ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM] = InTS[i];
                        ClE_AB_BGO_CSI[ClE_AB_BGO_CSIM] = InV[i];
                        ClE_AB_BGO_CSIN[ClE_AB_BGO_CSIM] = Cl1;
                        InUsed[i] = 1;
                        CrystalsUsed++;
                        for(Int_t j = 0; j< OuM; j++)
                        {

                            Cl2 = OuN[j]/16;
                            Cr2 = (OuN[j]-Cl2*16)/4;

                            if(OuUsed[j] == 0)
                            {

                                if((ClE_AB_BGO_CSIN[ClE_AB_BGO_CSIM] == Cl2) && (Cr1 == Cr2))
                                {
                                    if(((Int_t) (OuTS[j] - ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM]) >= TSGate[0])
                                            &&
                                            ((Int_t) (OuTS[j] - ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM]) <= TSGate[1]
                                             ))
                                    {

                                        OuUsed[j] = 1;
                                        OutersUsed++;
                                        if(OuV[j] >  ClE_SegMaxE_AB_BGO_CSI[ClE_AB_BGO_CSIM])
                                        {
                                            ClE_SegMaxE_AB_BGO_CSI[ClE_AB_BGO_CSIM] = OuV[j];
                                            ClE_AB_BGO_CSISegMaxN[ClE_AB_BGO_CSIM] = OuN[j];
                                        }

                                    }

                                }
                            }
                            //  cout << "<"<<j << "> : Cl_Outer " << Cl2 << " Cr_Outer " << Cr2 << " SegN " << OuN[j] << " -> Used : "<< OuUsed[j] <<  " Outer " << OuV[j] << "/"<< ClE_SegMaxE_AB_BGO_CSI[ClE_AB_BGO_CSIM] << "  DetalTS: " << ((Int_t) (OuTS[j]- ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM])) <<  endl;

                        }
                        //   cout << "Cl " << Cl1 << " Cr " << Cr1 << " ref " << ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM] << endl;
                    }
                    else
                    {
                        if(
                                Cl1 == ClE_AB_BGO_CSIN[ClE_AB_BGO_CSIM]
                                &&
                                ((Int_t) (InTS[i] - ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM])) >= TSGate[0]
                                &&
                                ((Int_t) (InTS[i] - ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM])) <= TSGate[1]
                                ) // If in the same Clover and in TSGate AddBack
                        {
                            ClE_AB_BGO_CSI[ClE_AB_BGO_CSIM] += InV[i];
                            InUsed[i] = 1;
                            CrystalsUsed++;
                            for(Int_t j = 0; j< OuM; j++)
                            {

                                Cl2 = OuN[j]/16;
                                Cr2 = (OuN[j]-Cl2*16)/4;

                                if(OuUsed[j] == 0)
                                {

                                    if((ClE_AB_BGO_CSIN[ClE_AB_BGO_CSIM] == Cl2) && (Cr1 == Cr2))
                                    {
                                        if(((Int_t) (OuTS[j] - ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM]) >= TSGate[0])
                                                &&
                                                ((Int_t) (OuTS[j] - ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM]) <= TSGate[1]
                                                 ))
                                        {

                                            OuUsed[j] = 1;
                                            OutersUsed++;
                                            if(OuV[j] >  ClE_SegMaxE_AB_BGO_CSI[ClE_AB_BGO_CSIM])
                                            {
                                                ClE_SegMaxE_AB_BGO_CSI[ClE_AB_BGO_CSIM] = OuV[j];
                                                ClE_AB_BGO_CSISegMaxN[ClE_AB_BGO_CSIM] = OuN[j];
                                            }

                                        }

                                    }
                                }
                                //   cout << "<"<<j << "> : Cl_Outer " << Cl2 << " Cr_Outer " << Cr2 << " SegN " << OuN[j] << " -> Used : "<< OuUsed[j] <<  " Outer " << OuV[j] << "/"<< ClE_SegMaxE_AB_BGO_CSI[ClE_AB_BGO_CSIM] << "  DetalTS: " << ((Int_t) (OuTS[j]- ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM])) <<  endl;

                            }
                            //		cout << "Added Cl " << Cl1 << " Cr " << Cr1 << " TDiff " << ((Int_t) (InTS[i] - ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM])) << endl;
                        }
                        else
                        {

                        }
                    }
                }
            }
            if(ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM] > 0)
            {
                if(ClE_AB_BGO_CSIM>15)
                {
                    cout << " Cannot Go Beyond " << ClE_AB_BGO_CSIM << " / " << NumberOfDetectors << endl;
                }
                ClE_AB_BGO_CSIM++;
                //	    cout << endl;
            }
            else
            {

                //                cout << "ADDBACK_BGO_CSI: I didn't find a new clover !" << endl;
                //                cout << "ADDBACK_BGO_CSI: CrystalsUsed " << CrystalsUsed << " out of " << InM << endl;
                //                if(CrystalsUsed == InM)
                //                    cout << "ADDBACK_BGO_CSI: Maybe all remaining crystals were suppressed" << endl;
                //                else

                if(CrystalsUsed < InM)
                {
                    cout << "ADDBACK_BGO_CSI: CrystalsUsed " << CrystalsUsed << " smaller than " << InM << endl;
                }

            }


            //	if(InM>0) cout << "CrystalsUsed " << CrystalsUsed << " " << InM  << " " << ClE_AB_BGO_CSIM << endl;
            nWhileLoops++;  // One while loop is one Clover !
            if(nWhileLoops == NumberOfDetectors)
            {
                cout << "ADDBACK_BGO_CSI: Reached " <<  nWhileLoops << " for " << NumberOfDetectors << " of possible Clovers" << endl;
            }
        }
        while(CrystalsUsed < InM && ClE_AB_BGO_CSIM < NumberOfDetectors && nWhileLoops < NumberOfDetectors);

#ifdef WITH_ROOT
        for(UShort_t k=0;k <ClE_AB_BGO_CSIM; k++)
        {
            Vec.SetXYZ(Pos_Outers[ClE_AB_BGO_CSISegMaxN[k]][0],Pos_Outers[ClE_AB_BGO_CSISegMaxN[k]][1],Pos_Outers[ClE_AB_BGO_CSISegMaxN[k]][2]);
            //            cout << k << "/"<< ClE_AB_BGO_CSIM << " Cl #" << ClE_AB_BGO_CSIN[k] << " E = " << ClE_AB_BGO_CSI[k] << " keV -  MaxSeg : " << ClE_AB_BGO_CSISegMaxN[k] << endl;
            //                cout << "\t  X: " << Pos_Outers[ClE_AB_BGO_CSISegMaxN[k]][0]
            //                        << " Y: " << Pos_Outers[ClE_AB_BGO_CSISegMaxN[k]][1]
            //                        << " Z: " << Pos_Outers[ClE_AB_BGO_CSISegMaxN[k]][2] << endl;
            //                cout << " Theta : " << Vec.Theta()*TMath::RadToDeg() << " Phi: " << Vec.Phi()*TMath::RadToDeg() << endl;

            Theta = Vec.Theta();
            Phi = Vec.Phi();

            ClE_AB_BGO_CSISegMaxT[k] = Theta;
            ClE_AB_BGO_CSISegMaxP[k] = Phi;
            ClE_AB_BGO_CSISegMaxX[k] = Pos_Outers[ClE_AB_BGO_CSISegMaxN[k]][0];
            ClE_AB_BGO_CSISegMaxY[k] = Pos_Outers[ClE_AB_BGO_CSISegMaxN[k]][1];
            ClE_AB_BGO_CSISegMaxZ[k] = Pos_Outers[ClE_AB_BGO_CSISegMaxN[k]][2];

            ThetaV = 0 ;//(TP->GetTheta())/1000.;
            PhiV = 0; //(TP->GetPhi())/1000.;
            Float_t Beta = 0.02; // Id->Beta;
            ClA_AB_BGO_CSI[k] = CosAlpha(Theta,Phi,ThetaV,PhiV);;
            //if(Beta> 0 && ThetaV >)
            ClEDC_AB_BGO_CSI[k] = DopplerCorrection(ClE_AB_BGO_CSI[k],Beta,ClA_AB_BGO_CSI[k]);

            //                cout << "DC : " << ClEDC_AB_BGO_CSI[k] << " keV"<<endl;
        }
#endif
#endif
    }
    return isPresent;
    END;
}



void Exogam::ReadAngles(void)
{
    START;
    Char_t A[4] = {'A','B','C','D'};

    MIFile *IF;
    char Line[255];
    int Len=255;
    stringstream *InOut;
    if(fCalData)
    {

        InOut = new stringstream();
        *InOut  << getenv("ANALYSIS") << "/Calibs/Outers_Angles.cal";
        *InOut>>Line;
        IF = new MIFile(Line);
        delete InOut;
        cout << "    <" << DetectorName << "><" << DetectorNameExt << "> Reading " << Line << endl;
        L->File << "    <" << DetectorName << "><" << DetectorNameExt << "> Reading " << Line << endl;
        for(UShort_t i=0;i<5;i++)
        {
            IF->GetLine(Line,Len);
            cout << Line << endl;
            L->File << Line << endl;
        }
        for(UShort_t i=0;i<NumberOfDetectors*16;i+=4)
        {
            Int_t Fl = i/16;
            IF->GetLine(Line,Len);
            InOut = new stringstream();
            *InOut << Line;
            for(UShort_t j=0;j<4;j++)
                *InOut >> Pos_Outers[i+j][0] >> Pos_Outers[i+j][1] >> Pos_Outers[i+j][2];
            //                for(UShort_t j=0;j<4;j++)
            //                    L->File <<  Pos_Outers[i+j][0] << " " << Pos_Outers[i+j][1]<< " " << Pos_Outers[i+j][2] << " ";

            L->File << "=== Flange : " << Fl  << "\t Cristal : " << A[(i-16*Fl)/4] <<   endl;

            for(UShort_t k=0;k<4;k++)
            {
                L->File << i+k << " " << Pos_Outers[i+k][0] << " " << Pos_Outers[i+k][1] << " " << Pos_Outers[i+k][1] << endl;
            }

            //                for(UShort_t k=0;k<4;k++)
            //                {
            //                    for(Int_t j=0;j<2;j++)
            //                        Ang_G[i+k][j] *= (M_PI)/180.;
            //                }
            delete InOut;
        }
        delete IF;
    }
    END;
}




#ifdef WITH_ROOT
void Exogam::CreateHistograms(TFile *OFile)
{
    START;
    for(UShort_t i=0;i<DetList->size();i++)
    {
        // A bit more advanced ... to be done
        // DetList->at(i)->SetHistogramsRaw(fHistogramsRaw);
        // DetList->at(i)->SetHistogramsCal(fHistogramsCal);
        // DetList->at(i)->SetMainHistogramFolder(MainHistogramFolder);
        // DetList->at(i)->CreateHistograms(HM, OFile);
    }
    END;
}

void Exogam::Lab2Vamos(Float_t ThetaL, Float_t PhiL, Float_t * Theta, Float_t * Phi)
{
#ifdef RECO_VAMOS
    TVector3 * myVec;
    Float_t AngleVamos = Reco->GetVamosAngle()*M_PI/180.;
    myVec = new TVector3(1.,1.,1.);
    myVec->SetMagThetaPhi(1.,ThetaL,PhiL);
    myVec->RotateY(-1.*AngleVamos);
    *Theta = atan(myVec->X()/myVec->Z());
    *Phi = atan(myVec->Y()/myVec->Z());
    *Phi = atan(tan(*Phi)*cos(*Theta));
    delete myVec;
#endif

    return;

}

void Exogam::SetOpt(TTree *OutTTree, TTree *InTTree)
{
    START;
    // BaseDetector::SetOpt(OutTTree,InTTree);
    //   // Set histogram Hierarchy
    SetMainHistogramFolder("Exogam");
    SetHistogramsCal1DFolder("Exo1D");
    SetHistogramsCal2DFolder("Exo2D");
    for(UShort_t i = 0;i<DetList->size();i++)
    {
        DetList->at(i)->SetMainHistogramFolder("");
        DetList->at(i)->SetHistogramsRaw1DFolder("Exo1D");
        DetList->at(i)->SetHistogramsRaw2DFolder("Exo2D");
        DetList->at(i)->SetHistogramsCal1DFolder("Exo1D");
        DetList->at(i)->SetHistogramsCal2DFolder("Exo2D");
    }

    if(fMode == MODE_WATCHER)
    {
        if(fRawData)
        {
            SetHistogramsRaw(false);
        }
        if(fCalData)
        {
            SetHistogramsCal(false);
        }
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw(false);
            }
            if(DetList->at(i)->HasCalData())
            {
                DetList->at(i)->SetHistogramsCalSummary(true);
                if(i==0) // innner
                {
                    DetList->at(i)->SetHistogramsCal1D(true);
                }
            }
        }
    }
    else if(fMode == MODE_D2R)
    {
        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetOutAttachRawV(true);
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
            }
            DetList->at(i)->OutAttach(OutTTree);
        }
        OutAttach(OutTTree);

    }
    else if(fMode == MODE_D2A)
    {
        if(fRawData)
        {
	  // Force No OutPut of cal Data, to keep fOutattach Flags for sub detectors
            SetHistogramsRaw(false);
            SetOutAttachRawI(false);
            SetOutAttachRawV(false);
        }
        if(fCalData)
        {
	  // Force No OutPut of cal Data, to keep fOutattach Flags for sub detectors
	  SetHistogramsCal(false);
	  SetOutAttachCalI(false);
	  SetOutAttachCalV(false);
        }
        //Out Attach

#ifdef ADDBACK
        // CLover AB
        OutTTree->Branch(Form("ClE_ABM"),&ClE_ABM,Form("ClE_ABM/I"));
        OutTTree->Branch(Form("ClE_ABN"),ClE_ABN,Form("ClE_ABN[ClE_ABM]/s"));
        OutTTree->Branch(Form("ClE_AB"),ClE_AB,Form("ClE_AB[ClE_ABM]/F"));
        OutTTree->Branch(Form("ClEDC_AB"),ClEDC_AB,Form("ClEDC_AB[ClE_ABM]/F"));
        OutTTree->Branch(Form("ClA_AB"),ClA_AB,Form("ClA_AB[ClE_ABM]/F"));
        OutTTree->Branch(Form("ClT_AB"),ClT_AB,Form("ClT_AB[ClE_ABM]/F"));
        OutTTree->Branch(Form("ClE_ABTS"),ClE_ABTS,Form("ClE_ABTS[ClE_ABM]/l"));

        OutTTree->Branch(Form("ClTheta_AB"),ClE_ABSegMaxT,Form("ClTheta_AB[ClE_ABM]/F"));
        OutTTree->Branch(Form("ClPhi_AB"),ClE_ABSegMaxP,Form("ClPhi_AB[ClE_ABM]/F"));
        OutTTree->Branch(Form("ClX_AB"),ClE_ABSegMaxX,Form("ClX_AB[ClE_ABM]/F"));
        OutTTree->Branch(Form("ClY_AB"),ClE_ABSegMaxY,Form("ClY_AB[ClE_ABM]/F"));
        OutTTree->Branch(Form("ClZ_AB"),ClE_ABSegMaxZ,Form("ClZ_AB[ClE_ABM]/F"));
        OutTTree->Branch(Form("ClMaxSegN_AB"),ClE_ABSegMaxN,Form("ClMaxSegN_AB[ClE_ABM]/s"));

#endif
#ifdef ADDBACK_BGO
        // CLover AB_BGO
        OutTTree->Branch(Form("ClE_AB_BGOM"),&ClE_AB_BGOM,Form("ClE_AB_BGOM/I"));
        OutTTree->Branch(Form("ClE_AB_BGON"),ClE_AB_BGON,Form("ClE_AB_BGON[ClE_AB_BGOM]/s"));
        OutTTree->Branch(Form("ClE_AB_BGO"),ClE_AB_BGO,Form("ClE_AB_BGO[ClE_AB_BGOM]/F"));
        OutTTree->Branch(Form("ClEDC_AB_BGO"),ClEDC_AB_BGO,Form("ClEDC_AB_BGO[ClE_AB_BGOM]/F"));
        OutTTree->Branch(Form("ClA_AB_BGO"),ClA_AB_BGO,Form("ClA_AB_BGO[ClE_AB_BGOM]/F"));
        OutTTree->Branch(Form("ClE_AB_BGOTS"),ClE_AB_BGOTS,Form("ClE_AB_BGOTS[ClE_AB_BGOM]/l"));
#endif
#ifdef ADDBACK_BGO_CSI
        // CLover AB_BGO_CSI
        OutTTree->Branch(Form("ClE_AB_BGO_CSIM"),&ClE_AB_BGO_CSIM,Form("ClE_AB_BGO_CSIM/I"));
        OutTTree->Branch(Form("ClE_AB_BGO_CSIN"),ClE_AB_BGO_CSIN,Form("ClE_AB_BGO_CSIN[ClE_AB_BGO_CSIM]/s"));
        OutTTree->Branch(Form("ClE_AB_BGO_CSI"),ClE_AB_BGO_CSI,Form("ClE_AB_BGO_CSI[ClE_AB_BGO_CSIM]/F"));
        OutTTree->Branch(Form("ClEDC_AB_BGO_CSI"),ClEDC_AB_BGO_CSI,Form("ClEDC_AB_BGO_CSI[ClE_AB_BGO_CSIM]/F"));
        OutTTree->Branch(Form("ClA_AB_BGO_CSI"),ClA_AB_BGO_CSI,Form("ClA_AB_BGO_CSI[ClE_AB_BGO_CSIM]/F"));
        OutTTree->Branch(Form("ClTheta_AB_BGO_CSI"),ClE_AB_BGO_CSISegMaxT,Form("ClTheta_AB_BGO_CSI[ClE_AB_BGO_CSIM]/F"));
        OutTTree->Branch(Form("ClPhi_AB_BGO_CSI"),ClE_AB_BGO_CSISegMaxP,Form("ClPhi_AB_BGO_CSI[ClE_AB_BGO_CSIM]/F"));
        OutTTree->Branch(Form("ClX_AB_BGO_CSI"),ClE_AB_BGO_CSISegMaxX,Form("ClX_AB_BGO_CSI[ClE_AB_BGO_CSIM]/F"));
        OutTTree->Branch(Form("ClY_AB_BGO_CSI"),ClE_AB_BGO_CSISegMaxY,Form("ClY_AB_BGO_CSI[ClE_AB_BGO_CSIM]/F"));
        OutTTree->Branch(Form("ClZ_AB_BGO_CSI"),ClE_AB_BGO_CSISegMaxZ,Form("ClZ_AB_BGO_CSI[ClE_AB_BGO_CSIM]/F"));
        OutTTree->Branch(Form("ClE_AB_BGO_CSITS"),ClE_AB_BGO_CSITS,Form("ClE_AB_BGO_CSITS[ClE_AB_BGO_CSIM]/l"));
#endif


        for(UShort_t i = 0;i<DetList->size();i++)
        {
            if(DetList->at(i)->HasRawData())
            {
                DetList->at(i)->SetHistogramsRaw1D(false);
                DetList->at(i)->SetHistogramsRaw2D(true);
                DetList->at(i)->SetHistogramsRawSummary(true);
                DetList->at(i)->SetOutAttachRawI(false);
                /*
                DetList->at(i)->SetOutAttachTS(true);
                DetList->at(i)->SetOutAttachRawV(true);*/
            }
            if(DetList->at(i)->HasCalData())
            {
#ifndef WITH_EXOGAM_REA
                if(i == Det_ECC_Offset || i == Det_ECC_Offset+1  || i == Det_ECC_Offset+2)
#else
                if(i == Det_ECC_Offset || i == Det_ECC_Offset+1)
#endif
                {
                    DetList->at(i)->SetHistogramsCal1D(true);
                    DetList->at(i)->SetHistogramsCalSummary(true);
                    
                    DetList->at(i)->SetOutAttachCalI(false);
		    // DetList->at(i)->SetOutAttachCalTS(fOutAttachCalV);
                    // DetList->at(i)->SetOutAttachCalV(fOutAttachCalV);
                    // DetList->at(i)->SetOutAttachTime(fOutAttachCalV);
                }
                else if(i == Det_GOCCE_Offset)
                {
                    DetList->at(i)->SetHistogramsCal1D(false);
                    DetList->at(i)->SetHistogramsCalSummary(true);
                    DetList->at(i)->SetOutAttachCalI(false);
                    // DetList->at(i)->SetOutAttachCalV(fOutAttachCalV);
                    // DetList->at(i)->SetOutAttachTime(fOutAttachCalV);
                }
                else if(i == Det_BGO_Offset || i == Det_CSI_Offset)
                {
                    DetList->at(i)->SetHistogramsCal1D(false);
                    DetList->at(i)->SetHistogramsCalSummary(true);
                    DetList->at(i)->SetOutAttachCalI(false);
                    DetList->at(i)->SetOutAttachCalV(fOutAttachCalV);
                }
                else if(i >= Det_Analysis_Offset)
                {
                    // All In vector mode except ExCl* parameters
                    DetList->at(i)->SetNoOutput();
                    //                    DetList->at(i)->SetHistogramsCal1D(false);
                    //                    DetList->at(i)->SetOutAttachCalI(false);
                    //                    DetList->at(i)->SetOutAttachCalV(true);
                    //                    DetList->at(i)->SetOutAttachCalF(false);
                    //                    if (i == Det_Ex_Offset
                    //                            || i == Det_Ex_Offset+1
                    //                            || i == Det_Ex_Offset+2
                    //                            || i == Det_ExCr_Offset
                    //                            )
                    //                    {
                    //                        DetList->at(i)->SetOutAttachCalV(false);
                    //                        DetList->at(i)->SetOutAttachCalF(true);
                    //                    }
                }
                else
                {
                    DetList->at(i)->SetHistogramsCal1D(false);
                    DetList->at(i)->SetOutAttachCalI(false);
                    DetList->at(i)->SetOutAttachCalV(false);
                    DetList->at(i)->SetOutAttachCalF(false);
                }
                DetList->at(i)->OutAttach(OutTTree);

            }

        }
        OutAttach(OutTTree);
    }
    //   else if(fMode == 3)
    //     {
    // 		for(UShort_t i = 0;i<DetList->size();i++)
    // 		  {
    // 			 if(i == 2)
    // 			 	DetList->at(i)->SetInAttachRawV(true);
    // 			 else
    //             {
    //               DetList->at(i)->SetInAttachRawV(false);
    //               DetList->at(i)->SetInAttachRawI(true);
    //             }

    // 			 DetList->at(i)->InAttach(InTTree);
    // 		  }

    // 		if(fRawData)
    // 		  {
    // 			 SetHistogramsRaw(false);
    // 			 SetOutAttachRawI(false);
    // 			 SetOutAttachRawV(false);
    // 		  }
    // 		if(fCalData)
    // 		  {
    // 			 SetHistogramsCal(true);
    // 			 SetOutAttachCalI(true);
    // 			 SetOutAttachCalV(false);
    // 		  }

    // 		for(UShort_t i = 0;i<DetList->size();i++)
    // 		  {
    // 			 if(DetList->at(i)->HasCalData())
    // 				{
    // 				  if(i == 2)
    // 					 {
    // 						DetList->at(i)->SetHistogramsCal1D(false);
    // 						DetList->at(i)->SetHistogramsCalSummary(true);
    // 						DetList->at(i)->SetOutAttachCalV(true);
    // 					 }
    // 				  else
    // 					 {
    // 						DetList->at(i)->SetHistogramsCal1D(true);
    // 						DetList->at(i)->SetOutAttachCalI(true);
    // 					 }
    // 				}
    // 			 DetList->at(i)->OutAttach(OutTTree);
    // 		  }
    // 		OutAttach(OutTTree);
    else if (fMode == MODE_CALC)
    {
        SetNoOutput();
    }
    else
    {
        Char_t Message[500];
        sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
        MErr * Er= new MErr(WhoamI,0,0, Message);
        throw Er;
    }

    if(VerboseLevel >= V_INFO)
        PrintOptions(cout)  ;

    END;
}

Float_t Exogam::DopplerCorrection(Float_t Eg, Float_t Beta, Float_t CosAlpha)
{
    Float_t Gamma = 1./sqrt(1-Beta*Beta);

    Float_t EDC = Eg * Gamma * (1-Beta*CosAlpha);
    return EDC;

}
Float_t Exogam::CosAlpha(Float_t ThetaG, Float_t PhiG, Float_t ThetaV, Float_t PhiV)
{

    Float_t CosAlpha = sin(ThetaV)*
            sin(ThetaG)*
            cos(PhiG)*
            cos(PhiV) +
            sin(PhiV)*
            sin(ThetaG) *
            sin(PhiG) +
            cos(ThetaV)*
            cos(PhiV)*
            cos(ThetaG);
    return CosAlpha;
}

#endif
