#include "Pista.h"

Pista::Pista(const Char_t *Name,
    UShort_t NDetectors,
    UShort_t NDeltaEStrips,
    UShort_t NEStrips,
    Bool_t RawData,
    Bool_t CalData,
    Bool_t DefaultCalibration,
    const Char_t *NameExt)
  : BaseDetector(Name,6,false, CalData, DefaultCalibration,false,NameExt)

{
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  NStrips_DE = NDeltaEStrips;
  NStrips_E = NEStrips;
  fCalData = CalData;
  DetManager *DM = DetManager::getInstance();
  AllocateComponents();

  if(CalData)
  {
    sprintf(CalNameI[0],"Pista_X");
    sprintf(CalNameI[1],"Pista_Y");
    sprintf(CalNameI[2],"Pista_Z");
    sprintf(CalNameI[3],"Pista_Theta");
    sprintf(CalNameI[4],"Pista_Phi");
    sprintf(CalNameI[5],"mult_pista");
  }

  EnvManager* EM = EnvManager::getInstance();
  string filename = getenv(EM->getPathVar().c_str());
  filename +=  "/Confs/pista.detector";

  ReadGeometry(filename.c_str());
}
Pista::~Pista(void)
{
  START;



  END;

}

void Pista::ReadGeometry(string filename){
  ifstream ifile;
  ifile.open(filename.c_str());

  string buffer;
  double x, y, z;
  TVector3 A;
  TVector3 B;
  TVector3 C;
  TVector3 D;

  if(ifile.is_open()){
    for(int i=0; i<NumberOfSubDetectors; i++){
      ifile >> buffer;
      ifile >> buffer >> x >> y >> z;
      A = TVector3(x,y,z);
      ifile >> buffer >> x >> y >> z;
      B = TVector3(x,y,z);
      ifile >> buffer >> x >> y >> z;
      C = TVector3(x,y,z);
      ifile >> buffer >> x >> y >> z;
      D = TVector3(x,y,z);

      ((PistaTelescope*)GetSubDet(i))->AddGeometry(A,B,C,D);
    }
  }
}

void Pista::Clear()
{
  START;
  BaseDetector::Clear();
  Pista_TS = 0;
  END;
}

void Pista::AllocateComponents(void)
{
  START;
  // Default Value
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = false;
  Bool_t PosInfo = true;
  Pista_TS = 0;
  Char_t DName[100];
  UShort_t MaxMult = 1;
#ifdef WITH_NPTOOL
  fPISTAData = new TPISTAData();
#endif

  BaseDetector *ADet = NULL;
  for(UShort_t i=0; i<NumberOfSubDetectors; i++)
  {
    Char_t Name[200];
    sprintf(Name,"PISTA_%02d",i);
    ADet = new  PistaTelescope(Name,NStrips_DE,NStrips_E,RawData,CalData,DefCal);
    AddComponent(ADet);
  }


  // Here we handle at global level for storage / Analysis
  // PistaEStr (EStr,Nr,StrNr)
  // PistaDEStr (dEStr,Nr,StrNr)
  // PistaEStrT (ETStr,Nr)
  // PistaDEStrT (dETStr,Nr)
  // PistaDE (E,Nr)
  // PistaE (dE,Nr)
  // PistaE_T (ET,Nr)
  // PistaDE_T (dET,Nr)

  Pista_E_Str_E = NULL;
  Pista_dE_Str_E = NULL;
  Pista_E_Str_T = NULL;
  Pista_dE_Str_T = NULL;
  //
  Pista_E = NULL;
  Pista_dE = NULL;
  Pista_E_T = NULL;
  Pista_dE_T = NULL;

  CalData = true;
  RawData = false;
  sprintf(DName,"Pista_E_Str_E");
  Pista_E_Str_E = new BaseDetector (DName,NumberOfSubDetectors*NStrips_E,RawData,CalData,DefCal,false,"",false,true,MaxMult);
  Pista_E_Str_E->SetNoOutput();
  AddComponent(Pista_E_Str_E);

  CalData = true;
  RawData = false;
  sprintf(DName,"Pista_dE_Str_E");
  Pista_dE_Str_E = new BaseDetector (DName,NumberOfSubDetectors*NStrips_DE,RawData,CalData,DefCal,false,"",false,true,MaxMult);
  Pista_dE_Str_E->SetNoOutput();
  AddComponent(Pista_dE_Str_E);

  CalData = true;
  RawData = false;
  sprintf(DName,"Pista_E_Str_T");
  Pista_E_Str_T = new BaseDetector (DName,NumberOfSubDetectors,RawData,CalData,DefCal,false,"",false,true,MaxMult);
  Pista_E_Str_T->SetNoOutput();
  AddComponent(Pista_E_Str_T);

  CalData = true;
  RawData = false;
  sprintf(DName,"Pista_dE_Str_T");
  Pista_dE_Str_T = new BaseDetector (DName,NumberOfSubDetectors,RawData,CalData,DefCal,false,"",false,true,MaxMult);
  Pista_dE_Str_T->SetNoOutput();
  AddComponent(Pista_dE_Str_T);

  CalData = true;
  RawData = false;
  sprintf(DName,"Pista_E");
  Pista_E = new BaseDetector (DName,NumberOfSubDetectors,RawData,CalData,DefCal,false,"",false,true,MaxMult);
  Pista_E->SetNoOutput();
  AddComponent(Pista_E);

  CalData = true;
  RawData = false;
  sprintf(DName,"Pista_dE");
  Pista_dE = new BaseDetector (DName,NumberOfSubDetectors,RawData,CalData,DefCal,false,"",false,true,MaxMult);
  Pista_dE->SetNoOutput();
  AddComponent(Pista_dE);

  CalData = true;
  RawData = false;
  sprintf(DName,"Pista_E_T");
  Pista_E_T = new BaseDetector (DName,NumberOfSubDetectors,RawData,CalData,DefCal,false,"",false,true,MaxMult);
  Pista_E_T->SetNoOutput();
  AddComponent(Pista_E_T);

  CalData = true;
  RawData = false;
  sprintf(DName,"Pista_dE_T");
  Pista_dE_T = new BaseDetector (DName,NumberOfSubDetectors,RawData,CalData,DefCal,false,"",false,true,MaxMult);
  Pista_dE_T->SetNoOutput();
  AddComponent(Pista_dE_T);


  END;
}
void Pista::SetParameters(Parameters* Par, Map *Map)
{
  START;

  if(isComposite)
  {
    for(UShort_t i=0; i< DetList->size(); i++)
    {
      DetList->at(i)->SetParameters(Par,Map);
    }
  }
  END;
}


Bool_t Pista::Treat()
{
  START;
#ifdef WITH_NPTOOL
  fPISTAData->Clear();
#endif
  Ctr->at(0)++;
  int mult_pista=0;
  int hit_det=-1;
  if(isComposite)
  {
    for(UShort_t i=0; i< NumberOfSubDetectors; i++)
    {
      DetList->at(i)->Treat();
    }
  }
  for(UShort_t i=0;i<NumberOfSubDetectors;i++)
  {
    //  Strip E
    for(UShort_t k=0;k<GetSubDet(i)->GetSubDet(1)->GetCalM();k++)
    {
            Pista_E_Str_E->SetCalData(GetSubDet(i)->GetSubDet(1)->GetNrAt(k)+i*NStrips_E,GetSubDet(i)->GetSubDet(1)->GetCalAt(k),0);// GetSubDet(i)->GetSubDet(0)->GetCalTSAt(k));
        }
    //  Strip dE

    for(UShort_t k=0;k<GetSubDet(i)->GetSubDet(0)->GetCalM();k++)
    {
      Pista_dE_Str_E->SetCalData(GetSubDet(i)->GetSubDet(0)->GetNrAt(k)+i*NStrips_DE,GetSubDet(i)->GetSubDet(0)->GetCalAt(k),0);// ,GetSubDet(i)->GetSubDet(1)->GetCalTSAt(k));
    }

    //  StrE Time
    if(GetSubDet(i)->GetSubDet(5)->GetCalM()==1){
      Pista_E_Str_T->SetCalData(i,GetSubDet(i)->GetSubDet(5)->GetCalAt(0),0);// ,GetSubDet(i)->GetSubDet(2)->GetCalTSAt(0));
    }

    //  StrdE Time
    if(GetSubDet(i)->GetSubDet(4)->GetCalM()==1){
      Pista_dE_Str_T->SetCalData(i,GetSubDet(i)->GetSubDet(4)->GetCalAt(0),0);// ,GetSubDet(i)->GetSubDet(3)->GetCalTSAt(0));
    }

    //  E Rear
    if(GetSubDet(i)->GetSubDet(2)->GetCalM()==1){
      Pista_E->SetCalData(i,GetSubDet(i)->GetSubDet(2)->GetCalAt(0),0);// ,GetSubDet(i)->GetSubDet(2)->GetCalTSAt(0));
    }

    //  dE Rear
    if(GetSubDet(i)->GetSubDet(3)->GetCalM()==1){
      Pista_dE->SetCalData(i,GetSubDet(i)->GetSubDet(3)->GetCalAt(0),0);// ,GetSubDet(i)->GetSubDet(3)->GetCalTSAt(0));
    }

    //  E Rear Time
    if(GetSubDet(i)->GetSubDet(2)->GetRawM()==1){
      Pista_E_T->SetCalData(i,GetSubDet(i)->GetSubDet(2)->GetRawTimeAt(0),0);// ,GetSubDet(i)->GetSubDet(2)->GetCalTSAt(0));
    }

    //  dE Rear Time
    if(GetSubDet(i)->GetSubDet(3)->GetRawM()==1){
      Pista_dE_T->SetCalData(i,GetSubDet(i)->GetSubDet(3)->GetRawTimeAt(0),0);// ,GetSubDet(i)->GetSubDet(3)->GetCalTSAt(0));
    }


    // Get Postion of interaction
    int multDE = GetSubDet(i)->GetSubDet(0)->GetCalM();
    int multE = GetSubDet(i)->GetSubDet(1)->GetCalM();
    if(multE==1 && multDE==1){
      mult_pista++;
      hit_det = i;
    }
#ifdef WITH_NPTOOL
    // DE Strip
    for(UShort_t k=0;k<GetSubDet(i)->GetSubDet(0)->GetRawM();k++){
      fPISTAData->SetPISTA_DE_DetectorNbr(i+1);
      fPISTAData->SetPISTA_DE_StripNbr(GetSubDet(i)->GetSubDet(0)->GetRawNrAt(k)+1);
      fPISTAData->SetPISTA_DE_StripEnergy(GetSubDet(i)->GetSubDet(0)->GetRawAt(k));
    }

    // E Strip
    for(UShort_t k=0;k<GetSubDet(i)->GetSubDet(1)->GetRawM();k++){
      fPISTAData->SetPISTA_E_DetectorNbr(i+1);
      fPISTAData->SetPISTA_E_StripNbr(GetSubDet(i)->GetSubDet(1)->GetRawNrAt(k)+1);
      fPISTAData->SetPISTA_E_StripEnergy(GetSubDet(i)->GetSubDet(1)->GetRawAt(k));
    }

    // E Time
    if(GetSubDet(i)->GetSubDet(5)->GetRawM()==1){
      fPISTAData->SetPISTA_E_StripTime(GetSubDet(i)->GetSubDet(5)->GetRawAt(0));
    }

    // DE Time
    if(GetSubDet(i)->GetSubDet(4)->GetRawM()==1){
      fPISTAData->SetPISTA_DE_StripTime(GetSubDet(i)->GetSubDet(4)->GetRawAt(0));
    }

    // E back side
    if(GetSubDet(i)->GetSubDet(2)->GetRawM()==1){
      fPISTAData->SetPISTA_E_BackEnergy(GetSubDet(i)->GetSubDet(2)->GetRawAt(0));
      fPISTAData->SetPISTA_E_BackTime(GetSubDet(i)->GetSubDet(2)->GetRawTimeAt(0));
      fPISTAData->SetPISTA_E_BackDetector(i+1);
    }
    // DE back side
    if(GetSubDet(i)->GetSubDet(3)->GetRawM()==1){
      fPISTAData->SetPISTA_DE_BackEnergy(GetSubDet(i)->GetSubDet(3)->GetRawAt(0));
      fPISTAData->SetPISTA_DE_BackTime(GetSubDet(i)->GetSubDet(3)->GetRawTimeAt(0));
      fPISTAData->SetPISTA_DE_BackDetector(i+1);
    }
#endif
  }

  SetCalData(5,mult_pista);
  if(mult_pista==1){
    SetCalData(0,GetSubDet(hit_det)->GetCal(0));
    SetCalData(1,GetSubDet(hit_det)->GetCal(1));
    SetCalData(2,GetSubDet(hit_det)->GetCal(2));
    SetCalData(3,GetSubDet(hit_det)->GetCal(3));
    SetCalData(4,GetSubDet(hit_det)->GetCal(4));
  }
  return IsPresent();
  END;
}



#ifdef WITH_ROOT
void Pista::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
  // Set PISTA  Outputs
  SetMainHistogramFolder("PISTA");
  SetHistogramsRaw1DFolder("Raw");
  SetHistogramsRaw2DFolder("Cal");
  SetHistogramsCal1DFolder("Cal");
  SetHistogramsCal2DFolder("Cal");
  for(UShort_t i = 0;i<NumberOfSubDetectors;i++)
  {
    DetList->at(i)->SetOpt(OutTTree,InTTree);

  }


  // Specific Attachement for TS :
  if(fMode != MODE_WATCHER)
  {
      OutTTree->Branch(Form("%s_TS",GetName()),&Pista_TS,Form("%s_TS/l",GetName()));
  }


  for(UShort_t i = NumberOfSubDetectors;i<DetList->size();i++)
  {
    DetList->at(i)->SetMainHistogramFolder("");
    DetList->at(i)->SetHistogramsRaw1DFolder("Raw");
    DetList->at(i)->SetHistogramsRaw2DFolder("Cal");
    DetList->at(i)->SetHistogramsCal1DFolder("Cal");
    DetList->at(i)->SetHistogramsCal2DFolder("Cal");

    if(fMode == MODE_WATCHER)
    {
      DetList->at(i)->SetHistogramsCal2DFolder("");
      DetList->at(i)->SetHistogramsCal1DFolder("");
      if(DetList->at(i)->HasCalData())
      {
        DetList->at(i)->SetHistogramsCalSummary(true);
        DetList->at(i)->SetHistogramsCal1D(false);
      }
    }
    else if(fMode == MODE_D2R)
    {
      {
        if(i >= NumberOfSubDetectors)
        {
          if( DetList->at(i)->HasRawData())
            if(i == NumberOfSubDetectors || i == (NumberOfSubDetectors+1)) // Attach TS VALUES
              DetList->at(i)->SetOutAttachTS(true);

          DetList->at(i)->SetOutAttachRawV(true);
          DetList->at(i)->OutAttach(OutTTree);
        }



      }
#ifdef WITH_NPTOOL
      OutTTree->Branch("PISTA","TPISTAData",&fPISTAData);
#endif


    }
    else if(fMode == MODE_D2A)
    {
      if(i >= NumberOfSubDetectors) // Beyond PistaTeslescopes
      {
        if( DetList->at(i)->HasCalData())
        {
          DetList->at(i)->SetHistogramsCalSummary(false);
          DetList->at(i)->SetHistogramsCal2D(false);
          DetList->at(i)->SetHistogramsCal1D(false);

          //                    DetList->at(i)->SetOutAttachCalTS(true);
          DetList->at(i)->SetOutAttachCalV(true);
        }
        DetList->at(i)->OutAttach(OutTTree);
      }
      //}
#ifdef WITH_NPTOOL
      OutTTree->Branch("PISTA","TPISTAData",&fPISTAData);
#endif

  }
  else if(fMode == MODE_R2A)
  {

    SetOutAttachCalV(true);


  }
  else if (fMode == MODE_RECAL)
  {
    SetInAttachCalI(true);
    InAttachCal(InTTree);

    SetOutAttachCalI(true);

  }
  else if (fMode == MODE_CALC)
  {
    SetNoOutput();
  }
  else
  {
    Char_t Message[500];
    sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
    MErr * Er= new MErr(WhoamI,0,0, Message);
    throw Er;
  }

}
OutAttach(OutTTree);

END;
}

void Pista::CreateHistogramsCal2D(TDirectory* Dir){
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
  Dir->cd("");

  if(SubFolderHistCal2D.size()>0){
    Name.clear();
    Name = SubFolderHistCal2D;
    if(!(gDirectory->GetDirectory(Name.c_str())))
      gDirectory->mkdir(Name.c_str());
    gDirectory->cd(Name.c_str());
  }

  AddHistoCal(GetCalName(0),GetCalName(1),"PistaXY","PistaXY (mm)",300,-150,150,300,-150,150);
  AddHistoCal(GetCalName(3),GetCalName(4),"PistaThetaPhi","PistaThetaPhi (rad)",300,-3,3,300,-3.1415,3.1415);

  END;
}
            

#endif

