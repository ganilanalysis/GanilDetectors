#include "PistaTelescope.h"

PistaTelescope::PistaTelescope(const Char_t * Name, UShort_t NDeltaEStrips, UShort_t NEStrips,
    Bool_t RawData,
    Bool_t CalData,
    Bool_t DefaultCalibration)
  : BaseDetector(Name,11,false,CalData,true,false,"")
{
  START;
  fRawDataSubDetectors = RawData;
  NStrips_DE = NDeltaEStrips;
  NStrips_E = NEStrips;
  AllocateComponents();
  RearE = NULL;
  RearDE = NULL;
  DeltaEStr = NULL;
  EStr = NULL;
  if(CalData)
  {
    sprintf(CalNameI[0],"%s_X",GetName());
    sprintf(CalNameI[1],"%s_Y",GetName());
    sprintf(CalNameI[2],"%s_Z",GetName());
    sprintf(CalNameI[3],"%s_Theta",GetName());
    sprintf(CalNameI[4],"%s_Phi",GetName());
    sprintf(CalNameI[5],"%s_Strip_DE",GetName());
    sprintf(CalNameI[6],"%s_DE",GetName());
    sprintf(CalNameI[7],"%s_Strip_E",GetName());
    sprintf(CalNameI[8],"%s_E",GetName());
    sprintf(CalNameI[9],"%s_ThetaNormal",GetName());
    sprintf(CalNameI[10],"%s_Strip_DE_corr",GetName());
  }
  END;
}


PistaTelescope::~PistaTelescope()
{
  START;

  END;
}


void PistaTelescope::CalculatePosition(void)
{
  START;

  END;
}
void PistaTelescope::AllocateComponents()
{
  START;
  Char_t Name[200];
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;

  // For Every telescope Allocate E and DE subdetectors
  // Delta E Strips
  sprintf(Name,"%s_DEStr",DetectorName);
  DeltaEStr = new BaseDetector(Name,NStrips_DE,RawData,CalData,DefCal,PosInfo,"",true);
  if(RawData)
  {
    for (int i=0; i < NStrips_DE; i++)
    {
      // Format Will be PISTA_01_DE_001chna
      sprintf(Name,"%s_DE_%03d",DetectorName,i);
      DeltaEStr->SetParameterName(Name, i);
    }
    DeltaEStr->SetGateRaw(0,4096);
  }
  if(CalData)
  {
    DeltaEStr->HasCalCharges(true);
    DeltaEStr->SetCalHistogramsParams(1000,0,100,"MeV");
  }
  AddComponent(DeltaEStr);
  // E Strips
  sprintf(Name,"%s_EStr",DetectorName);
  EStr = new BaseDetector(Name,NStrips_E,RawData,CalData,DefCal,PosInfo,"",true);
  if(RawData)
  {
    for (int i=0; i < NStrips_E; i++)
    {
      // Format Will be from MMR PISTA_01_E_001

      sprintf(Name,"%s_E_%03d",DetectorName,i);
      EStr->SetParameterName(Name, i);
    }
    EStr->SetGateRaw(0,4096);
  }
  if(CalData)
  {
    EStr->HasCalCharges(true);
    EStr->SetCalHistogramsParams(1000,0,250,"MeV");
    //DeltaE->SetGateCal(0,20000);
  }
  AddComponent(EStr);

  // Back Planes :
  sprintf(Name,"%s_E",DetectorName);
  RearE = new BaseDetector(Name,1,RawData,CalData,DefCal,PosInfo,"",true,true,1,true);
  if(RawData)
  {
    sprintf(Name,"%s_E",DetectorName);
    RearE->SetParameterName(Name, 0);
    RearE->SetGateRaw(0,65535);
    RearE->SetRawHistogramsParams(16384,0,65535);


  }
  if(CalData)
  {
    sprintf(Name,"%s_E_C",DetectorName);

    RearE->SetCalName(Name,0);
    RearE->SetCalHistogramsParams(1000,0,300,"MeV");
  }
  AddComponent(RearE);

  sprintf(Name,"%s_DE",DetectorName);
  RearDE = new BaseDetector(Name,1,RawData,CalData,DefCal,PosInfo,"",true,true,1,true);
  if(RawData)
  {
    RearDE->SetParameterName("%s");
    RearDE->SetGateRaw(0,65535);
    RearDE->SetRawHistogramsParams(16384,0,65535);

  }
  if(CalData)
  {
    sprintf(Name,"%s_DE_C",DetectorName);

    RearDE->SetCalName(Name,0);
    RearDE->SetCalHistogramsParams(1000,0,300,"MeV");

  }
  AddComponent(RearDE);


  sprintf(Name,"%s_DE_StrT",DetectorName);
  BaseDetector *DeltaEStrT = new BaseDetector(Name,1,RawData,CalData,DefCal,PosInfo,"",true);
  if(RawData)
  {

    DeltaEStrT->SetParameterName("%s");

    DeltaEStrT->SetGateRaw(0,65535);
    DeltaEStrT->SetRawHistogramsParams(16384,0,65535);

  }
  if(CalData)
  {
    sprintf(Name,"%s_C",DetectorName);
    DeltaEStrT->SetCalName(Name,0);
    DeltaEStrT->SetCalHistogramsParams(1000,0,1000,"ns");

  }
  AddComponent(DeltaEStrT);

  sprintf(Name,"%s_E_StrT",DetectorName);
  BaseDetector *EStrT = new BaseDetector(Name,1,RawData,CalData,DefCal,PosInfo,"",true);
  if(RawData)
  {

    EStrT ->SetParameterName("%s");

    EStrT ->SetGateRaw(0,65535);
    EStrT ->SetRawHistogramsParams(16384,0,65535);

  }
  if(CalData)
  {
    sprintf(Name,"%s_C",DetectorName);
    EStrT ->SetCalName(Name,0);
    EStrT ->SetCalHistogramsParams(1000,0,1000,"ns");

  }
  AddComponent(EStrT);

  END;
}
void PistaTelescope::SetParameters(Parameters* Par,Map* Map)
{
  START;
  MesytecParameters *MP = MesytecParameters::getInstance();
  if(isComposite)
  {
    for(UShort_t i=0; i< DetList->size(); i++)
    {
      DetList->at(i)->SetParametersMesytec(MP,Map);
    }
  }
  END;
}

Bool_t PistaTelescope::Treat()
{
  START;

  Ctr->at(0)++;

  m_PositionOfInteraction = TVector3(0,0,0);

  if(isComposite)
  {
    for(UShort_t i=0; i< DetList->size(); i++)
    {

      DetList->at(i)->Treat();
    }
  }
//  if(DetList->at(2)->GetRawM()>0)
//  {
//      DetList->at(2)->PrintRaw();
//      DetList->at(2)->PrintOptions(std::cout);
//  }
  int multDE = GetSubDet(0)->GetCalM();
  int multE = GetSubDet(1)->GetCalM();
//    if(multE==multDE){
//      for(int i=0; i<multE; i++){
//        int stripDE = GetSubDet(0)->GetNrAt(i);
//        int stripE = GetSubDet(1)->GetNrAt(i);
//        if(stripDE<91 && stripE<57){
//          m_PositionOfInteraction.push_back(CalculatePositionOfInteraction(stripE,stripDE));
//        }

//      }
//    }

    if(multE==1 && multDE==1){
      int stripDE = GetSubDet(0)->GetNrAt(0);
      int stripE = GetSubDet(1)->GetNrAt(0);

      double de_strip = GetSubDet(0)->GetCalAt(0);
      SetCalData(5,de_strip);
      if(GetSubDet(3)->GetCalM()==1)
          SetCalData(6,GetSubDet(3)->GetCalAt(0));

      SetCalData(7,GetSubDet(1)->GetCalAt(0));
      if(GetSubDet(2)->GetCalM()==1)
          SetCalData(8,GetSubDet(2)->GetCalAt(0));

      if(stripDE<91 && stripE<57){
          m_PositionOfInteraction = CalculatePositionOfInteraction(stripE,stripDE);
          SetCalData(0,m_PositionOfInteraction.X());
          SetCalData(1,m_PositionOfInteraction.Y());
          SetCalData(2,m_PositionOfInteraction.Z());
          SetCalData(3,m_PositionOfInteraction.Theta());
          SetCalData(4,m_PositionOfInteraction.Phi());
          double angle = m_PositionOfInteraction.Angle(GetDetectorNormal());
          double de_corr = de_strip*cos(angle);
          SetCalData(9,angle);
          SetCalData(10,de_corr);

      }
      else
      {
          SetCalData(0,-1500);
          SetCalData(1,-1500);
          SetCalData(2,-1500);
          SetCalData(3,-1500);
          SetCalData(4,-1500);
          SetCalData(9,-1500);
          SetCalData(10,-1500);

      }

//      //    if(GetSubDet(0)!=DeltaEStr)
//      //      cout << "Funny 1 Behviour" <<GetSubDet(0) << " " << DeltaEStr <<  endl;
//      //    if(GetSubDet(1)!=EStr)
//      //        cout << "Funny 2 Behviour" <<GetSubDet(1) << " " << EStr <<  endl;
//      //    if(GetSubDet(2)!=RearE)
////        cout << "Funny 3 Behviour" <<GetSubDet(2) << " " << RearE <<  endl;
////    if(GetSubDet(3)!=RearDE)
////        cout << "Funny 4 Behviour" <<GetSubDet(3) << " " << RearDE <<  endl;


  }

  if(CalM>0)
    isPresent=true;

  return IsPresent();
  END;
}
#ifdef WITH_ROOT

void PistaTelescope::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;

  // Set histogram Hierarchy
  SetMainHistogramFolder("");

  SetHistogramsCal1DFolder("");
  SetHistogramsCal2DFolder("");
  for(UShort_t i = 0;i<DetList->size();i++)
  {
    DetList->at(i)->SetMainHistogramFolder("");
    DetList->at(i)->SetHistogramsRaw1DFolder("PISTA_R");
    DetList->at(i)->SetHistogramsRaw2DFolder("PISTA_R");
    DetList->at(i)->SetHistogramsCal1DFolder("PISTA_C");
    DetList->at(i)->SetHistogramsCal2DFolder("PISTA_C");
  }

  if(fMode == MODE_WATCHER)
  {
    if(fRawData)
    {
      SetHistogramsRaw(true);
    }
    if(fCalData)
    {
      SetHistogramsCal(true);
    }
    for(UShort_t i = 0;i<DetList->size();i++)
    {
      if(DetList->at(i)->HasRawData())
      {
        if(i == 2 || i == 3) // Rear Detectors
        {

          DetList->at(i)->SetHistogramsRaw1D(true);
          DetList->at(i)->SetHistogramsRaw2D(false);
          DetList->at(i)->SetHistogramsRawSummary(false);
          DetList->at(i)->SetOutAttachRawV(false);
          DetList->at(i)->SetOutAttachRawI(false);

        }else // Strips E and DE
        {
          DetList->at(i)->SetHistogramsRaw1D(false);
          DetList->at(i)->SetHistogramsRaw2D(true);
          DetList->at(i)->SetHistogramsRawSummary(true);
          DetList->at(i)->SetOutAttachRawV(false);
          DetList->at(i)->SetOutAttachRawI(false);
        }
      }
      if(DetList->at(i)->HasCalData())
      {
        DetList->at(i)->SetHistogramsCal1D(false);
        DetList->at(i)->SetHistogramsCal2D(true);
        DetList->at(i)->SetHistogramsCalSummary(true);
      }
    }
  }
  else if(fMode == MODE_D2R)
  {
    for(UShort_t i = 0;i<DetList->size();i++)
    {
      if(DetList->at(i)->HasRawData())
      {
        if(i == 2 || i == 3 || i == 4|| i == 5) // Rear Detectors
        {

          DetList->at(i)->SetHistogramsRaw1D(true);
          DetList->at(i)->SetHistogramsRaw2D(false);
          DetList->at(i)->SetHistogramsRawSummary(false);
          DetList->at(i)->SetOutAttachRawV(false);
          DetList->at(i)->SetOutAttachRawI(true);

        }else // Strips E and DE
        {
          DetList->at(i)->SetHistogramsRaw1D(false);
          DetList->at(i)->SetHistogramsRaw2D(true);
          DetList->at(i)->SetHistogramsRawSummary(true);
          DetList->at(i)->SetOutAttachRawV(true);
          DetList->at(i)->SetOutAttachRawF(false);
          DetList->at(i)->SetOutAttachRawI(false);
        }
      }
      DetList->at(i)->OutAttach(OutTTree);
    }
    OutAttach(OutTTree);

  }
  else if(fMode == MODE_D2A)
  {
    if(fRawData)
    {
      SetHistogramsRaw(false);
      SetOutAttachRawI(false);
      SetOutAttachRawV(false);
    }
    if(fCalData)
    {
      SetHistogramsCal(true);
      SetOutAttachCalI(false);
      SetOutAttachCalV(false);
    }

    for(UShort_t i = 0;i<DetList->size();i++)
    {
      if(DetList->at(i)->HasRawData())
      {
        if(i == 2 || i == 3) // Rear Detectors
        {

          DetList->at(i)->SetHistogramsRaw1D(true);
          DetList->at(i)->SetHistogramsRaw2D(false);
          DetList->at(i)->SetHistogramsRawSummary(false);
          DetList->at(i)->SetOutAttachRawV(false);
          DetList->at(i)->SetOutAttachRawI(false);
          if(DetList->at(i)->HasTime())
              DetList->at(i)->SetOutAttachTime(true);

        }else // Strips E and DE
        {
          DetList->at(i)->SetHistogramsRaw1D(false);
          DetList->at(i)->SetHistogramsRaw2D(true);
          DetList->at(i)->SetHistogramsRawSummary(true);
          DetList->at(i)->SetOutAttachRawV(false);
          DetList->at(i)->SetOutAttachRawI(false);
        }
      }
      if(DetList->at(i)->HasCalData())
      {
        if(i == 2 || i == 3) // Rear Detectors
        {

          DetList->at(i)->SetHistogramsCal1D(true);
          DetList->at(i)->SetHistogramsCal2D(false);
          DetList->at(i)->SetHistogramsCalSummary(false);
          DetList->at(i)->SetOutAttachCalV(false);
          DetList->at(i)->SetOutAttachCalI(false);
          //DetList->at(i)->SetOutAttachCalI(true);

        }else // Strips E and DE
        {
          DetList->at(i)->SetHistogramsCal1D(false);
          DetList->at(i)->SetHistogramsCal2D(true);
          DetList->at(i)->SetHistogramsCalSummary(true);
          DetList->at(i)->SetOutAttachCalV(false);
          //DetList->at(i)->SetOutAttachCalV(true);
          DetList->at(i)->SetOutAttachCalI(false);
        }
      }
      DetList->at(i)->OutAttach(OutTTree);
    }

    OutAttach(OutTTree);
  }
  else if(fMode == MODE_R2A)
  {

    SetNoOutput();

  }
  else if(fMode == MODE_RECAL)
  {

    SetNoOutput();

  }
  else if (fMode == MODE_CALC)
  {
    SetNoOutput();
  }
  else
  {
    Char_t Message[500];
    sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
    MErr * Er= new MErr(WhoamI,0,0, Message);
    throw Er;
  }
  END;
}

void PistaTelescope::AddGeometry(TVector3 A, TVector3 B, TVector3 C, TVector3 D){
  // Front Face 
  //  A------------------------B
  //   *----------------------*                      
  //    *--------------------*
  //     *------------------*
  //      *----------------*
  //       *--------------*
  //        *------------*
  //         D----------C

  double Height = 61.7; // mm
  double LongBase = 78.1; // mm
  double NumberOfStripsX = 57;
  double NumberOfStripsY = 91;
  double StripPitchY = Height/NumberOfStripsY; // mm
  double StripPitchX = LongBase/NumberOfStripsX; // mm

  m_A = A;
  m_B = B;
  m_C = C;
  m_D = D;

  // Vector u on telescope face paralelle to Y strips
  TVector3 u = B - A;
  u = u.Unit();
  // Vector v on telescope face paralelle to X strips
  TVector3 v = (C+D)*0.5 - (A+B)*0.5;
  v = v.Unit();
  // Vector n normal to detector surface pointing to target
  TVector3 n = -0.25*(A+B+C+D);
  double norm = n.Mag();
  n = n.Unit();

  vector<double> lineX;
  vector<double> lineY;
  vector<double> lineZ;

  vector<vector<double>> OneDetectorStripPositionX;
  vector<vector<double>> OneDetectorStripPositionY;
  vector<vector<double>> OneDetectorStripPositionZ;

  TVector3 Strip_1_1;
  double ContractedStripPitchX = StripPitchX*norm/(norm+7);
  double ContractedLongBase = NumberOfStripsX*ContractedStripPitchX;
  double deltaX = LongBase/2-ContractedLongBase/2;

  //Strip_1_1 = A + u*(StripPitchX / 2.) + v*(StripPitchY / 2.);
  Strip_1_1 = A + u*deltaX + u*(ContractedStripPitchX / 2.) + v*(NumberOfStripsY*StripPitchY - StripPitchY / 2.);

  TVector3 StripPos;
  for(int i=0; i<NumberOfStripsX; i++){
    lineX.clear();
    lineY.clear();
    lineZ.clear();
    for(int j=0; j<NumberOfStripsY; j++){
      //StripPos = Strip_1_1 + i*u*StripPitchX + j*v*StripPitchY;
      StripPos = Strip_1_1 + i*u*ContractedStripPitchX - j*v*StripPitchY;
      lineX.push_back(StripPos.X());
      //lineX.push_back(StripPos.X()*norm/(norm+7*abs(sin(n.Phi()))));
      lineY.push_back(StripPos.Y());
      //lineY.push_back(StripPos.Y()*norm/(norm+7*abs(cos(n.Phi()))));
      lineZ.push_back(StripPos.Z());
    }

    OneDetectorStripPositionX.push_back(lineX);
    OneDetectorStripPositionY.push_back(lineY);
    OneDetectorStripPositionZ.push_back(lineZ);
  }

  m_StripPositionX=OneDetectorStripPositionX;
  m_StripPositionY=OneDetectorStripPositionY;
  m_StripPositionZ=OneDetectorStripPositionZ;
} 

TVector3 PistaTelescope::CalculatePositionOfInteraction(int stripE, int stripDE){
  TVector3 position = TVector3(GetStripPositionX(stripE,stripDE),
      GetStripPositionY(stripE,stripDE),
      GetStripPositionZ(stripE,stripDE));

  return position;
}

TVector3 PistaTelescope::GetDetectorNormal(){
  // Vector u on telescope face paralelle to Y strips
  TVector3 u = m_B - m_A;
  u.Unit();
  // Vector v on telescope face paralelle to X strips;
  TVector3 v = (m_C + m_D)*0.5 - (m_A + m_B)*0.5;
  v.Unit();

  TVector3 Normal = u.Cross(v);

  return (Normal.Unit());
}

void PistaTelescope::CreateHistogramsCal2D(TDirectory* Dir){
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
  Dir->cd("");

  if(SubFolderHistCal2D.size()>0){
    Name.clear();
    Name = SubFolderHistCal2D;
    if(!(gDirectory->GetDirectory(Name.c_str())))
      gDirectory->mkdir(Name.c_str());
    gDirectory->cd(Name.c_str());
  }

  TString histo_name = Form("%s_StripDEvsE",GetName());
  AddHistoCal(GetCalName(8),GetCalName(5),histo_name,histo_name,500,0,200,500,0,100);

  histo_name = Form("%s_StripDEcorrvsE",GetName());
  AddHistoCal(GetCalName(8),GetCalName(10),histo_name,histo_name,500,0,200,500,0,100);

  histo_name = Form("%s_StripDEvsThetaNormal",GetName());
  AddHistoCal(GetCalName(9),GetCalName(5),histo_name,histo_name,500,-3,3,500,0,100);

  histo_name = Form("%s_StripDEvsStripE",GetName());
  AddHistoCal(GetCalName(7),GetCalName(5),histo_name,histo_name,500,0,200,500,0,100);

  histo_name = Form("%s_StripDEvsDE",GetName());
  AddHistoCal(GetCalName(5),GetCalName(6),histo_name,histo_name,500,0,100,500,0,100);
  
  histo_name = Form("%s_StripEvsE",GetName());
  AddHistoCal(GetCalName(7),GetCalName(8),histo_name,histo_name,500,0,200,500,0,200);


  END;
}


#endif
