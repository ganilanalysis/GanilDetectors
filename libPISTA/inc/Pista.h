#ifndef PISTA_H
#define PISTA_H

#include "BaseDetector.hh"
#include "PistaTelescope.h"
#include "TimeStamp.hh"

#ifdef WITH_NPTOOL
#include "TPISTAData.h"
#endif

class Pista : public BaseDetector
{
public:
    //! Constructor instantiation by type, number of channels, and calibration files
    Pista(const Char_t * Name,
          UShort_t NDetectors,
          UShort_t NDeltaEStrips,
          UShort_t NEStrips,
          Bool_t RawData,
          Bool_t CalData,
          Bool_t DefaultCalibration,
          const Char_t * NameExt);

    ~Pista(void);
    //! Allocate sub components :
    void AllocateComponents(void);
#ifdef WITH_ROOT
    //! Set Input/OutPut Option
    void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
    //! Clear Arrays
    void Clear(void);
    //!
    void SetParameters(Parameters *Par, Map *Map);
    //! Treat the data
    Bool_t Treat(void);

    inline void SetTS(ULong64_t& TS){Pista_TS = TS;};
    inline ULong64_t GetTS(void){return Pista_TS;};

    void ReadGeometry(string filename);
    void CreateHistogramsCal2D(TDirectory* Dir); 

private:
    UShort_t NStrips_DE;
    UShort_t NStrips_E;

    // Strips
    BaseDetector* Pista_E_Str_E = NULL;
    BaseDetector* Pista_dE_Str_E = NULL;
    BaseDetector* Pista_E_Str_T = NULL;
    BaseDetector* Pista_dE_Str_T = NULL;
    // Rear
    BaseDetector* Pista_E = NULL;
    BaseDetector* Pista_dE = NULL;
    BaseDetector* Pista_E_T = NULL;
    BaseDetector* Pista_dE_T = NULL;
    // TimeStamps of Mesytec Crate
    ULong64_t Pista_TS;

   

#ifdef WITH_NPTOOL
    TPISTAData* fPISTAData;
#endif

};

#endif // PISTA_H
