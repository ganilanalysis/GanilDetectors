#ifndef PISTATELESCOPE_H
#define PISTATELESCOPE_H

#include "BaseDetector.hh"
#include "MesytecParameters.hh"
#include "TVector3.h"


class PistaTelescope : public BaseDetector
{
public:
    //! Constructor
    PistaTelescope(const Char_t * Name, UShort_t NDeltaEStrips, UShort_t NEStrips,
                   Bool_t RawData,
                   Bool_t CalData,
                   Bool_t DefaultCalibration);
     ~PistaTelescope(void); //!< Destructor

    void AllocateComponents(void);

    //!  Calculate Position from DE and E planes
    void CalculatePosition(void);

    //! Treat the data
    Bool_t Treat(void);

    void CreateHistogramsCal2D(TDirectory* Dir);

    //! SetParameters
    void SetParameters(Parameters* Par,Map* Map);
    void AddGeometry(TVector3 A, TVector3 B, TVector3 C, TVector3 D);
    
    double GetStripPositionX(int X, int Y){
      return m_StripPositionX[X][Y];
    };
    double GetStripPositionY(int X, int Y){
      return m_StripPositionY[X][Y];
    };
    double GetStripPositionZ(int X, int Y){
      return m_StripPositionZ[X][Y];
    };

    TVector3 CalculatePositionOfInteraction(int stripE, int stripDE);
    TVector3 GetDetectorNormal();
    TVector3 GetPositionOfInteraction() {return m_PositionOfInteraction;};

#ifdef WITH_ROOT
    void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif

private:
    UShort_t NStrips_DE;
    UShort_t NStrips_E;
    BaseDetector* RearE; 
    BaseDetector* RearDE; 
    BaseDetector* DeltaEStr; 
    BaseDetector* EStr; 
     // for position reconstruction
    TVector3 m_A; //!
    TVector3 m_B; //!
    TVector3 m_C; //!
    TVector3 m_D; //!
    vector<vector<double>> m_StripPositionX; //!
    vector<vector<double>> m_StripPositionY; //!
    vector<vector<double>> m_StripPositionZ; //!
    //vector<TVector3> m_PositionOfInteraction; //
    TVector3 m_PositionOfInteraction; //!
};

#endif // PISTATELESCOPE_H
