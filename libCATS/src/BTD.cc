/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "BTD.hh"

BTD::BTD(const Char_t *Name, 
         UShort_t NDetectors, 
         Bool_t RawData, 
         Bool_t CalData, 
         Bool_t DefaultCalibration, 
         const Char_t *NameExt)
  : BaseDetector(Name, 4, RawData, CalData, DefaultCalibration, false, NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;

  AllocateComponents();
  AddCounter("Present CATS1 X"); //2
  AddCounter("Present CATS1 Y"); //3
  AddCounter("Present CATS2 X"); //4
  AddCounter("Present CATS2 Y"); //5

  if(fCalData)
    {
      sprintf(CalNameI[0],"%s_X", DetectorName);
      sprintf(CalNameI[1],"%s_Theta", DetectorName);
      sprintf(CalNameI[2],"%s_Y", DetectorName);
      sprintf(CalNameI[3],"%s_Phi", DetectorName);
      // sprintf(CalNameI[4],"%s_T1", DetectorName);
      // sprintf(CalNameI[5],"%s_T2", DetectorName);
      // X in  [-1500, 0]
      SetGateCal(-2000,2000,0);
      // Y in [0,500]
      SetGateCal(-2000,2000,1);
      // Theta  in  [-1500, 0]
      SetGateCal(-2000,2000,2);
      // Phi in [0,500]
      SetGateCal(-2000,2000,3);
      // // T1 in [0,500]
      // SetGateCal(-100,10000,4);
      // // T2 in [0,500]
      // SetGateCal(-100,10000,5);    

    }
  //  ReadCalibration();


  END;
}
BTD::~BTD(void)
{
  START;
  END;
}
void BTD::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;
 
  string BaseName;
  string Name;


  for(UShort_t i=0; i<NumberOfSubDetectors; i++)
    {
      Char_t Name[20];
      sprintf(Name,"CATS%d",i+1);
      // // Hack for TPPAC
      BaseDetector *ADet = new CATS(Name,(i == 0 ? 64:92),RawData,CalData,DefCal,DetectorNameExt);
      //BaseDetector *ADet = new CATS(Name,28,RawData,CalData,DefCal,DetectorNameExt);
      AddComponent(ADet);
    }

  Name.clear();
  Name ="T_BTD";
  BaseDetector *TCATS = new BaseDetector(Name.c_str(),7,RawData,CalData,DefCal,PosInfo,"");
  TCATS->SetParameterName("T_CATS12",0);
  TCATS->SetParameterName("T_MUVI1_CATS2",1);
  TCATS->SetParameterName("T_M5M8_CATS2",2);
  TCATS->SetParameterName("T_M6M7_CATS2",3);
  TCATS->SetParameterName("T_PLG_CATS2",4);
  TCATS->SetParameterName("T_FING_CATS2",5);
  TCATS->SetParameterName("T_CATS2_HF",6);

  if(RawData)
    {
      TCATS->SetRawHistogramsParams(4096,0,16384,"");
    }
  if(CalData)
    {
      //     // Name = Name + "_C";
      TCATS->SetGateCal(0,16384);
      // TCATS->SetMainHistogramFolder(Name.c_str());
      TCATS->SetCalHistogramsParams(4096,0,16384,"ns","");
    }
 
  AddComponent(TCATS);




  END;     
}

void BTD::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        {
          DetList->at(i)->SetParameters(Par,Map);
        }
    }  
  END;
}

void BTD::ReadPosition(void)
{ 
  START;
  MIFile *IF;
  char Line[255];
  stringstream *InOut;
  
  InOut = new stringstream();
  *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
  *InOut>>Line;
  InOut = CleanDelete(InOut);
  if((IF = CheckCalibration(Line)))
    {
      // Position 
      try
        {
          ReadDefaultPosition(IF);
        }
      catch(...)
        {
          IF = CleanDelete(IF);
          throw;
        }
    }
  END;
}

Bool_t BTD::Treat(void)
{
  START;
 
  if(isComposite)
    {
      for(UShort_t i=0; i< DetList->size(); i++)
        DetList->at(i)->Treat();
    }


  if(fCalData)
    {
      // for(UShort_t i=0;i<DetList->size();i++)
      // 	if(DetList->at(i)->IsPresent())
      // 	  XMult++;
  
      // Ctr->at(5+XMult)++;
		
      // Require at least one (X,Y) position measurement per chamber 
		
      if(DetList->at(0)->IsPresent() && 
         DetList->at(1)->IsPresent() )
        {
          Ctr->at(3)++;
          Ctr->at(4)++;
          // cout << "BTD " << GetRefY() << " " << GetRefZ() << endl;
	  
          if(FocalX() && FocalY())
            isPresent = true;
			 
        }
      else
        {
          // Set Value to defaults
          for(UShort_t i=0;i<DetList->size();i++)
            SetCalData(i,-1500.);
        }
    }
  return(isPresent); 


  END;
}



Bool_t BTD::FocalX(void)
{ 
  START;
  UShort_t i;
  Double_t A[2];
  Double_t B[2];    
  Float_t Tf = -1500.;
  Float_t Xf = -1500.;
  
  SetMatX();
  
  for( i=0;i<2;i++)
    A[i] = B[i] = 0.0;
  
    
  for(UShort_t i=0;i<NumberOfSubDetectors; i++)
    if(DetList->at(i)->IsPresent())
      {
        A[0] += ((Double_t) DetList->at(i)->GetCal(0))*DetList->at(i)->GetRefZ();
        A[1] += (Double_t) DetList->at(i)->GetCal(0);
      }
  B[0] =A[0]*MatX[0][0] + A[1]*MatX[0][1]; 
  B[1] =A[0]*MatX[1][0] + A[1]*MatX[1][1];
  
  // Tf in mrad
  Tf = (Float_t) (1000.*atan(B[0]));
  Xf = (Float_t) (B[1]+B[0]*GetRefZ()); //FocalPos
  
  
  if(Xf >-1500.)
    {
      // Ctr->at(10)++;
      Xf = -Xf; // in VAMOS reference plane
      
      SetCalData(0,Xf);
    }
  else
    SetCalData(0,-1500.);
  
  if(Tf>-1500.)
    {
      Tf =  -Tf; // in VAMOS reference plane
      // Ctr->at(11)++;
      SetCalData(1,Tf);
    }
  else
    SetCalData(1,-1500.);
 
  if(Xf >-1500. && Tf>-1500.)
    return true;
  else 
    return false;
  END;
}

Bool_t BTD::FocalY(void)
{ 
  START;
  UInt_t i;
  Float_t A[2];
  Float_t B[2];
  Float_t Pf = -1500.;
  Float_t Yf = -1500.;
  
  SetMatY();
  
  for(i=0;i<2;i++)
    A[i] = B[i] = 0.0;
  
  
  for(i=0;i<NumberOfSubDetectors;i++)
    if(DetList->at(i)->IsPresent())
      {
        A[0] += ((Double_t) DetList->at(i)->GetCal(1))*DetList->at(i)->GetRefZ();
        A[1] += DetList->at(i)->GetCal(1);
      }
  B[0] =A[0]*MatY[0][0] + A[1]*MatY[0][1]; 
  B[1] =A[0]*MatY[1][0] + A[1]*MatY[1][1]; 

  // Pf in mrad
  Pf = (Float_t) (1000.*atan(B[0]));
  Yf = (Float_t) (B[1]+B[0]*GetRefZ()); //FocalPos
  
  if(Yf>-1500)
    {
      // Ctr->at(12)++;
      SetCalData(2,Yf);
    }
  else 
    SetCalData(2,-1500.);

  if(Pf > -1500.) 
    {
      // Ctr->at(13)++;
      SetCalData(3,Pf);
    }
  else 
    SetCalData(3,-1500.);

  if(Yf >-1500. && Pf>-1500.)
    return true;
  else 
    return false;
  END;
}



void BTD::SetMatX(void)
{
  START;
  UInt_t i,j;
  Double_t A[2][2];
  Double_t Det;
  Double_t Zref;
  
  for(i=0;i<2;i++)
    for(j=0;j<2;j++)
      A[i][j] = MatX[i][j] = 0.;
  
  for(i=0;i<NumberOfSubDetectors;i++)
    if(DetList->at(i)->IsPresent())
      {
        Zref = DetList->at(i)->GetRefZ();
        A[0][0] += pow(Zref,2.);
        A[0][1] += Zref;
        A[1][1] += 1.0;
      }
  A[1][0] = A[0][1];
  
  

  Det = A[0][0]*A[1][1] - A[0][1]*A[1][0];

  if(Det == 0.0)
    {
      MErr * Er = new MErr(WhoamI,0,0, "Det == 0 !");
      throw Er;      
    }
  else
    {
      MatX[0][0] = A[1][1]/Det;
      MatX[1][1] = A[0][0]/Det;
      MatX[1][0] = -1.0*A[0][1]/Det;
      MatX[0][1] = -1.0*A[1][0]/Det;
    }
  END;
}

void BTD::SetMatY(void)
{
  START;
  UInt_t i,j;
  Double_t A[2][2];
  Double_t Det;
  Double_t Zref;
  
  for(i=0;i<2;i++)
    for(j=0;j<2;j++)
      A[i][j] = MatY[i][j] = 0.;
  
  for(i=0;i<NumberOfSubDetectors;i++)
    if(DetList->at(i)->IsPresent())
      {
        Zref = DetList->at(i)->GetRefZ();
        A[0][0] += pow(Zref,2.);
        A[0][1] += Zref;
        A[1][1] += 1.0;
      }
  A[1][0] = A[0][1];
  
  

  Det = A[0][0]*A[1][1] - A[0][1]*A[1][0];

  if(Det == 0.0)
    {
      MErr * Er = new MErr(WhoamI,0,0, "Det == 0 !");
      throw Er;      
    }
  else
    {
      MatY[0][0] = A[1][1]/Det;
      MatY[1][1] = A[0][0]/Det;
      MatY[1][0] = -1.0*A[0][1]/Det;
      MatY[0][1] = -1.0*A[1][0]/Det;
    }
  END;
}


#ifdef WITH_ROOT
void BTD::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
  Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
		
  // XS
  AddHistoCal(CalNameI[0],CalNameI[0],"XS (mm)",600,0,60);
  // YS
  AddHistoCal(CalNameI[1],CalNameI[1],"YS (mm)",600,0,60);
  // XWA
  AddHistoCal(CalNameI[2],CalNameI[2],"Theta (mrad)",600,0,60);
  // YWA
  AddHistoCal(CalNameI[3],CalNameI[3],"Phi (mrad)",600,0,60);
 
  END;
}

void BTD::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }
  // AddHistoCal(CalNameI[1],CalNameI[0],"XY_S","X_vs_Y (SECHS)",600,0,60,600,0,60);
  // AddHistoCal(CalNameI[3],CalNameI[2],"XY_WA","X_vs_Y (WeigtedAverage)",600,0,60,600,0,60);
  // AddHistoCal(CalNameI[2],CalNameI[4],"XWA_vs_QX","XWA vs QX",600,0,60,1000,0,10000);
  // AddHistoCal(CalNameI[3],CalNameI[5],"YWA_vs_QY","YWA vs QY",600,0,60,1000,0,10000);

  // DetList->at(2)->AddHistoCal(DetList->at(2)->GetCalName(0),DetList->at(2)->GetCalName(1),"SIE_vs_SIT","SIE vs SIT",2000,0,13000,500,00,16500);
  // DetList->at(2)->AddHistoCal(DetList->at(2)->GetCalName(0),DetList->at(2)->GetCalName(2),"SIE_vs_PPT","SIE vs PPACT",2000,0,13000,500,12500,13500);
  
  END;
}


void BTD::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
  BaseDetector::SetOpt(OutTTree,InTTree);
#ifdef WITH_ROOT
  // Setup Histogram Hierarchy
  SetHistogramsCal1DFolder("");
  SetHistogramsCal2DFolder("");
#endif

  //   if(fMode == 0)
  //     {
  //       if(fRawData)
  //         {
  //           SetHistogramsRaw(true);
  //           // SetOutAttachRawV(true);
  //         }
  //       if(fCalData)
  //         {
  //           SetHistogramsCal(true);
  //           // SetOutAttachCalI(true);
  //         }
  //       for(UShort_t i = 0;i<2;i++)
  //         {
  //           if(fRawDataSubDetectors)
  //             {
  //               DetList->at(i)->SetHistogramsRaw2D(true);
  //               DetList->at(i)->SetHistogramsRawSummary(true);
  //               // DetList->at(i)->SetOutAttachRawV(true);
  //             }
  //           DetList->at(i)->SetHistogramsCal1D(false);
  //           DetList->at(i)->SetHistogramsCal2D(true);
  //           DetList->at(i)->SetHistogramsCalSummary(true);
  //           // DetList->at(i)->SetOutAttachCalI(true);	
  // 		  }
  //       if(fRawDataSubDetectors)
  //         {
  //           DetList->at(2)->SetHistogramsRaw(true);
  //           // DetList->at(2)->SetOutAttachRawI(true);
  //         }
  //       DetList->at(2)->SetHistogramsCal(true);
  //       // DetList->at(2)->SetOutAttachCalI(true);
	
  //       // OutAttach(OutTTree);
  //       // 	for(UShort_t i = 0;i<DetList->size();i++)
  //       // 	  DetList->at(i)->OutAttach(OutTTree);

  // 	 }
  //   else if(fMode == 1)
  // 	 {
  // 		for(UShort_t i = 0;i<2;i++)
  // 		  {
  // 			 DetList->at(i)->SetHistogramsRaw2D(true);
  // 			 DetList->at(i)->SetHistogramsRawSummary(true);
  // 			 DetList->at(i)->SetOutAttachRawV(true);
  // 		  }
  // 		DetList->at(2)->SetHistogramsRaw(true);
  // 		DetList->at(2)->SetOutAttachRawI(true);
  // 		OutAttach(OutTTree);
  // 		for(UShort_t i = 0;i<DetList->size();i++)
  // 		  DetList->at(i)->OutAttach(OutTTree);
  // 	 }
  //   else if(fMode == 2)
  // 	 {
  // 		if(fRawData)
  // 		  {
  // 			 SetHistogramsRaw(true);
  // 			 SetOutAttachRawV(true);
  // 		  }
  // 		if(fCalData)
  // 		  {
  // 			 SetHistogramsCal(true);
  // 			 SetOutAttachCalI(true);
  // 		  }
		
  // 		for(UShort_t i = 0;i<2;i++)
  // 		  {
  // 			 if(fRawDataSubDetectors)
  // 				{
  // 				  DetList->at(i)->SetHistogramsRaw1D(false);
  // 				  DetList->at(i)->SetHistogramsRaw2D(true);
  // 				  DetList->at(i)->SetHistogramsRawSummary(true);
  // 				  DetList->at(i)->SetOutAttachRawV(true);
  // 				}
  // 			 DetList->at(i)->SetHistogramsCal1D(false);
  // 			 DetList->at(i)->SetHistogramsCal2D(true);
  // 			 DetList->at(i)->SetHistogramsCalSummary(true);
  // 			 DetList->at(i)->SetOutAttachCalI(false);
  // 			 DetList->at(i)->SetOutAttachCalV(true);			 
  // 		  }
  // 		if(fRawDataSubDetectors)
  // 		  {
  // 			 DetList->at(2)->SetHistogramsRaw(false);
  // 			 DetList->at(2)->SetOutAttachRawI(false);
  // 		  }
  // 		DetList->at(2)->SetHistogramsCal(true);
  // 		DetList->at(2)->SetOutAttachCalI(true);
  // #ifdef BTD_TRACE
  // 		for(UShort_t i = 3;i<7;i++)
  // 		  {
  // 		    DetList->at(i)->SetHistogramsCal1D(false);
  // 		    DetList->at(i)->SetHistogramsCalSummary(true);
  // 		    DetList->at(i)->SetOutAttachCalI(false);
  // 		    DetList->at(i)->SetOutAttachCalV(true);
  // 		  }

  // #endif
  // 		OutAttach(OutTTree);
  // 		for(UShort_t i = 0;i<DetList->size();i++)
  // 		  DetList->at(i)->OutAttach(OutTTree);
  // 	 }
  //   else if(fMode == 3)
  //     {
  //       SetInAttachRawV(true);
  //       InAttach(InTTree);
		
  // 		SetOutAttachCalI(true);
  //       OutAttach(OutTTree);
		
  //     }
  //   else 
  // 	 {
  // 		Char_t Message[200];
  // 		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
  // 		MErr * Er= new MErr(WhoamI,0,0, Message);
  // 		throw Er;
  // 	 }



  END;
}

#endif
