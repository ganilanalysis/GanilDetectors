/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *
 *    Contributor(s) :
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#include "CATSTracker.hh"

CATSTracker::CATSTracker(const Char_t *Name,
                         UShort_t NDetectors,
                         Bool_t RawData,
                         Bool_t CalData,
                         Bool_t DefaultCalibration,
                         const Char_t *NameExt
                         ) : BaseDetector(Name, 8, false, CalData, DefaultCalibration,false,NameExt)
{
    START;
    NumberOfSubDetectors = NDetectors;
    fRawDataSubDetectors  = RawData;


    AllocateComponents();

    // Read Position
    ReadPosition();

    AddCounter("PresentX"); //2
    AddCounter("PresentY"); //3
    AddCounter("Tf Warning"); //4
    AddCounter("Pf Warning"); //5


    if(fCalData)
    {
        SetCalName("Xf",0);
        SetCalName("Tf",1);
        SetCalName("Yf",2);
        SetCalName("Pf",3);

        SetCalName("XfWa",4);
        SetCalName("TfWa",5);
        SetCalName("YfWa",6);
        SetCalName("PfWa",7);
    }
#ifdef WITH_ROOT
    // Setup Histogram Hierarchy
    SetHistogramsCal1DFolder("");
    SetHistogramsCal2DFolder("");
#endif

    END;
}

CATSTracker::~CATSTracker(void)
{
    START;
    END;
}

void CATSTracker::AllocateComponents(void)
{
    START;
    // Default Value
    Bool_t RawData = fRawDataSubDetectors;
    Bool_t CalData = fCalData;
    Bool_t DefCal = false;

    for(UShort_t i=0; i<2; i++)
    {
        Char_t Name[20];
        sprintf(Name,"CATS%d",i+1);
        BaseDetector *ADet = NULL;
        ADet = new CATS(Name,NumberOfSubDetectors,RawData,CalData,DefCal,"");
        AddComponent(ADet);
    }
    END;
}


void CATSTracker::SetParameters(Parameters* Par,Map* Map)
{
    START;
    // Add A Range of Parameters for sub components
    if(isComposite){
        for(UShort_t i=0; i< DetList->size(); i++)
        {
            DetList->at(i)->SetParameters(Par,Map);
        }
    }
    END;
}
void CATSTracker::ReadPosition(void)
{
    START;
    if(fCalData)
    {
        MIFile *IF;
        char Line[255];
        stringstream *InOut;
        UShort_t Len=255;
        Char_t *Ptr=NULL;

        InOut = new stringstream();
        *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
        *InOut>>Line;
        InOut = CleanDelete(InOut);

        if((IF = CheckCalibration(Line)))
        {
            // Position of projection
            try
            {
                ReadDefaultPosition(IF);
            }
            catch(...)
            {
                IF = CleanDelete(IF);
                throw;
            }
            IF = CleanDelete(IF);
        }
    }
    END;
}
Bool_t CATSTracker::Treat(void)
{
    START;
    Ctr->at(0)++;
    if(isComposite)
    {
        for(UShort_t i =0; i< DetList->size(); i++)
            DetList->at(i)->Treat();
    }



//    UShort_t XMult=0;

    if(fCalData)
    {
////        for(UShort_t i=0;i<DetList->size();i++)
////        {
////            if(DetList->at(i)->IsPresent())
////                XMult++;
////        }
////        Ctr->at(5+XMult)++;

//        // Require at least one (X,Y) position measurement per chamber

        if((DetList->at(0)->IsPresent() && DetList->at(1)->IsPresent()))
        {
            if(FocalX() && FocalY())
                isPresent = true;
        }
        else
        {
            // Set Value to defaults
            for(UShort_t i=0;i<8;i++)
                SetCalData(i,-1500.);
        }
    }

    return(isPresent);
    END;
}

Bool_t CATSTracker::FocalX(void)
{
    START;
    Float_t Tf = -1500.;
    Float_t Xf = -1500.;
    Float_t TfWa = -1500.;
    Float_t XfWa = -1500.;



    // Tf in mrad from SECHS
    if (DetList->at(0)->GetCal(0) ==DetList->at(1)->GetCal(0)) IncrementCounter(4);

    Tf = (Float_t) (1000.*atan((DetList->at(0)->GetCal(0) - DetList->at(1)->GetCal(0))/(DetList->at(0)->GetRefZ()-DetList->at(1)->GetRefZ())));
    // Tf in mrad from WA
    TfWa = (Float_t) (1000.*atan((DetList->at(0)->GetCal(2) - DetList->at(1)->GetCal(2))/(DetList->at(0)->GetRefZ()-DetList->at(1)->GetRefZ())));

    Xf = (Float_t) (DetList->at(1)->GetCal(0) + tan(Tf/1000.) * (GetRefZ()- DetList->at(1)->GetRefZ())); //FocalPos
    XfWa = (Float_t) (DetList->at(1)->GetCal(2) + tan(TfWa/1000.) * (GetRefZ()- DetList->at(1)->GetRefZ())); //FocalPosWA

    if(Tf>-1500. && Tf < 1500.)
    {
        SetCalData(1,Tf);
    }
    else
        SetCalData(1,-1500.);

    if(Xf >-1500. && Xf < 1500.)
    {
        SetCalData(0,Xf);
    }
    else
        SetCalData(0,-1500.);

    if(TfWa>-1500. && TfWa < 1500.)
    {
        SetCalData(5,TfWa);
    }
    else
        SetCalData(5,-1500.);

    if(XfWa >-1500. && XfWa < 1500.)
    {
        SetCalData(4,XfWa);
    }
    else
        SetCalData(4,-1500.);

    if(Xf >-1500. && Tf>-1500. && XfWa >-1500. && TfWa>-1500.){
        IncrementCounter(2);
        return true;
    }
    else
        return false;
    END;
}

Bool_t CATSTracker::FocalY(void)
{
    START;
    Float_t Pf = -1500.;
    Float_t Yf = -1500.;
    Float_t PfWa = -1500.;
    Float_t YfWa = -1500.;

    // Pf in mrad from SECHS
    if (DetList->at(0)->GetCal(1) ==DetList->at(1)->GetCal(1)) IncrementCounter(5);
    Pf = (Float_t) (1000.*atan((DetList->at(0)->GetCal(1) - DetList->at(1)->GetCal(1))/(DetList->at(0)->GetRefZ()-DetList->at(1)->GetRefZ())));
    // Pf in mrad from WA
    PfWa = (Float_t) (1000.*atan((DetList->at(0)->GetCal(3) - DetList->at(1)->GetCal(3))/(DetList->at(0)->GetRefZ()-DetList->at(1)->GetRefZ())));

    Yf = (Float_t) (DetList->at(1)->GetCal(1) + tan(Pf/1000.) * (GetRefZ()-DetList->at(1)->GetRefZ())); //FocalPos
    YfWa = (Float_t) (DetList->at(1)->GetCal(3) + tan(PfWa/1000.) * (GetRefZ()-DetList->at(1)->GetRefZ())); //FocalPos


    if(Pf > -1500. && Pf < 1500.)
    {
        SetCalData(3,Pf);
    }
    else
        SetCalData(3,-1500.);

    if(Yf>-1500. && Yf < 1500.)
    {
        SetCalData(2,Yf);
    }
    else
        SetCalData(2,-1500.);

    if(PfWa > -1500. && PfWa < 1500.)
    {
        SetCalData(7,PfWa);
    }
    else
        SetCalData(7,-1500.);

    if(YfWa>-1500. && YfWa < 1500.)
    {
        SetCalData(6,YfWa);
    }
    else
        SetCalData(6,-1500.);


    if(Yf >-1500. && Pf>-1500. && YfWa >-1500. && PfWa>-1500.){
        IncrementCounter(3);
        return true;
    }
    else
        return false;
    END;
}





#ifdef WITH_ROOT
void CATSTracker::CreateHistogramsCal1D(TDirectory *Dir)
{
    START;
    string Name;
    Dir->cd("");
    if(SubFolderHistCal1D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal1D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    AddHistoCal(GetCalName(0),GetCalName(0),"Xf (mm)",200,-50,50);
    AddHistoCal(GetCalName(2),GetCalName(2),"Yf (mm)",200,-50,50);
    AddHistoCal(GetCalName(1),GetCalName(1),"Tf (mrad)",100,-50,50);
    AddHistoCal(GetCalName(3),GetCalName(3),"Pf (mrad)",100,-50,50);

    AddHistoCal(GetCalName(4),GetCalName(4),"XfWa (mm)",200,-50,50);
    AddHistoCal(GetCalName(6),GetCalName(6),"YfWa (mm)",200,-50,50);
    AddHistoCal(GetCalName(5),GetCalName(5),"TfWa (mrad)",100,-50,50);
    AddHistoCal(GetCalName(7),GetCalName(7),"PfWa (mrad)",100,-50,50);

    END;
}
void CATSTracker::CreateHistogramsCal2D(TDirectory *Dir)
{
    START;
    string Name;

    BaseDetector::CreateHistogramsCal2D(Dir);

    Dir->cd("");

    if(SubFolderHistCal2D.size()>0)
    {
        Name.clear();
        Name = SubFolderHistCal2D ;
        if(!(gDirectory->GetDirectory(Name.c_str())))
            gDirectory->mkdir(Name.c_str());
        gDirectory->cd(Name.c_str());
    }

    AddHistoCal(GetCalName(0),GetCalName(2),"Xf_Yf","Xf vs Yf",200,-50,50,200,-50,50);
    AddHistoCal(GetCalName(0),GetCalName(1),"Xf_Tf","Xf vs Thetaf",200,-50,50,100,-50,50);
    AddHistoCal(GetCalName(2),GetCalName(3),"Yf_Pf","Yf vs Phif",200,-50,50,100,-50,50);
    AddHistoCal(GetCalName(3),GetCalName(1),"Pf_Tf","Phif vs Thetaf",100,-50,50,100,-50,50);

    AddHistoCal(GetCalName(4),GetCalName(6),"XfWa_YfWa","XfWa vs YfWa",200,-50,50,200,-50,50);
    AddHistoCal(GetCalName(4),GetCalName(5),"XfWa_TfWa","XfWa vs ThetafWa",200,-50,50,100,-50,50);
    AddHistoCal(GetCalName(6),GetCalName(7),"YfWa_PfWa","YfWa vs PhifWa",200,-50,50,100,-50,50);
    AddHistoCal(GetCalName(7),GetCalName(5),"PfWa_TfWa","PhifWa vs ThetafWa",100,-50,50,100,-50,50);

    END;
}



void CATSTracker::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;


  if(isComposite)
      for(UShort_t i=0; i< DetList->size(); i++)
      {
          DetList->at(i)->SetOpt(OutTTree,InTTree);
          // if(VerboseLevel >= V_INFO)
          // DetList->at(i)->PrintOptions(cout);
          ///		  DetList->at(i)->PrintOptions(L->File);
      }

  // Set histogram Hierarchy
  SetMainHistogramFolder("");
  SetHistogramsCal1DFolder("TP");
  SetHistogramsCal2DFolder("TP");

  for(UShort_t i = 0;i<DetList->size();i++)
     {
        DetList->at(i)->SetMainHistogramFolder("");
        DetList->at(i)->SetHistogramsRaw1DFolder("TP");
        DetList->at(i)->SetHistogramsRaw2DFolder("TP");
        DetList->at(i)->SetHistogramsCal1DFolder("TP");
        DetList->at(i)->SetHistogramsCal2DFolder("TP");
     }

  if(fMode == MODE_WATCHER)
  {
      if(fRawData)
          SetHistogramsRaw(true);
      if(fCalData)
          SetHistogramsCal(true);
      OutAttach(OutTTree);
  }
  else if(fMode == MODE_D2R)
  {
      if(fRawData)
      {
          if(!fOutAttachRawF)
              SetOutAttachRawV(true);
          if(fMaxMult>1)
              SetOutAttachRawMult(fOutAttachRawMult);
      }
      OutAttach(OutTTree);
  }
  else if(fMode == MODE_D2A)
  {
      if(fRawData)
      {
          SetHistogramsRaw(false);
          SetOutAttachRawI(fOutAttachRawI);
          SetOutAttachRawV(fOutAttachRawV);
          SetOutAttachRawF(fOutAttachRawF);
      }
      if(fCalData)
      {
          SetHistogramsCal(true);
          SetOutAttachCalI(true);
          SetOutAttachCalF(false);
          SetOutAttachCalV(false);
      }
      OutAttach(OutTTree);
  }
  else if(fMode == MODE_R2A)
  {

      SetInAttachRawV(true);
      InAttach(InTTree);

      if(fRawData)
      {
          SetOutAttachRawI(fOutAttachRawI);
          SetOutAttachRawV(fOutAttachRawV);
          SetOutAttachRawF(fOutAttachRawF);
      }

      if(fCalData)
      {
          SetOutAttachCalI(true);
          SetOutAttachCalI(true);
          SetOutAttachCalF(false);
          SetOutAttachCalV(false);
      }
      OutAttach(OutTTree);

  }
  else if(fMode == MODE_RECAL) // Recalibration mode
  {
      if(fCalData)
      {
          SetInAttachCalI(true);
          InAttachCal(InTTree);
      }
      if(fCalData)
      {
          SetOutAttachCalI(true);
          SetHistogramsCal(true);
      }
      OutAttach(OutTTree);
  }
  else if(fMode == MODE_CALC) // Calculation Mode No Output
  {
      SetNoOutput();
  }
  else
  {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
  }



  END;
}


#endif
