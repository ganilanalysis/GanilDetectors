/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/



#include "CATS.hh"
CATS::CATS(const Char_t *Name, 
           UShort_t NDetectors, 
           Bool_t RawData, 
           Bool_t CalData, 
           Bool_t DefaultCalibration, 
           const Char_t *NameExt)
  : BaseDetector(Name, 4 , false, CalData, DefaultCalibration, false, NameExt)
{
  START;
  NumberOfSubDetectors = NDetectors;
  fRawDataSubDetectors  = RawData;
  SetId(Name);
  AllocateComponents();
  AddCounter("Present X"); //2
  AddCounter("Present X-SECHS"); //3
  AddCounter("Present X-WA"); //4
  AddCounter("Saturated X"); //5
  AddCounter("Present Y"); //6
  AddCounter("Present Y-SECHS"); //7
  AddCounter("Present Y-WA"); //8
  AddCounter("Saturated Y"); //9

  // Scanf of ID
  if(fCalData)
    {
      sprintf(CalNameI[0],"%s_X", DetectorName);
      sprintf(CalNameI[1],"%s_Y", DetectorName);
      sprintf(CalNameI[2],"%s_XWA", DetectorName);
      sprintf(CalNameI[3],"%s_YWA", DetectorName);
      // X in  [-1500, 0]
      SetGateCal(-2000,2000,0);
      // Y in [0,500]
      SetGateCal(-2000,2000,1);
      //  XWA  in  [-1500, 0]
      SetGateCal(-2000,2000,2);
      // YWA in [0,500]
      SetGateCal(-2000,2000,3);

    }

  ReadPosition();
#ifdef WITH_ROOT
  // Setup Histogram Hierarchy
  SetHistogramsCal1DFolder("Cats1D");
  SetHistogramsCal2DFolder("Cats2D");
#endif

  END;
}
CATS::~CATS(void)
{
  START;
  orderingArray.clear();
  END;
}

void CATS::ReadPosition(void)
{ 
  START;
  MIFile *IF;
  char Line[255];
  stringstream *InOut;
  
  InOut = new stringstream();
  *InOut << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << GetName() << "_Ref.cal";
  *InOut>>Line;
  InOut = CleanDelete(InOut);
  if((IF = CheckCalibration(Line)))
    {
      // Position
      try
        {
          ReadDefaultPosition(IF);
        }
      catch(...)
        {
          IF = CleanDelete(IF);
          throw;
        }
    }
  END;
}

void CATS::ReadDefaultPosition(MIFile *IF)
{
    if(fCalData)
    {
        char Line[255];
        UShort_t Len=255;
        Char_t *Ptr=NULL;

        IF->GetLine(Line,Len);
        while((Ptr = CheckComment(Line)))
        {
            IF->GetLine(Line,Len);
            L->File << Line << endl;
        }
        // Reference position X,Y,Z
        if(!GetCoeffs(Line,RefPos,3))
        {
            Char_t Message[500];
            sprintf(Message,"<%s><%s> the calibration file %s seems corrupted  has wrong format !", DetectorName, DetectorNameExt, IF->GetFileName());
            MErr * Er= new MErr(WhoamI,0,0, Message);
            throw Er;
        }
    }
}

void CATS::AllocateComponents(void)
{
  START;
  Bool_t RawData = fRawDataSubDetectors;
  Bool_t CalData = fCalData;
  Bool_t DefCal = true;
  Bool_t PosInfo = false;
 
  string BaseName;
  string Name;

  // CATSX
  BaseName = DetectorName ;
  Name = BaseName + "X";
  BaseDetector *CATSX = new BaseDetector(Name.c_str(),NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"",true);
  CATSX->AddCounter("Saturated");
  CATSX->AddCounter("Not-Neighbours (even Dist.)");
  CATSX->AddCounter("Strips M > 3");
  CATSX->AddCounter("Strips M > 3 (contiguous)");
  CATSX->AddCounter("Almost Neighbours ");
  CATSX->HasCalCharges(true);

  orderingArray.resize(1);
  orderingArray[0].resize(NumberOfSubDetectors);
  //Reads the reordering file in case there is a mismatch between the channel assignments and real detector positions due to cabling
  if(readCATSReordering(0,Name.c_str(),NumberOfSubDetectors))
  {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> the ordering file %s seems corrupted", DetectorName, DetectorNameExt, Name.c_str());
      MErr * Er = new MErr(WhoamI,0,0, Message);
      throw Er;
  }

  if(RawData)
    {
      CATSX->SetRawHistogramsParams(1000,0,16384,"");
      CATSX->SetHistogramsRaw1DFolder("Cats1D");
      CATSX->SetHistogramsRaw2DFolder("Cats2D");
    }
  if(CalData)
    {
      //      CATSX->SetGateCal(0,16384);
      CATSX->SetGateCal(150,32768);
      CATSX->SetCalHistogramsParams(1000,0,16384,"MeV","");
      CATSX->SetMainHistogramFolder(Name.c_str());
      CATSX->SetHistogramsCal1DFolder("Cats1D");
      CATSX->SetHistogramsCal2DFolder("Cats2D");
    }
  AddComponent(CATSX);

  // CATSY
  Name.clear();
  Name = BaseName + "Y";
  BaseDetector *CATSY = new BaseDetector(Name.c_str(),NumberOfSubDetectors,RawData,CalData,DefCal,PosInfo,"",true);
  CATSY->AddCounter("Saturated");
  CATSY->AddCounter("Not-Neighbours (even Dist.)");
  CATSY->AddCounter("Strips M >3");
  CATSY->AddCounter("Strips M >3 (contiguous)");
  CATSY->AddCounter("Almost Neighbours ");
  CATSY->HasCalCharges(true);

  orderingArray.resize(2);
  orderingArray[1].resize(NumberOfSubDetectors);

  if(readCATSReordering(1,Name.c_str(),NumberOfSubDetectors))
  {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> the ordering file %s seems corrupted", DetectorName, DetectorNameExt, Name.c_str());
      MErr * Er = new MErr(WhoamI,0,0, Message);
      throw Er;
  }

  if(RawData)
    {
      CATSY->SetRawHistogramsParams(1000,0,16384,"");
      CATSY->SetHistogramsRaw1DFolder("Cats1D");
      CATSY->SetHistogramsRaw2DFolder("Cats2D");
    }
  if(CalData)
    {
      //      CATSY->SetGateCal(0,16384);
      CATSY->SetGateCal(150,32768);
      CATSY->SetMainHistogramFolder(Name.c_str());
      CATSY->SetCalHistogramsParams(1000,0,16384,"MeV","");
      CATSY->SetHistogramsCal1DFolder("Cats1D");
      CATSY->SetHistogramsCal2DFolder("Cats2D");
    }
 
  AddComponent(CATSY);


#ifdef CATS_TRACE
  cout <<  "CATS_TRACE defined" << endl;
  Name.clear();
  Name = BaseName + "_0Tr";
  BaseDetector *TrX = new BaseDetector(Name.c_str(),64,false,true,true,false,"");
  if(CalData)
    {
      TrX->SetGateCal(0,4094);
      TrX->SetOutAttachCalV(true);
      TrX->SetHistogramsCal(true);
    }  
  AddComponent(TrX);
  Name.clear();
  Name = BaseName + "_1Tr";
  BaseDetector *TrY = new BaseDetector(Name.c_str(),64,false,true,true,false,"");
  if(CalData)
    {
      TrY->SetGateCal(0,4094);
      TrY->SetOutAttachCalV(true);
      TrY->SetHistogramsCal(true);
    }  
  AddComponent(TrY);
#endif

  END;
}

//The function meant to read the reordering in case there is a mismatch between the channel assignments and real detector positions due to cabling
bool CATS::readCATSReordering(int index,const Char_t *Name,UShort_t NoOfDet)
{
    stringstream *InOut = NULL;
    char Line[300];
    MIFile *IF = NULL;

    //Generate the name of the reordering file
    InOut = new stringstream();
    (*InOut) << getenv((EM->getPathVar()).c_str()) << "/Calibs/" << Name << "-Order.cal";
    (*InOut) >>Line;

   //Check that the Ordering file exists. If it does, read it. If not, create the default
    if((IF = CheckCalibration(Line)))
    {
         //Write the filename to screen
        cout<<Line<<endl;

        //Initialiaze various variables needed for reading the ordering file
        UShort_t Len=300;
        UShort_t CoeffsRead = 0;
        UShort_t CommentsRead = 0;
        Char_t *Ptr=NULL;
        UShort_t fLine;
        Char_t **Comment = NULL;
        int readDetectorIndex;

        //Initialize comment array
        Comment = AllocateDynamicArray<Char_t>(NoOfDet,Len);
        for(UShort_t i=0; i<NoOfDet; i++)
            sprintf(Comment[i]," ");

        //Write to the logfile
        L->File << "<" << Name <<"> Reading calibration file " << IF->GetFileName() << endl;

        //Read through the file
        do
        {
            //Read the comments to the Comment array and write that to the log file
            if((fLine =IF->GetLine(Line,Len)) >= V_INFO)
            {
                if((Ptr = CheckComment(Line))) //Comment line
                {
                    L->File << Ptr ;
                    if(CommentsRead >= V_INFO)  // 3 Line Header
                        L->File << " (at Coefficient of detector " << CoeffsRead << ")" ;
                    L->File << endl;
                    CommentsRead++;
                }
                else // Can read
                {
                    stringstream *InOut2 = new stringstream();
                    *InOut2 << Line;
                    *InOut2 >> readDetectorIndex;

                    if(readDetectorIndex==CoeffsRead)
                        *InOut2 >> orderingArray[index][CoeffsRead];
                    else
                    {
                        cout<<"Bad ordering on the Ordering file for "<<Name<<". Was expecting "<<CoeffsRead<<" but instead received "<<readDetectorIndex;
                        return false;
                    }

                    if(InOut2->fail())
                        return false;

                    InOut2 = CleanDelete(InOut2);

                    CoeffsRead++;
                }
            }
        } while( (CoeffsRead < NoOfDet) && (fLine>2));

        if(CoeffsRead < NoOfDet)
        {
            Comment = FreeDynamicArray<Char_t>(Comment,NoOfDet);
            Char_t Message[500];
            sprintf(Message,"In <%s><%s> the calibration file %s seems corrupted %d Coeffs found while expecting %d!", DetectorName, DetectorNameExt, IF->GetFileName(), CoeffsRead,  NoOfDet);
            MErr * Er = new MErr(WhoamI,0,0, Message);
            throw Er;
        }

        InOut = CleanDelete(InOut);
        Comment = FreeDynamicArray<Char_t>(Comment,NoOfDet);
        IF = CleanDelete(IF);
    }
    else
    {
        // Did not find the calibration file, so generate default one
        generateDefaultOrdering(index,Line, Name);
    }

    return 0;
}

//This function is called whenever the program cannot find the required ordering calibration. It creates a default calibration file that assumes everything is in the correct position
void CATS::generateDefaultOrdering(int index, Char_t *fileName, const Char_t *detName)
{
   START;

    //Write to screen and log that the Default Ordering File was created

    cout << "==============================================================" << endl;
    cout << "<" << detName << "> :" << endl <<
            "File " << fileName << " not found" << endl <<
            "Generate a new file with Default Ordering!" << endl;
    cout << "==============================================================" << endl;
    L->File << "==============================================================" << endl;
    L->File  << "<" << detName << "> :" << endl <<
                "File " << fileName << " not found" << endl <<
                "Generate a new file with Default Ordering!" << endl;
    L->File  << "==============================================================" << endl;

    //Try to create the file. If not allowed, throw an error
    MOFile *OF = NULL;
    try
    {
        OF = new MOFile(fileName);
    }
    catch(MErr *Er)
    {
        cout << "==============================================================" << endl;
        cout << "<" << detName << "> :" << endl <<
                "File " << fileName << " Could not be created" << endl
             << "Using default Ordering instead!" << endl;;
        cout << "==============================================================" << endl;
        L->File  << "==============================================================" << endl;
        L->File  << "<" << detName << "> :" << endl <<
                    "File " << fileName << " Could not be created" << endl
                 << "Using default Ordering instead!" << endl;;
        L->File  << "==============================================================" << endl;
        OF = CleanDelete(OF);

    }
    const time_t now = time(0);
    char* dt = ctime(&now);

    //Comments for the ordering file top
    OF->File << "// Title   : Default reordering file for <" << detName << "> :" << endl;
    OF->File << "// Date    : " << dt ;
    OF->File << "// Comment : The convention, as currently agreed to with Thomas Roger, is that, for CATSY, wire 0 is the lowest one, while wire 27 is the topmost one. For CATSX, looking along the beam (from downstream towards upstream), wire 0 is at the left, while wire 27 is on the right" << endl;
    OF->File << "// Format  : Real Wire Number \t Channel Number" << endl;

    //Create and allocate temporary name vector
    Char_t **channelNames;
    channelNames = NULL;
    channelNames = AllocateDynamicArray<Char_t>(NumberOfSubDetectors,20);

    //For each channel, create the default channel name and write it to the new ordering file
    for(UShort_t i =0; i<NumberOfSubDetectors;i++)
    {
         sprintf(channelNames[i],"%s_%d", detName,i);
         if(OF) OF->File << i << " " << channelNames[i] << endl;
         orderingArray[index][i]=channelNames[i];
    }
    OF = CleanDelete(OF);
    END;
}

void CATS::SetId(const Char_t *Name)
{
  START;
  UShort_t Id=0;

  if(sscanf(Name,"CATS%1hu",&Id)==1)
    {
      CATSId = Id;
      cout << CATSId << " CATS id" <<endl;
    }
  else
    {
      Char_t Message[500];
      sprintf(Message,"In <%s><%s> : Wrong Detector Name in (%s) while extracting its Id!", 
              GetName(),
              GetNameExt(),
              GetName());
      MErr * Er= new MErr(WhoamI,0,0, Message);
      throw Er;
    };
  END;
}

void CATS::SetParameters(Parameters* Par,Map* Map)
{ 
  START;
  if(isComposite)
    {
      Char_t PName[20];
      for(UShort_t i=0; i < 2; i++)
        for(UShort_t j=0; j < DetList->at(i)->GetNumberOfDetectors(); j++)
          {
            //initial CATS
            // if(CATSId == 2 && i == 1 && j == 14 ) // Swap Channel 15 <-> 16 in CATS2 Y in DAQ naming
            //   sprintf(PName,"%s%02d",DetList->at(i)->GetName(),15+1);
            // else if(CATSId == 2 && i == 1 && j == 15 ) // Swap Channel 15 <-> 16 in CATS2 Y in DAQ naming
            //   sprintf(PName,"%s%02d",DetList->at(i)->GetName(),14+1);
            // else
            //std::cout<<orderingArray[i][j]<<std::endl;
            strcpy(PName, orderingArray[i][j].c_str());
            //sprintf(PName,"%s_%d",DetList->at(i)->GetName(),j);
            //sprintf(PName,"MW_%d_%d",CATSId-1,DetList->at(i)->GetNumberOfDetectors()*i+j);
            DetList->at(i)->SetParameterName(PName,j);
          }
      
      for(UShort_t i=0; i< DetList->size(); i++)
        DetList->at(i)->SetParameters(Par,Map);
    }
  
  END;
}

Bool_t CATS::Treat(void)
{
  START;
  Ctr->at(0)++;
  if(isComposite)
    {
     for(UShort_t i=0; i< DetList->size(); i++)
         if(DetList->at(i)->GetRawM())
            {
             DetList->at(i)->CalibrateCharges();
             if(DetList->at(i)->GetCalM()>0)
                 DetList->at(i)->IncrementCounter(0);
                 DetList->at(i)->SetPresent(true);
            }
       //DetList->at(i)->Treat();
    }
 
  if(fCalData)
    {
      if(DetList->at(0)->IsPresent())
        {
          fPresentX = TreatWire(0);
        }
      else
        {
          fPresentX = false;

        }
      if(DetList->at(1)->IsPresent())
        {
          fPresentY = TreatWire(1);
        }
      else
      {
          fPresentY = false;
      }
  }

  
  if(fPresentX)
  {
      SetCalData(0,XSECHIP);
      SetCalData(2,XWA);
  }
  else
  {
      SetCalData(0,-1500);
      SetCalData(2,-1500);
  }

  if(fPresentY)
  {
      SetCalData(1,YSECHIP);
      SetCalData(3,YWA);
  }
  else
  {
      SetCalData(1,-1500);
      SetCalData(3,-1500);
  }

  if(fPresentX && fPresentY){
      isPresent = true;
  }
  else{
      isPresent = false;
  }



  return isPresent;
  END;
}

Bool_t CATS::TreatWire(UShort_t n)
{
  START;

  UShort_t NStrips = 3 ;
  Float_t QTmp[28];
  Float_t QMax;
  UShort_t NMax;
  UShort_t FStrip[28];
  Bool_t Neighbours;
  Bool_t Present = false;
  Float_t Xtemp=-1500.;
  Float_t XWAtemp=-1500.;

  if(DetList->at(n)->GetM() >= NStrips)
    Ctr->at(2+n*4)++;
  else
    {
      return false;
    }

  for(UShort_t j=0; j< DetList->at(n)->GetM();j++) //loop over strips
    {
      QTmp[j] = DetList->at(n)->GetCalAt(j);// Copy Charges
    }
  // Find GetM highest charges NStrips
  for(UShort_t k=0;k<DetList->at(n)->GetM(); k++)
    {
      QMax=0.0;
      NMax=0;
      for(UShort_t j=0;j<DetList->at(n)->GetM();j++)
        {
          if(QTmp[j] > QMax)
            {
              QMax = QTmp[j];
              NMax = j;
            }
        }
      QTmp[NMax] = 0.0;
      FStrip[k] = NMax;
    }
  
  if(NStrips != 3) 
    {
      MErr * Er = new MErr(WhoamI,0,0, "NStrips != 3 but");
      throw Er;      
    }
  Neighbours = false;
  
  if((Neighbours = CheckNeighbours(FStrip,n)))
    {
      // DetList->at(n)->PrintCal();
      Xtemp = SECHIP(FStrip,n);
      // cout << "Sechip " <<  X << endl;
    }
//   else
//     {
//    DetList->at(n)->PrintCal();
//     }
  XWAtemp = WeightedAverage(FStrip,NStrips,n);

  if((XWAtemp > 0 && XWAtemp <  DetList->at(n)->GetNumberOfDetectors()))
    {
      Present = true;
  
      if(n == 0) // X
        {

          XWA=XWAtemp;

          // Now change to mm unit
          XWA -= DetList->at(n)->GetNumberOfDetectors()/2.-0.5; // Center in the middle
          XWA *= 2.54; //Goes 1/10 inch spacing

          XWA += GetRefX();
        }
      else if(n == 1) // Y
        {
          YWA=XWAtemp;

          // Now change to mm unit
          YWA -= DetList->at(n)->GetNumberOfDetectors()/2.-0.5; // Center in the middle
          YWA *= 2.54; //Goes 1/10 inch spacing

          YWA += GetRefY();
        }
    }
  else
    {
      if(n==0)
      {
        XWA = -1500;
      }
      else if(n==1)
      {
        YWA = -1500;
      }
    }
  if((Xtemp > 0 && Xtemp <  DetList->at(n)->GetNumberOfDetectors()))
    {
      Present = true;

      if(n == 0) // X
        {
          XSECHIP=Xtemp;

          // Now change to mm unit
          XSECHIP -= DetList->at(n)->GetNumberOfDetectors()/2.-0.5; // Center in the middle
          XSECHIP *= 2.54; //Goes 1/10 inch spacing

          XSECHIP += GetRefX();
        }
      else if(n == 1) // Y
        {
          YSECHIP=Xtemp;

          // Now change to mm unit
          YSECHIP -= DetList->at(n)->GetNumberOfDetectors()/2.-0.5; // Center in the middle
          YSECHIP *= 2.54; //Goes 1/10 inch spacing

          YSECHIP += GetRefY();
        }
    }
  else
    {
      if(n==0)
      {
        XSECHIP = -1500;
      }
      else if(n==1)
      {
        YSECHIP = -1500;
      }
    }
  return Present;
  END;
}


Bool_t CATS::CheckNeighbours(UShort_t *FStrip, UShort_t n)
{
  START;
  Bool_t rval = false;
  UShort_t tmp = 0;
  if( abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[1])) == 1 
      &&
      abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[2])) == 1 )
    return true;
  else
    {
      DetList->at(n)->IncrementCounter(3);
      // #ifdef CATS_TRACE
      //       for(UShort_t k=0; k<DetList->at(n)->GetM(); k++)
      //         {
      //           DetList->at(3+n)->SetCalData(DetList->at(n)->GetNrAt(FStrip[k]),DetList->at(n)->GetCalAt(FStrip[k]));
      //         }
      // #endif
      if(DetList->at(n)->GetM()>3)
        {

          if( abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[1])) == 1 
              && 
              abs(DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[3])) == 1)
            {
              Float_t Diff = (DetList->at(n)->GetCalAt(FStrip[2])-DetList->at(n)->GetCalAt(FStrip[3]))/0.5/(DetList->at(n)->GetCalAt(FStrip[2])+DetList->at(n)->GetCalAt(FStrip[3]))*100 ;
              // cout << " Were Almost neighbours" << endl;
              // cout <<  DetList->at(n)->GetCalAt(FStrip[1]) << endl;
              // cout <<  DetList->at(n)->GetCalAt(FStrip[2]) << endl;
              // cout <<  DetList->at(n)->GetCalAt(FStrip[3]) << endl;
              // cout << "\t " << Diff << "% difference"<< endl;
				  
              if (Diff < 15) 
                {						
                  tmp = FStrip[2];
                  FStrip[2] = FStrip[3] ;
                  FStrip[3] = tmp;	
                  rval = true;
                  DetList->at(n)->IncrementCounter(6);
                }
            }
        }

      return rval ;
    }
  END; 
}

Float_t CATS::WeightedAverage(UShort_t *FStrip, UShort_t NStrips, UShort_t n)
{ 
  START;
  Float_t v[2];
  Float_t QTotal=-10.;
  Float_t QRatio=-1.;
  // UShort_t StripsWA=0;
  UShort_t StripsWA=0;
  Float_t XWA=-1500.;
  UShort_t MaxWireNr[64];
  Float_t LStripQ[64];
  Bool_t isPresent = false;
  if(DetList->at(n)->GetM() > NStrips)
    {
      DetList->at(n)->IncrementCounter(4);
      //Looking for entire peak for W.A.
      //The Strips are NOT ordered  0-64 ...
      //Coul be done earlier but ....

      // Reoder the strips ...


      for(UShort_t j=0; j< DetList->at(n)->GetNumberOfDetectors();j++) 
        {
          LStripQ[j]=0;
          MaxWireNr[j] = 0.;
        }
      MaxWireNr[0] = DetList->at(n)->GetNrAt(FStrip[0]);
#ifdef  DEBUG_PP
      cout << "--> <"<<n<< "> Start ----------------------" <<endl;
#endif
	   
      for(UShort_t j=0; j< DetList->at(n)->GetM();j++) //loop over strips
        {	       
          LStripQ[DetList->at(n)->GetNrAt(j)] = DetList->at(n)->GetCalAt(j);// Copy Charges
#ifdef DEBUG_PP	 
          cout << j << " " << DetList->at(n)->GetNrAt(j) << "\t Cal : " <<  DetList->at(n)->GetCalAt(j) << endl;
#endif
        }
	   
#ifdef  DEBUG_PP
      for(UShort_t j=0; j< DetList->at(n)->GetNumberOfDetectors();j++) 
        {
          cout << "Nr : " << j <<  " Q: "  << LStripQ[j];
          if(j == MaxWireNr[0])  cout << "   <--- Max " ;
          cout <<endl;
        }

		cout << "Max Pad Nr First " << FStrip[0] << " -> " << DetList->at(n)->GetNrAt(FStrip[0]) << endl;
      
#endif
 

      StripsWA=0;

    
      for(Int_t j=MaxWireNr[0]-1;j>=0;j--)
        {
          if((LStripQ[j] <= LStripQ[j+1]) && LStripQ[j] > 0)
            {		   
              StripsWA++;
              MaxWireNr[StripsWA]=j;
            }
          else
            break;
        }
	   
      for(Int_t j=MaxWireNr[0]+1;64;j++)
        {
          if((LStripQ[j-1] >= LStripQ[j]) && LStripQ[j] > 0)
            {
              StripsWA++;
              MaxWireNr[StripsWA]=j;			  
            }
          else
            break;
        }
	       
	       
#ifdef DEBUG_PP
      cout << "CATS::TreatWire("<<n<<")::Weithed Average: "  << endl;
      cout << "StripsWA: " << StripsWA << endl; 
#endif	     

      if(StripsWA >= NStrips)
        {
          DetList->at(n)->IncrementCounter(5);
          v[0] = v[1] = 0.0;
          for(UShort_t k=0;k<=StripsWA;k++)
            {		      
              v[0] += LStripQ[MaxWireNr[k]] * ((Float_t) MaxWireNr[k]);
              v[1] += LStripQ[MaxWireNr[k]];
            }
			 
          XWA = v[0] / v[1];
          QTotal = v[1];
          QRatio = LStripQ[MaxWireNr[0]]/QTotal;
#ifdef DEBUG_PP
          // cout << "CATS::TreatWire("<<n<<")::Weithed Average: "  << endl;
          // cout << "StripsWA: " << StripsWA << endl;
          for(UShort_t k=0;k<=StripsWA;k++)
            cout <<  StripsWA << " WIREWA: " << MaxWireNr[k] << " Q: " << LStripQ[MaxWireNr[k]] << endl;
          cout << "Result 2: " << XWA << "mm" << endl ;
          cout << "Total Charge 2: " << QTotal << endl;
#endif
	 	    
          if(XWA > 0. && XWA <DetList->at(n)->GetNumberOfDetectors())
            {
              isPresent = true;
              Ctr->at(4+n*4)++;		  
            }
          else 
            {
              XWA = -1500.;
            }
	  

        }
      else
        {
          XWA = -1500.;
          QTotal = 0;
          QRatio = 0;
        }


      // QTotal
      // SetCalData(n+4,QTotal);
      // // MaxPadQ
      // SetCalData(n+6,LStripQ[MaxWireNr[0]]);
      // // MaxPadNrQ
      // SetCalData(n+8,MaxWireNr[0]);
      // // Ratio of Max Charge / QTotal
      // SetCalData(n+10,QRatio);
      // // Multiplicity
      // SetCalData(n+12,DetList->at(n)->GetM());

    }

  if(isPresent)
    {
#ifdef CATS_TRACE
      for(UShort_t k=0; k<DetList->at(n)->GetM(); k++)
        {
          // DetList->at(2+n)->SetCalData(DetList->at(n)->GetNrAt(FStrip[k]),DetList->at(n)->GetCalAt(FStrip[k]));
        }
#endif
    }
  return XWA;
  END;
}



Float_t CATS::SECHIP(UShort_t *FStrip,UShort_t n)
{
  START;
  Float_t v[6];
  Float_t XS=-1500;
  v[0] = sqrt(DetList->at(n)->GetCalAt(FStrip[0])/DetList->at(n)->GetCalAt(FStrip[2]));
  v[1] = sqrt(DetList->at(n)->GetCalAt(FStrip[0])/DetList->at(n)->GetCalAt(FStrip[1]));
  v[2] = 0.5*(v[0]+v[1]);
  v[3] = log(v[2]+sqrt(pow(v[2],2.f)-1.0));
  v[4] = (v[0] - v[1])/(2.0*sinh(v[3]));
  v[5] = 0.5*log((1.0+v[4])/(1.0-v[4]));
  

  XS = (Float_t) DetList->at(n)->GetNrAt(FStrip[0]) 
	 - (Float_t) (DetList->at(n)->GetNrAt(FStrip[0])-DetList->at(n)->GetNrAt(FStrip[1]))*v[5]/v[3];
  // cout <<  DetList->at(n)->GetNrAt(FStrip[0])  <<  " " << XS << endl; 

  if(XS > 0. && XS < DetList->at(n)->GetNumberOfDetectors())
    {
      Ctr->at(3+n*4)++;
      // for(j=0;j<3;j++)
      // 		{
      // 		  QPk[j] = Q[FStrip[j]];
      // 		  NPk[j] = N[FStrip[j]];
      // 		}
      
    }
  else
    XS = -1500.;
  
  return XS;
  END;
}


#ifdef WITH_ROOT
void CATS::CreateHistogramsCal1D(TDirectory *Dir)
{
  START;
  string Name;
 
 Dir->cd("");		
  if(SubFolderHistCal1D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal1D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());
		gDirectory->cd(Name.c_str());
	 }
		

  // XS
  AddHistoCal(CalNameI[0],CalNameI[0],"XS (mm)",1200,-60,60);
  // YS
  AddHistoCal(CalNameI[1],CalNameI[1],"YS (mm)",1200,-60,60);
  // XWA
  AddHistoCal(CalNameI[2],CalNameI[2],"XWA (mm)",1200,-60,60);
  // YWA
  AddHistoCal(CalNameI[3],CalNameI[3],"YWA (mm)",1200,-60,60);

  END;
}

void CATS::CreateHistogramsCal2D(TDirectory *Dir)
{
  START;
  string Name;

  BaseDetector::CreateHistogramsCal2D(Dir);
    
  Dir->cd("");
  
  if(SubFolderHistCal2D.size()>0)
	 {
		Name.clear();
		Name = SubFolderHistCal2D ;
		if(!(gDirectory->GetDirectory(Name.c_str())))
		  gDirectory->mkdir(Name.c_str());		 
		gDirectory->cd(Name.c_str());
	 }

  //Added these matrixNames in order to separate CATS1 and CATS2 2D plots from each other.
  Char_t **matrixNames;
  matrixNames = NULL;
  matrixNames = AllocateDynamicArray<Char_t>(2,20);
  sprintf(matrixNames[0],"%s_XY_S", DetectorName);
  sprintf(matrixNames[1],"%s_XY_WA", DetectorName);


  AddHistoCal(CalNameI[0],CalNameI[1],matrixNames[0],"X_vs_Y (SECHS)",1200,-60,60,1200,-60,60);
  AddHistoCal(CalNameI[2],CalNameI[3],matrixNames[1],"X_vs_Y (WeigtedAverage)",1200,-60,60,1200,-60,60);

  END;
}


void CATS::SetOpt(TTree *OutTTree, TTree *InTTree)
{
  START;
 
  // Set histogram Hierarchy
  SetMainHistogramFolder("");
  SetHistogramsCal1DFolder("Cats1D");
  SetHistogramsCal2DFolder("Cats2D");
 
  for(UShort_t i = 0;i<DetList->size();i++)
	 {
		DetList->at(i)->SetMainHistogramFolder("");
		DetList->at(i)->SetHistogramsRaw1DFolder("Cats1D");
		DetList->at(i)->SetHistogramsRaw2DFolder("Cats2D");
	 	DetList->at(i)->SetHistogramsCal1DFolder("Cats1D");
		DetList->at(i)->SetHistogramsCal2DFolder("Cats2D");
	 }

  if(fMode == 0)
    {
      if(fRawData)
        {
          SetHistogramsRaw(true);
          // SetOutAttachRawV(true);
        }
      if(fCalData)
        {
          SetHistogramsCal(true);
          // SetOutAttachCalI(true);
        }
      for(UShort_t i = 0;i<2;i++)
        {
          if(fRawDataSubDetectors)
            {
              DetList->at(i)->SetHistogramsRaw2D(true);
              DetList->at(i)->SetHistogramsRawSummary(true);
            }
          DetList->at(i)->SetHistogramsCal1D(false);
          DetList->at(i)->SetHistogramsCal2D(true);
          DetList->at(i)->SetHistogramsCalSummary(true);
        }
      if(fRawDataSubDetectors)
        {
          // DetList->at(2)->SetHistogramsRaw(true);
          // DetList->at(2)->SetOutAttachRawI(true);
        }
      // DetList->at(2)->SetHistogramsCal(true);
      // DetList->at(2)->SetOutAttachCalI(true);
	
      // OutAttach(OutTTree);
      // 	for(UShort_t i = 0;i<DetList->size();i++)
      // 	  DetList->at(i)->OutAttach(OutTTree);

    }
  else if(fMode == 1)
    {
      for(UShort_t i = 0;i<2;i++)
        {
          DetList->at(i)->SetHistogramsRaw2D(true);
          DetList->at(i)->SetHistogramsRawSummary(true);
          DetList->at(i)->SetOutAttachRawV(true);
        }
      // QFIl
      // DetList->at(2)->SetHistogramsRaw(true);
      // DetList->at(2)->SetOutAttachRawI(true);
      for(UShort_t i = 0;i<DetList->size();i++)
        DetList->at(i)->OutAttach(OutTTree);
      OutAttach(OutTTree);
	
    }
  else if(fMode == 2)
	 {
	   if(fRawData)
	     {
	       SetHistogramsRaw(true);
	       SetOutAttachRawV(true);
	     }
	   if(fCalData)
	     {
	       SetHistogramsCal(true);
	       SetOutAttachCalI(true);
	     }
		
	   for(UShort_t i = 0;i<2;i++)
	     {
	       if(fRawDataSubDetectors)
            {
              DetList->at(i)->SetHistogramsRaw1D(false);
              DetList->at(i)->SetHistogramsRaw2D(true);
              DetList->at(i)->SetHistogramsRawSummary(true);
//              DetList->at(i)->SetOutAttachRawV(true);
            }
	       DetList->at(i)->SetHistogramsCal1D(false);
	       DetList->at(i)->SetHistogramsCal2D(true);
	       DetList->at(i)->SetHistogramsCalSummary(true);
	       DetList->at(i)->SetOutAttachCalI(false);
           DetList->at(i)->SetOutAttachCalV(true);
	     }
	   if(fRawDataSubDetectors)
	     {
	       // DetList->at(2)->SetHistogramsRaw(false);
	       // DetList->at(2)->SetOutAttachRawI(false);
	     }
	   // QFIL
	   // DetList->at(2)->SetHistogramsCal(true);
	   // DetList->at(2)->SetOutAttachCalI(true);
#ifdef CATS_TRACE
	   for(UShort_t i = 2;i<4;i++)
		  {
		    DetList->at(i)->SetHistogramsCal1D(false);
		    DetList->at(i)->SetHistogramsCalSummary(true);
		    DetList->at(i)->SetOutAttachCalI(false);
		    DetList->at(i)->SetOutAttachCalV(true);
		  }

#endif
		OutAttach(OutTTree);
		for(UShort_t i = 0;i<DetList->size();i++)
		  DetList->at(i)->OutAttach(OutTTree);
	 }
  else if(fMode == 3)
    {
      SetInAttachRawV(true);
      InAttach(InTTree);
		
      SetOutAttachCalI(true);
      OutAttach(OutTTree);
		
    }
  else 
	 {
        Char_t Message[500];
		sprintf(Message,"In <%s><%s> Trying to set the detector unknown Mode (%d) !", GetName(), GetName(),fMode );
		MErr * Er= new MErr(WhoamI,0,0, Message);
		throw Er;
	 }



  END;
}

#endif
