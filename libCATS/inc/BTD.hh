/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _BTD_CLASS
#define _BTD_CLASS

/**
 * @file   BTD.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   Aug, 2013
 * @brief  BTD Class
 * 
 * Target BTD class
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#include "CATS.hh"
#ifdef WITH_ROOT
#include "TF1.h"
#endif
class BTD : public BaseDetector
{
public:
  BTD(const Char_t * Name,  //!< Detector Name
      UShort_t NDetectors,   //!< Number of sub detectors
      Bool_t RawData,        //!< Has Raw Data
      Bool_t CalData,        //!< Has Calibrated Data
      Bool_t DefaultCalibration,//!< Used Default Calibration
      const Char_t * NameExt //!< Name extension
      );
  ~BTD(void);

  // //! Clear At the end of event;
  // void Clear(void);

  Bool_t Treat(void);   //!< Treat the data
  // void ReadCalibration(void);   //!< Specific reading of Calibration and thresholds

#ifdef WITH_ROOT
  //! Create 2D Histograms for Calculated Parameters
  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Create 1D Histograms for Calculated Parameters
  void CreateHistogramsCal1D(TDirectory *Dir);
  // Set default options  Input/Output according to fMode 
  void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par, //!< Pointer to Parameter Names Table
                     Map* Map         //!< Pointer to Detectors Map
                     );   
  
  
protected:
  // Read Position information
  void ReadPosition(void);

  //! Calculate Focal plane X coordinates from sub detectors
  Bool_t FocalX(void);
  //! Calculate Focal plane Y coordinates from sub detectors
  Bool_t FocalY(void);
  //! Allocate subcomponents
  void AllocateComponents(void);
  //! Least Square Matrix for X
  void SetMatX(void);
  //! Least Square Matrix for Y
  void SetMatY(void);

  Double_t TargetPos; //< Focal Position
  Double_t MatX[2][2]; 
  Double_t MatY[2][2];


};
#endif
