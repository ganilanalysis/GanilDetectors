/****************************************************************************
 *    Copyright (C) 2012-2015 by Antoine Lemasson, Maurycy Rejmund
 *    lemasson@ganil.fr
 *    
 *    Contributor(s) : 
 *    Antoine Lemasson, lemasson@ganil.fr
 *    Maurycy Rejmund, rejmund@ganil.fr
 *    
 *    This software is  a computer program whose purpose  is to provide data
 *    manipulation tools to Analyse experimental data collected at the VAMOS
 *    spectrometer.
 *    
 *    This software is governed by the CeCILL-B license under French law and
 *    abiding by the  rules of distribution of free  software.  You can use,
 *    modify  and/ or  redistribute  the  software under  the  terms of  the
 *    CeCILL-B license as circulated by CEA, CNRS and INRIA at the following
 *    URL \"http://www.cecill.info\".
 *    
 *    As a counterpart to the access  to the source code and rights to copy,
 *    modify  and redistribute granted  by the  license, users  are provided
 *    only with a limited warranty  and the software's author, the holder of
 *    the economic  rights, and the  successive licensors have  only limited
 *    liability.
 *    
 *    In this respect, the user's attention is drawn to the risks associated
 *    with loading,  using, modifying  and/or developing or  reproducing the
 *    software by the user in light of its specific status of free software,
 *    that  may mean that  it is  complicated to  manipulate, and  that also
 *    therefore  means that it  is reserved  for developers  and experienced
 *    professionals having in-depth  computer knowledge. Users are therefore
 *    encouraged  to load  and test  the software's  suitability  as regards
 *    their  requirements  in  conditions  enabling the  security  of  their
 *    systems  and/or data to  be ensured  and, more  generally, to  use and
 *    operate it in the same conditions as regards security.
 *    
 *    The fact that  you are presently reading this means  that you have had
 *    knowledge of the CeCILL-B license and that you accept its terms.
 ***************************************************************************/
#ifndef _CATS_CLASS
#define _CATS_CLASS

/**
 * @file   CATS.hh
 * @Author Antoine Lemasson (lemasson@ganil.fr)
 * @date   Aug, 2013
 * @brief  CATS Class
 * 
 * Target CATS class
 */

#include "Defines.hh"
#include "BaseDetector.hh"
#include <math.h>
#ifdef WITH_ROOT
#include "TF1.h"
#endif
class CATS : public BaseDetector
{
public:
  CATS(const Char_t * Name,  //!< Detector Name
      UShort_t NDetectors,   //!< Number of sub detectors
      Bool_t RawData,        //!< Has Raw Data
      Bool_t CalData,        //!< Has Calibrated Data
      Bool_t DefaultCalibration,//!< Used Default Calibration
      const Char_t * NameExt //!< Name extension
      );
  ~CATS(void);

  // //! Clear At the end of event;
  // void Clear(void);
  //! True X position is present
  Bool_t PresentX(void) {return fPresentX;};
  //! True Y position is present
  Bool_t PresentY(void) {return fPresentY;};

  Bool_t Treat(void);   //!< Treat the data
  // void ReadCalibration(void);   //!< Specific reading of Calibration and thresholds

#ifdef WITH_ROOT
  //! Create 2D Histograms for Calculated Parameters
  void CreateHistogramsCal2D(TDirectory *Dir);
  //! Create 1D Histograms for Calculated Parameters
  void CreateHistogramsCal1D(TDirectory *Dir);
  // Set default options  Input/Output according to fMode 
  void SetOpt(TTree *OutTTree, TTree *InTTree);
#endif
  void SetId(const Char_t *Name); //!< Set Unique Id of the Chamber (0-3)
  //! Set Parameter Patterns associated with Detector
  void SetParameters(Parameters* Par, //!< Pointer to Parameter Names Table
							Map* Map         //!< Pointer to Detectors Map
							); 
  bool readCATSReordering(int i, const Char_t *Name, UShort_t NoOfDet); //!<Function meant to read any reordering of the CATS channels due to wrong cabling (Lucian)
  void generateDefaultOrdering(int index, Char_t *fileName, const Char_t *detName); //!< If an ordering file does note exist, create it (Lucian)
  Bool_t TreatWire(UShort_t N);   //!< Treat X position and return True if X within bounds [-1500,0]
  void ReadPosition(void);
  std::vector< std::vector< string> > orderingArray;
  void ReadDefaultPosition(MIFile *IF); //!<Reimplementation of the BaseDetector function in order to read the CATS position with an angle as well
  Float_t XSECHIP =-1500.;
  Float_t YSECHIP = -1500.;
  Float_t XWA = -1500.;
  Float_t YWA = -1500.;
  
  
protected:
  //! Allocate subcomponents
  void AllocateComponents(void);
  //! Look for Multiple Peaks
  Bool_t CheckMultiplePeaks(UShort_t *FStrip, //!< Array of Found Strips Number 
                            UShort_t N //!< Wire plane number
                            ); 
  //! Check that found strip are neigbours 
  Bool_t CheckNeighbours(UShort_t *FStrip, //!< Array of Found Strips Number 
                         UShort_t N //!< Wire plane number
                         ); 
  // //! Return position determined weigted average method
  Float_t WeightedAverage(UShort_t *FStrip, //!< Array of Found Strips Number 
                          UShort_t NStrips,  //!< Number of Strips
                          UShort_t N  //!< Number of Strips
                          ); 
  // void ReadReference(MIFile *IF); //!< Read reference information from Calibration File
  // void SetId(const Char_t *Name); //!< Set Unique Id of the Chamber (0-3)
  Float_t SECHIP(UShort_t *FStrip, UShort_t N); //!< Return position determined by Squente Hyperbolique


  UShort_t CATSId;  //!< Unique Id of the CATS (1-2)


  Bool_t fPresentWires;
  Bool_t fPresentX;
  Bool_t fPresentY;
  

};
#endif
