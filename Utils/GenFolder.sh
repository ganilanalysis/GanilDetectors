#!/bin/bash

if [[ $# -lt 1 ]]
then
    echo "Usage : $0 <NAME>"
    echo " Tool to generate directory structure "
    echo " libNAME/"
    echo "    |- inc/ "
    echo "    |- src/ "
    echo "    |- libTESTConfig.cmake.in"
    echo "    |- CMakeLists.txt "
    exit -1
else
    NAME=$1
    LIBNAME="lib${NAME}"
    echo "Generate directory structure for ${LIBNAME}"
    echo "  ${LIBNAME}/"
    echo "    |- inc/ "
    echo "    |- src/ "
    echo "    |- ${LIBNAME}Config.cmake.in"
    echo "    |- CMakeLists.txt "
fi


if [[ -d ${LIBNAME} ]]
then
    echo "The Folder ${LIBNAME} already exist !"
    exit -1
fi


#

echo "Creating ./${LIBNAME}"
echo "Creating ./${LIBNAME}/inc"
echo "Creating ./${LIBNAME}/inc"

mkdir -p ./${LIBNAME}/inc
mkdir -p ./${LIBNAME}/src

echo "Generating  ${LIBNAME}/CMakeLists.txt"
cat ./Utils/Default_CMakeLists.txt | sed "s/###NAME###/${NAME}/g" > ${LIBNAME}/CMakeLists.txt

echo "Generating  ${LIBNAME}/${LIBNAME}Config.cmake.in"
cat ./Utils/DefaultConfig.cmake.in | sed "s/###NAME###/${LIBNAME}/g" > ${LIBNAME}/${LIBNAME}Config.cmake.in

ls ${LIBNAME}
