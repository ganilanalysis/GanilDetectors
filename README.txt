###################################
# libVamos
# Antoine LEMASSON
# lemasson@ganil.fr
# December, 2012
###################################

1. Description : 

2. Build and Install : 

  2.1 Build the library: 
	cmake -DWITH_ROOT=ON -DCMAKE_INSTALL_PREFIX=/Path/to/InstallDir/ /Path/to/Source/
	make 
	make install

  2.2 Setup the environment:
	Set the enviroment variable VAMOSLIB to /Path/to/InstallDir/, 
	and add VAMOSLIB to LD_LIBRARY_PATH. This can be done using 
	generated environment scripts situated in /Path/to/InstallDir/bin
		
	For csh environment, add the following line to ~/.cshrc : 
	source /Path/to/InstallDir/bin/env.csh

	For csh environment, add the following line to ~/.bashrc :
	source /Path/to/InstallDir/bin/env.sh

4. Documentation
	in the install directory you can generate doxygen documentation 
	cd /Path/to/InstallDir/doc
	doxygen doxyfile


5. MUGAST
